#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <cctype>

extern "C" {
#include "uv.h"
#include <sys/stat.h>
}

#ifdef _WIN32
static const char SEPARATOR_CHAR = '\\';
static const char* SEPARATOR_STR = "\\/";
#else
static const char SEPARATOR_CHAR = '/';
static const char* SEPARATOR_STR = "/";
#endif // _WIN32

static bool exists(const char *path)
{
    struct stat b;
    return 0 == stat(path, &b);
}

static bool load_file(const char *name, bool binary, std::string *buf) {
    std::ifstream ifs(name, binary ? std::ifstream::binary : std::ifstream::in);
    if (!ifs.is_open())
        return false;
    *buf = std::string(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
    return !ifs.bad();
}

static std::string absolute_path(const std::string &filepath) {
  #ifdef _WIN32
    char abs_path[MAX_PATH];
    return GetFullPathNameA(filepath.c_str(), MAX_PATH, abs_path, nullptr)
  #else
    char abs_path[PATH_MAX];
    return realpath(filepath.c_str(), abs_path)
  #endif
    ? abs_path
    : filepath;
}

// Return the last component of the path, after the last separator.
static std::string strip_file_path(const std::string &filepath) {
    size_t i = filepath.find_last_of(SEPARATOR_STR);
    return i != std::string::npos ? filepath.substr(i + 1) : filepath;
}

// Strip the last component of the path + separator.
static std::string strip_file_name(const std::string &filepath) {
    size_t i = filepath.find_last_of(SEPARATOR_STR);
    return i != std::string::npos ? filepath.substr(0, i) : "";
}

inline bool ends_with(std::string const & value, std::string const & suffix)
{
    return suffix.size() <= value.size() && std::equal(suffix.rbegin(), suffix.rend(), value.rbegin());
}

static void trim_trailing_ws(std::string& str)
{
    for (char c = str.back(); std::isspace(c); c = str.back())
        str.pop_back();
}

static void add_args_to(std::vector<std::string>& list, std::string& args)
{
    size_t start = 0;
    for (size_t idx = args.find(' ');
            idx != std::string::npos;
            start = idx + 1, idx = args.find(' ', start))
    {
        list.push_back(args.substr(start, idx - start));
    }
    
    if (start == 0)
        list.push_back(args);
    else
        list.push_back(args.substr(start));
}

const std::string suffix_ts = "-ts";

bool get_ts_dir(const std::string &directory, std::string* out)
{
#ifdef _WIN32
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return false;
    
    do
    {
        if ('.' == file_data.cFileName[0])
            continue;
        
        std::string path = directory + "/" + file_data.cFileName;
        // check if dir
        if (0 == (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) || !ends_with(path, suffix_ts))
            continue;
        
        *out = strip_file_path(path);
        return 0 != out->length();
    }
    while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != nullptr)
    {
        std::string path = directory + "/" + ent->d_name;
        if (-1 == stat(path.c_str(), &st) || '.' == path[directory.length() + 1] ||
                // check if dir
                0 == (st.st_mode & S_IFDIR) || !ends_with(path, suffix_ts))
        {
            continue;
        }
        
        *out = strip_file_path(path);
        return 0 != out->length();
    }
    closedir(dir);
#endif
    return false;
}

static const char* const chrome_bins[] = {
#ifdef _WIN32
"C:\\opt\\chromium\\chrome.exe",
"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"
};
#define CHROME_BIN_COUNT 3
#else
"/opt/chromium/chrome",
"/usr/bin/chromium-browser",
"/usr/bin/chromium",
"/usr/bin/google-chrome"
};
#define CHROME_BIN_COUNT 4
#endif


static const char PDB_BIN[] = 
#ifdef _WIN32
"target\\protostuffdb.exe";
#else
"target/protostuffdb";
#endif

static const char PDB_TRY_BIN[] = 
#ifdef _WIN32
"target\\protostuffdb-sp.exe";
#else
"target/protostuffdb-rjre";
#endif

#define OUTPUT_SIZE 1024
static char output[OUTPUT_SIZE];
static int output_used = 0;

std::string root_absolute_dir;
std::string str_port; // port where pdb binds to

uv_loop_t *loop;
uv_process_options_t options;

uv_process_t child_req;
uv_pipe_t child_out;
bool child_started = false;

uv_process_options_t chrome_options;
uv_process_t chrome_req;
int chrome_idx;

static void on_chrome_exit(uv_process_t* req, int64_t status, int signal)
{
    if (status)
        std::cerr << "chrome exited with status " << status << std::endl;
    
    uv_close((uv_handle_t*) req, NULL);
    // sigterm
    exit(uv_process_kill(&child_req, 15) ? 1 : 0);
}

static void start_chrome()
{
    char* args[6];
    
    chrome_options.exit_cb = on_chrome_exit;
    chrome_options.file = chrome_bins[chrome_idx];
    chrome_options.args = args;
    
    std::string arg1 = "--app=http://127.0.0.1:" + str_port;
    std::string arg2 = "--user-data-dir=" + root_absolute_dir + "/target/session";
    
    args[0] = (char*)chrome_bins[chrome_idx];
    args[1] = (char*)arg1.c_str();
    args[2] = (char*)arg2.c_str();
    args[3] = (char*)"--no-proxy-server";
    args[4] = (char*)"--disk-cache-size 0";
    args[5] = NULL;
    
    int r;
    if ((r = uv_spawn(loop, &chrome_req, &chrome_options)))
    {
        fprintf(stderr, "chrome spawn failed: %s\n", uv_strerror(r));
    }
}

static void on_child_exit(uv_process_t* req, int64_t status, int signal)
{
    if (status)
        std::cerr << "protostuffdb exited with status " << status << std::endl;
    
    uv_close((uv_handle_t*) req, NULL);
}

static void on_alloc(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf)
{
    buf->base = output + output_used;
    buf->len = OUTPUT_SIZE - output_used;
}

static void on_read(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf)
{
    if (nread > 0)
        output_used += nread;
    
    if (child_started || nread <= 0)
        return;
    
    if (0 != strncmp(buf->base, "jni rpc: ", 9))
    {
        fprintf(stdout, "%.*s", static_cast<int>(nread), buf->base);
        return;
    }
    
    child_started = true;
    start_chrome();
}

int main(int argc, char *argv[])
{
    const char* pdb_bin;
    if (!exists(pdb_bin = PDB_TRY_BIN) && !exists(pdb_bin = PDB_BIN))
    {
        std::cerr << pdb_bin << " not found." << std::endl;
        return 1;
    }
    
    std::string ts_dir;
    if (!get_ts_dir(".", &ts_dir))
    {
        std::cerr << "$app_ts not found." << std::endl;
        return 1;
    }
    
    std::string app = ts_dir.substr(0, ts_dir.length() - 3);
    root_absolute_dir = absolute_path(".") + "/";
    #ifdef _WIN32
    std::string app_dir = root_absolute_dir;
    #else
    std::string app_dir;
    #endif
    
    std::string app_jar = app + "-all/target/" + app + "-all-jarjar.jar";
    if (!exists(app_jar.c_str()))
    {
        std::cerr << app_jar << " not found." << std::endl;
        return 1;
    }
    
    std::string app_json = ts_dir + "/g/user/UserServices.json";
    if (!exists(app_json.c_str()))
    {
         std::cerr << app_json << " not found." << std::endl;
         return 1;
    }
    
    std::string port_txt;
    if (!load_file("PORT.txt", false, &port_txt))
    {
        std::cerr << "PORT.txt not found." << std::endl;
        return 1;
    }
    trim_trailing_ws(port_txt);
    
    std::string args_txt;
    if (!load_file("ARGS.txt", false, &args_txt))
    {
        std::cerr << "ARGS.txt not found." << std::endl;
        return 1;
    }
    trim_trailing_ws(args_txt);
    
    // chrome bin
    int idx = 0;
    while (idx < CHROME_BIN_COUNT && !exists(chrome_bins[idx])) idx++;
    
    if (idx == CHROME_BIN_COUNT)
    {
        std::cerr << "chrome executable not found." << std::endl;
        return 1;
    }
    chrome_idx = idx;
    
    std::vector<std::string> arg_list;
    arg_list.push_back(pdb_bin);
    
    size_t colon;
    if (std::string::npos == (colon = port_txt.find(':')))
    {
        str_port = port_txt;
        arg_list.push_back("127.0.0.1:" + port_txt);
    }
    else
    {
        str_port = port_txt.substr(colon + 1);
        arg_list.push_back(port_txt);
    }
    
    arg_list.push_back(app_dir + app_json);
    
    add_args_to(arg_list, args_txt);
    
    arg_list.push_back("-Dprotostuffdb.rt_flags=2");
    
    #ifdef _WIN32
    arg_list.push_back("-Dprotostuffdb.assets_dir=" + app_dir + "/" + ts_dir);
    #else
    arg_list.push_back("-Dprotostuffdb.assets_dir=" + ts_dir);
    #endif
    
    arg_list.push_back("-Djava.class.path=" + app_dir + app_jar);
    
    arg_list.push_back(app + ".all.Main");
    
    #ifdef _WIN32
    arg_list.push_back(app_dir);
    #endif
    
    char* args[128]; // should be enough
    size_t len = arg_list.size();
    for (size_t i = 0; i < len; i++)
        args[i] = (char*)arg_list[i].c_str();
    args[len] = NULL;
    
    loop = uv_default_loop();
    
    options.exit_cb = on_child_exit;
    options.args = args;
    #ifdef _WIN32
    std::string pdb_bin_abs = root_absolute_dir + pdb_bin;
    std::string cwd_abs = root_absolute_dir + "target\\jre\\bin\\server";
    options.cwd = cwd_abs.c_str();
    options.file = pdb_bin_abs.c_str();
    #else
    options.file = pdb_bin;
    #endif
    
    uv_pipe_init(loop, &child_out, 0);
    
    uv_stdio_container_t child_stdio[3];
    options.stdio = child_stdio;
    options.stdio_count = 3;
    
    child_stdio[0].flags = UV_IGNORE;
    child_stdio[1].flags = static_cast<uv_stdio_flags>(UV_CREATE_PIPE | UV_WRITABLE_PIPE);
    child_stdio[1].data.stream = (uv_stream_t *) &child_out;
    child_stdio[2].flags = UV_INHERIT_FD;
    child_stdio[2].data.fd = 2;
    
    int r;
    if ((r = uv_spawn(loop, &child_req, &options)))
    {
        fprintf(stderr, "protostuffdb spawn failed: %s\n", uv_strerror(r));
        return 1;
    }
    
    if ((r = uv_read_start((uv_stream_t*)&child_out, on_alloc, on_read)))
    {
        fprintf(stderr, "%s\n", uv_strerror(r));
        return 1;
    }
    //fprintf(stderr, "Launched process with ID %d\n", child_req.pid);
    
    return uv_run(loop, UV_RUN_DEFAULT);
}
