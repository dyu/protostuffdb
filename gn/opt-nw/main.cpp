#include <cstdlib>
#ifdef _WIN32

#include <string>
extern "C" {
#include <sys/stat.h>
#include <tchar.h>
#include <windows.h>
#pragma comment(lib, "user32.lib")
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
}

static bool exists(const char *path)
{
    struct stat b;
    return 0 == stat(path, &b);
}

static std::string absolute_path(const std::string &filepath) {
    char abs_path[MAX_PATH];
    return GetFullPathNameA(filepath.c_str(), MAX_PATH, abs_path, nullptr) ? abs_path : filepath;
}

static LRESULT CALLBACK wnd_proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }
}

static bool show_window(MSG *msg)
{
    HINSTANCE hInstance = GetModuleHandle(NULL);
    WNDCLASS wc = {0};
    wc.lpfnWndProc = wnd_proc;
    wc.hInstance = hInstance;
    wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
    wc.lpszClassName = "nw_wrapper";
    if (!RegisterClass(&wc))
        return false;

    HWND w = CreateWindow(wc.lpszClassName,
            "",
            WS_MINIMIZE|WS_POPUP,
            0,0,1,1,NULL,NULL,hInstance,NULL);
    
    if (!w)
        return false;
    
    ShowWindow(w, SW_MINIMIZE);
    if (!exists("icon.ico"))
        return true;
    
    std::string icon_path = absolute_path(".") + "\\icon.ico";
    HANDLE icon = LoadImage(hInstance, icon_path.c_str(), IMAGE_ICON, 256, 256, LR_LOADFROMFILE);
    SendMessage(w, WM_SETICON, ICON_BIG, (LPARAM)icon);
    if (GetMessage(msg, NULL, 0, 0))
        DispatchMessage(msg);
    return true;
}

static int windows_system(const char *cmd)
{
    PROCESS_INFORMATION p_info;
    STARTUPINFO s_info;
    LPSTR cmdline, programpath;

    memset(&s_info, 0, sizeof(s_info));
    memset(&p_info, 0, sizeof(p_info));
    s_info.cb = sizeof(s_info);
    s_info.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
    s_info.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
    s_info.hStdOutput =  GetStdHandle(STD_OUTPUT_HANDLE);
    s_info.hStdError = GetStdHandle(STD_ERROR_HANDLE);
    s_info.wShowWindow = SW_MINIMIZE; //SW_HIDE;

    cmdline     = _tcsdup(TEXT(cmd));
    programpath = _tcsdup(TEXT(cmd));

    if (!CreateProcess(programpath, cmdline, NULL, NULL, false, 0/*CREATE_NO_WINDOW*/, NULL, NULL, &s_info, &p_info))
        return 1;

    MSG msg = {0};
    if (!show_window(&msg))
    {
        WaitForSingleObject(p_info.hProcess, INFINITE);
        CloseHandle(p_info.hProcess);
        CloseHandle(p_info.hThread);
        return 0;
    }

    bool loop = true;
    while (loop && WAIT_TIMEOUT == WaitForSingleObject(p_info.hProcess, 2000))
    {
        while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
        {
            if (!GetMessage(&msg, NULL, 0, 0) && loop)
            {
                loop = false;
                TerminateProcess(p_info.hProcess, 0);
            }
            DispatchMessage(&msg);
        }
    }

    CloseHandle(p_info.hProcess);
    CloseHandle(p_info.hThread);

    return 0;
}
#endif

int main(int argc, char *argv[])
{
    #ifdef _WIN32
    //HWND handleWindow;
    //AllocConsole();
    //handleWindow = FindWindowA("ConsoleWindowClass", NULL);
    //ShowWindow(handleWindow, SW_MINIMIZE);
    return windows_system("C:\\opt\\nw\\nw.exe .");
    //return WinExec("C:\\opt\\nw\\nw.exe .", SW_MINIMIZE);
    #else
    return std::system("/opt/nw/nw .");
    #endif
}