#include <gtest/gtest.h>
#include <db/log_reader.h>
#include <db/log_writer.h>

#if defined(HYPER)
#include <hyperleveldb/env.h>
#else
#include <leveldb/env.h>
#endif

TEST(log, write_read)
{
    auto env = leveldb::Env::Default();
    std::string path = "target/bar.log";
    leveldb::Status s;
    if (!env->FileExists("target"))
        ASSERT_TRUE(env->CreateDir("target").ok());
    
    uint64_t file_size = 0;
    if (env->FileExists(path))
        ASSERT_TRUE(env->GetFileSize(path, &file_size).ok());
    
    // open file for writes
    leveldb::WritableFile* wf;
    #if defined(HYPER)
    s = env->NewAppendableFile(path, &wf);
    #else
    s = env->NewSharedAppendableFile(path, &wf);
    #endif
    ASSERT_TRUE(s.ok());
    
    std::cout << "filesize: " << file_size << std::endl;
    
    // open file for reads
    leveldb::SequentialFile* sf;
    #if defined(HYPER)
    s = env->NewSequentialFile(path, &sf);
    #else
    s = env->NewSharedSequentialFile(path, &sf);
    #endif
    ASSERT_TRUE(s.ok());
    
    // write
    leveldb::log::SimpleWriter writer(wf, file_size);
    s = writer.AddRecord(leveldb::Slice("bar"));
    ASSERT_TRUE(s.ok());
    
    std::string scratch;
    leveldb::Slice record;
    
    // read
    leveldb::log::Reader reader(sf, nullptr, false, 0);
    int count = 0;
    while (reader.ReadRecord(&record, &scratch))
    {
        count++;
        std::cout << std::string(record.data_, record.size_) << std::endl;
    }
    
    if (count != 0)
    {
        reader.Reset(0);
        ASSERT_TRUE(reader.ReadRecord(&record, &scratch));
        ASSERT_TRUE(3 == record.size_);
        ASSERT_TRUE(0 == memcmp("bar", record.data_, record.size_));
    }
}


