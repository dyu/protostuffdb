#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <condition_variable>

#include <jni.h>
#include <protostuff/jni_rpc.h>
#include <protostuff/protostuff.h>
#include <protostuff/lsmdb.h>

#if defined(REP_MASTER) || defined(REP_SLAVE) || defined(SYNC_NODE)
#if defined(HYPER)
#include <hyperleveldb/write_batch.h>
#else
#include <leveldb/write_batch.h>
#endif
#endif

extern "C" {
#include <openssl/evp.h>
#include <openssl/md5.h>

#if !defined(_WIN32)
#include <dirent.h>
#include <sys/stat.h>
#elif defined(SP)
#pragma comment(lib, "user32.lib")
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif
}

#include <uWS.h>

#include <json.hpp>

#ifdef BENCH
#include <chrono>
#if defined(HYPER)
#include <hyperleveldb/env.h>
#else
#include <leveldb/env.h>
#endif
#endif

#define B32_DECODE_SIZE 512
#define B32_OOB_SIZE 256

#if defined(REP_MASTER) || defined(REP_SLAVE) || defined(SYNC_NODE)
//#include <snappy.h>
// workaround for vcbuild 2015 bug
namespace snappy {
    size_t Compress(const char* data, size_t length, std::string* out); 
    bool Uncompress(const char* data, size_t length, std::string* out);
}
#endif


using nlohmann::json;

namespace srv {
    const uint8_t
            VOID_ARG = 0x01,
            READ_ONLY = 0x02,
            AUTH_REQUIRED = 0x04,
            PUB = 0x08,
            SYNC_PUB = 0x10,
            // 0x20
            AUTH_LOCAL = 0x44,
            PUB_LAST_FIELD = 0x88;
    
    struct Entry
    {
        // previous releases expected this
        //bool va; // VOID_ARG
        //bool ro; // READ_ONLY
        //bool ar; // AUTH_REQUIRED
        
        uint8_t f; // flags
        std::string p; // params
    };
    
    void to_json(json& j, const Entry& p)
    {
        j = json{{"f", p.f}, {"p", p.p}};
    }
    
    void from_json(const json& j, Entry& p)
    {
        p.f = j["f"].get<uint8_t>();
        p.p = j["p"].get<std::string>();
    }
}

void JNICALL Java_protostuffdb_Jni_newDb(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_deleteDb(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_backupDb(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_close(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_sput(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_put(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_sget(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_iget(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_get(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_scan(JNIEnv * env, jclass clazz, jlong contextPtr);

static const char B32_ALPHABET[] = "0123456789abcdefghijklmnopqrstuv";
static char basis32[256] = { 77 };

// from set-misc-nginx-module
static bool
decode_base32_to(char* out, size_t* outlen, char* src, size_t srclen)
{
    size_t                   len, mod;
    char                    *s = src;
    char                    *d = out;

    for (len = 0; len < srclen; len++) {
        if (s[len] == '=') {
            break;
        }

        if (basis32[s[len]] == 77) {
            return false;
        }
    }

    mod = len % 8;

    if (mod == 1 || mod == 3 || mod == 6) {
        /* bad Base32 digest length */
        return false;
    }

    while (len > 7) {
        *d++ = (basis32[s[0]] << 3) | ((basis32[s[1]] >> 2) & 0x07);

        *d++ = ((basis32[s[1]] & 0x03) << 6) | (basis32[s[2]] << 1) |
            ((basis32[s[3]] >> 4) & 1);

        *d++ = ((basis32[s[3]] & 0x0f) << 4) | ((basis32[s[4]] >> 1) & 0x0f);

        *d++ = ((basis32[s[4]] & 1) << 7) | ((basis32[s[5]] & 0x1f) << 2) |
            ((basis32[s[6]] >> 3) & 0x03);
        *d++ = ((basis32[s[6]] & 0x07) << 5) | (basis32[s[7]] & 0x1f);

        s += 8;
        len -= 8;
    }

    if (len) {
        /* 2 bytes left */
        *d++ = (basis32[s[0]] << 3) | ((basis32[s[1]] >> 2) & 0x07);

        if (len > 2) {
            /* 4 bytes left */
            *d++ = ((basis32[s[1]] & 0x03) << 6) | ((basis32[s[2]] & 0x1f) << 1)
                | ((basis32[s[3]] >> 4) & 1);

            if (len > 4) {
                /* 5 bytes left */
                *d++ = ((basis32[s[3]] & 0x0f) << 4) |
                    ((basis32[s[4]] >> 1) & 0x0f);

                if (len > 5) {
                    /* 7 bytes left */
                    *d++ = ((basis32[s[4]] & 1) << 7) |
                        ((basis32[s[5]] & 0x1f) << 2) |
                        ((basis32[s[6]] >> 3) & 0x03);
                }
            }
        }
    }
    
    *outlen = (d - out);
    return true;
}

const std::string dot_jar = ".jar";

// encrypted session
const std::string pfx_es = "-Des.";
const std::string pfx_protostuffdb = "-Dprotostuffdb.";
#ifdef BENCH
const std::string pfx_bench = "-Dbench.";
#endif

inline bool ends_with(std::string const & value, std::string const & suffix)
{
    return suffix.size() <= value.size() && std::equal(suffix.rbegin(), suffix.rend(), value.rbegin());
}

inline bool starts_with(std::string const & value, std::string const & prefix)
{
    return prefix.size() <= value.size() && std::equal(prefix.begin(), prefix.end(), value.begin());
}

inline void
write16(char* buf, uint16_t value)
{
    *(uint16_t*)(buf) = value;
}

inline uint16_t read16(char* buf)
{
    return *(uint16_t*)(buf);
}

inline void
write32(char* buf, uint32_t value)
{
    *(uint32_t*)(buf) = value;
}

inline uint32_t read32(char* buf)
{
    return *(uint32_t*)(buf);
}

static char* write(char* buf, 
        const char* header, size_t headerSize, 
        const char* payload, size_t payloadSize)
{
    write16(buf, headerSize);
    buf += 2;
    write16(buf, payloadSize);
    buf += 2;

    memcpy(buf, header, headerSize);
    buf += headerSize;
    
    if (payloadSize != 0)
    {
        memcpy(buf, payload, payloadSize);
        buf += payloadSize;
    }

    return buf;
}

static char* writeWithToken(char* buf, const bool b64, const char* token, size_t tokenSize, 
        const char* header, size_t headerSize, 
        const char* payload, size_t payloadSize)
{
    // ',7::'
    write16(buf, headerSize + 4 + tokenSize);
    buf += 2;
    write16(buf, payloadSize);
    buf += 2;

    memcpy(buf, header, headerSize);
    buf += headerSize;
    
    // token
    *(buf++) = ',';
    *(buf++) = '7';
    *(buf++) = b64 ? '.' : ':';
    *(buf++) = ':';
    memcpy(buf, token, tokenSize);
    buf += tokenSize;
    
    if (payloadSize != 0)
    {
        memcpy(buf, payload, payloadSize);
        buf += payloadSize;
    }

    return buf;
}

static char* writeNew(const char* header, size_t headerSize, 
        const char* payload, size_t payloadSize, size_t remaining,
        uint8_t flags)
{
    size_t len = 1 + 4 + headerSize + payloadSize + remaining;
    char* bufNew = new char[len];
    char* buf = bufNew + 1;
    
    *bufNew = flags;
    
    write16(buf, headerSize);
    buf += 2;
    write16(buf, payloadSize);
    buf += 2;

    memcpy(buf, header, headerSize);
    buf += headerSize;
    
    if (payloadSize != 0)
        memcpy(buf, payload, payloadSize);

    return bufNew;
}

static char* writeNewWithToken(const bool b64, const char* token, size_t tokenSize, 
        const char* header, size_t headerSize, 
        const char* payload, size_t payloadSize,
        size_t remaining, uint8_t flags)
{
    size_t len = 1 + 4 + headerSize + 4 + tokenSize + payloadSize + remaining;
    char* bufNew = new char[len];
    char* buf = bufNew + 1;
    
    *bufNew = flags;
    
    write16(buf, headerSize + 4 + tokenSize);
    buf += 2;
    write16(buf, payloadSize);
    buf += 2;

    memcpy(buf, header, headerSize);
    buf += headerSize;
    
    // token
    *(buf++) = ',';
    *(buf++) = '7';
    *(buf++) = b64 ? '.' : ':';
    *(buf++) = ':';
    memcpy(buf, token, tokenSize);
    buf += tokenSize;
    
    if (payloadSize != 0)
        memcpy(buf, payload, payloadSize);

    return bufNew;
}

static bool ex(JNIEnv *jvm_env)
{
    bool occured = jvm_env->ExceptionOccurred();
    if (occured)
    {
        jvm_env->ExceptionDescribe();
        jvm_env->ExceptionClear();
    }
    return occured;
}

static bool check(bool condition, JNIEnv *jvm_env) {
    if (condition && jvm_env->ExceptionOccurred()) {
        jvm_env->ExceptionDescribe();
        jvm_env->ExceptionClear();
    }

    return condition;
}

static void usage(int argc, char *argv[])
{
    std::cerr << "Usage:\n"
        << argv[0]
        << " (ip:)port /path/to/service.json -Djava.class.path=/path/to/app.jar com.example.Main\n" << std::endl;
}

static void serve_http(void* ptr, JniContext* jni);

typedef void* (*ThreadFn)(void*, JniContext*);

// runtime flags
const uint8_t RT_ASSETS_SERVE_DEFAULT = 1; // serve index.html from assets_dir when uri not found
const uint8_t RT_LOCAL_APP = 2;
const uint8_t RT_PKV_SYNC = 4;
const uint8_t RT_WAIT_FOR_STDIN_EOF = 8;

/*static void print_bytes(std::ostream& out, const char *title, const unsigned char *data, size_t dataLen, bool format = true) {
    out << title << std::endl;
    out << std::setfill('0');
    for(size_t i = 0; i < dataLen; ++i) {
        out << std::hex << std::setw(2) << (int)data[i];
        if (format) {
            out << (((i + 1) % 16 == 0) ? "\n" : " ");
        }
    }
    out << std::endl;
}*/

class MyApplication
{
public:
    const char* bind_ip;
    const int port;
    const std::map<std::string, srv::Entry> entries;
    
    int writers;
    int readers;
    int bgworkers;
    int byte_array_offset;
    int main_class_offset;
    JNIEnv *jvm_env;
    JavaVM *jvm;
    jclass class_;
    jmethodID createThread_;
    
    // jvm args
    std::vector<std::string> other_args;
    
    // hub
    uWS::Hub* hub;
    
    // serve static files
    std::string assets_dir;
    bool assets_preloaded = false; // assets mapped to memory before the http server starts
    
    uint8_t rt_flags = 0; // runtime flags
    
    #if defined(REP_MASTER) || defined(SYNC_NODE)
    uWS::Group<uWS::SERVER>* pkvSyncGroup = nullptr;
    #endif
    
    #ifdef REP_MASTER
    int timer_interval = 500;
    uint8_t master_batch_threshold_mb = 1; // default to 1 mb batch size
    bool master_batch_multi_frame = false;
    //uWS::Group<uWS::SERVER>* repSlaveGroup;
    //uint8_t master_flags = 0;
    uint8_t master_update_ticks = 0;
    uint8_t master_update_interval = 4;
    #endif
    
    #ifdef REP_SLAVE
    int timer_interval = 500;
    uint8_t slave_batch_threshold_mb = 0xFF; // prefer server config
    uWS::WebSocket<uWS::CLIENT> *slave_client_ws = nullptr;
    std::string slave_connect_url;
    std::string slave_master;
    bool slave_connected = false;
    //uint8_t slave_flags = 0;
    uint8_t slave_ack_ticks = 0;
    uint8_t slave_ack_interval = 2;
    uint8_t slave_idle_refresh_ticks = 0;
    uint8_t slave_idle_refresh_interval = 0; // disabled by default
    bool slave_refresh = false;
    #endif
    
    #ifdef BENCH
    int warmups = 0;
    int iterations = 10000000; // 10M by default
    std::string uri;
    std::string payload;
    #endif
    
    // websockets
    bool pubsub = false;
    int auto_ping = 0;
    
    // auth
    bool token_as_user = false;
    
    // encryption
    const char* encrypted_session_key = nullptr;
    int encrypted_session_key_len = 0;

    const char* encrypted_session_iv = nullptr;
    int encrypted_session_iv_len = 0;

    int64_t encrypted_session_expires = 0;
    
    int write_buffer_mb = 0;
    int keepalive_timeout_start = 1000;
    int keepalive_timeout_interval = 5000;
    
    MyApplication(const char* _bind_ip, const int _port, const std::map<std::string, srv::Entry> _entries):
        bind_ip(_bind_ip),
        port(_port),
        entries(_entries) {}
    
    int run(int argc, char *argv[], int start);
    bool serve();
    void cleanup(bool success);
    void destroy_jvm();
    bool init_jvm();
    bool init_jni(const char* lookupClass);
};

int MyApplication::run(int argc, char *argv[], int start)
{
    cleanup(init_jvm());
    return 0;
}

void MyApplication::cleanup(bool success){
    if (!success)
        exit(1);
}

void MyApplication::destroy_jvm() {
    jclass systemClass = jvm_env->FindClass("java/lang/System");
    jmethodID exitMethod = jvm_env->GetStaticMethodID(systemClass, "exit", "(I)V");
    jvm_env->CallStaticVoidMethod(systemClass, exitMethod, 0);
    jvm->DestroyJavaVM();
}

bool MyApplication::init_jvm() {
    const size_t size = other_args.size();
    size_t len = size, offset = size, start = 0;
    JavaVMInitArgs vm_args;
    JavaVMOption *options;
    JNIEnv *env;
    
    while (offset-- > 0 && !ends_with(other_args[offset], dot_jar))
        len--;

    if (len == size || len == 0) {
        fprintf(stderr, "Required jvm options: -Djava.class.path=/path/to/app.jar com.example.Main\n");
        return false;
    }

    main_class_offset = ++offset;
    
    std::string jniClass;
    if (other_args[start] != "-jni" || other_args[++start][0] == '-') {
        // default class to lookup
        jniClass += "protostuffdb/Jni";
    } else {
        jniClass += other_args[start++];
        std::replace(jniClass.begin(), jniClass.end(), '.', '/');
    }

    len = offset - start;
    if (NULL == (options = static_cast<JavaVMOption*>(malloc(len * sizeof(JavaVMOption))))) {
        return false;
    }

    for (size_t i = 0; i < len; i++) {
        options[i].extraInfo = NULL;
        options[i].optionString = const_cast<char*>(other_args[start++].c_str());
    }

    vm_args.version = JNI_VERSION_1_6;
    vm_args.ignoreUnrecognized = JNI_TRUE;
    vm_args.options = options;
    vm_args.nOptions = len;
    vm_args.ignoreUnrecognized = false;
    
    if (JNI_CreateJavaVM(&jvm, (void **)&env, &vm_args) < 0) {
        free(options);
        fprintf(stderr, "Could not create java vm.\n");
        return false;
    }

    free(options);
    jvm_env = static_cast<JNIEnv*>(env);

    if (!init_jni(jniClass.c_str())) {
        fprintf(stderr, "Could not initialize jni env for %s\n", jniClass.c_str());
        destroy_jvm();
        return false;
    }
    
    auto mcString = other_args[offset++];
    std::replace(mcString.begin(), mcString.end(), '.', '/');
    auto mc = mcString.c_str();

    jclass mainClass = jvm_env->FindClass(mc);
    if (check(mainClass == NULL, jvm_env)) {
        fprintf(stderr, "Could not find main class: %s\n", mc);
        destroy_jvm();
        return false;
    }

    jmethodID mainMethod = jvm_env->GetStaticMethodID(mainClass, "main", "([Ljava/lang/String;)V");
    if (check(mainMethod == NULL, jvm_env)) {
        fprintf(stderr, "%s does not have the method: public static void main(String[] args)\n", mc);
        destroy_jvm();
        return false;
    }

    len = size - offset;
    jobjectArray arr = jvm_env->NewObjectArray(len, jvm_env->FindClass("java/lang/String"), NULL);
    if (check(arr == NULL, jvm_env)) {
        fprintf(stderr, "Could not create args for %s.main\n", mc);
        destroy_jvm();
        return false;
    }
    
    for (size_t i = 0; i < len; i++)
        jvm_env->SetObjectArrayElement(arr, i, jvm_env->NewStringUTF(other_args[offset++].c_str()));
    
    jvm_env->CallStaticVoidMethod(mainClass, mainMethod, arr);
    bool ok = !ex(jvm_env);

    jvm_env->DeleteLocalRef(arr);
    destroy_jvm();
    return ok;
}

bool MyApplication::serve() {
    intptr_t pFn = (intptr_t)&serve_http;
    
    jvm_env->CallStaticVoidMethod(class_, createThread_, 
            (jlong)pFn, 0, 0, 0);
    
    if (rt_flags & RT_WAIT_FOR_STDIN_EOF)
    {
        while (EOF != getchar());
    }
    else
    {
        std::condition_variable cv;
        std::mutex m;
        std::unique_lock<std::mutex> lock(m);
        cv.wait(lock, []{return false;});
    }
    
    return true;
}

static MyApplication* instance;

void setupThread(JniContext* jni, int type, int id)
{
    jni->type = type;
    jni->id = id;
    jmethodID m = jni->env->GetStaticMethodID(jni->class_, "setupThread", "(IIJ)V");
    jni->env->CallStaticVoidMethod(jni->class_, m, (jint)type, (jint)id, (jlong)(intptr_t)jni);
}

static void threadCreated(void* ptr, JniContext* jni)
{
    setupThread(jni, 1, (int)(intptr_t)ptr);
}

#if defined(REP_MASTER) || defined(REP_SLAVE)
static void serve_http_readonly(void* ptr, JniContext* jni);
#endif

//extern "C" {

// public static native boolean init(int writers, int readers);
/*
 * Class:     protostuffdb_Jni
 * Method:    init
 * Signature: (IIII)Z
 */
//JNIEXPORT
jboolean JNICALL Java_protostuffdb_Jni_init(JNIEnv * env, jclass clazz,
        jint writers, jint readers, jint bgworkers, jint byte_array_offset) {
    instance->writers = writers;
    instance->readers = readers;
    instance->bgworkers = bgworkers;
    instance->byte_array_offset = byte_array_offset;
    instance->class_ = clazz;
    instance->createThread_ = env->GetStaticMethodID(clazz, "createThread", "(JJII)V");
    
    intptr_t pFn = (intptr_t)&threadCreated;
    
    for (int i = 0, len = bgworkers, bgstart = writers + readers; i < len; i++, bgstart++)
    {
        // create bgworkers
        instance->jvm_env->CallStaticVoidMethod(clazz, instance->createThread_, 
                (jlong)pFn, (jlong)bgstart, 1, bgstart);
    }
    
    #if defined(REP_MASTER) || defined(REP_SLAVE)
    intptr_t roFn = (intptr_t)&serve_http_readonly;
    for (int i = 0; i < readers; i++)
    {
        instance->jvm_env->CallStaticVoidMethod(clazz, instance->createThread_, 
                (jlong)roFn, (jlong)(i + 1), 1, i + 1);
    }
    #endif
    
    return instance->serve();
}

// public static native boolean initThread(long ptrFn, long ptrArg, byte[] data);
/*
 * Class:     protostuffdb_Jni
 * Method:    initThread
 * Signature: (JJ[B[B[B)Z
 */
//JNIEXPORT
jboolean JNICALL Java_protostuffdb_Jni_initThread(JNIEnv * env, jclass clazz, jlong ptrFn, jlong ptrArg, 
        jbyteArray jbufRpc, jbyteArray jbufDb, jbyteArray jbufTmp)
{
    JniContext jni;
    intptr_t pFn = ptrFn,
            pArg = ptrArg;
    
    char* bufRpc = ((char*)(*(intptr_t*)jbufRpc) + instance->byte_array_offset);
    char* bufDb = ((char*)(*(intptr_t*)jbufDb) + instance->byte_array_offset);
    char* bufTmp = ((char*)(*(intptr_t*)jbufTmp) + instance->byte_array_offset);

    jni.env = env;
    jni.class_ = clazz;
    jni.handle_ = env->GetStaticMethodID(clazz, "handle", "(II)V");
    jni.buf = bufRpc;
    jni.bufDb = bufDb;
    jni.bufTmp = bufTmp;
    
    ThreadFn fn = (ThreadFn)pFn;
    (*fn)(reinterpret_cast<void*>(pArg), &jni);
    
    return true;
}

static uint64_t
convert_ntohll(uint64_t n)
{
#ifdef ntohll
    return ntohll(n);
#else
    return ((uint64_t) ntohl((unsigned long) n) << 32)
           + ntohl((unsigned long) (n >> 32));
#endif
}

static uint64_t
convert_htonll(uint64_t n)
{
#ifdef htonll
    return htonll(n);
#else
    return ((uint64_t) htonl((unsigned long) n) << 32)
           + htonl((unsigned long) (n >> 32));
#endif
}

// encrypt/decrypt
static bool
encrypt(const u_char *in, size_t in_len,
        size_t* out_len, size_t* out_offset) {
    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_CIPHER_CTX           ctx;
    #else
    EVP_CIPHER_CTX*          ctx;
    #endif
    const EVP_CIPHER        *cipher;
    u_char                  *p, *data;
    const u_char            *key, *iv;
    int                      ret, key_len;//, iv_len;
    size_t                   block_size, buf_size, data_size;
    int                      len;
    uint64_t                 expires_time;
    time_t                   now;
    
    size_t dst_len;
    const u_char *dst;
    int64_t expires;

    key_len = instance->encrypted_session_key_len;
    if (key_len == 0) {
        return false;
    }
    key = (const u_char*)instance->encrypted_session_key;

    //iv_len = instance->encrypted_session_iv_len;
    iv = (const u_char*)instance->encrypted_session_iv;

    expires = instance->encrypted_session_expires;

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_CIPHER_CTX_init(&ctx);
    #else
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL)
        return false;
    #endif

    cipher = EVP_aes_256_cbc();

    block_size = EVP_CIPHER_block_size(cipher);

    data_size = in_len + sizeof(expires_time);

    buf_size = MD5_DIGEST_LENGTH /* for the digest */
               + (data_size + block_size - 1) /* for EVP_EncryptUpdate */
               + block_size; /* for EVP_EncryptFinal */
    
    data = (u_char*)in;

    if (expires == 0) {
        expires_time = 0;
    } else if (-1 != (now = time(NULL))) {
        expires_time = (uint64_t) now + (uint64_t) expires;
    } else {
        #if OPENSSL_VERSION_NUMBER < 0x10100000L
        EVP_CIPHER_CTX_cleanup(&ctx);
        #else
        //EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        #endif
        return false;
    }

    //dd("expires before encryption: %lld", (long long) expires_time);

    expires_time = convert_htonll(expires_time);

    memcpy(data + in_len, (u_char *) &expires_time, sizeof(expires_time));

    // no need to allocate a tmp buf as the buf is 0xFFFF bytes long
    p = data + data_size;
    dst = p;

    MD5(data, data_size, p);

    p += MD5_DIGEST_LENGTH;
    
    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    ret = EVP_EncryptInit(&ctx, cipher, key, iv);
    #else
    ret = EVP_EncryptInit(ctx, cipher, key, iv);
    #endif
    if (!ret) {
        #if OPENSSL_VERSION_NUMBER < 0x10100000L
        EVP_CIPHER_CTX_cleanup(&ctx);
        #else
        //EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        #endif
        return false;
    }

    /* encrypt the raw input data */

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    ret = EVP_EncryptUpdate(&ctx, p, &len, data, data_size);
    #else
    ret = EVP_EncryptUpdate(ctx, p, &len, data, data_size);
    #endif
    if (!ret) {
        #if OPENSSL_VERSION_NUMBER < 0x10100000L
        EVP_CIPHER_CTX_cleanup(&ctx);
        #else
        //EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        #endif
        return false;
    }

    p += len;

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    ret = EVP_EncryptFinal(&ctx, p, &len);
    EVP_CIPHER_CTX_cleanup(&ctx);
    #else
    ret = EVP_EncryptFinal_ex(ctx, p, &len);
    //EVP_CIPHER_CTX_reset(ctx);
    EVP_CIPHER_CTX_free(ctx);
    #endif

    if (!ret) {
        return false;
    }

    p += len;

    dst_len = p - dst;

    if (dst_len > buf_size) {
        //ngx_log_error(NGX_LOG_ERR, log, 0,
        //              "encrypted_session: aes_mac_encrypt: buffer error");

        return false;
    }
    
    *out_len = dst_len;
    *out_offset = data_size;
    return true;
}

static bool
decrypt(const u_char* in, size_t in_len, 
        size_t* out_len, size_t* out_offset) {
    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_CIPHER_CTX           ctx;
    #else
    EVP_CIPHER_CTX*          ctx;
    #endif
    const EVP_CIPHER        *cipher;
    int                      ret;
    size_t                   block_size, buf_size, key_len;//, iv_len;
    int                      len;
    u_char                  *p;
    const u_char            *digest, *key, *iv;
    uint64_t                 expires_time;
    time_t                   now;

    u_char new_digest[MD5_DIGEST_LENGTH];
    
    size_t dst_len;
    const u_char *dst;

    key_len = instance->encrypted_session_key_len;
    if (key_len == 0 || in_len < MD5_DIGEST_LENGTH) {
        return false;
    }
    key = (const u_char*)instance->encrypted_session_key;

    //iv_len = instance->encrypted_session_iv_len;
    iv = (const u_char*)instance->encrypted_session_iv;

    //expires = instance->encrypted_session_expires;

    digest = in;

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_CIPHER_CTX_init(&ctx);
    #else
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL)
        return false;
    #endif

    cipher = EVP_aes_256_cbc();

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    ret = EVP_DecryptInit(&ctx, cipher, key, iv);
    #else
    ret = EVP_DecryptInit(ctx, cipher, key, iv);
    #endif
    if (!ret) {
        #if OPENSSL_VERSION_NUMBER < 0x10100000L
        EVP_CIPHER_CTX_cleanup(&ctx);
        #else
        //EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        #endif
        
        return false;
    }

    block_size = EVP_CIPHER_block_size(cipher);

    buf_size = in_len + block_size /* for EVP_DecryptUpdate */
               + block_size; /* for EVP_DecryptFinal */

    p = (u_char*)(in + in_len);

    dst = p;

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    ret = EVP_DecryptUpdate(&ctx, p, &len, in + MD5_DIGEST_LENGTH,
                            in_len - MD5_DIGEST_LENGTH);
    #else
    ret = EVP_DecryptUpdate(ctx, p, &len, in + MD5_DIGEST_LENGTH,
                            in_len - MD5_DIGEST_LENGTH);
    #endif

    if (!ret) {
        //dd("decrypt update failed");
        #if OPENSSL_VERSION_NUMBER < 0x10100000L
        EVP_CIPHER_CTX_cleanup(&ctx);
        #else
        //EVP_CIPHER_CTX_reset(ctx);
        EVP_CIPHER_CTX_free(ctx);
        #endif
        
        return false;
    }

    p += len;

    #if OPENSSL_VERSION_NUMBER < 0x10100000L
    ret = EVP_DecryptFinal(&ctx, p, &len);
    EVP_CIPHER_CTX_cleanup(&ctx);
    #else
    ret = EVP_DecryptFinal_ex(ctx, p, &len);
    //EVP_CIPHER_CTX_reset(ctx);
    EVP_CIPHER_CTX_free(ctx);
    #endif

    if (!ret) {
        //ngx_log_debug0(NGX_LOG_DEBUG_HTTP, log, 0,
        //               "failed to decrypt session: bad AES-256 digest");
        return false;
    }

    p += len;

    dst_len = p - dst;

    if (dst_len > buf_size) {
        //ngx_log_error(NGX_LOG_ERR, log, 0,
        //              "encrypted_session: aes_mac_decrypt: buffer error");
        return false;
    }

    if (dst_len < sizeof(expires_time)) {
        return false;
    }

    MD5(dst, dst_len, new_digest);

    if (strncmp((const char*)digest, (const char*)new_digest, MD5_DIGEST_LENGTH) != 0) {
        //ngx_log_debug0(NGX_LOG_DEBUG_HTTP, log, 0,
        //               "failed to decrypt session: MD5 checksum mismatch");
        return false;
    }

    dst_len -= sizeof(expires_time);

    //dd("dst len: %d", (int) *dst_len);
    //dd("dst: %.*s", (int) *dst_len, *dst);

    p -= sizeof(expires_time);

    expires_time = convert_ntohll(*((uint64_t *) p));

    if (expires_time && (
        (-1 == (now = time(NULL))) || expires_time <= (uint64_t)now
    )) {
        //ngx_log_debug2(NGX_LOG_DEBUG_HTTP, log, 0,
        //               "encrypted_session: session expired: %uL <= %T",
        //               expires_time, now);
        return false;
    }
    
    *out_len = dst_len;
    *out_offset = in_len;
    //dd("decrypted successfully");
    return true;
}

void JNICALL Java_protostuffdb_Jni_encrypt(JNIEnv * env, jclass clazz, jlong contextPtr) {
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* buf = context->bufTmp;
    const u_char *in = (const u_char*)(buf + 2);
    auto in_len = read16(buf);
    size_t out_len, out_offset;
    if (encrypt(in, in_len, &out_len, &out_offset))
    {
        write16(buf, out_len);
        write16(buf + 2, 2 + out_offset);
    }
    else
    {
        write16(buf, 0);
    }
}

void JNICALL Java_protostuffdb_Jni_decrypt(JNIEnv * env, jclass clazz, jlong contextPtr) {
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* buf = context->bufTmp;
    const u_char *in = (const u_char*)(buf + 2);
    auto in_len = read16(buf);
    size_t out_len, out_offset;
    if (decrypt(in, in_len, &out_len, &out_offset))
    {
        write16(buf, out_len);
        write16(buf + 2, 2 + out_offset);
    }
    else
    {
        write16(buf, 0);
    }
}

static size_t sign_hmacsha(const char* key, size_t key_len,
        char* in, size_t in_len, 
        char* out)
{
    const EVP_MD *alg = EVP_sha256();
    
    unsigned int len;
    
    HMAC(alg, key, key_len, (const unsigned char *)in, in_len, (unsigned char *)out, &len);
    
    return len;
}

static bool verify_hmacsha(const char* key, size_t key_len,
        char* in, size_t in_len,
        char* sig, size_t sig_len,
        char* out)
{
    return key != nullptr && sig_len == sign_hmacsha(key, key_len, in, in_len, out) && 0 == memcmp(out, sig, sig_len);
}

static bool verify_token(char* decode_buf, size_t decode_len, 
        bool* ar, uint8_t flags, 
        char *body, size_t body_len, size_t remainingBytes,
        size_t* out_len, size_t* out_offset)
{
    if (srv::AUTH_LOCAL != (srv::AUTH_LOCAL & flags))
        return decrypt((const u_char*)decode_buf, decode_len, out_len, out_offset);
    
    if (0 == body_len || 0 != remainingBytes || !verify_hmacsha(
            instance->encrypted_session_iv, instance->encrypted_session_iv_len, 
            body, body_len, 
            decode_buf, decode_len,
            decode_buf + decode_len))
    {
        return false;
    }
    
    *ar = false;
    return true;
}

//} // extern C

/*void createThread(ThreadFn ptrFn, void* ptrArg, int type, int id) {
    intptr_t pFn = (intptr_t)ptrFn,
        pArg = (intptr_t)ptrArg;
    instance->jvm_env->CallStaticVoidMethod(instance->class_, instance->createThread_, 
            (jlong)pFn, (jlong)pArg, (jint)type, (jint)id);
}*/

bool MyApplication::init_jni(const char* lookupClass) {
    jclass jniClass = jvm_env->FindClass(lookupClass);
    if (check(jniClass == NULL, jvm_env))
        return false;
    
    JNINativeMethod methods[] = {
        { const_cast<char*>("init"), const_cast<char*>("(IIII)Z"), reinterpret_cast<void*>(Java_protostuffdb_Jni_init) },
        { const_cast<char*>("initThread"), const_cast<char*>("(JJ[B[B[B)Z"), reinterpret_cast<void*>(Java_protostuffdb_Jni_initThread) },
        { const_cast<char*>("encrypt"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_encrypt) },
        { const_cast<char*>("decrypt"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_decrypt) },
        { const_cast<char*>("newDb"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_newDb) },
        { const_cast<char*>("deleteDb"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_deleteDb) },
        { const_cast<char*>("backupDb"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_backupDb) },
        { const_cast<char*>("close"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_close) },
        { const_cast<char*>("sput"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_sput) },
        { const_cast<char*>("put"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_put) },
        { const_cast<char*>("sget"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_sget) },
        { const_cast<char*>("iget"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_iget) },
        { const_cast<char*>("get"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_get) },
        { const_cast<char*>("scan"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_scan) }
    };

    jvm_env->RegisterNatives(jniClass, methods, sizeof(methods) / sizeof(JNINativeMethod));
    if (ex(jvm_env))
        return false;

    instance = this;
    return true;
}

static char* rfind_char(char* buf, char c)
{
    while (c != *buf) buf--;
    
    return buf;
}

static char* rfind_dquote(char* buf)
{
    while ('"' != *buf || '\\' == *(buf - 1)) buf--;
    
    return buf;
}

// forward definition
static char* rfind_lbracket(char* buf);

static char* rfind_lsquare(char* buf)
{
    for (;;)
    {
        switch(*buf)
        {
            case '"':
                buf = rfind_dquote(buf - 1) - 1;
                continue;
            case '[':
                return buf;
            case '}': // nested message
                buf = rfind_lbracket(buf - 1) - 1;
                continue;
            default:
                buf--;
                continue;
        }
    }
}

static char* rfind_lbracket(char* buf)
{
    for (;;)
    {
        switch(*buf)
        {
            case '"':
                buf = rfind_dquote(buf - 1) - 1;
                continue;
            case ']':
                buf = rfind_lsquare(buf - 1) - 1;
                continue;
            case '{':
                return buf;
            case '}': // nested message
                buf = rfind_lbracket(buf - 1) - 1;
                continue;
            default:
                buf--;
                continue;
        }
    }
}

static char* buffer_file(const char* filename, size_t* out_len)
{
    std::ifstream ifs(filename, std::ifstream::binary);
    if (!ifs)
    {
        *out_len = 0;
        return nullptr;
    }
    
    // get pointer to associated buffer object
    std::filebuf* pbuf = ifs.rdbuf();

    // get file size using buffer's members
    size_t size = pbuf->pubseekoff (0, ifs.end, ifs.in);
    pbuf->pubseekpos (0,ifs.in);

    // allocate memory to contain file data
    char* buffer = new char[size];

    // get file data
    pbuf->sgetn (buffer, size);

    ifs.close();
    
    *out_len = size;
    return buffer;
}

struct BufAndLen { char* buf; size_t len; };
static std::map<std::string, BufAndLen> assets_map;

// originally from https://stackoverflow.com/questions/306533/how-do-i-get-a-list-of-files-in-a-directory-in-c/1932861#1932861
void map_files(const std::string &directory)
{
#ifdef _WIN32
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return;
    
    do
    {
        if ('.' == file_data.cFileName[0])
            continue;
        
        std::string path = directory + "/" + file_data.cFileName;
        // check if dir
        if (0 != (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
        {
            map_files(path);
            continue;
        }
        
        BufAndLen bl;
        bl.buf = buffer_file(path.c_str(), &bl.len);
        assets_map[path] = bl;
    }
    while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != nullptr)
    {
        std::string path = directory + "/" + ent->d_name;
        if (-1 == stat(path.c_str(), &st) || '.' == path[directory.length() + 1])
            continue;
        
        // check if dir
        if (0 != (st.st_mode & S_IFDIR))
        {
            map_files(path);
            continue;
        }
        
        BufAndLen bl;
        bl.buf = buffer_file(path.c_str(), &bl.len);
        assets_map[path] = bl;
    }
    closedir(dir);
#endif
}

static BufAndLen index_html;

static void serve_assets(uWS::HttpResponse *res, std::string path)
{
    BufAndLen bl;
    
    // normalize
    if ('/' == path.back())
        path += "index.html";
    else if (path.length() > 4 && 0 == path.compare(path.length() - 4, 4, ".htm"))
        path += 'l';
    
    // lookup cache
    auto it = assets_map.find(path);
    if (it != assets_map.end())
    {
        // found
        bl = it->second;
    }
    else if (!instance->assets_preloaded)
    {
        // lazy create
        bl.buf = buffer_file(path.c_str(), &bl.len);
        assets_map[path] = bl;
    }
    else if (RT_ASSETS_SERVE_DEFAULT == (RT_ASSETS_SERVE_DEFAULT & instance->rt_flags))
    {
        // default to /index.html
        bl = index_html;
    }
    else
    {
        bl.buf = nullptr;
        bl.len = 0;
    }
    
    res->end(bl.buf, bl.len);
}

static const char unknown_request[] = "-Unknown request.";
static const char invalid_request[] = "-Invalid request.";
static const char unauthorized[] = "-Unauthorized.";

#if defined(REP_MASTER) || defined(REP_SLAVE)
static const uint8_t REQ_REP = 0;
static const uint8_t REQ_BACKUP = 1;

static const uint8_t ERR_DB_NOT_FOUND = 1;
static const uint8_t ERR_ENTRY_ID_INVALID = 2;
static const uint8_t ERR_NEED_RESTORE_FROM_RECENT_BACKUP = 3;
#endif

#ifdef REP_SLAVE

#ifdef HYPER
static void ssr_backup(protostuff::RepEntry* rep_entry, const char* req_buf, const size_t req_buf_len);
#endif
static void send_req_to(uWS::WebSocket<uWS::CLIENT> *ws,
        const uint8_t batch_threshold_mb, const std::vector<protostuff::RepEntry*>& list);
#endif

#ifdef REP_MASTER
struct WsEntry {
    //uWS::WebSocket<uWS::SERVER>* ws;
    std::vector<protostuff::RepEntry*> subs;
};

static void send_err_to(uWS::WebSocket<uWS::SERVER> *ws, 
        const uint8_t entry_id,
        const uint8_t err);
static void send_batch_logs_to(uWS::WebSocket<uWS::SERVER> *ws,
        const protostuff::RepEntry* rep_entry, leveldb::log::Reader& reader,
        const uint64_t seq_num, const uint8_t entry_id,
        const size_t threshold_bytes,
        std::string* scratch,
        std::string* ws_batch);
static void send_logs_to(uWS::WebSocket<uWS::SERVER> *ws, 
        const protostuff::RepEntry* rep_entry, leveldb::log::Reader& reader,
        uint64_t seq_num, const uint8_t entry_id,
        const size_t threshold_bytes,
        std::string* scratch);
#ifndef REP_RT
static void send_recent_logs();
#endif
#if defined(REP_MT) && defined(REP_MT_TIMER)
static void send_recent_logs_from(protostuff::ReaderEntry& reader_entry, uint64_t updates);
#endif

// standalone ws requests
static int handle_ws_req(uWS::WebSocket<uWS::SERVER> *ws,
        const protostuff::RepEntry* rep_entry,
        const std::unordered_map<void*, protostuff::SlaveEntry>* slave_map,
        const uint8_t entry_id, char* buf);
#endif

static void broadcast(JniContext* jni, char* buf, uint32_t size, bool last_field)
{
    char* end;
    if (last_field)
    {
        // exclude the '}' at the end
        end = buf + size - 1;
        // the last character
        buf = end - 1;
        switch (*buf)
        {
            case '"': buf = rfind_dquote(buf - 1); break;
            case ']': buf = rfind_lsquare(buf - 1); break;
            case '}': buf = rfind_lbracket(buf - 1); break;
            default: buf = rfind_char(buf - 1, ':') + 1; // bool/number/null
        }
        size = end - buf;
    }
    instance->hub->getDefaultGroup<uWS::SERVER>().broadcast(buf, size, uWS::OpCode::TEXT);
    
    //#ifdef REP_MASTER
    /*intptr_t db_ptr = protostuff::readPtrDB(jni->bufDb);
    auto db = (leveldb::DB*)db_ptr;
    auto rep_entry = protostuff::RepEntry::db_map()[db];
    if (rep_entry->slave_map.empty())
        return;
    
    const char tmp = buf[size];
    buf[size] = 0xFF;
    auto prepared = uWS::WebSocket<uWS::SERVER>::prepareMessage(
            buf, size + 1, uWS::OpCode::BINARY, false);
    rep_entry->pub_msgs.push_back(prepared);
    buf[size] = tmp;
    */
    //#endif
}

static void onHttpData(JniContext* jni,
        uWS::HttpResponse *res, char *data, size_t length, size_t remainingBytes)
{
    char *buf;
    char* bufStart;
    uint16_t headerSize, prevPayloadSize;
    uint32_t responseSize;
    bool left_paren;
    uint8_t flags;
    char *bufNew = (char*)res->httpSocket->getUserData();
    if (bufNew == nullptr || !length)
        return;
    
    // the first byte is occupied by the flags
    bufStart = bufNew + 1;
    
    headerSize = read16(bufStart);
    
    prevPayloadSize = read16(bufStart + 2);
    
    if (remainingBytes)
    {
        // write to end
        memcpy(bufStart + 4 + headerSize + prevPayloadSize, data, length);
        // persist current payload size
        write16(bufStart + 2, prevPayloadSize + length);
        return;
    }
    
    buf = jni->buf;
    // copy to jni buf
    memcpy(buf, bufStart, 4 + headerSize + prevPayloadSize);
    
    // unset user data
    res->httpSocket->setUserData(nullptr);
    
    // flags
    flags = *bufNew;
    
    // delete user data
    delete[] bufNew;
    
    // write to end
    memcpy(buf + 4 + headerSize + prevPayloadSize, data, length);
    
    // write payload size
    write16(buf + 2, prevPayloadSize + length);
    
    #if defined(REP_MASTER) || defined(SYNC_NODE)
    bool sync_pub = 0 == jni->id && 0 != (srv::SYNC_PUB & flags) && nullptr != instance->pkvSyncGroup;
    if (sync_pub)
        write32(jni->bufTmp, 0);
    #endif
    
    // call rpc
    jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
    
    // response size
    responseSize = read32(buf);
    left_paren = '[' == *(buf + 4);
    
    *(buf + 3) = left_paren ? '+' : '-';
    res->end(buf + 3, responseSize + 1);
    
    // must be a successful request
    if (!left_paren || '0' != *(buf + 5))
        return;
    
    if (instance->pubsub && 0 != (srv::PUB & flags))
    {
        // exclude the wrapper array
        // 3 is for '[0,'
        // 1 is for ']'
        broadcast(jni, buf + 4 + 3, responseSize - 3 - 1, srv::PUB_LAST_FIELD == (srv::PUB_LAST_FIELD & flags));
    }
    
    #if defined(REP_MASTER) || defined(SYNC_NODE)
    if (sync_pub && 0 != (responseSize = read32(jni->bufTmp)))
        instance->pkvSyncGroup->broadcast(jni->bufTmp + 4, responseSize, uWS::OpCode::BINARY);
    #endif
}

#if defined(HYPER) && defined(SYNC_NODE)
static const char OK[] = "+[0,0]";
static const char BACKUP_URI[] = "/backup/";
static const size_t BACKUP_URI_LEN = sizeof(BACKUP_URI) -1;

static void http_backup(std::string& url, size_t start, uWS::HttpResponse *res)
{
    size_t tilde = url.find('~', start);
    if (std::string::npos == tilde || start == tilde || std::string::npos != url.find("..", tilde, 2))
    {
        res->end(unknown_request, sizeof(unknown_request) - 1);
        return;
    }
    
    std::string backup;
    backup.append(url.data() + start, tilde - start);
    
    auto it = protostuff::RepEntry::name_map().find(backup);
    if (it == protostuff::RepEntry::name_map().end())
    {
        res->end(unknown_request, sizeof(unknown_request) - 1);
        return;
    }
    
    backup.assign("live/", sizeof("live/") - 1);
    backup.append(url.data() + tilde + 1, url.size() - tilde - 1);
    
    auto status = it->second->db->LiveBackup(backup);
    if (!status.ok())
    {
        backup.clear();
        backup += '-';
        backup += status.ToString();
        res->end(backup.data(), backup.size());
    }
    else
    {
        res->end(OK, sizeof(OK) - 1);
    }
}
#endif

static void onHttpRequest(JniContext* jni, bool readonly, 
        uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t length, size_t remainingBytes)
{
    char decode_buf[B32_DECODE_SIZE];
    size_t decode_len, decode_offset;
    auto url_h = req.getUrl();
    if (url_h.valueLength == 1)
    {
        if (instance->assets_dir.length())
            res->end(index_html.buf, index_html.len);
        else
            res->end(PROTOSTUFFDB_VERSION, sizeof(PROTOSTUFFDB_VERSION) - 1);
        return;
    }
    auto qmark = static_cast<char*>(memchr(url_h.value, '?', url_h.valueLength));
    auto url = std::string(url_h.value, qmark ? (qmark - url_h.value) : url_h.valueLength);
    auto it = instance->entries.find(url);
    if (it == instance->entries.end())
    {
        #if defined(HYPER) && defined(SYNC_NODE)
        if (url.size() > BACKUP_URI_LEN && 0 == url.compare(0, BACKUP_URI_LEN, BACKUP_URI))
        {
            http_backup(url, BACKUP_URI_LEN, res);
        }
        else if (!instance->assets_dir.length())
        #else
        if (!instance->assets_dir.length())
        #endif
        {
            res->end(unknown_request, sizeof(unknown_request) - 1);
        }
        else if (url.length() >= 10 && 0 == url.compare(0, 10, "/index.htm"))
        {
            res->end(index_html.buf, index_html.len);
        }
        else
        {
            // prepend the assets_dir
            url.insert(0, instance->assets_dir);
            serve_assets(res, std::move(url));
        }
        return;
    }
    
    const auto& p = it->second.p;
    const uint8_t flags = it->second.f;
    
    if (readonly && 0 == (srv::READ_ONLY & flags))
    {
        // TODO proxy?
        res->end(invalid_request, sizeof(invalid_request) - 1);
        return;
    }
    
    bool ar = instance->token_as_user || srv::AUTH_REQUIRED == (srv::AUTH_REQUIRED & flags);
    bool b64 = true;
    
    char *buf = jni->buf;
    uWS::Header header;
    if (!ar)
    {
        // no need to retrieve token from header
    }
    else if (0 == (RT_LOCAL_APP & instance->rt_flags))
    {
        header = req.getHeader("token-b64", 9);
    }
    else if (qmark != nullptr && 0 == memcmp(qmark + 1, "access_token=", 13) && 
            B32_OOB_SIZE > (decode_len = url_h.valueLength - url.length() - 1 - 13) && 
            decode_base32_to(decode_buf, &decode_len, qmark + 1 + 13, decode_len) &&
            verify_token(decode_buf, decode_len, &ar, flags, data, length, remainingBytes, &decode_len, &decode_offset) && ar)
    {
        header.key = qmark;
        header.value = decode_buf + decode_offset;
        header.valueLength = decode_len;
        b64 = false;
    }
    else
    {
        header.key = nullptr;
    }
    
    if ((srv::VOID_ARG & flags))
    {
        if (!ar)
        {
            write(buf, p.data(), p.length(), nullptr, 0);
        }
        else if (header)
        {
            writeWithToken(buf, b64, header.value, header.valueLength, 
                    p.data(), p.length(), nullptr, 0);
        }
        else
        {
            res->end(unauthorized, sizeof(unauthorized) - 1);
            return;
        }
    }
    else if (!length)
    {
        if (!remainingBytes || remainingBytes > (65535 - 4 - p.length()))
        {
            res->end(invalid_request, sizeof(invalid_request) - 1);
        }
        else if (!ar)
        {
            res->httpSocket->setUserData(writeNew(p.data(), p.length(), data, length, remainingBytes,
                    flags));
        }
        else if (header)
        {
            res->httpSocket->setUserData(writeNewWithToken(
                    b64, header.value, header.valueLength, 
                    p.data(), p.length(), data, length, 
                    remainingBytes, flags));
        }
        else
        {
            res->end(unauthorized, sizeof(unauthorized) - 1);
        }
        return;
    }
    else if (remainingBytes + length > (65535 - 4 - p.length()))
    {
        res->end(invalid_request, sizeof(invalid_request) - 1);
        return;
    }
    else if (!remainingBytes)
    {
        if (!ar)
        {
            write(buf, p.data(), p.length(), data, length);
        }
        else if (header)
        {
            writeWithToken(buf, b64, header.value, header.valueLength, 
                    p.data(), p.length(), data, length);
        }
        else
        {
            res->end(unauthorized, sizeof(unauthorized) - 1);
            return;
        }
    }
    else if (!ar)
    {
        // persist end buf
        res->httpSocket->setUserData(writeNew(p.data(), p.length(), data, length, remainingBytes,
                flags));
        return;
    }
    else if (header)
    {
        // persist end buf
        res->httpSocket->setUserData(writeNewWithToken(
                b64, header.value, header.valueLength, 
                p.data(), p.length(), data, length, 
                remainingBytes, flags));
        return;
    }
    else
    {
        res->end(unauthorized, sizeof(unauthorized) - 1);
        return;
    }
    
    #if defined(REP_MASTER) || defined(SYNC_NODE)
    bool sync_pub = 0 == jni->id && 0 != (srv::SYNC_PUB & flags) && nullptr != instance->pkvSyncGroup;
    if (sync_pub)
        write32(jni->bufTmp, 0);
    #endif
    
    jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
    
    auto responseSize = read32(buf);
    bool left_paren = '[' == *(buf + 4);
    
    *(buf + 3) = left_paren ? '+' : '-';
    res->end(buf + 3, responseSize + 1);
    
    // must be a successful request
    if (!left_paren || '0' != *(buf + 5))
        return;
    
    if (instance->pubsub && 0 != (srv::PUB & flags))
    {
        // exclude the wrapper array
        // 3 is for '[0,'
        // 1 is for ']'
        broadcast(jni, buf + 4 + 3, responseSize - 3 - 1, srv::PUB_LAST_FIELD == (srv::PUB_LAST_FIELD & flags));
    }
    
    #if defined(REP_MASTER) || defined(SYNC_NODE)
    if (sync_pub && 0 != (responseSize = read32(jni->bufTmp)))
        instance->pkvSyncGroup->broadcast(jni->bufTmp + 4, responseSize, uWS::OpCode::BINARY);
    #endif
}

static void onCancelledHttpRequest(uWS::HttpResponse *res)
{
    char *bufNew = (char*)res->httpSocket->getUserData();
    if (bufNew != nullptr)
    {
        res->httpSocket->setUserData(nullptr);
        delete[] bufNew;
    }
}

static void onHttpDisconnection(uWS::HttpSocket<uWS::SERVER> *s)
{
    char *bufNew = (char*)s->getUserData();
    if (bufNew != nullptr)
    {
        s->setUserData(nullptr);
        delete[] bufNew;
    }
}

static inline void setup_http_handlers(JniContext* jni, uWS::Hub& hub, bool readonly)
{
    hub.onHttpData([jni](uWS::HttpResponse *res, char *data, size_t length, size_t remainingBytes) {
        // null when request body is too large since it's end() method was already invoked.
        if (res != nullptr)
            onHttpData(jni, res, data, length, remainingBytes);
    });
    
    hub.onHttpRequest([jni, readonly](uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t length, size_t remainingBytes) {
        onHttpRequest(jni, readonly, res, req, data, length, remainingBytes);
    });
    
    hub.onCancelledHttpRequest(onCancelledHttpRequest);
    
    hub.onHttpDisconnection(onHttpDisconnection);
    
    hub.getDefaultGroup<uWS::SERVER>().setHttpSocketKeepAlive(
        std::max(0, instance->keepalive_timeout_start),
        std::max(1000, instance->keepalive_timeout_interval)
    );
    
    if (instance->pubsub && instance->auto_ping)
        hub.getDefaultGroup<uWS::SERVER>().startAutoPing(instance->auto_ping);
}

#if defined(REP_MASTER) || defined(SYNC_NODE)
static const uint64_t MAX_SEQ_NUM = 0xFFFFFFFFFFFFFFFF;
static const size_t PKV_BATCH_THRESHOLD_BYTES = 1 << 20;
static const char PKV_SYNC_PARAM[] = "3:3";
//                                                      header     param         idx (occupieed by the null terminator spot)
static const size_t MAX_PKV_SYNC_PAYLOAD_SIZE = 65535 - 4 - sizeof(PKV_SYNC_PARAM);
static const uint8_t TYPE_PULL_SYNC = 1;
static const uint8_t TYPE_MULTI_GET = 2;
static const uint8_t TYPE_PUSH = 3;

static void send_pull_sync_to(uWS::WebSocket<uWS::SERVER> *ws,
        std::string& ws_batch,
        leveldb::Iterator* it,
        char* message, size_t seq_key_size,
        bool skipCompression)
{
    leveldb::Slice ks, vs;
    char* data;
    size_t seq_pfx_size = seq_key_size - 8, size;
    uint16_t count = 0, key_size, val_size;
    
    ks.data_ = message;
    ks.size_ = seq_key_size;
    
    it->Seek(ks);
    // find exact match
    if (it->Valid() && seq_key_size == (ks = it->key()).size_ && 0 == memcmp(ks.data_, message, seq_key_size))
    {
        key_size = static_cast<uint16_t>(seq_key_size);
        ws_batch.assign((char*)&count, 2); // occupy first 2 bytes
        do
        {
            vs = it->value();
            val_size = static_cast<uint16_t>(vs.size_);
            
            ws_batch.append((char*)&key_size, 2);
            ws_batch.append((char*)&val_size, 2);
            
            ws_batch.append(ks.data_, ks.size_);
            ws_batch.append(vs.data_, vs.size_);
            
            if (++count == 65535 || ws_batch.size() > PKV_BATCH_THRESHOLD_BYTES)
                break;
            
            it->Next();
        }
        while (it->Valid() && seq_key_size == (ks = it->key()).size_ && 0 == memcmp(ks.data_, message, seq_pfx_size));
        
        if (skipCompression)
        {
            ws_batch.push_back(0);
            
            data = (char*)ws_batch.data();
            // write count
            write16(data, count);
            
            ws->send(data, ws_batch.size(), uWS::OpCode::BINARY);
        }
        else
        {
            data = (char*)ws_batch.data();
            // write count
            write16(data, count);
            
            size = snappy::Compress(data, ws_batch.size(), &protostuff::RepEntry::scratch);
            protostuff::RepEntry::scratch.push_back(TYPE_PULL_SYNC);
            protostuff::RepEntry::scratch.push_back(0xFF);
            
            ws->send(protostuff::RepEntry::scratch.data(), size + 2, uWS::OpCode::BINARY);
        }
    }
}

static void send_pkv_next_seq_to(uWS::WebSocket<uWS::SERVER> *ws,
        std::string& ws_batch,
        leveldb::Iterator* it,
        char* message, size_t length,
        bool last1)
{
    auto seq_key_size = length - 1;
    auto seq_pfx_size = seq_key_size - 8;
    auto seq_num_start = message + seq_key_size - 8;
    uint64_t next_seq_num = 1;
    uint64_t pull_seq_num = protostuff::decodeSeq(seq_num_start);
    
    // seek to most recent seq num
    memcpy(seq_num_start, &MAX_SEQ_NUM, 8);
    
    leveldb::Slice ks(message, seq_key_size);
    
    it->Seek(ks);
    
    if (it->Valid())
    {
        it->Prev();
        
        if (it->Valid() && seq_key_size == (ks = it->key()).size_ && 0 == memcmp(ks.data_, message, seq_pfx_size))
            next_seq_num += protostuff::decodeSeq(ks.data_ + ks.size_ - 8);
    }
    
    if (last1)
        message[length - 1] = 0;
    
    protostuff::encodeSeq(seq_num_start, next_seq_num);
    ws->send(message, length, uWS::OpCode::BINARY);
    
    if (pull_seq_num != 0)
    {
        if (last1)
        {
            message[1] = 0xFF;
            message[2] = 0xFF;
        }
    
        // modify type for pull sync
        *(seq_num_start - 1) = 0x80 | *(seq_num_start - 1);
        protostuff::encodeSeq(seq_num_start, pull_seq_num);
        
        send_pull_sync_to(ws, ws_batch, it, message, seq_key_size, false);
    }
}

static void apply_pkv_and_reply_to(uWS::WebSocket<uWS::SERVER> *ws,
        protostuff::RepEntry* rep_entry,
        char* message, size_t length,
        char* key, uint16_t key_size)
{
    auto val_size = read16(message + 2);
    auto val = message + 4 + key_size;
    
    leveldb::WriteOptions wo;
    leveldb::WriteBatch batch;
    
    batch.Put(leveldb::Slice(key, key_size), leveldb::Slice(val, val_size));
    
    #ifdef SYNC_NODE
    if (rep_entry->db->Write(wo, &batch).ok())
    #else
    if (rep_entry->write(wo, &batch))
    #endif
    {
        // ok (error code: 0)
        *(key + key_size) = 0;
        
        // next seq
        val = key + key_size - 8;
        protostuff::encodeSeq(val, 1 + protostuff::decodeSeq(val));
    }
    else
    {
        // error
        *(key + key_size) = 1;
    }
    
    ws->send(key, key_size + 1, uWS::OpCode::BINARY);
}

static void apply_pkv_jni_and_reply_to(uWS::WebSocket<uWS::SERVER> *ws,
        JniContext* jni, uWS::Hub& hub, uint8_t idx,
        char* message, size_t length,
        char* key, uint16_t key_size)
{
    char* buf = jni->buf;
    uint16_t headerSize = sizeof(PKV_SYNC_PARAM) - 1;
    uint16_t payloadSize = 1 + length; // 1 is for the idx
    
    write16(buf, headerSize);
    buf += 2;
    write16(buf, payloadSize);
    buf += 2;
    
    memcpy(buf, PKV_SYNC_PARAM, headerSize);
    buf += headerSize;
    
    *buf++ = idx;
    memcpy(buf, message, length);
    //buf += length;
    
    // clear res buf
    write32(jni->bufTmp, 0);
    jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
    auto payload_size = read32(jni->bufTmp);
    
    if (payload_size == 0)
    {
        // error
        *(key + key_size) = 1;
    }
    else
    {
        // ok (error code: 0)
        *(key + key_size) = 0;
        
        // next seq
        buf = key + key_size - 8;
        protostuff::encodeSeq(buf, 1 + protostuff::decodeSeq(buf));
    }
    
    ws->send(key, key_size + 1, uWS::OpCode::BINARY);
    
    // broadcast payload
    if (payload_size > 1)
        hub.getDefaultGroup<uWS::SERVER>().broadcast(jni->bufTmp + 4, payload_size, uWS::OpCode::TEXT);
}

static void send_multi_get_to(uWS::WebSocket<uWS::SERVER> *ws,
        std::string& ws_batch,
        leveldb::DB* db,
        char* message, size_t length,
        uint16_t count)
{
    size_t size;
    uint16_t i, key_size, val_size, missing_count = 0;
    leveldb::Slice ks, vs;
    
    leveldb::ReadOptions ro;
    ro.fill_cache = false;

    ws_batch.assign((char*)&missing_count, 2);
    // copy count
    ws_batch.append((char*)&count, 2);
    
    // go to first element
    char* buf = message + 2;
    
    auto it = db->NewIterator(ro);
    
    for (i = 0; i < count && ws_batch.size() < PKV_BATCH_THRESHOLD_BYTES; i++, buf += key_size)
    {
        // copy key size
        ws_batch.append(buf, 2);
        
        key_size = read16(buf);
        buf += 2;
        
        // check prefix key
        if (0x80 != *(uint8_t*)buf)
        {
            // write val size
            val_size = 0;
            ws_batch.append((char*)&val_size, 2);
            
            // write key only
            ws_batch.append(buf, key_size);
            
            missing_count++;
            continue;
        }
        
        size = key_size;
        ks.size_ = size;
        ks.data_ = buf;
        it->Seek(ks);
        
        if (it->Valid() && size == (ks = it->key()).size_ && 0 == memcmp(ks.data_, buf, size))
        {
            vs = it->value();
            
            // write val size
            val_size = static_cast<uint16_t>(vs.size_);
            ws_batch.append((char*)&val_size, 2);
            
            // write key
            ws_batch.append(buf, key_size);
            // write val
            ws_batch.append(vs.data_, val_size);
        }
        else
        {
            // write val size
            val_size = 0;
            ws_batch.append((char*)&val_size, 2);
            
            // write key only
            ws_batch.append(buf, key_size);
            
            missing_count++;
        }
    }
    
    delete it;
    
    buf = (char*)ws_batch.data();
    
    if (missing_count != 0)
        write16(buf, missing_count);
    
    if (i != count)
        write16(buf + 2, i);
    
    size = snappy::Compress(buf, ws_batch.size(), &protostuff::RepEntry::scratch);
    protostuff::RepEntry::scratch.push_back(TYPE_MULTI_GET);
    protostuff::RepEntry::scratch.push_back(0xFF);
    
    ws->send(protostuff::RepEntry::scratch.data(), size + 2, uWS::OpCode::BINARY);
}

static inline void setup_pkv_sync(JniContext* jni, uWS::Hub& hub,
        #ifdef SYNC_NODE
        std::string& ws_batch)
        #else
        std::string& ws_batch, bool withOnConnectionHandler)
        #endif
{
    instance->pkvSyncGroup->onMessage([jni, &hub, &ws_batch](
            uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length, uWS::OpCode opCode) {
        auto rep_entry = (protostuff::RepEntry*)ws->getUserData();
        auto last = *(uint8_t*)(message + length - 1);
        uint16_t key_size;
        if (last == 0 || last == 1)
        {
            // client query for the seq num to start with
            if (0x80 != *(uint8_t*)message)
            {
                // invalid
                return;
            }
            
            leveldb::ReadOptions ro;
            ro.fill_cache = false;
            
            auto it = rep_entry->db->NewIterator(ro);
            
            // check type
            if (0x80 < *(uint8_t*)(message + length - 10))
            {
                // pull req
                send_pull_sync_to(ws, ws_batch, it, message, length - 1, last == 1);
            }
            else
            {
                send_pkv_next_seq_to(ws, ws_batch, it, message, length, last == 1);
            }
            
            delete it;
            return;
        }
        
        if (last == TYPE_MULTI_GET)
        {
            // multi-get request
            // store count to key_size
            if (0 != (key_size = read16(message)))
                send_multi_get_to(ws, ws_batch, rep_entry->db, message, length - 1, key_size);
            return;
        }
        
        auto data = message;
        auto size = length - 1;
        if (last == 0xFF)
        {
            if (!snappy::Uncompress(data, size, &protostuff::RepEntry::scratch))
            {
                std::cerr << "pkv_sync failed on snappy decompress." << std::endl;
                return;
            }
            
            data = (char*)protostuff::RepEntry::scratch.data();
            size = protostuff::RepEntry::scratch.size();
        }
        else if (last != TYPE_PUSH)
        {
            // unknown request
            return;
        }
        
        if (size > MAX_PKV_SYNC_PAYLOAD_SIZE)
        {
            std::cerr << "pkv_sync payload is too large." << std::endl;
            return;
        }
        
        key_size = read16(data);
        if (9 > key_size || 0x80 != *(uint8_t*)(data + 4))
        {
            std::cerr << "pkv_sync key is invalid." << std::endl;
            return;
        }
        
        // copy key
        if (data != message)
            memcpy(message + 4, data + 4, key_size);
        
        auto key = message + 4;
        auto type = *(uint8_t*)(key + key_size - 9);
        leveldb::ReadOptions ro;
        ro.fill_cache = false;
        
        // check if this key exists (already acked if it does exist)
        if (rep_entry->db->Get(ro, leveldb::Slice(key, key_size), &protostuff::RepEntry::scratch).ok())
        {
            *(key + key_size) = 0;
            // next seq
            data = key + key_size - 8;
            protostuff::encodeSeq(data, 1 + protostuff::decodeSeq(data));
            
            ws->send(key, key_size + 1, uWS::OpCode::BINARY);
        }
        else if (type < 100)
        {
            apply_pkv_jni_and_reply_to(ws, jni, hub, rep_entry->idx,
                    data, size,
                    key, key_size);
        }
        else
        {
            apply_pkv_and_reply_to(ws, rep_entry,
                    data, size,
                    key, key_size);
        }
    });
    
    #ifndef SYNC_NODE
    if (!withOnConnectionHandler)
        return;
    #endif
    
    hub.onConnection([](uWS::WebSocket<uWS::SERVER> *ws, uWS::HttpRequest req) {
        auto url_h = req.getUrl();
        auto key_len = instance->encrypted_session_key_len;
        auto min_expect_len = url_h.valueLength - 1; // exclude first slash
        const char* data = url_h.value + 1;
        if (key_len == 0 ||
                min_expect_len <= key_len ||
                0 != memcmp(data, instance->encrypted_session_key, key_len) ||
                '/' != data[key_len] ||
                0 == (min_expect_len = min_expect_len - key_len - 1))
        {
            return;
        }
        
        std::string db_name;
        db_name.assign(data + key_len + 1, min_expect_len);
        
        const auto entry_map = protostuff::RepEntry::name_map();
        auto entry_it = entry_map.find(db_name);
        if (entry_it != entry_map.end())
        {
            ws->setUserData(entry_it->second);
            ws->transfer(instance->pkvSyncGroup);
        }
    });
}
#endif

#ifdef REP_SLAVE
static const char LOG_UPDATE_PARAM[] = "3:2";
static const uint16_t LOG_UPDATE_PAYLOAD_HEADER_LEN = 4 + sizeof(LOG_UPDATE_PARAM) - 1;

static void print_rep_err(protostuff::RepEntry* rep_entry, uint8_t err)
{
    switch (err)
    {
        case ERR_DB_NOT_FOUND:
            std::cerr << rep_entry->name << ": db not found." << std::endl;
            break;
        case ERR_ENTRY_ID_INVALID:
            std::cerr << rep_entry->name << ": invalid entry id (db count cannot exceed 255)." << std::endl;
            break;
        case ERR_NEED_RESTORE_FROM_RECENT_BACKUP:
            std::cerr << rep_entry->name 
                    << ": restore from a recent backup, to sync with master." << std::endl;
            break;
        default:
            std::cerr << rep_entry->name << ": unknown error." << std::endl;
    }
}

static bool handle_log(JniContext* jni, uWS::Hub& hub, 
        protostuff::RepEntry* rep_entry, uint8_t entry_id,
        char* message, size_t length,
        std::function<void(const leveldb::Slice&, const leveldb::Slice&)> updates_handler)
{
    // decompress
    if (!snappy::Uncompress(message, length, &protostuff::RepEntry::scratch))
    {
        protostuff::RepEntry::scratch.clear();
        std::cerr << rep_entry->name << ": could not decompress log update" << std::endl;
        return false;
    }
    
    auto scratch_buf = (char*)protostuff::RepEntry::scratch.data();
    auto scratch_size = protostuff::RepEntry::scratch.size();
    uint64_t seq_num = *(uint64_t*)scratch_buf;
    if (seq_num != rep_entry->seq_num)
    {
        protostuff::RepEntry::scratch.clear();
        std::cout << rep_entry->name << ": received log twice at " << seq_num << " < " << rep_entry->seq_num << std::endl;
        return false;
    }
    
    // set header
    protostuff::RepEntry::segments.clear();
    write(jni->buf, LOG_UPDATE_PARAM, sizeof(LOG_UPDATE_PARAM) - 1, (char*)&entry_id, 1);
    
    leveldb::WriteOptions wo;
    #ifdef HYPER
    leveldb::Slice updates(scratch_buf, scratch_size);
    #else
    leveldb::WriteBatch updates;
    updates.rep_.assign(scratch_buf, scratch_size);
    #endif
    
    #ifdef HYPER
    if (!rep_entry->db->WriteUpdates(wo, updates, updates_handler).ok())
    #else
    if (!rep_entry->db->WriteUpdates(wo, &updates, updates_handler).ok())
    #endif
    {
        protostuff::RepEntry::scratch.clear();
        std::cerr << rep_entry->name << ": could not apply log update at " << seq_num << std::endl;
        return false;
    }
    
    // update seq num
    ++rep_entry->seq_num;
    if (!rep_entry->queued)
    {
        rep_entry->queued = true;
        protostuff::RepEntry::ack_queue.push_back(rep_entry);
        // delay ack (consume the current batch of update logs before ack)
        instance->slave_ack_ticks = 0;
    }
    
    // TODO
    // req payload size
    // if (1 == read16(jni->buf + 2))
    // {
    //     protostuff::RepEntry::scratch.clear();
    //     return true;
    // }
    
    // call jni update handler
    write32(jni->bufTmp, 0);
    jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
    auto payload_size = read32(jni->bufTmp);
    
    // broadcast if pub payload is available
    if (payload_size != 0)
        hub.getDefaultGroup<uWS::SERVER>().broadcast(jni->bufTmp + 4, payload_size, uWS::OpCode::TEXT);
    
    if (protostuff::RepEntry::segments.empty())
    {
        protostuff::RepEntry::scratch.clear();
        return true;
    }
    
    scratch_buf = (char*)protostuff::RepEntry::scratch.data();
    for (auto segment_size : protostuff::RepEntry::segments)
    {
        // write the payload size
        write16(jni->buf + 2, segment_size);
        // write the payload
        memcpy(jni->buf + LOG_UPDATE_PAYLOAD_HEADER_LEN + 1, // 1 is for the entry_id
                scratch_buf + scratch_size, segment_size);
        
        scratch_size += segment_size;
        
        // call jni update handler
        write32(jni->bufTmp, 0);
        jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
        payload_size = read32(jni->bufTmp);
        
        // broadcast if pub payload is available
        if (payload_size != 0)
            hub.getDefaultGroup<uWS::SERVER>().broadcast(jni->bufTmp + 4, payload_size, uWS::OpCode::TEXT);
    }
    
    protostuff::RepEntry::scratch.clear();
    protostuff::RepEntry::segments.clear();
    return true;
}

static void handle_batch_logs(JniContext* jni, uWS::Hub& hub, 
        protostuff::RepEntry* rep_entry, uint8_t entry_id,
        char* message, size_t length,
        std::function<void(const leveldb::Slice&, const leveldb::Slice&)> updates_handler)
{
    char* buf = message;
    uint32_t record_size;
    uint16_t count = read16(buf), i = 0;
    buf += 2;
    for (; i < count; i++)
    {
        record_size = *(uint32_t*)buf;
        buf += 4;
        
        // exclude last byte
        if (!handle_log(jni, hub, rep_entry, entry_id, buf, record_size - 1, updates_handler))
            break;
        
        buf += record_size;
    }
    
    if (i != 0)
    {
        instance->slave_refresh = false;
        instance->slave_idle_refresh_ticks = 0;
        send_req_to(instance->slave_client_ws, instance->slave_batch_threshold_mb, protostuff::RepEntry::ack_queue);
        protostuff::RepEntry::ack_queue.clear();
    }
}
#endif

#ifndef BENCH
static void serve_http(void* ptr, JniContext* jni) {
    // initialize assets
    if (instance->assets_preloaded)
    {
        map_files(instance->assets_dir);
        auto it = assets_map.find(instance->assets_dir + "/index.html");
        if (it == assets_map.end())
        {
            index_html.buf = nullptr;
            index_html.len = 0;
        }
        else
        {
            index_html = it->second;
        }
    }
    else if (instance->assets_dir.length())
    {
        auto len = instance->assets_dir.length();
        instance->assets_dir += "/index.html";
        index_html.buf = buffer_file(instance->assets_dir.c_str(), &index_html.len);
        instance->assets_dir.erase(len);
    }
    #if defined(REP_MASTER) && defined(REP_MT)
    // initialize list size
    protostuff::ReaderEntry::list.resize(instance->readers);
    std::fill(protostuff::ReaderEntry::list.begin(), protostuff::ReaderEntry::list.end(), nullptr);
    #endif
    // initialize db
    setupThread(jni, 0, 0);
    
    #if defined(REP_MASTER) || defined(REP_SLAVE)
    // reserve space for scratch
    protostuff::RepEntry::scratch.reserve(protostuff::SCRATCH_RESERVED_SIZE);
    #endif
    
    #if defined(SYNC_NODE)
    std::string ws_batch;
    if (0 != (instance->rt_flags & RT_PKV_SYNC))
        ws_batch.reserve(2 << 20);
    #elif defined(REP_MASTER)
    std::string ws_batch;
    if (!instance->master_batch_multi_frame || 0 != (instance->rt_flags & RT_PKV_SYNC))
        ws_batch.reserve((1 + instance->master_batch_threshold_mb) << 20);
    #endif
    
    uWS::Hub hub;
    instance->hub = &hub;
    
    // initialize http handlers
    #ifdef REP_SLAVE
    setup_http_handlers(jni, hub, true);
    #else
    setup_http_handlers(jni, hub, false);
    #endif
    
    // websockets
    /*hub.onPing([](uWS::WebSocket<uWS::CLIENT> *ws, char *message, size_t length) {
        std::cout << "@ping " << length << std::endl;
    });
    
    hub.onPong([](uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length) {
        std::cout << "@pong " << length << std::endl;
    });*/
    
    #if defined(REP_SLAVE)
    
    auto updates_handler = [jni](const leveldb::Slice& key, const leveldb::Slice& val) {
        uint32_t projected_size;
        uint16_t payload_size;
        uint8_t first = *key.data_;
        if (first == 0 || first > 128 || (first != 128 && key.size_ != 9))
            return;
        
        // pkv or entity
        if (protostuff::RepEntry::segments.empty())
        {
            payload_size = read16(jni->buf + 2);
            projected_size = 4 + key.size_ + val.size_ + LOG_UPDATE_PAYLOAD_HEADER_LEN + payload_size;
            if (0xFFFF >= projected_size)
            {
                write(jni->buf + LOG_UPDATE_PAYLOAD_HEADER_LEN + payload_size, key.data_, key.size_, val.data_, val.size_);
                
                // update size
                write16(jni->buf + 2, payload_size + 4 + key.size_ + val.size_);
                return;
            }
            
            if (0xFFFF < (projected_size - payload_size + 1)) // + 1 for the entry_id
            {
                std::cerr << "Skipping an entry due to being too large." << std::endl;
                return;
            }
            
            // add to segments
            payload_size = static_cast<uint16_t>(4 + key.size_ + val.size_ + 1); // + 1 for the entry_id
            protostuff::RepEntry::segments.push_back(payload_size);
        }
        else
        {
            payload_size = protostuff::RepEntry::segments.back();
            projected_size = 4 + key.size_ + val.size_ + LOG_UPDATE_PAYLOAD_HEADER_LEN + payload_size;
            if (0xFFFF >= projected_size)
            {
                // fits in the current segment
            }
            else if (0xFFFF < (projected_size - payload_size + 1)) // + 1 for the entry_id
            {
                std::cerr << "Skipping an entry due to being too large." << std::endl;
                return;
            }
            else
            {
                // add to segments
                payload_size = static_cast<uint16_t>(4 + key.size_ + val.size_ + 1); // + 1 for the entry_id
                protostuff::RepEntry::segments.push_back(payload_size);
            }
        }
        
        // header
        payload_size = static_cast<uint16_t>(key.size_);
        protostuff::RepEntry::scratch.append((char*)&payload_size, 2);
        
        payload_size = static_cast<uint16_t>(val.size_);
        protostuff::RepEntry::scratch.append((char*)&payload_size, 2);
        
        // payload
        protostuff::RepEntry::scratch.append(key.data_, key.size_);
        
        protostuff::RepEntry::scratch.append(val.data_, val.size_);
    };
    
    hub.onMessage([&hub, jni, updates_handler](uWS::WebSocket<uWS::CLIENT> *ws, char *message, size_t length, uWS::OpCode opCode) {
        protostuff::RepEntry* rep_entry;
        uint8_t entry_id = *(message + length - 1);
        
        if (entry_id != 0xFF)
        {
            if (entry_id >= protostuff::RepEntry::list.size())
            {
                std::cerr << "ws response invalid" << std::endl;
            }
            else
            {
                handle_log(jni, hub, protostuff::RepEntry::list[entry_id], 
                        // exclude last byte
                        entry_id, message, length - 1, updates_handler);
            }
            return;
        }
        
        switch (*(uint8_t*)message)
        {
            case REQ_REP:
                entry_id = *(message + length - 2);
                if (entry_id == 0xFF)
                {
                    instance->slave_refresh = true;
                    break;
                }
                if (entry_id >= protostuff::RepEntry::list.size())
                {
                    std::cerr << "@rep: ws response invalid" << std::endl;
                    break;
                }
                rep_entry = protostuff::RepEntry::list[entry_id];
                if (4 != length)
                {
                    // logs on a single frame
                    handle_batch_logs(jni, hub, rep_entry, 
                            //        exclude the first byte and the last 2 bytes
                            entry_id, message + 1, length - 3, updates_handler);
                }
                // store err to entry_id
                else if (0 != (entry_id = *(uint8_t*)(message + 1)))
                {
                    print_rep_err(rep_entry, entry_id);
                }
                else
                {
                    // send log request
                    instance->slave_idle_refresh_ticks = 0;
                    protostuff::RepEntry::ack_queue.push_back(rep_entry);
                    send_req_to(instance->slave_client_ws, instance->slave_batch_threshold_mb, protostuff::RepEntry::ack_queue);
                    protostuff::RepEntry::ack_queue.clear();
                }
                break;
            case REQ_BACKUP:
                #ifdef HYPER
                entry_id = *(message + length - 2);
                if (entry_id >= protostuff::RepEntry::list.size())
                {
                    std::cerr << "@backup: ws response invalid" << std::endl;
                    break;
                }
                ssr_backup(protostuff::RepEntry::list[entry_id], message + 1, length - 3); // exclude 1st, 2nd-to-the-last, last
                #endif
                break;
            default:
                // broadcast
                hub.getDefaultGroup<uWS::SERVER>().broadcast(message, length - 1, uWS::OpCode::TEXT);
        }
    });
    
    hub.onConnection([](uWS::WebSocket<uWS::CLIENT> *ws, uWS::HttpRequest req) {
        //std::cout << "connected" << std::endl;
        instance->slave_connected = true;
        instance->slave_client_ws = ws;
        
        // reset
        instance->slave_refresh = false;
        instance->slave_ack_ticks = 0;
        protostuff::RepEntry::ack_queue.clear();
        
        // unsets the entry's queue bool field and sends the req
        send_req_to(ws, instance->slave_batch_threshold_mb, protostuff::RepEntry::list);
    });
    
    hub.onDisconnection([](uWS::WebSocket<uWS::CLIENT> *ws, int code, char *message, size_t length) {
        //std::cout << "reconnecting ..." << std::endl;
        instance->slave_connected = false;
    });
    
    Timer* timer = new Timer(hub.getLoop());
    timer->start([](Timer *timer) {
        if (!instance->slave_connected)
        {
            //std::cout << "try to connect ..." << std::endl;
            instance->hub->connect(instance->slave_connect_url.c_str(), nullptr);
            
            // once connected, onConnection is called
            return;
        }
        
        if (instance->slave_ack_interval == ++instance->slave_ack_ticks)
        {
            instance->slave_ack_ticks = 0;
            if (!protostuff::RepEntry::ack_queue.empty())
            {
                instance->slave_refresh = false;
                instance->slave_idle_refresh_ticks = 0;
                send_req_to(instance->slave_client_ws, instance->slave_batch_threshold_mb, protostuff::RepEntry::ack_queue);
                protostuff::RepEntry::ack_queue.clear();
            }
            else if (instance->slave_refresh)
            {
                instance->slave_refresh = false;
                instance->slave_idle_refresh_ticks = 0;
                send_req_to(instance->slave_client_ws, instance->slave_batch_threshold_mb, protostuff::RepEntry::list);
            }
            else if (instance->slave_idle_refresh_interval != 0 &&
                    instance->slave_idle_refresh_interval == ++instance->slave_idle_refresh_ticks)
            {
                instance->slave_idle_refresh_ticks = 0;
                send_req_to(instance->slave_client_ws, instance->slave_batch_threshold_mb, protostuff::RepEntry::list);
            }
        }
    }, 0, instance->timer_interval);
    
    #elif defined(REP_MASTER) || defined(SYNC_NODE)
    
    if (instance->rt_flags & RT_PKV_SYNC)
    {
        instance->pkvSyncGroup = hub.createGroup<uWS::SERVER>();
        instance->pkvSyncGroup->listen(uWS::TRANSFERS);
        
        #if defined(SYNC_NODE)
        setup_pkv_sync(jni, hub, ws_batch);
        #elif defined(REP_RT)
        setup_pkv_sync(jni, hub, ws_batch, true);
        #else
        setup_pkv_sync(jni, hub, ws_batch, false);
        #endif
    }
    
    #endif
    
    #if defined(REP_MASTER) && !defined(REP_RT)
    std::map<uWS::WebSocket<uWS::SERVER>*, WsEntry> ws_map;
    
    uWS::Group<uWS::SERVER>* repSlaveGroup = hub.createGroup<uWS::SERVER>();
    //instance->repSlaveGroup = repSlaveGroup;
    
    repSlaveGroup->listen(uWS::TRANSFERS);
    
    hub.onConnection([&ws_map, repSlaveGroup](uWS::WebSocket<uWS::SERVER> *ws, uWS::HttpRequest req) {
        auto url_h = req.getUrl();
        auto key_len = instance->encrypted_session_key_len;
        auto min_expect_len = url_h.valueLength - 1; // exclude first slash
        const char* data = url_h.value + 1;
        if (key_len == 0 ||
                min_expect_len < key_len ||
                0 != memcmp(data, instance->encrypted_session_key, key_len))
        {
            return;
        }
        
        // check exact match
        if (key_len == min_expect_len)
        {
            // transfer
            ws->transfer(repSlaveGroup);
            // register
            ws_map[ws] = {};
            return;
        }
        
        if (0 == (instance->rt_flags & RT_PKV_SYNC) ||
                '/' != data[key_len] ||
                0 == (min_expect_len = min_expect_len - key_len - 1))
        {
            return;
        }
        
        std::string db_name;
        db_name.assign(data + key_len + 1, min_expect_len);
        
        const auto entry_map = protostuff::RepEntry::name_map();
        auto entry_it = entry_map.find(db_name);
        if (entry_it != entry_map.end())
        {
            ws->setUserData((void*)(intptr_t)entry_it->second->idx);
            ws->transfer(instance->pkvSyncGroup);
        }
    });
    
    repSlaveGroup->onDisconnection([&ws_map](uWS::WebSocket<uWS::SERVER> *ws, int code, char *message, size_t length) {
        auto it = ws_map.find(ws);
        if (it == ws_map.end())
            return;
        
        // deregister
        for (auto rep_entry : it->second.subs)
        {
            // unsubscribe
            rep_entry->slave_map.erase(ws);
        }
        ws_map.erase(ws);
        //std::cout << "client disconnected" << std::endl;
    });
    
    repSlaveGroup->onMessage([&hub, &ws_map, &ws_batch](
            uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length, uWS::OpCode opCode) {
        auto it = ws_map.find(ws);
        if (it == ws_map.end())
            return;
        
        uint64_t seq_num;
        char* buf = message;
        uint8_t type = *buf++; // 0 means standalone ws request
        uint8_t count = *buf++;
        uint8_t entry_id, entry_len, i;
        std::string tmp;
        protostuff::RepEntry* rep_entry;
        
        // if type is non-zero, its the batch_threshold_mb setting from a slave
        const size_t threshold_bytes = type == 0 ? 0 :
                ((type == 0xFF ? instance->master_batch_threshold_mb : type) << 20);
        
        const auto entry_map = protostuff::RepEntry::name_map();
        for (i = 0; i < count; i++)
        {
            entry_id = *buf++;
            entry_len = *buf++;
            tmp.assign(buf, entry_len);
            buf += entry_len;
            
            auto entry_it = entry_map.find(tmp);
            if (entry_it == entry_map.end())
            {
                if (type == 0)
                {
                    buf += handle_ws_req(ws, nullptr, nullptr, entry_id, buf);
                }
                else
                {
                    // consume seq_num
                    buf += 8;
                    send_err_to(ws, entry_id, ERR_DB_NOT_FOUND);
                }
                continue;
            }
            
            if (entry_id == 255 && type != 0)
            {
                // consume seq_num
                buf += 8;
                send_err_to(ws, entry_id, ERR_ENTRY_ID_INVALID);
                continue;
            }
            
            rep_entry = entry_it->second;
            
            if (type == 0)
            {
                // standalone ws request
                buf += handle_ws_req(ws, rep_entry, &rep_entry->slave_map, entry_id, buf);
                continue;
            }
            
            seq_num = *(uint64_t*)buf;
            buf += 8;
            
            if (seq_num <= rep_entry->oob_num)
            {
                // slave needs to load/restore from a recent backup
                send_err_to(ws, entry_id, ERR_NEED_RESTORE_FROM_RECENT_BACKUP);
                continue;
            }
            
            const bool pending = seq_num > rep_entry->seq_num;
            
            // subscribe/update seq_num
            auto slave_it = rep_entry->slave_map.find(ws);
            
            if (slave_it == rep_entry->slave_map.end())
            {
                // subscribe
                it->second.subs.push_back(rep_entry);
                rep_entry->slave_map[ws] = { ws, seq_num, entry_id, pending, true };
                //std::cout << "client subscribed to " << tmp << " | " << seq_num << " | " << (int)entry_id << std::endl;
            }
            else
            {
                // update fields
                slave_it->second.seq_num = seq_num;
                slave_it->second.entry_id = entry_id;
                slave_it->second.pending = pending;
                slave_it->second.acked = true;
            }
            
            if (pending)
            {
                // noop
            }
            else if (instance->master_batch_multi_frame)
            {
                //std::cout << "seq_num: " << seq_num << " <= " << rep_entry->seq_num << std::endl;
                send_logs_to(ws, rep_entry, rep_entry->reader, seq_num, entry_id, threshold_bytes,
                        &protostuff::RepEntry::scratch);
            }
            else
            {
                send_batch_logs_to(ws, rep_entry, rep_entry->reader, seq_num, entry_id, threshold_bytes,
                        &protostuff::RepEntry::scratch, &ws_batch);
            }
        }
    });
    
    Timer* timer = new Timer(hub.getLoop());
    timer->start([](Timer *timer) {
        if (instance->master_update_interval == ++instance->master_update_ticks)
        {
            if (!protostuff::RepEntry::queue.empty())
            {
                instance->master_update_ticks = 0;
                send_recent_logs();
            }
            else
            {
                // wait for next tick
                --instance->master_update_ticks;
            }
        }
    }, 0, instance->timer_interval);
    
    #endif
    
    hub.listen(instance->bind_ip, instance->port);
    hub.run();
}
#endif

#if defined(REP_MASTER) || defined(REP_SLAVE)
static void serve_http_readonly(void* ptr, JniContext* jni) {
    int id = (int)(intptr_t)ptr;
    setupThread(jni, 1, id);
    
    #if defined(REP_MASTER) && defined(REP_MT)
    std::map<uWS::WebSocket<uWS::SERVER>*, std::vector<uint8_t>> ws_map;
    std::atomic<uint64_t> reader_queued(0);
    auto db_count = protostuff::RepEntry::db_map().size();
    auto reader_entry = protostuff::ReaderEntry(db_count, &reader_queued);
    // register
    protostuff::ReaderEntry::list[id - 1] = &reader_entry;
    auto env = leveldb::Env::Default();
    for (auto& db_it : protostuff::RepEntry::db_map())
    {
        leveldb::SequentialFile* sf;
        #if defined(HYPER)
        if (!env->NewSequentialFile(db_it.second->log_path, &sf).ok())
        #else
        if (!env->NewSharedSequentialFile(db_it.second->log_path, &sf).ok())
        #endif
        {
            std::cerr << "Reader " << id << " could not open log: " << db_it.second->log_path << std::endl;
            return;
        }
        //reader_entry.mapping[db_it.second->idx].sf = sf;
        reader_entry.mapping[db_it.second->idx].reader = new leveldb::log::Reader(sf, nullptr, true, 0);
        reader_entry.mapping[db_it.second->idx].rep_entry = db_it.second;
        reader_entry.mapping[db_it.second->idx].rep_num = 0;
        reader_entry.mapping[db_it.second->idx].idx = db_it.second->idx;
    }
    #endif
    
    #if defined(REP_MASTER) && defined(REP_MT)
    std::string ws_batch;
    if (!instance->master_batch_multi_frame)
        ws_batch.reserve((1 + instance->master_batch_threshold_mb) << 20);
    #endif
    
    uWS::Hub hub;
    setup_http_handlers(jni, hub, true);
    
    #if defined(REP_MASTER) && defined(REP_MT)
    
    uWS::Group<uWS::SERVER>* repSlaveGroup = hub.createGroup<uWS::SERVER>();
    repSlaveGroup->listen(uWS::TRANSFERS);
    
    hub.onConnection([&ws_map, repSlaveGroup](uWS::WebSocket<uWS::SERVER> *ws, uWS::HttpRequest req) {
        auto url_h = req.getUrl();
        auto len = instance->encrypted_session_key_len;
        //                     exclude first slash
        if (len != 0 && len == url_h.valueLength - 1 &&
                0 == memcmp(url_h.value + 1, instance->encrypted_session_key, len))
        {
            // transfer
            ws->transfer(repSlaveGroup);
            // register
            ws_map[ws] = {};
        }
    });
    
    repSlaveGroup->onDisconnection([&ws_map, &reader_entry](uWS::WebSocket<uWS::SERVER> *ws, int code, char *message, size_t length) {
        auto it = ws_map.find(ws);
        if (it == ws_map.end())
            return;
        
        // deregister
        for (auto idx : it->second)
        {
            // unsubscribe
            reader_entry.mapping[idx].slave_map.erase(ws);
        }
        ws_map.erase(ws);
        //std::cout << "client disconnected" << std::endl;
    });
    
    repSlaveGroup->onMessage([&hub, &ws_map, &reader_entry, &ws_batch](
            uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length, uWS::OpCode opCode) {
        auto it = ws_map.find(ws);
        if (it == ws_map.end())
            return;
        
        uint64_t seq_num;
        char* buf = message;
        uint8_t type = *buf++; // 0 means standalone ws request
        uint8_t count = *buf++;
        uint8_t entry_id, entry_len, i;
        std::string tmp;
        protostuff::RepEntry* rep_entry;
        
        // if type is non-zero, its the batch_threshold_mb setting from a slave
        const size_t threshold_bytes = type == 0 ? 0 :
                ((type == 0xFF ? instance->master_batch_threshold_mb : type) << 20);
        
        const auto entry_map = protostuff::RepEntry::name_map();
        for (i = 0; i < count; i++)
        {
            entry_id = *buf++;
            entry_len = *buf++;
            tmp.assign(buf, entry_len);
            buf += entry_len;
            
            auto entry_it = entry_map.find(tmp);
            if (entry_it == entry_map.end())
            {
                if (type == 0)
                {
                    buf += handle_ws_req(ws, nullptr, nullptr, entry_id, buf);
                }
                else
                {
                    // consume seq_num
                    buf += 8;
                    send_err_to(ws, entry_id, ERR_DB_NOT_FOUND);
                }
                continue;
            }
            
            if (entry_id == 255 && type != 0)
            {
                // consume seq_num
                buf += 8;
                send_err_to(ws, entry_id, ERR_ENTRY_ID_INVALID);
                continue;
            }
            
            rep_entry = entry_it->second;
            
            if (type == 0)
            {
                // standalone ws request
                buf += handle_ws_req(ws, rep_entry, &reader_entry.mapping[rep_entry->idx].slave_map, entry_id, buf);
                continue;
            }
            
            seq_num = *(uint64_t*)buf;
            buf += 8;
            
            if (seq_num <= rep_entry->oob_num)
            {
                // slave needs to load/restore from a recent backup
                send_err_to(ws, entry_id, ERR_NEED_RESTORE_FROM_RECENT_BACKUP);
                continue;
            }
            
            uint64_t current_seq_num = rep_entry->reader_seq_num;
            const bool pending = seq_num > current_seq_num;
            
            auto& mapping_entry = reader_entry.mapping[rep_entry->idx];
            
            // subscribe/update seq_num
            auto slave_it = mapping_entry.slave_map.find(ws);
            
            if (slave_it == mapping_entry.slave_map.end())
            {
                // subscribe
                it->second.push_back(rep_entry->idx);
                mapping_entry.slave_map[ws] = { ws, seq_num, entry_id, pending, true };
                //std::cout << "client subscribed to " << tmp << " | " << seq_num << " | " << (int)entry_id << std::endl;
            }
            else
            {
                // update fields
                slave_it->second.seq_num = seq_num;
                slave_it->second.entry_id = entry_id;
                slave_it->second.pending = pending;
                slave_it->second.acked = true;
                
                //std::cout << "Acked: " << seq_num << std::endl;
            }
            
            if (pending)
            {
                // noop
            }
            else if (instance->master_batch_multi_frame)
            {
                //std::cout << "Sending seq_num: " << seq_num << " <= " << current_seq_num << std::endl;
                send_logs_to(ws, rep_entry, *mapping_entry.reader, seq_num, entry_id, threshold_bytes,
                        &reader_entry.scratch);
            }
            else
            {
                send_batch_logs_to(ws, rep_entry, *mapping_entry.reader, seq_num, entry_id, threshold_bytes, 
                        &reader_entry.scratch, &ws_batch);
            }
        }
    });
    
    // TODO remove this preprocessor when concurrent send_recent_logs_from becomes stable
    #if defined(REP_MT_TIMER) || defined(REP_RT)
    Timer* timer = new Timer(hub.getLoop());
    timer->start([](Timer *timer) {
        int id = (int)(intptr_t)timer->getData();
        auto reader_entry = protostuff::ReaderEntry::list[id - 1];
        if (instance->master_update_interval != ++reader_entry->update_ticks)
            return;
        
        #ifdef REP_MT_TIMER
        
        uint64_t updates = reader_entry->queued->load();
        if (updates != 0)
        {
            reader_entry->update_ticks = 0;
            send_recent_logs_from(*reader_entry, updates);
            // unset the flags
            reader_entry->queued->fetch_xor(updates);
        }
        else
        {
            // wait for next tick
            --reader_entry->update_ticks;
        }
        
        #else
        
        protostuff::RepEntry* rep_entry;
        uint64_t reader_seq_num;
        bool multi_frame = instance->master_batch_multi_frame;
        reader_entry->update_ticks = 0;
        for (auto& mapping_entry : reader_entry->mapping)
        {
            rep_entry = mapping_entry.rep_entry;
            reader_seq_num = rep_entry->reader_seq_num;
            for (auto& slave_it : mapping_entry.slave_map)
            {
                if (reader_seq_num >= slave_it.second.seq_num && (multi_frame || slave_it.second.acked))
                {
                    send_err_to((uWS::WebSocket<uWS::SERVER>*)slave_it.second.socket,
                            multi_frame ? 0xFF : slave_it.second.entry_id, 0);
                    
                    if (multi_frame)
                        break;
                    
                    slave_it.second.acked = false;
                }
            }
        }
        
        #endif
    }, 0, instance->timer_interval);
    timer->setData(ptr);
    #endif
    
    #endif
    
    hub.listen(instance->bind_ip, instance->port + id);
    hub.run();
}
#endif

#ifdef REP_SLAVE

#ifdef HYPER
static void ssr_backup(protostuff::RepEntry* rep_entry, const char* req_buf, const size_t req_buf_len)
{
    // go to len (flags is the first element)
    const char* buf = req_buf + 1;
    
    std::string backup;
    backup += "live/";
    backup.append(buf + 1, *(uint8_t*)buf);
    
    auto status = rep_entry->db->LiveBackup(backup);
    if (!status.ok())
        std::cerr << rep_entry->name << ": " << status.ToString() << std::endl;
}
#endif
static void send_req_to(uWS::WebSocket<uWS::CLIENT> *ws,
        const uint8_t batch_threshold_mb, const std::vector<protostuff::RepEntry*>& list)
{
    // subscribe
    char req_buf[1024]; // TODO enough?
    // skip type and count (handled at the bottom)
    char* buf = req_buf + 2;
    uint8_t name_size, count = 0;
    for (auto rep_entry : list)
    {
        rep_entry->queued = false;
        
        name_size = static_cast<uint8_t>(rep_entry->name.size());
        *buf++ = rep_entry->idx;
        
        *buf++ = name_size;
        memcpy(buf, rep_entry->name.data(), name_size);
        buf += name_size;
        
        memcpy(buf, &rep_entry->seq_num, 8);
        buf += 8;
        count++;
    }
    
    *req_buf = batch_threshold_mb;
    *(req_buf + 1) = count;
    ws->send(req_buf, buf - req_buf, uWS::OpCode::BINARY);
}

#endif

#ifdef REP_MASTER
/*static void send_prepared(protostuff::RepEntry* rep_entry, uWS::WebSocket<uWS::SERVER>::PreparedMessage* prepared)
{
    for (auto& slave_it : rep_entry->slave_map)
        ((uWS::WebSocket<uWS::SERVER>*)slave_it.second.socket)->sendPrepared(prepared);
    
    uWS::WebSocket<uWS::SERVER>::finalizeMessage(prepared);
}

static void send_pub_msgs(protostuff::RepEntry* rep_entry)
{
    if (!rep_entry->pub_msgs.empty())
    {
        for (auto ptr : rep_entry->pub_msgs)
            send_prepared(rep_entry, (uWS::WebSocket<uWS::SERVER>::PreparedMessage*)ptr);
        
        rep_entry->pub_msgs.clear();   
    }
}*/

#ifndef REP_RT
static void send_recent_logs()
{
    //std::cout << "New log updates" << std::endl;
    char seq_start[protostuff::SEQ_KEY_LEN];
    leveldb::Slice kseek(seq_start, protostuff::SEQ_KEY_LEN);
    leveldb::Slice record;
    leveldb::Slice val;
    
    leveldb::ReadOptions ro;
    ro.fill_cache = false;
    
    std::vector<protostuff::SlaveEntry> ws_batch;
    uint64_t seq_num;
    
    uint64_t abs_offset, expect_offset;
    uint32_t abs_size, wb_size = 0;
    size_t count = 0;
    char* buf_last;
    
    // initialize prefix of seq_start
    *seq_start = protostuff::SEQ_KEY_START;
    
    // prepare
    protostuff::RepEntry::scratch.clear();
    
    for (auto rep_entry : protostuff::RepEntry::queue)
    {
        rep_entry->queued = false;
        
        seq_num = rep_entry->rep_num;
        
        // move to the latest on next update
        rep_entry->rep_num = rep_entry->seq_num;
        
        // start at the least recent update
        if (1 == ++seq_num)
        {
            // a new slave that will receive the initial update right after it connects.
            // so skip this
            //send_pub_msgs(rep_entry);
            continue;
        }
        
        for (auto& slave_it : rep_entry->slave_map)
        {
            // include the least recent seq_num that has not been sent to the client yet
            if (seq_num == slave_it.second.seq_num && slave_it.second.pending)
                ws_batch.push_back(slave_it.second);
        }
        
        // skip if all behind
        if (0 == ws_batch.size())
        {
            //std::cout << "All behind on " << rep_entry->name << ": " << seq_num << std::endl;
            //send_pub_msgs(rep_entry);
            continue;
        }
        
        //std::cout << "preparing to send log updates to slaves starting at " << seq_num << std::endl;
        
        protostuff::encodeSeq(seq_start + 1, seq_num);
        
        auto it = rep_entry->db->NewIterator(ro);
        it->Seek(kseek);
        
        if (!it->Valid() || protostuff::END_KEY_LEN == it->key().size_)
        {
            // reached end
            // should not happen since rep_num < current seq_num
            std::cerr << "Illegal state on replication with seq_num: " << seq_num
                    << " <= " << rep_entry->seq_num << " (latest)" << std::endl;
            delete it;
            //send_pub_msgs(rep_entry);
            continue;
        }
        
        abs_offset = 0;
        abs_size = 0;
        do
        {
            val = it->value();
            
            expect_offset = abs_offset + abs_size;
            
            abs_offset = *(uint64_t*)val.data_;
            abs_size = *(uint32_t*)(val.data_ + 8);
            wb_size = *(uint32_t*)(val.data_ + 12);
            
            if (count == 0)
            {
                // initial, jump to offset
                rep_entry->reader.Reset(abs_offset);
                //std::cout << "jumped to offset: " << abs_offset << std::endl;
            }
            
            if (!rep_entry->reader.ReadRecord(&record, &protostuff::RepEntry::scratch))
            {
                std::cerr << "Could not read binlog at " << (seq_num + count) << std::endl;
                break;
            }
            
            if (count != 0 && expect_offset != abs_offset)
            {
                std::cout << rep_entry->name << ": seeking to offset " << abs_offset << std::endl;
                
                // jump to offset
                rep_entry->reader.Reset(abs_offset);
                
                if (!rep_entry->reader.ReadRecord(&record, &protostuff::RepEntry::scratch))
                {
                    std::cerr << "Could not read binlog at " << (seq_num + count) << std::endl;
                    break;
                }
            }
            
            if (wb_size != record.size_)
            {
                std::cerr << "Corrupt binlog at " << (seq_num + count) << std::endl;
                break;
            }
            
            buf_last = (char*)(record.data_ + record.size_ - 1);
            for (const auto& slave_entry : ws_batch)
            {
                 *buf_last = slave_entry.entry_id;
                 ((uWS::WebSocket<uWS::SERVER>*)slave_entry.socket)->send(record.data_, record.size_, uWS::OpCode::BINARY);
            }
            
            /*if (1 == ws_batch.size())
            {
                *buf_last = ws_batch[0].entry_id;
                ((uWS::WebSocket<uWS::SERVER>*)ws_batch[0].socket)->send(record.data_, record.size_, uWS::OpCode::BINARY);
            }
            else
            {
                *buf_last = ws_batch[0].entry_id;
                
                // data not modified (uws api need to add const modifier)
                auto prepared = uWS::WebSocket<uWS::SERVER>::prepareMessage(
                        (char*)record.data_, record.size_, uWS::OpCode::BINARY, false);
                
                for (const auto& slave_entry : ws_batch)
                    ((uWS::WebSocket<uWS::SERVER>*)slave_entry.socket)->sendPrepared(prepared);
                
                uWS::WebSocket<uWS::SERVER>::finalizeMessage(prepared);
            }*/
            
            it->Next();
            count++;
        }
        while (it->Valid() && protostuff::END_KEY_LEN != it->key().size_);
        
        delete it;
        ws_batch.clear();
        count = 0;
        //send_pub_msgs(rep_entry);
    }
    protostuff::RepEntry::queue.clear();
    protostuff::RepEntry::scratch.clear();
    
    /*auto group = instance->hub->getDefaultGroup<uWS::SERVER>();
    for (auto entry : protostuff::RepEntry::queue)
    {
        for (auto wb : entry->wb_list)
        {
            group.broadcast(wb->rep_.data(), wb->rep_.size(), uWS::OpCode::BINARY);
            delete wb;
        }
        entry->queued = false;
        entry->wb_list.clear();
    }*/
}
#endif

#if defined(REP_MT) && defined(REP_MT_TIMER)
static void send_recent_logs_from(protostuff::ReaderEntry& reader_entry, uint64_t updates)
{
    char seq_start[protostuff::SEQ_KEY_LEN];
    leveldb::Slice kseek(seq_start, protostuff::SEQ_KEY_LEN);
    leveldb::Slice record;
    leveldb::Slice val;
    
    leveldb::ReadOptions ro;
    ro.fill_cache = false;
    
    std::vector<protostuff::SlaveEntry> ws_batch;
    uint64_t seq_num;
    
    uint64_t abs_offset, expect_offset;
    uint32_t abs_size, wb_size = 0;
    size_t count = 0;
    char* buf_last;
    
    // initialize prefix of seq_start
    *seq_start = protostuff::SEQ_KEY_START;
    
    // prepare
    reader_entry.scratch.clear();
    
    protostuff::RepEntry* rep_entry;
    uint64_t flag, reader_seq_num;
    for (auto& mapping_entry : reader_entry.mapping)
    {
        flag = 1 << mapping_entry.idx;
        if (0 == (updates & flag))
            continue;
        
        rep_entry = mapping_entry.rep_entry;
        
        reader_seq_num = rep_entry->reader_seq_num;
        
        seq_num = mapping_entry.rep_num;
        // move to the latest on next update
        mapping_entry.rep_num = reader_seq_num;
        
        if (0 == seq_num)
        {
            // first broadcast
            if (1 == reader_seq_num)
            {
                // a new slave that will receive the initial update right after it connects.
                continue;
            }
            seq_num = reader_seq_num;
        }
        // start at the least recent update
        else if (1 == ++seq_num)
        {
            // a new slave that will receive the initial update right after it connects.
            // so skip this
            continue;
        }
        
        for (auto& slave_it : mapping_entry.slave_map)
        {
            // include the least recent seq_num that has not been sent to the client yet
            if (seq_num == slave_it.second.seq_num && slave_it.second.pending)
                ws_batch.push_back(slave_it.second);
        }
        
        // skip if all behind
        if (0 == ws_batch.size())
        {
            //std::cout << "All behind on " << rep_entry->name << ": " << seq_num << std::endl;
            continue;
        }
        
        //std::cout << "preparing to send log updates to slaves starting at " << seq_num << std::endl;
        
        protostuff::encodeSeq(seq_start + 1, seq_num);
        
        auto it = rep_entry->db->NewIterator(ro);
        it->Seek(kseek);
        
        if (!it->Valid() || protostuff::END_KEY_LEN == it->key().size_)
        {
            // reached end
            // should not happen since rep_num < current seq_num
            std::cerr << "Illegal state on replication with seq_num: " << seq_num
                    << " <= " << reader_seq_num << " (latest)" << std::endl;
            delete it;
            continue;
        }
        
        abs_offset = 0;
        abs_size = 0;
        do
        {
            val = it->value();
            
            expect_offset = abs_offset + abs_size;
            
            abs_offset = *(uint64_t*)val.data_;
            abs_size = *(uint32_t*)(val.data_ + 8);
            wb_size = *(uint32_t*)(val.data_ + 12);
            
            if (count == 0)
            {
                // initial, jump to offset
                mapping_entry.reader->Reset(abs_offset);
                //std::cout << "jumped to offset: " << abs_offset << std::endl;
            }
            
            if (!mapping_entry.reader->ReadRecord(&record, &protostuff::RepEntry::scratch))
            {
                std::cerr << "Could not read binlog at " << (seq_num + count) << std::endl;
                break;
            }
            
            if (count != 0 && expect_offset != abs_offset)
            {
                std::cout << rep_entry->name << ": seeking to offset " << abs_offset << std::endl;
                
                // jump to offset
                mapping_entry.reader->Reset(abs_offset);
                
                if (!mapping_entry.reader->ReadRecord(&record, &protostuff::RepEntry::scratch))
                {
                    std::cerr << "Could not read binlog at " << (seq_num + count) << std::endl;
                    break;
                }
            }
            
            if (wb_size != record.size_)
            {
                std::cerr << "Corrupt binlog at " << (seq_num + count) << std::endl;
                break;
            }
            
            buf_last = (char*)(record.data_ + record.size_ - 1);
            for (const auto& slave_entry : ws_batch)
            {
                 *buf_last = slave_entry.entry_id;
                 ((uWS::WebSocket<uWS::SERVER>*)slave_entry.socket)->send(record.data_, record.size_, uWS::OpCode::BINARY);
            }
            
            it->Next();
            count++;
        }
        while (it->Valid() && protostuff::END_KEY_LEN != it->key().size_);
        
        delete it;
        ws_batch.clear();
        count = 0;
    }
    reader_entry.scratch.clear();
}
#endif

static void write_errmsg_to(uWS::WebSocket<uWS::SERVER> *ws,
        const uint8_t entry_id,
        const leveldb::Slice& msg)
{
    char res_buf[1 + 1 + 1 + 255]; // entry_id, err, msg_len, msg
    char* buf = res_buf;
    
    *buf++ = entry_id;
    *buf++ = 1;
    // write errmsg
    const char* msg_data = msg.data_;
    auto msg_size = msg.size_;
    bool truncated = false;
    if (msg_size == 0)
    {
        msg_data = "operation failed.";
        msg_size = sizeof("operation failed.") - 1;
    }
    else if (msg_size > 255)
    {
        truncated = true;
        msg_size = 255;
    }
    
    auto msg_len = static_cast<uint8_t>(msg_size);
    *buf++ = msg_len;
    
    memcpy(buf, msg_data, msg_len);
    buf += msg_len;
    
    if (truncated)
    {
        *(buf - 1) = '.';
        *(buf - 2) = '.';
        *(buf - 3) = '.';
        *(buf - 4) = ' ';
    }
    
    ws->send(res_buf, buf - res_buf, uWS::OpCode::BINARY);
}

static void write_ok_to(uWS::WebSocket<uWS::SERVER> *ws,
        const uint8_t entry_id)
{
    char res_buf[2]; // entry_id, status
    res_buf[0] = entry_id;
    res_buf[1] = 0;
    ws->send(res_buf, 2, uWS::OpCode::BINARY);
}

static void write_status_to(uWS::WebSocket<uWS::SERVER> *ws,
        const uint8_t entry_id,
        leveldb::Status status)
{
    if (status.ok())
        write_ok_to(ws, entry_id);
    else
        write_errmsg_to(ws, entry_id, leveldb::Slice(status.ToString()));
}

#ifdef HYPER
static void handle_backup(uWS::WebSocket<uWS::SERVER> *ws,
        const protostuff::RepEntry* rep_entry, const uint8_t entry_id,
        const char* backup_name, const uint8_t backup_name_len)
{
    std::string backup;
    backup += "live/";
    backup.append(backup_name, backup_name_len);
    
    write_status_to(ws, entry_id, rep_entry->db->LiveBackup(backup));
}
#endif

static int handle_ws_req(uWS::WebSocket<uWS::SERVER> *ws,
        const protostuff::RepEntry* rep_entry,
        const std::unordered_map<void*, protostuff::SlaveEntry>* slave_map,
        const uint8_t entry_id,
        char* buf)
{
    char* const start_buf = buf;
    uint8_t flags, len;
    uint16_t trailer;
    
    uint8_t type = *buf++;
    switch (type)
    {
        case REQ_BACKUP:
            flags = *buf++;
            len = *buf++;
            if (rep_entry == nullptr)
            {
                write_errmsg_to(ws, entry_id, leveldb::Slice("db not found."));
            }
            else if (flags == 0)
            {
                #ifndef HYPER
                write_errmsg_to(ws, entry_id, leveldb::Slice("only backup on slaves are supported."));
                #else
                handle_backup(ws, rep_entry, entry_id, buf, len);
                #endif
            }
            else if (slave_map->empty())
            {
                write_errmsg_to(ws, entry_id, leveldb::Slice("no slaves connected."));
            }
            #ifndef HYPER
            else if (flags & 2)
            {
                write_errmsg_to(ws, entry_id, leveldb::Slice("only backup on slaves are supported."));
            }
            else
            {
                // forward to slaves
                trailer = *(uint16_t*)(buf + len); // dump
                *(buf + len + 1) = 0xFF; // overwrite
                
                for (auto& slave_it : *slave_map)
                {
                    *(buf + len) = slave_it.second.entry_id; // overwrite
                    
                    ((uWS::WebSocket<uWS::SERVER>*)slave_it.second.socket)->send(
                            start_buf, (buf - start_buf) + len + 2, uWS::OpCode::BINARY);
                }
                
                *(uint16_t*)(buf + len) = trailer; // restore
                
                write_ok_to(ws, entry_id);
            }
            #else
            else
            {
                if (flags & 1)
                {
                    // forward to slaves
                    trailer = *(uint16_t*)(buf + len); // dump
                    *(buf + len + 1) = 0xFF; // overwrite
                    
                    for (auto& slave_it : *slave_map)
                    {
                        *(buf + len) = slave_it.second.entry_id; // overwrite
                        
                        ((uWS::WebSocket<uWS::SERVER>*)slave_it.second.socket)->send(
                                start_buf, (buf - start_buf) + len + 2, uWS::OpCode::BINARY);
                    }
                    
                    *(uint16_t*)(buf + len) = trailer; // restore
                }
                
                if (flags & 2)
                    handle_backup(ws, rep_entry, entry_id, buf, len);
                else
                    write_ok_to(ws, entry_id);
            }
            #endif
            
            buf += len;
            break;
    }
    
    return buf - start_buf;
}

static void send_err_to(uWS::WebSocket<uWS::SERVER> *ws,
        const uint8_t entry_id,
        const uint8_t err)
{
    char buf[4]; // type, err, entry_id, 0xFF
    
    buf[0] = REQ_REP;
    buf[1] = err;
    buf[2] = entry_id;
    buf[3] = 0xFF;
    
    ws->send(buf, 4, uWS::OpCode::BINARY);
}

static void send_batch_logs_to(uWS::WebSocket<uWS::SERVER> *ws,
        const protostuff::RepEntry* rep_entry, leveldb::log::Reader& reader,
        const uint64_t seq_num, const uint8_t entry_id,
        const size_t threshold_bytes,
        std::string* scratch,
        std::string* ws_batch)
{
    //std::cout << "sending logs starting at " << seq_num << " | " << (int)entry_id << std::endl;
    char seq_start[protostuff::SEQ_KEY_LEN];
    
    leveldb::Slice kseek(seq_start,  protostuff::SEQ_KEY_LEN);
    
    leveldb::Slice record;
    leveldb::Slice val;
    
    leveldb::ReadOptions ro;
    ro.fill_cache = false;
    
    uint16_t header = 0;
    uint64_t abs_offset = 0, expect_offset;
    uint32_t abs_size = 0, wb_size = 0;
    size_t count = 0;
    
    *seq_start = protostuff::SEQ_KEY_START;
    protostuff::encodeSeq(seq_start + 1, seq_num);
    
    //print_bytes(std::cout, "seq_start", (const unsigned char*)seq_start, protostuff::SEQ_KEY_LEN);
    
    auto it = rep_entry->db->NewIterator(ro);
    it->Seek(kseek);
    
    if (!it->Valid() || protostuff::END_KEY_LEN == it->key().size_)
    {
        // reached end
        delete it;
        return;
    }
    
    // reset and fill with 3 zeroes
    ws_batch->assign((char*)&wb_size, 3);
    
    do
    {
        val = it->value();
        
        expect_offset = abs_offset + abs_size;
        
        abs_offset = *(uint64_t*)val.data_;
        abs_size = *(uint32_t*)(val.data_ + 8);
        wb_size = *(uint32_t*)(val.data_ + 12);
        
        //std::cout << "abs_offset: " << abs_offset << std::endl;
        
        if (count == 0)
        {
            // initial, jump to offset
            reader.Reset(abs_offset);
            //std::cout << "jumped to offset: " << abs_offset << std::endl;
        }
        
        if (!reader.ReadRecord(&record, scratch))
        {
            std::cerr << rep_entry->name << ": could not read binlog at " << (seq_num + count) << std::endl;
            break;
        }
        
        if (count != 0 && expect_offset != abs_offset)
        {
            std::cout << rep_entry->name << ": seeking to offset " << abs_offset << std::endl;
            
            // jump to offset
            reader.Reset(abs_offset);
            
            if (!reader.ReadRecord(&record, scratch))
            {
                std::cerr << rep_entry->name << ": could not read binlog at " << (seq_num + count) << std::endl;
                break;
            }
        }
        
        if (wb_size != record.size_)
        {
            std::cerr << rep_entry->name << ": corrupt binlog at " << (seq_num + count) << std::endl;
            break;
        }
        
        ws_batch->append((char*)&wb_size, 4);
        ws_batch->append(record.data_, record.size_);
        
        if (++count == 65535 || ws_batch->size() > threshold_bytes)
        {
            //std::cout << rep_entry->name << ": sent 1mb to slave starting at " << seq_num << std::endl;
            break;
        }
        
        it->Next();
    }
    while (it->Valid() && protostuff::END_KEY_LEN != it->key().size_);
    
    delete it;
    
    if (count != 0)
    {
        ws_batch->push_back(entry_id);
        ws_batch->push_back(0xFF);
        header = static_cast<uint16_t>(count);
        write16((char*)(ws_batch->data() + 1), header);
        ws->send(ws_batch->data(), ws_batch->size(), uWS::OpCode::BINARY);
    }
}

static void send_logs_to(uWS::WebSocket<uWS::SERVER> *ws,
        const protostuff::RepEntry* rep_entry, leveldb::log::Reader& reader,
        const uint64_t seq_num, const uint8_t entry_id,
        const size_t threshold_bytes,
        std::string* scratch)
{
    //std::cout << "sending logs starting at " << seq_num << " | " << (int)entry_id << std::endl;
    char seq_start[protostuff::SEQ_KEY_LEN];
    
    leveldb::Slice kseek(seq_start,  protostuff::SEQ_KEY_LEN);
    
    leveldb::Slice record;
    leveldb::Slice val;
    
    leveldb::ReadOptions ro;
    ro.fill_cache = false;
    
    uint64_t abs_offset = 0, expect_offset;
    uint32_t abs_size = 0, wb_size = 0;
    size_t count = 0, bytes_sent = 0;
    char* buf_last;
    
    *seq_start = protostuff::SEQ_KEY_START;
    protostuff::encodeSeq(seq_start + 1, seq_num);
    
    //print_bytes(std::cout, "seq_start", (const unsigned char*)seq_start, protostuff::SEQ_KEY_LEN);
    
    auto it = rep_entry->db->NewIterator(ro);
    it->Seek(kseek);
    
    if (!it->Valid() || protostuff::END_KEY_LEN == it->key().size_)
    {
        // reached end
        delete it;
        return;
    }
    
    do
    {
        val = it->value();
        
        expect_offset = abs_offset + abs_size;
        
        abs_offset = *(uint64_t*)val.data_;
        abs_size = *(uint32_t*)(val.data_ + 8);
        wb_size = *(uint32_t*)(val.data_ + 12);
        
        //std::cout << "abs_offset: " << abs_offset << std::endl;
        
        if (count == 0)
        {
            // initial, jump to offset
            reader.Reset(abs_offset);
            //std::cout << "jumped to offset: " << abs_offset << std::endl;
        }
        
        if (!reader.ReadRecord(&record, scratch))
        {
            std::cerr << rep_entry->name << ": could not read binlog at " << (seq_num + count) << std::endl;
            break;
        }
        
        if (count != 0 && expect_offset != abs_offset)
        {
            std::cout << rep_entry->name << ": seeking to offset " << abs_offset << std::endl;
            
            // jump to offset
            reader.Reset(abs_offset);
            
            if (!reader.ReadRecord(&record, scratch))
            {
                std::cerr << rep_entry->name << ": could not read binlog at " << (seq_num + count) << std::endl;
                break;
            }
        }
        
        if (wb_size != record.size_)
        {
            std::cerr << rep_entry->name << ": corrupt binlog at " << (seq_num + count) << std::endl;
            break;
        }
        
        // write entry_id
        buf_last = (char*)(record.data_ + record.size_ - 1);
        *buf_last = entry_id;
        ws->send(record.data_, record.size_, uWS::OpCode::BINARY);
        
        count++;
        bytes_sent += record.size_;
        if (bytes_sent > threshold_bytes)
        {
            //std::cout << rep_entry->name << ": sent 1mb to slave starting at " << seq_num << std::endl;
            break;
        }
        
        it->Next();
    }
    while (it->Valid() && protostuff::END_KEY_LEN != it->key().size_);
    
    delete it;
}
#endif

// -Des.
static const char ARG_TOKEN_AS_USER[] = "token_as_user=true";
static const char ARG_KEY[] = "key=";
static const char ARG_IV[] = "iv=";
static const char ARG_EXPIRES[] = "expires=";

// -Dprotostuffdb.
static const char ARG_CHECKSUM[] = "checksum=true";
static const char ARG_WRITE_BUFFER_MB[] = "write_buffer_mb=";
static const char ARG_KEEPALIVE_TIMEOUT_START[] = "keepalive_timeout_start=";
static const char ARG_KEEPALIVE_TIMEOUT_INTERVAL[] = "keepalive_timeout_interval=";
static const char ARG_WITH_PUBSUB[] = "with_pubsub=true";
static const char ARG_AUTO_PING[] = "auto_ping=";
static const char ARG_ASSETS_DIR[] = "assets_dir=";
static const char ARG_RT_FLAGS[] = "rt_flags=";

#if defined(REP_MASTER) || defined(REP_SLAVE)
static const char ARG_TIMER_INTERVAL[] = "timer_interval=";
//static const char ARG_REP_FLAGS[] = "rep_flags=";
static const char ARG_REP_BATCH_THRESHOLD_MB[] = "rep_batch_threshold_mb=";
#endif
#ifdef REP_SLAVE
static const char ARG_MASTER[] = "master=";
static const char ARG_REP_IDLE_REFRESH_INTERVAL[] = "rep_idle_refresh_interval=";
static const char ARG_REP_ACK_INTERVAL[] = "rep_ack_interval=";
#endif
#ifdef REP_MASTER
static const char ARG_REP_UPDATE_INTERVAL[] = "rep_update_interval=";
static const char ARG_REP_BATCH_MULTI_FRAME[] = "rep_batch_multi_frame=true";
#endif
#ifdef BENCH
static const char ARG_ITERATIONS[] = "iterations=";
static const char ARG_WARMUPS[] = "warmups=";
static const char ARG_URI[] = "uri=";
static const char ARG_PAYLOAD[] = "payload=";
#endif

static bool check_jvm_args(MyApplication& app, std::string& arg)
{
    // null-terminate
    arg.c_str();
    
    int pfx_len, sfx_len;
    const int len = arg.size();
    
    if (starts_with(arg, pfx_es))
    {
        pfx_len = pfx_es.size();
        sfx_len = len - pfx_len;
        if (sfx_len == sizeof(ARG_TOKEN_AS_USER) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_TOKEN_AS_USER) - 1, ARG_TOKEN_AS_USER))
        {
            app.token_as_user = true;
        }
        else if (sfx_len > sizeof(ARG_KEY) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_KEY) - 1, ARG_KEY))
        {
            pfx_len += sizeof(ARG_KEY) - 1;
            sfx_len = len - pfx_len;
            if (sfx_len != 32)
            {
                fprintf(stderr, "es.key must be exactly 32 bytes long.\n");
                return false;
            }
            app.encrypted_session_key = arg.data() + pfx_len;
            app.encrypted_session_key_len = sfx_len;
        }
        else if (sfx_len > sizeof(ARG_IV) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_IV) - 1, ARG_IV))
        {
            pfx_len += sizeof(ARG_IV) - 1;
            sfx_len = len - pfx_len;
            if (sfx_len != EVP_MAX_IV_LENGTH)
            {
                fprintf(stderr, "es.iv must be exactly %d bytes long.\n", EVP_MAX_IV_LENGTH);
                return false;
            }
            app.encrypted_session_iv = arg.data() + pfx_len;
            app.encrypted_session_iv_len = sfx_len;
        }
        else if (sfx_len > sizeof(ARG_EXPIRES) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_EXPIRES) - 1, ARG_EXPIRES))
        {
            pfx_len += sizeof(ARG_EXPIRES) - 1;
            app.encrypted_session_expires = static_cast<int64_t>(std::stoll(arg.data() + pfx_len));
        }
    }
    else if (starts_with(arg, pfx_protostuffdb))
    {
        pfx_len = pfx_protostuffdb.size();
        sfx_len = len - pfx_len;
        if (sfx_len == sizeof(ARG_CHECKSUM) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_CHECKSUM) - 1, ARG_CHECKSUM))
        {
            protostuff::flags |= protostuff::FGLOBAL_CHECKSUM;
        }
        else if (sfx_len > sizeof(ARG_WRITE_BUFFER_MB) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_WRITE_BUFFER_MB) - 1, ARG_WRITE_BUFFER_MB))
        {
            pfx_len += sizeof(ARG_WRITE_BUFFER_MB) - 1;
            app.write_buffer_mb = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len > sizeof(ARG_KEEPALIVE_TIMEOUT_START) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_KEEPALIVE_TIMEOUT_START) - 1, ARG_KEEPALIVE_TIMEOUT_START))
        {
            pfx_len += sizeof(ARG_KEEPALIVE_TIMEOUT_START) - 1;
            app.keepalive_timeout_start = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len > sizeof(ARG_KEEPALIVE_TIMEOUT_INTERVAL) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_KEEPALIVE_TIMEOUT_INTERVAL) - 1, ARG_KEEPALIVE_TIMEOUT_INTERVAL))
        {
            pfx_len += sizeof(ARG_KEEPALIVE_TIMEOUT_INTERVAL) - 1;
            app.keepalive_timeout_interval = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len == sizeof(ARG_WITH_PUBSUB) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_WITH_PUBSUB) - 1, ARG_WITH_PUBSUB))
        {
            app.pubsub = true;
        }
        else if (sfx_len > sizeof(ARG_AUTO_PING) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_AUTO_PING) - 1, ARG_AUTO_PING))
        {
            pfx_len += sizeof(ARG_AUTO_PING) - 1;
            app.auto_ping = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len > sizeof(ARG_ASSETS_DIR) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_ASSETS_DIR) - 1, ARG_ASSETS_DIR))
        {
            pfx_len += sizeof(ARG_ASSETS_DIR) - 1;
            app.assets_dir = arg.substr(pfx_len, len - pfx_len - static_cast<uint8_t>(app.assets_preloaded = '/' == arg.back()));
        }
        else if (sfx_len > sizeof(ARG_RT_FLAGS) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_RT_FLAGS) - 1, ARG_RT_FLAGS))
        {
            pfx_len += sizeof(ARG_RT_FLAGS) - 1;
            app.rt_flags |= static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
        }
        #if defined(REP_MASTER) || defined(REP_SLAVE)
        else if (sfx_len > sizeof(ARG_TIMER_INTERVAL) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_TIMER_INTERVAL) - 1, ARG_TIMER_INTERVAL))
        {
            pfx_len += sizeof(ARG_TIMER_INTERVAL) - 1;
            app.timer_interval = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len > sizeof(ARG_REP_BATCH_THRESHOLD_MB) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_BATCH_THRESHOLD_MB) - 1, ARG_REP_BATCH_THRESHOLD_MB))
        {
            pfx_len += sizeof(ARG_REP_BATCH_THRESHOLD_MB) - 1;
            #ifdef REP_SLAVE
            app.slave_batch_threshold_mb = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
            if (app.slave_batch_threshold_mb == 0)
                app.slave_batch_threshold_mb = 0xFF; // restore to default
            #else
            app.master_batch_threshold_mb = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
            if (app.master_batch_threshold_mb == 0)
                app.master_batch_threshold_mb = 1; // restore to default
            #endif
        }
        #endif
        #ifdef REP_SLAVE
        /*else if (sfx_len > sizeof(ARG_REP_FLAGS) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_FLAGS) - 1, ARG_REP_FLAGS))
        {
            pfx_len += sizeof(ARG_REP_FLAGS) - 1;
            app.slave_flags = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
        }*/
        else if (sfx_len > sizeof(ARG_MASTER) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_MASTER) - 1, ARG_MASTER))
        {
            pfx_len += sizeof(ARG_MASTER) - 1;
            app.slave_master = arg.substr(pfx_len);
        }
        else if (sfx_len > sizeof(ARG_REP_IDLE_REFRESH_INTERVAL) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_IDLE_REFRESH_INTERVAL) - 1, ARG_REP_IDLE_REFRESH_INTERVAL))
        {
            pfx_len += sizeof(ARG_REP_IDLE_REFRESH_INTERVAL) - 1;
            app.slave_idle_refresh_interval = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
        }
        else if (sfx_len > sizeof(ARG_REP_ACK_INTERVAL) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_ACK_INTERVAL) - 1, ARG_REP_ACK_INTERVAL))
        {
            pfx_len += sizeof(ARG_REP_ACK_INTERVAL) - 1;
            app.slave_ack_interval = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
        }
        #endif
        #ifdef REP_MASTER
        /*else if (sfx_len > sizeof(ARG_REP_FLAGS) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_FLAGS) - 1, ARG_REP_FLAGS))
        {
            pfx_len += sizeof(ARG_REP_FLAGS) - 1;
            app.master_flags = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
        }*/
        else if (sfx_len > sizeof(ARG_REP_UPDATE_INTERVAL) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_UPDATE_INTERVAL) - 1, ARG_REP_UPDATE_INTERVAL))
        {
            pfx_len += sizeof(ARG_REP_UPDATE_INTERVAL) - 1;
            app.master_update_interval = static_cast<uint8_t>(std::atoi(arg.data() + pfx_len));
        }
        else if (sfx_len == sizeof(ARG_REP_BATCH_MULTI_FRAME) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_REP_BATCH_MULTI_FRAME) - 1, ARG_REP_BATCH_MULTI_FRAME))
        {
            app.master_batch_multi_frame = true;
        }
        #endif
    }
    #ifdef BENCH
    else if (starts_with(arg, pfx_bench))
    {
        pfx_len = pfx_bench.size();
        sfx_len = len - pfx_len;
        if (sfx_len > sizeof(ARG_ITERATIONS) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_ITERATIONS) - 1, ARG_ITERATIONS))
        {
            pfx_len += sizeof(ARG_ITERATIONS) - 1;
            app.iterations = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len > sizeof(ARG_WARMUPS) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_WARMUPS) - 1, ARG_WARMUPS))
        {
            pfx_len += sizeof(ARG_WARMUPS) - 1;
            app.warmups = std::atoi(arg.data() + pfx_len);
        }
        else if (sfx_len > sizeof(ARG_URI) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_URI) - 1, ARG_URI))
        {
            pfx_len += sizeof(ARG_URI) - 1;
            app.uri = arg.substr(pfx_len);
        }
        else if (sfx_len > sizeof(ARG_PAYLOAD) - 1 &&
                0 == arg.compare(pfx_len, sizeof(ARG_PAYLOAD) - 1, ARG_PAYLOAD))
        {
            pfx_len += sizeof(ARG_PAYLOAD) - 1;
            app.payload = arg.substr(pfx_len);
        }
    }
    #endif
    
    return true;
}

int main(int argc, char *argv[])
{
    if (argc == 2 && 0 == strcmp(argv[1], "--version"))
    {
        printf("%s\n", PROTOSTUFFDB_VERSION);
        return 0;
    }
    
    if (argc <= 4)
    {
        usage(argc, argv);
        return 1;
    }
    
    int port;
    char* bind_ip = nullptr;
    char* arg = argv[1];
    char* colon = strchr(arg, ':');
    if (colon != nullptr && ':' != *arg) // must not be first character
    {
        bind_ip = arg;
        // arg assigned as bind_ip, so we null terminate the end (which is where the colon is)
        *(colon++) = '\0';
        arg = colon;
    }
    if (0 == (port = std::atoi(arg)))
    {
        usage(argc, argv);
        return 1;
    }
    
    std::ifstream ij(argv[2]);
    json j;
    ij >> j;
    
    MyApplication app(bind_ip ? bind_ip : "*", port, j.get<std::map<std::string, srv::Entry>>());
    
    app.other_args.push_back("-XX:PretenureSizeThreshold=65534");
    app.other_args.push_back("-XX:+UseConcMarkSweepGC");
    
    // start inspecting other args at 3
    // 1st jvm arg triggers whether to use tlab or not
    if (0 == strcmp("-Dprotostuffdb.tlab=true", argv[3]))
    {
        app.other_args.push_back("-XX:MinTLABSize=65534");
        app.other_args.push_back("-XX:TLABSize=65534");
    }
    else
    {
        app.other_args.push_back("-XX:-UseTLAB");
    }
    
    for (int i = 3, offset = app.other_args.size(); i < argc; i++, offset++)
    {
        app.other_args.push_back(argv[i]);
        if (!check_jvm_args(app, app.other_args[offset]))
            return 1;
    }
    
    if (app.write_buffer_mb != 0)
    {
        if (app.write_buffer_mb < 4 || app.write_buffer_mb > 128)
        {
            std::cerr << "write_buffer_mb should be between 4-128.\n";
            return 1;
        }
        
        protostuff::write_buffer_mb = app.write_buffer_mb;
    }
    
    #if defined(REP_MASTER) || defined(REP_SLAVE)
    // TODO if assets_dir is provided and multiple readers, ensure that the assets are preloaded.
    
    if (app.encrypted_session_key_len == 0)
    {
        std::cerr << "Required arg: -Des.key=<32 chars long>\n";
        return 1;
    }
    #endif
    
    #ifdef REP_SLAVE
    if (app.slave_master.size() == 0)
    {
        std::cerr << "Required arg: -Dprotostuffdb.master=ws://<ip>:<port>\n";
        return 1;
    }
    app.slave_connect_url += app.slave_master;
    app.slave_connect_url += '/';
    app.slave_connect_url.append(app.encrypted_session_key, app.encrypted_session_key_len);
    #endif
    
    // init base32
    for (int8_t i = 0; i < 32; i++)
        basis32[B32_ALPHABET[i]] = (char)i;
    
    return app.run(argc, argv, 3);
}

#ifdef BENCH
// https://stackoverflow.com/a/15514271
std::string commify(unsigned long long n)
{
    std::string s;
    int cnt = 0;
    do
    {
        s.insert(0, 1, char('0' + n % 10));
        n /= 10;
        if (++cnt == 3 && n)
        {
            s.insert(0, 1, ',');
            cnt = 0;
        }
    }
    while (n);
    
    return s;
}
static void serve_http(void* ptr, JniContext* jni)
{
    if (instance->uri.empty())
    {
        std::cerr << "Missing param: -Dbench.uri (e.g. -Dbench.uri=/todo/user/Todo/create)" << std::endl;
        exit(1);
        return;
    }
    auto it = instance->entries.find(instance->uri);
    if (it == instance->entries.end())
    {
        std::cerr << "Unknown uri: " << instance->uri << std::endl;
        exit(1);
        return;
    }
    BufAndLen bl;
    auto env = leveldb::Env::Default();
    const auto& p = it->second.p;
    const uint8_t flags = it->second.f;
    const bool needs_payload = 0 == (srv::VOID_ARG & flags);
    if (!needs_payload)
    {
        bl.buf = nullptr;
        bl.len = 0;
    }
    else if (instance->payload.empty())
    {
        std::cerr << "Missing param: -Dbench.payload (e.g. -Dbench.uri=path/to/payload.json)" << std::endl;
        exit(1);
        return;
    }
    else if (!env->FileExists(instance->payload))
    {
        std::cerr << "Make sure the payload param exists in the filesystem." << std::endl;
        exit(1);
        return;
    }
    else
    {
        bl.buf = buffer_file(instance->payload.c_str(), &bl.len);
        if (bl.len > (65535 - 4 - p.length()))
        {
            std::cerr << "The payload cannot exceed " << static_cast<int>(65535 - 4 - p.length()) << " bytes." << std::endl;
            exit(1);
            return;
        }
    }
    
    setupThread(jni, 0, 0);
    
    char* buf = jni->buf;
    write(buf, p.data(), p.length(), bl.buf, bl.len);
    jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
    
    auto size = read32(buf);
    if ('[' != *(buf + 4) || '0' != *(buf + 5))
    {
        std::cerr << "rpc error: " << std::string(buf + 4, size) << std::endl;
        exit(1);
        return;
    }
    
    for (int i = 0, l = instance->warmups; i < l; i++)
    {
        write(buf, p.data(), p.length(), bl.buf, bl.len);
        jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
        if ('[' != *(buf + 4) || '0' != *(buf + 5))
        {
            std::cerr << "rpc error during warmup: " << std::string(buf + 4, read32(buf)) << std::endl;
            exit(1);
            return;
        }
    }
    
    auto start = std::chrono::steady_clock::now();
    for (int i = 0, l = instance->iterations; i < l; i++)
    {
        write(buf, p.data(), p.length(), bl.buf, bl.len);
        jni->env->CallStaticVoidMethod(jni->class_, jni->handle_, jni->type, jni->id);
        if ('[' != *(buf + 4) || '0' != *(buf + 5))
        {
            std::cerr << "rpc error during run: " << std::string(buf + 4, read32(buf)) << std::endl;
            exit(1);
            return;
        }
    }
    auto end = std::chrono::steady_clock::now();
    auto diff = end - start;
    auto ms = std::chrono::duration<double, std::milli>(diff).count();
    auto ops_per_sec = static_cast<double>(instance->iterations) / (ms / 1000);
    std::cout << "elapsed ms: " << commify(ms) << " | ops/sec: " << commify(ops_per_sec)
            << " | response body bytes: " << commify(size + 1) << std::endl;
    exit(0);
}
#endif
