#include <iostream>
#include <uWS.h>

extern "C" {
#include <sys/stat.h>
}

static void usage(int argc, char *argv[])
{
    std::cerr << "Usage:\n"
        << argv[0]
        << " (bind_ip:)port connect_url reconnect_interval(ms) [ certs_dir ]" << std::endl;
}

static bool exists(const char *path)
{
    struct stat b;
    return 0 == stat(path, &b);
}

static bool get_ip_port(char* arg, char** bind_ip, int* port)
{
    char* colon = strchr(arg, ':');
    if (colon != nullptr && ':' != *arg) // must not be first character
    {
        *bind_ip = arg;
        // arg assigned as bind_ip, so we null terminate the end (which is where the colon is)
        *(colon++) = '\0';
        arg = colon;
    }
    
    return 0 != (*port = std::atoi(arg));
}

static bool listen(uWS::Hub& h, const char* bind_ip, int port, const char* cert_dir)
{
    if (cert_dir != nullptr)
    {
        std::string dir = cert_dir;
        std::string crt_file = dir + "/cert_chain.crt";
        std::string key_file = dir + "/private.key";
        if (!exists(crt_file.c_str()))
        {
            std::cerr << crt_file << " does not exist." << std::endl;
            return false;
        }
        if (!exists(key_file.c_str()))
        {
            std::cerr << key_file << " does not exist." << std::endl;
            return false;
        }
        if (!h.listen(bind_ip, port, uS::TLS::createContext(crt_file.c_str(), key_file.c_str(), "1234")))
        {
            std::cerr << "Could not bind to " << bind_ip << std::endl;
            return false;
        }
    }
    else if (!h.listen(bind_ip, port))
    {
        std::cerr << "Could not bind to " << bind_ip << std::endl;
        return false;
    }
    
    return true;
}

static uWS::Hub* instance;
static char* connect_url;
static int reconnect_interval;
static bool connected = false;

static int run_default(const char* bind_ip, int port, const char* certs_dir)
{
    uWS::Hub h;
    instance = &h;
    
    //uWS::Group<uWS::SERVER> *tServerGroup = h.createGroup<uWS::SERVER>();
    //tServerGroup->listen(uWS::TRANSFERS);
    
    //h.onConnection([/*tServerGroup*/](uWS::WebSocket<uWS::SERVER> *ws, uWS::HttpRequest req) {
        //ws->transfer(tServerGroup);
    //});
    
    h.onMessage([&h/*tServerGroup*/](
            uWS::WebSocket<uWS::CLIENT> *ws, char *message, size_t length, uWS::OpCode opCode) {
        //std::cout << std::string(message, length) << std::endl;
        h.getDefaultGroup<uWS::SERVER>().broadcast(message, length, opCode);
        //tServerGroup->broadcast(message, length, opCode);
    });
    
    h.onConnection([](uWS::WebSocket<uWS::CLIENT> *ws, uWS::HttpRequest req) {
        connected = true;
    });
    
    h.onDisconnection([](uWS::WebSocket<uWS::CLIENT> *ws, int code, char *message, size_t length) {
        //std::cout << "reconnecting ..." << std::endl;
        connected = false;
    });
    
    //if (bind_ip) std::cout << bind_ip << ':';
    //std::cout << port << std::endl;
    
    if (!listen(h, bind_ip, port, certs_dir))
        return 1;
    
    Timer* timer = new Timer(h.getLoop());
    timer->start([](Timer *timer) {
        if (!connected)
        {
            //std::cout << "try to connect ..." << std::endl;
            instance->connect(connect_url, nullptr);
        }
    }, reconnect_interval, reconnect_interval);
    
    h.connect(connect_url, nullptr);
    h.run();
    
    //delete tServerGroup;
    
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc == 2 && 0 == strcmp(argv[1], "--version"))
    {
        printf("%s\n", PROTOSTUFFDB_VERSION);
        return 0;
    }
    
    if (argc < 4)
    {
        usage(argc, argv);
        return 1;
    }
    
    int port;
    char* bind_ip = nullptr;
    if (!get_ip_port(argv[1], &bind_ip, &port))
    {
        usage(argc, argv);
        return 1;
    }
    
    connect_url = argv[2];
    reconnect_interval = std::atoi(argv[3]);
    
    char* certs_dir = argc > 4 ? argv[4] : nullptr;
    
    return run_default(bind_ip, port, certs_dir);
}
