#include <cstdlib>
#include <string>
#include <fstream>

extern "C" {
#include <stdio.h>
#include <sys/stat.h>
}

#ifndef _WIN32

extern "C" {
#include <unistd.h>
}
#define GetCurrentDir getcwd

#else

extern "C" {
#include <direct.h>
#include <tchar.h>
#include <windows.h>
#pragma comment(lib, "user32.lib")
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
}
#define GetCurrentDir _getcwd

#endif

#ifdef _WIN32
static const char SEPARATOR_CHAR = '\\';
static const char* SEPARATOR_STR = "\\/";
#else
static const char SEPARATOR_CHAR = '/';
static const char* SEPARATOR_STR = "/";
#endif // _WIN32

static bool exists(const char *path)
{
    struct stat b;
    return 0 == stat(path, &b);
}

// Load file "name" into "buf" returning true if successful
// false otherwise.  If "binary" is false data is read
// using ifstream's text mode, otherwise data is read with
// no transcoding.
static bool LoadFile(const char *name, bool binary, std::string *buf) {
  std::ifstream ifs(name, binary ? std::ifstream::binary : std::ifstream::in);
  if (!ifs.is_open()) return false;
  *buf = std::string(std::istreambuf_iterator<char>(ifs),
                    std::istreambuf_iterator<char>());
  return !ifs.bad();
}

// Save data "buf" of length "len" bytes into a file
// "name" returning true if successful, false otherwise.
// If "binary" is false data is written using ifstream's
// text mode, otherwise data is written with no
// transcoding.
static bool SaveFile(const char *name, const char *buf, size_t len,
                     bool binary) {
  std::ofstream ofs(name, binary ? std::ofstream::binary : std::ofstream::out);
  if (!ofs.is_open()) return false;
  ofs.write(buf, len);
  return !ofs.bad();
}

static bool writeShortcutTo(const std::string& out_file,
        const std::string& name, const std::string& pwd,
        const std::string& cwd_path,
        const std::string& exe_path,
        const std::string& icon_path)
{
    bool ok;
    std::string content;
    
    // TODO inspect content and return true if no need to rewrite
    /*if (exists(out_file.c_str()))
    {
        
    }*/
    
    #ifdef _WIN32
    
    content += "Set oWS=WScript.CreateObject(\"WScript.Shell\")\n";
    content += "sLinkFile=\""; content += pwd; content += '\\'; content += out_file; content += "\"\n";
    content += "Set oLink=oWS.CreateShortcut(sLinkFile)\n";
    content += "oLink.WorkingDirectory=\""; content += pwd;
    if (!cwd_path.empty())
    {
        content += '\\'; content += cwd_path;
    }
    content += "\"\noLink.IconLocation=\""; content += pwd; content += '\\'; content += icon_path;
    content += "\"\noLink.TargetPath=\""; content += pwd; content += '\\'; content += exe_path;
    content += "\"\noLink.Save\n";
    
    std::string vbs_file;
    std::string cscript;
    
    vbs_file += pwd; vbs_file += "\\target\\setup.vbs";
    cscript += "cscript /nologo \""; cscript += vbs_file; cscript += '"';
    
    ok = SaveFile(vbs_file.c_str(), content.data(), content.size(), false) && 0 == std::system(cscript.c_str());
    
    #else
    
    content += "[Desktop Entry]\n";
    content += "Version=1.0\n";
    content += "Name="; content += name; content += "\n";
    content += "X-GNOME-FullName="; content += name; content += "\n";
    content += "Type=Application\n";
    content += "Categories=Application\n";
    content += "Terminal=false\n";
    content += "StartupNotify=true\n";
    content += "Path="; content += pwd;
    if (!cwd_path.empty())
    {
        content += '/'; content += cwd_path;
    }
    content += "\nIcon="; content += pwd; content += '/'; content += icon_path;
    content += "\nExec="; content += pwd; content += '/'; content += exe_path;
    content += '\n';
    
    std::string chmod;
    chmod += "/bin/chmod +x "; chmod += out_file;
    ok = SaveFile(out_file.c_str(), content.data(), content.size(), false) && 0 == std::system(chmod.c_str());
    
    #endif
    
    return ok;
}

static bool isSPCRLF(char c)
{
    return c == ' ' || c == '\r' || c == '\n';
}

int main(int argc, char *argv[])
{
    char buf[FILENAME_MAX];
    if (!GetCurrentDir(buf, FILENAME_MAX))
    {
        std::fprintf(stderr, "Could not obtain $PWD.\n");
        return 1;
    }
    
    std::string name;
    if (!LoadFile("target/name.txt", false, &name))
    {
        std::fprintf(stderr, "target/name.txt not found.\n");
        return 1;
    }
    
    while (isSPCRLF(name.back()))
        name.pop_back();
    
    std::string pwd(buf);
    std::string out_file;
    std::string cwd_path;
    std::string exe_path;
    std::string icon_path;
    
    out_file += name;
    exe_path += "target"; exe_path += SEPARATOR_CHAR;
    if (exists("assets"))
    {
        exe_path += "todo";
        icon_path += "assets";
    }
    else
    {
        exe_path += "opt-nw";
        
        cwd_path += name; cwd_path += "-ts";
        
        icon_path += cwd_path; icon_path += SEPARATOR_CHAR; icon_path += "nw";
    }
    
    icon_path += SEPARATOR_CHAR; icon_path += "icon.ico";
    
    #ifdef _WIN32
    out_file += ".lnk";
    exe_path += ".exe";
    #else
    out_file += ".desktop";
    #endif
    
    if (!writeShortcutTo(out_file, name, pwd, cwd_path, exe_path, icon_path))
    {
        std::fprintf(stderr, "Could not write shortcut.\n");
        return 1;
    }
    
    return 0;
}
