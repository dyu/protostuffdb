#include <iostream>
#include <vector>
#include <uWS.h>

static void usage(int argc, char *argv[])
{
    std::cerr << "Usage:\n"
        << argv[0]
        << " connect_url backup_name flags db_name .." << std::endl;
}

static bool connected = false;

int main(int argc, char *argv[])
{
    if (argc == 2 && 0 == strcmp(argv[1], "--version"))
    {
        printf("%s\n", PROTOSTUFFDB_VERSION);
        return 0;
    }
    
    if (argc < 5)
    {
        usage(argc, argv);
        return 1;
    }
    
    const char* connect_url = argv[1];
    
    std::string backup_name = argv[2];
    
    const auto flags = static_cast<uint8_t>(std::atoi(argv[3]));
    
    std::vector<std::string> db_list;
    
    for (int i = 4; i < argc; i++)
        db_list.push_back(argv[i]);
    
    auto db_count = db_list.size();
    auto err_count = 0;
    
    uWS::Hub h;
    
    h.onConnection([flags, &backup_name, &db_list](uWS::WebSocket<uWS::CLIENT> *ws, uWS::HttpRequest req) {
        connected = true;
        
        char req_buf[1024]; // TODO enough?
        // skip type and count (handled at the bottom)
        char* buf = req_buf + 2;
        uint8_t name_size, count = 0, backup_name_size = static_cast<uint8_t>(backup_name.size());
        
        for (auto& name : db_list)
        {
            *buf++ = count++;
            
            name_size = static_cast<uint8_t>(name.size());
            *buf++ = name_size;
            memcpy(buf, name.data(), name_size);
            buf += name_size;
            
            *buf++ = 1; // backup request
            *buf++ = flags; // flags
            
            *buf++ = backup_name_size;
            memcpy(buf, backup_name.data(), backup_name_size);
            buf += backup_name_size;
        }
        
        *req_buf = 0; // standlone ws request
        *(req_buf + 1) = count;
        ws->send(req_buf, buf - req_buf, uWS::OpCode::BINARY);
    });
    
    h.onMessage([&db_list, &db_count, &err_count](uWS::WebSocket<uWS::CLIENT> *ws, char *message, size_t length, uWS::OpCode opCode) {
        // entry_id, status, msg_len, msg
        char* buf = message;
        uint8_t entry_id = *buf++;
        uint8_t status = *buf++;
        if (status == 0)
        {
            std::cout << db_list[entry_id] << ": ok" << std::endl;
        }
        else
        {
            ++err_count;
            std::cout << db_list[entry_id] << ": " << std::string(buf + 1, *(uint8_t*)buf) << std::endl;
        }
        
        if (0 == --db_count)
            exit(err_count == 0 ? 0 : 1);
    });
    
    h.connect(connect_url, nullptr);
    h.run();
    
    if (!connected)
        std::cout << "Could not connect." << std::endl;
    
    return static_cast<int>(!connected);
}
