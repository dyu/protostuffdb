//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds.v1;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.v1.VisitP1;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.test.AbstractPersonVisitorTest;
import com.dyuproject.protostuffdb.test.Person;

/**
 * Tests for {@link Visit1}.
 * 
 * @author David Yu
 * @created Jul 8, 2013
 */
public class Visit1Test extends AbstractPersonVisitorTest
{

    @Override
    protected void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req)
            throws Exception
    {
        assertTrue(VisitP1.byP11(Person.IDX_1i_1i, 
                e.xInt8, e.yInt8, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP1.byP14(Person.IDX_1i_4i, 
                e.xInt8, e.xInt32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP1.byP18(Person.IDX_1i_8i, 
                e.xInt8, e.xInt64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP1.byP1K(Person.IDX_1i_K, 
                e.xInt8, e.xKey, 0, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP1.byP1S(Person.IDX_1i_S, 
                e.xInt8, e.xStr, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        // prefix test
        for(int i = 1, len = e.xStr.length(); i <= len; i++)
        {
            assertTrue(VisitP1.byP1Sp(Person.IDX_1i_S, 
                    e.xInt8, prefix(e.xStr, i), null, 
                    em, fieldNumber, req, 
                    RangeV.Store.ENTITY_PV, store, res.context, 
                    RangeV.RES_PV, res));
            anc(1, res);
        }
    }

}
