//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds.vs;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.vs.VisitPS;
import com.dyuproject.protostuff.ds.prk.vs.VisitPSS;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.test.AbstractPersonVisitorTest;
import com.dyuproject.protostuffdb.test.Person;
import com.dyuproject.protostuffdb.test.Tags;

/**
 * Tag tests for {@link VisitSS}.
 * 
 * @author David Yu
 * @created Jul 9, 2013
 */
public class VisitSSTagTest extends AbstractPersonVisitorTest
{

    @Override
    protected void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req)
            throws Exception
    {
        assertTrue(VisitPSS.byPSS1(Tags.IDX_S_S_1i, 
                e.xStr, e.yStr, e.xInt8, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPSS.byPSS4(Tags.IDX_S_S_4i, 
                e.xStr, e.yStr, e.xInt32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPSS.byPSS8(Tags.IDX_S_S_8i, 
                e.xStr, e.yStr, e.xInt64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPSS.byPSSD(Tags.IDX_S_S_D, 
                e.xStr, e.yStr, e.xDate, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPSS.byPSSK(Tags.IDX_S_S_K, 
                e.xStr, e.yStr, e.xKey, 0, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPSS.byPSSS(Tags.IDX_S_S_S, 
                e.xStr, e.yStr, e.zStr, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        // prefix test
        for(int i = 1, len = e.yStr.length(); i <= len; i++)
        {
            assertTrue(VisitPS.byPSSp(Tags.IDX_S_S, 
                    e.xStr, prefix(e.yStr, i), null, 
                    em, fieldNumber, req, 
                    RangeV.Store.ENTITY_PV, store, res.context, 
                    RangeV.RES_PV, res));
            anc(1, res);
        }
    }
}
