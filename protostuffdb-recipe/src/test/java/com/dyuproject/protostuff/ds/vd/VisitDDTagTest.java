//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds.vd;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.vd.VisitPDD;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.test.AbstractPersonVisitorTest;
import com.dyuproject.protostuffdb.test.Person;
import com.dyuproject.protostuffdb.test.Tags;

/**
 * Tag tests for {@link VisitDD}.
 * 
 * @author David Yu
 * @created Jul 9, 2013
 */
public class VisitDDTagTest extends AbstractPersonVisitorTest
{

    @Override
    protected void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req)
            throws Exception
    {
        assertTrue(VisitPDD.byPDD1(Tags.IDX_D_D_1i, 
                e.xDate, e.yDate, e.xInt8, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPDD.byPDD4(Tags.IDX_D_D_4i, 
                e.xDate, e.yDate, e.xInt32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPDD.byPDD8(Tags.IDX_D_D_8i, 
                e.xDate, e.yDate, e.xInt64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPDD.byPDDD(Tags.IDX_D_D_D, 
                e.xDate, e.yDate, e.zDate, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPDD.byPDDK(Tags.IDX_D_D_K, 
                e.xDate, e.yDate, e.xKey, 0, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitPDD.byPDDS(Tags.IDX_D_D_S, 
                e.xDate, e.yDate, e.xStr, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        // prefix test
        for(int i = 1, len = e.xStr.length(); i <= len; i++)
        {
            assertTrue(VisitPDD.byPDDSp(Tags.IDX_D_D_S, 
                    e.xDate, e.yDate, prefix(e.xStr, i), null, 
                    em, fieldNumber, req, 
                    RangeV.Store.ENTITY_PV, store, res.context, 
                    RangeV.RES_PV, res));
            anc(1, res);
        }
    }
}
