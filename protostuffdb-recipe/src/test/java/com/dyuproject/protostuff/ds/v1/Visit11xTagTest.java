//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds.v1;

import static com.dyuproject.protostuffdb.SerializedValueUtil.i1;
import static com.dyuproject.protostuffdb.SerializedValueUtil.i4;
import static com.dyuproject.protostuffdb.SerializedValueUtil.i8;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.v1.VisitP11;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.test.AbstractPersonVisitorTest;
import com.dyuproject.protostuffdb.test.Person;
import com.dyuproject.protostuffdb.test.Tags;

/**
 * Tag tests for {@link Visit11}.
 * 
 * @author David Yu
 * @created Jul 9, 2013
 */
public class Visit11xTagTest extends AbstractPersonVisitorTest
{

    @Override
    protected void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req)
            throws Exception
    {
        assertTrue(VisitP11.byP111(Tags.IDX_1b_1b_1b, 
                i1(e.xBool), i1(e.yBool), i1(e.zBool), 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP111(Tags.IDX_1b_1b_1i, 
                i1(e.xBool), i1(e.yBool), e.xInt8, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP114(Tags.IDX_1b_1b_4f, 
                i1(e.xBool), i1(e.yBool), i4(e.xFloat), 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP114(Tags.IDX_1b_1b_4i, 
                i1(e.xBool), i1(e.yBool), e.xInt32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP114(Tags.IDX_1b_1b_4, 
                i1(e.xBool), i1(e.yBool), e.xFixed32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP118(Tags.IDX_1b_1b_8d, 
                i1(e.xBool), i1(e.yBool), i8(e.xDouble), 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP118(Tags.IDX_1b_1b_8i, 
                i1(e.xBool), i1(e.yBool), e.xInt64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP118(Tags.IDX_1b_1b_8, 
                i1(e.xBool), i1(e.yBool), e.xFixed64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP11K(Tags.IDX_1b_1b_K, 
                i1(e.xBool), i1(e.yBool), e.xKey, 0, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP11.byP11S(Tags.IDX_1b_1b_S, 
                i1(e.xBool), i1(e.yBool), e.xStr, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
    }
}
