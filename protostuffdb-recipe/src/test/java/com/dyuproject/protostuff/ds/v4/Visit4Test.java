//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds.v4;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.v4.VisitP4;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.test.AbstractPersonVisitorTest;
import com.dyuproject.protostuffdb.test.Person;

/**
 * Tests for {@link Visit4}.
 * 
 * @author David Yu
 * @created Jul 8, 2013
 */
public class Visit4Test extends AbstractPersonVisitorTest
{
    
    @Override
    protected void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req)
            throws Exception
    {
        assertTrue(VisitP4.byP41(Person.IDX_4i_1i, 
                e.xInt32, e.xInt8, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP4.byP44(Person.IDX_4i_4i, 
                e.xInt32, e.yInt32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP4.byP48(Person.IDX_4i_8i, 
                e.xInt32, e.xInt64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP4.byP4K(Person.IDX_4i_K, 
                e.xInt32, e.xKey, 0, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP4.byP4S(Person.IDX_4i_S, 
                e.xInt32, e.xStr, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        // prefix test
        for(int i = 1, len = e.xStr.length(); i <= len; i++)
        {
            assertTrue(VisitP4.byP4Sp(Person.IDX_4i_S, 
                    e.xInt32, prefix(e.xStr, i), null, 
                    em, fieldNumber, req, 
                    RangeV.Store.ENTITY_PV, store, res.context, 
                    RangeV.RES_PV, res));
            anc(1, res);
        }
    }

}
