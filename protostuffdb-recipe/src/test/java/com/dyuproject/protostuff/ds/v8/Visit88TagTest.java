//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds.v8;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.v8.VisitP88;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.test.AbstractPersonVisitorTest;
import com.dyuproject.protostuffdb.test.Person;
import com.dyuproject.protostuffdb.test.Tags;

/**
 * Tag tests for {@link Visit88}.
 * 
 * @author David Yu
 * @created Jul 9, 2013
 */
public class Visit88TagTest extends AbstractPersonVisitorTest
{

    @Override
    protected void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req)
            throws Exception
    {
        assertTrue(VisitP88.byP881(Tags.IDX_8i_8i_1i, 
                e.xInt64, e.yInt64, e.xInt8, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP88.byP884(Tags.IDX_8i_8i_4i, 
                e.xInt64, e.yInt64, e.xInt32, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP88.byP888(Tags.IDX_8i_8i_8i, 
                e.xInt64, e.yInt64, e.zInt64, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP88.byP88D(Tags.IDX_8i_8i_D, 
                e.xInt64, e.yInt64, e.xDate, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP88.byP88K(Tags.IDX_8i_8i_K, 
                e.xInt64, e.yInt64, e.xKey, 0, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        assertTrue(VisitP88.byP88S(Tags.IDX_8i_8i_S, 
                e.xInt64, e.yInt64, e.xStr, 
                em, fieldNumber, req, 
                RangeV.Store.ENTITY_PV, store, res.context, 
                RangeV.RES_PV, res));
        anc(1, res);
        
        // prefix test
        for(int i = 1, len = e.xStr.length(); i <= len; i++)
        {
            assertTrue(VisitP88.byP88Sp(Tags.IDX_8i_8i_S, 
                    e.xInt64, e.yInt64, prefix(e.xStr, i), null, 
                    em, fieldNumber, req, 
                    RangeV.Store.ENTITY_PV, store, res.context, 
                    RangeV.RES_PV, res));
            anc(1, res);
        }
    }
}
