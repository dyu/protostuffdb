//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import static com.dyuproject.protostuff.WireFormat.WIRETYPE_END_GROUP;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_START_GROUP;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_TAIL_DELIMITER;
import static com.dyuproject.protostuff.WireFormat.makeTag;

import java.io.IOException;

import com.dyuproject.protostuffdb.WriteContext;

/**
 * For testing purposes.
 * 
 * @author David Yu
 * @created Jul 20, 2017
 */
public class ProtostuffResponse extends RpcResponse
{
    private final ProtostuffOutput po;
    
    public ProtostuffResponse(WriteContext context)
    {
        super(new ProtostuffOutput(LinkedBuffer.allocate(LinkedBuffer.DEFAULT_BUFFER_SIZE)), 
                context, RpcProtocol.DEFAULT);
        
        po = (ProtostuffOutput)output;
    }

    public String errmsg;

    @Override
    public RpcWorker getWorker()
    {
        return RpcWorker.get();
    }
    
    public byte[] toByteArray()
    {
        return po.toByteArray();
    }

    @Override
    protected <T> boolean fail(RpcError error, T message, Schema<T> fSchema) throws IOException
    {
        errmsg = message.toString();
        return false;
    }

    @Override
    public <T> Schema<?> scope(int fieldNumber, boolean push, 
            Schema<T> schema, boolean repeated) throws IOException
    {
        if (push)
        {
            po.tail = po.sink.writeVarInt32(
                    makeTag(fieldNumber, WIRETYPE_START_GROUP), 
                    po, 
                    po.tail);
        }
        else
        {
            po.tail = po.sink.writeVarInt32(
                    makeTag(fieldNumber, WIRETYPE_END_GROUP), 
                    po, 
                    po.tail);
        }
        
        return null;
    }

    @Override
    protected void end() throws IOException
    {
        po.tail = po.sink.writeByte(
                (byte)WIRETYPE_TAIL_DELIMITER, 
                po, po.tail);
    }

}
