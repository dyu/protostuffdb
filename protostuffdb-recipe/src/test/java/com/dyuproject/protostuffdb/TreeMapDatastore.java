//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.CAS;


/**
 * Datastore impl using a {@link NavigableMap} (for local testing purposes).
 *
 * @author David Yu
 * @created Mar 15, 2011
 */
public final class TreeMapDatastore extends Datastore
{
    
    private final Comparator<byte[]> comparator;
    private final NavigableMap<byte[],byte[]> map;
    
    public final long createTs = System.currentTimeMillis();
    
    public TreeMapDatastore()
    {
        this("test");
    }
    
    public TreeMapDatastore(final String name)
    {
        this(name, 
                new ConcurrentSkipListMap<byte[],byte[]>(Comparators.DEFAULT), 
                Comparators.DEFAULT);
    }
    
    public TreeMapDatastore(final String name, 
            NavigableMap<byte[], byte[]> map, 
            Comparator<byte[]> comparator)
    {
        super(name, null);
        
        this.map = map;
        
        this.comparator = comparator;
        
        assert comparator != null;
    }
    
    public long getCreateTs()
    {
        return createTs;
    }
    
    public boolean isVisitorSessionIsolated()
    {
        return false;
    }
    
    public <T,P,V> boolean chain(EntityMetadata<T> root, 
            Op<P> op, P param, int retries, 
            WriteContext context, byte[] key, 
            VisitorSession.Handler<V> vhandler, V vparam, VisitorSession vs)
    {
        // TODO rollback and retries
        try
        {
            TreeMapOpChain chain = new TreeMapOpChain(root, context, vs, map);
            if(!op.handle(chain, key, null, 0, 0, param))
            {
                return false;
            }
            
            if(vhandler != null)
                vhandler.handle(chain.vs(), vparam);
            
            return true;
        }
        finally
        {
            context.clearData();
        }
    }
    
    public <P> void session(WriteContext context, 
            VisitorSession.Handler<P> handler, P param)
    {
        final TreeMapVisitorSession session = new TreeMapVisitorSession(map, comparator);
        handler.handle(session, param);
    }
    
    public void rawPut(byte[] key, int koffset, int klen, 
            byte[] value, int voffset, int vlen, WriteContext context)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        byte[] v = new byte[vlen];
        System.arraycopy(value, voffset, v, 0, vlen);
        
        map.put(k, v);
    }

    public void rawDelete(byte[] key, int koffset, int klen, WriteContext context)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        map.remove(k);
    }

    public byte[] rawGet(byte[] key, int koffset, int klen, WriteContext context)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        return map.get(k);
    }
    
    public boolean rawExists(byte[] key, int koffset, int klen, WriteContext context)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        return map.containsKey(k);
    }
    
    // ======================================================================

    @Override
    public boolean exists(boolean prefix, WriteContext context, KeyBuilder kb, boolean pop)
    {
        byte[] key = ValueUtil.copy(kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len());
        if (!prefix)
            return map.containsKey(key);
        
        byte[] higherKey = map.higherKey(key);
        return higherKey != null && higherKey.length >= key.length && 
                ValueUtil.isEqual(key, higherKey, 0, key.length);
    }

    @Override
    public boolean get(HasKV kv, WriteContext context, KeyBuilder kb, boolean pop)
    {
        byte[] key = ValueUtil.copy(kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len());
        byte[] value = map.get(key);
        if (value == null)
            return false;
        
        kv.setValue(value);
        return true;
    }

    @Override
    public boolean pget(HasKV kv, boolean includeValue, boolean extractEntityKey,
            WriteContext context, KeyBuilder kb, boolean pop)
    {
        byte[] key = ValueUtil.copy(kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len());
        Map.Entry<byte[],byte[]> entry = map.higherEntry(key);
        final byte[] higherKey;
        if (entry == null 
                || (higherKey = entry.getKey()).length < key.length
                || !ValueUtil.isEqual(key, higherKey, 0, key.length))
        {
            return false;
        }
        
        if (includeValue)
            kv.setValue(entry.getValue());
        
        kv.setKey(extractEntityKey ? KeyUtil.extractFrom(higherKey) : higherKey);
        return true;
    }
    
    // ======================================================================
    

    public <T> boolean update(byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener, 
            byte[] parentKey, WriteContext context)
    {
        if(cas.isEmpty())
            throw DSRuntimeExceptions.invalidArg("CAS has no operations to perform");
        
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null.");
                
                KeyUtil.validateSameKind(key, em);
                try
                {
                    return update(map, key, key, cas, listener, em, context);
                }
                catch(ClassCastException e)
                {
                    throw DSRuntimeExceptions.invalidArg("CAS fields not in sync with the schema: " + 
                            em.pipeSchema.wrappedSchema.messageFullName());
                }
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null.");
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                try
                {
                    return update(map, key, KeyUtil.newOneToManyKey(key, parentKey), 
                        cas, listener, em, context);
                }
                catch(ClassCastException e)
                {
                    throw DSRuntimeExceptions.invalidArg("CAS fields not in sync with the schema: " + 
                            em.pipeSchema.wrappedSchema.messageFullName());
                }
        }
        
        throw DSRuntimeExceptions.invalidArg("Delete operation not applicable for type: " + 
                em.type);
    }
    
    private static <T> boolean update(NavigableMap<byte[],byte[]> map, 
            byte[] key, byte[] searchKey, 
            CAS cas, CAS.Listener listener, 
            EntityMetadata<T> em, WriteContext context)
    {
        final byte[] value = map.get(searchKey);
        if(value == null)
            return false;
        
        if(listener != null && !listener.onBeforeApply(searchKey, value, null))
            return false;
        
        final int size = context.casAndIndexCollect(value, 0, value.length, em, cas);
        final CASAndIndexCollectOutput output = context.casAndIndexCollectOutput;
        
        if (output.casCount == 0)
            return false;
        switch (em.updateFlags)
        {
            case 1:
                if (output.casCount == 1 && output.isUpdated(em.revField, cas))
                    return false;
                break;
            case 2:
                if (output.casCount == 1 && output.isUpdated(em.updateTsField, cas))
                    return false;
                break;
            case 3:
                if (output.casCount == 2 && 
                    output.isUpdated(em.revField, cas) && 
                    output.isUpdated(em.updateTsField, cas))
                {
                    return false;
                }
                break;
        }
        
        final byte[] entityBuffer = context.entityBuffer;
        final int entityOffset = context.entityOffset;
        
        if (listener != null && !listener.onApply(searchKey, value, cas, output, null, 
                entityBuffer, entityOffset, size))
        {
            return false;
        }

        
        final byte[] newValue = new byte[size];
        System.arraycopy(entityBuffer, entityOffset, newValue, 0, size);
        
        if (!Arrays.equals(value, map.put(searchKey, newValue)))
            throw DSRuntimeExceptions.unknownState("Should not happen. Sychhronized access only.");
        
        if(em.indexOnFields && (output.trackCount != 0 || em.index.valueCount != 0))
        {
            indexOnUpdate(map, output, searchKey, searchKey.length == 9 ? 0 : 9, 
                    em, context, entityBuffer, entityOffset, size, value, newValue);
        }
        
        if(0 != em.linkIndex.length)
            linkIndexOnWrite(map, searchKey, searchKey.length == 9 ? 0 : 9, em, newValue);
        
        return true;
    }
    
    public <T> boolean delete(byte[] key, EntityMetadata<T> em, byte[] parentKey, WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
            {
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null.");
                
                KeyUtil.validateSameKind(key, em);
                
                final byte[] value = map.remove(key);
                if(value == null)
                    return false;
                
                if(em.indexOnFields)
                    indexOnDelete(map, key, 0, em, context, value);
                
                if(0 != em.linkIndex.length)
                    linkIndexOnDelete(map, key, 0, em);
                
                return true;
            }   
            case LINKED_CHILD:
            {
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null.");
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                final byte[] oneToManyKey = KeyUtil.newOneToManyKey(key, parentKey);
                final byte[] value = map.remove(oneToManyKey);
                
                if(value == null)
                    return false;
                
                map.remove(key);
                
                if(em.indexOnFields)
                    indexOnDelete(map, oneToManyKey, 9, em, context, value);
                
                if(0 != em.linkIndex.length)
                    linkIndexOnDelete(map, oneToManyKey, 9, em);
                
                return true;
            }
        }
        
        throw DSRuntimeExceptions.invalidArg("Delete operation not applicable for type: " + 
                em.type);
    }
    
    public <T> boolean exists(byte[] key, EntityMetadata<T> em, byte[] parentKey, 
            WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                KeyUtil.validateSameKind(key, em);
                
                return map.containsKey(key);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw DSRuntimeExceptions.invalidArg("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    return map.containsKey(key);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return map.containsKey(KeyUtil.newOneToManyKey(key, parentKey));
        }
        
        throw DSRuntimeExceptions.invalidArg("Exists operation not applicable for type: " + 
                em.type);
    }
    
    public <T> byte[] get(byte[] key, EntityMetadata<T> em, byte[] parentKey, 
            WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                KeyUtil.validateSameKind(key, em);
                
                return map.get(key);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw DSRuntimeExceptions.invalidArg("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    return map.get(key);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return map.get(KeyUtil.newOneToManyKey(key, parentKey));
        }
        
        throw DSRuntimeExceptions.invalidArg("Get operation not applicable for type: " + 
                em.type);
    }
    
    protected <T> void insert(final byte[] key, final EntityMetadata<T> em, 
            final byte[] parentKey, final WriteContext context, 
            final SerAndIndexCollectOutput output, 
            byte[] entityBuffer, final int entityOffset, final int size)
    {
        final byte[] value = ValueUtil.copy(entityBuffer, entityOffset, size);
        
        final byte[] indexKey;
        final int indexKeyOffset;
        if(parentKey == null)
        {
            indexKey = key;
            indexKeyOffset = 0;
            
            map.put(key, value);
        }
        else
        {
            indexKey = KeyUtil.newOneToManyKey(key, parentKey);
            indexKeyOffset = 9;
            
            map.put(indexKey, value);
            map.put(key, parentKey);
        }
        
        if(em.indexOnFields)
        {
            indexOnInsert(map, indexKey, indexKeyOffset, em, context, 
                    output, entityBuffer, entityOffset, size, 
                    value);
        }
        
        if(0 != em.linkIndex.length)
            linkIndexOnWrite(map, indexKey, indexKeyOffset, em, value);
    }
    
    static <T> void linkIndexOnWrite(NavigableMap<byte[], byte[]> map, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, byte[] value)
    {
        for(int kind : em.linkIndex)
        {
            // new array instance everytime because of a treemap bug (SkipListMap fixes it)
            map.put(KeyUtil.newParentLinkKey(kind, key, keyOffset), value);
        }
    }

    static <T> void linkIndexOnDelete(NavigableMap<byte[], byte[]> map, 
            byte[] key, int keyOffset,
            EntityMetadata<T> em)
    {
        for(int kind : em.linkIndex)
        {
            // new array instance everytime because of a treemap bug (SkipListMap fixes it)
            // the last one is the kind
            map.remove(KeyUtil.newParentLinkKey(kind, key, keyOffset));
        }
    }
    
    static <T> void indexOnDelete(NavigableMap<byte[],byte[]> map, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, WriteContext context, 
            byte[] value)
    {
        final IndexCollectOutput output = context.indexCollectOutput;
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        
        final int[] result = context.intset();
        
        for(int i = 0, count = context.indexCollect(
                value, 0, value.length, em), field = 0;
                i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                removeIndexOnDelete(map, output, em, context, id, 
                        key, keyOffset, 
                        buf, bufOffset, 
                        result, value);
            }
        }
        
        // value dependencies
        for(int i = 0, id = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            removeIndexOnDelete(map, output, em, context, id, 
                    key, keyOffset, 
                    buf, bufOffset, 
                    result, value);
        }
    }
    
    private static <T> void removeIndexOnDelete(NavigableMap<byte[],byte[]> map, 
            IndexCollectOutput output, EntityMetadata<T> em, final WriteContext context, 
            int id, byte[] key, int keyOffset, 
            byte[] buf, int bufOffset, 
            int[] result, 
            byte[] value)
    {
        final int secondaryTags = em.index.secondaryTagEntries[id];
        
        final int oldKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetRemoveIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                value, 0, value.length, 
                result, true);
        
        final byte[] ik = new byte[oldKeySize];
        System.arraycopy(buf, bufOffset, ik, 0, oldKeySize);
        
        map.remove(ik);
        
        if(secondaryTags == 0)
            return;
        
        final byte[] tagKey;
        if(id < 224)
        {
            // tag index
            //tagKey = ValueUtil.copy(ik);
            
            // compiler tries to optimize the copy on map.put(ValueUtil.copy(tagKey))
            // works on remove but not in put?
            tagKey = ik;
        }
        else
        {
            tagKey = ValueUtil.copy(ik, 1, ik.length -1);
            tagKey[0] = 0;
            //tagKey[1] = (byte)id;
        }
        
        for(int shift = 0; shift <= 24; shift+=8)
        {
            int tag = 0xFF & (secondaryTags >>> shift);
            if(tag == 0)
                break;
            
            tagKey[1] = (byte)tag;
            map.remove(ValueUtil.copy(tagKey));
        }
    }
    
    static <T> void indexOnInsert(NavigableMap<byte[],byte[]> map, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, WriteContext context, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            byte[] value)
    {
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        
        final byte[] ts = new byte[6];
        System.arraycopy(key, keyOffset+1, ts, 0, 6);
        
        final int[] result = context.intset();
        
        for(int i = 0, count = output.count, field = 0; i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                writeIndexOnInsert(map, output, id, 
                        key, keyOffset, 
                        em, context, 
                        buf, bufOffset, 
                        result, 
                        entityBuffer, entityOffset, size, 
                        value, ts);
            }
        }
        
        // value dependencies
        for(int i = 0, id = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            writeIndexOnInsert(map, output, id, 
                    key, keyOffset, 
                    em, context, 
                    buf, bufOffset, 
                    result, 
                    entityBuffer, entityOffset, size, 
                    value, ts);
        }
    }
    
    private static <T> void writeIndexOnInsert(NavigableMap<byte[],byte[]> map, 
            SerAndIndexCollectOutput output, int id, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, final WriteContext context, 
            byte[] buf, int bufOffset, 
            int[] result, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            byte[] value, 
            byte[] ts)
    {
        final int secondaryTags = em.index.secondaryTagEntries[id];
        // key
        final int newKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetAddIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                entityBuffer, entityOffset, size, 
                result, false);
        
        final byte[] ik = new byte[newKeySize];
        System.arraycopy(buf, bufOffset, ik, 0, newKeySize);
        
        // value
        final int newValueSize = result[0];
        final byte[] writtenValue;
        if(newValueSize == 0)
        {
            // timestamp
            map.put(ik, ts);
            writtenValue = ts;
        }
        else if(newValueSize == -1)
        {
            result[0] = 0; // reset
            
            // the message itself.
            map.put(ik, value);
            writtenValue = value;
        }
        else
        {
            result[0] = 0; // reset
            
            // custom value
            writtenValue = new byte[newValueSize];
            System.arraycopy(buf, bufOffset + newKeySize, writtenValue, 0, newValueSize);
            map.put(ik, writtenValue);
        }
        
        if(secondaryTags == 0)
            return;
        
        final byte[] tagKey;
        if(id < 224)
        {
            // tag index
            tagKey = ValueUtil.copy(ik);
            
            // compiler tries to optimize the copy on map.put(ValueUtil.copy(tagKey))
            // tagKey = ik;
        }
        else
        {
            tagKey = ValueUtil.copy(ik, 1, ik.length -1);
            tagKey[0] = 0;
            //tagKey[1] = (byte)id;
        }
        
        for(int shift = 0; shift <= 24; shift+=8)
        {
            int tag = 0xFF & (secondaryTags >>> shift);
            if(tag == 0)
                break;
            
            tagKey[1] = (byte)tag;
            map.put(ValueUtil.copy(tagKey), writtenValue);
        }
    }
    
    static <T> void indexOnUpdate(NavigableMap<byte[],byte[]> map, 
            CASAndIndexCollectOutput output, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, WriteContext context, 
            byte[] entityBuffer, int entityOffset, int size, 
            byte[] value, byte[] newValue)
    {
        final byte[] ts = KeyUtil.new6ByteTimestamp();
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        
        final int[] result = context.intset();
        
        // value dependencies
        for(int i = 0, id = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            writeIndexOnUpdate(map, output, id, //true, 
                    key, keyOffset, 
                    em, context, 
                    buf, bufOffset, 
                    result, 
                    entityBuffer, entityOffset, size, 
                    value, newValue, ts);
        }
        
        for(int i = 0, count = output.trackCount, field = 0; 
                i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                writeIndexOnUpdate(map, output, id, //false, 
                        key, keyOffset, 
                        em, context, 
                        buf, bufOffset, 
                        result, 
                        entityBuffer, entityOffset, size, 
                        value, newValue, ts);
            }
        }
    }
    
    private static <T> void writeIndexOnUpdate(NavigableMap<byte[],byte[]> map, 
            CASAndIndexCollectOutput output, int id, //boolean valueDependency, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, final WriteContext context, 
            byte[] buf, int bufOffset, 
            int[] result, 
            byte[] entityBuffer, int entityOffset, int size, 
            byte[] oldValue, byte[] newValue, byte[] ts)
    {
        final int secondaryTags = em.index.secondaryTagEntries[id];
        
        // key
        final int newKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetAddIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                entityBuffer, entityOffset, size, 
                result, false);
        
        byte[] ik = new byte[newKeySize];
        System.arraycopy(buf, bufOffset, ik, 0, newKeySize);
        
        // value
        final int newValueSize = result[0];
        final byte[] writtenValue;
        if(newValueSize == 0)
        {
            // timestamp
            map.put(ik, ts);
            writtenValue = ts;
        }
        else if(newValueSize == -1)
        {
            result[0] = 0; // reset
            
            // the message itself.
            map.put(ik, newValue);
            writtenValue = newValue;
        }
        else
        {
            result[0] = 0; // reset
            
            // custom value
            writtenValue = new byte[newValueSize];
            System.arraycopy(buf, bufOffset + newKeySize, writtenValue, 0, newValueSize);
            
            map.put(ik, writtenValue);
            
        }
        
        if(secondaryTags != 0)
        {
            final byte[] tagKey;
            if(id < 224)
            {
                // tag index
                tagKey = ValueUtil.copy(ik);
                
                // compiler tries to optimize the copy on map.put(ValueUtil.copy(tagKey))
                // tagKey = ik;
            }
            else
            {
                tagKey = ValueUtil.copy(ik, 1, ik.length -1);
                tagKey[0] = 0;
                //tagKey[1] = (byte)id;
            }
            
            for(int shift = 0; shift <= 24; shift+=8)
            {
                int tag = 0xFF & (secondaryTags >>> shift);
                if(tag == 0)
                    break;
                
                tagKey[1] = (byte)tag;
                map.put(ValueUtil.copy(tagKey), writtenValue);
            }
        }
        
        // remove the old keys
        
        if(0 < em.index.offsetEntries[id])
        {
            // the fields in the index key are all immutable
            // don't bother removing since we already replaced the value
            return;
        }

        // old key
        final int oldKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetRemoveIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                oldValue, 0, oldValue.length, 
                result, true);
        
        if(ValueUtil.isEqual(ik, buf, bufOffset, oldKeySize))
        {
            // nothing changed in the index key
            // don't bother removing since we already replaced the value
            return;
        }
        
        ik = new byte[oldKeySize];
        System.arraycopy(buf, bufOffset, ik, 0, oldKeySize);
        
        map.remove(ik);
        
        if(secondaryTags == 0)
            return;
        
        final byte[] tagKey;
        if(id < 224)
        {
            // tag index
            tagKey = ik;
        }
        else
        {
            tagKey = ValueUtil.copy(ik, 1, ik.length -1);
            tagKey[0] = 0;
            //tagKey[1] = (byte)id;
        }
        
        for(int shift = 0; shift <= 24; shift+=8)
        {
            int tag = 0xFF & (secondaryTags >>> shift);
            if(tag == 0)
                break;
            
            tagKey[1] = (byte)tag;
            map.remove(ValueUtil.copy(tagKey));
        }
    }
    
    public <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            boolean extractEntityKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys");
     
        byte[] k,v;
        int count = 0;
        if(reverse)
        {
            for(int i = keys.size(); i-->0;)
            {
                k = keys.get(i);
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i);
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        
        return count;
    }
    
    public <V> int visitHasKeys(List<? extends HasKey> keys, boolean reverse, 
            boolean extractEntityKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys");
     
        byte[] k,v;
        int count = 0;
        if(reverse)
        {
            for(int i = keys.size(); i-->0;)
            {
                k = keys.get(i).getKey();
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i).getKey();
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        
        return count;
    }
    
    public <T,V> int visitKind(EntityMetadata<T> em, 
            int limit, boolean desc, byte[] startKey, byte[] parentKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                if(startKey == null)
                {
                    final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
                    final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
                    
                    return desc ? visitRangeDesc(map, comparator,limit, 
                            visitor, param, false, false, sk, ek, false) : visitRangeAsc(map, 
                                    comparator, limit, visitor, param, false, false, 
                                    sk, ek, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                if(desc)
                {
                    return visitRangeWithIndexKeyDesc(map, comparator, limit, startKey, 
                            false, visitor, param, false, false, 
                            KeyUtil.newEntityRangeKeyStart(em.kind), false);
                }
                
                    
                return visitRangeWithIndexKeyAsc(map, comparator, limit, startKey, 
                        false, visitor, param, false, false, 
                        KeyUtil.newEntityRangeKeyEnd(em.kind), false);
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null");
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                if(startKey == null)
                {
                    final byte[] sk = KeyUtil.newLinkRangeKeyStart(em.kind, parentKey);
                    final byte[] ek = KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey);
                    
                    return desc ? visitRangeDesc(map, comparator,limit, 
                            visitor, param, false, false, sk, ek, false) : visitRangeAsc(map, 
                                    comparator, limit, visitor, param, false, false, sk, ek, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                final byte[] ik = KeyUtil.newLinkIndexKey(em.kind, parentKey, startKey);
                
                if(desc)
                {
                    return visitRangeWithIndexKeyDesc(map, comparator, limit, ik, 
                            false, visitor, param, false, false, 
                            KeyUtil.newLinkRangeKeyStart(em.kind, parentKey), 
                            false);
                }
                
                return visitRangeWithIndexKeyAsc(map, comparator, limit, ik, 
                        false, visitor, param, false, false, 
                        KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey), 
                        false);
                
        }
        
        throw DSRuntimeExceptions.invalidArg("Visit operation not applicable for type: " + 
                em.type);
    }
    
    public <V> int scan(boolean keysOnly, int limit, boolean desc, 
            byte[] limitKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        int count = 0;
        
        if(limitKey == null)
        {
            Set<Map.Entry<byte[],byte[]>> set = desc ? map.descendingMap().entrySet() : 
                map.entrySet();
            
            byte[] v = null;
            int vlen = 0;
            for(Map.Entry<byte[], byte[]> entry : set)
            {
                if(!keysOnly)
                {
                    v = entry.getValue();
                    vlen = v.length;
                }
                
                if(visitor.visit(entry.getKey(), v, 0, vlen, 
                        param, count++) || (limited && count == limit))
                {
                    break;
                }
            }
            
            return count;
        }
        
        if(desc)
        {
            byte[] v = null;
            int vlen = 0;
            for(Map.Entry<byte[], byte[]> entry : map.descendingMap().entrySet())
            {
                if(!keysOnly)
                {
                    v = entry.getValue();
                    vlen = v.length;
                }
                
                if(comparator.compare(entry.getKey(), limitKey) <= 0 || 
                        visitor.visit(entry.getKey(), v, 0, vlen, 
                        param, count++) || (limited && count == limit))
                {
                    break;
                }
            }
            
            return count;
        }
        
        byte[] v = null;
        int vlen = 0;
        for(Map.Entry<byte[], byte[]> entry : map.entrySet())
        {
            if(!keysOnly)
            {
                v = entry.getValue();
                vlen = v.length;
            }
            
            if(comparator.compare(entry.getKey(), limitKey) >= 0 || 
                    visitor.visit(entry.getKey(), v, 0, vlen, 
                    param, count++) || (limited && count == limit))
            {
                break;
            }
        }
        
        return count;
    }
    
    public <V> int visitRange(
            boolean keysOnly, 
            int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param,
            boolean valueAsKey, boolean skRelativeLimit,
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen)
    {
        if(rawStartKey == null)
        {
            byte[] startKey = ValueUtil.copy(sk, skoffset, sklen), 
                    endKey = ValueUtil.copy(ek, ekoffset, eklen);
            
            return desc ? visitRangeDesc(map, comparator, limit, visitor, param, valueAsKey, skRelativeLimit, 
                    startKey, endKey, keysOnly) : visitRangeAsc(map, comparator, limit, 
                            visitor, param, valueAsKey, skRelativeLimit, startKey, endKey, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, sk, skoffset, sklen) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, ek, ekoffset, eklen) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            byte[] startKey = ValueUtil.copy(sk, skoffset, sklen);
            
            return visitRangeWithIndexKeyDesc(map, comparator, limit, rawStartKey, 
                    false, visitor, param, valueAsKey, skRelativeLimit, startKey, keysOnly);
        }
        
        byte[] endKey = ValueUtil.copy(ek, ekoffset, eklen);
        
        return visitRangeWithIndexKeyAsc(map, comparator, limit, rawStartKey, 
                false, visitor, param, valueAsKey, skRelativeLimit, endKey, keysOnly);
    }
    
    public <V> int visitIndex(
            boolean keysOnly, 
            int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop)
    {
        int offset = kb.offset(), size = pop ? kb.popLen() : kb.len();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            // append 0x00
            buf[offset+size] = 0;
            final byte[] sk = new byte[size+1];
            System.arraycopy(buf, offset, sk, 0, size+1);
            
            // append 0x01
            buf[offset+size] = (byte)1;
            final byte[] ek = new byte[size+1];
            System.arraycopy(buf, offset, ek, 0, size+1);
            
            return desc ? visitRangeDesc(map, comparator, limit, 
                    visitor, param, false, false, sk, ek, keysOnly) : visitRangeAsc(map, 
                            comparator, limit, visitor, param, false, false, sk, ek, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            // append 0x00
            buf[offset+size] = 0;
            final byte[] sk = new byte[size+1];
            System.arraycopy(buf, offset, sk, 0, size+1);
            
            return visitRangeWithIndexKeyDesc(map, comparator, limit, rawStartKey, 
                    false, visitor, param, false, false, sk, keysOnly);
        }
        
        // append 0x01
        buf[offset+size] = (byte)1;
        final byte[] ek = new byte[size+1];
        System.arraycopy(buf, offset, ek, 0, size+1);
        
        return visitRangeWithIndexKeyAsc(map, comparator, limit, rawStartKey, 
                false, visitor, param, false, false, ek, keysOnly);
    }

    /*public <V> int visitIndexPrefixMatch(
            boolean keysOnly, 
            int limit, boolean desc,
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        int offset = kb.offset(), size = kb.popLen();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            final byte[] sk = new byte[size];
            System.arraycopy(buf, offset, sk, 0, size);
            
            // append 0xFF
            buf[offset+size] = (byte)0xFF;
            final byte[] ek = new byte[size+1];
            System.arraycopy(buf, offset, ek, 0, size+1);
            
            return desc ? visitRangeDesc(map, comparator, limit, 
                    visitor, param, sk, ek, keysOnly) : visitRangeAsc(map, 
                            comparator, limit, visitor, param, sk, ek, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            final byte[] sk = new byte[size];
            System.arraycopy(buf, offset, sk, 0, size);
            
            return visitRangeWithIndexKeyDesc(map, comparator, limit, rawStartKey, 
                    false, visitor, param, sk, keysOnly);
        }
        
        // append 0xFF
        buf[offset+size] = (byte)0xFF;
        final byte[] ek = new byte[size+1];
        System.arraycopy(buf, offset, ek, 0, size+1);
        
        return visitRangeWithIndexKeyAsc(map, comparator, limit, rawStartKey, 
                false, visitor, param, ek, keysOnly);
    }*/
    
    private static int resolveMatchKeyLen(int klen)
    {
        // tag index (2) + key (9)
        return klen < 11 ? klen + 9 : klen;
    }

    static <T> int visitRangeAsc(NavigableMap<byte[],byte[]> map, 
            Comparator<byte[]> comparator, final int limit, 
            Visitor<T> visitor, T param,
            boolean valueAsKey, boolean skRelativeLimit, 
            byte[] startCompare, byte[] endCompare, boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        final ArrayList<byte[]> keysToGet = valueAsKey && !keysOnly ? 
                new ArrayList<byte[]>() : null;
        final int matchKeyLen = !skRelativeLimit ? 0 : 
                resolveMatchKeyLen(startCompare.length);
        
        int count = 0, vlen = 0, limitCount = 0;
        byte[] k, v = null;
        boolean matchBeforeVisit = false;
        for(Map.Entry<byte[], byte[]> entry : map.subMap(
                startCompare, true, endCompare, false).entrySet())
        {
            k = entry.getKey();
            if (matchBeforeVisit && matchKeyLen == k.length)
                break;
            
            if (keysOnly)
            {
                if (valueAsKey)
                    k = entry.getValue();
            }
            else if (valueAsKey)
            {
                keysToGet.add(KeyUtil.extractFrom(entry.getValue()));
                continue;
            }
            else
            {
                v = entry.getValue();
                vlen = v.length;
            }
            
            if (visitor.visit(k, v, 0, vlen, param, count++))
                break;
            
            if (matchBeforeVisit || !limited)
            {
                // continue
            }
            else if (!skRelativeLimit)
            {
                if (limit == ++limitCount) break;
            }
            else if (matchKeyLen == k.length && limit == ++limitCount)
            {
                matchBeforeVisit = true;
            }
        }
        
        if (keysToGet == null)
            return count;
        
        for (byte[] key : keysToGet)
        {
            v = map.get(key);
            if (v != null && visitor.visit(key, v, 0, v.length, param, count++))
                break;
        }
        
        return count;
    }
    
    static <T> int visitRangeDesc(NavigableMap<byte[],byte[]> map, 
            Comparator<byte[]> comparator, final int limit, 
            Visitor<T> visitor, T param,
            boolean valueAsKey, boolean skRelativeLimit, 
            byte[] startCompare, byte[] endCompare, boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        final ArrayList<byte[]> keysToGet = valueAsKey && !keysOnly ? 
                new ArrayList<byte[]>() : null;
        final int matchKeyLen = !skRelativeLimit ? 0 : 
                resolveMatchKeyLen(startCompare.length);
        
        int count = 0, vlen = 0, limitCount = 0;
        byte[] k, v = null;
        for(Map.Entry<byte[], byte[]> entry : map.descendingMap().subMap(
                endCompare, false, startCompare, false).entrySet())
        {
            k = entry.getKey();
            
            if (keysOnly)
            {
                if (valueAsKey)
                    k = entry.getValue();
            }
            else if (valueAsKey)
            {
                keysToGet.add(KeyUtil.extractFrom(entry.getValue()));
                continue;
            }
            else
            {
                v = entry.getValue();
                vlen = v.length;
            }
            
            if (visitor.visit(k, v, 0, vlen, param, count++))
                break;
            
            if (limited && (!skRelativeLimit || matchKeyLen == k.length) && limit == ++limitCount)
                break;
        }
        
        if (keysToGet == null)
            return count;
        
        for (byte[] key : keysToGet)
        {
            v = map.get(key);
            if (v != null && visitor.visit(key, v, 0, v.length, param, count++))
                break;
        }
        
        return count;
    }
    
    static <T> int visitRangeWithIndexKeyAsc(NavigableMap<byte[],byte[]> map, 
            Comparator<byte[]> comparator, final int limit, 
            byte[] indexKey, boolean inclusiveIndexKey, 
            Visitor<T> visitor, T param,
            boolean valueAsKey, boolean skRelativeLimit, 
            byte[] endCompare, boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        final ArrayList<byte[]> keysToGet = valueAsKey && !keysOnly ? 
                new ArrayList<byte[]>() : null;
        final int matchKeyLen = !skRelativeLimit ? 0 : 
                resolveMatchKeyLen(endCompare.length - 1);
        int count = 0, vlen = 0, limitCount = 0;
        byte[] k, v = null;
        boolean matchBeforeVisit = false;
        for(Map.Entry<byte[], byte[]> entry : map.subMap(
                indexKey, inclusiveIndexKey, endCompare, false).entrySet())
        {
            k = entry.getKey();
            if (matchBeforeVisit && matchKeyLen == k.length)
                break;
            
            if (keysOnly)
            {
                if (valueAsKey)
                    k = entry.getValue();
            }
            else if (valueAsKey)
            {
                keysToGet.add(KeyUtil.extractFrom(entry.getValue()));
                continue;
            }
            else
            {
                v = entry.getValue();
                vlen = v.length;
            }
            
            if (visitor.visit(k, v, 0, vlen, param, count++))
                break;
            
            if (matchBeforeVisit || !limited)
            {
                // continue
            }
            else if (!skRelativeLimit)
            {
                if (limit == ++limitCount) break;
            }
            else if (matchKeyLen == k.length && limit == ++limitCount)
            {
                matchBeforeVisit = true;
            }
        }
        
        if (keysToGet == null)
            return count;
        
        for (byte[] key : keysToGet)
        {
            v = map.get(key);
            if (v != null && visitor.visit(key, v, 0, v.length, param, count++))
                break;
        }
        
        return count;
    }
    
    static <T> int visitRangeWithIndexKeyDesc(NavigableMap<byte[],byte[]> map, 
            Comparator<byte[]> comparator, final int limit, 
            byte[] indexKey, boolean inclusiveIndexKey, 
            Visitor<T> visitor, T param,
            boolean valueAsKey, boolean skRelativeLimit, 
            byte[] startCompare, boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        final ArrayList<byte[]> keysToGet = valueAsKey && !keysOnly ? 
                new ArrayList<byte[]>() : null;
        final int matchKeyLen = !skRelativeLimit ? 0 : 
                resolveMatchKeyLen(startCompare.length);
        int count = 0, vlen = 0, limitCount = 0;
        byte[] k, v = null;
        for(Map.Entry<byte[], byte[]> entry : map.descendingMap().subMap(
                indexKey, inclusiveIndexKey, startCompare, false).entrySet())
        {
            k = entry.getKey();
            
            if (keysOnly)
            {
                if (valueAsKey)
                    k = entry.getValue();
            }
            else if (valueAsKey)
            {
                keysToGet.add(KeyUtil.extractFrom(entry.getValue()));
                continue;
            }
            else
            {
                v = entry.getValue();
                vlen = v.length;
            }
            
            if (visitor.visit(k, v, 0, vlen, param, count++))
                break;
            
            if (limited && (!skRelativeLimit || matchKeyLen == k.length) && limit == ++limitCount)
                break;
        }
        
        if (keysToGet == null)
            return count;
        
        for (byte[] key : keysToGet)
        {
            v = map.get(key);
            if (v != null && visitor.visit(key, v, 0, v.length, param, count++))
                break;
        }
        
        return count;
    }
}
