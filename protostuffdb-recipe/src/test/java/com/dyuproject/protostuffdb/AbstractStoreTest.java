//================================================================================
//Copyright (c) 2011, David Yu
//All rights reserved.
//--------------------------------------------------------------------------------
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of protostuff nor the names of its contributors may be used
//    to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//================================================================================


package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.DatastoreManager.CREATE_IF_MISSING;

import java.io.File;
import java.io.IOException;

import protostuffdb.Jni;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.FilterOutput;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.ProtostuffOutput;
import com.dyuproject.protostuff.RpcError;
import com.dyuproject.protostuff.RpcHeader;
import com.dyuproject.protostuff.RpcProtocol;
import com.dyuproject.protostuff.RpcResponse;
import com.dyuproject.protostuff.RpcValidation;
import com.dyuproject.protostuff.RpcWorker;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.ds.ParamKey;
import com.dyuproject.protostuff.ds.ParamRangeKey;

/**
 * Base test for datastore ops.
 *
 * @author David Yu
 * @created Jan 10, 2012
 */
public abstract class AbstractStoreTest extends AbstractTest
{
    
    public static final boolean TEST_LSMDB = Boolean.getBoolean("test.lsmdb");
    
    static
    {
        if (TEST_LSMDB)
        {
            protostuffdb.Jni.loadNativeLibrary();
            protostuffdb.LsmdbJni.setup(com.dyuproject.protostuff.JniStream.BAO);
        }
    }
    
    public static <T extends Message<T>> void assertInitialized(T message)
    {
        assertTrue(message.cachedSchema().isInitialized(message));
        
        if(RpcValidation.hasErrors())
        {
            System.err.println(RpcValidation.getMessages());
            
            fail("Validation failed on " + message.cachedSchema().messageFullName());
        }
    }
    
    public static final class MockOutput extends FilterOutput<ProtostuffOutput>
    {
        
        public int count;
        public int fieldCount;
        MockResponse res;

        public MockOutput()
        {
            super(new ProtostuffOutput(buf()));
        }
        

        @Override
        public <T> void writeObject(int fieldNumber, T value, Schema<T> schema, 
                boolean repeated) throws IOException
        {
            if (res.scopeCount == 0)
                res.rawNestedCount++;
            
            count++;
            //super.writeObject(fieldNumber, value, schema, repeated);
        }


        @Override
        public void writeBool(int fieldNumber, boolean value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeByteArray(int fieldNumber, byte[] value, boolean repeated)
                throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeByteRange(boolean utf8String, int fieldNumber, byte[] value, int offset,
                int length, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeBytes(int fieldNumber, ByteString value, boolean repeated)
                throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeDouble(int fieldNumber, double value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeEnum(int fieldNumber, int value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeFixed32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeFixed64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeFloat(int fieldNumber, float value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeInt32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeInt64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeSFixed32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeSFixed64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeSInt32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeSInt64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeString(int fieldNumber, String value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeUInt32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }


        @Override
        public void writeUInt64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            if(count == 0)
                fieldCount++;
        }
    }
    
    public static final class MockResponse extends RpcResponse
    {
        public int rawNestedCount, scopeCount;
        
        public final MockOutput mockOutput;
        
        public MockResponse(MockOutput output, WriteContext context)
        {
            super(output, context, RpcProtocol.DEFAULT);
            
            this.mockOutput = output;
            
            output.res = this;
        }
        
        public RpcWorker getWorker()
        {
            return RpcWorker.get();
        }
        
        @Override
        protected <T> boolean fail(RpcError error, T message, Schema<T> fSchema) throws IOException
        {
            return false;
        }
        
        @Override
        protected void end() throws IOException
        {
            
        }

        @Override
        public <T> Schema<?> scope(int fieldNumber, boolean push, Schema<T> schema, boolean repeated)
                throws IOException
        {
            if (push)
                scopeCount++;
            else if (0 == --scopeCount)
                rawNestedCount++;
            return null;
        }
    }
    
    private LsmdbDatastoreManager manager;
    
    protected Datastore store;
    
    protected final WriteContext context = new WriteContext(Jni.bufDb(0), Jni.bufTmp(0), 
            0, Jni.PARTITION_SIZE);
    
    protected MockResponse res;
    protected MockOutput output;
    
    protected RpcHeader header;
    protected final RpcWorker worker = new RpcWorker(0, Jni.buf(0), Jni.cbuf(0), null, context);

    protected File getDataDir()
    {
        return new File("target/data");
    }
    
    protected String getStoreName()
    {
        return "test";
    }

    @Override
    protected void setUp() throws Exception
    {
        if(TEST_LSMDB)
        {
            File dataDir = getDataDir();
            File dir = new File(dataDir, getStoreName());
            if(dir.exists())
            {
                for(File f : dir.listFiles())
                    f.delete();
            }
            
            manager = new LsmdbDatastoreManager(Jni.bufDb(0), dataDir);
            store = manager.getStore(getStoreName(), CREATE_IF_MISSING);
        }
        else
        {
            store = new TreeMapDatastore();
        }
        
        output = new MockOutput();
        
        res = new MockResponse(output, context);
        
        RpcWorker.set(worker);
        
        header = worker.header;
        
        worker.session.clear();
        
        init();
        
        super.setUp();
    }
    
    protected abstract void init();

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
        
        if(manager != null)
            manager.close();
    }
    
    protected MockResponse resetCounts()
    {
        return reset(res);
    }

    protected static MockResponse reset(MockResponse res)
    {
        res.scopeCount = 0;
        res.rawNestedCount = 0;
        res.mockOutput.count = 0;
        res.mockOutput.fieldCount = 0;
        
        return res;
    }
    
    protected static void assertNestedCount(int count, MockResponse res)
    {
        if (res.rawNestedCount == 0)
            assertEquals(count, res.mockOutput.count);
        else
            assertEquals(count, res.rawNestedCount);
    }
    
    /**
     * Shortcut for {@link #assertNestedCount(int, MockResponse)} and 
     * {@link #reset(MockResponse)}.
     */
    protected static void anc(int count, MockResponse res)
    {
        assertNestedCount(count, res);
        reset(res);
    }
    
    /**
     * Shortcut for {@link #assertNestedCount(int, MockResponse)} and 
     * {@link #reset(MockResponse)}.
     */
    protected static void ac(MockResponse res)
    {
        if (res.mockOutput.count == 0)
            assertTrue(0 != res.mockOutput.fieldCount);

        // success since rawCount is not zero
        reset(res);
    }
    
    protected <T extends Message<T>> T parseFrom(KV kv, EntityMetadata<T> em)
    {
        T message = em.pipeSchema.wrappedSchema.newMessage();
        
        context.mergeFrom(kv.value, message, message.cachedSchema());
        
        assertTrue(message.cachedSchema().isInitialized(message));
        
        if(RpcValidation.hasErrors())
        {
            System.err.println(RpcValidation.getMessages());
            
            fail("Validation failed on " + message.cachedSchema().messageFullName());
        }
        
        return message;
    }
    
    protected static ParamRangeKey prk()
    {
        return new ParamRangeKey(false);
    }
    
    protected static ParamRangeKey prk(byte[] parentKey)
    {
        ParamRangeKey prk = new ParamRangeKey(false);
        prk.parentKey = parentKey;
        return prk;
    }
    
    protected static ParamKey pk(byte[] key)
    {
        return new ParamKey(key);
    }
    
    /*protected static PS pacrk(String str)
    {
        return PS.create(str);
    }
    
    protected static PS pacrk(String str, byte[] parentKey)
    {
        PS p = pacrk(str);
        p.prk.parentKey = parentKey;
        return p;
    }*/
}
