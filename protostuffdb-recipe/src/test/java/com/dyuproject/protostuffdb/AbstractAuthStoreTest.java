//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import com.dyuproject.protostuff.StringSerializer.STRING;

/**
 * TODO
 * 
 * @author David Yu
 * @created Mar 14, 2017
 */
public abstract class AbstractAuthStoreTest extends AbstractStoreTest
{
    protected static final String USERNAME = "admin";
    protected static final String PASSWORD = "password@123Z";
    
    protected static final byte[] username = STRING.ser(USERNAME);
    protected static final byte[] password = STRING.ser(PASSWORD);
    
    protected abstract void addRootUser(String firstName, String lastName, 
            String username, String password,
            Datastore store, WriteContext context);
    
    protected abstract boolean isSamePassword(String pw, byte[] credential);
    
    protected abstract byte[] newToken(byte[] credential, byte[] username);

    protected void init()
    {
        // create admin user
        String pw = PASSWORD;
        
        addRootUser("John", "Doe", USERNAME, pw, store, context);
        
        byte[] credential = store.get(username, context);
        
        assertNotNull(credential);
        
        assertTrue(isSamePassword(PASSWORD, credential));
        
        // hack to 
        header.authToken = newToken(credential, username);
    }
}
