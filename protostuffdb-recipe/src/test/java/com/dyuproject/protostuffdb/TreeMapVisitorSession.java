//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.TreeMapDatastore.visitRangeAsc;
import static com.dyuproject.protostuffdb.TreeMapDatastore.visitRangeDesc;
import static com.dyuproject.protostuffdb.TreeMapDatastore.visitRangeWithIndexKeyAsc;
import static com.dyuproject.protostuffdb.TreeMapDatastore.visitRangeWithIndexKeyDesc;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;

import com.dyuproject.protostuff.KeyBuilder;

/**
 * A visitor session using TreeMap.
 *
 * @author David Yu
 * @created Aug 19, 2011
 */
public class TreeMapVisitorSession extends VisitorSession
{
    
    final NavigableMap<byte[],byte[]> map;
    final Comparator<byte[]> comparator;
    
    public TreeMapVisitorSession(NavigableMap<byte[],byte[]> map, 
            Comparator<byte[]> comparator)
    {
        this.map = map;
        this.comparator = comparator;
    }
    
    public byte[] rawGet(byte[] key, int koffset, int klen)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        return map.get(k);
    }
    
    public boolean rawExists(byte[] key, int koffset, int klen)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        return map.containsKey(k);
    }
    
    @Override
    public boolean exists(boolean prefix, KeyBuilder kb, boolean pop)
    {
        byte[] key = ValueUtil.copy(kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len());
        if (!prefix)
            return map.containsKey(key);
        
        byte[] higherKey = map.higherKey(key);
        return higherKey != null && higherKey.length >= key.length && 
                ValueUtil.isEqual(key, higherKey, 0, key.length);
    }

    @Override
    public boolean get(HasKV kv, KeyBuilder kb, boolean pop)
    {
        byte[] key = ValueUtil.copy(kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len());
        byte[] value = map.get(key);
        if (value == null)
            return false;
        
        kv.setValue(value);
        return true;
    }
    
    @Override
    public boolean pget(HasKV kv, boolean includeValue, boolean extractEntityKey, 
            KeyBuilder kb, boolean pop)
    {
        byte[] key = ValueUtil.copy(kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len());
        Map.Entry<byte[],byte[]> entry = map.higherEntry(key);
        final byte[] higherKey;
        if (entry == null 
                || (higherKey = entry.getKey()).length < key.length
                || !ValueUtil.isEqual(key, higherKey, 0, key.length))
        {
            return false;
        }
        
        if (includeValue)
            kv.setValue(entry.getValue());
        
        kv.setKey(extractEntityKey ? KeyUtil.extractFrom(higherKey) : higherKey);
        return true;
    }
    
    // ======================================================================
    
    
    public <T> boolean exists(byte[] key, EntityMetadata<T> em, byte[] parentKey)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                KeyUtil.validateSameKind(key, em);
                
                return map.containsKey(key);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw DSRuntimeExceptions.invalidArg("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    return map.containsKey(key);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return map.containsKey(KeyUtil.newOneToManyKey(key, parentKey));
        }
        
        throw DSRuntimeExceptions.invalidArg("Exists operation not applicable for type: " + 
                em.type);
    }
    
    public <T> byte[] get(byte[] key, EntityMetadata<T> em, byte[] parentKey)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                KeyUtil.validateSameKind(key, em);
                
                return map.get(key);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw DSRuntimeExceptions.invalidArg("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    return map.get(key);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return map.get(KeyUtil.newOneToManyKey(key, parentKey));
        }
        
        throw DSRuntimeExceptions.invalidArg("Get operation not applicable for type: " + 
                em.type);
    }
    
    public <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            boolean extractEntityKey, Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys");
        
        byte[] k, v;
        int count = 0;
        if(reverse)
        {
            for(int i = keys.size(); i-->0;)
            {
                k = keys.get(i);
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i);
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        
        return count;
    }
    
    public <V> int visitHasKeys(List<? extends HasKey> keys, boolean reverse, 
            boolean extractEntityKey, Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys");
        
        byte[] k, v;
        int count = 0;
        if(reverse)
        {
            for(int i = keys.size(); i-->0;)
            {
                k = keys.get(i).getKey();
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i).getKey();
                if(extractEntityKey)
                    k = KeyUtil.extractFrom(k);
                
                v = map.get(k);
                if(v != null && visitor.visit(k, v, 0, v.length, param, count++))
                    break;
            }
        }
        
        return count;
    }
    
    public <T,V> int visitKind(EntityMetadata<T> em, int limit, boolean desc, byte[] startKey, byte[] parentKey, Visitor<V> visitor, V param)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                if(startKey == null)
                {
                    final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
                    final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
                    
                    return desc ? visitRangeDesc(map, comparator,limit, 
                            visitor, param, false, false, sk, ek, false) : visitRangeAsc(map, 
                                    comparator, limit, visitor, param, false, false, 
                                    sk, ek, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                if(desc)
                {
                    return visitRangeWithIndexKeyDesc(map, comparator, limit, startKey, 
                            false, visitor, param, false, false, 
                            KeyUtil.newEntityRangeKeyStart(em.kind), false);
                }
                
                    
                return visitRangeWithIndexKeyAsc(map, comparator, limit, startKey, 
                        false, visitor, param, false, false, 
                        KeyUtil.newEntityRangeKeyEnd(em.kind), false);
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null");
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                if(startKey == null)
                {
                    final byte[] sk = KeyUtil.newLinkRangeKeyStart(em.kind, parentKey);
                    final byte[] ek = KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey);
                    
                    return desc ? visitRangeDesc(map, comparator,limit, 
                            visitor, param, false, false, sk, ek, false) : visitRangeAsc(map, 
                                    comparator, limit, visitor, param, false, false, sk, ek, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                final byte[] ik = KeyUtil.newLinkIndexKey(em.kind, parentKey, startKey);
                
                if(desc)
                {
                    return visitRangeWithIndexKeyDesc(map, comparator, limit, ik, 
                            false, visitor, param, false, false, 
                            KeyUtil.newLinkRangeKeyStart(em.kind, parentKey), 
                            false);
                }
                
                return visitRangeWithIndexKeyAsc(map, comparator, limit, ik, 
                        false, visitor, param, false, false, 
                        KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey), 
                        false);
                
        }
        
        throw DSRuntimeExceptions.invalidArg("Visit operation not applicable for type: " + 
                em.type);
    }
    
    public <V> int scan(boolean keysOnly, int limit, boolean desc, 
            Visitor<V> visitor, V param)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        int count = 0;
        
        Set<Map.Entry<byte[],byte[]>> set = desc ? map.descendingMap().entrySet() : 
            map.entrySet();
        
        byte[] v = null;
        int vlen = 0;
        for(Map.Entry<byte[], byte[]> entry : set)
        {
            if(!keysOnly)
            {
                v = entry.getValue();
                vlen = v.length;
            }
            
            if(visitor.visit(entry.getKey(), v, 0, vlen, param, 
                    count++) || (limited && count == limit))
            {
                break;
            }
        }
        
        return count;
    }
    
    public <V> int visitRange(
            boolean keysOnly, 
            int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param,
            boolean valueAsKey, boolean skRelativeLimit,
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen)
    {
        if(rawStartKey == null)
        {
            byte[] startKey = ValueUtil.copy(sk, skoffset, sklen), 
                    endKey = ValueUtil.copy(ek, ekoffset, eklen);
            
            return desc ? visitRangeDesc(map, comparator, limit, visitor, param, valueAsKey, skRelativeLimit, 
                    startKey, endKey, keysOnly) : visitRangeAsc(map, comparator, limit, 
                            visitor, param, valueAsKey, skRelativeLimit, startKey, endKey, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, sk, skoffset, sklen) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, ek, ekoffset, eklen) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            byte[] startKey = ValueUtil.copy(sk, skoffset, sklen);
            
            return visitRangeWithIndexKeyDesc(map, comparator, limit, rawStartKey, 
                    false, visitor, param, valueAsKey, skRelativeLimit, startKey, keysOnly);
        }
        
        byte[] endKey = ValueUtil.copy(ek, ekoffset, eklen);
        
        return visitRangeWithIndexKeyAsc(map, comparator, limit, rawStartKey, 
                false, visitor, param, valueAsKey, skRelativeLimit, endKey, keysOnly);
    }
    
    public <V> int visitIndex(
            boolean keysOnly, 
            int limit, boolean desc, 
            byte[] rawStartKey,
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop)
    {
        int offset = kb.offset(), size = pop ? kb.popLen() : kb.len();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            // append 0x00
            buf[offset+size] = 0;
            final byte[] sk = new byte[size+1];
            System.arraycopy(buf, offset, sk, 0, size+1);
            
            // append 0x01
            buf[offset+size] = (byte)1;
            final byte[] ek = new byte[size+1];
            System.arraycopy(buf, offset, ek, 0, size+1);
            
            return desc ? visitRangeDesc(map, comparator, limit, 
                    visitor, param, false, false, sk, ek, keysOnly) : visitRangeAsc(map, 
                            comparator, limit, visitor, param, false, false, sk, ek, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            // append 0x00
            buf[offset+size] = 0;
            final byte[] sk = new byte[size+1];
            System.arraycopy(buf, offset, sk, 0, size+1);
            
            return visitRangeWithIndexKeyDesc(map, comparator, limit, rawStartKey, 
                    false, visitor, param, false, false, sk, keysOnly);
        }
        
        // append 0x01
        buf[offset+size] = (byte)1;
        final byte[] ek = new byte[size+1];
        System.arraycopy(buf, offset, ek, 0, size+1);
        
        return visitRangeWithIndexKeyAsc(map, comparator, limit, rawStartKey, 
                false, visitor, param, false, false, ek, keysOnly);
    }

    /*public <V> int visitIndexPrefixMatch(
            boolean keysOnly, 
            int limit, boolean desc,
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        int offset = kb.offset(), size = kb.popLen();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            final byte[] sk = new byte[size];
            System.arraycopy(buf, offset, sk, 0, size);
            
            // append 0xFF
            buf[offset+size] = (byte)0xFF;
            final byte[] ek = new byte[size+1];
            System.arraycopy(buf, offset, ek, 0, size+1);
            
            return desc ? visitRangeDesc(map, comparator, limit, 
                    visitor, param, sk, ek, keysOnly) : visitRangeAsc(map, 
                            comparator, limit, visitor, param, sk, ek, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            final byte[] sk = new byte[size];
            System.arraycopy(buf, offset, sk, 0, size);
            
            return visitRangeWithIndexKeyDesc(map, comparator, limit, rawStartKey, 
                    false, visitor, param, sk, keysOnly);
        }
        
        // append 0xFF
        buf[offset+size] = (byte)0xFF;
        final byte[] ek = new byte[size+1];
        System.arraycopy(buf, offset, ek, 0, size+1);
        
        return visitRangeWithIndexKeyAsc(map, comparator, limit, rawStartKey, 
                false, visitor, param, ek, keysOnly);
    }*/

}
