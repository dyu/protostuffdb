//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.test;

import static com.dyuproject.protostuffdb.EntityMetadata.ZERO_KEY;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuffdb.AbstractVisitorTest;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.ListV;

/**
 * TODO
 * 
 * @author David Yu
 * @created Oct 19, 2016
 */
public abstract class AbstractPersonVisitorTest extends AbstractVisitorTest
{
    public void testIt() throws Exception
    {
        EntityMetadata<Person> em = Person.EM;
        int fieldNumber = Person.PList.FN_P;
        
        Person e = new Person()
            .provide(ZERO_KEY, ZERO_KEY, ZERO_KEY);
        
        final byte[] key = store.insert(e, e.em(), null, context);
        
        assertNotNull(key);
        
        ParamRangeKey req = new ParamRangeKey(false);
        
        assertTrue(ListV.list(e.em(), fieldNumber, req, store, res));
        anc(1, res);
        
        verify(e, em, fieldNumber, req);
    }
    
    protected abstract void verify(Person e, EntityMetadata<Person> em, 
            int fieldNumber, ParamRangeKey req) throws Exception;

}
