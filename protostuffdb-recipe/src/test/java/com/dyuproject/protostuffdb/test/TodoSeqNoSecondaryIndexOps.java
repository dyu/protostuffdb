package com.dyuproject.protostuffdb.test;

import com.dyuproject.protostuff.RpcHeader;
import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.WriteContext;

public final class TodoSeqNoSecondaryIndexOps
{
    private TodoSeqNoSecondaryIndexOps() {}

    static String validateAndProvide(TodoSeqNoSecondaryIndex req, long now, Datastore store,
            WriteContext context, RpcHeader header)
    {
        req.provide(now);
        return null;
    }    
}
