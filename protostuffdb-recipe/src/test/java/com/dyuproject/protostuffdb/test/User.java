// Generated by fbsgen from protostuffdb-recipe/src/test/proto/user.proto

package com.dyuproject.protostuffdb.test;

import java.io.IOException;
import java.util.ArrayList;


import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.UninitializedMessageException;
import com.dyuproject.protostuff.WireFormat.FieldType;
import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuff.ds.MultiCAS;
import com.dyuproject.protostuffdb.CachedEntity;
import com.dyuproject.protostuffdb.Entity;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.EntityType;

/**
 * <pre>// kind: 10
 * message User {
 *   optional bytes key = 1;
 *   optional uint64 ts = 2;
 *   required string alias = 3;
 *   optional bool active = 4;
 *   optional uint32 id = 5;
 * }
 * </pre>
 */
public final class User implements Message<User>, CachedEntity<User>
{
    public static final int KIND = 10;
    public static final int LAST_FN = 5;

    public static final int FN_KEY = 1;
    public static final int FN_TS = 2;
    public static final int FN_ALIAS = 3;
    public static final int FN_ACTIVE = 4;
    public static final int FN_ID = 5;

    /* ================================================== */
    // entity stuff

    public static final int VO_ID = 4;
    /** Mutable (provided). */
    public static final int VO_ACTIVE = 6;
    public static final CAS.Fixed64Op NOOP_TS = CAS.Fixed64Op.asNoop(FN_TS);

    /** Overrides unwanted ops. */
    public static MultiCAS override(MultiCAS mc)
    {
        return mc.addOp(NOOP_TS);
    }

    /* ================================================== */

    static final String ALIAS_REQUIRED_ERR_MESSAGE = "Alias is required.";


    public static final int DEFAULT_ID = 0;
    /** Optional. */
    public byte[] key;

    /** Optional. */
    public long ts;

    /** Required. */
    public String alias;

    /** Optional. */
    public boolean active = true;

    /** Optional. */
    public int id = DEFAULT_ID;


    public byte[] value;

    public User() {}

    public User(
        String alias
    )
    {
        this.alias = alias;
    }


    /**
     * Must be called before persisting this entity.
     */
    public User provide(long ts, int id)
    {
        this.key = null;
        this.ts = ts;
        active = true;
        this.id = id;
        return this;
    }



    public byte[] getKey() { return key; };
    public void setKey(byte[] key) { this.key = key; }
    public long getTs() { return ts; };
    public void setTs(long ts) { this.ts = ts; }
    public byte[] getValue() { return value; }
    public void setValue(byte[] value) { this.value = value; }
    public void setKV(byte[] key, byte[] value) { this.key = key; this.value = value; }
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public EntityMetadata<User> em() { return EM; }

    public Schema<User> cachedSchema() { return SCHEMA; }

    static final Schema<User> SCHEMA = new Schema<User>()
    {
        // schema methods

        public User newMessage()
        {
            return new User();
        }

        public Class<User> typeClass()
        {
            return User.class;
        }

        public String messageName()
        {
            return User.class.getSimpleName();
        }

        public String messageFullName()
        {
            return User.class.getName();
        }

        public boolean isInitialized(final User message)
        {
            com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    ALIAS_REQUIRED_ERR_MESSAGE, message.alias);







            return true;
        }
        public void mergeFrom(final Input input, final User message) throws IOException
        {
            for (int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch (number)
                {
                    case 0:
                        return;
                    case 1:
                        message.key = input.readByteArray();
                        break;
                    case 2:
                        message.ts = input.readFixed64();
                        break;
                    case 3:
                        message.alias = input.readString();
                        break;
                    case 4:
                        message.active = input.readBool();
                        break;
                    case 5:
                        message.id = input.readFixed32();
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }

        public void writeTo(final Output output, final User message) throws IOException
        {
            if (message.key != null)
                output.writeByteArray(1, message.key, false);



            if (message.ts != 0)
                output.writeFixed64(2, message.ts, false);




            if (message.alias == null)
                throw new UninitializedMessageException("alias", message);
            output.writeString(3, message.alias, false);


            output.writeBool(4, message.active, false);




            output.writeFixed32(5, message.id, false);




        }

        public String getFieldName(final int number)
        {
            switch(number)
            {
                case 1: return "key";
                case 2: return "ts";
                case 3: return "alias";
                case 4: return "active";
                case 5: return "id";
                default: return null;
            }
        }

        public int getFieldNumber(final String name)
        {
            final Integer number = fieldMap.get(name);
            return number == null ? 0 : number.intValue();
        }

        final java.util.HashMap<String,Integer> fieldMap = new java.util.HashMap<String,Integer>();
        {
            fieldMap.put("key", 1);
            fieldMap.put("ts", 2);
            fieldMap.put("alias", 3);
            fieldMap.put("active", 4);
            fieldMap.put("id", 5);
        }

    };

    /**
     * Useful for filtering (called by your custom pipe schema).
     */
    public static void transferField(int number, Pipe pipe, Input input, Output output, 
            Schema<User> wrappedSchema) throws IOException
    {
        switch (number)
        {
            case 1:
                input.transferByteRangeTo(output, false, number, false);
                break;
            case 2:
                output.writeFixed64(number, input.readFixed64(), false);
                break;
            case 3:
                input.transferByteRangeTo(output, true, number, false);
                break;
            case 4:
                output.writeBool(number, input.readBool(), false);
                break;
            case 5:
                output.writeFixed32(number, input.readFixed32(), false);
                break;
            default:
                input.handleUnknownField(number, wrappedSchema);
        }
    }

    static final Pipe.Schema<User> PIPE_SCHEMA = new Pipe.Schema<User>(SCHEMA)
    {
        protected void transfer(final Pipe pipe, final Input input, final Output output) throws IOException
        {
            for (int number = input.readFieldNumber(wrappedSchema);
                    number != 0;
                    number = input.readFieldNumber(wrappedSchema))
            {
                transferField(number, pipe, input, output, wrappedSchema);
            }
        }
    };

    public static Schema<User> getSchema() { return SCHEMA; }
    public static Pipe.Schema<User> getPipeSchema() { return PIPE_SCHEMA; }

    public void mergeFrom(CAS cas, CAS.Query query)
    {
        CAS.BoolOp activeOp = query.getAppliedOp(4, cas);
        if (activeOp != null)
            this.active = activeOp.getS();

    }

    public static final EntityMetadata<User> EM = new EntityMetadata<User>(
            PIPE_SCHEMA, 10, 1, 
            EntityType.DEFAULT, null, 0, 0, 0, null)
    {

        public boolean isMutable(final int field) { return field == FN_ACTIVE; } // single mutable


        public FieldType getFieldType(final int field)
        {
            switch(field)
            {
                case FN_ALIAS: return FieldType.STRING;
                case FN_ACTIVE: return FieldType.BOOL;
                case FN_ID: return FieldType.FIXED32;
                default: return null;
            }
        }

    };



    /**
     * <pre>
     * message PList {
     *   repeated User p = 1;
     * }
     * </pre>
     */
    public static final class PList implements Message<PList>
    {
        public static final int FN_P = 1;
        /** Repeated. */
        public java.util.List<User> p;


        public PList() {}



        public void addP(User p)
        {
            if (this.p == null)
                this.p = new ArrayList<User>();
            this.p.add(p);
        }



        public Schema<PList> cachedSchema() { return SCHEMA; }

        static final Schema<PList> SCHEMA = new Schema<PList>()
        {
            // schema methods

            public PList newMessage()
            {
                return new PList();
            }

            public Class<PList> typeClass()
            {
                return PList.class;
            }

            public String messageName()
            {
                return PList.class.getSimpleName();
            }

            public String messageFullName()
            {
                return PList.class.getName();
            }

            public boolean isInitialized(final PList message)
            {


                return true;
            }
            public void mergeFrom(final Input input, final PList message) throws IOException
            {
                for (int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
                {
                    switch (number)
                    {
                        case 0:
                            return;
                        case 1:
                            if (message.p == null)
                                message.p = new ArrayList<User>();
                            message.p.add(input.mergeObject(null, User.getSchema()));

                            break;
                        default:
                            input.handleUnknownField(number, this);
                    }   
                }
            }

            public void writeTo(final Output output, final PList message) throws IOException
            {
                if (message.p != null)
                {
                    for (User p : message.p)
                    {
                        if (p != null)
                            output.writeObject(1, p, User.getSchema(), true);
                    }
                }


            }

            public String getFieldName(final int number)
            {
                return number == 1 ? "p" : null;
            }

            public int getFieldNumber(final String name)
            {
                return 1 == name.length() && 'p' == name.charAt(0) ? 1 : 0;
            }

        };

        /**
         * Useful for filtering (called by your custom pipe schema).
         */
        public static void transferField(int number, Pipe pipe, Input input, Output output, 
                Schema<PList> wrappedSchema) throws IOException
        {
            switch (number)
            {
                case 1:
                    output.writeObject(number, pipe, User.getPipeSchema(), true);
                    break;
                default:
                    input.handleUnknownField(number, wrappedSchema);
            }
        }

        static final Pipe.Schema<PList> PIPE_SCHEMA = new Pipe.Schema<PList>(SCHEMA)
        {
            protected void transfer(final Pipe pipe, final Input input, final Output output) throws IOException
            {
                for (int number = input.readFieldNumber(wrappedSchema);
                        number != 0;
                        number = input.readFieldNumber(wrappedSchema))
                {
                    transferField(number, pipe, input, output, wrappedSchema);
                }
            }
        };

        public static Schema<PList> getSchema() { return SCHEMA; }
        public static Pipe.Schema<PList> getPipeSchema() { return PIPE_SCHEMA; }



    }


}

