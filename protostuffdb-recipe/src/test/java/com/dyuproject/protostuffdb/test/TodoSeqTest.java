//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.test;

import java.io.IOException;
import java.util.Arrays;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuff.ds.MultiCAS;
import com.dyuproject.protostuff.ds.ParamKey;
import com.dyuproject.protostuff.ds.ParamUpdate;
import com.dyuproject.protostuffdb.AbstractStoreTest;
import com.dyuproject.protostuffdb.DSRuntimeExceptions;
import com.dyuproject.protostuffdb.DateTimeUtil;
import com.dyuproject.protostuffdb.KV;
import com.dyuproject.protostuffdb.KeyUtil;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.VisitorSession;

/**
 * TODO
 * 
 * @author David Yu
 * @created Apr 6, 2017
 */
public class TodoSeqTest extends AbstractStoreTest
{
    @Override
    protected void init()
    {
        
    }
    
    TodoSeq newTodoSeq(String title) throws IOException
    {
        TodoSeq message = new TodoSeq(title);
        assertInitialized(message);
        
        assertTrue(XTodoSeqOps.create(message, store, res, 
                TodoSeq.PList.getPipeSchema(), header));
        
        assertNotNull(message.key);
        
        return message;
    }
    
    public void testMaxSize() throws IOException
    {
        newTodoSeq("12345678901234");
    }
    public void testMaxSizeOverflow() throws IOException
    {
        try
        {
            newTodoSeq("123456789012345");
        }
        catch (DSRuntimeExceptions.Validation e)
        {
            assertTrue(e.getMessage().startsWith("Entity too large"));
            return;
        }
        fail("Expected validation exception");
    }
    
    public void testCRUD() throws IOException
    {
        // verify no existing entity
        assertEquals(0, store.visitKind(TodoSeq.EM, 10, false, null, null, context, 
                Visitor.NOOP, null));
        
        final TodoSeq entity = newTodoSeq("hello");
        byte[] value = store.get(entity.key, entity.em(), null, context);
        assertNotNull(value);
        // bool is the last field
        assertTrue(value[value.length - 1] == 0);
        
        final KeyBuilder kb = context.kb();
        final KV kv = new KV();
        // verify secondary index (materialized view)
        assertTrue(store.pget(kv, context, TodoSeq.$$COMPLETED(kb, false).$push()));
        assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
        assertTrue(Arrays.equals(value, kv.value));
        
        store.session(context, new VisitorSession.Handler<Void>()
        {
            @Override
            public void handle(VisitorSession session, Void param)
            {
                byte[] key = kv.key, value = kv.value;
                assertTrue(session.pget(kv, TodoSeq.$$COMPLETED(kb, false).$push()));
                assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
                assertTrue(Arrays.equals(value, kv.value));
                assertTrue(Arrays.equals(key, kv.key));
            }
        }, null);
        
        assertTrue(store.pget(kv, context, TodoSeq.$$TITLE(kb, entity.title).$push()));
        assertEquals(6 + 6 + 1, kv.value.length);
        assertEquals(entity.ts, KeyUtil.readTimestamp(kv.value, 0));
        assertEquals(DateTimeUtil.startOfDayMS(entity.ts), KeyUtil.readTimestamp(kv.value, 6));
        assertEquals(0, kv.value[kv.value.length - 1]);
        assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
        
        store.session(context, new VisitorSession.Handler<Void>()
        {
            @Override
            public void handle(VisitorSession session, Void param)
            {
                byte[] key = kv.key, value = kv.value;
                assertTrue(session.pget(kv, TodoSeq.$$TITLE(kb, entity.title).$push()));
                assertEquals(6 + 6 + 1, kv.value.length);
                assertEquals(entity.ts, KeyUtil.readTimestamp(kv.value, 0));
                assertEquals(DateTimeUtil.startOfDayMS(entity.ts), KeyUtil.readTimestamp(kv.value, 6));
                assertEquals(0, kv.value[kv.value.length - 1]);
                assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
                assertTrue(Arrays.equals(value, kv.value));
                assertTrue(Arrays.equals(key, kv.key));
            }
        }, null);
        
        // mark completed
        ParamUpdate req = new ParamUpdate(entity.key, 
                new MultiCAS().addOp(new CAS.BoolOp(TodoSeq.FN_COMPLETED, false, true)));
        assertInitialized(req);
        assertNull(XTodoSeqOps.update(req, store, context, header));
        
        // verify completed
        value = store.get(entity.key, entity.em(), null, context);
        assertNotNull(value);
        // bool is the last field
        assertTrue(value[value.length - 1] == 1);
        
        assertTrue(store.pget(kv, context, TodoSeq.$$COMPLETED(kb, true).$push()));
        assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
        assertTrue(Arrays.equals(value, kv.value));
        
        store.session(context, new VisitorSession.Handler<Void>()
        {
            @Override
            public void handle(VisitorSession session, Void param)
            {
                assertFalse(session.pget(kv, TodoSeq.$$COMPLETED(kb, false).$push()));
                byte[] key = kv.key, value = kv.value;
                assertTrue(session.pget(kv, TodoSeq.$$COMPLETED(kb, true).$push()));
                assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
                assertTrue(Arrays.equals(value, kv.value));
                assertTrue(Arrays.equals(key, kv.key));
            }
        }, null);
        
        assertTrue(store.pget(kv, context, TodoSeq.$$TITLE(kb, entity.title).$push()));
        assertEquals(6 + 6 + 1, kv.value.length);
        assertEquals(1, kv.value[kv.value.length - 1]);
        assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
        
        store.session(context, new VisitorSession.Handler<Void>()
        {
            @Override
            public void handle(VisitorSession session, Void param)
            {
                byte[] key = kv.key, value = kv.value;
                assertTrue(session.pget(kv, TodoSeq.$$TITLE(kb, entity.title).$push()));
                assertEquals(6 + 6 + 1, kv.value.length);
                assertEquals(1, kv.value[kv.value.length - 1]);
                assertTrue(Arrays.equals(entity.key, KeyUtil.extractFrom(kv.key)));
                assertTrue(Arrays.equals(value, kv.value));
                assertTrue(Arrays.equals(key, kv.key));
            }
        }, null);
        
        // delete
        assertNull(XTodoSeqOps.delete(new ParamKey(entity.key), store, context, header));
        // verify
        assertNull(store.get(entity.key, entity.em(), null, context));
        assertFalse(store.pget(kv, context, TodoSeq.$$COMPLETED(kb, true).$push()));
        assertFalse(store.pget(kv, context, TodoSeq.$$TITLE(kb, entity.title).$push()));
        
        store.session(context, new VisitorSession.Handler<Void>()
        {
            @Override
            public void handle(VisitorSession session, Void param)
            {
                assertFalse(session.pget(kv, TodoSeq.$$COMPLETED(kb, true).$push()));
                assertFalse(session.pget(kv, TodoSeq.$$TITLE(kb, entity.title).$push()));
            }
        }, null);
    }
}
