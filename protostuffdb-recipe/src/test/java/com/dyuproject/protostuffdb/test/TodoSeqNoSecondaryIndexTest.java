//========================================================================
//Copyright 2021 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.test;

import java.io.IOException;

import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuff.ds.MultiCAS;
import com.dyuproject.protostuff.ds.ParamKey;
import com.dyuproject.protostuff.ds.ParamUpdate;
import com.dyuproject.protostuffdb.AbstractStoreTest;
import com.dyuproject.protostuffdb.DSRuntimeExceptions;
import com.dyuproject.protostuffdb.Visitor;

/**
 * TODO
 * 
 * @author David Yu
 * @created Nov 23, 2021
 */
public class TodoSeqNoSecondaryIndexTest extends AbstractStoreTest
{
    @Override
    protected void init()
    {
        
    }
    
    TodoSeqNoSecondaryIndex newTodoSeqNoSecondaryIndex(String title) throws IOException
    {
        TodoSeqNoSecondaryIndex message = new TodoSeqNoSecondaryIndex(title);
        assertInitialized(message);
        
        assertTrue(XTodoSeqNoSecondaryIndexOps.create(message, store, res, 
                TodoSeqNoSecondaryIndex.PList.getPipeSchema(), header));
        
        assertNotNull(message.key);
        
        return message;
    }
    
    public void testMaxSize() throws IOException
    {
        newTodoSeqNoSecondaryIndex("12345678901234");
    }
    public void testMaxSizeOverflow() throws IOException
    {
        try
        {
            newTodoSeqNoSecondaryIndex("123456789012345");
        }
        catch (DSRuntimeExceptions.Validation e)
        {
            assertTrue(e.getMessage().startsWith("Entity too large"));
            return;
        }
        fail("Expected validation exception");
    }
    
    public void testCRUD() throws IOException
    {
        // verify no existing entity
        assertEquals(0, store.visitKind(TodoSeqNoSecondaryIndex.EM, 10, false, null, null, context, 
                Visitor.NOOP, null));
        
        final TodoSeqNoSecondaryIndex entity = newTodoSeqNoSecondaryIndex("hello");
        byte[] value = store.get(entity.key, entity.em(), null, context);
        assertNotNull(value);
        // bool is the last field
        assertTrue(value[value.length - 1] == 0);
        
        // mark completed
        ParamUpdate req = new ParamUpdate(entity.key, 
                new MultiCAS().addOp(new CAS.BoolOp(TodoSeqNoSecondaryIndex.FN_COMPLETED, false, true)));
        assertInitialized(req);
        assertNull(XTodoSeqNoSecondaryIndexOps.update(req, store, context, header));
        
        // verify completed
        value = store.get(entity.key, entity.em(), null, context);
        assertNotNull(value);
        // bool is the last field
        assertTrue(value[value.length - 1] == 1);
        
        // delete
        assertNull(XTodoSeqNoSecondaryIndexOps.delete(new ParamKey(entity.key), store, context, header));
        // verify
        assertNull(store.get(entity.key, entity.em(), null, context));
    }    
}
