//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.test;

import static com.dyuproject.protostuffdb.SerializedValueUtil.asInt32;

import com.dyuproject.protostuffdb.AbstractStoreTest;
import com.dyuproject.protostuffdb.SerialArrayCache;
import com.dyuproject.protostuffdb.ValueUtil;
import com.dyuproject.protostuffdb.Visitor;

/**
 * TODO
 * 
 * @author David Yu
 * @created Jul 11, 2017
 */
public class ArrayCacheFillTest extends AbstractStoreTest
{
    
    static final int SEGMENT_SIZE = 512;
    
    SerialArrayCache cache;
    
    @Override
    protected void init()
    {
        cache = new SerialArrayCache(User.VO_ID, SEGMENT_SIZE);
    }
    
    public void testIt()
    {
        final int minEntitySize = 1 + 8 + // tag, ts
                1 + 1 + 1 + // tag, delim, alias
                1 + 1 + // tag, active
                1 + 4; // tag, id
                
        final int loops = 0xFFFF/minEntitySize + 1;
        final long ts = System.currentTimeMillis();
        byte[] key = null;
        for (int i = 0, id; i < loops; i++)
        {
            id = i + 1;
            key = context.newEntityKey(User.EM);
            User entity = new User(Integer.toString(id)).provide(ts, id);
            store.insertWithKey(key, entity, entity.em(), null, context);
        }
        
        final long seqId = ValueUtil.toInt64(key, 1) >>> 2;
        assertTrue(loops == (int)seqId);
        // set sequenceId to zero
        Visitor.SET_SEQ_ID.visit(new byte[9], null, 0, 0, User.EM, 0);
        
        assertEquals(seqId, cache.fill(User.EM, store, context));
        assertEquals(loops, cache.size());
        
        byte[] lastKey = cache.$k(loops, context);
        int lastKeyOffset = context.$offset;
        assertTrue(ValueUtil.isEqual(key, lastKey, lastKeyOffset, 9));
        
        byte[] lastValue = cache.$v(loops, context);
        assertEquals(loops, asInt32(User.VO_ID, lastValue, context.$offset, context.$len));
        
        byte[] vk = cache.$vk(loops, context);
        int vkOffset = context.$offset, vkLen = context.$len;
        assertTrue(ValueUtil.isEqual(key, vk, vkOffset + vkLen - 9, 9));
        assertEquals(loops, asInt32(User.VO_ID, vk, vkOffset, vkLen - 9));
        
        assertEquals(seqId, ValueUtil.toInt64(lastKey, 1 + lastKeyOffset) >>> 2);
    }

}
