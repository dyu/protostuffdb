package com.dyuproject.protostuffdb.test;

import com.dyuproject.protostuff.RpcHeader;
import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.WriteContext;

public final class TodoNoSecondaryIndexOps
{
    private TodoNoSecondaryIndexOps() {}

    public static String validateAndProvide(TodoNoSecondaryIndex req, long now, Datastore store, 
            WriteContext context, RpcHeader header)
    {
        req.provide();
        return null;
    }
}
