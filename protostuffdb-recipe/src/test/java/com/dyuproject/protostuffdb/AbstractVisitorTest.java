//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.File;

import protostuffdb.Jni;

import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.RpcHeader;
import com.dyuproject.protostuff.RpcValidation;
import com.dyuproject.protostuff.RpcWorker;
import com.dyuproject.protostuff.StringSerializer.STRING;
import com.dyuproject.protostuffdb.AbstractStoreTest.MockOutput;
import com.dyuproject.protostuffdb.AbstractStoreTest.MockResponse;

/**
 * Base visitor test.
 * 
 * @author David Yu
 * @created Jul 8, 2013
 */
public abstract class AbstractVisitorTest extends AbstractTest
{
    
    static final String USERNAME = "username";
    static final String PASSWORD = "password@123Z";
    
    public static String prefix(String str, int len)
    {
        return str.substring(0, len);
    }
    
    public static <T extends Message<T>> void assertInitialized(T message)
    {
        assertTrue(message.cachedSchema().isInitialized(message));
        
        if(RpcValidation.hasErrors())
        {
            System.err.println(RpcValidation.getMessages());
            
            fail("Validation failed on " + message.cachedSchema().messageFullName());
        }
    }
    
    protected Datastore store;
    protected final WriteContext context = new WriteContext(Jni.bufDb(0), Jni.bufTmp(0), 
            0, Jni.PARTITION_SIZE);
    //protected ServiceProvider sp;
    
    protected MockResponse res;
    protected MockOutput output;
    
    protected static final byte[] username = STRING.ser(USERNAME);
    protected static final byte[] password = STRING.ser(PASSWORD);
    
    protected RpcHeader header;
    protected final RpcWorker worker = new RpcWorker(0, Jni.buf(0), Jni.cbuf(0), null, context);

    protected File getDataDir()
    {
        return new File("target/data");
    }
    
    protected String getStoreName()
    {
        return "test";
    }

    @Override
    protected void setUp() throws Exception
    {
        store = new TreeMapDatastore();
        
        //sp = new ServiceProvider(store, context);
        
        output = new MockOutput();
        
        res = new MockResponse(output, context/*, sp*/);
        
        RpcWorker.set(worker);
        
        header = worker.header;
        
        //User.PNew req = new User.PNew(new User("Mike", "Ekim", USERNAME), 
        //        PASSWORD, PASSWORD);
        
        //req.role = Arrays.asList(Role.values());
        
        //assertInitialized(req);
        
        //assertNull(UserServicesImpl.ForSuperAdmin.addNewUser(req, store, context));
        
        //byte[] credential = store.get(username);
        
        //assertNotNull(credential);
        
        //assertTrue(Auth.isSamePassword(PASSWORD, credential));
        
        // hack to 
        //header.authToken = Auth.newToken(credential, username);
        
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
        
        //manager.destroy();
    }
    
    protected void resetCounts()
    {
        reset(res);
    }

    protected static void reset(MockResponse res)
    {
        res.rawNestedCount = 0;
        ((MockOutput)res.output).count = 0;
    }
    
    protected static void assertNestedCount(int count, MockResponse res)
    {
        assertTrue(res.rawNestedCount == count || ((MockOutput)res.output).count == count);
    }
    
    /**
     * Shortcut for {@link #assertNestedCount(int, MockResponse)} and 
     * {@link #reset(MockResponse)}.
     */
    protected static void anc(int count, MockResponse res)
    {
        assertNestedCount(count, res);
        reset(res);
    }
}
