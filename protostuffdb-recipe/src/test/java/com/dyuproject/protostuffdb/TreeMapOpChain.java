//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.TreeMapDatastore.indexOnDelete;
import static com.dyuproject.protostuffdb.TreeMapDatastore.indexOnInsert;
import static com.dyuproject.protostuffdb.TreeMapDatastore.indexOnUpdate;
import static com.dyuproject.protostuffdb.TreeMapDatastore.linkIndexOnDelete;
import static com.dyuproject.protostuffdb.TreeMapDatastore.linkIndexOnWrite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;

import com.dyuproject.protostuff.ds.CAS;

/**
 * Chain of operations performed against a tree map (for test purposes).
 * 
 * TODO rollback handling
 *
 * @author David Yu
 * @created Mar 24, 2011
 */
public class TreeMapOpChain extends OpChain
{
    
    final NavigableMap<byte[],byte[]> map;

    public TreeMapOpChain(EntityMetadata<?> root, WriteContext context, 
            VisitorSession vs, 
            NavigableMap<byte[],byte[]> map)
    {
        super(root,context, vs);
        
        this.map = map;
    }
    
    protected VisitorSession newVisitorSession()
    {
        return new TreeMapVisitorSession(map, Comparators.DEFAULT);
    }
    
    public int rangeRemove(byte[] startKey, byte[] endKey)
    {
        final ArrayList<byte[]> keys = new ArrayList<byte[]>();
        for(Map.Entry<byte[], byte[]> entry : map.subMap(
                startKey, true, endKey, false).entrySet())
        {
            keys.add(entry.getKey());
        }
        
        final int total = keys.size();
        
        for(byte[] k : keys)
            map.remove(k);
        
        return total;
    }
    
    public int rangeReplace(byte[] startKey, byte[] endKey, 
            byte[] prefix, int offset, int len)
    {
        final ArrayList<Entry<byte[],byte[]>> entries = new ArrayList<Entry<byte[],byte[]>>();
        for(Map.Entry<byte[], byte[]> entry : map.subMap(
                startKey, true, endKey, false).entrySet())
        {
            entries.add(new MutableEntry<byte[],byte[]>(entry.getKey(), entry.getValue()));
        }
        
        final int total = entries.size();
        
        for(Entry<byte[],byte[]> entry : entries)
        {
            // remove old
            map.remove(entry.getKey());
            
            // put new
            System.arraycopy(prefix, offset, entry.getKey(), 0, len);
            map.put(entry.getKey(), entry.getValue());
        }
        
        return total;
    }
    
    public void rawPut(byte[] key, int koffset, int klen, 
            byte[] value, int voffset, int vlen)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        byte[] v = new byte[vlen];
        System.arraycopy(value, voffset, v, 0, vlen);
        
        map.put(k, v);
    }

    public void rawDelete(byte[] key, int koffset, int klen)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        map.remove(k);
    }

    /*public byte[] rawGet(byte[] key, int koffset, int klen)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        return map.get(k);
    }
    
    public boolean rawExists(byte[] key, int koffset, int klen)
    {
        byte[] k = new byte[klen];
        System.arraycopy(key, koffset, k, 0, klen);
        
        return map.containsKey(k);
    }*/
    
    // ======================================================================
    
    protected <T,P> boolean doDeleteWithValue(final byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        final byte[] removed = map.remove(key);
        if (removed == null || (parentKey == null && !Arrays.equals(value, removed)))
            return false;
        
        final byte[] indexKey;
        final int indexKeyOffset;
        if (parentKey == null)
        {
            indexKey = key;
            indexKeyOffset = 0;
        }
        else
        {
            indexKey = KeyUtil.newOneToManyKey(key, parentKey);
            indexKeyOffset = 9;
            
            // the actual value
            if (!Arrays.equals(value, map.remove(indexKey)))
                return false;
        }
        
        if (em.indexOnFields)
            indexOnDelete(map, indexKey, indexKeyOffset, em, context, value);
        
        return nextOp == null ? true : nextOp.handle(this, key, 
                value, 0, value.length, param);
    }
    
    protected <T,P> boolean doDelete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        return delete(key, em, parentKey, nextOp, param, null, null, null);
    }
    
    protected <T,P,C> boolean doCascadeDelete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            Op<P> nextOp, P param, 
            EntityMetadataResource cascadeResource, 
            Op<C> cascadeOp, C cascadeParam)
    {
        return delete(key, em, parentKey, nextOp, param, 
                cascadeResource, cascadeOp, cascadeParam);
    }
    
    private <T,P,C> boolean delete(final byte[] key, final EntityMetadata<T> em, 
            final byte[] parentKey, 
            Op<P> nextOp, P param, 
            EntityMetadataResource cascadeResource, 
            Op<C> cascadeOp, C cascadeParam)
    {
        byte[] value = null;
        final boolean deleteOnScan = cascadeResource != null && 
                0 != em.linkIndex.length && 0 == em.linkIndex[0];
        
        if(!deleteOnScan)
        {
            // regular delete
            value = map.remove(key);
            if(value == null)
                return false;
            
            final byte[] indexKey;
            final int indexKeyOffset;
            if(parentKey == null)
            {
                indexKey = key;
                indexKeyOffset = 0;
            }
            else
            {
                indexKey = KeyUtil.newOneToManyKey(key, parentKey);
                indexKeyOffset = 9;
                
                // the actual value
                value = map.remove(indexKey);
                if (value == null)
                    return false;
            }
            
            if(em.indexOnFields)
                indexOnDelete(map, indexKey, indexKeyOffset, em, context, value);
            
            if(cascadeResource == null && 0 != em.linkIndex.length)
                linkIndexOnDelete(map, indexKey, indexKeyOffset, em);
        }
        
        if(cascadeResource != null)
        {
            if(value != null && 0 != em.linkIndex.length && 0 == em.linkIndex[0])
                context.data(em.kind, value);
            
            ArrayList<byte[]> cascadeKeys = new ArrayList<byte[]>();
            if(!deleteRange(this, map, context, Comparators.DEFAULT, 
                            cascadeResource, cascadeOp, cascadeParam,  
                            deleteOnScan ? em : null, key, parentKey, 
                            cascadeKeys, 0, 
                            KeyUtil.newLinkRangeKeyStart(key), 
                            KeyUtil.newLinkRangeKeyEnd(key)))
            {
                return false;
            }
            
            if(deleteOnScan)
            {
                // the actual value
                value = cascadeKeys.get(0);
            }
        }
        
        return nextOp == null ? true : nextOp.handle(this, key, 
                value, 0, value.length, param);
    }
    
    private static <T,C> boolean deleteRange(TreeMapOpChain chain, 
            NavigableMap<byte[],byte[]> map, 
            WriteContext context, Comparator<byte[]> comparator, 
            EntityMetadataResource cascadeResource, 
            Op<C> cascadeOp, C cascadeParam, 
            EntityMetadata<?> emTD, byte[] keyTD, byte[] parentKeyTD, 
            ArrayList<byte[]> cascadeKeys, int cascadeKeysStart, 
            byte[] startCompare, byte[] endCompare)
    {
        int iterCount = 0, cascadeCount = 0, lastKind = 0;
        EntityMetadata<?> em = null;
        for(Map.Entry<byte[], byte[]> entry : map.subMap(
                startCompare, true, endCompare, false).entrySet())
        {
            iterCount++;
            
            final byte[] oneToManyKey = entry.getKey();
            if(oneToManyKey.length == 10)
            {
                // link index
                if(emTD != null)
                {
                    if(0 != oneToManyKey[9])
                        throw DSRuntimeExceptions.runtime("Corrupt db.");
                    
                    byte[] valueTD = entry.getValue();
                    cascadeKeys.add(valueTD);
                    cascadeKeysStart++;
                    
                    // link index
                    map.remove(oneToManyKey);
                    
                    // entry or link-to-parent entry
                    map.remove(keyTD);
                    
                    int offsetTD = 0;
                    
                    if(parentKeyTD != null)
                    {
                        keyTD = KeyUtil.newOneToManyKey(keyTD, parentKeyTD);
                        offsetTD = 9;
                        
                        // actual entry
                        map.remove(keyTD);
                    }
                    
                    if(emTD.indexOnFields)
                        indexOnDelete(map, keyTD, offsetTD, emTD, context, valueTD);
                    
                    if(0 != emTD.linkIndex.length && 0 == emTD.linkIndex[0])
                        context.data(emTD.kind, valueTD);
                    
                    // unset
                    emTD = null;
                    continue;
                }
                
                map.remove(oneToManyKey);
                continue;
            }
            
            final byte[] value = entry.getValue();
            
            int kind = KeyUtil.getKind(oneToManyKey);
            assert kind != 0;
            
            if(kind != lastKind)
            {
                em = cascadeResource.getEntityMetadata(kind);
                if(em == null)
                {
                    throw DSRuntimeExceptions.runtime("Kind: " + kind + 
                            " not found on EntityMetadataResource.");
                }
                lastKind = kind;
            }
            
            final byte[] key = KeyUtil.keyFromLinkKey(oneToManyKey);
            
            // remove the link key entry
            map.remove(oneToManyKey);
            // remove the entity key entry
            map.remove(key);
            
            if(em.indexOnFields)
                indexOnDelete(map, oneToManyKey, 9, em, context, value);
            
            // cascade handler
            if(cascadeOp != null && !cascadeOp.handle(chain, oneToManyKey, 
                    value, 0, value.length, cascadeParam))
            {
                return false;
            }
            
            if(em.hasChildren())
            {
                if(0 != em.linkIndex.length && 0 == em.linkIndex[0])
                    context.data(em.kind, value);
                
                cascadeCount++;
                cascadeKeys.add(oneToManyKey);
            }
        }
        
        if(emTD != null && iterCount == 0)
        {
            // nothing was deleted
            return false;
        }
        
        for(int i = 0; i < cascadeCount; i++)
        {
            byte[] oneToManyKey = cascadeKeys.get(cascadeKeysStart++);
            
            if(!deleteRange(chain, map, context, comparator, 
                    cascadeResource, cascadeOp, cascadeParam, 
                    null, null, null, 
                    cascadeKeys, cascadeKeys.size(), 
                    KeyUtil.newLinkRangeKeyStart(oneToManyKey), 
                    KeyUtil.newLinkRangeKeyEnd(oneToManyKey)))
            {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Copy the key (new instance for test purposes) to avoid the internal {@link #map}'s 
     * side effects when the same instance (but different content) is used.
     */
    protected <T, P> boolean insert(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            Op<P> nextOp, P param)
    {
        key = ValueUtil.copy(key);
        final byte[] value = ValueUtil.copy(entityBuffer, entityOffset, size);
        
        final byte[] indexKey;
        final int indexKeyOffset;
        if(parentKey == null)
        {
            indexKey = key;
            indexKeyOffset = 0;
            
            map.put(key, value);
        }
        else
        {
            indexKey = KeyUtil.newOneToManyKey(key, parentKey);
            indexKeyOffset = 9;
            
            map.put(indexKey, value);
            map.put(key, parentKey);
        }
        
        if(em.indexOnFields)
        {
            indexOnInsert(map, indexKey, indexKeyOffset, em, context, 
                    output, entityBuffer, entityOffset, size, 
                    value);
        }
        
        if(0 != em.linkIndex.length)
            linkIndexOnWrite(map, indexKey, indexKeyOffset, em, value);
        
        return nextOp == null ? true : nextOp.handle(this, key, 
                entityBuffer, entityOffset, size, param);
    }
    
    protected <T, P> boolean doUpdate(byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener,  
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        return update(this, map, value, key, 
                parentKey == null ? key : KeyUtil.newOneToManyKey(key, parentKey), 
                cas, listener, em, context, nextOp, param);
    }
    
    private static <T,P> boolean update(OpChain chain, NavigableMap<byte[],byte[]> map, 
            byte[] value, byte[] key, byte[] searchKey, 
            CAS cas, CAS.Listener listener,  
            EntityMetadata<T> em, WriteContext context, 
            Op<P> nextOp, P param)
    {
        if(listener != null && !listener.onBeforeApply(searchKey, value, chain))
            return false;
        
        final int size = context.casAndIndexCollect(value, 0, value.length, em, cas);
        final CASAndIndexCollectOutput output = context.casAndIndexCollectOutput;
        
        if (output.casCount == 0)
            return false;
        switch (em.updateFlags)
        {
            case 1:
                if (output.casCount == 1 && output.isUpdated(em.revField, cas))
                    return false;
                break;
            case 2:
                if (output.casCount == 1 && output.isUpdated(em.updateTsField, cas))
                    return false;
                break;
            case 3:
                if (output.casCount == 2 && 
                    output.isUpdated(em.revField, cas) && 
                    output.isUpdated(em.updateTsField, cas))
                {
                    return false;
                }
                break;
        }
        
        final byte[] entityBuffer = context.entityBuffer;
        final int entityOffset = context.entityOffset;
        
        if(listener != null && !listener.onApply(searchKey, value, cas, output, chain, 
                entityBuffer, entityOffset, size))
        {
            return false;
        }
        
        final byte[] newValue = new byte[size];
        System.arraycopy(entityBuffer, entityOffset, newValue, 0, size);
        
        if (!Arrays.equals(value, map.put(searchKey, newValue)))
            throw DSRuntimeExceptions.unknownState("Should not happen. Sychhronized access only.");
        
        if(em.indexOnFields && (output.trackCount != 0 || em.index.valueCount != 0))
        {
            indexOnUpdate(map, output, searchKey, searchKey.length == 9 ? 0 : 9, 
                    em, context, entityBuffer, entityOffset, size, value, newValue);
        }
        
        if(0 != em.linkIndex.length)
            linkIndexOnWrite(map, searchKey, searchKey.length == 9 ? 0 : 9, em, newValue);
        
        return nextOp == null ? true : nextOp.handle(chain, key, 
                entityBuffer, entityOffset, size, param);
    }
    
}
