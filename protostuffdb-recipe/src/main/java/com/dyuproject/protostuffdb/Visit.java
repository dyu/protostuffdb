//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.SerializedValueUtil.i1;
import static com.dyuproject.protostuffdb.SerializedValueUtil.i4;
import static com.dyuproject.protostuffdb.SerializedValueUtil.i8;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.P1;
import com.dyuproject.protostuff.ds.P4;
import com.dyuproject.protostuff.ds.P8;
import com.dyuproject.protostuff.ds.PD;
import com.dyuproject.protostuff.ds.PK;
import com.dyuproject.protostuff.ds.PS;
import com.dyuproject.protostuff.ds.ParamRangeKey;

/**
 * Visit entries using a single filter.
 * 
 * @author David Yu
 * @created Jul 11, 2013
 */
public final class Visit
{
    private Visit() {}
    
    public static <T,S,V> boolean by1(int indexId, 
            boolean value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by1(indexId, 
                i1(value), 
                        em, fieldNumber, prk, rangeV, store, context, visitor, res);
    }
    
    public static <T,S,V> boolean by1(int indexId, 
            int value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append8(value)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append8(value)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append8(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append8(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append8(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append8(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean by1(int indexId, 
            P1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by1(indexId, 
                req.value, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean by1r(int indexId, 
            int value, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append8(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append8(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append8(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append8(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append8(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append8(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean by1r(int indexId, 
            P1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by1r(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean by4(int indexId, 
            float value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        return by4(indexId, 
                i4(value), 
                        em, fieldNumber, prk, rangeV, store, context, visitor, res);
    }
    
    public static <T,S,V> boolean by4(int indexId, 
            int value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append(value)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean by4(int indexId, 
            P4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by4(indexId, 
                req.value, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean by4r(int indexId, 
            int value, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean by4r(int indexId, 
            P4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by4r(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean by8(int indexId, 
            double value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        return by8(indexId, 
                i8(value), 
                        em, fieldNumber, prk, rangeV, store, context, visitor, res);
    }
    
    public static <T,S,V> boolean by8(int indexId, 
            long value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append(value)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean by8(int indexId, 
            P8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by8(indexId, 
                req.value, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean by8r(int indexId, 
            long value, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean by8r(int indexId, 
            P8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return by8r(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean byD(int indexId, 
            long value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append6Bytes(value)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append6Bytes(value)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append6Bytes(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append6Bytes(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append6Bytes(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append6Bytes(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean byD(int indexId, 
            PD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byD(indexId, 
                req.value, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean byDr(int indexId, 
            long value, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append6Bytes(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append6Bytes(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append6Bytes(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append6Bytes(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append6Bytes(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append6Bytes(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean byDr(int indexId, 
            PD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byDr(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean byK(int indexId, 
            byte[] value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append(value)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean byK(int indexId, 
            PK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byK(indexId, 
                req.value, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean byKr(int indexId, 
            byte[] value, byte[] end, byte[] pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .$append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean byKr(int indexId, 
            PK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byKr(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean byS(int indexId, 
            String value, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(value)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .append(value)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .append(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean byS(int indexId, 
            PS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byS(indexId, 
                req.value, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean bySr(int indexId, 
            String value, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .append(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean bySr(int indexId, 
            PS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return bySr(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean bySp(int indexId, 
            String value, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(value)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean bySp(int indexId, 
            PS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return bySp(indexId, 
                req.value, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
    
    /* ================================================== */
    
    public static <T,S,V> boolean bySpr(int indexId, 
            String value, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        
        if(prk.startKey != null)
        {
            if(prk.parentKey != null)
            {
                kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
            else
            {
                kb.begin(indexId, em)
                    .append(pgstart)
                    .$append(prk.startKey)
                    .$push();
            }
        }
        
        if(prk.parentKey != null)
        {
                    // from
            kb.begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(prk.parentKey)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        else
        {
                    // from
            kb.begin(indexId, em)
                    .$append(value)
                    .$push()
                    // to
                    .begin(indexId, em)
                    .$append(end)
                    .$append8(0xFF)
                    .$push();
        }
        
        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-2), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());
        
        return true;
    }
    
    public static <T,S,V> boolean bySpr(int indexId, 
            PS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return bySpr(indexId, 
                req.value, req.end, req.pgstart, 
                em, fieldNumber, req.prk, rangeV, store, context, visitor, res);
    }
}