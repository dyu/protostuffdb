//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Comparator;

/**
 * TODO
 * 
 * @author David Yu
 * @created Dec 27, 2016
 */
public final class MustBeUnique extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    public MustBeUnique(){}
    public MustBeUnique(String msg){super(msg);}
    
    @Override
    public Throwable fillInStackTrace()
    {
        return this;
    }
    
    public static final Comparator<String> CMP_STRING = new Comparator<String>()
    {
        @Override
        public int compare(String o1, String o2)
        {
            int ret = o1.compareTo(o2);
            if (ret == 0)
                throw new MustBeUnique();
            
            return ret;
        }
    };
    
    public static final Comparator<Integer> CMP_ID = new Comparator<Integer>()
    {
        @Override
        public int compare(Integer o1, Integer o2)
        {
            if (o1.intValue() == o2.intValue())
                throw new MustBeUnique();
            
            return o1.intValue() - o2.intValue();
        }
    };
    
    public static final Comparator<byte[]> CMP_KEY = new Comparator<byte[]>()
    {
        @Override
        public int compare(byte[] o1, byte[] o2)
        {
            int ret = ValueUtil.compare(o1, o2);
            if (ret == 0)
                throw new MustBeUnique();
            
            return ret;
        }
    };
    
    public static final Comparator<HasKey> CMP_HK = new Comparator<HasKey>()
    {
        @Override
        public int compare(HasKey o1, HasKey o2)
        {
            int ret = ValueUtil.compare(o1.getKey(), o2.getKey());
            if (ret == 0)
                throw new MustBeUnique();
            
            return ret;
        }
    };
    
    public static final Comparator<HasKey> CMP_HK_END = new Comparator<HasKey>()
    {
        @Override
        public int compare(HasKey o1, HasKey o2)
        {
            byte[] k1 = o1.getKey(), k2 = o2.getKey();
            int ret = ValueUtil.compare(k1, k1.length - 9, k1.length, 
                    k2, k2.length - 9, k2.length);
            
            if (ret == 0)
                throw new MustBeUnique();
            
            return ret;
        }
    };
}