//================================================================================
//Copyright (c) 2011, David Yu
//All rights reserved.
//--------------------------------------------------------------------------------
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of protostuff nor the names of its contributors may be used
//    to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//================================================================================


package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.RpcResponse;
import com.dyuproject.protostuff.ds.ParamRangeKey;

/**
 * List view/visitor.
 *
 * @author David Yu
 * @created Jan 17, 2012
 */
public final class ListV
{
    
    private ListV() {}
    
    public static final VisitorSession.Handler<RpcResponse> VSH_PAGINATE = 
            new VisitorSession.Handler<RpcResponse>()
    {
        public void handle(VisitorSession session, RpcResponse res)
        {
            final ProtostuffPipe pipe = res.context.pipe;
            try
            {
                // pipe.key is the last seen key
                session.visitKind(pipe.em, -1, false, pipe.key, pipe.parentKey, 
                    RpcResponse.PIPED_VISITOR, res);
            }
            finally
            {
                pipe.clear();
            }
        }
    };
    
    /*
     * Visits the store starting at the {@code lastSeenKey} if provided.
     * 
     * Otherwise the response with the result set will contain only the 
     * {@code entity} provided.
     */
    /*public static <T extends Entity<T>> void paginateFrom(
            byte[] lastSeenKey, int fieldNumber, 
            T req, Datastore store, RpcResponse res) throws IOException
    {
        final EntityMetadata<T> em = req.em();
        if(lastSeenKey == null || lastSeenKey.length == 0)
        {
            // write ts
            if (!em.seq)
                req.setTs(KeyUtil.getTs(req.getKey()));
            
            res.output.writeObject(fieldNumber, 
                    req, em.pipeSchema.wrappedSchema, true);
            return;
        }

        final ProtostuffPipe pipe = res.context.pipe.init(
                em.pipeSchema, fieldNumber, true);
        try
        {
            store.visitKind(em, -1, false, lastSeenKey, null, 
                RpcResponse.PIPED_VISITOR, res);
        }
        finally
        {
            pipe.clear();
        }
    }*/
    
    public static <T> boolean list(EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, Datastore store, RpcResponse res) throws IOException
    {
        return list(em, fieldNumber, 
                prk.limit, prk.desc, prk.startKey, prk.parentKey, 
                store, res, em.pipeSchema);
    }
    
    public static <T> boolean list(EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, Datastore store, RpcResponse res, 
            Pipe.Schema<?> pipeSchema) throws IOException
    {
        return list(em, fieldNumber, 
                prk.limit, prk.desc, prk.startKey, prk.parentKey, 
                store, res, pipeSchema);
    }
    
    public static <T> boolean list(EntityMetadata<T> em, int fieldNumber, 
            int limit, boolean desc, byte[] startKey, byte[] parentKey, 
            Datastore store, RpcResponse res) throws IOException
    {
        return list(em, fieldNumber, limit, desc, startKey, parentKey, 
                store, res, em.pipeSchema);
    }
    
    public static <T> boolean list(EntityMetadata<T> em, int fieldNumber, 
            int limit, boolean desc, byte[] startKey, byte[] parentKey, 
            Datastore store, RpcResponse res, 
            Pipe.Schema<?> pipeSchema) throws IOException
    {
        if(limit == 0)
        {
            if(startKey == null)
                return res.fail("Key not provided.");
            
            // a get request
            final byte[] value = store.get(startKey, em, parentKey, res.context);
            if(value == null)
                return res.fail("Not found.");
            
            // initialize pipe
            final ProtostuffPipe pipe = res.context.pipe.em(em).set(startKey, value);
            try
            {
                res.output.writeObject(fieldNumber, pipe, pipeSchema, true);
            }
            finally
            {
                pipe.clear();
            }
            
            return true;
        }
        
        final ProtostuffPipe pipe = res.context.pipe.init(
                em, pipeSchema, fieldNumber, true);
        try
        {
            store.visitKind(em, limit, desc, startKey, parentKey, 
                res.context, RpcResponse.PIPED_VISITOR, res);
        }
        finally
        {
            pipe.clear();
        }
        
        return true;
    }
    
    public static <T extends Entity<T>> boolean writeTo(RpcResponse res, 
            Datastore store, EntityMetadata<T> em, int fieldNumber, 
            byte[] lastSeenKey, 
            byte[] value, byte[] key, byte[] parentKey,
            Pipe.Schema<?> pipeSchema) throws IOException
    {
        if (lastSeenKey != null)
            return list(em, fieldNumber, -1, false, lastSeenKey, parentKey, store, res, pipeSchema);
        
        final ProtostuffPipe pipe = res.context.pipe.em(em).set(key, value);
        try
        {
            res.output.writeObject(fieldNumber, pipe, pipeSchema, true);
        }
        finally
        {
            pipe.clear();
        }
        
        return true;
    }
    
    public static <T extends Entity<T>> boolean writeTo(RpcResponse res, 
            Datastore store, EntityMetadata<T> em, int fieldNumber, 
            byte[] lastSeenKey, 
            T entity, byte[] key, byte[] parentKey) throws IOException
    {
        if (lastSeenKey != null)
            return list(em, fieldNumber, -1, false, lastSeenKey, parentKey, store, res);
        
        entity.setKey(key);
        if (!em.seq)
            entity.setTs(KeyUtil.extractTsFrom(key));
        
        res.output.writeObject(fieldNumber, 
                entity, em.pipeSchema.wrappedSchema, true);
        
        return true;
    }
    
    public static <T extends CachedEntity<T>> boolean writeTo(RpcResponse res, 
            Datastore store, EntityMetadata<T> em, int fieldNumber, 
            Cache cache, int lastSeenId, 
            byte[] value, int id, byte[] key, byte[] parentKey,
            Pipe.Schema<?> pipeSchema) throws IOException
    {
        final ProtostuffPipe pipe;
        if (lastSeenId == 0 || id == (lastSeenId + 1))
        {
            pipe = res.context.pipe.em(em).set(key, value);
            try
            {
                res.output.writeObject(fieldNumber, pipe, pipeSchema, true);
            }
            finally
            {
                pipe.clear();
            }
        }
        else
        {
            pipe = res.context.pipe.init(em, pipeSchema, fieldNumber, true);
            try
            {
                cache.pipeTo(res, lastSeenId);
            }
            finally
            {
                pipe.clear();
            }
        }
        
        return true;
    }
    
    public static <T extends CachedEntity<T>> boolean writeTo(RpcResponse res, 
            Datastore store, EntityMetadata<T> em, int fieldNumber, 
            Cache cache, int lastSeenId, 
            T entity, byte[] key, byte[] parentKey) throws IOException
    {
        if (lastSeenId == 0 || entity.getId() == (lastSeenId + 1))
        {
            if (!em.seq)
                entity.setTs(KeyUtil.extractTsFrom(key));
            
            res.output.writeObject(fieldNumber, 
                    entity, em.pipeSchema.wrappedSchema, true);
        }
        else
        {
            final ProtostuffPipe pipe = res.context.pipe.init(
                    entity.em(), entity.em().pipeSchema, fieldNumber, true);
            try
            {
                cache.pipeTo(res, lastSeenId);
            }
            finally
            {
                pipe.clear();
            }
        }
        
        return true;
    }
    
    /*public static <T extends CachedEntity<T>> boolean writeTo(RpcResponse res, 
            Datastore store, EntityMetadata<T> em, int fieldNumber, 
            HasIdAndKVCache<?> cache, int lastSeenId, 
            byte[] value, int id, byte[] key, byte[] parentKey,
            Pipe.Schema<?> pipeSchema) throws IOException
    {
        final ProtostuffPipe pipe;
        if (lastSeenId == 0 || id == (lastSeenId + 1))
        {
            pipe = res.context.pipe.em(em).set(key, value);
            try
            {
                res.output.writeObject(fieldNumber, pipe, pipeSchema, true);
            }
            finally
            {
                pipe.clear();
            }
        }
        else
        {
            pipe = res.context.pipe.init(em, pipeSchema, fieldNumber, true);
            try
            {
                cache.pipeTo(res, lastSeenId);
            }
            finally
            {
                pipe.clear();
            }
        }
        
        return true;
    }
    
    public static <T extends CachedEntity<T>> boolean writeTo(RpcResponse res, 
            Datastore store, EntityMetadata<T> em, int fieldNumber, 
            HasIdAndKVCache<?> cache, int lastSeenId, 
            T entity, byte[] key, byte[] parentKey) throws IOException
    {
        if (lastSeenId == 0 || entity.getId() == (lastSeenId + 1))
        {
            if (!em.seq)
                entity.setTs(KeyUtil.extractTsFrom(key));
            
            res.output.writeObject(fieldNumber, 
                    entity, entity.em().pipeSchema.wrappedSchema, true);
        }
        else
        {
            final ProtostuffPipe pipe = res.context.pipe.init(
                    entity.em(), entity.em().pipeSchema, fieldNumber, true);
            try
            {
                cache.pipeTo(res, lastSeenId);
            }
            finally
            {
                pipe.clear();
            }
        }
        
        return true;
    }*/
}
