//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.SerializedValueUtil.transferByteArrayTo;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.RpcResponse;
import com.dyuproject.protostuff.RpcRuntimeExceptions;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.ds.ACResult;

/**
 * TODO
 * 
 * @author David Yu
 * @created Jun 18, 2014
 */
public final class CommonPV
{
    private CommonPV() {}
    
    public static enum BUILTIN implements Visitor<RpcResponse>, Schema<RpcResponse>
    {
        AUTO_COMPLETE(ACResult.getSchema())
        {
            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                    RpcResponse res, int index)
            {
                res.context.pipe.set(key, v, voffset, vlen);
                
                try
                {
                    res.output.writeObject(ACResult.PList.FN_P, res, this, true);
                }
                catch (IOException e)
                {
                    throw RpcRuntimeExceptions.pipe(e);
                }
                
                return false;
            }

            public void writeTo(Output output, RpcResponse res) throws IOException
            {
                final ProtostuffPipe pipe = res.context.pipe;
                
                final byte[] indexKey = pipe.k();
                
                // the display value 
                res.output.writeByteRange(true, ACResult.FN_NAME, 
                        pipe.v(), pipe.voffset(), pipe.vlen(), false);
                
                // the entity key
                res.output.writeByteRange(false, ACResult.FN_VALUE, 
                        indexKey, indexKey.length-9, 9, false);
            }
        },
        /**
         * The value ends with the key.
         */
        AC_WITH_KEY(ACResult.getSchema())
        {
            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                    RpcResponse res, int index)
            {
                res.context.pipe.set(key, v, voffset, vlen);
                
                try
                {
                    res.output.writeObject(ACResult.PList.FN_P, res, this, true);
                }
                catch (IOException e)
                {
                    throw RpcRuntimeExceptions.pipe(e);
                }
                
                return false;
            }

            public void writeTo(Output output, RpcResponse res) throws IOException
            {
                final ProtostuffPipe pipe = res.context.pipe;
                
                final byte[] indexKey = pipe.k();
                
                // the display value 
                res.output.writeByteRange(true, ACResult.FN_NAME, 
                        pipe.v(), pipe.voffset(), pipe.vlen() - 9, false);
                
                // the entity key
                res.output.writeByteRange(false, ACResult.FN_VALUE, 
                        indexKey, indexKey.length-9, 9, false);
            }
        },
        /**
         * The value ends with the id.
         */
        AC_WITH_ID(ACResult.getSchema())
        {
            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                    RpcResponse res, int index)
            {
                res.context.pipe.set(key, v, voffset, vlen);
                
                try
                {
                    res.output.writeObject(ACResult.PList.FN_P, res, this, true);
                }
                catch (IOException e)
                {
                    throw RpcRuntimeExceptions.pipe(e);
                }
                
                return false;
            }

            public void writeTo(Output output, RpcResponse res) throws IOException
            {
                final ProtostuffPipe pipe = res.context.pipe;
                
                final byte[] indexKey = pipe.k();
                
                // the display value 
                res.output.writeByteRange(true, ACResult.FN_NAME, 
                        pipe.v(), pipe.voffset, pipe.vlen - 4, false);
                
                // the entity key
                res.output.writeByteRange(false, ACResult.FN_VALUE, 
                        indexKey, indexKey.length-9, 9, false);
                
                // the id
                res.output.writeFixed32(ACResult.FN_ID, ValueUtil.toInt32LE(
                        pipe.v(), pipe.voffset + pipe.vlen - 4), false);
            }
        },
        AC_WITH_CONTEXT_FIELD(ACResult.getSchema())
        {
            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                    RpcResponse res, int index)
            {
                res.context.pipe.set(key, v, voffset, vlen);
                
                try
                {
                    res.output.writeObject(ACResult.PList.FN_P, res, this, true);
                }
                catch (IOException e)
                {
                    throw RpcRuntimeExceptions.pipe(e);
                }
                
                return false;
            }

            public void writeTo(Output output, RpcResponse res) throws IOException
            {
                final WriteContext context = res.context;
                final ProtostuffPipe pipe = context.pipe;
                
                final int field = context.$field;
                // value offset
                final int vo = context.$offset;
                
                final byte[] indexKey = pipe.k();
                
                // this display value
                transferByteArrayTo(output, true, ACResult.FN_NAME, false, 
                        // the string field
                        field, 
                        pipe.v(), pipe.voffset(), pipe.vlen(), res.context);
                
                // the entity key
                res.output.writeByteRange(false, ACResult.FN_VALUE, 
                        indexKey, indexKey.length-9, 9, false);
                
                if (vo != 0)
                {
                    res.output.writeFixed32(ACResult.FN_ID, SerializedValueUtil.asInt32(
                            vo, pipe.v(), pipe.voffset(), pipe.vlen()), false);
                }
            }
        };
        
        final Schema<?> schema;
        
        private BUILTIN(Schema<?> schema)
        {
            this.schema = schema;
        }
        
        public String getFieldName(int number)
        {
            return schema.getFieldName(number);
        }

        public int getFieldNumber(String name)
        {
            return schema.getFieldNumber(name);
        }

        public boolean isInitialized(RpcResponse message)
        {
            return true;
        }

        public RpcResponse newMessage()
        {
            throw new UnsupportedOperationException();
        }

        public String messageName()
        {
            return schema.messageName();
        }

        public String messageFullName()
        {
            return schema.messageFullName();
        }

        public Class<? super RpcResponse> typeClass()
        {
            return RpcResponse.class;
        }

        public void mergeFrom(Input input, RpcResponse message) throws IOException
        {
            throw new UnsupportedOperationException();
        }
    }
    
    /*public static enum AC implements Visitor<RpcResponse>, Schema<RpcResponse>
    {
        $3(3),
        $4(4),
        $5(5),
        $6(6),
        $7(7),
        $8(8),
        $9(9),
        $10(10),
        $11(11),
        $12(12),
        $13(13),
        $14(14),
        $15(15);
        
        //final Schema<ACResult> schema = ACResult.getSchema();
        final int field;
        
        private AC(int field)
        {
            this.field = field;
        }
        
        public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                RpcResponse res, int index)
        {
            res.context.pipe.set(key, v, voffset, vlen);
            
            try
            {
                res.output.writeObject(ACResult.PList.FN_P, res, this, true);
            }
            catch (IOException e)
            {
                throw RpcRuntimeExceptions.pipe(e);
            }
            
            return false;
        }

        public void writeTo(Output output, RpcResponse res) throws IOException
        {
            final ProtostuffPipe pipe = res.context.pipe;
            
            final byte[] indexKey = pipe.k();
            
            // this display value
            transferByteArrayTo(output, true, ACResult.FN_NAME, false, 
                    field, pipe.v(), pipe.voffset(), pipe.vlen(), res.context);
            
            // the entity key
            res.output.writeByteRange(false, ACResult.FN_VALUE, 
                    indexKey, indexKey.length-9, 9, false);
        }
        
        public String getFieldName(int number)
        {
            return ACResult.SCHEMA.getFieldName(number);
        }

        public int getFieldNumber(String name)
        {
            return ACResult.SCHEMA.getFieldNumber(name);
        }

        public boolean isInitialized(RpcResponse message)
        {
            return true;
        }

        public RpcResponse newMessage()
        {
            throw new UnsupportedOperationException();
        }

        public String messageName()
        {
            return ACResult.SCHEMA.messageName();
        }

        public String messageFullName()
        {
            return ACResult.SCHEMA.messageFullName();
        }

        public Class<? super RpcResponse> typeClass()
        {
            return RpcResponse.class;
        }

        public void mergeFrom(Input input, RpcResponse message) throws IOException
        {
            throw new UnsupportedOperationException();
        }
    }*/
}
