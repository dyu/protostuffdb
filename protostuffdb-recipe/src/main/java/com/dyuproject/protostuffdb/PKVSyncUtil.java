//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuff.KeyBuilder;

/**
 * Utils for pkv-sync.
 * 
 * @author David Yu
 * @created Jan 10, 2018
 */
public final class PKVSyncUtil
{
    private PKVSyncUtil() {}
    
    private static final byte[] OOB_SEQ = new byte[]{
        (byte)0, (byte)0,
        (byte)0, (byte)0, 
        (byte)0, (byte)0, 
        (byte)0, (byte)0
    };
    
    private static final byte[] END_SEQ = new byte[]{
        (byte)0xff, (byte)0xff,
        (byte)0xff, (byte)0xff, 
        (byte)0xff, (byte)0xff, 
        (byte)0xff, (byte)0xff
    };
    
    /**
     * Get the current seq number.
     */
    public static long getCurrentSeq(int type, byte[] prefix, int offset, 
            KV kv, VisitorSession vs, KeyBuilder kb)
    {
        kb.$append(prefix, offset, 3)
            .$append8(type)
            .$append(OOB_SEQ)
            .$opushRange(END_SEQ, OOB_SEQ.length);
        
        long seqNum = 0;
        if (1 == vs.visitRange(true, 1, true, null, Visitor.SET_KEY, kv, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len()))
        {
            seqNum = ValueUtil.toInt64(kv.key, kv.key.length - 8);
        }
        
        return seqNum;
    }
    
    /**
     * Get the current seq number.
     */
    public static long getCurrentSeq(int instanceNum, int type, 
            KV kv, VisitorSession vs, KeyBuilder kb)
    {
        kb.$append8(0x80)
            .$append16(instanceNum)
            .$append8(type)
            .$append(OOB_SEQ)
            .$opushRange(END_SEQ, OOB_SEQ.length);
        
        long seqNum = 0;
        if (1 == vs.visitRange(true, 1, true, null, Visitor.SET_KEY, kv, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len()))
        {
            seqNum = ValueUtil.toInt64(kv.key, kv.key.length - 8);
        }
        
        return seqNum;
    }
    
    /**
     * Append the sync key to the key builder.
     */
    public static KeyBuilder appendKey(int instanceNum, int type, long seqNum, 
            KeyBuilder kb)
    {
        return kb.$append8(0x80)
            .$append16(instanceNum)
            .$append8(type)
            .$append(seqNum);
    }
    
    /**
     * Returns the new sync key.
     */
    public static byte[] newKey(int instanceNum, int type, long seqNum)
    {
        byte[] key = new byte[1 + 2 + 1 + 8]; // 0x80, instance_id, type, seq_num
        int offset = 0;
        
        // prefixed key
        key[offset++] = (byte)0x80;
        
        IntSerializer.writeInt16(instanceNum, key, offset);
        offset += 2;
        
        key[offset++] = (byte)type;
        
        IntSerializer.writeInt64(seqNum, key, offset);
        offset += 8;
        
        return key;
    }
}
