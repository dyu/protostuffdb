//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.ArrayList;

import com.dyuproject.protostuff.RpcResponse;


/**
 * Range visitor.
 * 
 * @author David Yu
 * @created Jul 10, 2013
 */
public interface RangeV<S>
{

    public <T,V> int visitRange(
            EntityMetadata<T> em, int fieldNumber, 
            S store, WriteContext context, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen);
    
    /**
     * The visitor to use when using {@link Store#ENTITY_PV} or {@link Session#ENTITY_PV}.
     */
    public static final Visitor<RpcResponse> RES_PV = RpcResponse.PIPED_VISITOR;
    
    public enum Store implements RangeV<Datastore>
    {
        /**
         * Used on a clustered index.
         */
        CONTEXT_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, context.ps, fieldNumber, true);
                try
                {
                    return store.visitRange(keysOnly, limit, desc, 
                            rawStartKey, context, visitor, param, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index where the end of the value is the key.
         */
        CONTEXT_VK_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, context.ps, fieldNumber, true);
                try
                {
                    return store.visitRange(keysOnly, limit, desc, 
                            rawStartKey, context, visitor, param, 
                            true, false,
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index.
         */
        CONTEXT_MGET_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ArrayList<byte[]> list = new ArrayList<byte[]>();
                if (0 == store.visitRange(true, limit, desc, 
                        rawStartKey, context, Visitor.APPEND_KEY, list, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen))
                {
                    return 0;
                }
                
                final ProtostuffPipe pipe = context.pipe.init(
                        em, context.ps, fieldNumber, true);
                try
                {
                    return store.visitKeys(list, false, true, context, visitor, param);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a clustered index.
         */
        ENTITY_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, em.pipeSchema, fieldNumber, true);
                try
                {
                    return store.visitRange(keysOnly, limit, desc, 
                            rawStartKey, context, visitor, param, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index where the end of the value is the key.
         */
        ENTITY_VK_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, em.pipeSchema, fieldNumber, true);
                try
                {
                    return store.visitRange(keysOnly, limit, desc, 
                            rawStartKey, context, visitor, param, 
                            true, false,
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index.
         */
        ENTITY_MGET_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ArrayList<byte[]> list = new ArrayList<byte[]>();
                if (0 == store.visitRange(true, limit, desc, 
                        rawStartKey, context, Visitor.APPEND_KEY, list, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen))
                {
                    return 0;
                }
                
                final ProtostuffPipe pipe = context.pipe.init(
                        em, em.pipeSchema, fieldNumber, true);
                try
                {
                    return store.visitKeys(list, false, true, context, visitor, param);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index.
         */
        MGET_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ArrayList<byte[]> list = new ArrayList<byte[]>();
                if (0 == store.visitRange(true, limit, desc, 
                        rawStartKey, context, Visitor.APPEND_KEY, list, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen))
                {
                    return 0;
                }
                
                try
                {
                    return store.visitKeys(list, false, true, context, visitor, param);
                }
                finally
                {
                    context.pipe.clear();
                }
            }
        },
        /**
         * Used on a clustered index.
         */
        PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                try
                {
                    return store.visitRange(keysOnly, limit, desc, 
                            rawStartKey, context, visitor, param, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    context.pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index where the end of the value is the key.
         */
        VK_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                try
                {
                    return store.visitRange(keysOnly, limit, desc, 
                            rawStartKey, context, visitor, param, 
                            true, false,
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    context.pipe.clear();
                }
            }
        },
        V
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                return store.visitRange(keysOnly, limit, desc, 
                        rawStartKey, context, visitor, param, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen);
            }
        },
        V_KEYS_ONLY
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    Datastore store, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                return store.visitRange(true, limit, desc, 
                        rawStartKey, context, visitor, param, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen);
            }
        }
        ;
    }
    
    public enum Session implements RangeV<VisitorSession>
    {
        /**
         * Used on a clustered index.
         */
        CONTEXT_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, context.ps, fieldNumber, true);
                try
                {
                    return session.visitRange(keysOnly, limit, desc, 
                            rawStartKey, visitor, param, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index where the end of the value is the key.
         */
        CONTEXT_VK_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, context.ps, fieldNumber, true);
                try
                {
                    return session.visitRange(keysOnly, limit, desc, 
                            rawStartKey, visitor, param, 
                            true, false,
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index.
         */
        CONTEXT_MGET_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ArrayList<byte[]> list = new ArrayList<byte[]>();
                if (0 == session.visitRange(true, limit, desc, 
                            rawStartKey, Visitor.APPEND_KEY, list, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen))
                {
                    return 0;
                }
                
                final ProtostuffPipe pipe = context.pipe.init(
                        em, context.ps, fieldNumber, true);
                try
                {
                    return session.visitKeys(list, false, true, visitor, param);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a clustered index.
         */
        ENTITY_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, em.pipeSchema, fieldNumber, true);
                try
                {
                    return session.visitRange(keysOnly, limit, desc, 
                            rawStartKey, visitor, param, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index where the end of the value is the key.
         */
        ENTITY_VK_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ProtostuffPipe pipe = context.pipe.init(
                        em, em.pipeSchema, fieldNumber, true);
                try
                {
                    return session.visitRange(keysOnly, limit, desc, 
                            rawStartKey, visitor, param,
                            true, false,
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index.
         */
        ENTITY_MGET_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ArrayList<byte[]> list = new ArrayList<byte[]>();
                if (0 == session.visitRange(true, limit, desc, 
                            rawStartKey, Visitor.APPEND_KEY, list, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen))
                {
                    return 0;
                }
                
                final ProtostuffPipe pipe = context.pipe.init(
                        em, em.pipeSchema, fieldNumber, true);
                try
                {
                    return session.visitKeys(list, false, true, visitor, param);
                }
                finally
                {
                    pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index.
         */
        MGET_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                final ArrayList<byte[]> list = new ArrayList<byte[]>();
                if (0 == session.visitRange(true, limit, desc, 
                            rawStartKey, Visitor.APPEND_KEY, list, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen))
                {
                    return 0;
                }
                
                try
                {
                    return session.visitKeys(list, false, true, visitor, param);
                }
                finally
                {
                    context.pipe.clear();
                }
            }
        },
        /**
         * Used on a clustered index.
         */
        PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                try
                {
                    return session.visitRange(keysOnly, limit, desc, 
                            rawStartKey, visitor, param, 
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    context.pipe.clear();
                }
            }
        },
        /**
         * Used on a non-clustered index where the end of the value is the key.
         */
        VK_PV
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                try
                {
                    return session.visitRange(keysOnly, limit, desc, 
                            rawStartKey, visitor, param, 
                            true, false,
                            sk, skoffset, sklen, 
                            ek, ekoffset, eklen);
                }
                finally
                {
                    context.pipe.clear();
                }
            }
        },
        V
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                return session.visitRange(keysOnly, limit, desc, 
                        rawStartKey, visitor, param, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen);
            }
        },
        V_KEYS_ONLY
        {
            public <T,V> int visitRange(
                    EntityMetadata<T> em, int fieldNumber, 
                    VisitorSession session, WriteContext context, 
                    boolean keysOnly, int limit, boolean desc, 
                    byte[] rawStartKey, Visitor<V> visitor, V param, 
                    byte[] sk, int skoffset, int sklen, 
                    byte[] ek, int ekoffset, int eklen)
            {
                return session.visitRange(true, limit, desc, 
                        rawStartKey, visitor, param, 
                        sk, skoffset, sklen, 
                        ek, ekoffset, eklen);
            }
        }
        ;
    }
}
