//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Schema;

/**
 * A write-only schema for {@link WriteContext}.
 * 
 * @author David Yu
 * @created Aug 9, 2017
 */
public abstract class ContextWS<T> implements Schema<WriteContext>
{
    public final Schema<T> schema;
    
    protected ContextWS(Schema<T> schema)
    {
        this.schema = schema;
    }
    
    public String getFieldName(int number)
    {
        return schema.getFieldName(number);
    }

    public int getFieldNumber(String name)
    {
        return schema.getFieldNumber(name);
    }

    public boolean isInitialized(WriteContext message)
    {
        return true;
    }

    public WriteContext newMessage()
    {
        throw new UnsupportedOperationException();
    }

    public String messageName()
    {
        return schema.messageName();
    }

    public String messageFullName()
    {
        return schema.messageFullName();
    }

    public Class<? super WriteContext> typeClass()
    {
        return WriteContext.class;
    }

    public void mergeFrom(Input input, WriteContext message) throws IOException
    {
        throw new UnsupportedOperationException();
    }
    
}
