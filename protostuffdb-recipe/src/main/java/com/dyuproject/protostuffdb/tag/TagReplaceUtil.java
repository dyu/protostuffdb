//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.tag;

import static com.dyuproject.protostuffdb.tag.TagRemoveUtil.t1;
import static com.dyuproject.protostuffdb.tag.TagRemoveUtil.t2;
import static com.dyuproject.protostuffdb.tag.TagRemoveUtil.t3;
import static com.dyuproject.protostuffdb.tag.TagRemoveUtil.t4;
import static com.dyuproject.protostuffdb.ValueUtil.toInt32LE;

import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuff.ds.MultiCAS;
import com.dyuproject.protostuffdb.DSRuntimeExceptions;
import com.dyuproject.protostuffdb.KV;
import com.dyuproject.protostuffdb.KeyUtil;
import com.dyuproject.protostuffdb.OpChain;
import com.dyuproject.protostuffdb.VisitorSession;

/**
 * TODO
 * 
 * @author David Yu
 * @created Feb 7, 2017
 */
public final class TagReplaceUtil
{
    private TagReplaceUtil() {}
    
    private static MultiCAS newMC(int field, int tagId, int newTagId/*, 
            int fieldTs, long ts*/)
    {
        return new MultiCAS()
                .addOp(new CAS.Fixed32Op(field, tagId, newTagId))/*
                .addOp(CAS.Fixed64Op.asPassthrough(fieldTs, ts))*/;
    }
    
    private static CAS.Fixed32Op op(int field, int tagId, int newTagId)
    {
        return new CAS.Fixed32Op(field, tagId, newTagId);
    }

    private static int replaceLast(final int tagCount,
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 1)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return tagCount;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 2)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 4);
            return tagCount;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 8);
            return tagCount;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f4, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        IntSerializer.writeInt32LE(newTagId, serTags, 12);
        return tagCount;
    }
    
    private static int replace1(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        // update t2
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t2) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 2)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, tagId, t2, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        IntSerializer.writeInt32LE(newTagId, serTags, 0);
        return 1;
    }
    
    private static int replace2(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        // update entry
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 4);
            return 2;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        IntSerializer.writeInt32LE(newTagId, serTags, 4);
        return 2;
    }
    
    private static int replace3(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        // update entry
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        IntSerializer.writeInt32LE(newTagId, serTags, 8);
        return 3;
    }
    
    private static int replace12(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        if (t2 < newTagId)
        {
            // tagCount: 2
            if (!t2(vs, tif, entryKey, kv, kb, tagId, t2) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                    newMC(tif.f1, tagId, t2/*, TagIndex2.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t2, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // shift 1 slot to the left
            System.arraycopy(serTags, 4, serTags, 0, 4);
            IntSerializer.writeInt32LE(newTagId, serTags, 4);
            return 2;
        }
        
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t2) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 2)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, tagId, t2, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        IntSerializer.writeInt32LE(newTagId, serTags, 0);
        return 1;
    }
    
    private static int replace21(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t1, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 2)
        {
            // shift 1 slot to the right
            System.arraycopy(serTags, 0, serTags, 4, 4);
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t1, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            // shift 1 slot to the right
            System.arraycopy(serTags, 0, serTags, 4, 4);
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t1, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, t1, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        // shift 1 slot to the right
        System.arraycopy(serTags, 0, serTags, 4, 4);
        IntSerializer.writeInt32LE(newTagId, serTags, 0);
        return 1;
    }
    
    private static int replace13(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t2) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, t2/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, newTagId)), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        
        if (t3 < newTagId)
        {
            // tagCount: 3 (last entry)
            
            // 1-3 permutation
            if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                    newMC(tif.f1, tagId, t3/*, TagIndex2.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t3, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // update t3
            if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t3) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f1, tagId, t2/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t2, t3))
                    .addOp(op(tif.f3, t3, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // shift 2nd and 3rd slot to the left
            System.arraycopy(serTags, 4, serTags, 0, 8);
            // replace the 3rd slot
            IntSerializer.writeInt32LE(newTagId, serTags, 8);
            return 3;
        }
        
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, t2/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, newTagId)), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            // shift 1 slot to the left
            System.arraycopy(serTags, 4, serTags, 0, 4);
            IntSerializer.writeInt32LE(newTagId, serTags, 4);
            return 2;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, t2/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, tagId, t2, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, tagId, t2/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // shift 1 slot to the left
        System.arraycopy(serTags, 4, serTags, 0, 4);
        IntSerializer.writeInt32LE(newTagId, serTags, 4);
        return 2;
    }
    
    private static int replace31(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t1, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t2, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // 1-2-3 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t1, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, t1))
                .addOp(op(tif.f3, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            // shift 2nd slot to the right
            System.arraycopy(serTags, 4, serTags, 8, 4);
            // shift 1 slot to the right
            System.arraycopy(serTags, 0, serTags, 4, 4);
            IntSerializer.writeInt32LE(newTagId, serTags, 0);
            return 1;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t1, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t2, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, t1, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, t1))
                .addOp(op(tif.f3, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // shift 2nd slot to the right
        System.arraycopy(serTags, 4, serTags, 8, 4);
        // shift 1 slot to the right
        System.arraycopy(serTags, 0, serTags, 4, 4);
        IntSerializer.writeInt32LE(newTagId, serTags, 0);
        return 1;
    }
    
    private static int replace14(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t2) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, t2/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, newTagId)), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, t3/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // 1-2-3 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, t2/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, t3))
                .addOp(op(tif.f3, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        
        if (t4 < newTagId)
        {
            // last entry
            
            // 1-4 permutation
            if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                    newMC(tif.f1, tagId, t4/*, TagIndex2.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // 1-2-4 permutation
            if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f1, tagId, t2/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t2, t4))
                    .addOp(op(tif.f3, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // 1-3-4 permutation
            if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f1, tagId, t3/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t3, t4))
                    .addOp(op(tif.f3, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // update t4
            if (!t4(vs, tif, entryKey, kv, kb, tagId, t2, t3, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                    newMC(tif.f1, tagId, t2/*, TagIndex4.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t2, t3))
                    .addOp(op(tif.f3, t3, t4))
                    .addOp(op(tif.f4, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // shift to the left with the length of 3 slots
            System.arraycopy(serTags, 4, serTags, 0, 12);
            // write to the 4th slot
            IntSerializer.writeInt32LE(newTagId, serTags, 12);
            return 3;
        }
        
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t2, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, t2/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, t3/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, tagId, t2, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, tagId, t2/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, t3))
                .addOp(op(tif.f3, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // shift to the left with the length of 2 slots
        System.arraycopy(serTags, 4, serTags, 0, 8);
        IntSerializer.writeInt32LE(newTagId, serTags, 8);
        return 3;
    }
    
    private static int replace41(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t1, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t1)), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t2, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t3, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t1, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, t1))
                .addOp(op(tif.f3, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t1, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, t1))
                .addOp(op(tif.f3, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t2, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, t2))
                .addOp(op(tif.f3, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f1, t1, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t2, t1))
                .addOp(op(tif.f3, t3, t2))
                .addOp(op(tif.f4, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // shift 3rd slot to the right
        System.arraycopy(serTags, 8, serTags, 12, 4);
        // shift 2nd slot to the right
        System.arraycopy(serTags, 4, serTags, 8, 4);
        // shift 1 slot to the right
        System.arraycopy(serTags, 0, serTags, 4, 4);
        IntSerializer.writeInt32LE(newTagId, serTags, 0);
        return 1;
    }
    
    private static int replace23(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        if (t3 < newTagId)
        {
            // tagCount: 3
            
            // 2-3 permutation
            if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                    newMC(tif.f1, tagId, t3/*, TagIndex2.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t3, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // update t3
            if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t3) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f2, tagId, t3/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f3, t3, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // move to last slot to the left
            System.arraycopy(serTags, 8, serTags, 4, 4);
            // write to the last slot
            IntSerializer.writeInt32LE(newTagId, serTags, 8);
            return 2;
        }
        
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t3
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        if (tagCount == 3)
        {
            // replace the 2nd slot
            IntSerializer.writeInt32LE(newTagId, serTags, 4);
            return 2;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // replace the 2nd slot
        IntSerializer.writeInt32LE(newTagId, serTags, 4);
        return 2;
    }
    
    private static int replace32(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t2, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-3 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, t2, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t2, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f2, t2, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // shift 2nd slot to the right
        System.arraycopy(serTags, 4, serTags, 8, 4);
        // write to 2nd slot
        IntSerializer.writeInt32LE(newTagId, serTags, 4);
        return 2;
    }
    
    private static int replace24(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        
        // 1-2 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, t3/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // 1-2-3 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t3) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, t3/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        
        if (t4 < newTagId)
        {
            // last entry
            
            // 2-4 permutation
            if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                    newMC(tif.f1, tagId, t4/*, TagIndex2.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // 1-2-4 permutation
            if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f2, tagId, t4/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f3, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // 2-3-4 permutation
            if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f1, tagId, t3/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t3, t4))
                    .addOp(op(tif.f3, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // update t4
            if (!t4(vs, tif, entryKey, kv, kb, t1, tagId, t3, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                    newMC(tif.f2, tagId, t3/*, TagIndex4.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f3, t3, t4))
                    .addOp(op(tif.f4, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // shift 3rd slot to the left with 2 slots
            System.arraycopy(serTags, 8, serTags, 4, 8);
            // write to 4th slot
            IntSerializer.writeInt32LE(newTagId, serTags, 12);
            return 4;
        }
        
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, tagId, t3/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, tagId, t3, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f2, tagId, t3/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, t3, newTagId)), null, null, null))
        {
            return 0;
        }
        
        // shift 3rd slot to the left
        System.arraycopy(serTags, 8, serTags, 4, 4);
        // write to 3rd slot
        IntSerializer.writeInt32LE(newTagId, serTags, 8);
        return 3;
    }
    
    private static int replace42(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t2, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t3, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, t2, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, tagId, t2)), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, t3, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f1, t2, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, t3, t2))
                .addOp(op(tif.f3, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f2, t2, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, t3, t2))
                .addOp(op(tif.f4, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // shift 3rd slot to the right
        System.arraycopy(serTags, 8, serTags, 12, 4);
        // shift 2nd slot to the right
        System.arraycopy(serTags, 4, serTags, 8, 4);
        // write to 2nd slot
        IntSerializer.writeInt32LE(newTagId, serTags, 4);
        return 2;
    }
    
    private static int replace34(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        
        // 1-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        
        // 2-3 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t4 = toInt32LE(serTags, 12);
        if (t4 < newTagId)
        {
            // 3-4 permutation
            if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                    newMC(tif.f1, tagId, t4/*, TagIndex2.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f2, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // 1-2-3 permutation
            if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f3, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
            {
                return 0;
            }
            
            // 1-3-4 permutation
            if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f2, tagId, t4/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f3, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // 2-3-4 permutation
            if (!t3(vs, tif, entryKey, kv, kb, t2, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                    newMC(tif.f2, tagId, t4/*, TagIndex3.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f3, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // update t4
            if (!t4(vs, tif, entryKey, kv, kb, t1, t2, tagId, t4) || 
                    !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                    newMC(tif.f3, tagId, t4/*, TagIndex4.FN_UPDATE_TS, now*/)
                    .addOp(op(tif.f4, t4, newTagId)), null, null, null))
            {
                return 0;
            }
            
            // shift 4th slot to the left
            System.arraycopy(serTags, 12, serTags, 8, 4);
            // replace the 4th slot
            IntSerializer.writeInt32LE(newTagId, serTags, 12);
            return 3;
        }
        
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-2-3 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, tagId, t4) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // replace the 3rd slot
        IntSerializer.writeInt32LE(newTagId, serTags, 8);
        return 3;
    }
    
    private static int replace43(final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        if (!t1(vs, tif, entryKey, kv, kb, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em1, 
                newMC(tif.f1, tagId, newTagId/*, TagIndex1.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t1 = toInt32LE(serTags, 0);
        
        // 1-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t1, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t2 = toInt32LE(serTags, 4);
        
        // 2-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f2, tagId, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        final int t3 = toInt32LE(serTags, 8);
        
        // 3-4 permutation
        if (!t2(vs, tif, entryKey, kv, kb, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em2, 
                newMC(tif.f1, t3, newTagId/*, TagIndex2.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f2, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // 1-2-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t2, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f3, tagId, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/), null, null, null))
        {
            return 0;
        }
        
        // 1-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t1, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, t3, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // 2-3-4 permutation
        if (!t3(vs, tif, entryKey, kv, kb, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em3, 
                newMC(tif.f2, t3, newTagId/*, TagIndex3.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f3, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // update t4
        if (!t4(vs, tif, entryKey, kv, kb, t1, t2, t3, tagId) || 
                !chain.updateWithValue(kv.value, KeyUtil.extractFrom(kv.key), tif.em4, 
                newMC(tif.f3, t3, newTagId/*, TagIndex4.FN_UPDATE_TS, now*/)
                .addOp(op(tif.f4, tagId, t3)), null, null, null))
        {
            return 0;
        }
        
        // shift 3rd slot to the right
        System.arraycopy(serTags, 8, serTags, 12, 4);
        // replace the 3rd slot
        IntSerializer.writeInt32LE(newTagId, serTags, 8);
        return 3;
    }
    
    private static int replaceSameSlot(final int slot, 
            final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        switch (slot)
        {
            case 1:
                return replace1(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 2:
                return replace2(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 3:
                return replace3(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 4:
                return replaceLast(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            default:
                return 0;
        }
    }
    
    private static int replaceCount2(final int slot, 
            final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        switch (slot)
        {
            case 1:
                return replace12(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 2:
                return replace21(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            default:
                return 0;
        }
    }
    
    private static int replaceCount3(final int slot, final int targetSlot, 
            final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        switch (slot)
        {
            case 1:
                if (targetSlot == 2)
                    return replace12(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace13(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 2:
                if (targetSlot == 1)
                    return replace21(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace23(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 3:
                if (targetSlot == 1)
                    return replace31(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace32(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            default:
                return 0;
        }
    }
    
    private static int replaceCount4(final int slot, final int targetSlot, 
            final int tagCount, 
            final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            final byte[] serTags, final long now, 
            int tagId, int newTagId)
    {
        switch (slot)
        {
            case 1:
                if (targetSlot == 2)
                    return replace12(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                if (targetSlot == 3)
                    return replace13(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace14(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 2:
                if (targetSlot == 1)
                    return replace21(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                if (targetSlot == 3)
                    return replace23(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace24(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 3:
                if (targetSlot == 1)
                    return replace31(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                if (targetSlot == 2)
                    return replace32(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace34(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            case 4:
                if (targetSlot == 1)
                    return replace41(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                if (targetSlot == 2)
                    return replace42(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
                
                return replace43(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
            default:
                return 0;
        }
    }
    
    /**
     * Returns the index the replacement was inserted.
     */
    static int replace(final int tagId, final int slot, 
            final int newTagId, final int newSlot, 
            final TagIndexFactory<?, ?, ?, ?> tif, 
            final OpChain chain, byte[] entryKey, byte[] serTags)
    {
        final long now = System.currentTimeMillis();
        
        final VisitorSession vs = chain.vs();
        final KV kv = new KV();
        final KeyBuilder kb = chain.context.kb();
        
        final int tagCount = serTags.length/4,
                targetSlot = -newSlot,
                actualSlot;
        
        // existing tag is last and new tag is also last.
        if (tagCount == 1 || (tagCount == slot && targetSlot >= slot))
        {
            actualSlot = replaceLast(tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
        }
        else if (slot == targetSlot)
        {
            actualSlot = replaceSameSlot(slot, tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
        }
        else if (tagCount == 2)
        {
            actualSlot = replaceCount2(slot, tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
        }
        else if (tagCount == 3)
        {
            actualSlot = replaceCount3(slot, targetSlot, tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
        }
        else
        {
            actualSlot = replaceCount4(slot, targetSlot, tagCount, vs, tif, chain, entryKey, kv, kb, serTags, now, tagId, newTagId);
        }
        
        if (actualSlot == 0)
            throw DSRuntimeExceptions.operationFailure("Could not replace tag.");
        
        return actualSlot - 1;
    }
}
