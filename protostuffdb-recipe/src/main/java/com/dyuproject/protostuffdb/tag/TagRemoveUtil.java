//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.tag;

import static com.dyuproject.protostuffdb.ValueUtil.toInt32LE;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.KV;
import com.dyuproject.protostuffdb.KeyUtil;
import com.dyuproject.protostuffdb.OpChain;
import com.dyuproject.protostuffdb.ValueUtil;
import com.dyuproject.protostuffdb.VisitorSession;

/**
 * TODO
 * 
 * @author David Yu
 * @created Feb 7, 2017
 */
public final class TagRemoveUtil
{
    private TagRemoveUtil() {}
    
    /**
     * <pre>
     * 4K
     * </pre>
     * gen_kb
     */
    private static KeyBuilder $$TAG1_ID__ENTRY_KEY(
            EntityMetadata<?> em, 
            KeyBuilder kb, final TagIndexFactory<?, ?, ?, ?> tif,
            int tag1Id, 
            byte[] entryKey, int entryKey_offset)
    {
        kb.begin(tif.idxId, em);
        if (tif.toggled)
            kb.$append8(1);
        
        return kb.$append(tag1Id)
                .$append(entryKey, entryKey_offset, 9);
    }
    
    /**
     * <pre>
     * 44K
     * </pre>
     * gen_kb
     */
    private static KeyBuilder $$TAG1_ID__TAG2_ID__ENTRY_KEY(
            EntityMetadata<?> em, 
            KeyBuilder kb, final TagIndexFactory<?, ?, ?, ?> tif,
            int tag1Id, 
            int tag2Id, 
            byte[] entryKey, int entryKey_offset)
    {
        kb.begin(tif.idxId, em);
        if (tif.toggled)
            kb.$append8(1);
        
        return kb
                .$append(tag1Id)
                .$append(tag2Id)
                .$append(entryKey, entryKey_offset, 9);
    }

    /**
     * <pre>
     * 444K
     * </pre>
     * gen_kb
     */
    private static KeyBuilder $$TAG1_ID__TAG2_ID__TAG3_ID__ENTRY_KEY(
            EntityMetadata<?> em,
            KeyBuilder kb, final TagIndexFactory<?, ?, ?, ?> tif,
            int tag1Id, 
            int tag2Id, 
            int tag3Id, 
            byte[] entryKey, int entryKey_offset)
    {
        kb.begin(tif.idxId, em);
        if (tif.toggled)
            kb.$append8(1);
        
        return kb
                .$append(tag1Id)
                .$append(tag2Id)
                .$append(tag3Id)
                .$append(entryKey, entryKey_offset, 9);
    }
    
    /**
     * <pre>
     * 4444K
     * </pre>
     * gen_kb
     */
    private static KeyBuilder $$TAG1_ID__TAG2_ID__TAG3_ID__TAG4_ID__ENTRY_KEY(
            EntityMetadata<?> em,
            KeyBuilder kb, final TagIndexFactory<?, ?, ?, ?> tif,
            int tag1Id, 
            int tag2Id, 
            int tag3Id, 
            int tag4Id, 
            byte[] entryKey, int entryKey_offset)
    {
        kb.begin(tif.idxId, em);
        if (tif.toggled)
            kb.$append8(1);
        
        return kb
                .$append(tag1Id)
                .$append(tag2Id)
                .$append(tag3Id)
                .$append(tag4Id)
                .$append(entryKey, entryKey_offset, 9);
    }
    
    static boolean t1(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1)
    {
        $$TAG1_ID__ENTRY_KEY(tif.em1, kb, tif, 
                tag1, 
                entryKey, 0);
        if (!tif.unique)
            return vs.pget(kv, kb.$append8(tif.em1.kind).$push());
        
        final boolean exists =  null != (kv.value = vs.rawGet(
                kb.$push().buf(), kb.offset(), kb.len()));
        
        if (exists)
            kv.key = ValueUtil.copy(kb.buf(), kb.offset(), kb.len());
        
        return exists;
    }
    
    static boolean t2(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2)
    {
        $$TAG1_ID__TAG2_ID__ENTRY_KEY(tif.em2, kb, tif, 
                tag1, tag2,
                entryKey, 0);
        
        if (!tif.unique)
            return vs.pget(kv, kb.$append8(tif.em2.kind).$push());
        
        final boolean exists =  null != (kv.value = vs.rawGet(
                kb.$push().buf(), kb.offset(), kb.len()));
        
        if (exists)
            kv.key = ValueUtil.copy(kb.buf(), kb.offset(), kb.len());
        
        return exists;
    }
    
    static boolean t3(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3)
    {
        $$TAG1_ID__TAG2_ID__TAG3_ID__ENTRY_KEY(tif.em3, kb, tif, 
                tag1, tag2, tag3,
                entryKey, 0);
        
        if (!tif.unique)
            return vs.pget(kv, kb.$append8(tif.em3.kind).$push());
        
        final boolean exists =  null != (kv.value = vs.rawGet(
                kb.$push().buf(), kb.offset(), kb.len()));
        
        if (exists)
            kv.key = ValueUtil.copy(kb.buf(), kb.offset(), kb.len());
        
        return exists;
    }
    
    static boolean t4(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3, int tag4)
    {
        $$TAG1_ID__TAG2_ID__TAG3_ID__TAG4_ID__ENTRY_KEY(tif.em4, kb, tif, 
                tag1, tag2, tag3, tag4,
                entryKey, 0);
        
        if (!tif.unique)
            return vs.pget(kv, kb.$append8(tif.em4.kind).$push());
        
        final boolean exists =  null != (kv.value = vs.rawGet(
                kb.$push().buf(), kb.offset(), kb.len()));
        
        if (exists)
            kv.key = ValueUtil.copy(kb.buf(), kb.offset(), kb.len());
        
        return exists;
    }
    
    static boolean removeFirstAndOnly(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1)
    {
        return t1(vs, tif, entryKey, kv, kb, tag1)
                && rm(kv, tif.unique, tif.em1, chain);
    }
    
    private static <T> boolean rm(KV kv, boolean unique, EntityMetadata<T> em, OpChain chain)
    {
        if (!unique)
        {
            return chain.deleteWithValue(kv.value, KeyUtil.extractFrom(kv.key), em, 
                    null, null, null);
        }
        
        chain.rawDelete(kv.key);
        // first element is the tag index key
        chain.rawDelete(kv.value, 0, 9);
        return true;
    }
    
    private static boolean removeFrom2F(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2)
    {
        return t1(vs, tif, entryKey, kv, kb, tag1) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag2) && rm(kv, tif.unique, tif.em2, chain);
    }
    
    private static boolean removeFrom2L(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2)
    {
        return t1(vs, tif, entryKey, kv, kb, tag2) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag2) && rm(kv, tif.unique, tif.em2, chain);
    }
    
    private static boolean removeFrom3F(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3)
    {
        return t1(vs, tif, entryKey, kv, kb, tag1) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag2) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag3) && rm(kv, tif.unique, tif.em3, chain);
    }
    
    private static boolean removeFrom3L(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3)
    {
        return t1(vs, tif, entryKey, kv, kb, tag3) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag2, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag3) && rm(kv, tif.unique, tif.em3, chain);
    }
    
    private static boolean removeFrom3M(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3)
    {
        return t1(vs, tif, entryKey, kv, kb, tag2) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag2) && rm(kv,tif.unique,  tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag2, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag3) && rm(kv, tif.unique, tif.em3, chain);
    }
    
    private static boolean removeFrom4F(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3, int tag4)
    {
        return t1(vs, tif, entryKey, kv, kb, tag1) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag2) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag4) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag3) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag3, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t4(vs, tif, entryKey, kv, kb, tag1, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em4, chain);
    }
    
    private static boolean removeFrom4L(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3, int tag4)
    {
        return t1(vs, tif, entryKey, kv, kb, tag4) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag4) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag2, tag4) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag3, tag4) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag3, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t4(vs, tif, entryKey, kv, kb, tag1, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em4, chain);
    }
    
    private static boolean removeFrom4N2(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3, int tag4)
    {
        return t1(vs, tif, entryKey, kv, kb, tag2) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag2) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag2, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag2, tag4) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag3) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t4(vs, tif, entryKey, kv, kb, tag1, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em4, chain);
    }
    
    private static boolean removeFrom4N3(final VisitorSession vs, final TagIndexFactory<?, ?, ?, ?> tif, final OpChain chain, 
            final byte[] entryKey, final KV kv, final KeyBuilder kb, 
            int tag1, int tag2, int tag3, int tag4)
    {
        return t1(vs, tif, entryKey, kv, kb, tag3) && rm(kv, tif.unique, tif.em1, chain)
                && t2(vs, tif, entryKey, kv, kb, tag1, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag2, tag3) && rm(kv, tif.unique, tif.em2, chain)
                && t2(vs, tif, entryKey, kv, kb, tag3, tag4) && rm(kv, tif.unique, tif.em2, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag2, tag3) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag1, tag3, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t3(vs, tif, entryKey, kv, kb, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em3, chain)
                && t4(vs, tif, entryKey, kv, kb, tag1, tag2, tag3, tag4) && rm(kv, tif.unique, tif.em4, chain);
    }
    
    static byte[] remove(int slot, int tagId, 
            byte[] value, int serOffset, int serSize, 
            final TagIndexFactory<?, ?, ?, ?> tif, 
            final OpChain chain, byte[] entryKey)
    {
        final VisitorSession vs = chain.vs();
        final KV kv = new KV();
        final KeyBuilder kb = chain.context.kb();
        
        final byte[] serTags = new byte[serSize - 4];
        
        final int prevTagCount = serSize/4;
        
        if (slot == 1)
        {
            // exclude first
            System.arraycopy(value, serOffset + 4, serTags, 0, serTags.length);
            
            switch (prevTagCount)
            {
                case 2:
                    if (!removeFrom2F(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4)))
                    {
                        return null;
                    }
                    break;
                case 3:
                    if (!removeFrom3F(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4),
                            toInt32LE(value, serOffset + 8)))
                    {
                        return null;
                    }
                    break;
                case 4:
                    if (!removeFrom4F(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4),
                            toInt32LE(value, serOffset + 8),
                            toInt32LE(value, serOffset + 12)))
                    {
                        return null;
                    }
                    break;
            }
            
            return serTags;
        }
        
        final int initialLen = 4 * (slot - 1);
        System.arraycopy(value, serOffset, serTags, 0, initialLen);
        
        if ((initialLen + 4) == serSize)
        {
            // last
            switch (prevTagCount)
            {
                case 2:
                    if (!removeFrom2L(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4)))
                    {
                        return null;
                    }
                    break;
                case 3:
                    if (!removeFrom3L(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4),
                            toInt32LE(value, serOffset + 8)))
                    {
                        return null;
                    }
                    break;
                case 4:
                    if (!removeFrom4L(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4),
                            toInt32LE(value, serOffset + 8),
                            toInt32LE(value, serOffset + 12)))
                    {
                        return null;
                    }
                    break;
            }
            
            return serTags;
        }
        
        // slot is somewhere in the middle, so add the remaining
        System.arraycopy(value, serOffset + initialLen + 4, 
                serTags, initialLen, 
                serSize - initialLen - 4);
        
        switch (prevTagCount)
        {
            case 3:
                if (!removeFrom3M(vs, tif, chain, entryKey, kv, kb, 
                        toInt32LE(value, serOffset), 
                        toInt32LE(value, serOffset + 4),
                        toInt32LE(value, serOffset + 8)))
                {
                    return null;
                }
                break;
            case 4:
                if (initialLen == 4)
                {
                    if (!removeFrom4N2(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4),
                            toInt32LE(value, serOffset + 8),
                            toInt32LE(value, serOffset + 12)))
                    {
                        return null;
                    }
                }
                else
                {
                    if (!removeFrom4N3(vs, tif, chain, entryKey, kv, kb, 
                            toInt32LE(value, serOffset), 
                            toInt32LE(value, serOffset + 4),
                            toInt32LE(value, serOffset + 8),
                            toInt32LE(value, serOffset + 12)))
                    {
                        return null;
                    }
                }
                break;
        }
        
        return serTags;
    }
}
