//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.tag;

import com.dyuproject.protostuffdb.Entity;
import com.dyuproject.protostuffdb.EntityMetadata;

/**
 * TODO
 * 
 * @author David Yu
 * @created Feb 7, 2017
 */
public abstract class TagIndexFactory<T1 extends Entity<T1>, 
        T2 extends Entity<T2>, 
        T3 extends Entity<T3>, 
        T4 extends Entity<T4>>
{
    /**
     * Default value is 225 (by convention).
     */
    public final int idxId;
    public final boolean unique, toggled;
    public final EntityMetadata<T1> em1;
    public final EntityMetadata<T2> em2;
    public final EntityMetadata<T3> em3;
    public final EntityMetadata<T4> em4;
    public final int f1, f2, f3, f4;
    public final int ie1, ie2, ie3, ie4;
    public final int vo1, vo2, vo3, vo4;
    
    protected TagIndexFactory(
            EntityMetadata<T1> em1, int f1, int ie1, int vo1, 
            EntityMetadata<T2> em2, int f2, int ie2, int vo2, 
            EntityMetadata<T3> em3, int f3, int ie3, int vo3, 
            EntityMetadata<T4> em4, int f4, int ie4, int vo4)
    {
        this(225, false, false,
                em1, f1, ie1, vo1,
                em2, f2, ie2, vo2,
                em3, f3, ie3, vo3,
                em4, f4, ie4, vo4);
    }
    
    protected TagIndexFactory(boolean unique, 
            EntityMetadata<T1> em1, int f1, int ie1, int vo1, 
            EntityMetadata<T2> em2, int f2, int ie2, int vo2, 
            EntityMetadata<T3> em3, int f3, int ie3, int vo3, 
            EntityMetadata<T4> em4, int f4, int ie4, int vo4)
    {
        this(225, unique, false,
                em1, f1, ie1, vo1,
                em2, f2, ie2, vo2,
                em3, f3, ie3, vo3,
                em4, f4, ie4, vo4);
    }
    
    protected TagIndexFactory(int idxId, boolean unique, boolean toggled, 
            EntityMetadata<T1> em1, int f1, int ie1, int vo1, 
            EntityMetadata<T2> em2, int f2, int ie2, int vo2, 
            EntityMetadata<T3> em3, int f3, int ie3, int vo3, 
            EntityMetadata<T4> em4, int f4, int ie4, int vo4)
    {
        this.idxId = idxId;
        this.unique = unique;
        this.toggled = toggled;
        this.em1 = em1;
        this.f1 = f1;
        this.ie1 = ie1;
        this.vo1 = vo1;
        
        this.em2 = em2;
        this.f2 = f2;
        this.ie2 = ie2;
        this.vo2 = vo2;
        
        this.em3 = em3;
        this.f3 = f3;
        this.ie3 = ie3;
        this.vo3 = vo3;
        
        this.em4 = em4;
        this.f4 = f4;
        this.ie4 = ie4;
        this.vo4 = vo4;
    }
    
    public abstract T1 newTagIndex1(byte[] entryKey, int tag1);
    
    public abstract T2 newTagIndex2(byte[] entryKey, int tag1, int tag2);
    
    public abstract T3 newTagIndex3(byte[] entryKey, int tag1, int tag2, int tag3);
    
    public abstract T4 newTagIndex4(byte[] entryKey, int tag1, int tag2, int tag3, int tag4);

}
