//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.tag;

import static com.dyuproject.protostuffdb.ValueUtil.toInt32LE;

import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuffdb.DSRuntimeExceptions;
import com.dyuproject.protostuffdb.Entity;
import com.dyuproject.protostuffdb.OpChain;

/**
 * TODO
 * 
 * @author David Yu
 * @created Feb 7, 2017
 */
public final class TagInsertUtil
{
    private TagInsertUtil() {}
    
    static <T extends Entity<T>> void tagInsert(
            OpChain chain, 
            final long now, final byte[] tiKey, 
            final T ti)
    {
        chain.context.fillEntityKey(tiKey, ti.em(), now);
        
        if (!chain.insertWithKey(tiKey, ti, ti.em(), 
                null, null, null))
        {
            throw DSRuntimeExceptions.operationFailure("Failed on tag indexing.");
        }
    }
    
    static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            byte[] insertFirstAndOnly(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tagToInsert)
    {
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tagToInsert/*, now*/));
        
        final byte[] serTags = new byte[4];
        IntSerializer.writeInt32LE(tagToInsert, serTags, 0);
        return serTags;
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom1(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tagToInsert, int tag1, int tag2)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tagToInsert/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag2/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom2F(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag1/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag2/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag3/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag3/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom2L(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag3/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag2, 
                tag3/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag3/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom2M(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag2/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag2/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag2, 
                tag3/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag3/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom3F(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3, int tag4)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag1/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag2/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag3, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex4(entryKey, 
                tag1, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom3L(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3, int tag4)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag2, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag3, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag3, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex4(entryKey, 
                tag1, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom3N2(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3, int tag4)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag2/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag2/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag2, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag2, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex4(entryKey, 
                tag1, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
    }
    
    private static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            void insertFrom3N3(byte[] tiKey, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, long now, byte[] entryKey, 
            int tag1, int tag2, int tag3, int tag4)
    {
        //final Long updateTs = now;
        tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                tag3/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag1, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag2, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                tag3, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag2, 
                tag3/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag1, 
                tag3, 
                tag4/*,
                updateTs*/));
        tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
        
        tagInsert(chain, now, tiKey, tif.newTagIndex4(entryKey, 
                tag1, 
                tag2, 
                tag3, 
                tag4/*,
                updateTs*/));
    }
    
    /**
     * Returns the index the tag was inserted.
     */
    static <T1 extends Entity<T1>, T2 extends Entity<T2>, T3 extends Entity<T3>, T4 extends Entity<T4>>
            int insert(int slot, int tagId, 
            byte[] value, int serOffset, int serSize, 
            final TagIndexFactory<T1,T2,T3,T4> tif, OpChain chain, byte[] entryKey, byte[] serTags)
    {
        final long now = System.currentTimeMillis();
        
        final byte[] tiKey = new byte[9];
        
        final int prevTagCount = serSize/4;
        
        final int insertIdx;
        if (slot == -1)
        {
            // first
            insertIdx = 0;
            System.arraycopy(value, serOffset, serTags, 4, serSize);
            IntSerializer.writeInt32LE(tagId, serTags, 0);
            
            switch (prevTagCount)
            {
                case 1:
                    insertFrom1(tiKey, tif, chain, now, entryKey, 
                            tagId, 
                            tagId,
                            toInt32LE(serTags, 4));
                    break;
                case 2:
                    insertFrom2F(tiKey, tif, chain, now, entryKey, 
                            tagId,
                            toInt32LE(serTags, 4), 
                            toInt32LE(serTags, 8));
                    break;
                case 3:
                    insertFrom3F(tiKey, tif, chain, now, entryKey, 
                            tagId,
                            toInt32LE(serTags, 4), 
                            toInt32LE(serTags, 8), 
                            toInt32LE(serTags, 12));
                    break;
            }
        }
        else if ((-slot - 1) == prevTagCount)
        {
            // last
            insertIdx = prevTagCount;
            System.arraycopy(value, serOffset, serTags, 0, serSize);
            IntSerializer.writeInt32LE(tagId, serTags, serSize);
            
            switch (prevTagCount)
            {
                case 1:
                    insertFrom1(tiKey, tif, chain, now, entryKey, 
                            tagId, 
                            toInt32LE(serTags, 0),
                            tagId);
                    break;
                case 2:
                    insertFrom2L(tiKey, tif, chain, now, entryKey, 
                            toInt32LE(serTags, 0),
                            toInt32LE(serTags, 4),
                            tagId);
                    break;
                case 3:
                    insertFrom3L(tiKey, tif, chain, now, entryKey, 
                            toInt32LE(serTags, 0),
                            toInt32LE(serTags, 4),
                            toInt32LE(serTags, 8),
                            tagId);
                    break;
            }
        }
        else
        {
            // mid
            // convert to zero-based
            int initialLen = 4 * (-slot - 1);
            System.arraycopy(value, serOffset, serTags, 0, initialLen);
            
            // write somewhere in the middle
            IntSerializer.writeInt32LE(tagId, serTags, initialLen);
            
            System.arraycopy(value, serOffset + initialLen, 
                    serTags, initialLen + 4, 
                    serTags.length - initialLen - 4);
            
            switch (prevTagCount)
            {
                case 2:
                    insertIdx = 1;
                    insertFrom2M(tiKey, tif, chain, now, entryKey, 
                            toInt32LE(serTags, 0),
                            tagId,
                            toInt32LE(serTags, 8));
                    break;
                case 3:
                    if (initialLen == 4)
                    {
                        insertIdx = 1;
                        insertFrom3N2(tiKey, tif, chain, now, entryKey, 
                                toInt32LE(serTags, 0),
                                tagId,
                                toInt32LE(serTags, 8),
                                toInt32LE(serTags, 12));
                    }
                    else
                    {
                        insertIdx = 2;
                        insertFrom3N3(tiKey, tif, chain, now, entryKey, 
                                toInt32LE(serTags, 0),
                                toInt32LE(serTags, 4),
                                tagId,
                                toInt32LE(serTags, 12));
                    }
                    break;
                default:
                    throw new RuntimeException("Should not happen.");
            }
        }
        
        return insertIdx;
    }
}
