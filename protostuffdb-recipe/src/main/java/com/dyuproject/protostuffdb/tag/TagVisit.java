//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.tag;

import static com.dyuproject.protostuffdb.SerializedValueUtil.asKey;

import java.util.ArrayList;
import java.util.List;

import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuff.ds.prk.v4.VisitP4;
import com.dyuproject.protostuff.ds.prk.v4.VisitP44;
import com.dyuproject.protostuff.ds.prk.v4.VisitP444;
import com.dyuproject.protostuff.ds.prk.v4.VisitP4444;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.KV;
import com.dyuproject.protostuffdb.KeyUtil;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.VisitorSession;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * TODO
 * 
 * @author David Yu
 * @created Jan 18, 2017
 */
public final class TagVisit
{
    static final class KeyExtractorVisitor implements Visitor<List<KV>>
    {
        final int vo;

        KeyExtractorVisitor(int vo)
        {
            this.vo = vo;
        }

        @Override
        public boolean visit(byte[] key, byte[] v, int voffset, int vlen, List<KV> param,
                int index)
        {
            param.add(new KV(asKey(vo, v, voffset, vlen), key));
            return false;
        }
    }
    
    public static TagVisit create(EntityMetadata<?> em, TagIndexFactory<?, ?, ?, ?> tif)
    {
        return new TagVisit(em.kind, tif);
    }
    
    private final byte[] startKey, endKey;
    final TagIndexFactory<?, ?, ?, ?> tif;
    final KeyExtractorVisitor kev1, kev2, kev3, kev4;
    
    private TagVisit(int kind, TagIndexFactory<?, ?, ?, ?> tif)
    {
        startKey = KeyUtil.newEntityKey(kind, 0, 0);
        endKey = new byte[]{
            (byte)kind, 
            (byte)0xFF, (byte)0xFF, 
            (byte)0xFF, (byte)0xFF, 
            (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF
        };
        
        this.tif = tif;
        kev1 = new KeyExtractorVisitor(tif.vo1);
        kev2 = new KeyExtractorVisitor(tif.vo2);
        kev3 = new KeyExtractorVisitor(tif.vo3);
        kev4 = new KeyExtractorVisitor(tif.vo4);
    }
    
    public boolean visit(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        switch (tagIds.size())
        {
            case 1:
                //if (sortByTagUpdateTs)
                //    by1u(tagIds, prk, session, context, list);
                //else
                    by1e(tagIds, prk, session, context, list);
                break;
            case 2:
                //if (sortByTagUpdateTs)
                //    by2u(tagIds, prk, session, context, list);
                //else
                    by2e(tagIds, prk, session, context, list);
                break;
            case 3:
                //if (sortByTagUpdateTs)
                //    by3u(tagIds, prk, session, context, list);
                //else
                    by3e(tagIds, prk, session, context, list);
                break;
            case 4:
                //if (sortByTagUpdateTs)
                //    by4u(tagIds, prk, session, context, list);
                //else
                    by4e(tagIds, prk, session, context, list);
                break;
            default:
                return false;
        }
        
        return true;
    }
    
    /*private void by1u(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        Visit.by4(
                TagIndex1.IDX_TAG1_ID__UPDATE_TS, 
                tagIds.get(0), 
                tif.em1, 
                TagIndex1.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev1, list);
    }
    
    private void by2u(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        VisitP4.byP44(
                TagIndex2.IDX_TAG1_ID__TAG2_ID__UPDATE_TS, 
                tagIds.get(0), 
                tagIds.get(1), 
                tif.em2, 
                TagIndex2.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev2, list);
    }
    
    private void by3u(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        VisitP44.byP444(
                TagIndex3.IDX_TAG1_ID__TAG2_ID__TAG3_ID__UPDATE_TS, 
                tagIds.get(0), 
                tagIds.get(1), 
                tagIds.get(2), 
                tif.em3, 
                TagIndex3.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev3, list);
    }
    
    private void by4u(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        VisitP444.byP4444(
                TagIndex4.IDX_TAG1_ID__TAG2_ID__TAG3_ID__TAG4_ID__UPDATE_TS, 
                tagIds.get(0), 
                tagIds.get(1), 
                tagIds.get(2), 
                tagIds.get(3), 
                tif.em4, 
                TagIndex4.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev4, list);
    }*/
    
    private void by1e(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        byte[] entryKey = prk.parentKey;
        if (entryKey != null)
            prk.parentKey = null;
        
        VisitP4.byP4Kr(
                tif.ie1, 
                tagIds.get(0), 
                startKey, 0,
                endKey,
                entryKey,
                tif.em1, 
                1, //TagIndex1.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev1, list);
    }
    
    private void by2e(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        byte[] entryKey = prk.parentKey;
        if (entryKey != null)
            prk.parentKey = null;
        
        VisitP44.byP44Kr(
                tif.ie2, 
                tagIds.get(0), 
                tagIds.get(1), 
                startKey, 0,
                endKey,
                entryKey,
                tif.em2, 
                1, //TagIndex2.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev2, list);
    }
    
    private void by3e(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        byte[] entryKey = prk.parentKey;
        if (entryKey != null)
            prk.parentKey = null;
        
        VisitP444.byP444Kr(
                tif.ie3, 
                tagIds.get(0), 
                tagIds.get(1), 
                tagIds.get(2), 
                startKey, 0,
                endKey,
                entryKey,
                tif.em3, 
                1, //TagIndex3.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev3, list);
    }
    
    private void by4e(List<Integer> tagIds, ParamRangeKey prk, VisitorSession session,
            WriteContext context, ArrayList<KV> list)
    {
        byte[] entryKey = prk.parentKey;
        if (entryKey != null)
            prk.parentKey = null;
        
        VisitP4444.byP4444Kr(
                tif.ie4, 
                tagIds.get(0), 
                tagIds.get(1), 
                tagIds.get(2), 
                tagIds.get(3), 
                startKey, 0,
                endKey,
                entryKey,
                tif.em4, 
                1, //TagIndex4.PList.FN_P, 
                prk, 
                RangeV.Session.V, session, context, 
                kev4, list);
    }
}
