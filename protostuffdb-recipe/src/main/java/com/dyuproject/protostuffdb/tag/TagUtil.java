//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb.tag;

import static com.dyuproject.protostuffdb.SerializedValueUtil.readBAO$len;
import static com.dyuproject.protostuffdb.SerializedValueUtil.readByteArray;
import static com.dyuproject.protostuffdb.ValueUtil.toInt32LE;
import static com.dyuproject.protostuffdb.tag.TagInsertUtil.insertFirstAndOnly;
import static com.dyuproject.protostuffdb.tag.TagInsertUtil.tagInsert;

import java.util.List;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuff.ds.MultiCAS;
import com.dyuproject.protostuffdb.DSRuntimeExceptions;
import com.dyuproject.protostuffdb.Entity;
import com.dyuproject.protostuffdb.KV;
import com.dyuproject.protostuffdb.OpChain;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * TODO
 * 
 * @author David Yu
 * @created Jan 18, 2017
 */
public final class TagUtil
{
    private TagUtil() {}
    
    private static Integer writeTo(byte[] ser_tags, List<Integer> tags, int i)
    {
        Integer tag = tags.get(i);
        IntSerializer.writeInt32LE(tag.intValue(), ser_tags, i * 4);
        return tag;
    }
    
    private static Integer $(List<Integer> tags, int i)
    {
        return tags.get(i);
    }
    
    public static <
            T1 extends Entity<T1>, 
            T2 extends Entity<T2>, 
            T3 extends Entity<T3>, 
            T4 extends Entity<T4>
            > byte[] insert(
            List<Integer> tags, 
            final TagIndexFactory<T1, T2, T3, T4> tif, 
            final OpChain chain, long now, byte[] entryKey)
    {
        final int size = tags.size();
        final byte[] serTags = new byte[4 * size];
        final byte[] tiKey = new byte[9];
        //final Long updateTs = now;
        switch (size)
        {
            case 1:
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        writeTo(serTags, tags, 0)/*, 
                        updateTs*/));
                break;
            case 2:
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 0)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 1)/*, 
                        updateTs*/));
                
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        writeTo(serTags, tags, 0), 
                        writeTo(serTags, tags, 1)/*, 
                        updateTs*/));
                break;
            case 3:
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 0)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 1)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 2)/*, 
                        updateTs*/));
                
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 0), 
                        $(tags, 1)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 0), 
                        $(tags, 2)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 1), 
                        $(tags, 2)/*, 
                        updateTs*/));
                
                tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                        writeTo(serTags, tags, 0), 
                        writeTo(serTags, tags, 1), 
                        writeTo(serTags, tags, 2)/*, 
                        updateTs*/));
                break;
            case 4:
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 0)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 1)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 2)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex1(entryKey, 
                        $(tags, 3)/*, 
                        updateTs*/));
                
                /* ================================================== */
                
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 0), 
                        $(tags, 1)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 0), 
                        $(tags, 2)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 0), 
                        $(tags, 3)/*, 
                        updateTs*/));
                
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 1), 
                        $(tags, 2)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 1), 
                        $(tags, 3)/*, 
                        updateTs*/));
                
                tagInsert(chain, now, tiKey, tif.newTagIndex2(entryKey, 
                        $(tags, 2), 
                        $(tags, 3)/*, 
                        updateTs*/));
                
                /* ================================================== */
                
                tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                        $(tags, 0), 
                        $(tags, 1), 
                        $(tags, 2)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                        $(tags, 0), 
                        $(tags, 1), 
                        $(tags, 3)/*, 
                        updateTs*/));
                tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                        $(tags, 0), 
                        $(tags, 2), 
                        $(tags, 3)/*, 
                        updateTs*/));
                
                tagInsert(chain, now, tiKey, tif.newTagIndex3(entryKey, 
                        $(tags, 1), 
                        $(tags, 2), 
                        $(tags, 3)/*, 
                        updateTs*/));

                tagInsert(chain, now, tiKey, tif.newTagIndex4(entryKey, 
                        writeTo(serTags, tags, 0), 
                        writeTo(serTags, tags, 1), 
                        writeTo(serTags, tags, 2), 
                        writeTo(serTags, tags, 3)/*, 
                        updateTs*/));
                
                break;
                
             default:
                 throw DSRuntimeExceptions.operationFailure("Min of 1 and max of 4 tags.");
        }
        
        return serTags;
    }
    
    public static <
            T1 extends Entity<T1>, 
            T2 extends Entity<T2>, 
            T3 extends Entity<T3>, 
            T4 extends Entity<T4>
            > int update(
            int tagId, boolean remove, 
            final TagIndexFactory<T1, T2, T3, T4> tif, //int fnSerTags, 
            final OpChain chain, 
            byte[] entryKey, MultiCAS mc, int field, byte[] entryValue)
    {
        final WriteContext context = chain.context;
        
        final int serOffset = readBAO$len(field, entryValue, context),
                serSize = context.$len;
        
        if (serSize == 0)
        {
            if (remove)
            {
                throw DSRuntimeExceptions.operationFailure(
                        "That tag has previously been removed.");
            }
            
            // insert single
            mc.addOp(CAS.BytesOp.asPassthrough(field, insertFirstAndOnly(new byte[9], 
                    tif, chain, System.currentTimeMillis(), entryKey, tagId)));
            return 0;
        }
        
        if (remove && serSize == 4)
        {
            if (tagId != toInt32LE(entryValue, serOffset))
            {
                throw DSRuntimeExceptions.operationFailure(
                        "That tag has previously been removed.");
            }
            
            // remove single
            if (!TagRemoveUtil.removeFirstAndOnly(chain.vs(), tif, chain, entryKey, 
                    new KV(), context.kb(), tagId))
            {
                throw DSRuntimeExceptions.operationFailure("Could not remove tag.");
            }
            
            mc.addOp(CAS.BytesOp.asPassthrough(field, ByteString.EMPTY_BYTE_ARRAY));
            return -1;
        }
        
        final byte[] serTags;
        final int slot = TagUtil.getTagSlot(tagId, entryValue, serOffset, serSize);
        int insertSlot = -1;
        if (remove)
        {
            if (slot < 0)
            {
                throw DSRuntimeExceptions.operationFailure(
                        "That tag has previously been removed.");
            }
            serTags = TagRemoveUtil.remove(slot, tagId, entryValue, serOffset, serSize,
                    tif, chain, entryKey);
            
            if (serTags == null)
                throw DSRuntimeExceptions.operationFailure("Could not remove tag.");
        }
        else if (slot > 0)
        {
            throw DSRuntimeExceptions.operationFailure(
                    "The entry already contains that tag.");
        }
        else
        {
            // store slot in param
            serTags = new byte[serSize + 4];
            insertSlot = TagInsertUtil.insert(slot, tagId, entryValue, 
                    serOffset, serSize,
                    tif, chain, entryKey, serTags);
        }
        
        mc.addOp(CAS.BytesOp.asPassthrough(field, serTags));
        return insertSlot;
    }
    
    public static <
            T1 extends Entity<T1>, 
            T2 extends Entity<T2>, 
            T3 extends Entity<T3>, 
            T4 extends Entity<T4>
            > int replace(
            int tagId, int newTagId, 
            final TagIndexFactory<T1, T2, T3, T4> tif, //int fnSerTags, 
            final OpChain chain, 
            byte[] entryKey, MultiCAS mc, int field, byte[] entryValue)
    {
        final WriteContext context = chain.context;
        
        final byte[] serTags = readByteArray(field, entryValue, context);
        
        int slot, newSlot;
        if (serTags.length == 0 || 
                (slot = getTagSlots(tagId, newTagId, serTags, 0, serTags.length, context)) < 0)
        {
            throw DSRuntimeExceptions.operationFailure(
                    "That tag has previously been removed.");
        }
        
        if ((newSlot = context.$offset) > 0)
        {
            throw DSRuntimeExceptions.operationFailure(
                    "The entry already contains that tag.");
        }
        
        // serTags updated by the replaceTag proc
        mc.addOp(CAS.BytesOp.asPassthrough(field, serTags));
        
        return TagReplaceUtil.replace(tagId, slot, newTagId, newSlot, tif, chain, entryKey, serTags);
    }
    
    /**
     * Returns a 1-based slot. Negative if not found.
     */
    static int getTagSlot(int tagId, byte[] value, int serOffset, int serSize)
    {
        int id = 0,
                slot = 1;
        for (int i = 0; i < serSize; i += 4, slot++)
        {
            id = toInt32LE(value, serOffset + i);
            if (tagId == id)
                return slot;
            
            if (tagId < id)
                return -slot;
        }
        
        return -slot;
    }
    
    private static int getTagSlots(int tagId, int newTagId, 
            byte[] value, int serOffset, int serSize,
            WriteContext context)
    {
        int id = 0,
                slot = 1,
                tagSlot = 0,
                newTagSlot = 0;
        for (int i = 0; i < serSize; i += 4, slot++)
        {
            id = toInt32LE(value, serOffset + i);
            if (tagId == id)
                tagSlot = slot;
            if (newTagId == id)
                newTagSlot = slot;
            
            if (tagSlot == 0 && tagId < id)
                tagSlot = -slot;
            if (newTagSlot == 0 && newTagId < id)
                newTagSlot = -slot;
            
            if (tagSlot != 0 && newTagSlot != 0)
            {
                context.$offset = newTagSlot;
                return tagSlot;
            }
        }
        
        if (tagSlot == 0)
            tagSlot = -slot;
        if (newTagSlot == 0)
            newTagSlot = -slot;
        
        context.$offset = newTagSlot;
        return tagSlot;
    }
}
