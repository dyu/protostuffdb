//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import com.dyuproject.protostuff.KeyBuilder;

/**
 * Key builders.
 * 
 * @author David Yu
 * @created Aug 7, 2014
 */
public final class Kb
{
    private Kb() {}

    public static <T,S,V> KeyBuilder by1(int indexId, 
            int p0, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .$append8(p0);
    }
    
    public static <T,S,V> KeyBuilder by4(int indexId, 
            int p0, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .$append(p0);
    }
    
    public static <T,S,V> KeyBuilder by8(int indexId, 
            long p0, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .$append(p0);
    }
    
    public static <T,S,V> KeyBuilder byD(int indexId, 
            long p0, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .$append6Bytes(p0);
    }
    
    public static <T,S,V> KeyBuilder byD(int indexId, 
            byte[] p0, int p0off, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 6);
    }
    
    public static <T,S,V> KeyBuilder byK(int indexId, 
            byte[] p0, int p0off, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9);
    }
    
    public static <T,S,V> KeyBuilder byS(int indexId, 
            String p0, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .append(p0);
    }
    
    public static <T,S,V> KeyBuilder byS(int indexId, 
            byte[] p0, int p0off, int p0len, 
            EntityMetadata<T> em, 
            KeyBuilder kb) 
    {
        return kb.begin(indexId, em)
                .append(p0, p0off, p0len);
    }
}
