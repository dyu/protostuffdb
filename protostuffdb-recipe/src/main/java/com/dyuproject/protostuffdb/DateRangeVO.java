//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.SerializedValueUtil.asInt64;

/**
 * TODO
 * 
 * @author David Yu
 * @created May 24, 2016
 */
public final class DateRangeVO
{
    public static final Visitor<DateRangeVO> VISITOR = new Visitor<DateRangeVO>()
    {
        @Override
        public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                DateRangeVO param, int index)
        {
            if (param.dateStart == 0)
            {
                param.dateStart = asInt64(param.voStart, v, voffset, vlen);
                param.dateEnd = asInt64(param.voEnd, v, voffset, vlen);
            }
            
            return false;
        }
    };
    
    public final int voStart, voEnd;
    public long dateStart, dateEnd;
    
    public DateRangeVO(int voStart, int voEnd)
    {
        this.voStart = voStart;
        this.voEnd = voEnd;
    }
}
