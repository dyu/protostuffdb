// Generated by fbsgen from p/ds/prk/p8/P8K.proto

package com.dyuproject.protostuff.ds.prk.v8;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;
import com.dyuproject.protostuff.ds.prk.p8.P8K1;
import com.dyuproject.protostuff.ds.prk.p8.P8K4;
import com.dyuproject.protostuff.ds.prk.p8.P8K8;
import com.dyuproject.protostuff.ds.prk.p8.P8KD;
import com.dyuproject.protostuff.ds.prk.p8.P8KK;
import com.dyuproject.protostuff.ds.prk.p8.P8KS;

public final class VisitP8K
{
    private VisitP8K() {}

    public static <T,S,V> boolean byP8K1(int indexId, 
            long p0, byte[] p1, int p1off, 
            int p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append8(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8K1(int indexId, 
            P8K1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8K1(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8K1r(int indexId, 
            long p0, byte[] p1, int p1off, 
            int p2, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append8(p2)
                .$opushRange8(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy8(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8K1r(int indexId, 
            P8K1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8K1r(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8K4(int indexId, 
            long p0, byte[] p1, int p1off, 
            int p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8K4(int indexId, 
            P8K4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8K4(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8K4r(int indexId, 
            long p0, byte[] p1, int p1off, 
            int p2, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8K4r(int indexId, 
            P8K4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8K4r(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8K8(int indexId, 
            long p0, byte[] p1, int p1off, 
            long p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {        
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8K8(int indexId, 
            P8K8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8K8(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8K8r(int indexId, 
            long p0, byte[] p1, int p1off, 
            long p2, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8K8r(int indexId, 
            P8K8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8K8r(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KD(int indexId, 
            long p0, byte[] p1, int p1off, 
            long p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append6Bytes(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KD(int indexId, 
            P8KD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KD(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KDr(int indexId, 
            long p0, byte[] p1, int p1off, 
            long p2, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append6Bytes(p2)
                .$opushRange6Bytes(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy6Bytes(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KDr(int indexId, 
            P8KD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KDr(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KK(int indexId, 
            long p0, byte[] p1, int p1off, 
            byte[] p2, int p2offset, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2, p2offset, 9)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KK(int indexId, 
            P8KK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KK(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 0, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KKr(int indexId, 
            long p0, byte[] p1, int p1off, 
            byte[] p2, int p2offset, byte[] end, byte[] pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2, p2offset, 9)
                .$opushRange(end, 9);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, 9, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KKr(int indexId, 
            P8KK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KKr(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 0, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KS(int indexId, 
            long p0, byte[] p1, int p1off, 
            String p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KS(int indexId, 
            P8KS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KS(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KSr(int indexId, 
            long p0, byte[] p1, int p1off, 
            String p2, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$(lvs=kb.appendAndGetSize(p2))
                .$opushRange(end, lvs);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KSr(int indexId, 
            P8KS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KSr(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KSp(int indexId, 
            long p0, byte[] p1, int p1off, 
            String p2, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$(lvs=kb.$appendAndGetSize(p2))
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KSp(int indexId, 
            P8KS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KSp(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP8KSpr(int indexId, 
            long p0, byte[] p1, int p1off, 
            String p2, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append(p1, p1off, 9)
                .$(lvs=kb.$appendAndGetSize(p2))
                .$opushRange(end, lvs, true);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, true, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP8KSpr(int indexId, 
            P8KS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP8KSpr(indexId, 
                req.p0, req.p1, 0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }
}