// Generated by fbsgen from p/ds/prk/p8/P8K.proto

package com.dyuproject.protostuff.ds.prk.b8;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbP8K
{
    private KbP8K() {}

    public static <T> KeyBuilder byP8K1(int indexId, 
            long p0, byte[] p1, int p1off, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .$append8(p2);
    }

    public static <T> KeyBuilder byP8K4(int indexId, 
            long p0, byte[] p1, int p1off, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2);
    }

    public static <T> KeyBuilder byP8K8(int indexId, 
            long p0, byte[] p1, int p1off, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2);
    }

    public static <T> KeyBuilder byP8KD(int indexId, 
            long p0, byte[] p1, int p1off, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .$append6Bytes(p2);
    }

    public static <T> KeyBuilder byP8KD(int indexId, 
            long p0, byte[] p1, int p1off, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2, p2off, 6);
    }

    public static <T> KeyBuilder byP8KK(int indexId, 
            long p0, byte[] p1, int p1off, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .$append(p2, p2off, 9);
    }

    public static <T> KeyBuilder byP8KS(int indexId, 
            long p0, byte[] p1, int p1off, 
            String p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .append(p2);
    }

    public static <T> KeyBuilder byP8KS(int indexId, 
            long p0, byte[] p1, int p1off, 
            byte[] p2, int p2off, int p2len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).$append(p1, p1off, 9)
                .append(p2, p2off, p2len);
    }
}