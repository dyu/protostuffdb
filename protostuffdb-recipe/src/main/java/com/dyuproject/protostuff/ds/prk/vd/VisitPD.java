// Generated by fbsgen from p/ds/prk/pd/PD.proto

package com.dyuproject.protostuff.ds.prk.vd;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;
import com.dyuproject.protostuff.ds.prk.pd.PD1;
import com.dyuproject.protostuff.ds.prk.pd.PD4;
import com.dyuproject.protostuff.ds.prk.pd.PD8;
import com.dyuproject.protostuff.ds.prk.pd.PDD;
import com.dyuproject.protostuff.ds.prk.pd.PDK;
import com.dyuproject.protostuff.ds.prk.pd.PDS;

public final class VisitPD
{
    private VisitPD() {}

    public static <T,S,V> boolean byPD1(int indexId, 
            long p0, 
            int p1, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append8(p1)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPD1(int indexId, 
            PD1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPD1(indexId, 
                req.p0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPD1r(int indexId, 
            long p0, 
            int p1, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append8(p1)
                .$opushRange8(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy8(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPD1r(int indexId, 
            PD1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPD1r(indexId, 
                req.p0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPD4(int indexId, 
            long p0, 
            int p1, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append(p1)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPD4(int indexId, 
            PD4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPD4(indexId, 
                req.p0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPD4r(int indexId, 
            long p0, 
            int p1, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append(p1)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPD4r(int indexId, 
            PD4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPD4r(indexId, 
                req.p0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPD8(int indexId, 
            long p0, 
            long p1, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {        
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append(p1)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPD8(int indexId, 
            PD8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPD8(indexId, 
                req.p0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPD8r(int indexId, 
            long p0, 
            long p1, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append(p1)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPD8r(int indexId, 
            PD8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPD8r(indexId, 
                req.p0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDD(int indexId, 
            long p0, 
            long p1, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append6Bytes(p1)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDD(int indexId, 
            PDD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDD(indexId, 
                req.p0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDDr(int indexId, 
            long p0, 
            long p1, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append6Bytes(p1)
                .$opushRange6Bytes(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy6Bytes(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDDr(int indexId, 
            PDD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDDr(indexId, 
                req.p0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDK(int indexId, 
            long p0, 
            byte[] p1, int p1offset, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append(p1, p1offset, 9)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDK(int indexId, 
            PDK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDK(indexId, 
                req.p0, 
                req.l.value, 0, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDKr(int indexId, 
            long p0, 
            byte[] p1, int p1offset, byte[] end, byte[] pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$append(p1, p1offset, 9)
                .$opushRange(end, 9);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, 9, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDKr(int indexId, 
            PDK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDKr(indexId, 
                req.p0, 
                req.l.value, 0, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDS(int indexId, 
            long p0, 
            String p1, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .append(p1)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDS(int indexId, 
            PDS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDS(indexId, 
                req.p0, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDSr(int indexId, 
            long p0, 
            String p1, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$(lvs=kb.appendAndGetSize(p1))
                .$opushRange(end, lvs);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDSr(int indexId, 
            PDS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDSr(indexId, 
                req.p0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDSp(int indexId, 
            long p0, 
            String p1, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$(lvs=kb.$appendAndGetSize(p1))
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDSp(int indexId, 
            PDS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDSp(indexId, 
                req.p0, 
                req.l.value, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPDSpr(int indexId, 
            long p0, 
            String p1, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append6Bytes(p0)
                .$(lvs=kb.$appendAndGetSize(p1))
                .$opushRange(end, lvs, true);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, true, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPDSpr(int indexId, 
            PDS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPDSpr(indexId, 
                req.p0, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }
}