// Generated by fbsgen from p/ds/prk/p1/P114.proto

package com.dyuproject.protostuff.ds.prk.b1;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbP114
{
    private KbP114() {}

    public static <T> KeyBuilder byP1141(int indexId, 
            int p0, int p1, int p2, 
            int p3, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .$append8(p3);
    }

    public static <T> KeyBuilder byP1144(int indexId, 
            int p0, int p1, int p2, 
            int p3, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .$append(p3);
    }

    public static <T> KeyBuilder byP1148(int indexId, 
            int p0, int p1, int p2, 
            long p3, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .$append(p3);
    }

    public static <T> KeyBuilder byP114D(int indexId, 
            int p0, int p1, int p2, 
            long p3, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .$append6Bytes(p3);
    }

    public static <T> KeyBuilder byP114D(int indexId, 
            int p0, int p1, int p2, 
            byte[] p3, int p3off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .$append(p3, p3off, 6);
    }

    public static <T> KeyBuilder byP114K(int indexId, 
            int p0, int p1, int p2, 
            byte[] p3, int p3off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .$append(p3, p3off, 9);
    }

    public static <T> KeyBuilder byP114S(int indexId, 
            int p0, int p1, int p2, 
            String p3, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .append(p3);
    }

    public static <T> KeyBuilder byP114S(int indexId, 
            int p0, int p1, int p2, 
            byte[] p3, int p3off, int p3len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append(p2)
                .append(p3, p3off, p3len);
    }
}