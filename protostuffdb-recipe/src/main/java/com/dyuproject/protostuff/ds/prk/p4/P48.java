// Generated by fbsgen from p/ds/prk/p4/P4.proto

package com.dyuproject.protostuff.ds.prk.p4;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.UninitializedMessageException;
/**
 * <pre>
 * message P48 {
 *   required uint32 p0 = 1;
 *   required com.dyuproject.protostuff.ds.P8 l = 2;
 * }
 * </pre>
 */
public final class P48 implements Message<P48>
{
    public static final int FN_P0 = 1;
    public static final int FN_L = 2;
    static final String P0_REQUIRED_ERR_MESSAGE = "P0 is required.";

    static final String P0_MIN_ERR_MESSAGE = "P0 is invalid.";

    static final String L_REQUIRED_ERR_MESSAGE = "L is required.";


    /** Required. */
    public Integer p0;

    /** Required. */
    public com.dyuproject.protostuff.ds.P8 l;


    public P48() {}

    public P48(
        Integer p0,
        com.dyuproject.protostuff.ds.P8 l
    )
    {
        this.p0 = p0;
        this.l = l;
    }



    public Schema<P48> cachedSchema() { return SCHEMA; }

    static final Schema<P48> SCHEMA = new Schema<P48>()
    {
        // schema methods

        public P48 newMessage()
        {
            return new P48();
        }

        public Class<P48> typeClass()
        {
            return P48.class;
        }

        public String messageName()
        {
            return P48.class.getSimpleName();
        }

        public String messageFullName()
        {
            return P48.class.getName();
        }

        public boolean isInitialized(final P48 message)
        {
            if (com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    P0_REQUIRED_ERR_MESSAGE, message.p0))
            {
                com.dyuproject.protostuff.RpcValidation.verifyMin(P0_MIN_ERR_MESSAGE, message.p0, 0);
            }



            com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    L_REQUIRED_ERR_MESSAGE, message.l);




            return true;
        }
        public void mergeFrom(final Input input, final P48 message) throws IOException
        {
            for (int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch (number)
                {
                    case 0:
                        return;
                    case 1:
                        message.p0 = input.readUInt32();
                        break;
                    case 2:
                        message.l = input.mergeObject(message.l, com.dyuproject.protostuff.ds.P8.getSchema());
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }

        public void writeTo(final Output output, final P48 message) throws IOException
        {
            if (message.p0 == null)
                throw new UninitializedMessageException("p0", message);
            output.writeUInt32(1, message.p0, false);


            if (message.l == null)
                throw new UninitializedMessageException("l", message);
            output.writeObject(2, message.l, com.dyuproject.protostuff.ds.P8.getSchema(), false);


        }

        public String getFieldName(final int number)
        {
            switch(number)
            {
                case 1: return "p0";
                case 2: return "l";
                default: return null;
            }
        }

        public int getFieldNumber(final String name)
        {
            final Integer number = fieldMap.get(name);
            return number == null ? 0 : number.intValue();
        }

        final java.util.HashMap<String,Integer> fieldMap = new java.util.HashMap<String,Integer>();
        {
            fieldMap.put("p0", 1);
            fieldMap.put("l", 2);
        }

    };

    /**
     * Useful for filtering (called by your custom pipe schema).
     */
    public static void transferField(int number, Pipe pipe, Input input, Output output, 
            Schema<P48> wrappedSchema) throws IOException
    {
        switch (number)
        {
            case 1:
                output.writeUInt32(number, input.readUInt32(), false);
                break;
            case 2:
                output.writeObject(number, pipe, com.dyuproject.protostuff.ds.P8.getPipeSchema(), false);
                break;
            default:
                input.handleUnknownField(number, wrappedSchema);
        }
    }

    static final Pipe.Schema<P48> PIPE_SCHEMA = new Pipe.Schema<P48>(SCHEMA)
    {
        protected void transfer(final Pipe pipe, final Input input, final Output output) throws IOException
        {
            for (int number = input.readFieldNumber(wrappedSchema);
                    number != 0;
                    number = input.readFieldNumber(wrappedSchema))
            {
                transferField(number, pipe, input, output, wrappedSchema);
            }
        }
    };

    public static Schema<P48> getSchema() { return SCHEMA; }
    public static Pipe.Schema<P48> getPipeSchema() { return PIPE_SCHEMA; }



}

