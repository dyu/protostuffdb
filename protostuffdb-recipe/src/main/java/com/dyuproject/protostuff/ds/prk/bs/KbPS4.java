// Generated by fbsgen from p/ds/prk/ps/PS4.proto

package com.dyuproject.protostuff.ds.prk.bs;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbPS4
{
    private KbPS4() {}

    public static <T> KeyBuilder byPS41(int indexId, 
            String p0, int p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append8(p2);
    }

    public static <T> KeyBuilder byPS44(int indexId, 
            String p0, int p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byPS48(int indexId, 
            String p0, int p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byPS4D(int indexId, 
            String p0, int p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append6Bytes(p2);
    }

    public static <T> KeyBuilder byPS4D(int indexId, 
            String p0, int p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2, p2off, 6);
    }

    public static <T> KeyBuilder byPS4K(int indexId, 
            String p0, int p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2, p2off, 9);
    }

    public static <T> KeyBuilder byPS4S(int indexId, 
            String p0, int p1, 
            String p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .append(p2);
    }

    public static <T> KeyBuilder byPS4S(int indexId, 
            String p0, int p1, 
            byte[] p2, int p2off, int p2len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .append(p2, p2off, p2len);
    }
}