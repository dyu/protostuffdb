// Generated by fbsgen from p/ds/prk/p8/P8S.proto

package com.dyuproject.protostuff.ds.prk.b8;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbP8S
{
    private KbP8S() {}

    public static <T> KeyBuilder byP8S1(int indexId, 
            long p0, String p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .$append8(p2);
    }

    public static <T> KeyBuilder byP8S4(int indexId, 
            long p0, String p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byP8S8(int indexId, 
            long p0, String p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byP8SD(int indexId, 
            long p0, String p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .$append6Bytes(p2);
    }

    public static <T> KeyBuilder byP8SD(int indexId, 
            long p0, String p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .$append(p2, p2off, 6);
    }

    public static <T> KeyBuilder byP8SK(int indexId, 
            long p0, String p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .$append(p2, p2off, 9);
    }

    public static <T> KeyBuilder byP8SS(int indexId, 
            long p0, String p1, 
            String p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .append(p2);
    }

    public static <T> KeyBuilder byP8SS(int indexId, 
            long p0, String p1, 
            byte[] p2, int p2off, int p2len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0).append(p1)
                .append(p2, p2off, p2len);
    }
}