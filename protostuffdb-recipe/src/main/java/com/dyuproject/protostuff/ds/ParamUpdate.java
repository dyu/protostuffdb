// Generated by fbsgen from p/ds/prk.proto

package com.dyuproject.protostuff.ds;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.UninitializedMessageException;
/**
 * <pre>
 * message ParamUpdate {
 *   required bytes key = 1;
 *   required MultiCAS mc = 2;
 *   optional uint32 id = 3;
 * }
 * </pre>
 */
public final class ParamUpdate implements com.dyuproject.protostuffdb.HasKey, Message<ParamUpdate>
{
    public static final int FN_KEY = 1;
    public static final int FN_MC = 2;
    public static final int FN_ID = 3;
    static final String KEY_REQUIRED_ERR_MESSAGE = "Key is required.";

    static final String KEY_KEY_ERR_MESSAGE = "Key is invalid.";

    static final String MC_REQUIRED_ERR_MESSAGE = "Mc is required.";

    static final String MC_NOT_EMPTY_ERR_MESSAGE = "Mc is invalid.";

    static final String ID_MIN_ERR_MESSAGE = "Id is invalid.";

    public static final int DEFAULT_ID = 0;

        public byte[] getKey() { return key; }
        public void setKey(byte[] key) { this.key = key; }
      
    /** Required. */
    public byte[] key;

    /** Required. */
    public MultiCAS mc;

    /** Optional. */
    public int id = DEFAULT_ID;


    public ParamUpdate() {}

    public ParamUpdate(
        byte[] key,
        MultiCAS mc
    )
    {
        this.key = key;
        this.mc = mc;
    }



    public Schema<ParamUpdate> cachedSchema() { return SCHEMA; }

    static final Schema<ParamUpdate> SCHEMA = new Schema<ParamUpdate>()
    {
        // schema methods

        public ParamUpdate newMessage()
        {
            return new ParamUpdate();
        }

        public Class<ParamUpdate> typeClass()
        {
            return ParamUpdate.class;
        }

        public String messageName()
        {
            return ParamUpdate.class.getSimpleName();
        }

        public String messageFullName()
        {
            return ParamUpdate.class.getName();
        }

        public boolean isInitialized(final ParamUpdate message)
        {
            if (com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    KEY_REQUIRED_ERR_MESSAGE, message.key))
            {
                com.dyuproject.protostuff.RpcValidation.verifyKey(KEY_KEY_ERR_MESSAGE, message.key);
            }



            if (com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    MC_REQUIRED_ERR_MESSAGE, message.mc))
            {
                com.dyuproject.protostuff.RpcValidation.verifyNotEmpty(MC_NOT_EMPTY_ERR_MESSAGE, message.mc);
            }



            if (message.id != DEFAULT_ID)
            {
                // default value configured to be out-of-range
                com.dyuproject.protostuff.RpcValidation.verifyMin(ID_MIN_ERR_MESSAGE, message.id, 1);
            }





            return true;
        }
        public void mergeFrom(final Input input, final ParamUpdate message) throws IOException
        {
            for (int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch (number)
                {
                    case 0:
                        return;
                    case 1:
                        message.key = input.readByteArray();
                        break;
                    case 2:
                        message.mc = input.mergeObject(message.mc, MultiCAS.getSchema());
                        break;
                    case 3:
                        message.id = input.readFixed32();
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }

        public void writeTo(final Output output, final ParamUpdate message) throws IOException
        {
            if (message.key == null)
                throw new UninitializedMessageException("key", message);
            output.writeByteArray(1, message.key, false);


            if (message.mc == null)
                throw new UninitializedMessageException("mc", message);
            output.writeObject(2, message.mc, MultiCAS.getSchema(), false);


            if (message.id != DEFAULT_ID)
                output.writeFixed32(3, message.id, false);




        }

        public String getFieldName(final int number)
        {
            switch(number)
            {
                case 1: return "key";
                case 2: return "mc";
                case 3: return "id";
                default: return null;
            }
        }

        public int getFieldNumber(final String name)
        {
            final Integer number = fieldMap.get(name);
            return number == null ? 0 : number.intValue();
        }

        final java.util.HashMap<String,Integer> fieldMap = new java.util.HashMap<String,Integer>();
        {
            fieldMap.put("key", 1);
            fieldMap.put("mc", 2);
            fieldMap.put("id", 3);
        }

    };

    /**
     * Useful for filtering (called by your custom pipe schema).
     */
    public static void transferField(int number, Pipe pipe, Input input, Output output, 
            Schema<ParamUpdate> wrappedSchema) throws IOException
    {
        switch (number)
        {
            case 1:
                input.transferByteRangeTo(output, false, number, false);
                break;
            case 2:
                output.writeObject(number, pipe, MultiCAS.getPipeSchema(), false);
                break;
            case 3:
                output.writeFixed32(number, input.readFixed32(), false);
                break;
            default:
                input.handleUnknownField(number, wrappedSchema);
        }
    }

    static final Pipe.Schema<ParamUpdate> PIPE_SCHEMA = new Pipe.Schema<ParamUpdate>(SCHEMA)
    {
        protected void transfer(final Pipe pipe, final Input input, final Output output) throws IOException
        {
            for (int number = input.readFieldNumber(wrappedSchema);
                    number != 0;
                    number = input.readFieldNumber(wrappedSchema))
            {
                transferField(number, pipe, input, output, wrappedSchema);
            }
        }
    };

    public static Schema<ParamUpdate> getSchema() { return SCHEMA; }
    public static Pipe.Schema<ParamUpdate> getPipeSchema() { return PIPE_SCHEMA; }



}

