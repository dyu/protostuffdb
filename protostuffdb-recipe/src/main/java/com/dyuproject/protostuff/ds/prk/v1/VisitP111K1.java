// Generated by fbsgen from p/ds/prk/p1/P111K1.proto

package com.dyuproject.protostuff.ds.prk.v1;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;
import com.dyuproject.protostuff.ds.prk.p1.P111K11;
import com.dyuproject.protostuff.ds.prk.p1.P111K14;
import com.dyuproject.protostuff.ds.prk.p1.P111K18;
import com.dyuproject.protostuff.ds.prk.p1.P111K1D;
import com.dyuproject.protostuff.ds.prk.p1.P111K1K;
import com.dyuproject.protostuff.ds.prk.p1.P111K1S;

public final class VisitP111K1
{
    private VisitP111K1() {}

    public static <T,S,V> boolean byP111K11(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            int p5, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append8(p5)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K11(int indexId, 
            P111K11 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K11(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K11r(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            int p5, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append8(p5)
                .$opushRange8(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy8(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K11r(int indexId, 
            P111K11 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K11r(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K14(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            int p5, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append(p5)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K14(int indexId, 
            P111K14 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K14(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K14r(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            int p5, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append(p5)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K14r(int indexId, 
            P111K14 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K14r(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K18(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            long p5, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {        
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append(p5)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K18(int indexId, 
            P111K18 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K18(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K18r(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            long p5, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append(p5)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K18r(int indexId, 
            P111K18 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K18r(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1D(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            long p5, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append6Bytes(p5)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1D(int indexId, 
            P111K1D req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1D(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1Dr(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            long p5, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append6Bytes(p5)
                .$opushRange6Bytes(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy6Bytes(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1Dr(int indexId, 
            P111K1D req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1Dr(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1K(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            byte[] p5, int p5offset, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append(p5, p5offset, 9)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1K(int indexId, 
            P111K1K req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1K(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 0, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1Kr(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            byte[] p5, int p5offset, byte[] end, byte[] pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$append(p5, p5offset, 9)
                .$opushRange(end, 9);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, 9, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1Kr(int indexId, 
            P111K1K req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1Kr(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 0, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1S(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            String p5, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .append(p5)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1S(int indexId, 
            P111K1S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1S(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1Sr(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            String p5, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$(lvs=kb.appendAndGetSize(p5))
                .$opushRange(end, lvs);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1Sr(int indexId, 
            P111K1S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1Sr(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1Sp(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            String p5, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$(lvs=kb.$appendAndGetSize(p5))
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1Sp(int indexId, 
            P111K1S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1Sp(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP111K1Spr(int indexId, 
            int p0, int p1, int p2, byte[] p3, int p3off, int p4, 
            String p5, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3, p3off, 9).$append8(p4)
                .$(lvs=kb.$appendAndGetSize(p5))
                .$opushRange(end, lvs, true);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, true, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP111K1Spr(int indexId, 
            P111K1S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP111K1Spr(indexId, 
                req.p0, req.p1, req.p2, req.p3, 0, req.p4, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }
}