// Generated by fbsgen from p/ds/prk.proto

package com.dyuproject.protostuff.ds;

import java.io.IOException;

import com.dyuproject.protostuff.ByteString;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.UninitializedMessageException;
/**
 * <pre>
 * message PK {
 *   required bytes value = 1;
 *   optional bytes end = 2;
 *   optional bytes pgstart = 3;
 *   required ParamRangeKey prk = 4;
 * }
 * </pre>
 */
public final class PK implements Message<PK>
{
    public static final int FN_VALUE = 1;
    public static final int FN_END = 2;
    public static final int FN_PGSTART = 3;
    public static final int FN_PRK = 4;
    static final String VALUE_REQUIRED_ERR_MESSAGE = "Value is required.";

    static final String VALUE_KEY_ERR_MESSAGE = "Value is invalid.";

    static final String END_KEY_ERR_MESSAGE = "End is invalid.";

    static final String PGSTART_KEY_ERR_MESSAGE = "Pgstart is invalid.";

    static final String PRK_REQUIRED_ERR_MESSAGE = "Prk is required.";



        public static PK create(byte[] value) { return new PK(value, new ParamRangeKey(false)); }
        //public PK(byte[] value) { this(value, new ParamRangeKey(false)); }
      
    /** Required. */
    public byte[] value;

    /** Optional. */
    public byte[] end = ByteString.EMPTY_BYTE_ARRAY;

    /** Optional. */
    public byte[] pgstart = ByteString.EMPTY_BYTE_ARRAY;

    /** Required. */
    public ParamRangeKey prk;


    public PK() {}

    public PK(
        byte[] value,
        ParamRangeKey prk
    )
    {
        this.value = value;
        this.prk = prk;
    }



    public Schema<PK> cachedSchema() { return SCHEMA; }

    static final Schema<PK> SCHEMA = new Schema<PK>()
    {
        // schema methods

        public PK newMessage()
        {
            return new PK();
        }

        public Class<PK> typeClass()
        {
            return PK.class;
        }

        public String messageName()
        {
            return PK.class.getSimpleName();
        }

        public String messageFullName()
        {
            return PK.class.getName();
        }

        public boolean isInitialized(final PK message)
        {
            if (com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    VALUE_REQUIRED_ERR_MESSAGE, message.value))
            {
                com.dyuproject.protostuff.RpcValidation.verifyKey(VALUE_KEY_ERR_MESSAGE, message.value);
            }



            if (/*message.end != null && */message.end != ByteString.EMPTY_BYTE_ARRAY)
            {
                com.dyuproject.protostuff.RpcValidation.verifyKey(END_KEY_ERR_MESSAGE, message.end);
                    com.dyuproject.protostuff.RpcValidation.invalidIf(message.value != null && 
                      com.dyuproject.protostuffdb.ValueUtil.compare(message.value, message.end) > 0, 
                      END_KEY_ERR_MESSAGE);
                  
            }




            if (/*message.pgstart != null && */message.pgstart != ByteString.EMPTY_BYTE_ARRAY)
            {
                com.dyuproject.protostuff.RpcValidation.verifyKey(PGSTART_KEY_ERR_MESSAGE, message.pgstart);
                    com.dyuproject.protostuff.RpcValidation.invalidIf(message.value != null && 
                      com.dyuproject.protostuffdb.ValueUtil.compare(message.value, message.pgstart) > 0, 
                      PGSTART_KEY_ERR_MESSAGE);
                  
            }




            com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    PRK_REQUIRED_ERR_MESSAGE, message.prk);




            return true;
        }
        public void mergeFrom(final Input input, final PK message) throws IOException
        {
            for (int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch (number)
                {
                    case 0:
                        return;
                    case 1:
                        message.value = input.readByteArray();
                        break;
                    case 2:
                        message.end = input.readByteArray();
                        break;
                    case 3:
                        message.pgstart = input.readByteArray();
                        break;
                    case 4:
                        message.prk = input.mergeObject(message.prk, ParamRangeKey.getSchema());
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }

        public void writeTo(final Output output, final PK message) throws IOException
        {
            if (message.value == null)
                throw new UninitializedMessageException("value", message);
            output.writeByteArray(1, message.value, false);


            if (message.end != null && message.end != ByteString.EMPTY_BYTE_ARRAY)
                output.writeByteArray(2, message.end, false);



            if (message.pgstart != null && message.pgstart != ByteString.EMPTY_BYTE_ARRAY)
                output.writeByteArray(3, message.pgstart, false);



            if (message.prk == null)
                throw new UninitializedMessageException("prk", message);
            output.writeObject(4, message.prk, ParamRangeKey.getSchema(), false);


        }

        public String getFieldName(final int number)
        {
            switch(number)
            {
                case 1: return "value";
                case 2: return "end";
                case 3: return "pgstart";
                case 4: return "prk";
                default: return null;
            }
        }

        public int getFieldNumber(final String name)
        {
            final Integer number = fieldMap.get(name);
            return number == null ? 0 : number.intValue();
        }

        final java.util.HashMap<String,Integer> fieldMap = new java.util.HashMap<String,Integer>();
        {
            fieldMap.put("value", 1);
            fieldMap.put("end", 2);
            fieldMap.put("pgstart", 3);
            fieldMap.put("prk", 4);
        }

    };

    /**
     * Useful for filtering (called by your custom pipe schema).
     */
    public static void transferField(int number, Pipe pipe, Input input, Output output, 
            Schema<PK> wrappedSchema) throws IOException
    {
        switch (number)
        {
            case 1:
                input.transferByteRangeTo(output, false, number, false);
                break;
            case 2:
                input.transferByteRangeTo(output, false, number, false);
                break;
            case 3:
                input.transferByteRangeTo(output, false, number, false);
                break;
            case 4:
                output.writeObject(number, pipe, ParamRangeKey.getPipeSchema(), false);
                break;
            default:
                input.handleUnknownField(number, wrappedSchema);
        }
    }

    static final Pipe.Schema<PK> PIPE_SCHEMA = new Pipe.Schema<PK>(SCHEMA)
    {
        protected void transfer(final Pipe pipe, final Input input, final Output output) throws IOException
        {
            for (int number = input.readFieldNumber(wrappedSchema);
                    number != 0;
                    number = input.readFieldNumber(wrappedSchema))
            {
                transferField(number, pipe, input, output, wrappedSchema);
            }
        }
    };

    public static Schema<PK> getSchema() { return SCHEMA; }
    public static Pipe.Schema<PK> getPipeSchema() { return PIPE_SCHEMA; }



}

