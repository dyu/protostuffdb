// Generated by fbsgen from p/ds/prk/p1/P1D111.proto

package com.dyuproject.protostuff.ds.prk.b1;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbP1D111
{
    private KbP1D111() {}

    public static <T> KeyBuilder byP1D1111(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            int p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .$append8(p5);
    }

    public static <T> KeyBuilder byP1D1114(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            int p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .$append(p5);
    }

    public static <T> KeyBuilder byP1D1118(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            long p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .$append(p5);
    }

    public static <T> KeyBuilder byP1D111D(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            long p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .$append6Bytes(p5);
    }

    public static <T> KeyBuilder byP1D111D(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            byte[] p5, int p5off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .$append(p5, p5off, 6);
    }

    public static <T> KeyBuilder byP1D111K(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            byte[] p5, int p5off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .$append(p5, p5off, 9);
    }

    public static <T> KeyBuilder byP1D111S(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            String p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .append(p5);
    }

    public static <T> KeyBuilder byP1D111S(int indexId, 
            int p0, long p1, int p2, int p3, int p4, 
            byte[] p5, int p5off, int p5len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append6Bytes(p1).$append8(p2).$append8(p3).$append8(p4)
                .append(p5, p5off, p5len);
    }
}