// Generated by fbsgen from p/ds/prk/p1/P11141.proto

package com.dyuproject.protostuff.ds.prk.b1;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbP11141
{
    private KbP11141() {}

    public static <T> KeyBuilder byP111411(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            int p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .$append8(p5);
    }

    public static <T> KeyBuilder byP111414(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            int p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .$append(p5);
    }

    public static <T> KeyBuilder byP111418(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            long p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .$append(p5);
    }

    public static <T> KeyBuilder byP11141D(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            long p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .$append6Bytes(p5);
    }

    public static <T> KeyBuilder byP11141D(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            byte[] p5, int p5off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .$append(p5, p5off, 6);
    }

    public static <T> KeyBuilder byP11141K(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            byte[] p5, int p5off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .$append(p5, p5off, 9);
    }

    public static <T> KeyBuilder byP11141S(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            String p5, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .append(p5);
    }

    public static <T> KeyBuilder byP11141S(int indexId, 
            int p0, int p1, int p2, int p3, int p4, 
            byte[] p5, int p5off, int p5len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append8(p0).$append8(p1).$append8(p2).$append(p3).$append8(p4)
                .append(p5, p5off, p5len);
    }
}