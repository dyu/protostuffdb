// Generated by fbsgen from p/ds/prk/pk/PK111.proto

package com.dyuproject.protostuff.ds.prk.bk;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbPK111
{
    private KbPK111() {}

    public static <T> KeyBuilder byPK1111(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            int p4, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .$append8(p4);
    }

    public static <T> KeyBuilder byPK1114(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            int p4, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .$append(p4);
    }

    public static <T> KeyBuilder byPK1118(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            long p4, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .$append(p4);
    }

    public static <T> KeyBuilder byPK111D(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            long p4, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .$append6Bytes(p4);
    }

    public static <T> KeyBuilder byPK111D(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            byte[] p4, int p4off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .$append(p4, p4off, 6);
    }

    public static <T> KeyBuilder byPK111K(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            byte[] p4, int p4off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .$append(p4, p4off, 9);
    }

    public static <T> KeyBuilder byPK111S(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            String p4, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .append(p4);
    }

    public static <T> KeyBuilder byPK111S(int indexId, 
            byte[] p0, int p0off, int p1, int p2, int p3, 
            byte[] p4, int p4off, int p4len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .$append(p0, p0off, 9).$append8(p1).$append8(p2).$append8(p3)
                .append(p4, p4off, p4len);
    }
}