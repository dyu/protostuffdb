// Generated by fbsgen from p/ds/prk/ps/PSS.proto

package com.dyuproject.protostuff.ds.prk.bs;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbPSS
{
    private KbPSS() {}

    public static <T> KeyBuilder byPSS1(int indexId, 
            String p0, String p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .$append8(p2);
    }

    public static <T> KeyBuilder byPSS4(int indexId, 
            String p0, String p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byPSS8(int indexId, 
            String p0, String p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byPSSD(int indexId, 
            String p0, String p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .$append6Bytes(p2);
    }

    public static <T> KeyBuilder byPSSD(int indexId, 
            String p0, String p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .$append(p2, p2off, 6);
    }

    public static <T> KeyBuilder byPSSK(int indexId, 
            String p0, String p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .$append(p2, p2off, 9);
    }

    public static <T> KeyBuilder byPSSS(int indexId, 
            String p0, String p1, 
            String p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .append(p2);
    }

    public static <T> KeyBuilder byPSSS(int indexId, 
            String p0, String p1, 
            byte[] p2, int p2off, int p2len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).append(p1)
                .append(p2, p2off, p2len);
    }
}