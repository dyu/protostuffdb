// Generated by fbsgen from p/ds/prk/pd/PD8.proto

package com.dyuproject.protostuff.ds.prk.pd;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.UninitializedMessageException;
/**
 * <pre>
 * message PD8K {
 *   required uint64 p0 = 1;
 *   required uint64 p1 = 2;
 *   required com.dyuproject.protostuff.ds.PK l = 3;
 * }
 * </pre>
 */
public final class PD8K implements Message<PD8K>
{
    public static final int FN_P0 = 1;
    public static final int FN_P1 = 2;
    public static final int FN_L = 3;
    static final String P0_REQUIRED_ERR_MESSAGE = "P0 is required.";

    static final String P1_REQUIRED_ERR_MESSAGE = "P1 is required.";

    static final String P1_MIN_ERR_MESSAGE = "P1 is invalid.";

    static final String L_REQUIRED_ERR_MESSAGE = "L is required.";


    /** Required. */
    public Long p0;

    /** Required. */
    public Long p1;

    /** Required. */
    public com.dyuproject.protostuff.ds.PK l;


    public PD8K() {}

    public PD8K(
        Long p0,
        Long p1,
        com.dyuproject.protostuff.ds.PK l
    )
    {
        this.p0 = p0;
        this.p1 = p1;
        this.l = l;
    }



    public Schema<PD8K> cachedSchema() { return SCHEMA; }

    static final Schema<PD8K> SCHEMA = new Schema<PD8K>()
    {
        // schema methods

        public PD8K newMessage()
        {
            return new PD8K();
        }

        public Class<PD8K> typeClass()
        {
            return PD8K.class;
        }

        public String messageName()
        {
            return PD8K.class.getSimpleName();
        }

        public String messageFullName()
        {
            return PD8K.class.getName();
        }

        public boolean isInitialized(final PD8K message)
        {
            com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    P0_REQUIRED_ERR_MESSAGE, message.p0);



            if (com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    P1_REQUIRED_ERR_MESSAGE, message.p1))
            {
                com.dyuproject.protostuff.RpcValidation.verifyMin(P1_MIN_ERR_MESSAGE, message.p1, 0);
            }



            com.dyuproject.protostuff.RpcValidation.verifyNotNull(
                    L_REQUIRED_ERR_MESSAGE, message.l);




            return true;
        }
        public void mergeFrom(final Input input, final PD8K message) throws IOException
        {
            for (int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch (number)
                {
                    case 0:
                        return;
                    case 1:
                        message.p0 = input.readFixed64();
                        break;
                    case 2:
                        message.p1 = input.readFixed64();
                        break;
                    case 3:
                        message.l = input.mergeObject(message.l, com.dyuproject.protostuff.ds.PK.getSchema());
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }

        public void writeTo(final Output output, final PD8K message) throws IOException
        {
            if (message.p0 == null)
                throw new UninitializedMessageException("p0", message);
            output.writeFixed64(1, message.p0, false);


            if (message.p1 == null)
                throw new UninitializedMessageException("p1", message);
            output.writeFixed64(2, message.p1, false);


            if (message.l == null)
                throw new UninitializedMessageException("l", message);
            output.writeObject(3, message.l, com.dyuproject.protostuff.ds.PK.getSchema(), false);


        }

        public String getFieldName(final int number)
        {
            switch(number)
            {
                case 1: return "p0";
                case 2: return "p1";
                case 3: return "l";
                default: return null;
            }
        }

        public int getFieldNumber(final String name)
        {
            final Integer number = fieldMap.get(name);
            return number == null ? 0 : number.intValue();
        }

        final java.util.HashMap<String,Integer> fieldMap = new java.util.HashMap<String,Integer>();
        {
            fieldMap.put("p0", 1);
            fieldMap.put("p1", 2);
            fieldMap.put("l", 3);
        }

    };

    /**
     * Useful for filtering (called by your custom pipe schema).
     */
    public static void transferField(int number, Pipe pipe, Input input, Output output, 
            Schema<PD8K> wrappedSchema) throws IOException
    {
        switch (number)
        {
            case 1:
                output.writeFixed64(number, input.readFixed64(), false);
                break;
            case 2:
                output.writeFixed64(number, input.readFixed64(), false);
                break;
            case 3:
                output.writeObject(number, pipe, com.dyuproject.protostuff.ds.PK.getPipeSchema(), false);
                break;
            default:
                input.handleUnknownField(number, wrappedSchema);
        }
    }

    static final Pipe.Schema<PD8K> PIPE_SCHEMA = new Pipe.Schema<PD8K>(SCHEMA)
    {
        protected void transfer(final Pipe pipe, final Input input, final Output output) throws IOException
        {
            for (int number = input.readFieldNumber(wrappedSchema);
                    number != 0;
                    number = input.readFieldNumber(wrappedSchema))
            {
                transferField(number, pipe, input, output, wrappedSchema);
            }
        }
    };

    public static Schema<PD8K> getSchema() { return SCHEMA; }
    public static Pipe.Schema<PD8K> getPipeSchema() { return PIPE_SCHEMA; }



}

