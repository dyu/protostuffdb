// Generated by fbsgen from p/ds/prk/pk/PK4.proto

package com.dyuproject.protostuff.ds.prk.vk;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;
import com.dyuproject.protostuff.ds.prk.pk.PK41;
import com.dyuproject.protostuff.ds.prk.pk.PK44;
import com.dyuproject.protostuff.ds.prk.pk.PK48;
import com.dyuproject.protostuff.ds.prk.pk.PK4D;
import com.dyuproject.protostuff.ds.prk.pk.PK4K;
import com.dyuproject.protostuff.ds.prk.pk.PK4S;

public final class VisitPK4
{
    private VisitPK4() {}

    public static <T,S,V> boolean byPK41(int indexId, 
            byte[] p0, int p0off, int p1, 
            int p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append8(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK41(int indexId, 
            PK41 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK41(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK41r(int indexId, 
            byte[] p0, int p0off, int p1, 
            int p2, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append8(p2)
                .$opushRange8(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy8(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK41r(int indexId, 
            PK41 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK41r(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK44(int indexId, 
            byte[] p0, int p0off, int p1, 
            int p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK44(int indexId, 
            PK44 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK44(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK44r(int indexId, 
            byte[] p0, int p0off, int p1, 
            int p2, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append(p2)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK44r(int indexId, 
            PK44 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK44r(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK48(int indexId, 
            byte[] p0, int p0off, int p1, 
            long p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {        
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK48(int indexId, 
            PK48 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK48(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK48r(int indexId, 
            byte[] p0, int p0off, int p1, 
            long p2, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append(p2)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK48r(int indexId, 
            PK48 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK48r(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4D(int indexId, 
            byte[] p0, int p0off, int p1, 
            long p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append6Bytes(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4D(int indexId, 
            PK4D req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4D(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4Dr(int indexId, 
            byte[] p0, int p0off, int p1, 
            long p2, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append6Bytes(p2)
                .$opushRange6Bytes(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy6Bytes(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4Dr(int indexId, 
            PK4D req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4Dr(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4K(int indexId, 
            byte[] p0, int p0off, int p1, 
            byte[] p2, int p2offset, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append(p2, p2offset, 9)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4K(int indexId, 
            PK4K req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4K(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 0, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4Kr(int indexId, 
            byte[] p0, int p0off, int p1, 
            byte[] p2, int p2offset, byte[] end, byte[] pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$append(p2, p2offset, 9)
                .$opushRange(end, 9);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, 9, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4Kr(int indexId, 
            PK4K req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4Kr(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 0, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4S(int indexId, 
            byte[] p0, int p0off, int p1, 
            String p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4S(int indexId, 
            PK4S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4S(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4Sr(int indexId, 
            byte[] p0, int p0off, int p1, 
            String p2, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$(lvs=kb.appendAndGetSize(p2))
                .$opushRange(end, lvs);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4Sr(int indexId, 
            PK4S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4Sr(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4Sp(int indexId, 
            byte[] p0, int p0off, int p1, 
            String p2, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$(lvs=kb.$appendAndGetSize(p2))
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4Sp(int indexId, 
            PK4S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4Sp(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byPK4Spr(int indexId, 
            byte[] p0, int p0off, int p1, 
            String p2, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0, p0off, 9).$append(p1)
                .$(lvs=kb.$appendAndGetSize(p2))
                .$opushRange(end, lvs, true);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, true, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byPK4Spr(int indexId, 
            PK4S req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byPK4Spr(indexId, 
                req.p0, 0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }
}