// Generated by fbsgen from p/ds/prk/ps/PS8.proto

package com.dyuproject.protostuff.ds.prk.bs;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuffdb.EntityMetadata;

public final class KbPS8
{
    private KbPS8() {}

    public static <T> KeyBuilder byPS81(int indexId, 
            String p0, long p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append8(p2);
    }

    public static <T> KeyBuilder byPS84(int indexId, 
            String p0, long p1, 
            int p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byPS88(int indexId, 
            String p0, long p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2);
    }

    public static <T> KeyBuilder byPS8D(int indexId, 
            String p0, long p1, 
            long p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append6Bytes(p2);
    }

    public static <T> KeyBuilder byPS8D(int indexId, 
            String p0, long p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2, p2off, 6);
    }

    public static <T> KeyBuilder byPS8K(int indexId, 
            String p0, long p1, 
            byte[] p2, int p2off, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .$append(p2, p2off, 9);
    }

    public static <T> KeyBuilder byPS8S(int indexId, 
            String p0, long p1, 
            String p2, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .append(p2);
    }

    public static <T> KeyBuilder byPS8S(int indexId, 
            String p0, long p1, 
            byte[] p2, int p2off, int p2len, 
            EntityMetadata<T> em, KeyBuilder kb)
    {
        return kb.begin(indexId, em)
                .append(p0).$append(p1)
                .append(p2, p2off, p2len);
    }
}