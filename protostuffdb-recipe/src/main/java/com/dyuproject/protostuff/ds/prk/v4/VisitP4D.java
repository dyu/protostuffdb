// Generated by fbsgen from p/ds/prk/p4/P4D.proto

package com.dyuproject.protostuff.ds.prk.v4;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.ParamRangeKey;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.RangeV;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;
import com.dyuproject.protostuff.ds.prk.p4.P4D1;
import com.dyuproject.protostuff.ds.prk.p4.P4D4;
import com.dyuproject.protostuff.ds.prk.p4.P4D8;
import com.dyuproject.protostuff.ds.prk.p4.P4DD;
import com.dyuproject.protostuff.ds.prk.p4.P4DK;
import com.dyuproject.protostuff.ds.prk.p4.P4DS;

public final class VisitP4D
{
    private VisitP4D() {}

    public static <T,S,V> boolean byP4D1(int indexId, 
            int p0, long p1, 
            int p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append8(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4D1(int indexId, 
            P4D1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4D1(indexId, 
                req.p0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4D1r(int indexId, 
            int p0, long p1, 
            int p2, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append8(p2)
                .$opushRange8(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy8(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4D1r(int indexId, 
            P4D1 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4D1r(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4D4(int indexId, 
            int p0, long p1, 
            int p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4D4(int indexId, 
            P4D4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4D4(indexId, 
                req.p0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4D4r(int indexId, 
            int p0, long p1, 
            int p2, int end, int pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append(p2)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4D4r(int indexId, 
            P4D4 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4D4r(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4D8(int indexId, 
            int p0, long p1, 
            long p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {        
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4D8(int indexId, 
            P4D8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4D8(indexId, 
                req.p0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4D8r(int indexId, 
            int p0, long p1, 
            long p2, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append(p2)
                .$opushRange(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4D8r(int indexId, 
            P4D8 req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4D8r(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DD(int indexId, 
            int p0, long p1, 
            long p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append6Bytes(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DD(int indexId, 
            P4DD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DD(indexId, 
                req.p0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DDr(int indexId, 
            int p0, long p1, 
            long p2, long end, long pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append6Bytes(p2)
                .$opushRange6Bytes(end);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy6Bytes(-1, pgstart, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DDr(int indexId, 
            P4DD req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DDr(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DK(int indexId, 
            int p0, long p1, 
            byte[] p2, int p2offset, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append(p2, p2offset, 9)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DK(int indexId, 
            P4DK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DK(indexId, 
                req.p0, req.p1, 
                req.l.value, 0, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DKr(int indexId, 
            int p0, long p1, 
            byte[] p2, int p2offset, byte[] end, byte[] pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$append(p2, p2offset, 9)
                .$opushRange(end, 9);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, 9, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DKr(int indexId, 
            P4DK req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DKr(indexId, 
                req.p0, req.p1, 
                req.l.value, 0, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DS(int indexId, 
            int p0, long p1, 
            String p2, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .append(p2)
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.copy(-1, prk.startKey), 
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DS(int indexId, 
            P4DS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DS(indexId, 
                req.p0, req.p1, 
                req.l.value, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DSr(int indexId, 
            int p0, long p1, 
            String p2, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$(lvs=kb.appendAndGetSize(p2))
                .$opushRange(end, lvs);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DSr(int indexId, 
            P4DS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DSr(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DSp(int indexId, 
            int p0, long p1, 
            String p2, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$(lvs=kb.$appendAndGetSize(p2))
                .$pushRange();

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DSp(int indexId, 
            P4DS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DSp(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }

    /* ================================================== */

    public static <T,S,V> boolean byP4DSpr(int indexId, 
            int p0, long p1, 
            String p2, String end, String pgstart, 
            EntityMetadata<T> em, int fieldNumber, 
            ParamRangeKey prk, RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        final KeyBuilder kb = context.kb();
        int lvs;

        kb.begin(indexId, em)
                .$appendIfNotNull(prk.parentKey)
                .$append(p0).$append6Bytes(p1)
                .$(lvs=kb.$appendAndGetSize(p2))
                .$opushRange(end, lvs, true);

        rangeV.visitRange(em, fieldNumber, store, context, 
                false, prk.limit, prk.desc, 
                prk.startKey == null ? null : kb.ocopy(-1, pgstart, lvs, true, prk.startKey),
                visitor, res, 
                kb.buf(), kb.offset(-1), kb.len(-1), 
                kb.buf(), kb.offset(), kb.len());

        return true;
    }

    public static <T,S,V> boolean byP4DSpr(int indexId, 
            P4DS req, 
            EntityMetadata<T> em, int fieldNumber, 
            RangeV<S> rangeV, S store, WriteContext context, 
            Visitor<V> visitor, V res) 
    {
        return byP4DSpr(indexId, 
                req.p0, req.p1, 
                req.l.value, req.l.end, req.l.pgstart, 
                em, fieldNumber, req.l.prk, rangeV, store, context, visitor, res);
    }
}