# Pulling data from protostuffdb

The data pulled will all be prefixed with `0x80`.

A seq is a 64-bit number (uint64 big endian)

## seq_key
```
0x80 0xFF 0xFF type(uint8) seq(uint64)
```
The `type` must be greater than `0x80`.

## seq_request
```
seq_key trailer(0)
```

## seq_response (trailing byte = 0xFF)
- decompress the data (without the trailer) with snappy and the data will be:
  ```
  count(uint16) entry+
  
  entry:
    key_size(uint16) val_size(uint16) key(data) val(data)
  ```

## pub from server
Your connected client will receive a server push with the same payload as `seq_req` whenever a new sequence entry is added.
You then have the option to pull the data right after via `send_request`.


# Pushing data into protostuffdb

The `type` must be less than or equal to `0x80`.

```
1-99 = push data and allow the jni handlers to preprocess the data

100-128 = push data without preprocessing
```
