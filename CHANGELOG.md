## 1.0.0-SNAPSHOT

* 

## 0.20.0 (2020-02-11)

* Fix base32 decoding
* Bump protostuff version to 1.1.5

## 0.19.1 (2019-10-14)
* Fix secondary index bug on updates when there are multiple index tags 

## 0.19.0 (2018-03-13)

* Merge multiple `-Dprotostuffdb.rt_flags` if provided more than once.
* Do not fill-cache on pkv-sync checks for retries
* add keepalive timeout config (millisecond) for http rpc via `-Dprotostuffdb.keepalive_timeout_interval=5000`
* blocking wait changed to EOF instead of LF (activate via `-Dprotostuffdb.rt_flags=8`)
  - default behavior has been changed to wait indefinitely instead of reading from stdin

## 0.18.0 (2018-01-18)

* protostuffdb-sync and hprotostuffdb-sync variant for pkv_sync support only (no master-slave replication)
* add `PKVSyncUtil`
* handle pkv_sync retry requests (check if already acked)

## 0.17.0 (2017-12-29)

* both `end_key` (9) and `end_value` (9) as key if `FSCAN_VALUE_AS_KEY` is set

## 0.16.0 (2017-12-05)

* sync protocol for append-only data (pkv_sync)

## 0.15.1 (2017-10-20)

* fix overflow check placement
* added option: `-Dprotostuffdb.write_buffer_mb` (min: 4, max: 128)
* tune hyperleveldb to have 16MB write buffer size by default (was 4MB)
* pull-based replication changed to use a single ws frame by default
  `-Dprotostuffdb.rep_batch_multi_frame=true` makes it multi-frame

**3 replication master variants**
1. rmaster
   - pull-based replication only on the single writer thread
   - the slaves that have caught up to the master receive the latest updates
     via push-based replication (together with other slaves)
2. rmaster-rt
   - pull-based replication only on the reader threads
3. rmaster-mt
   - combination of both of the above (replication on all threads)

## 0.15.0 (2017-10-04)

* master-slave replication

## 0.14.0 (2017-09-01)

* Background workers via: ```-Dprotostuffdb.bgworkers=com.example.MyRunnable```
  The runnable class must have a constructor: ```(int id, RpcServer server)```
* Change field numbers of ACResult (autocomplete result)
  ```required name = 3``` (was 1)
  As a convention, use the field number '3' for entities that have a string field for display (primary text)

## 0.13.1 (2017-08-03)

* Fix local auth (header key not set)

## 0.13.0 (2017-08-03)

* Auth via openssl encrypt/decrypt or signed via hmac-sha256
* Added chrome-app and opt-nw executables for desktop deployment

## 0.12.3 (2017-06-22)

* fix segfault when request body is too large

## 0.12.2 (2017-06-22)

* websockets auto ping support (server to client)
  - ```-Dprotostuffdb.auto_ping=7000``` (7 seconds)

* serve root index.html when assets are preloaded and the first bit from rt_flags is set:
  - ```-Dprotostuffdb.rt_flags=1```

## 0.12.1 (2017-06-19)

* preload assets dir by adding a trailing slash to the arg.
  - ```-Dprotostuffdb.assets_dir=assets/```

## 0.12.0 (2017-06-19)

* pubsub support via websockets to support realtime data change feedback
  - ```-Dprotostuffdb.with_pubsub=true```
* first program arg now accepts (ip:)port
  - Specifying only port *now* binds to all (previous versions defaulted to ```127.0.0.1```)
  - To keep the old behaviour, you have to explicitly specify the ip (```127.0.0.1:5000```)
* option to serve static assets
    - ```-Dprotostuffdb.assets_dir=assets```
    - must not have a trailing slash (```/```)
    - must **only** be used when deploying on desktop/intranet

## 0.11.0 (2017-06-14)

* external auth via ```-Des.token_as_user=true``` using the ```token-b64``` header from reverse proxy

## 0.10.3 (2017-04-27)

* fix iget usage on LsmdbDatastore failing to set db ptr

## 0.10.2 (2017-04-27)

* update to latest uWebSockets (2017-04-24)
* add proper chunked body handling
* fix ListV forgetting to set em

## 0.10.1

* fix iget method unsatisfied link error
* add multi-get pipe visitor
* add autocomplete-with-id visitor

## 0.10.0

* setup byte-array-offset once (embedded mode, after loading the shared library)
* remove deprecated methods

## 0.9.10

* add iterator get with prefix flag (fast path)

## 0.9.9

* add Entity flags
* add sequence keys support

## 0.9.8

* Fix javadoc typo in RpcWorker.backup
* Add KeyLockFactory to skip locking on single-threaded runtime

## 0.9.7

* Added these options (false by default):
  ```
  -Dprotostuffdb.print_stack_trace=true
  -Dprotostuffdb.st_unauthorized=true
  -Dprotostuffdb.st_operation=true
  -Dprotostuffdb.st_validation=true
  -Dprotostuffdb.st_failure=true
  -Dprotostuffdb.st_pipe=true
  -Dprotostuffdb.st_io=true
  ```
* Better error msg on CAS failure.
* Fix RpcWorker.backup (use the worker's jni stream)

## 0.9.6

* hprotostuffdb (linux) for hot backups
* use snappy 1.1.4 for all variants
* add RpcWorker.backup method for convenient backups

## 0.9.5

* protostuffdb (windows and linux) and ssdb-server (linux)


