## Manual release process

1. Put protostuffdb.exe, protostuffdb, and all other binaries into dist/
2. Bump version in package.json
3. Execute: npm publish
