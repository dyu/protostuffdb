After every release, bump to the next version (non-snapshot) the following files:
- BUILD.gn
- cpp/Tupfile
- npm/package.json
