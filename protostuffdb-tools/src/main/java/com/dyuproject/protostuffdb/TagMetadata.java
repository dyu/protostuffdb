//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

/**
 * Tag metadata.
 * 
 * @author David Yu
 * @created Nov 21, 2012
 */
public interface TagMetadata
{
    /**
     * Returns true if the tag is managed by the user.
     * This is configured with a "@UserManaged" annotation in the .proto files.
     * For the tags not configured, it is assumed that the tag is referenced 
     * somewhere in an entity index, which in turn allows it to be managed by 
     * the datastore (tag index inserts/updates/deletes).
     */
    public boolean isUserManaged(int tag);
    
    /**
     * Returns the name of the tag.
     */
    public String getName(int tag);
}