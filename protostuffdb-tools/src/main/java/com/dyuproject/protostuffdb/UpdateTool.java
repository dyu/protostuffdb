//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.B64Code;
import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.DSUtils;
import com.dyuproject.protostuff.EnumLite;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.ProtostuffOutput;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.ToolsIOUtil;
import com.dyuproject.protostuff.StringSerializer.STRING;
import com.dyuproject.protostuffdb.SchemaUpdateInput.UpdateException;

/**
 * Tool for updating a specific kind in the datastore.
 * 
 * @author David Yu
 * @created Sep 3, 2012
 */
public final class UpdateTool<T> implements Op<Datastore>, Visitor<OpChain>
{
    
    public enum UpdateType implements EnumLite<UpdateType>
    {
        
        /**
         * Add new field or indices.
         * Remove fields or indices.
         */
        SCHEMA(1), 
        
        /**
         * Numeric field type changes (e.g fixed32 to uint32).
         * Similar to {@link #SCHEMA} but faster to finish because schema 
         * comparison is skipped.
         */
        NUMBER_EXPANSION(2), 
                
        /**
         * Manually load from a file/stream (which the user has manually edited).
         */
        LOAD_FROM_INPUT(3), 
        
        /**
         * Manually load from a file/stream (which the user has manually edited).
         */
        LOAD_FROM_INPUT_NUMERIC(4);
        
        public final int number;
        
        private UpdateType(int number)
        {
            this.number = number;
        }
        
        public int getNumber()
        {
            return number;
        }
        
        public static UpdateType valueOf(int number, UpdateType returnValueIfNoneMatch)
        {
            switch(number) 
            {
                case 1: return SCHEMA;
                case 2: return NUMBER_EXPANSION;
                case 3: return LOAD_FROM_INPUT;
                case 4: return LOAD_FROM_INPUT_NUMERIC;
                default: return returnValueIfNoneMatch;
            }
        }
    }
    
    /**
     * Execute the schema update.
     */
    public static <T> UpdateTool<T> exec(final WriteContext context, 
            final Datastore source, final Datastore target, 
            final EntityMetadata<?> oldEM, final EntityMetadata<T> newEM, 
            final TagMetadata tm, final UpdateType updateType, final boolean useOpChain)
    {
        final UpdateTool<T> op = new UpdateTool<T>(oldEM, newEM, 
                tm, updateType, target, context);
        
        final boolean success;
        if(useOpChain)
        {
            // one atomic chain
            success = target.chain(newEM, op, source, 0, context);
        }
        else
        {
            source.scan(false, -1, false, null, context, new Visitor<Void>()
            {
                public boolean visit(byte[] key, 
                        byte[] v, int voffset, int vlen, 
                        Void param, int index)
                {
                    op.visit(key, v, voffset, vlen, null, index);
                        
                    return false;
                }
                
            }, null);
            
            success = true;
        }
        
        if(success)
        {
            System.out.println(op.schema.messageName() + " update summary (" + op.indexCount +  " indices) : \n------------------------------");
            System.out.println(op.count + " entities were updated.");
            for(int i = 0, count = 0, len = op.addedCountIdx.length; i < len; i++)
            {
                count = op.addedCountIdx[i];
                if(count != 0)
                {
                    System.out.println("\n" + count + " " + op.schema.getFieldName(i) + 
                            " fields were added.");
                }
            }
            
            for(int i = 0, count = 0, len = op.removedCountIdx.length; i < len; i++)
            {
                count = op.removedCountIdx[i];
                if(count != 0)
                {
                    System.out.println("\n" + count + " " + 
                            op.oldEM.pipeSchema.wrappedSchema.getFieldName(i) + 
                            " fields were removed.");
                }
            }
        }
        
        return op;
    }
    
    static <T> void mergeFrom(DSByteArrayInput input, T message, Schema<T> schema)
    {
        try
        {
            schema.mergeFrom(input, message);
            input.checkLastTagWas(0);
        }
        catch(IOException e)
        {
            throw new UpdateException("mergeFrom", e);
        }
    }
    
    /**
     * Returns the serialized size of the entity being updated.
     */
    static <T> int collectChanges(SchemaUpdateInput schemaUpdateInput, 
            final ProtostuffOutput output, 
            byte[] data1, int offset1, int len1, EntityMetadata<?> em1, 
            byte[] data2, int offset2, int len2, EntityMetadata<T> em2)
    {
        // TODO /dev/null output
        schemaUpdateInput.input1.reset(data1, offset1, len1);
        schemaUpdateInput.input2.reset(data2, offset2, len2);
        try
        {
            em2.pipeSchema.writeTo(output, schemaUpdateInput.init(em1, em2).pipe);
            
            if(DSUtils.isBufferOverflow(output))
            {
                throw DSRuntimeExceptions.invalidArg("Entity too large: " + 
                        em2.pipeSchema.wrappedSchema.messageFullName());
            }
            
            return output.getSize();
        }
        catch (IOException e)
        {
            throw new UpdateException("updateAndIndexCollect IO", e);
        }
        finally
        {
            output.clear();
        }
    }
    
    static <T> byte[] toByteArray(T message, Schema<T> schema, 
            final ProtostuffOutput output, WriteContext context)
    {
        try
        {
            schema.writeTo(output, message);
            
            if(DSUtils.isBufferOverflow(output))
            {
                throw new UpdateException("Entity too large: " + schema.messageName() + 
                        " - " + output.getSize());
            }
            
            return output.toByteArray();
        }
        catch (IOException e)
        {
            throw new UpdateException("toByteArray", e);
        }
        finally
        {
            output.clear();
        }
    }
    
    final Fixed64Input f64input = new Fixed64Input(0, 0);
    
    final EntityMetadata<?> oldEM;
    
    public final EntityMetadata<T> newEM;
    
    final TagMetadata tm;
    
    final Datastore store;
    
    final WriteContext context;
    
    final UpdateType updateType;
    
    final Schema<T> schema;
    
    public int count;
    
    public int indexCount;
    
    final int[] addedCountIdx = new int[128], removedCountIdx = new int[128];
    
    private boolean loadFinished;
    
    public UpdateTool(EntityMetadata<?> oldEM, EntityMetadata<T> newEM, 
            TagMetadata tm, UpdateType updateType, 
            Datastore store, WriteContext context)
    {
        this.oldEM = oldEM;
        this.newEM = newEM;
        this.tm = tm;
        this.updateType = updateType;
        
        this.store = store;
        this.context = context;
        
        
        this.schema = newEM.pipeSchema.wrappedSchema;
    }

    public boolean handle(OpChain chain, byte[] key, byte[] value, int offset, int len,
            Datastore source)
    {
        source.scan(false, -1, false, null, chain.context, this, chain);
        
        return true;
    }

    public boolean visit(byte[] key, 
            byte[] v, int voffset, int vlen, 
            OpChain chain, int index)
    {
        // TODO avoid copy
        final byte[] value = ValueUtil.copy(v, voffset, vlen);
        
        if(0 == key[0])
        {
            // the first entry
            final int tag = 0xFF & key[1];
            try
            {
                if(tag > 223)
                {
                    // managed entity index
                    onVisitET(chain, key, value, tag);
                }
                else if(!tm.isUserManaged(tag))
                {
                    // managed tag index
                    onVisitMT(chain, key, value, tag);
                }
                else
                {
                    // user-mangaged tag index
                    onVisitUMT(chain, key, value, tag);
                }
            }
            catch (IOException e)
            {
                throw new UpdateException("Failed on write", e);
            }
            return false;
        }
        
        if(0xFF == (key[0]&0xFF) && 0xFF == (key[1]&0xFF))
        {
            // the built-in key (the last entry)
            try
            {
                onVisitFooter(chain, key, value);
            }
            catch (IOException e)
            {
                throw new UpdateException("Failed on write", e);
            }
            return false;
        }
        
        if(0x80 == (key[0]&0xFF))
        {
            // the prefixed keys
            try
            {
                onVisitPKV(chain, key, value);
            }
            catch (IOException e)
            {
                throw new UpdateException("Failed on write", e);
            }
            return false;
        }
        
        if(key.length == 9)
        {
            // entity (default)
            try
            {
                onVisitEntity(chain, key, value);
            }
            catch (IOException e)
            {
                throw new UpdateException("Failed on write", e);
            }
            return false;
        }
        
        if(key.length != 18)
            throw new RuntimeException("Should not happen.");
        
        // entity (linked)
        try
        {
            onVisitLinkedEntity(chain, key, value);
        }
        catch (IOException e)
        {
            throw new UpdateException("Failed on write", e);
        }
        
        return false;
    }

    private void onVisitPKV(OpChain chain, byte[] key, byte[] value) throws IOException
    {
        if(chain == null)
            store.rawPut(key, value, context);
        else
            chain.rawPut(key, value);
    }

    private void onVisitFooter(OpChain chain, byte[] key, byte[] value) throws IOException
    {
        if(chain == null)
            store.rawPut(key, value, context);
        else
            chain.rawPut(key, value);
    }

    private void onVisitUMT(OpChain chain, byte[] key, byte[] value, int tag) throws IOException
    {
        if(chain == null)
            store.rawPut(key, value, context);
        else
            chain.rawPut(key, value);
        
        if(newEM.kind == DSOpUtil.getKind(key))
        {
            System.err.println("Potential UMT: " + STRING.deser(B64Code.encode(key)) + 
                    " -> " + STRING.deser(B64Code.encode(value)));
        }
    }

    private void onVisitMT(OpChain chain, byte[] key, byte[] value, int tag) throws IOException
    {
        // this assumes that the tag index 
        if(newEM.kind != DSOpUtil.getKind(key))
        {
            // doesn't belong to the kind we are interested in
            if(chain == null)
                store.rawPut(key, value, context);
            else
                chain.rawPut(key, value);
        }
        
        // ignore it since it will be re-indexed on insert
    }

    private void onVisitET(OpChain chain, byte[] key, byte[] value, int tag) throws IOException
    {
        if(newEM.kind != (key[2]&0x7F))
        {
            // doesn't belong to the kind we are interested in
            if(chain == null)
                store.rawPut(key, value, context);
            else
                chain.rawPut(key, value);
        }
        
        // ignore it since it will be re-indexed on insert
    }
    
    private void onVisitEntity(OpChain chain, byte[] key, byte[] v1) 
            throws IOException
    {
        if(newEM.kind != key[0])
        {
            // we're not interested in kinds that don't match
            if(chain == null)
                store.rawPut(key, v1, context);
            else
                chain.rawPut(key, v1);
            return;
        }
        
        if (newEM.parent != null)
        {
            // this entry's value is the parent key
            switch (updateType)
            {
                case SCHEMA:
                case NUMBER_EXPANSION:
                    // persist the parent key for the link
                    if(chain == null)
                        store.rawPut(key, v1, context);
                    else
                        chain.rawPut(key, v1);
                    return;
                default:
                    break;
            }
            
            // ignore parent key link if loading from file
            return;
        }
        
        switch (updateType)
        {
            case SCHEMA:
            case NUMBER_EXPANSION:
                compare(chain, key, v1);
                return;
            default:
                break;
        }
        
        if (loadFinished)
            return;
        
        boolean numeric = updateType == UpdateType.LOAD_FROM_INPUT_NUMERIC;
        
        count = LoadTool.mergeEntityTo(chain, store, context, null, 
                ToolsIOUtil.newJsonInput(System.in, numeric, LinkedBuffer.allocate(8192)), 
                newEM, OpChainDelegate.BUILTIN.DEFAULT, 
                LoadTool.SKIP_ENTITY_VALIDATION,
                f64input);
        
        loadFinished = true;
    }
    
    private void onVisitLinkedEntity(OpChain chain, byte[] key, byte[] v1) 
            throws IOException
    {
        if(newEM.kind != key[9])
        {
            // we're not interested in kinds that don't match
            if(chain == null)
                store.rawPut(key, v1, context);
            else
                chain.rawPut(key, v1);
            return;
        }
        
        switch (updateType)
        {
            case SCHEMA:
            case NUMBER_EXPANSION:
                compare(chain, key, v1);
                return;
            default:
                break;
        }
        
        if (loadFinished)
            return;
        
        boolean numeric = updateType == UpdateType.LOAD_FROM_INPUT_NUMERIC;
        
        count = LoadTool.mergeLinkedEntityTo(chain, store, context, null, 
                ToolsIOUtil.newJsonInput(System.in, numeric, LinkedBuffer.allocate(8192)), 
                newEM, OpChainDelegate.BUILTIN.DEFAULT, 
                LoadTool.SKIP_ENTITY_VALIDATION,
                f64input);
        
        loadFinished = true;
    }
    
    private void compare(final OpChain chain, final byte[] key, final byte[] v1) 
            throws IOException
    {
        final EntityMetadata<T> em = newEM;
        final SchemaUpdateInput schemaUpdateInput = context.schemaUpdateInput;
        
        final T message = context.parseFrom(v1, schema);
        
        final byte[] entityBuffer = context.entityBuffer;
        final int entityOffset = context.entityOffset;
        final int size = context.ser(message, em);
        
        // no secondary indexing
        DSOpUtil.writeTo(chain, key, em, store, context, 
                false, 0, 
                entityBuffer, entityOffset, size);
        
        /*final int size = newEM.indexOnFields ? context.serAndIndexCollect(
                message, em) : context.ser(message, em);
        
        if(chain == null)
            store.rawPut(key, 0, key.length, entityBuffer, entityOffset, size);
        else
            chain.rawPut(key, 0, key.length, entityBuffer, entityOffset, size);
        
        if(em.indexOnFields)
        {
            int idxInsertCount = DSOpUtil.indexOnInsert(chain, 
                    key, key.length == 9 ? 0 : 9, 
                    em, store, context, 
                    entityBuffer, entityOffset, size);
            
            if(indexCount == 0)
                indexCount = idxInsertCount;
        }*/
        
        if(updateType == UpdateType.NUMBER_EXPANSION)
        {
            // on numeric field type changes (e.g fixed32 to uint32), 
            // skip everything below and simply return
            return;
        }
        
        collectChanges(schemaUpdateInput, 
                new ProtostuffOutput(LinkedBuffer.use(context.udfBuffer)), 
                v1, 0, v1.length, oldEM, 
                entityBuffer, entityOffset, size, newEM);
        
        if(schemaUpdateInput.changes == 0)
            return;
        
        count++;
        
        for(int i = 0, len = schemaUpdateInput.addCount; i < len; i++)
        {
            addedCountIdx[schemaUpdateInput.fieldVerifyAddIdx[i]]++;
        }
        
        for(int i = 0, len = schemaUpdateInput.removeCount; i < len; i++)
        {
            removedCountIdx[schemaUpdateInput.fieldVerifyRemoveIdx[i]]++;
        }
    }

    /*public boolean visit2(byte[] key, byte[] v1, OpChain chain, int index)
    {
        if(0 == key[0])
        {
            if(224 > (0xFF&key[1]) || newEM.kind != (key[2]&0x7F))
            {
                // possibilities:
                // 1. tag index 
                // 2. indices that don't belong to the entity that we're updating
                chain.rawPut(key, 0, key.length, v1, 0, v1.length);
            }
            
            // exclude the target entity's indices because we're doing a full index rebuild
            return false;
        }
        
        if(0x80 == (key[0]&0xFF))
        {
            // the prefixed keys
            chain.rawPut(key, 0, key.length, v1, 0, v1.length);
            return false;
        }
        
        if(0xFF == (key[0]&0xFF) && 0xFF == (key[1]&0xFF))
        {
            // the built-in key
            chain.rawPut(key, 0, key.length, v1, 0, v1.length);
            return false;
        }
        
        if(key.length == 9)
        {
            if(newEM.parent != null || newEM.kind != key[0])
            {
                // we're not interested in linked entities, nor kinds that don't match
                chain.rawPut(key, 0, key.length, v1, 0, v1.length);
                return false;
            }
        }
        else if(newEM.parent == null || newEM.kind != key[9])
        {
            // we're not interested in default entities, nor kinds that don't match
            chain.rawPut(key, 0, key.length, v1, 0, v1.length);
            return false;
        }
        
        final EntityMetadata<T> em = newEM;
        final WriteContext context = chain.context;
        final SchemaUpdateInput schemaUpdateInput = context.schemaUpdateInput;
        
        final T message = schema.newMessage();
        
        // on numeric field type changes (e.g fixed32 to uint32), 
        // uncomment the line below and comment out the line after it
        //ProtostuffIOUtil.mergeFrom(v1, message, schema);
        mergeFrom(schemaUpdateInput.input1.reset(v1, 0, v1.length), message, schema);
        
        final byte[] entityBuffer = context.entityBuffer;
        final int entityOffset = context.entityOffset;
        
        final int size = newEM.indexOnFields ? context.serAndIndexCollect(
                message, em) : context.ser(message, em);
        
        chain.rawPut(key, 0, key.length, entityBuffer, entityOffset, size);
        
        if(em.indexOnFields)
        {
            int idxInsertCount = DSOpUtil.indexOnInsert(chain, 
                    key, key.length == 9 ? 0 : 9, 
                    em, context, 
                    entityBuffer, entityOffset, size);
            
            if(indexCount == 0)
                indexCount = idxInsertCount;
        }
        
        // on numeric field type changes (e.g fixed32 to uint32), skip everything below
        // and simply return
        
        collectChanges(schemaUpdateInput, 
                new ProtostuffOutput(LinkedBuffer.use(context.udfBuffer)), 
                v1, 0, v1.length, oldEM, 
                entityBuffer, entityOffset, size, newEM);
        
        if(schemaUpdateInput.changes == 0)
            return false;
        
        count++;
        
        for(int i = 0, len = schemaUpdateInput.addCount; i < len; i++)
        {
            addedCountIdx[schemaUpdateInput.fieldVerifyAddIdx[i]]++;
        }
        
        for(int i = 0, len = schemaUpdateInput.removeCount; i < len; i++)
        {
            removedCountIdx[schemaUpdateInput.fieldVerifyRemoveIdx[i]]++;
        }
        
        //if(schemaUpdateInput.changes == 0)
        //{
        //    if(!checkIndexUpdate(chain, key, v1, v2, context))
        //        throw new UpdateException("No changes were made on the schema.");
        //    
        //    // write the index
        //    return false;
        //}
        
        // overwrite
        //checkFieldRemove(chain, key, v1, v2, context);
        
        //checkFieldAdd(chain, key, v1, v2, context);
        
        return false;
    }*/
    
    /*private boolean checkIndexUpdate(final OpChain chain, 
            byte[] key, byte[] v1, byte[] v2, 
            final WriteContext context)
    {
        if(!newEM.indexOnFields)
        {
            if(oldEM.indexOnFields)
            {
                // removed the indices
                // TODO
                
                return true;
            }
            
            return false;
        }
        
        if(!oldEM.indexOnFields)
        {
            // first time to add indices
            // TODO
            
            return true;
        }
        
        // verify if there are changed indices
        // get all the fields
        final int[] fields = context.schemaUpdateInput.fields2;
        int changes = 0;
        for(int i = 0, len = context.schemaUpdateInput.fields2Count; i < len; i++)
        {
            int[] indices1 = oldEM.index.getIndices(fields[i]);
            int[] indices2 = newEM.index.getIndices(fields[i]);
            if(indices1.length != indices2.length)
            {
                // the field has new dependent indices
                // TODO
                changes++;
            }
            
            changes += compareIndices(indices1, indices2);
        }
        
        return changes != 0;
    }
    
    private int compareIndices(int[] indices1, int[] indices2)
    {
        int changes = 0;
        for(int i = 0, len = indices2.length, index1 = 0, 
                offset1 = 0, size1 = 0, index2 = 0, offset2 = 0, size2 = 0;
                i < len; i++)
        {
            index1 = indices1[i];
            index2 = 0;
            
            for(int id : indices2)
            {
                if(id == 0)
                    break;
                
                if(id == index1)
                {
                    index2 = id;
                    break;
                }
            }
            
            // no match
            if(index2 == 0)
            {
                // remove this index.
                // TODO
                
                continue;
            }
            
            offset1 = oldEM.index.offsetEntries[index1];
            offset2 = newEM.index.offsetEntries[index2];
            
            size1 = oldEM.index.delimitedEntries[offset1++];
            size2 = newEM.index.delimitedEntries[offset2++];
            
            if(size1 == size2)
                continue;
            
            for(int j = 0; j < size2; j++)
            {
                if(oldEM.index.offsetEntries[offset1++] != newEM.index.offsetEntries[offset2++])
                {
                    // index changed
                    // TODO
                    changes++;
                    
                }
            }
        }
        
        // TODO
        
        return changes;
    }
    
    private boolean checkFieldRemove(final OpChain chain, 
            byte[] key, byte[] v1, byte[] v2, 
            final WriteContext context)
    {
        return false;
    }
    
    private boolean checkFieldAdd(final OpChain chain, 
            byte[] key, byte[] v1, byte[] v2, 
            final WriteContext context)
    {
        return false;
    }*/
    
}
