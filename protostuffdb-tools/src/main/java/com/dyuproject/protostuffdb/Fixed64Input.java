//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.EnumMapping;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Schema;

/**
 * Input that returns a single fixed64 field.
 * 
 * @author David Yu
 * @created Jun 30, 2015
 */
public final class Fixed64Input implements Input
{
    
    public int field;
    public long value;
    
    public Fixed64Input(int field, long value)
    {
        this.field = field;
        this.value = value;
    }

    public <T> void handleUnknownField(int fieldNumber, Schema<T> schema) throws IOException
    {
        
    }

    public <T> int readFieldNumber(Schema<T> schema) throws IOException
    {
        return field;
    }

    public int readInt32() throws IOException
    {
        return 0;
    }

    public int readUInt32() throws IOException
    {
        return 0;
    }

    public int readSInt32() throws IOException
    {
        return 0;
    }

    public int readFixed32() throws IOException
    {
        return 0;
    }

    public int readSFixed32() throws IOException
    {
        return 0;
    }

    public long readInt64() throws IOException
    {
        return 0;
    }

    public long readUInt64() throws IOException
    {
        return 0;
    }

    public long readSInt64() throws IOException
    {
        return 0;
    }

    public long readFixed64() throws IOException
    {
        field = 0;
        return value;
    }

    public long readSFixed64() throws IOException
    {
        return 0;
    }

    public float readFloat() throws IOException
    {
        return 0;
    }

    public double readDouble() throws IOException
    {
        return 0;
    }

    public boolean readBool() throws IOException
    {
        return false;
    }

    public int readEnum() throws IOException
    {
        return 0;
    }
    
    public int readEnumIdx(EnumMapping mapping) throws IOException
    {
        return 0;
    }

    public String readString() throws IOException
    {
        return null;
    }

    public ByteString readBytes() throws IOException
    {
        return null;
    }

    public byte[] readByteArray() throws IOException
    {
        return null;
    }

    public <T> T mergeObject(T value, Schema<T> schema) throws IOException
    {
        return null;
    }

    public void transferByteRangeTo(Output output, boolean utf8String, int fieldNumber,
            boolean repeated) throws IOException
    {
        
    }
    
    public void transferEnumTo(Output output, EnumMapping mapping, int fieldNumber,
            boolean repeated) throws IOException
    {
        
    }
}
