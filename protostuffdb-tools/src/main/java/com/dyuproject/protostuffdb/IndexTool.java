//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.DumpLoadUtil.printStats;
import protostuffdb.Jni;

import com.dyuproject.protostuff.B64Code;

/**
 * A tool to write secondary, tag, and link indices from entities.
 * 
 * @author David Yu
 * @created Dec 16, 2013
 */
public final class IndexTool implements Op<Datastore>
{
    
    public static class IndexException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public IndexException(){}
        public IndexException(String msg){super(msg);}
        public IndexException(String msg, Exception cause){super(msg, cause);}
    }
    
    public static IndexTool exec(boolean useOpChain, boolean deserializeEntities, 
            EntityMetadataResource registry, TagMetadata tm, 
            WriteContext context, Datastore source, Datastore target)             
    {
        final IndexTool tool = new IndexTool(deserializeEntities, registry, tm, context);
        
        final boolean success;
        if(useOpChain)
        {
            success = target.chain(null, tool, source, 0, tool.context);
        }
        else
        {
            tool.count = source.scan(false, -1, false, null, context, tool.VISITOR, target);
            
            success = true;
        }
        
        if(success)
        {
            printStats(tool.count, tool.kvCount, 
                    tool.tagCount, tool.entityCount, tool.linkedEntityCount, 
                    registry, tm);
            
            System.out.println(tool.idxWriteCount + " index entries were written.");
            //System.out.println(tool.count + " entries were visited.");
        }
        
        return tool;
    }
    
    final Visitor<OpChain> CHAIN_VISITOR = new Visitor<OpChain>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                OpChain chain, int index)
        {
            return IndexTool.this.visit(key, 
                    v, voffset, vlen, 
                    chain, null, index);
        }
    };
    
    final Visitor<Datastore> VISITOR = new Visitor<Datastore>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                Datastore store, int index)
        {
            return IndexTool.this.visit(key, 
                    v, voffset, vlen, 
                    null, store, index);
        }
    };
    
    public int count;
    public int idxWriteCount;
    
    final boolean deserializeEntity;
    final EntityMetadataResource registry;
    final TagMetadata tm;
    
    final WriteContext sourceContext;
    // for writing, bufTmp is chosen because it is not used during indexing.
    final WriteContext context = new WriteContext(Jni.bufTmp(0), Jni.buf(0), 
            0, Jni.PARTITION_SIZE);
    
    int kvCount = 0;
    final int[] tagCount = new int[256], 
            entityCount = new int[128], 
            linkedEntityCount = new int[128];
    
    IndexTool(boolean deserializeEntities, 
            EntityMetadataResource registry, TagMetadata tm, 
            WriteContext sourceContext)
    {
        this.deserializeEntity = deserializeEntities;
        this.registry = registry;
        this.tm = tm;
        
        this.sourceContext = sourceContext;
    }
    
    public boolean handle(OpChain chain, byte[] key, 
            byte[] value, int offset, int len,
            Datastore source)
    {
        count = source.scan(false, -1, false, null, sourceContext, CHAIN_VISITOR, chain);
        
        return true;
    }
    
    boolean visit(byte[] key, 
            byte[] v, int voffset, int vlen, 
            OpChain chain, Datastore store, int index) 
    {
        final int first = key[0] & 0xFF;
        if (0 == first)
        {
            // the first entry
            int tag = 0xFF & key[1];
            if(tag > 223 || !tm.isUserManaged(tag))
            {
                // 2 scenarios, both managed by datastore:
                // 1. entity index
                // 2. tag index
                return false;
            }
            
            // user-mangaged tag index
            onVisitUMT(key, v, voffset, vlen, tag, chain, store);
            return false;
        }
        
        if (0x80 == first)
        {
            // the prefixed keys
            onVisitPKV(key, v, voffset, vlen, chain, store);
            return false;
        }
        
        switch(key.length)
        {
            case 9:
                if (0xFF == first)
                {
                    // rep seq key
                    onVisitPKV(key, v, voffset, vlen, chain, store);
                }
                else
                {
                    // entity (default)
                    onVisitEntity(key, v, voffset, vlen, chain, store);
                }
                return false;
                
            case 10:
                if (0 == key[9])
                {
                    // its a primary link index (managed)
                    context.data((0x7f & key[0]), ValueUtil.copy(v, voffset, vlen));
                }
                else if (LsmdbDatastore.isEndKey(key))
                {
                    // the built-in key (the last entry)
                    onVisitFooter(key, v, voffset, vlen, chain, store);
                }
                
                return false;
                
            case 18:
                // entity (linked)
                onVisitLinkedEntity(key, v, voffset, vlen, chain, store);
                return false;
                
            default:
                throw new RuntimeException("Invalid key: " + 
                        new String(B64Code.encode(key)));
        }
    }

    private void onVisitFooter(byte[] key, 
            byte[] v, int voffset, int vlen, 
            OpChain chain, Datastore store)
    {
        if(chain == null)
            store.rawPut(key, 0, key.length, v, voffset, vlen, context);
        else
            chain.rawPut(key, 0, key.length, v, voffset, vlen);
    }
    
    private void onVisitPKV(byte[] key, 
            byte[] v, int voffset, int vlen, 
            OpChain chain, Datastore store)
    {
        if(chain == null)
            store.rawPut(key, 0, key.length, v, voffset, vlen, context);
        else
            chain.rawPut(key, 0, key.length, v, voffset, vlen);
        
        //kvCount++;
    }
    
    private void onVisitUMT(byte[] key, 
            byte[] v, int voffset, int vlen, 
            int tag, 
            OpChain chain, Datastore store)
    {
        if(chain == null)
            store.rawPut(key, 0, key.length, v, voffset, vlen, context);
        else
            chain.rawPut(key, 0, key.length, v, voffset, vlen);
        
        //tagCount[tag]++;
    }
    
    private <T> void onVisitEntity(byte[] key, 
            byte[] v, int voffset, int vlen, 
            OpChain chain, Datastore store)
    {
        final int kind = KeyUtil.getKind(key);
        @SuppressWarnings("unchecked")
        final EntityMetadata<T> em = 
                (EntityMetadata<T>)registry.getEntityMetadata(kind);
        
        if(em.parent != null && key.length == 9)
        {
            // link to parent key (handled by onVisitLinkedEntity)
            return;
        }
        
        final int idxWriteCount;
        if(deserializeEntity)
        {
            T entity = context.parseFrom(v, voffset, vlen, em);
            
            final boolean index;
            final int size, idxCount;
            if(em.indexOnFields)
            {
                size = context.serAndIndexCollect(entity, em);
                idxCount = context.serAndIndexCollectOutput.count;
                index = true;
            }
            else
            {
                size = context.ser(entity, em);
                idxCount = 0;
                index = false;
            }
            
            // copy because the entity buffer will get overritten on the first 
            // rawPut from DSOpUtil
            byte[] copy = ValueUtil.copy(context.entityBuffer, context.entityOffset, size);
            
            idxWriteCount = DSOpUtil.writeTo(chain, key, em, store, context, 
                    index, idxCount, 
                    copy, 0, copy.length);
        }
        else
        {
            final boolean index;
            final int idxCount;
            if(em.indexOnFields)
            {
                context.indexCollect(v, voffset, vlen, em);
                idxCount = context.indexCollectOutput.count;
                index = true;
            }
            else
            {
                idxCount = 0;
                index = false;
            }
            
            idxWriteCount = DSOpUtil.writeTo(chain, key, em, store, context, 
                    index, idxCount, 
                    v, voffset, vlen);
        }
        
        if(em.parent == null)
            entityCount[kind] += idxWriteCount;
        else
            linkedEntityCount[kind] += idxWriteCount;
        
        this.idxWriteCount += idxWriteCount;
    }
    
    private void onVisitLinkedEntity(byte[] key, 
            byte[] v, int voffset, int vlen, 
            OpChain chain, Datastore store)
    {
        final byte[] parentKey = new byte[9];
        System.arraycopy(key, 1, parentKey, 1, 8);
        parentKey[0] = (byte)(0x7f & key[0]);
        
        // link to parent key
        if(chain == null)
            store.rawPut(key, 9, 9, parentKey, 0, parentKey.length, context);
        else
            chain.rawPut(key, 9, 9, parentKey, 0, parentKey.length);
        
        onVisitEntity(key, 
                v, voffset, vlen, 
                chain, store);
    }

}