//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.DumpLoadUtil.newOutputStream;
import static com.dyuproject.protostuffdb.DumpLoadUtil.printStats;
import static com.dyuproject.protostuffdb.DumpLoadUtil.toStr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.dyuproject.protostuff.B64Code;
import com.dyuproject.protostuff.JsonFlags;
import com.dyuproject.protostuff.JsonXIOUtil;
import com.dyuproject.protostuff.JsonXOutput;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.ProtostuffIOUtil;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.ToolsIOUtil;
import com.dyuproject.protostuff.YamlIOUtil;

/**
 * A tool to dump the contents of the datastore to a set of files.
 * The type of data will be the name of the folder containg these files.
 * 
 * @author David Yu
 * @created Nov 12, 2012
 */
public final class DumpTool implements Visitor<Void>
{
    
    public static class DumpException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public DumpException(){}
        public DumpException(String msg){super(msg);}
        public DumpException(String msg, Exception cause){super(msg, cause);}
    }
    
    static final boolean DUMP_WITH_SEQ_KEY = Boolean.getBoolean("dump.with_seq_key");
    
    static final byte[] CRLF = new byte[]{'\r', '\n'}, 
            ARRAY_START_QUOTE = new byte[]{'[', '"'},
            QUOTE_ARRAY_END_CRLF = new byte[]{'"', ']', '\r', '\n'};
    
    static final int UMT = 1, ENTITY = 2, PKV = 3, LINKED = 4, RSKV = 5;
    
    /**
     * Exec this tool.
     */
    public static <T> void exec(WriteContext context, Datastore store, 
            File dumpDir, EntityMetadataResource registry, TagMetadata tm, 
            boolean numeric)
    {
        exec(context, store, dumpDir, registry, tm, numeric, false);
    }
    
    /**
     * Exec this tool.
     */
    public static <T> void exec(WriteContext context, Datastore store, 
            File dumpDir, EntityMetadataResource registry, TagMetadata tm, 
            boolean numeric, boolean deserializeEntity)
    {
        File umtDir = new File(dumpDir, "umt");
        if(!umtDir.exists())
            umtDir.mkdir();
        
        File entityDir = new File(dumpDir, "entity");
        if(!entityDir.exists())
            entityDir.mkdir();
        
        File pkvDir = new File(dumpDir, "pkv");
        if(!pkvDir.exists())
            pkvDir.mkdir();
        
        File linkedEntityDir = new File(dumpDir, "linked");
        if(!linkedEntityDir.exists())
            linkedEntityDir.mkdir();
        
        final DumpTool op = new DumpTool(registry, tm, numeric, deserializeEntity, 
                dumpDir, umtDir, entityDir, pkvDir, linkedEntityDir);

        final int count = store.scan(false, -1, false, null, context, op, null);

        final Stats stats = printStats(count, op.pkvCount, 
                op.tagCount, op.entityCount, op.linkedEntityCount, 
                registry, tm);
        try
        {
            YamlIOUtil.writeTo(newOutputStream(dumpDir, "stats.yaml"), 
                    stats, Stats.getSchema(), op.lb.clear());
        }
        catch (IOException e)
        {
            throw new DumpException("Failed to write yaml", e);
        }
        
        if (op.rskvCount != 0)
            System.out.println("rskv: " + op.rskvCount);
    }
    
    public static JsonXOutput newOutput(File dir, String fileName, 
            boolean numeric, LinkedBuffer lb) throws IOException
    {
        return new JsonXOutput(lb, newOutputStream(dir, fileName), JsonFlags.NUMERIC, null);
    }
    
    static void writeTo(OutputStream out, LinkedBuffer lb, boolean numeric, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        JsonXIOUtil.writeTo(out, new KV(key, ValueUtil.copy(v, voffset, vlen)), 
                KV.getSchema(), numeric, lb);
    }
    
    static void writeTagTo(JsonXOutput output, boolean numeric, int tag, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        ToolsIOUtil.writeTo(output, new KV(key, ValueUtil.copy(v, voffset, vlen)), 
                KV.getSchema());
        
        output.sink.writeByteArray(CRLF, output, output.head);
    }
    
    static void writePKVTo(JsonXOutput output, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        ToolsIOUtil.writeTo(output, new KV(key, ValueUtil.copy(v, voffset, vlen)), 
                KV.getSchema());
        
        output.sink.writeByteArray(CRLF, output, output.head);
    }
    
    static void fillSequenceKey(int kind, long seqId, final byte[] key, int offset)
    {
        key[offset++] = (byte)kind;
        ValueUtil.writeInt64((seqId << 2) | 1, key, offset);
    }
    
    public static <T> void writeEntityTo(JsonXOutput output, 
            int count, Fixed64Input f64Input, 
            EntityMetadata<T> em, 
            byte[] key, 
            byte[] v, int voffset, int vlen, 
            boolean deserializeEntity) throws IOException
    {
        if (DUMP_WITH_SEQ_KEY)
        {
            f64Input.field = 2;
            f64Input.value = KeyUtil.extractTsFrom(key);
            fillSequenceKey(em.kind, count, key, 0);
        }
        
        output.sink.writeByteArray(ARRAY_START_QUOTE, output, output.head);
        output.sink.writeByteArrayB64(key, output, output.head);
        output.sink.writeByteArray(QUOTE_ARRAY_END_CRLF, output, output.head);
        
        if (deserializeEntity || DUMP_WITH_SEQ_KEY)
        {
            Schema<T> schema = em.pipeSchema.wrappedSchema;
            T message = schema.newMessage();
            if (DUMP_WITH_SEQ_KEY)
                schema.mergeFrom(f64Input, message);
            ProtostuffIOUtil.mergeFrom(v,  voffset, vlen, message, schema);
            ToolsIOUtil.writeTo(output, message, schema);
        }
        else
        {
            ToolsIOUtil.writeTo(output, ProtostuffIOUtil.newPipe(v, voffset, vlen), 
                    em.pipeSchema);
        }
        
        output.sink.writeByteArray(CRLF, output, output.head);
    }
    
    static <T> void writeLinkedEntityTo(JsonXOutput output, 
            int count, Fixed64Input f64Input, 
            EntityMetadata<T> em, 
            byte[] key, 
            byte[] v, int voffset, int vlen, 
            boolean deserializeEntity) throws IOException
    {
        if (DUMP_WITH_SEQ_KEY)
        {
            f64Input.field = 2;
            f64Input.value = KeyUtil.extractTsFrom(key);
            fillSequenceKey(em.kind, count, key, 0);
        }
        
        output.sink.writeByteArray(ARRAY_START_QUOTE, output, output.head);
        output.sink.writeByteArrayB64(key, output, output.head);
        output.sink.writeByteArray(QUOTE_ARRAY_END_CRLF, output, output.head);
        
        if (deserializeEntity || DUMP_WITH_SEQ_KEY)
        {
            Schema<T> schema = em.pipeSchema.wrappedSchema;
            T message = schema.newMessage();
            if (DUMP_WITH_SEQ_KEY)
                schema.mergeFrom(f64Input, message);
            ProtostuffIOUtil.mergeFrom(v,  voffset, vlen, message, schema);
            ToolsIOUtil.writeTo(output, message, schema);
        }
        else
        {
            ToolsIOUtil.writeTo(output, ProtostuffIOUtil.newPipe(v, voffset, vlen), 
                    em.pipeSchema);
        }
        
        output.sink.writeByteArray(CRLF, output, output.head);
    }
    
    final Fixed64Input f64Input = new Fixed64Input(2, 0);
    final EntityMetadataResource registry;
    final TagMetadata tm;
    final boolean numeric, deserializeEntity;
    
    final File baseDir, umtDir, entityDir, pkvDir, linkedEntityDir;
    
    int pkvCount = 0, rskvCount = 0;
    final int[] tagCount = new int[256], 
            entityCount = new int[128], 
            linkedEntityCount = new int[128];
    
    final LinkedBuffer lb = LinkedBuffer.allocate(8192);
    
    private int previousState;
    private int previousTagOrKind;
    
    private JsonXOutput previousOutput;
    
    DumpTool(EntityMetadataResource registry, TagMetadata tm, 
            boolean numeric, boolean deserializeEntity, 
            File baseDir, File umtDir, File entityDir, 
            File pkvDir, File linkedEntityDir)
    {
        this.registry = registry;
        this.tm = tm;
        this.numeric = numeric;
        this.deserializeEntity = deserializeEntity;
        
        this.baseDir = baseDir;
        this.umtDir = umtDir;
        this.entityDir = entityDir;
        this.pkvDir = pkvDir;
        this.linkedEntityDir = linkedEntityDir;
    }
    
    private void onVisitFooter(byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        if(previousOutput != null)
        {
            // flush and close previous output
            LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
            previousOutput.clear().out.close();
        }
        
        final FileOutputStream out = newOutputStream(baseDir, "footer.json");
        try
        {
            writeTo(out, lb.clear(), numeric, 
                    key, 
                    v, voffset, vlen);
        }
        finally
        {
            lb.clear();
            out.close();
        }
    }
    
    private void onVisitUMT(final byte[] key, 
            final byte[] v, int voffset, int vlen, 
            final int tag) throws IOException
    {
        tagCount[tag]++;
        
        if(previousState != UMT)
        {
            // initial state
            previousState = UMT;
            
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
            }
            
            previousTagOrKind = tag;
            previousOutput = newOutput(umtDir, toStr(tag) + ".json", numeric, lb);
        }
        else if(previousTagOrKind != tag)
        {
            // next umt
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
            }
            
            previousTagOrKind = tag;
            previousOutput = newOutput(umtDir, toStr(tag) + ".json", numeric, lb);
        }
        
        writeTagTo(previousOutput, numeric, tag, 
                key, 
                v, voffset, vlen);
    }
    
    private void onVisitEntity(byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        final int kind = key[0]&0x7F;
        
        if(previousState != ENTITY)
        {
            // initial state
            previousState = ENTITY;
            
            // reset for possible previous tag == kind (linked entity)
            previousTagOrKind = 0;
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
                previousOutput = null;
            }
            
            if(registry.getEntityMetadata(kind).parent != null)
            {
                // handle by onVisitLinkedEntity
                return;
            }
            
            previousTagOrKind = kind;
            previousOutput = newOutput(entityDir, toStr(kind) + ".json", numeric, lb);
        }
        else if(previousTagOrKind != kind)
        {
            // next entity
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
                previousOutput = null;
            }
            
            if(registry.getEntityMetadata(kind).parent != null)
            {
                // handle by onVisitLinkedEntity
                return;
            }
            
            previousTagOrKind = kind;
            previousOutput = newOutput(entityDir, toStr(kind) + ".json", numeric, lb);
        }
        
        writeEntityTo(previousOutput, 
                ++entityCount[kind], f64Input, 
                registry.getEntityMetadata(kind), 
                key, 
                v, voffset, vlen, 
                deserializeEntity);
    }
    
    private void onVisitPKV(byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        pkvCount++;
        
        if(previousState != PKV)
        {
            // initial state
            previousState = PKV;
            
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
            }
            
            previousOutput = newOutput(pkvDir, "all.json", numeric, lb);
        }
        
        writePKVTo(previousOutput, 
                key, 
                v, voffset, vlen);
    }
    
    private void onVisitRSKV(byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        rskvCount++;
        
        if (previousState != RSKV)
        {
            // initial state
            previousState = RSKV;
            
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
            }
            
            previousOutput = newOutput(baseDir, "rep.json", numeric, lb);
        }
        
        writePKVTo(previousOutput, 
                key, 
                v, voffset, vlen);
    }
    
    private void onVisitLinkedEntity(byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        final int parentKind = key[0]&0x7F, kind = key[9]&0x7f;
        
        if(previousState != LINKED)
        {
            // initial state
            previousState = LINKED;
            
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
            }
            
            previousTagOrKind = parentKind;
            previousOutput = newOutput(linkedEntityDir, toStr(kind) + ".json", numeric, lb);
        }
        else if(previousTagOrKind != parentKind)
        {
            // next linked entity
            if(previousOutput != null)
            {
                // flush and close previous output
                LinkedBuffer.writeTo(previousOutput.out, previousOutput.head);
                previousOutput.clear().out.close();
            }
            
            previousTagOrKind = parentKind;
            previousOutput = newOutput(linkedEntityDir, toStr(kind) + ".json", numeric, lb);
        }
        
        writeLinkedEntityTo(previousOutput, 
                ++linkedEntityCount[kind], f64Input, 
                registry.getEntityMetadata(kind), 
                key, 
                v, voffset, vlen, 
                deserializeEntity);
    }
    
    public boolean visit(byte[] key, 
            byte[] v, int voffset, int vlen, 
            Void param, int index)
    {
        final int first = key[0] & 0xFF;
        if (0 == first)
        {
            // the first entry
            int tag = 0xFF & key[1];
            if(tag > 223 || !tm.isUserManaged(tag))
            {
                // 2 scenarios, both managed by datastore:
                // 1. entity index
                // 2. tag index
                return false;
            }
            
            // user-mangaged tag index
            try
            {
                onVisitUMT(key, v, voffset, vlen, tag);
            }
            catch (IOException e)
            {
                throw new DumpException("Failed on write", e);
            }
            return false;
        }
        
        if (0x80 == first)
        {
            // the prefixed keys
            try
            {
                onVisitPKV(key, v, voffset, vlen);
            }
            catch (IOException e)
            {
                throw new DumpException("Failed on write", e);
            }
            return false;
        }
        
        switch(key.length)
        {
            case 9:
                if (0xFF == first)
                {
                    // rep seq key
                    try
                    {
                        onVisitRSKV(key, v, voffset, vlen);
                    }
                    catch (IOException e)
                    {
                        throw new DumpException("Failed on write", e);
                    }
                }
                else
                {
                    // entity (default)
                    try
                    {
                        onVisitEntity(key, v, voffset, vlen);
                    }
                    catch (IOException e)
                    {
                        throw new DumpException("Failed on write", e);
                    }
                }
                return false;
                
            case 10:
                if (0 != key[9] && LsmdbDatastore.isEndKey(key))
                {
                    // the built-in key (the last entry)
                    try
                    {
                        onVisitFooter(key, v, voffset, vlen);
                    }
                    catch (IOException e)
                    {
                        throw new DumpException("Failed on write", e);
                    }
                }
                // if not built-in key, its a link index (managed)
                return false;
                
            case 18:
                // entity (linked)
                try
                {
                    onVisitLinkedEntity(key, v, voffset, vlen);
                }
                catch (IOException e)
                {
                    throw new DumpException("Failed on write", e);
                }
                return false;
                
            default:
                throw new RuntimeException("Invalid key: " + 
                        new String(B64Code.encode(key)));
        }
    }

}
