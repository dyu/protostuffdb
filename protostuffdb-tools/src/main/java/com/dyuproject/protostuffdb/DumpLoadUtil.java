//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;



/**
 * Common utils to be used by {@link DumpTool} and {@link LoadTool}.
 * 
 * @author David Yu
 * @created Nov 21, 2012
 */
public final class DumpLoadUtil
{
    private DumpLoadUtil() {}
    
    static final char[] FILLER = new char[]{
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
    };
    
    public static String display(EntityMetadata<?> em)
    {
        if(em.parent == null)
            return em.pipeSchema.messageName();
        
        return display(em.parent) + "." + em.pipeSchema.messageName();
    }
    
    public static String toStr(int kindOrTag)
    {
        String str = Integer.toString(kindOrTag);
        switch(str.length())
        {
            case 1:
                return "00" + str;
                
            case 2:
                return "0" + str;
                
            case 3:
                return str;
                
            default:
                throw new RuntimeException("Should not happen.");
        }
    }
    
    public static int toInt(String str)
    {
        final int dot = str.lastIndexOf('.');
        return dot == -1 ? Integer.parseInt(str) : Integer.parseInt(str.substring(0, dot));
    }
    
    public static FileOutputStream newOutputStream(File dir, String fileName)
    throws IOException
    {
        return new FileOutputStream(new File(dir, fileName));
    }

    public static Stats printStats(int count, int kvCount, int[] tagCount, int[] entityCount, 
            int[] linkedEntityCount, EntityMetadataResource registry, TagMetadata tagQuery)
    {
        final Stats stats = new Stats();
        stats.visited = count;
        stats.pkv = kvCount;
        
        if(count != 0)
            System.out.println(count + " entries were visited.");
        
        System.out.println("********** stats **********\n");
        
        // pkv
        
        System.out.println("pkv: " + kvCount);
        
        // umt
        
        System.out.println("\n==================== umt:");
        int umtc = 0;
        for(int i = 0, len = tagCount.length, c = 0; i < len; i++)
        {
            if(0 == (c = tagCount[i]))
                continue;
            
            stats.addUmtEntry(new Stats.TagEntry(i, c));
            
            String str = i + ": " + c;
            System.out.print(str);
            
            String display = tagQuery.getName(i);
            int spaces = 21 - str.length();
            if(spaces > 0)
            {
                char[] copy = new char[spaces];
                System.arraycopy(FILLER, 0, copy, 0, copy.length);
                System.out.print(copy);
            }
            else
            {
                // single space
                System.out.print(' ');
            }
            
            System.out.println(display);
            
            umtc += c;
        }
        System.out.println(  "                     " + umtc);
        stats.umt = umtc;
        
        // entity
        
        System.out.println("\n==================== entity:");
        int entityc = 0;
        for(int i = 0, len = entityCount.length, c = 0; i < len; i++)
        {
            if(0 == (c = entityCount[i]))
                continue;
            
            stats.addEntityEntry(new Stats.KindEntry(i, c));
            
            String str = i + ": " + c;
            System.out.print(str);
            
            String display = display(registry.getEntityMetadata(i));
            int spaces = 21 - str.length();
            if(spaces > 0)
            {
                char[] copy = new char[spaces];
                System.arraycopy(FILLER, 0, copy, 0, copy.length);
                System.out.print(copy);
            }
            else
            {
                // single space
                System.out.print(' ');
            }
            
            System.out.println(display);
            
            entityc += c;
        }
        System.out.println(  "                     " + entityc);
        stats.entity = entityc;
        
        // linked
        
        System.out.println("\n==================== linked:");
        int linkedc = 0;
        for(int i = 0, len = linkedEntityCount.length, c = 0; i < len; i++)
        {
            if(0 == (c = linkedEntityCount[i]))
                continue;

            stats.addLinkedEntityEntry(new Stats.KindEntry(i, c));
            
            String str = i + ": " + c;
            System.out.print(str);
            
            String display = display(registry.getEntityMetadata(i));
            int spaces = 21 - str.length();
            if(spaces > 0)
            {
                char[] copy = new char[spaces];
                System.arraycopy(FILLER, 0, copy, 0, copy.length);
                System.out.print(copy);
            }
            else
            {
                // single space
                System.out.print(' ');
            }
            
            System.out.println(display);
            
            linkedc += c;
        }
        System.out.println(  "                     " + linkedc);
        stats.linkedEntity = linkedc;
        
        return stats;
    }
}
