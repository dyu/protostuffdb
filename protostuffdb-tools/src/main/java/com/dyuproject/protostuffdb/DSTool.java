//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.DatastoreManager.CREATE_IF_MISSING;
import static com.dyuproject.protostuffdb.DatastoreManager.PARANOID_CHECKS;
import io.airlift.command.Arguments;
import io.airlift.command.Cli;
import io.airlift.command.Cli.CliBuilder;
import io.airlift.command.Command;
import io.airlift.command.Help;
import io.airlift.command.Option;
import io.airlift.command.OptionType;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import protostuffdb.Jni;

/**
 * Datastore tools cli.
 * 
 * @author David Yu
 * @created Feb 4, 2014
 */
public final class DSTool
{
    private DSTool() {}
    
    public static final WriteContext CONTEXT = new WriteContext(
            Jni.bufDb(0), Jni.bufTmp(0), 0, Jni.PARTITION_SIZE);
    
    private static final HashMap<String,Module> __modules = new HashMap<String,Module>();
    
    @SuppressWarnings("unchecked")
    public static CliBuilder<Runnable> newBuilder()
    {
        return Cli.<Runnable>builder("dstool")
                .withDescription("data and schema tool")
                .withDefaultCommand(Help.class)
                .withCommands(Help.class, 
                        ListModules.class, 
                        ListKinds.class, 
                        Dump.class, 
                        Load.class, 
                        Index.class, 
                        Pipe.class);
    }
    
    public static final class Module
    {
        public final EntityMetadataResource registry;
        public final TagMetadata tm;
        
        Module(EntityMetadataResource registry, TagMetadata tm)
        {
            this.registry = registry;
            this.tm = tm;
        }
    }
    
    public static void register(String name, 
            EntityMetadataResource registry, TagMetadata tm)
    {
        __modules.put(name, new Module(registry, tm));
    }
    
    public static Module getModule(String name, Base base)
    {
        Module m = __modules.get(name);
        if(m == null)
            throw err("Unregistered module: %s", name);
        
        return m;
    }
    
    public static boolean isSame(File f1, File f2)
    {
        try
        {
            return f1.getCanonicalPath().equals(f2.getCanonicalPath());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    public static RuntimeException err(String msg, Object ... args)
    {
        throw new IllegalArgumentException(String.format(msg, args));
    }
    
    public static File getExistingDir(File parentDir, String path)
    {
        File dir = new File(parentDir, path);
        if(!dir.exists() || !dir.isDirectory())
            throw err("%s does not exist.", dir);
        
        return dir;
    }
    
    public static File getExistingDir(String path)
    {
        File dir = new File(path);
        if(!dir.exists() || !dir.isDirectory())
            throw err("%s does not exist.", dir);
        
        return dir;
    }
    
    public static File getOrCreateDir(File parentDir, String path)
    {
        final File dir = new File(parentDir, path);
        if(!dir.exists())
            dir.mkdirs();
        
        return dir;
    }
    
    public static File getOrCreateDir(String path)
    {
        final File dir = new File(path);
        if(!dir.exists())
            dir.mkdirs();
        
        return dir;
    }
    
    public static StringBuilder display(EntityMetadata<?> em, StringBuilder sb)
    {
        if(em.parent == null)
            return sb.append(em.pipeSchema.messageName());
        
        return display(em.parent, sb).append('.').append(em.pipeSchema.messageName());
    }
    
    public static abstract class Base implements Runnable
    {
        @Option(name = "-v", type = OptionType.GLOBAL, 
                description = "Verbose mode")
        public boolean verbose;
        
        public static RuntimeException ex(String msg)
        {
            return new RuntimeException(msg);
        }
    }
    
    @Command(name = "listmodules", description = "Lists the modules.")
    public static class ListModules extends Base
    {
        public void run()
        {
            for(String name : __modules.keySet())
                System.out.println(name);
        }
    }
    
    @Command(name = "listkinds", description = "Lists the kinds of a module.")
    public static class ListKinds extends Base
    {
        @Arguments(required = true, 
                description = "The module names")
        public List<String> names;
        
        public void run()
        {
            final StringBuilder sb = new StringBuilder();
            for(String name : names)
            {
                Module m = getModule(name, this);
                //sb.append("========================= ").append(name).append('\n');
                for(int i = 0; i < 128; i++)
                {
                    final EntityMetadata<?> em = m.registry.getEntityMetadata(i);
                    if(em != null)
                    {
                        sb.append(i);
                        
                        if(i > 99)
                            sb.append("  ");
                        else if(i > 9)
                            sb.append("   ");
                        else
                            sb.append("    ");
                        
                        display(em, sb).append('\n');
                    }
                }
            }
            
            System.out.println(sb.toString());
        }
    }
    
    @Command(name = "dump", description = "Dumps the contents of a datastore.")
    public static class Dump extends Base
    {
        
        @Option(name = "-n", 
                description = "Numeric json format.")
        public boolean numeric;
        
        @Option(name = "-pc", 
                description = "Paranoid checks while opening the database.")
        public boolean paranoidChecks;
        
        @Option(name = "-de", 
                description = "Deserialize entity (new fields will be added/indexed).")
        public boolean deserializeEntity;
        
        @Option(name = "-i", required = true, 
                description = "The input/source dir.")
        public String in;
        
        @Option(name = "-o", required = true, 
                description = "The output/destination dir.")
        public String out;
        
        @Arguments(required = true, 
                description = "The module names")
        public List<String> names;

        public void run()
        {
            final File inDir = getExistingDir(in), 
                    outDir = getOrCreateDir(out); // the dump dir
            
            if(isSame(inDir, outDir))
                throw err("%s cannot be both an input and output.", inDir);
            
            int flags = 0;
            
            if(paranoidChecks)
                flags |= PARANOID_CHECKS;
            
            final LsmdbDatastoreManager manager = new LsmdbDatastoreManager(
                    // using the rpc buf (unused) on purpose
                    Jni.buf(0), inDir);
            try
            {
                for(String name : names)
                    dump(outDir, getModule(name, this), manager, name, flags);
            }
            finally
            {
                manager.close();
            }
        }

        private void dump(File outDir, 
                Module module, DatastoreManager manager, String name, int flags)
        {
            final File targetDir = getOrCreateDir(outDir, name);
            
            final Datastore store = manager.getStore(name, flags);
            
            if(verbose)
            {
                System.out.println("Db file was created @ " + 
                        new java.sql.Date(store.getCreateTs()));
            }
            
            DumpTool.exec(CONTEXT, store, targetDir, 
                    module.registry, module.tm, 
                    numeric, deserializeEntity);
        }
    }
    
    @Command(name = "load", description = "Loads the json files from a dump into a datastore.")
    public static class Load extends Base
    {
        
        @Option(name = "-n", 
                description = "Numeric json format.")
        public boolean numeric;
        
        @Option(name = "-pc", 
                description = "Paranoid checks while opening the database.")
        public boolean paranoidChecks;
        
        @Option(name = "-wb", 
                description = "All writes are done via a single write batch.")
        public boolean useOpChain;
        
        
        @Option(name = "-sev", 
                description = "Skip entity validation.")
        public boolean skipEntityValidation;
        
        @Option(name = "-i", required = true, 
                description = "The input/source dir (from dump).")
        public String in;
        
        @Option(name = "-o", required = true, 
                description = "The output/destination dir.")
        public String out;
        
        @Arguments(required = true, 
                description = "The module names")
        public List<String> names;

        public void run()
        {
            final File inDir = getExistingDir(in), // the dump dir
                    outDir = getOrCreateDir(out);
            
            if(isSame(inDir, outDir))
                throw err("%s cannot be both an input and output.", inDir);
            
            int flags = 0;
            
            if(paranoidChecks)
                flags |= PARANOID_CHECKS;
            
            final LsmdbDatastoreManager manager = new LsmdbDatastoreManager(
                    // using the rpc buf (unused) on purpose
                    Jni.buf(0), outDir);
            try
            {
                for(String name : names)
                    load(inDir, getModule(name, this), manager, name, flags);
            }
            finally
            {
                manager.close();
            }
        }
        
        private void load(File inDir, 
                Module module, DatastoreManager manager, String name, int flags)
        {
            final File sourceDir = getExistingDir(inDir, name);
            
            final Datastore store = manager.getStore(name, flags | CREATE_IF_MISSING);
            
            if(verbose)
            {
                System.out.println("Db file was created @ " + 
                        new java.sql.Date(store.getCreateTs()));
            }
            
            LoadTool.exec(CONTEXT, sourceDir, store, 
                    module.registry, module.tm, 
                    OpChainDelegate.BUILTIN.DEFAULT, numeric, useOpChain, 
                    skipEntityValidation);
        }
    }
    
    @Command(name = "index", description = "Indexes the entities in a datastore.")
    public static class Index extends Base
    {
        
        //@Option(name = "-n", description = "Numeric json format.")
        //public boolean numeric;
        
        @Option(name = "-pc", 
                description = "Paranoid checks while opening the database.")
        public boolean paranoidChecks;
        
        @Option(name = "-wb", 
                description = "All writes are done via a single write batch.")
        public boolean useOpChain;
        
        @Option(name = "-de", 
                description = "Deserialize entity (new fields will be added/indexed).")
        public boolean deserializeEntity;
        
        @Option(name = "-i", required = true, 
                description = "The input/source dir.")
        public String in;
        
        @Option(name = "-o", 
                description = "The output/destination dir.")
        public String out;
        
        @Arguments(required = true, 
                description = "The module names")
        public List<String> names;

        public void run()
        {
            final File inDir = getExistingDir(in), 
                    outDir = out == null ? null : getOrCreateDir(out);
            
            int flags = 0;
            
            if(paranoidChecks)
                flags |= PARANOID_CHECKS;
            
            final LsmdbDatastoreManager manager = new LsmdbDatastoreManager(
                    // using the rpc buf (unused) on purpose
                    Jni.buf(0), inDir);
            try
            {
                for(String name : names)
                    index(inDir, outDir, getModule(name, this), manager, name, flags);
            }
            finally
            {
                manager.close();
            }
        }

        private void index(File inDir, File outDir, 
                Module module, DatastoreManager manager, String name, int flags)
        {
            File targetDir;
            
            final Datastore store = manager.getStore(name, flags), 
                    target;
            
            if(verbose)
            {
                System.out.println("Db file was created @ " + 
                        new java.sql.Date(store.getCreateTs()));
            }
            
            if(outDir == null)
            {
                // in-place update
                target = store;
            }
            else if((targetDir = new File(outDir, name)).exists() && 
                    isSame(targetDir, new File(inDir, name)))
            {
                // points to the same dir
                target = store;
            }
            else
            {
                // a separate dir
                if(!targetDir.exists())
                    targetDir.mkdirs();
                
                target = manager.getStore(store.name + "-target", 
                        flags | CREATE_IF_MISSING, 
                        targetDir);
            }
            
            IndexTool.exec(useOpChain, deserializeEntity, 
                    module.registry, module.tm, 
                    CONTEXT, store, target);
        }
    }
    
    @Command(name = "pipe", description = "Transfers the contents of a datastore to another.")
    public static class Pipe extends Base
    {
        
        //@Option(name = "-n", description = "Numeric json format.")
        //public boolean numeric;
        
        @Option(name = "-pc", 
                description = "Paranoid checks while opening the database.")
        public boolean paranoidChecks;
        
        @Option(name = "-wb", 
                description = "All writes are done via a single write batch.")
        public boolean useOpChain;
        
        @Option(name = "-i", required = true, 
                description = "The input/source dir.")
        public String in;
        
        @Option(name = "-o", required = true, 
                description = "The output/destination dir.")
        public String out;
        
        @Arguments(required = true, 
                description = "The module names")
        public List<String> names;

        public void run()
        {
            final File inDir = getExistingDir(in), 
                    outDir = getOrCreateDir(out);
            
            if(isSame(inDir, outDir))
                throw err("%s cannot be both an input and output.", inDir);
            
            int flags = 0;
            
            if(paranoidChecks)
                flags |= PARANOID_CHECKS;
            
            final LsmdbDatastoreManager manager = new LsmdbDatastoreManager(
                    // using the rpc buf (unused) on purpose
                    Jni.buf(0), inDir);
            try
            {
                for(String name : names)
                    pipe(outDir, manager, name, flags);
            }
            finally
            {
                manager.close();
            }
        }

        private void pipe(File outDir, 
                LsmdbDatastoreManager manager, String name, int flags)
        {
            final File targetDir = getOrCreateDir(outDir, name);
            
            if (!manager.pipe(name, flags, name + "-target", targetDir))
                throw err("Could not pipe %s to dir %s.", name, targetDir.toString());
            
            /*final Datastore store = manager.getStore(name, flags);
            
            if(verbose)
            {
                System.out.println("Db file was created @ " + 
                        new java.sql.Date(store.getCreateTs()));
            }
            
            PipeDBTool.exec(useOpChain, CONTEXT, store, 
                    manager.getStore(store.name + "-target", 
                            flags | CREATE_IF_MISSING, targetDir));*/
        }
    }
    
    static
    {
        protostuffdb.Jni.loadNativeLibrary();
        protostuffdb.LsmdbJni.setup(com.dyuproject.protostuff.JniStream.BAO);
    }
}
