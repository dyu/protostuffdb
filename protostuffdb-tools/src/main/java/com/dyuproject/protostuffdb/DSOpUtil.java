//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;



/**
 * Utility functions that involve the datastore.
 * 
 * @author David Yu
 * @created Nov 19, 2012
 */
public final class DSOpUtil
{
    
    /**
     * Performs a standard write operation to the {@link OpChain}.
     * Returns the total number of entity and tag indices written.
     */
    public static <T> int writeTo(OpChain chain, byte[] key, 
            EntityMetadata<T> em, 
            final Datastore store, final WriteContext context, 
            final boolean index, final int idxCount, 
            final byte[] entityBuffer, int entityOffset, int size)
    {
        if(chain == null)
            store.rawPut(key, 0, key.length, entityBuffer, entityOffset, size, context);
        else
            chain.rawPut(key, 0, key.length, entityBuffer, entityOffset, size);
        
        final int keyOffset = key.length == 9 ? 0 : 9;
        
        int writeCount = 0;
        if(index)
        {
            writeCount += indexOnInsert(chain, 
                    key, keyOffset, 
                    em, store, context, 
                    idxCount, 
                    entityBuffer, entityOffset, size);
        }
        
        if(0 != em.linkIndex.length)
        {
            // the last one is the kind
            final byte[] linkIndexKey = new byte[10];
            linkIndexKey[0] = (byte)(0x80 | key[keyOffset]);
            System.arraycopy(key, keyOffset+1, linkIndexKey, 1, 8);
            
            for(int kind : em.linkIndex)
            {
                linkIndexKey[9] = (byte)kind;
                
                if(chain == null)
                {
                    store.rawPut(linkIndexKey, 0, linkIndexKey.length, 
                            entityBuffer, entityOffset, size, context);
                }
                else
                {
                    chain.rawPut(linkIndexKey, 0, linkIndexKey.length, 
                            entityBuffer, entityOffset, size);
                }
                
                writeCount++;
            }
        }
        
        return writeCount;
    }
    
    private static <T> void writeIndexOnInsert(OpChain chain, 
            SerAndIndexCollectOutput output, int id, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, 
            Datastore store, WriteContext context, 
            byte[] buf, int bufOffset, 
            int[] result, 
            byte[] entityBuffer, int entityOffset, int size) 
    {
        final int secondaryTags = em.index.secondaryTagEntries[id];
        
        // key
        final int newKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetAddIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                entityBuffer, entityOffset, size, 
                result, false);
        
        // value
        final int newValueSize = result[0];
        
        final byte[] valueBuf;
        final int valueOffset, valueSize;
        if(newValueSize == 0)
        {
            // timestamp
            valueBuf = key;
            valueOffset = keyOffset+1;
            valueSize = 6;
        }
        else if(newValueSize == -1)
        {
            result[0] = 0; // reset
            
            // the message itself.
            valueBuf = entityBuffer;
            valueOffset = entityOffset;
            valueSize = size;
        }
        else
        {
            result[0] = 0; // reset
            
            // custom value
            valueBuf = buf;
            valueOffset = bufOffset+newKeySize;
            valueSize = newValueSize;
        }
        
        // custom value
        if(chain == null)
            store.rawPut(buf, bufOffset, newKeySize, valueBuf, valueOffset, valueSize, context);
        else
            chain.rawPut(buf, bufOffset, newKeySize, valueBuf, valueOffset, valueSize);
        
        if(secondaryTags == 0)
            return;
        
        int tagKeyOffset = bufOffset, tagKeySize = newKeySize;
        if(id > 223)
        {
            // entity secondary index
            buf[++tagKeyOffset] = 0;
            tagKeySize--;
        }
        
        for(int shift = 0; shift <= 24; shift+=8)
        {
            int tag = 0xFF & (secondaryTags >>> shift);
            if(tag == 0)
                break;
            
            buf[tagKeyOffset+1] = (byte)tag;
            
            if(chain == null)
                store.rawPut(buf, tagKeyOffset, tagKeySize, valueBuf, valueOffset, valueSize, context);
            else
                chain.rawPut(buf, tagKeyOffset, tagKeySize, valueBuf, valueOffset, valueSize);
        }
    }
    
    private static <T> int indexOnInsert(final OpChain chain, 
            final byte[] key, final int keyOffset, 
            final EntityMetadata<T> em, 
            final Datastore store, final WriteContext context, 
            final int idxCount, 
            final byte[] entityBuffer, final int entityOffset, final int size)
    {
        final SerAndIndexCollectOutput output = context.serAndIndexCollectOutput;
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        
        final int[] result = context.intset();
        
        int writeCount = 0;
        for(int i = 0, count = idxCount, field = 0; i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                writeCount++;
                
                writeIndexOnInsert(chain, output, id, 
                        key, keyOffset, 
                        em, store, context, 
                        buf, bufOffset, 
                        result, 
                        entityBuffer, entityOffset, size);
            }
        }
        
        // value dependencies
        for(int i = 0, id = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            writeCount++;
            
            writeIndexOnInsert(chain, output, id, 
                    key, keyOffset, 
                    em, store, context, 
                    buf, bufOffset, 
                    result, 
                    entityBuffer, entityOffset, size);
        }
        
        return writeCount;
    }
    
    
    /**
     * Returns zero if the key does not contain an entity key.
     */
    static int getKind(byte[] key)
    {
        if(key.length < 9)
            return 0;
        
        final int keyOffset = key.length - 9;
        
        int kind = 0xFF & key[keyOffset];
        if(kind < 1 || kind > 127)
            return 0;
        
        //long ts = KeyUtil.readTimestamp(key, keyOffset+1);
        //if(ts > System.currentTimeMillis())
        //    return 0;
        
        return kind;
    }

}
