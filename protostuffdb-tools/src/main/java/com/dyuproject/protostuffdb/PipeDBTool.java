//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import protostuffdb.Jni;


/**
 * Pipes the source datastore's data to another datastore.  
 * 
 * @author David Yu
 * @created Sep 3, 2012
 */
public final class PipeDBTool implements Op<Datastore>, Visitor<Datastore>
{
    
    public static PipeDBTool exec(boolean useOpChain, 
            WriteContext context, Datastore source, Datastore target)             
    {
        final PipeDBTool tool = new PipeDBTool(context);
        
        final boolean success;
        if(useOpChain)
        {
            success = target.chain(null, tool, source, 0, tool.context);
        }
        else
        {
            tool.count = source.scan(false, -1, false, null, context, tool, target);
            
            success = true;
        }
        
        if(success)
        {
            System.out.println(tool.count + " entries were transferred.");
        }
        
        return tool;
    }
    
    final WriteContext sourceContext;
    // for writing
    final WriteContext context = new WriteContext(Jni.bufDb(0), Jni.bufTmp(0), 
            0, Jni.PARTITION_SIZE);
    
    public int count;
    
    PipeDBTool(WriteContext sourceContext)
    {
        this.sourceContext = sourceContext;
    }
    
    public boolean handle(OpChain chain, byte[] key, 
            byte[] value, int offset, int len,
            Datastore source)
    {
        count = source.scan(false, -1, false, null, sourceContext, CHAIN_VISITOR, chain);
        
        return true;
    }
    
    public boolean visit(byte[] key, 
            byte[] v, int voffset, int vlen, 
            Datastore store, int index)
    {
        store.rawPut(key, 0, key.length, 
                v, voffset, vlen, 
                context);
        
        return false;
    }
    
    static final Visitor<OpChain> CHAIN_VISITOR = new Visitor<OpChain>()
    {
        
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                OpChain chain, int index)
        {
            chain.rawPut(key, 0, key.length, 
                    v, voffset, vlen);
            
            return false;
        }
    };

}
