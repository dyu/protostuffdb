//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.DumpLoadUtil.newOutputStream;
import static com.dyuproject.protostuffdb.DumpLoadUtil.printStats;
import static com.dyuproject.protostuffdb.DumpLoadUtil.toInt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import com.dyuproject.protostuff.B64Code;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.JsonInput;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.ProtostuffException;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.ToolsIOUtil;
import com.dyuproject.protostuff.YamlIOUtil;
import com.dyuproject.protostuffdb.DumpTool.DumpException;

/**
 * Loads the content from the files dumped by {@code DumpTool}.
 * 
 * @author David Yu
 * @created Nov 14, 2012
 */
public final class LoadTool implements Op<Void>
{
    
    public static final boolean SKIP_ENTITY_VALIDATION = 
            Boolean.getBoolean("loadtool.skip_entity_validation");
    
    public static class LoadException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public LoadException(){}
        public LoadException(String msg){super(msg);}
        public LoadException(String msg, Exception cause){super(msg, cause);}
    }
    
    static final Comparator<File> FILE_COMPARATOR = new Comparator<File>()
    {
        public int compare(File f1, File f2)
        {
            return ValueUtil.compare(f1.getName(), f2.getName());
        }
    };
    
    /**
     * Sorted by directory.
     */
    static final Comparator<File> DIR_FILE_COMPARATOR = new Comparator<File>()
    {
        public int compare(File f1, File f2)
        {
            if(f1.isDirectory())
            {
                return f2.isDirectory() ? ValueUtil.compare(f1.getName(), f2.getName()) : 
                    -1;
            }
            
            if(f2.isDirectory())
                return 1;
            
            return ValueUtil.compare(f1.getName(), f2.getName());
        }
    };
    
    static ArrayList<File> getFilesByExtension(File dir, String extension, boolean sort)
    {
        ArrayList<File> files = new ArrayList<File>();
        addFilesByExtension(files, dir, extension, sort);
        return files;
    }
    
    /**
     * Only adds specific file types from the dir (recursive). 
     */
    static void addFilesByExtension(ArrayList<File> list, File dir, String extension, 
            boolean sort)
    {
        final File[] files = dir.listFiles();
        
        if(sort)
            Arrays.sort(files, DIR_FILE_COMPARATOR);
        
        for(File f : files)
        {
            if(f.isDirectory())
                addFilesByExtension(list, f, extension, sort);
            else if(f.getName().endsWith(extension))
                list.add(f);
        }
    }
    
    static ArrayList<File> getFiles(File baseDir, String dirName, String extension)
    {
        File dir = new File(baseDir, dirName);
        return dir.exists() ? getFilesByExtension(dir, extension, false) : null;
    }
    
    static ArrayList<File> getPreSortedFiles(File baseDir, String dirName, String extension)
    {
        File dir = new File(baseDir, dirName);
        return dir.exists() ? getFilesByExtension(dir, extension, true) : null;
    }
    
    public static <T> void exec(WriteContext context, File loadDir, 
            Datastore target, EntityMetadataResource registry, TagMetadata tm, 
            OpChainDelegate delegate, boolean numeric, boolean useOpChain)
    {
        exec(context, loadDir, target, registry, tm, delegate, numeric, useOpChain, 
                SKIP_ENTITY_VALIDATION);
    }

    public static <T> void exec(WriteContext context, File loadDir, 
            Datastore target, EntityMetadataResource registry, TagMetadata tm, 
            OpChainDelegate delegate, boolean numeric, boolean useOpChain, 
            boolean skipEntityValidation)
    {
        int includeCount = 0;
        final ArrayList<File> umtFiles = getFiles(loadDir, "umt", ".json");
        if(umtFiles != null)
        {
            if(umtFiles.isEmpty())
                System.err.println("No .json files found in " + new File(loadDir, "umt"));
            else
            {
                Collections.sort(umtFiles, FILE_COMPARATOR);
                includeCount++;
            }
        }
        
        final ArrayList<File> entityFiles = getFiles(loadDir, "entity", ".json");
        if(entityFiles != null)
        {
            if(entityFiles.isEmpty())
                System.err.println("No .json files found in " + new File(loadDir, "entity"));
            else
            {
                Collections.sort(entityFiles, FILE_COMPARATOR);
                includeCount++;
            }
        }
        
        final ArrayList<File> pkvFiles = getFiles(loadDir, "pkv", ".json");
        if(pkvFiles != null)
        {
            if(pkvFiles.isEmpty())
                System.err.println("No .json files found in " + new File(loadDir, "pkv"));
            else
            {
                Collections.sort(pkvFiles, FILE_COMPARATOR);
                includeCount++;
            }
        }
        
        final ArrayList<File> linkedEntityFiles = getFiles(loadDir, "linked", ".json");
        if(linkedEntityFiles != null)
        {
            if(linkedEntityFiles.isEmpty())
                System.err.println("No .json files found in " + new File(loadDir, "linked"));
            else
            {
                Collections.sort(linkedEntityFiles, FILE_COMPARATOR);
                includeCount++;
            }
        }
        
        final File repFile = new File(loadDir, "rep.json");
        
        final File footerFile = new File(loadDir, "footer.json");
        
        final ArrayList<File> loadFiles = getPreSortedFiles(loadDir, "load", ".json");
        if(loadFiles != null)
        {
            if(loadFiles.isEmpty())
                throw new LoadException("No .json files found in " + new File(loadDir, "load"));
            
            includeCount++;
        }
        
        if(includeCount == 0 && !footerFile.exists())
            throw new LoadException("Nothing to load from " + loadDir);
        
        final LoadTool op = new LoadTool(registry, delegate, 
                numeric, skipEntityValidation, 
                target, context, 
                umtFiles, entityFiles, 
                pkvFiles, linkedEntityFiles, 
                repFile, footerFile, loadFiles);
        
        if(useOpChain)
        {
            if(!target.chain(null, op, null, 0, context))
                throw new LoadException("Load failed.");
        }
        else
        {
            op.handle(null, null, null, 0, 0, null);
        }
        
        final Stats stats = printStats(0, op.kvCount, 
                op.tagCount, op.entityCount, op.linkedEntityCount, 
                registry, tm);
        
        // dump to target dir if it exists (otherwise current dir)
        File statsDir = new File("target");
        if(!statsDir.exists())
            statsDir = new File(".");
        try
        {
            YamlIOUtil.writeTo(newOutputStream(statsDir, "load-stats.yaml"), 
                    stats, Stats.getSchema(), op.lb.clear());
        }
        catch (IOException e)
        {
            throw new DumpException("Failed to write yaml", e);
        }
    }
    
    final ArrayList<File> umtFiles, entityFiles, pkvFiles, linkedEntityFiles;
    final File repFile, footerFile;
    final ArrayList<File> loadFiles;
    final byte[] buf = new byte[8192];
    final LinkedBuffer lb = LinkedBuffer.use(buf);
    final Fixed64Input f64input = new Fixed64Input(0, 0);
    
    final EntityMetadataResource registry;
    final OpChainDelegate delegate;
    final boolean numeric;
    final boolean skipEntityValidation;
    
    final Datastore store;
    final WriteContext context;
    
    final byte[][] keys = new byte[128][];
    
    int kvCount = 0, loadEntityCount = 0;
    final int[] tagCount = new int[256], 
            entityCount = new int[128], 
            linkedEntityCount = new int[128];
    
    LoadTool(EntityMetadataResource registry, OpChainDelegate delegate, 
            boolean numeric, boolean skipEntityValidation, 
            Datastore store, WriteContext context,  
            ArrayList<File> umtFiles, ArrayList<File> entityFiles, 
            ArrayList<File> pkvFiles, ArrayList<File> linkedEntityFiles, 
            File repFile, File footerFile, ArrayList<File> loadFiles)
    {
        this.registry = registry;
        this.delegate = delegate;
        this.numeric = numeric;
        this.skipEntityValidation = skipEntityValidation;
        
        this.store = store;
        this.context = context;
        
        this.umtFiles = umtFiles;
        this.entityFiles = entityFiles;
        this.pkvFiles = pkvFiles;
        this.linkedEntityFiles = linkedEntityFiles;
        this.repFile = repFile;
        this.footerFile = footerFile;
        this.loadFiles = loadFiles;
    }
    
    static int mergeKVTo(OpChain chain, 
            Datastore store, WriteContext context, File file, 
            boolean numeric, LinkedBuffer lb) throws IOException
    {
        int count = 0;
        final KV message = new KV();
        final Schema<KV> schema = KV.getSchema();
        final JsonInput input = ToolsIOUtil.newJsonInput(file, numeric, lb);
        final JsonParser parser = input.parser;
        try
        {
            while(parser.nextToken() == JsonToken.START_OBJECT)
            {
                schema.mergeFrom(input, message);
                
                if(!schema.isInitialized(message))
                    throw new LoadException("Both key and value required on " + file);
                
                if(chain == null)
                    store.rawPut(message.key, message.value, context);
                else
                    chain.rawPut(message.key, message.value);
                
                if(parser.getCurrentToken() != JsonToken.END_OBJECT)
                    throw new LoadException("Malformed json on " + file);
                
                count++;
                
                // reset
                message.key = null;
                message.value = null;
            }
        }
        finally
        {
            parser.close();
        }
        
        return count;
    }
    
    static <T> int mergeEntityTo(OpChain chain, 
            Datastore store, WriteContext context, File file, 
            boolean numeric, LinkedBuffer lb, 
            EntityMetadata<T> em, OpChainDelegate delegate, 
            boolean skipEntityValidation, 
            final Fixed64Input f64input) throws IOException
    {
        return mergeEntityTo(chain, store, context, file, 
                ToolsIOUtil.newJsonInput(file, numeric, lb), 
                em, delegate, 
                skipEntityValidation,
                f64input);
    }
    
    static <T> int mergeEntityTo(OpChain chain, 
            Datastore store, WriteContext context, File file, 
            final JsonInput input, 
            EntityMetadata<T> em, OpChainDelegate delegate, 
            boolean skipEntityValidation, 
            final Fixed64Input f64input) throws IOException
    {
        int count = 0;
        final Schema<T> schema = em.pipeSchema.wrappedSchema;
        final JsonParser parser = input.parser;
        try
        {
            if(parser.nextToken() == null)
                return count;
            
            T message = null;
            for(byte[] key = null; ;key = null)
            {
                switch(parser.getCurrentToken())
                {
                    case START_ARRAY:
                        if(parser.nextToken() != JsonToken.VALUE_STRING)
                            throw new LoadException("Malformed json on " + file);
                        
                        key = parser.getBinaryValue();
                        
                        if(parser.nextToken() != JsonToken.END_ARRAY)
                            throw new LoadException("Malformed json on " + file);
                        
                        if(parser.nextToken() != JsonToken.START_OBJECT)
                            throw new LoadException("Malformed json on " + file);
                        
                    case START_OBJECT:
                        message = schema.newMessage();
                        try
                        {
                            schema.mergeFrom(input, message);
                            
                            if(!skipEntityValidation)
                                schema.isInitialized(message);
                        }
                        catch(IOException e)
                        {
                            // print the invalid message.
                            System.err.println("Malformed message on " + file);
                            throw e;
                        }
                        catch(RuntimeException e)
                        {
                            // print the invalid message.
                            System.err.println("Invalid message on " + file);
                            throw e;
                        }
                        
                        if(parser.getCurrentToken() != JsonToken.END_OBJECT)
                            throw new LoadException("Malformed json on " + file);
                        
                        break;
                    default:
                        throw new LoadException("Malformed json on " + file);
                }
                
                count++;
                
                if(key == null)
                {
                    byte[] k = new byte[9];
                    f64input.value = context.fillEntityKey(k, em, context.ts(em));
                    
                    // new entry
                    delegate.insert(chain, store, context, 
                            k, 
                            message, em, null, 
                            f64input);
                }
                else
                {
                    // existing entry
                    if(key.length != 9)
                        throw new LoadException("Malformed json on " + file);
                    
                    if(em.kind != key[0])
                    {
                        throw new LoadException("Not the same kind: " + 
                                em.kind + " != " + (int)key[0] + 
                                " on " + file);
                    }
                    
                    delegate.update(chain, store, context, 
                            key, message, em, null);
                }
                
                if(parser.nextToken() == null)
                    return count;
            }
        }
        finally
        {
            parser.close();
        }
    }
    
    static <T> int mergeLinkedEntityTo(OpChain chain, 
            Datastore store, WriteContext context, File file, 
            boolean numeric, LinkedBuffer lb, 
            EntityMetadata<T> em, OpChainDelegate delegate, 
            boolean skipEntityValidation, 
            final Fixed64Input f64input) throws IOException
    {
        return mergeLinkedEntityTo(chain, store, context, file, 
                ToolsIOUtil.newJsonInput(file, numeric, lb), 
                em, delegate, 
                skipEntityValidation,
                f64input);
    }
    
    static <T> int mergeLinkedEntityTo(OpChain chain, 
            Datastore store, WriteContext context, File file, 
            final JsonInput input, 
            EntityMetadata<T> em, OpChainDelegate delegate, 
            boolean skipEntityValidation,
            final Fixed64Input f64input) throws IOException
    {
        int count = 0;
        final Schema<T> schema = em.pipeSchema.wrappedSchema;
        final JsonParser parser = input.parser;
        try
        {
            if(parser.nextToken() == null)
                return count;
            
            T message = null;
            for(byte[] key = null, parentKey = null; ;key = null)
            {
                switch(parser.getCurrentToken())
                {
                    case START_ARRAY:
                        if(parser.nextToken() != JsonToken.VALUE_STRING)
                            throw new LoadException("Malformed json on " + file);
                        
                        key = parser.getBinaryValue();
                        
                        if(parser.nextToken() != JsonToken.END_ARRAY)
                            throw new LoadException("Malformed json on " + file);
                        
                        if(parser.nextToken() != JsonToken.START_OBJECT)
                            throw new LoadException("Malformed json on " + file);
                        
                    case START_OBJECT:
                        message = schema.newMessage();
                        try
                        {
                            schema.mergeFrom(input, message);
                            
                            if(!skipEntityValidation)
                                schema.isInitialized(message);
                        }
                        catch(IOException e)
                        {
                            // print the invalid message.
                            System.err.println("Malformed message on " + file);
                            throw e;
                        }
                        catch(RuntimeException e)
                        {
                            // print the invalid message.
                            System.err.println("Invalid message on " + file);
                            throw e;
                        }
                        
                        if(parser.getCurrentToken() != JsonToken.END_OBJECT)
                            throw new LoadException("Malformed json on " + file);
                        
                        break;
                    default:
                        throw new LoadException("Malformed json on " + file);
                }
                
                count++;
                
                if(key == null)
                {
                    if(parentKey == null)
                        throw new LoadException("Malformed json on " + file);
                    
                    
                    byte[] k = new byte[9];
                    f64input.value = context.fillEntityKey(k, em, context.ts(em));
                    // insert with a new key and the same parent key
                    delegate.insert(chain, store, context, 
                            k, 
                            message, em, parentKey, 
                            f64input);
                    
                    if(parser.nextToken() == null)
                        return count;
                    
                    continue;
                }
                
                switch(key.length)
                {
                    case 9:
                    {
                        // insert with parent key
                        if(em.parent.kind != key[0])
                        {
                            throw new LoadException("Not the same parent: " + 
                                    em.parent.kind + " != " + (int)key[0] + 
                                    " on " + file);
                        }
                        
                        parentKey = key;
                        
                        byte[] k = new byte[9];
                        f64input.value = context.fillEntityKey(k, em, context.ts(em));
                        delegate.insert(chain, store, context, 
                                k, 
                                message, em, parentKey, 
                                f64input);
                        break;
                    }   
                    case 18:
                    {
                        // existing entry
                        final int parentKind = (key[0]&0x7f);
                        if(em.parent.kind != parentKind)
                        {
                            throw new LoadException("Not the same parent: " + 
                                    em.parent.kind + " != " + parentKind + 
                                    " on " + file);
                        }
                        
                        if(em.kind != key[9])
                        {
                            throw new LoadException("Not the same kind: " + 
                                    em.kind + " != " + (int)key[9] + 
                                    " on " + file);
                        }
                        
                        parentKey = null;
                        
                        final byte[] k = new byte[9], pk = new byte[9];
                        System.arraycopy(key, 0, pk, 0, 9);
                        pk[0] = (byte)parentKind;
                        
                        System.arraycopy(key, 9, k, 0, 9);
                        
                        delegate.update(chain, store, context, 
                                k, message, em, pk);
                        break;
                    }   
                     
                    default: 
                        throw new LoadException("Malformed json on " + file);
                }
                
                if(parser.nextToken() == null)
                    return count;
            }
        }
        finally
        {
            parser.close();
        }
    }
    
    @SuppressWarnings("unchecked")
    static int mergeAndInsertEntityTo(OpChain chain, 
            Datastore store, WriteContext context, File file, 
            boolean numeric, LinkedBuffer lb, 
            byte[][] keys, EntityMetadataResource registry, OpChainDelegate delegate, 
            int[] entityCount, int[] linkedEntityCount,
            final Fixed64Input f64input) throws IOException
    {
        //System.out.println("Processing: " + file);
        int count = 0;
        final JsonInput input = ToolsIOUtil.newJsonInput(file, numeric, lb);
        final JsonParser parser = input.parser;
        try
        {
            if(parser.nextToken() == null)
                return count;
            
            Object message = null;
            EntityMetadata<Object> em = null;
            int kind = 0;
            for(byte[] parentKey = null; ; parentKey = null)
            {
                switch(parser.getCurrentToken())
                {
                    case START_ARRAY:
                        switch(parser.nextToken())
                        {
                            case VALUE_STRING:
                                // comment ... consume until end array (ignore comments)
                                while(parser.nextToken() != JsonToken.END_ARRAY);
                                
                                if(parser.nextToken() == null)
                                    return count;
                                
                                continue;
                                
                            case VALUE_NUMBER_INT:
                                // standard op which specifies the kind
                                // reset
                                //parentKey = null;
                                break;
                                
                            default:
                                throw new LoadException("Malformed json on " + file);
                        }
                        
                        kind = parser.getIntValue();
                        em = (EntityMetadata<Object>)registry.getEntityMetadata(kind);
                        
                        if(em == null)
                            throw new LoadException("Unknown kind: " + kind + " on " + file);
                        
                        switch(parser.nextToken())
                        {
                            case END_ARRAY:
                                // no comments
                                if(em.parent != null)
                                {
                                    parentKey = keys[em.parent.kind];
                                    
                                    if(parentKey == null)
                                    {
                                        // insertion needs parent key that was 
                                        // inserted before this op
                                        throw new LoadException("Insertion of linked entity invalid on " + file);
                                    }
                                }
                                break;
                                
                            case VALUE_STRING:
                                // the parent key
                                final char[] textChars = parser.getTextCharacters();
                                final byte[] toDecode = new byte[12];
                                for(int i = 0, offset = parser.getTextOffset(); i < 12;)
                                    toDecode[i++] = (byte)textChars[offset++];

                                // overwrite
                                keys[em.parent.kind] = parentKey = B64Code.decode(toDecode);
                                
                                // must be end array
                                if(parser.nextToken() != JsonToken.END_ARRAY)
                                    throw new LoadException("Malformed json on " + file);
                                
                                break;
                            
                            default:
                                throw new LoadException("Malformed json on " + file);
                        }
                        
                        if(parser.nextToken() != JsonToken.START_OBJECT)
                            throw new LoadException("Malformed json on " + file);
                        
                    case START_OBJECT:
                        if(em == null)
                            throw new LoadException("Malformed json on " + file);
                        
                        if(parentKey == null)
                        {
                            if(em.parent != null && (parentKey = keys[em.parent.kind]) == null)
                                throw new LoadException("Insertion of linked entity invalid on " + file);
                        }
                        else if(em.parent == null)
                            throw new LoadException("Insertion of default entity invalid on " + file);
                        
                        message = em.pipeSchema.wrappedSchema.newMessage();
                        try
                        {
                            em.pipeSchema.wrappedSchema.mergeFrom(input, message);
                            // must validate
                            em.pipeSchema.wrappedSchema.isInitialized(message);
                        }
                        catch(ProtostuffException e)
                        {
                            // print the invalid message.
                            System.err.println("Malformed message on " + file);
                            throw e;
                        }
                        catch(RuntimeException e)
                        {
                            // print the invalid message.
                            System.err.println("Invalid message on " + file);
                            System.err.println(new String(YamlIOUtil.toByteArray(message, 
                                    em.pipeSchema.wrappedSchema, LinkedBuffer.allocate(512))));
                            throw e;
                        }
                        
                        if(parser.getCurrentToken() != JsonToken.END_OBJECT)
                            throw new LoadException("Malformed json on " + file);
                        
                        break;
                        
                    default:
                        throw new LoadException("Malformed json on " + file);
                }
                
                count++;
                
                final byte[] key = new byte[9];
                if(em.parent == null)
                {
                    f64input.value = context.fillEntityKey(key, em, context.ts(em));
                    entityCount[em.kind]++;
                }
                else
                {
                    f64input.value = context.fillEntityKey(key, em, 
                            em.seq ? context.ts(em) : KeyUtil.extractTsFrom(parentKey));
                    linkedEntityCount[em.kind]++;
                }
                
                // overwrite
                keys[em.kind] = key;
                
                delegate.insert(chain, store, context, 
                        key, message, em, parentKey, 
                        f64input);
                
                if(parser.nextToken() == null)
                    return count;
            }
        }
        finally
        {
            parser.close();
        }
    }

    private void loadUMTTo(OpChain chain)
    {
        for(File f : umtFiles)
        {
            final int tag = toInt(f.getName());
            try
            {
                tagCount[tag] += mergeKVTo(chain, store, context, f, numeric, lb);
            }
            catch (IOException e)
            {
                throw new LoadException("Failed on umt file: " + f, e);
            }
        }
    }
    
    private void loadPKVTo(OpChain chain)
    {
        for(File f : pkvFiles)
        {
            try
            {
                kvCount += mergeKVTo(chain, store, context, f, numeric, lb);
            }
            catch (IOException e)
            {
                throw new LoadException("Failed on pkv file: " + f, e);
            }
        }
    }
    
    private void loadRepFileTo(OpChain chain)
    {
        try
        {
            mergeKVTo(chain, store, context, repFile, numeric, lb);
        }
        catch (IOException e)
        {
            throw new LoadException("Failed on rep file: " + repFile, e);
        }
    }
    
    private void loadFooterFileTo(OpChain chain)
    {
        try
        {
            mergeKVTo(chain, store, context, footerFile, numeric, lb);
        }
        catch (IOException e)
        {
            throw new LoadException("Failed on footer file: " + footerFile, e);
        }
    }
    
    private void loadEntityTo(OpChain chain)
    {
        for(File f : entityFiles)
        {
            final int kind = toInt(f.getName());
            try
            {
                entityCount[kind] += mergeEntityTo(chain, store, context, f, 
                        numeric, lb, 
                        registry.getEntityMetadata(kind), delegate, 
                        skipEntityValidation,
                        f64input);
            }
            catch (IOException e)
            {
                throw new LoadException("Failed on entity file: " + f, e);
            }
        }
    }
    
    private void loadLinkedEntityTo(OpChain chain)
    {
        for(File f : linkedEntityFiles)
        {
            final int kind = toInt(f.getName());
            try
            {
                linkedEntityCount[kind] += mergeLinkedEntityTo(chain, store, context, f, 
                        numeric, lb, 
                        registry.getEntityMetadata(kind), delegate, 
                        skipEntityValidation,
                        f64input);
            }
            catch (IOException e)
            {
                throw new LoadException("Failed on linked entity file: " + f, e);
            }
        }
    }
    
    private void loadLoadFilesTo(OpChain chain)
    {
        for(File f : loadFiles)
        {
            // reset
            Arrays.fill(keys, 0, keys.length, null);
            
            try
            {
                loadEntityCount += mergeAndInsertEntityTo(chain, store, context, f, 
                        numeric, lb, 
                        keys, registry, delegate, 
                        entityCount, linkedEntityCount,
                        f64input);
            }
            catch (IOException e)
            {
                throw new LoadException("Failed on load file: " + f, e);
            }
        }
    }

    public boolean handle(OpChain chain, byte[] key, byte[] value, int offset, int len, 
            Void param)
    {
        if(umtFiles != null && !umtFiles.isEmpty())
            loadUMTTo(chain);
        
        if(entityFiles != null && !entityFiles.isEmpty())
            loadEntityTo(chain);
        
        if(pkvFiles != null && !pkvFiles.isEmpty())
            loadPKVTo(chain);
        
        if(linkedEntityFiles != null && !linkedEntityFiles.isEmpty())
            loadLinkedEntityTo(chain);
        
        if (repFile.exists())
            loadRepFileTo(chain);
        
        if(footerFile.exists())
            loadFooterFileTo(chain);
        
        if(loadFiles != null && !loadFiles.isEmpty())
            loadLoadFilesTo(chain);
        
        return true;
    }
    
    static final Schema<OpChain> MERGE_AND_PUT_SCHEMA = new Schema<OpChain>()
    {
        // schema methods

        public OpChain newMessage()
        {
            throw new UnsupportedOperationException();
        }

        public Class<OpChain> typeClass()
        {
            return OpChain.class;
        }

        public String messageName()
        {
            return OpChain.class.getSimpleName();
        }

        public String messageFullName()
        {
            return OpChain.class.getName();
        }

        public boolean isInitialized(OpChain message)
        {
            return true;
        }

        public void mergeFrom(Input input, OpChain chain) throws IOException
        {
            byte[] key = null, value = null;
            for(int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch(number)
                {
                    case 0:
                        if(key == null || value == null)
                            throw new LoadException("Malformed json - key and value must be present.");
                        
                        // raw put
                        chain.rawPut(key, value);
                        return;
                    case 1:
                        if(key != null)
                            throw new LoadException("Malformed json - key already present.");
                        
                        key = input.readByteArray();
                        break;
                    case 2:
                        if(value != null)
                            throw new LoadException("Malformed json - value already present.");
                        
                        value = input.readByteArray();
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }

        public void writeTo(Output output, OpChain message) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public String getFieldName(int number)
        {
            return Integer.toString(number);
        }

        public int getFieldNumber(String name)
        {
            return Integer.parseInt(name);
        }
    };
    
}
