//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import io.airlift.command.Cli;
import io.airlift.command.ParseException;

import java.util.ArrayList;

/**
 * Cli utils.
 * 
 * @author David Yu
 * @created Feb 4, 2014
 */
public final class CliUtil
{
    private CliUtil() {}
    
    static final char CLI_SEPARATOR = parseSeparator(
            System.getProperty("cli.separator", ""));
    
    static char parseSeparator(String arg)
    {
        return 1 == arg.length() ? arg.charAt(0) : 0;
    }
    
    static char getSeparator(String arg)
    {
        return 1 == arg.length() && CLI_SEPARATOR == arg.charAt(0) ? CLI_SEPARATOR : 0;
    }
    
    static boolean isSeparator(String arg, char separator)
    {
        return 1 == arg.length() && separator == arg.charAt(0);
    }
    
    static String[] copy(String[] args, int offset, int len)
    {
        String[] copy = new String[len];
        System.arraycopy(args, offset, copy, 0, len);
        return copy;
    }
    
    public static void run(ArrayList<Runnable> list)
    {
        try
        {
            for(Runnable r : list)
                r.run();
        }
        catch(IllegalArgumentException e)
        {
            System.err.println(e.getMessage());
        }
        catch (ParseException e)
        {
            System.err.println(e.getMessage());
        }
    }
    
    public static ArrayList<Runnable> getRunnables(final String[] args, 
            final Cli<Runnable> git)
    {
        final ArrayList<Runnable> list = new ArrayList<Runnable>();
        char separator;
        if(0 != args.length && 0 != (separator = getSeparator(args[0])))
        {
            int start = 1;
            for(int offset = start, limit = args.length; offset < limit; offset++)
            {
                if(isSeparator(args[offset], separator))
                {
                    list.add(git.parse(copy(args, start, offset - start)));
                    start = offset + 1;
                }
            }
            
            if(!isSeparator(args[args.length - 1], separator))
            {
                // append the last entry
                list.add(git.parse(copy(args, start, args.length - start)));
            }
        }
        else
        {
            list.add(git.parse(args));
        }
        
        return list;
    }
}
