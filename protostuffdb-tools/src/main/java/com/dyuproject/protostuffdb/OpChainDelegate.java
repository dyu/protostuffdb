//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

/**
 * Responsbile for the actual persistence.
 * 
 * @author David Yu
 * @created Nov 19, 2012
 */
public interface OpChainDelegate
{
    
    public <T,P> void insert(OpChain chain, 
            Datastore store, WriteContext context, 
            byte[] key, 
            T message, EntityMetadata<T> em, byte[] parentKey,
            Fixed64Input input);
    
    public <T,P> void update(OpChain chain, 
            Datastore store, WriteContext context, 
            byte[] key, 
            T message, EntityMetadata<T> em, byte[] parentKey);
    
    
    public enum BUILTIN implements OpChainDelegate
    {
        DEFAULT
        {

            public <T,P> void insert(OpChain chain, 
                    Datastore store, WriteContext context, 
                    byte[] key, 
                    T message, EntityMetadata<T> em, byte[] parentKey, 
                    Fixed64Input input)
            {
                final byte[] insertKey = em.parent == null ? key : 
                    KeyUtil.newOneToManyKey(key, parentKey);
                
                if (em.updateTsField != 0)
                {
                    input.field = em.updateTsField;
                    try
                    {
                        em.pipeSchema.wrappedSchema.mergeFrom(input, message);
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
                
                // no indexing
                /*if(em.parent != null)
                {
                    // the child-parent link
                    if(chain == null)
                        store.rawPut(key, parentKey);
                    else
                        chain.rawPut(key, parentKey);
                }
                
                final int size = em.indexOnFields ? context.serAndIndexCollect(
                        message, em) : context.ser(message, em);*/
                
                final int size = context.ser(message, em);
                
                // copy because the entity buffer will get overritten on the first 
                // rawPut from DSOpUtil
                byte[] copy = ValueUtil.copy(context.entityBuffer, context.entityOffset, 
                        size);
                
                DSOpUtil.writeTo(chain, insertKey, em, store, context, 
                        false, 0, 
                        copy, 0, copy.length);
            }

            public <T,P> void update(OpChain chain, 
                    Datastore store, WriteContext context, 
                    byte[] key, 
                    T message, EntityMetadata<T> em, byte[] parentKey)
            {
                final byte[] updateKey = em.parent == null ? key : 
                    KeyUtil.newOneToManyKey(key, parentKey);
                
                // no indexing
                /*if(em.parent != null)
                {
                    // the child-parent link
                    if(chain == null)
                        store.rawPut(key, parentKey);
                    else
                        chain.rawPut(key, parentKey);
                }
                
                final int size = em.indexOnFields ? context.serAndIndexCollect(
                        message, em) : context.ser(message, em);*/
                
                final int size = context.ser(message, em);
                
                // copy because the entity buffer will get overritten on the first 
                // rawPut from DSOpUtil
                byte[] copy = ValueUtil.copy(context.entityBuffer, context.entityOffset, 
                        size);
                
                DSOpUtil.writeTo(chain, updateKey, em, store, context, 
                        false, 0, 
                        copy, 0, copy.length);
            }
            
        }
        ;
    }
    
}