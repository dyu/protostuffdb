//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.io.IOContext;

/**
 * Utils for accessing the package-private io classes/methods.
 * 
 * @author David Yu
 * @created Nov 13, 2012
 */
public final class ToolsIOUtil
{
    
    private ToolsIOUtil() {}
    
    /**
     * Write the message to the json output.
     */
    public static <T> void writeTo(JsonXOutput output, T message, Schema<T> schema)
    throws IOException
    {
        output.use(schema).reset();
        
        output.writeStartObject();
        
        schema.writeTo(output, message);
        
        if(output.isLastRepeated())
            output.writeEndArray();
        
        output.writeEndObject();
    }
    
    public static JsonInput newJsonInput(File file, boolean numeric, 
            LinkedBuffer lb) throws IOException
    {
        return newJsonInput(new FileInputStream(file), numeric, lb);
    }

    public static JsonInput newJsonInput(InputStream in, boolean numeric, 
            LinkedBuffer lb) throws IOException
    {
        final IOContext context = new IOContext(
                JsonIOUtil.DEFAULT_JSON_FACTORY._getBufferRecycler(), 
                in, true);
        final JsonParser parser = JsonIOUtil.newJsonParser(in, lb.buffer, 0, 0, 
                false, context);
        
        return new JsonInput(parser, numeric);
    }
}
