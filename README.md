# protostuffdb

A c++ datastore based on leveldb with:
- stored procedures via JNI
- http rpc
- cross-platform
- replication
- hot-backup
- real-time updates
- embbedable as a shared library for migration tools and testing

For more details and history, go to the [site](https://dyuproject.com/#protostuffdb)

## Java setup
Install java deps via https://sdkman.io/
```sh
# if windows, execute this via msys2 bash
sdk install java 8.0.422-librca
sdk install maven 3.6.3
```

On linux/mac, symlink the jdk dir to `/usr/local/lib/jvm/default`
On windows, link the jdk dir to `C:/jdk8u422`

## C++ Setup

Download [gn](http://refi64.com/gn-builds/) and add it to your exec PATH.
```
# print out your gn version (At the time of this writing, mine was 446079)
gn --version

# import the cross-platform build config/toolchain template
git clone --depth 1 --single-branch -b build-py3 https://github.com/dyu/gn-build.git build

# deps
git clone --depth 1 --single-branch -b build https://github.com/dyu/gn-deps.git

# execute this via msys2 bash if on windows
./gn-deps/fetch.sh psdb
```

### Build Dependencies
- gcc/g++ 9.4.x (or higher) on linux
- ninja 1.7.x (or higher)
- python 3.x.x (windows: choco install -y python3)
- msys2 on windows (choco install -y msys2)
- [vc build tools 2015](http://go.microsoft.com/fwlink/?LinkId=691126&fixForIE=.exe) on windows
  ```C:\Program Files (x86)\Microsoft Visual Studio 14.0``` is the install dir
- vcpkg on windows
  ```
  cd c:\
  mkdir vcpkg && cd vcpkg
  git clone --depth 1 --single-branch -b master https://github.com/Microsoft/vcpkg.git
  cd vcpkg
  powershell -exec bypass scripts\bootstrap.ps1
  .\vcpkg.exe install zlib:x64-windows-static
  .\vcpkg.exe install openssl:x64-windows-static
  ```

### Linux
```sh
sudo apt-get install autoconf libtool libssl-dev

gn gen gn-out --args='gcc_cc="gcc-9" gcc_cxx="g++-9" symbol_level=0 is_debug=false is_clang=false is_official_build=true'
```

### Mac
```sh
# Install xcode
#sudo rm -rf /Library/Developer/CommandLineTools
sudo xcode-select --install

gn gen gn-out --args='symbol_level=0 is_debug=false is_clang=true is_official_build=true'
```

### Windows
```sh
gn gen gn-out --args="visual_studio_path=\"C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\" visual_studio_version=\"2015\" symbol_level=0 is_debug=false is_clang=false is_official_build=true"
```

### Build
```sh
ninja -C gn-out
```

### Build
```sh
# on root project dir
mvn install
```
