//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import junit.framework.TestCase;

import com.dyuproject.protostuff.B64Code;
import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.StringSerializer.STRING;

/**
 * TODO
 *
 * @author David Yu
 * @created Feb 26, 2011
 */
public abstract class AbstractTest extends TestCase
{
    
    static final int BUF_SIZE = 256;
    
    protected int builtinKeys = 0;
    
    public static LinkedBuffer buf()
    {
        return LinkedBuffer.allocate(BUF_SIZE);
    }
    
    public static LinkedBuffer buf(int size)
    {
        return LinkedBuffer.allocate(size);
    }
    
    public static byte[] copy(byte[] data, int offset, int len)
    {
        byte[] copy = new byte[len];
        System.arraycopy(data, offset, copy, 0, len);
        return copy;
    }
    
    public static KeyBuilder newKB()
    {
        return new KeyBuilder(buf());
    }
    
    public static <T> ArrayList<T> newList()
    {
        return new ArrayList<T>();
    }
    
    public static <K,V> HashMap<K,V> newMap()
    {
        return new HashMap<K,V>();
    }
    
    public static void assertEquals(byte[] b1, byte[] b2)
    {
        assertTrue(Arrays.equals(b1, b2));
    }
    
    public static void assertNotEqual(byte[] b1, byte[] b2)
    {
        assertFalse(Arrays.equals(b1, b2));
    }
    
    public static void assertEquals(byte[] b1, byte[] b2, int offset, int len)
    {
        assertEquals(0, ValueUtil.compare(b1, 0, b1.length, b2, offset, len));
    }
    
    public static void assertEquals(byte[] b1, int b1offset, int b1len, 
            byte[] b2, int b2offset, int b2len)
    {
        assertEquals(0, ValueUtil.compare(b1, b1offset, b1len, b2, b2offset, b2len));
    }
    
    public static String str(byte[] data)
    {
        return STRING.deser(data);
    }
    
    public static byte[] data(byte[] data)
    {
        return data;
    }
    
    public static byte[] data(String value)
    {
        return ValueUtil.toBytes(value);
    }
    
    public static byte[] data(ByteString value)
    {
        return ValueUtil.toBytes(value);
    }
    
    public static byte[] data(boolean value)
    {
        return ValueUtil.toBytes(value);
    }
    
    public static byte[] data(int value)
    {
        return ValueUtil.toBytes(value);
    }
    
    public static byte[] data(long value)
    {
        return ValueUtil.toBytes(value);
    }
    
    public static byte[] data(float value)
    {
        return ValueUtil.toBytes(value);
    }
    
    public static byte[] data(double value)
    {
        return ValueUtil.toBytes(value);
    }
    
    /*public static byte[] data(byte[] dataWithRev)
    {
        byte[] data = new byte[dataWithRev.length-6];
        System.arraycopy(dataWithRev, 6, data, 0, dataWithRev.length-6);
        return data;
    }
    
    public static byte[] rev(byte[] dataWithRev)
    {
        byte[] data = new byte[6];
        System.arraycopy(dataWithRev, 0, data, 0, 6);
        return data;
    }*/
    
    public static String b64(byte[] data)
    {
        try
        {
            return new String(B64Code.encode(data), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    // store utils
    
    public static <T> ArrayList<HasKV> listKV(EntityMetadata<T> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listKV(em, -1, false, null, store, context);
    }
    
    public static <T> ArrayList<HasKV> listKV(EntityMetadata<T> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listKV(em, -1, false, parentKey, store, context);
    }
    
    public static <T> ArrayList<HasKV> listKV(EntityMetadata<T> em, 
            int limit, boolean desc, 
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listKV(em, limit, desc, parentKey, store, context);
    }
    
    public static <T> ArrayList<byte[]> listK(EntityMetadata<T> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listK(em, -1, false, null, store, context);
    }
    
    public static <T> ArrayList<byte[]> listK(EntityMetadata<T> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listK(em, -1, false, parentKey, store, context);
    }
    
    public static <T> ArrayList<byte[]> listK(EntityMetadata<T> em, 
            int limit, boolean desc,  
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listK(em, limit, desc, parentKey, store, context);
    }
    
    public static <T> ArrayList<byte[]> listV(EntityMetadata<T> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listV(em, -1, false, null, store, context);
    }
    
    public static <T> ArrayList<byte[]> listV(EntityMetadata<T> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listV(em, -1, false, parentKey, store, context);
    }
    
    public static <T> ArrayList<byte[]> listV(EntityMetadata<T> em, 
            int limit, boolean desc,  
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.listV(em, limit, desc, parentKey, store, context);
    }
    
    // kv
    
    public static <T> KV getKV(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        KV kv = new KV();
        kv.key = key;
        kv.value = store.get(key, em, parentKey, context);
        return kv;
    }
    
    public static <T> KV getKV(EntityMetadata<T> em, 
            boolean desc,  
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKV(em, desc, parentKey, store, context);
    }
    
    public static KV getKVFirst(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKVFirst(em, store, context);
    }
    
    public static KV getKVFirst(EntityMetadata<?> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKV(em, false, parentKey, store, context);
    }
    
    public static KV getKVLast(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKVLast(em, store, context);
    }
    
    public static KV getKVLast(EntityMetadata<?> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKV(em, true, parentKey, store, context);
    }
    
    // k
    
    public static byte[] getKFirst(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKFirst(em, store, context);
    }
    
    public static byte[] getKFirst(EntityMetadata<?> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return getKVFirst(em, parentKey, store, context).getKey();
    }
    
    public static byte[] getKLast(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getKLast(em, store, context);
    }
    
    public static byte[] getKLast(EntityMetadata<?> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return getKVLast(em, parentKey, store, context).getKey();
    }
    
    // v
    
    public static byte[] getVFirst(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getVFirst(em, store, context);
    }
    
    public static byte[] getVFirst(EntityMetadata<?> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return getKVFirst(em, parentKey, store, context).getValue();
    }
    
    public static byte[] getVLast(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return VisitorUtil.getVLast(em, store, context);
    }
    
    public static byte[] getVLast(EntityMetadata<?> em, byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        return getKVLast(em, parentKey, store, context).getValue();
    }
    
}
