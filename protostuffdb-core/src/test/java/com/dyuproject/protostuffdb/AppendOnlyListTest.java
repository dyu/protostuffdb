//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Arrays;

import com.dyuproject.protostuff.IntSerializer;

import junit.framework.TestCase;

/**
 * {@link ConcurrentAppendOnlyList} tests.
 * 
 * @author David Yu
 * @created Aug 23, 2014
 */
public class AppendOnlyListTest extends TestCase
{
    
    static final class Item implements HasKV
    {
        int id;
        int index;
        
        byte[] key, value;
        
        public Item(int id)
        {
            this.id = id;
            key = new byte[4];
            IntSerializer.writeInt32(id, key, 0);
            value = key;
        }

        public byte[] getKey()
        {
            return key;
        }

        public void setKey(byte[] key)
        {
            this.key = key;
        }

        public byte[] getValue()
        {
            return value;
        }

        public void setValue(byte[] value)
        {
            this.value = value;
        }

        public void setKV(byte[] key, byte[] value)
        {
            this.key = key;
            this.value = value;
        }
        
    };
    
    static final AbstractAppendOnlyList.Listener<Item> LISTENER = new AbstractAppendOnlyList.Listener<Item>()
    {
        public Item[] newArray(int size)
        {
            return new Item[size];
        }

        public void assignIndexTo(Item item, int index)
        {
            item.index = index;
        }
    };
    
    public void testAdd()
    {
        final int segmentSize = 8;
        doAdd(new ConcurrentAppendOnlyList<Item>(segmentSize, LISTENER));
        doAdd(new SerialAppendOnlyList<Item>(segmentSize, LISTENER));
    }

    static AbstractAppendOnlyList<Item> doAdd(final AbstractAppendOnlyList<Item> list)
    {
        final int len = list.segmentSize * 100;
        for(int i = 0; i < len; i++)
        {
            list.add(new Item(i + 1));
        }
        
        int count = 0;
        for(int i = 0; i < len; i++)
        {
            assertTrue(list.get(i).id == i + 1);
            count++;
        }
        
        assertTrue(len == count);
        
        /*int visited = list.visit(new Visitor<Item>()
        {
            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, Item param, int index)
            {
                return false;
            }
        });
        
        assertTrue(visited == len);*/
        
        AbstractAppendOnlyList.visit(list, new Visitor<Integer>()
        {

            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                    Integer param, int index)
            {
                assertTrue(Arrays.equals(
                        list.get(param).getValue(), ValueUtil.copy(v, voffset, vlen)));
                return false;
            }
        }, new Integer(count - 1), count - 1);
        
        return list;
    }
    
    public void testFill()
    {
        final int segmentSize = 8;
        doFill(new ConcurrentAppendOnlyList<Item>(segmentSize, LISTENER));
        doFill(new SerialAppendOnlyList<Item>(segmentSize, LISTENER));
    }
    
    static void doFill(AbstractAppendOnlyList<Item> list)
    {
        final int len = list.segmentSize * 100;
        for(int i = 0; i < len; i++)
        {
            list.fill(new Item(i + 1));
        }
        
        int count = 0;
        for(int i = 0; i < len; i++)
        {
            assertTrue(list.get(i).id == i + 1);
            count++;
        }
        
        assertTrue(len == count);
    }
    
    public void testClear()
    {
        final int segmentSize = 8;
        AbstractAppendOnlyList<Item> concurrent = 
                doAdd(new ConcurrentAppendOnlyList<Item>(segmentSize, LISTENER));
        
        concurrent.clear();
        assertEquals(0, concurrent.size());
        assertNull(concurrent.get(0));
        
        AbstractAppendOnlyList<Item> serial = 
                doAdd(new SerialAppendOnlyList<Item>(segmentSize, LISTENER));
        
        serial.clear();
        assertEquals(0, serial.size());
        assertNull(serial.get(0));
        
    }
}
