//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.SerializedValueUtil.asInt32;

import java.util.ArrayList;

import com.dyuproject.protostuff.IntSerializer;


/**
 * TODO
 * 
 * @author David Yu
 * @created Jul 10, 2017
 */
public class ArrayCacheTest extends AbstractTest
{
    static final int SEGMENT_SIZE = 8;
    
    static byte[] newValue(final long ts, final int id)
    {
        byte[] value = new byte[1 + 8 + 1 + 4];
        // ts field
        value[0] = 2;
        IntSerializer.writeInt64LE(ts, value, 1);
        // id field
        value[value.length - 4 - 1] = 3;
        IntSerializer.writeInt32LE(id, value, value.length - 4);
        return value;
    }

    SerialArrayCache verifySingle(int maxValueSize)
    {
        final int id = 1;
        final int voId = 4; // last field
        final long ts = System.currentTimeMillis();
        byte[] key = KeyUtil.newEntityKey(10, ts, 2048);
        byte[] value = newValue(ts, id);
        
        assertEquals(id, asInt32(voId, value));
        
        SerialArrayCache cache = new SerialArrayCache(voId, SEGMENT_SIZE, maxValueSize);
        KV kv = new KV();
        assertFalse(cache.exists(id));
        assertFalse(cache.get(id, kv));
        
        ArrayList<HasKV> list = new ArrayList<HasKV>();
        assertEquals(0, cache.visit(Visitor.APPEND_KV, list));
        assertEquals(0, list.size());
        
        // add
        assertEquals(0, cache.add(key, value));
        assertTrue(cache.exists(id));
        assertTrue(cache.get(id, kv));
        
        assertEquals(key, kv.key);
        assertEquals(value, kv.value);
        
        assertEquals(1, cache.visit(Visitor.APPEND_KV, list));
        assertEquals(1, list.size());
        
        HasKV hkv = list.get(0);
        assertEquals(key, hkv.getKey());
        assertEquals(value, hkv.getValue());
        
        return cache;
    }

    void verifyAnother(int maxValueSize)
    {
        SerialArrayCache cache = verifySingle(maxValueSize);
        
        final int id = 2;
        final int voId = 4; // last field
        final long ts = System.currentTimeMillis();
        byte[] key = KeyUtil.newEntityKey(10, ts, 2048);
        byte[] value = newValue(ts, id);
        
        assertEquals(id, asInt32(voId, value));
        
        KV kv = new KV();
        assertFalse(cache.get(id, kv));
        
        ArrayList<HasKV> list = new ArrayList<HasKV>();
        assertEquals(1, cache.visit(Visitor.APPEND_KV, list));
        assertEquals(1, list.size());
        list.clear();
        
        // add
        assertEquals(1, cache.add(key, value));
        assertTrue(cache.get(id, kv));
        
        assertEquals(key, kv.key);
        assertEquals(value, kv.value);
        
        assertEquals(2, cache.visit(Visitor.APPEND_KV, list));
        assertEquals(2, list.size());
        
        HasKV hkv = list.get(1);
        assertEquals(key, hkv.getKey());
        assertEquals(value, hkv.getValue());
    }
    
    public void testForceNewSegment()
    {
        final int voId = 4; // last field
        final long ts = System.currentTimeMillis();
        final byte[] key = KeyUtil.newEntityKey(10, ts, 2048);
        final int loops = 0xFFFF/newValue(ts, 0).length + 1;
        
        final SerialArrayCache cache = new SerialArrayCache(voId, loops - 1);
        for (int i = 0; i < loops; i++)
            assertEquals(i, cache.add(key, newValue(ts, i + 1)));
        
        assertEquals(loops, cache.size());
        final KV kv = new KV();
        for (int i = 0; i < loops; i++)
        {
            assertTrue(cache.get(i + 1, kv));
            assertEquals(key, kv.key);
            assertEquals(i + 1, asInt32(voId, kv.value));
        }
        
        final Visitor<Void> visitor = new Visitor<Void>()
        {
            @Override
            public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                    Void param, int index)
            {
                assertEquals(index + 1, asInt32(voId, v, voffset, vlen));
                return false;
            }
        };
        
        assertEquals(loops, cache.visit(visitor, null));
    }
    
    void verifyUpdate(int maxValueSize)
    {
        final int id = 1;
        SerialArrayCache cache = verifySingle(maxValueSize);
        byte[] value = newValue(0, id);
        cache.update(id, value);
        KV kv = new KV();
        assertTrue(cache.get(id, kv));
        
        assertEquals(0l, ValueUtil.toInt64LE(kv.value, 1));
    }

    public void testSingle()
    {
        verifySingle(0);
    }
    public void testAnother()
    {
        verifyAnother(0);
    }
    public void testUpdated()
    {
        verifyUpdate(0);
    }

    public void testFixedSingle()
    {
        verifySingle(32);
    }
    public void testFixedAnother()
    {
        verifyAnother(32);
    }
    public void testFixedUpdate()
    {
        verifyUpdate(32);
    }
}
