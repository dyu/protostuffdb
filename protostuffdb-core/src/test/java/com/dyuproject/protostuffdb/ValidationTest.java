//========================================================================
//Copyright 2023 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import junit.framework.TestCase;

/**
 * TODO
 *
 * @author David Yu
 * @created Dec 21, 2023
 */
public class ValidationTest  extends TestCase
{
    public void testEmail()
    {
        assertFalse(Validation.isValidEmail("foo@bar."));
        assertTrue(Validation.isValidEmail("foo@bar.c"));
        assertTrue(Validation.isValidEmail("foo@bar.co"));
    }
}
