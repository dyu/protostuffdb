//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.KvpTextByteArrayInput;
import com.dyuproject.protostuff.RpcHeader;

/**
 * TODO
 * 
 * @author David Yu
 * @created Oct 14, 2016
 */
public class KvpTextTest extends AbstractTest
{
    
    public RpcHeader parseHeader(String text) throws IOException
    {
        RpcHeader message = new RpcHeader();
        byte[] data = text.getBytes();
        KvpTextByteArrayInput input = new KvpTextByteArrayInput(data, 0, data.length);
        RpcHeader.getSchema().mergeFrom(input, message);
        return message;
    }

    public void testIt() throws Exception
    {
        RpcHeader simple = parseHeader("2:2,3:10,7:foo");
        assertEquals(2, simple.service);
        assertEquals(10, simple.method);
        assertEquals("foo".getBytes(), simple.authToken);
    }
    
    public void testEmpty() throws Exception
    {
        RpcHeader simple = parseHeader("2,3,7");
        assertEquals(1, simple.service);
        assertEquals(1, simple.method);
        assertEquals(new byte[]{}, simple.authToken);
    }
    
    public void testBytesWithTrailingEntry() throws Exception
    {
        RpcHeader simple = parseHeader("1:abc,2:2");
        assertEquals("abc".getBytes(), simple.data);
        assertEquals(2, simple.service);
    }
    
    public void testBytesWithTrailingEntryZero() throws Exception
    {
        RpcHeader simple = parseHeader("1:abc,0:bar");
        assertEquals("abc,0:bar".getBytes(), simple.data);
    }
    
    public void testDelim() throws Exception
    {
        RpcHeader simple = parseHeader("6.4:abcd,7:foo");
        assertEquals("abcd".getBytes(), simple.remoteAddress);
        assertEquals("foo".getBytes(), simple.authToken);
    }
    
    public void testDelimLast() throws Exception
    {
        RpcHeader simple = parseHeader("6.4:abcd");
        assertEquals("abcd".getBytes(), simple.remoteAddress);
    }
    
    public void testDelimEager() throws Exception
    {
        RpcHeader simple = parseHeader("6::abcd,7:foo");
        assertEquals("abcd,7:foo".getBytes(), simple.remoteAddress);
    }
    
    public void testDelimNonByteString() throws Exception
    {
        RpcHeader simple = parseHeader("2::2,3::10");
        assertEquals(2, simple.service);
        assertEquals(10, simple.method);
    }
    
}
