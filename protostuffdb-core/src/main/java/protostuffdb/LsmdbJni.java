//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package protostuffdb;

/**
 * Jni access to lsmdb.
 * 
 * @author David Yu
 * @created Dec 27, 2013
 */
public final class LsmdbJni
{
    /**
     * Should be called right after a successful load of the shared object library.
     */
    public static native void setup(int bao);
    
    /**
     * Writes a pointer to the new db instance.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - name (string, required)
     * </pre>
     */
    static native boolean newDb(byte[] data);
    
    
    /**
     * Deletes the instance.
     */
    static native boolean deleteDb(byte[] data);
    
    
    /**
     * Performs a backup of the database.
     */
    static native boolean backupDb(byte[] data);
    
    /**
     * Writes the contents of the database into another.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - ptrTo (int64)
     * </pre>
     */
    static native boolean pipeDb(byte[] data);
    
    
    /**
     * Put single data in the database.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - key (bytes)
     *   - value (bytes)
     * </pre>
     */
    static native boolean sput(byte[] data);
    
    
    /**
     * Put data in the database.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - kv entries
     * </pre>
     */
    static native boolean put(byte[] data);
    
    /**
     * Get single data from the database.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - key (bytes)
     * </pre>
     */
    static native boolean sget(byte[] data);
    
    /**
     * Get single data using the iterator from the database.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - key (bytes)
     * </pre>
     */
    static native boolean iget(byte[] data);
    
    /**
     * Get data from the database.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - k entries
     * </pre>
     */
    static native boolean get(byte[] data);
    
    /**
     * Scan the database.
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     *   - limit (int16)
     *   - limitKey (bytes)
     *   - startKey (bytes)
     * </pre>
     */
    static native boolean scan(byte[] data);
    
    /**
     * Close the iterator and the write batch (flushing/committing it to the db).
     * 
     * <pre>
     * Required params:
     *   - flags (int8)
     * </pre>
     */
    static native boolean close(byte[] data);
    
}
