package protostuffdb;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import com.dyuproject.protostuff.B32Code;
import com.dyuproject.protostuff.DSUtils;
import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuff.JniStream;
import com.dyuproject.protostuff.RpcCodec;
import com.dyuproject.protostuff.RpcConfig;
import com.dyuproject.protostuff.RpcError;
import com.dyuproject.protostuff.RpcHeader;
import com.dyuproject.protostuff.RpcLogin;
import com.dyuproject.protostuff.RpcProtocol;
import com.dyuproject.protostuff.RpcServer;
import com.dyuproject.protostuff.RpcService;
import com.dyuproject.protostuff.RpcServiceProvider;
import com.dyuproject.protostuff.RpcWorker;
import com.dyuproject.protostuff.WriteSession;
import com.dyuproject.protostuffdb.ValueUtil;
import com.dyuproject.protostuffdb.WriteContext;

public final class Jni
{
    public static final long STARTUP_NS = System.nanoTime();
    // max: 2, min: 1
    public static final int WRITERS = Math.max(Math.min(2, Integer.getInteger("protostuffdb.writers", 1)), 1);
    public static final int READERS = Math.max(Integer.getInteger("protostuffdb.readers", 0), 0);
    
    public static final int PARTITION_OFFSET = Math.max(Integer.getInteger("protostuffdb.poffset", 1), 1);
    public static final int PARTITION_SIZE = Math.max(Integer.getInteger("protostuffdb.psize", 2048), 2048);
    
    // limit: 8mb, default/min: 1mb
    public static final int MAX_RESPONSE_MB = Math.max(Math.min(8, Integer.getInteger("protostuffdb.max_response_mb", 1)), 1);
    
    private static String[] BGWORKERS = csv(System.getProperty("protostuffdb.bgworkers"));
    public static final int BGTHREADS = BGWORKERS.length;
    private static final Runnable[] BGRUNNABLES = new Runnable[BGTHREADS];
    
    public static final int THREADS = WRITERS + READERS + BGTHREADS;
    static final byte[][] RPC_BUFFERS = new byte[THREADS][0xFFFFF * MAX_RESPONSE_MB];
    static final byte[][] DB_BUFFERS = new byte[THREADS * 2][0xFFFF]; // 64K
    static final char[][] CHAR_BUFFERS = new char[THREADS][0xFFFB]; // 64K max message size granted there is only 1 string/btyes field and everything is ascii
    public static final RpcWorker[] WORKERS = new RpcWorker[THREADS];
    
    public static final boolean PRINT_STACK_TRACE = Boolean.getBoolean("protostuffdb.print_stack_trace");
    static final boolean WITH_CRYPTO = Boolean.getBoolean("protostuffdb.with_crypto");
    public static final boolean WITH_PUBSUB = Boolean.getBoolean("protostuffdb.with_pubsub");
    public static final boolean TOKEN_AS_USER = Boolean.getBoolean("es.token_as_user");
    
    private static final CountDownLatch START_LATCH = new CountDownLatch(1);
    
    public static final String SEQ_ERRMSG = 
            "Do not configure protostuffdb with more than 1 writer since some of your entities have sequence keys.";
    
    private static File BASE_DIR;
    private static Properties PROPS;
    
    public static File getBaseDir()
    {
        return BASE_DIR;
    }
    
    public static Properties getProps()
    {
        return PROPS;
    }
    
    private static String[] csv(String str)
    {
        return str == null || str.isEmpty() ? new String[0] : str.split(",");
    }
    
    static int offset(int id)
    {
        return id * 2;
    }
    
    public static char[] cbuf(int id)
    {
        return CHAR_BUFFERS[id];
    }
    
    public static byte[] buf(int id)
    {
        return RPC_BUFFERS[id];
    }
    
    public static byte[] bufDb(int id)
    {
        return DB_BUFFERS[offset(id)];
    }
    
    public static byte[] bufTmp(int id)
    {
        return DB_BUFFERS[offset(id) + 1];
    }
    
    public static RpcWorker worker(int id)
    {
        return WORKERS[id];
    }
    
    /* ================================================== */
    
    /**
     * Initializes the server.
     */
    static native boolean init(int writers, int readers, int bgworkers, int bao);
    
    /**
     * Initializes the worker thread.
     */
    static native boolean initThread(long ptrFn, long ptrArg, 
            byte[] bufRpc, byte[] bufDb, byte[] bufTmp);
    
    /**
     * Returns the encrypted size (via openssl).
     * 2-byte req/res header
     */
    static native void encrypt(long contextPtr);
    
    /**
     * Openssl decryption.
     * 2-byte req/res header
     */
    static native void decrypt(long contextPtr);
    
    /* ================================================== */
    // lsmdb ops
    
    /**
     * Writes a pointer to the new db instance.
     */
    static native void newDb(long ptr);
    
    /**
     * Deletes the instance.
     */
    static native void deleteDb(long ptr);
    
    /**
     * Performs a backup of the database.
     */
    static native void backupDb(long ptr);
    
    /**
     * Writes the contents of the database into another.
     */
    static native void pipeDb(long ptr);
    
    /**
     * Put single data in the database.
     */
    static native void sput(long ptr);
    
    /**
     * Put data in the database.
     */
    static native void put(long ptr);
    
    /**
     * Get single data from the database.
     */
    static native void sget(long ptr);
    
    /**
     * Get single data using the iterator from the database.
     */
    static native void iget(long ptr);
    
    /**
     * Get data from the database.
     */
    static native void get(long ptr);
    
    /**
     * Scan the database.
     */
    static native void scan(long ptr);
    
    /**
     * Close the iterator and the write batch (flushing/committing it to the db).
     */
    static native void close(long ptr);
    
    /* ================================================== */
    
    private static RpcServer server;
    
    public static byte[] decryptData(byte[] v, int voffset, int vlen, RpcWorker worker)
    {
        final long ptr = worker.getPtr();
        if (ptr == 0)
            return null;
        
        final WriteContext context = worker.context;
        final byte[] buf = context.bufTmp;
        buf[0] = (byte)(vlen & 0xFF);
        buf[1] = (byte)((vlen >>>  8) & 0xFF);
        System.arraycopy(v, voffset, buf, 2, vlen);
        
        decrypt(ptr);
        
        int offset = 0,
                resultSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8),
                resultOffset = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
        
        if (resultSize == 0)
            return null;
        
        context.$offset = resultOffset;
        context.$len = resultSize;
        return buf;
    }
    
    public static byte[] encryptData(byte[] v, int voffset, int vlen, RpcWorker worker)
    {
        final long ptr = worker.getPtr();
        if (ptr == 0)
            return null;
        
        final WriteContext context = worker.context;
        final byte[] buf = context.bufTmp;
        buf[0] = (byte)(vlen & 0xFF);
        buf[1] = (byte)((vlen >>>  8) & 0xFF);
        System.arraycopy(v, voffset, buf, 2, vlen);
        
        encrypt(ptr);
        
        int offset = 0,
                resultSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8),
                resultOffset = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
        
        if (resultSize == 0)
            return null;
        
        context.$offset = resultOffset;
        context.$len = resultSize;
        return buf;
    }
    
    private static boolean verifyEncryption(RpcWorker worker, long ptr)
    {
        final WriteContext context = worker.context;
        final byte[] buf = context.bufTmp;
        
        int size = EMPTY_BODY.length;
        buf[0] = (byte)(size & 0xFF);
        buf[1] = (byte)((size >>>  8) & 0xFF);
        System.arraycopy(EMPTY_BODY, 0, buf, 2, size);
        
        encrypt(ptr);
        
        int offset = 0,
                resultSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8),
                resultOffset = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
        
        if (resultSize == 0)
            return false;
        
        byte[] encrypted = new byte[resultSize];
        System.arraycopy(buf, resultOffset, encrypted, 0, resultSize);
        
        size = encrypted.length;
        buf[0] = (byte)(size & 0xFF);
        buf[1] = (byte)((size >>>  8) & 0xFF);
        System.arraycopy(encrypted, 0, buf, 2, size);
        
        decrypt(ptr);
        
        offset = 0;
        resultSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
        resultOffset = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
        
        return resultSize != 0 && ValueUtil.isEqual(EMPTY_BODY, 
                buf, resultOffset, resultSize);
    }
    
    static void setupThread(final int type, final int id, long ptr)
    {
        RpcWorker worker = worker(id);
        worker.init(ptr);
        if (type == 0 && id == 0)
        {
            long startNs = System.nanoTime(), endNs;
            try
            {
                RpcConfig.init(server, worker.context);
                if (BGTHREADS != 0)
                    initRunnables();
            }
            catch (Throwable e)
            {
                System.err.println(e.getMessage());
                System.exit(1);
            }
            if (WITH_CRYPTO && !verifyEncryption(worker, ptr))
            {
                System.err.println("Could not verify encrypt/decrypt");
                System.exit(1);
            }
            
            endNs = System.nanoTime();
            
            START_LATCH.countDown();
            
            System.out.println("jni rpc: " + 
                    ((endNs - startNs) / 1000000) + " ms | total: " + 
                    (((endNs - STARTUP_NS) / 1000000)) + " ms");
            
            return;
        }
        
        try
        {
            START_LATCH.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        
        int bgStart = WRITERS + READERS;
        if (id >= bgStart)
        {
            // convert to idx
            BGRUNNABLES[id - bgStart].run();
        }
    }
    
    static void createThread(final long ptrFn, final long ptrArg, 
            final int type, final int id)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final int offset = offset(id),
                        start = type == 0 ? ((id + PARTITION_OFFSET) * PARTITION_SIZE) : PARTITION_SIZE;
                
                final byte[] buf = RPC_BUFFERS[offset],
                        bufDb = DB_BUFFERS[offset],
                        bufTmp = DB_BUFFERS[offset + 1];
                
                final RpcWorker worker = new RpcWorker(id, buf, CHAR_BUFFERS[offset], server, 
                        new WriteContext(bufDb, bufTmp, start, start + PARTITION_SIZE));
                
                WORKERS[id] = worker;
                
                RpcWorker.set(worker);
                
                if (!initThread(ptrFn, ptrArg, buf, bufDb, bufTmp))
                    System.err.println("Could not start thread " + id + " of type: " + type);
            }
        }).start();
    }
    
    static final byte[] ERR_INTERNAL_ERROR = "Internal error.".getBytes();
    static final byte[] ERR_INVALID_LOGIN = "Invalid login.".getBytes();
    
    static final byte[] RES_UNAUTHORIZED = "[3,{\"1\":\"Unauthorized.\"}]".getBytes();
    static final byte[] RES_LOGIN_FAILED = "[1,{\"1\":\"Login failed.\"}]".getBytes();
    static final byte[] RES_INVALID_REQUEST = "[1,{\"1\":\"Invalid request.\"}]".getBytes();
    static final byte[] RES_BUFFER_OVERFLOW = "[3,{\"1\":\"Buffer overflow.\"}]".getBytes();
    
    static boolean writeData(byte[] buf, byte[] data)
    {
        final int size = data.length;
        IntSerializer.writeInt32LE(size, buf, 0);
        System.arraycopy(data, 0, buf, 4, size);
        return false;
    }
    
    static final byte[] QUOTE_COMMA_QUOTE = "\",\"".getBytes();
    static boolean handleLogin(RpcWorker worker, RpcHeader header, 
            byte[] buf, int offset, int payloadSize) throws IOException
    {
        // username
        if (header.authToken == null)
            return writeData(buf, ERR_INTERNAL_ERROR);
        
        // auth
        final byte[] password = new byte[payloadSize];
        System.arraycopy(buf, offset, password, 0, payloadSize);
        
        final RpcLogin login = worker.login;
        login.username = header.authToken;
        login.password = password;
        login.remoteAddress = header.remoteAddress;
        
        final byte[] token = worker.server.providers[0].authenticate(login, 
                worker.server.stores[0], worker.context);
        
        if (token == null)
            return writeData(buf, RES_LOGIN_FAILED);
        
        writeData(buf, token);
        encrypt(worker.getPtr());
        
        int off = 0,
                resultSize = ((buf[off++] & 0xFF) | (buf[off++] & 0xFF) << 8),
                resultOffset = ((buf[off++] & 0xFF) | (buf[off++] & 0xFF) << 8);
        
        if (resultSize == 0)
            return writeData(buf, ERR_INTERNAL_ERROR);
        
        byte[] tokenEncryptedB32 = B32Code.encode(buf, resultOffset, resultSize, false);
        final WriteSession session = worker.session;
        session.sink.writeByte((byte)']', session, 
                session.sink.writeByte((byte)'"', session, 
                        session.sink.writeByteArray(tokenEncryptedB32, session, 
                                session.sink.writeByteArray(QUOTE_COMMA_QUOTE, session, 
                                        session.sink.writeByteArrayB64(token, session, 
                                                session.sink.writeByte((byte)'"', session, session.head))))));
        
        final int size = 3 + session.getSize();
        IntSerializer.writeInt32LE(size, buf, 0);
        buf[4] = '[';
        buf[5] = '0';
        buf[6] = ',';
        return true;
    }
    
    static final byte[] ERR_MISSING_PAYLOAD = "Missing payload".getBytes();
    static final byte[] ERR_FF_ID = "Invalid fastfunc id".getBytes();
    
    static final byte[] EMPTY_BODY = "[0,0]".getBytes();
    
    static boolean finish(byte[] buf, WriteSession current, 
            boolean writePrefix) throws IOException
    {
        if (!DSUtils.writeTip(current, (byte)']'))
            return writeData(buf, RES_BUFFER_OVERFLOW);
        
        final int size = 3 + current.getSize();
        IntSerializer.writeInt32LE(size, buf, 0);
        if (writePrefix)
        {
            buf[4] = '[';
            buf[5] = '0';
            buf[6] = ',';
        }
        
        return true;
    }
    
    static boolean handleBackup(RpcWorker worker,
            byte[] buf, int offset, int payloadSize) throws IOException
    {
        if (payloadSize == 0)
            return writeData(buf, ERR_MISSING_PAYLOAD);
        
        final WriteSession session = worker.session;
        
        final String name = new String(buf, offset, payloadSize),
                backupName = worker.backup(name);
        
        final StringBuilder sb = new StringBuilder();
        if (backupName != null)
            sb.append("\"Backup completed: ").append(name).append(" @ ").append(backupName).append('"');
        else
            sb.append("\"Backup failed: ").append(name).append('"');
        
        session.sink.writeStrAscii(sb.toString(), session, session.head);
        
        return finish(buf, session, true);
    }
    
    static void handleLogUpdates(RpcWorker worker,
            byte[] buf, int offset, int payloadSize) throws IOException
    {
        final int idx = buf[offset] & 0xFF;
        final RpcServiceProvider[] providers = worker.server.providers;
        final RpcServiceProvider provider = idx < providers.length ? providers[idx] : null;
        if (provider == null)
            return;
        
        final WriteContext context = worker.context;
        final byte[] bufOut = context.bufTmp;
        int size = -1;
        boolean overflow;
        try
        {
            provider.handleLogUpdates(worker, buf, offset + 1, payloadSize - 1);
            size = context.pubOutput.getSize();
        }
        catch (IOException e)
        {
            // ignore
        }
        finally
        {
            if (size > 0)
            {
                overflow = DSUtils.isBufferOverflow(context.pubOutput);
                
                context.pubOutput.clear().reset();
                
                if (overflow)
                {
                    System.err.println("Could not publish due to buffer overflow.");
                }
                else
                {
                    IntSerializer.writeInt32LE(size, bufOut, 0);
                }
            }
            else if (size == -1 && 0 != context.pubOutput.getSize())
            {
                // exception happened after write started
                context.pubOutput.clear().reset();
            }
        }
    }
    
    static void handlePkvSync(RpcWorker worker,
            byte[] buf, int offset, int payloadSize) throws IOException
    {
        final int idx = buf[offset++] & 0xFF;
        final RpcServiceProvider[] providers = worker.server.providers;
        final RpcServiceProvider provider = idx < providers.length ? providers[idx] : null;
        if (provider == null)
            return;
        
        final WriteContext context = worker.context;
        final byte[] bufOut = context.bufTmp;
        int size = 0,
                keySize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8),
                valSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
        boolean ok = false, overflow;
        try
        {
            if (provider.handlePkvSync(worker,
                    buf, offset, keySize,
                    buf, offset + keySize, valSize))
            {
                ok = true;
                size = context.pubOutput.getSize();
            }
        }
        catch (IOException e)
        {
            // ignore
        }
        finally
        {
            if (!ok)
            {
                if (0 != context.pubOutput.getSize())
                {
                    // exception happened after write started
                    context.pubOutput.clear().reset();
                }
            }
            else if (size == 0)
            {
                // signal ok
                bufOut[0] = (byte)1;
                bufOut[1] = (byte)0;
                bufOut[2] = (byte)0;
                bufOut[3] = (byte)0;
            }
            else
            {
                overflow = DSUtils.isBufferOverflow(context.pubOutput);
                
                context.pubOutput.clear().reset();
                
                if (overflow)
                {
                    bufOut[0] = (byte)1;
                    bufOut[1] = (byte)0;
                    bufOut[2] = (byte)0;
                    bufOut[3] = (byte)0;
                    System.err.println("Could not publish due to buffer overflow.");
                }
                else
                {
                    IntSerializer.writeInt32LE(size, bufOut, 0);
                }
            }
        }
    }
    
    static boolean handleFastFunc(RpcWorker worker, RpcHeader header, 
            byte[] buf, int offset, int payloadSize) throws IOException
    {
        switch (header.method)
        {
            case 1:
                return handleBackup(worker, buf, offset, payloadSize);
            case 2:
                handleLogUpdates(worker, buf, offset, payloadSize);
                return true;
            case 3:
                handlePkvSync(worker, buf, offset, payloadSize);
                return true;
            default:
                return writeData(buf, EMPTY_BODY);
        }
    }
    
    static final byte[] ERR_INVALID_INPUT_CODEC = "Invalid input codec.".getBytes();
    static final byte[] ERR_INVALID_OUTPUT_CODEC = "Invalid output codec.".getBytes();
    static final byte[] ERR_SERVICE_NOT_FOUND = "Service not found.".getBytes();
    
    static boolean handleService(RpcWorker worker, RpcHeader header, 
            byte[] buf, int offset, int payloadSize) throws IOException
    {
        final RpcCodec[] codecs = worker.jniCodecs;
        final RpcCodec inCodec;
        if (header.inCodec < 0 || header.inCodec >= codecs.length || 
                !(inCodec = codecs[header.inCodec]).readable)
        {
            return writeData(buf, ERR_INVALID_INPUT_CODEC);
        }
        
        if (header.outCodec < 0 || header.outCodec >= codecs.length)
            return writeData(buf, ERR_INVALID_OUTPUT_CODEC);
        
        
        final RpcService service = 
                header.service < 0 || header.service > worker.server.services.length ? 
                        null : worker.server.services[header.service];
        
        if (service == null)
            return writeData(buf, ERR_SERVICE_NOT_FOUND);
        
        if (!DSUtils.isAuthorized(service, header))
            return writeData(buf, RES_UNAUTHORIZED);
        
        final RpcCodec outCodec = codecs[header.outCodec];
        
        final WriteSession session = worker.session;
        final boolean ok;
        try
        {
            ok = RpcError.NONE == service.dispatch(worker.header, 
                    null, buf, offset, offset + payloadSize, inCodec, 
                    RpcProtocol.DEFAULT, outCodec, worker, worker.session, 
                    worker.server.stores[service.id], worker.context);
        }
        catch (NumberFormatException e)
        {
            return writeData(buf, RES_INVALID_REQUEST);
        }
        
        WriteSession current = worker.currentOutput;
        return finish(buf, current == null ? session : current, ok);
    }
    
    static void handle(int type, int id)
    {
        final RpcWorker worker = worker(id);
        final byte[] buf = worker.buf;
        int offset = 0,
                headerSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8),
                payloadSize = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8),
                headerEnd = offset + headerSize;
        
        //System.err.println("h: " + headerSize + " | p: " + payloadSize);
        
        final RpcHeader header = worker.header;
        try
        {
            RpcHeader.getSchema().mergeFrom(
                    worker.kvpInput.reset(offset, headerEnd), header);
            
            if (header.service != 0)
                handleService(worker, header, buf, headerEnd, payloadSize);
            else if (header.method != 0)
                handleFastFunc(worker, header, buf, headerEnd, payloadSize);
            else
                handleLogin(worker, header, buf, headerEnd, payloadSize);
        }
        catch (Exception e)
        {
            writeData(buf, ERR_INTERNAL_ERROR);
            if (PRINT_STACK_TRACE)
                e.printStackTrace();
        }
        finally
        {
            DSUtils.clear(worker);
        }
    }
    
    public static void main(String[] args) throws Exception
    {
        if (args.length == 0)
        {
            System.err.println("Must provide baseDir as the only arg.");
            return;
        }
        
        final String path = args[0];
        File baseDir = new File(path);
        if (!baseDir.exists() || !baseDir.isDirectory())
        {
            System.err.println(path + " must be an existing directory.");
            return;
        }
        
        File file = new File(baseDir, "run.properties");
        if (!file.exists())
        {
            System.err.println(file + " not found.");
            return;
        }
        
        // overrides the workers from props if provided
        Properties props = new Properties();
        BufferedInputStream bin = new BufferedInputStream(new FileInputStream(file));
        try
        {
            props.load(bin);
        }
        finally
        {
            bin.close();
        }
        
        BASE_DIR = baseDir;
        PROPS = props;
        server = RpcConfig.getServerFrom(props, baseDir, null);
        init(WRITERS, READERS, BGTHREADS, JniStream.BYTE_ARRAY_OFFSET);
    }
    
    private static void initRunnables()
    {
        Class<?> clazz;
        for (int i = 0, bgstart = WRITERS + READERS; i < BGTHREADS; i++)
        {
            String fqcn = BGWORKERS[i];
            if (i == 0 || 1 != fqcn.length())
                clazz = RpcConfig.loadClass(fqcn);
            else
                clazz = BGRUNNABLES[i - 1].getClass();
            
            BGRUNNABLES[i] = newRunnable(clazz, bgstart + i, server);
        }
    }
    
    private static Runnable newRunnable(Class<?> clazz, int id, RpcServer server)
    {
        Constructor<?> c;
        try
        {
            c = (Constructor<?>)clazz.getConstructor(int.class, RpcServer.class);
            return (Runnable)c.newInstance(id, server);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    /*public static void main(String[] args)
    {
        for (MemoryPoolMXBean memoryPoolMXBean: ManagementFactory.getMemoryPoolMXBeans())
        {
            System.err.println(memoryPoolMXBean.getName());
            System.err.println(memoryPoolMXBean.getUsage().getUsed());
        }
        
        System.err.println("protostuffdb.Jni main - w: " + WRITERS + ", r: " + READERS);
        init(WRITERS, READERS);
    }*/

    private static String getLibSuffix(String osName)
    {
        return osName.startsWith("mac") || -1 != osName.indexOf("darwin") ? "dylib" : "so";
    }

    public static void loadNativeLibrary()
    {
        String osName = System.getProperty("os.name", "").toLowerCase();
        if (osName.startsWith("windows"))
        {
            System.load("C:/protostuffdb/lib/protostuffdbjni.dll");
        }
        else
        {
            System.load("/opt/protostuffdb/lib/libprotostuffdbjni." +
                    getLibSuffix(osName));
        }
    }
}
