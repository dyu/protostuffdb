//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package protostuffdb;

import com.dyuproject.protostuff.JniOp;
import com.dyuproject.protostuff.JniStream;

/**
 * Jni ops.
 * 
 * @author David Yu
 * @created Mar 10, 2017
 */
public enum JniOps implements JniOp
{
    NEW_DB
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.newDb(data);
            
            Jni.newDb(ptr);
            return 0 == data[0];
        }
    },
    DELETE_DB
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.deleteDb(data);
            
            Jni.deleteDb(ptr);
            return 0 == data[0];
        }
    },
    BACKUP_DB
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.backupDb(data);
            
            Jni.backupDb(ptr);
            return 0 == data[0];
        }
    },
    PIPE_DB
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.pipeDb(data);
            
            Jni.pipeDb(ptr);
            return 0 == data[0];
        }
    },
    CLOSE
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.close(data);
            
            Jni.close(ptr);
            return 0 == data[0];
        }
    },
    SPUT 
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.sput(data);
            
            Jni.sput(ptr);
            return 0 == data[0];
        }
    },
    PUT 
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.put(data);
            
            Jni.put(ptr);
            return 0 == data[0];
        }
    },
    SGET
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.sget(data);
            
            Jni.sget(ptr);
            return 0 == data[0];
        }
    },
    IGET
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.iget(data);
            
            Jni.iget(ptr);
            return 0 == data[0];
        }
    },
    GET
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.get(data);
            
            Jni.get(ptr);
            return 0 == data[0];
        }
    },
    SCAN
    {
        public boolean exec(byte[] data, long ptr)
        {
            if (ptr == 0)
                return LsmdbJni.scan(data);
            
            Jni.scan(ptr);
            return 0 == data[0];
        }
    };
    
    /* ================================================== */
    // pointer offsets
    
    public static final int PTR_DB = JniStream.OFFSET_PTRS,
            PTR_WB = PTR_DB + 8,
            PTR_IT = PTR_DB + 16;
    
    /* ================================================== */
    // error types
    
    public static final int ERR_MISSING_DB                    = 20;
    public static final int ERR_MISSING_WRITEBATCH            = 21;
    public static final int ERR_MISSING_ITERATOR              = 22;
    
    public static final int ERR_WRITEBATCH_FLUSH              = 23;
    
    public static final int ERR_BUF_TMP_NOT_AVAILABLE         = 24;
    public static final int ERR_INVALID_VALUE_AS_KEY          = 25;
    
    /* ================================================== */
    // newDb flags
    
    public static final int FNEWDB_CREATE_IF_MISSING          = 1;
    public static final int FNEWDB_ERROR_IF_EXISTS            = 1 << 1;
    public static final int FNEWDB_PARANOID_CHECKS            = 1 << 2;
    
    /* ================================================== */
    // common op flags
    
    public static final int FOP_CLOSE                         = 1 << 6;
    public static final int FOP_CLOSE_SYNC                    = 1 << 7; // sync
    
    /* ================================================== */
    // close flags
    
    public static final int FCLOSE_CLEANUP_ONLY               = 1;
    
    /* ================================================== */
    // put flags
    
    public static final int FPUT_SYNC                         = 1 << 7; // sync
    
    /* ================================================== */
    // get flags
    
    public static final int FGET_CACHE                        = 1 << 1;
    public static final int FGET_KEY_ONLY                     = 1 << 2;
    public static final int FGET_PREFIX                       = 1 << 5;
    
    /* ================================================== */
    // scan flags
    
    // the value of the scanned entry will be treated as the key with its value retrieved.
    // this flag requires a start key to be activated
    public static final int FSCAN_VALUE_AS_KEY                = 1;
    public static final int FSCAN_CACHE                       = 1 << 1;
    public static final int FSCAN_KEYS_ONLY                   = 1 << 2;
    public static final int FSCAN_DESC                        = 1 << 3;
    public static final int FSCAN_EXCLUDE_START_KEY           = 1 << 4;
    public static final int FSCAN_RELATIVE_LIMIT              = 1 << 5;
    public static final int FSCAN_DELETE_RESULT               = 1 << 6;
    public static final int FSCAN_DELETE_RESULT_SYNC          = 1 << 7; // sync
}