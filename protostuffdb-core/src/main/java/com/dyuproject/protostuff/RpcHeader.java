//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

/**
 * <pre>
 * message RpcHeader {
 *   optional bytes data = 1;
 *   optional uint32 service = 2;
 *   optional uint32 method = 3;
 *   optional uint32 in_codec = 4;
 *   optional uint32 out_codec = 5;
 *   optional bytes remote_address = 6;
 *   optional bytes auth_token = 7;
 * }
 * </pre>
 */
public final class RpcHeader implements Message<RpcHeader>
{

    public static Schema<RpcHeader> getSchema()
    {
        return SCHEMA;
    }
    
    public byte[] data; //1
    public int service; //2
    public int method; //3
    public int inCodec; //4
    public int outCodec; //5
    
    public byte[] remoteAddress; //6
    public byte[] authToken; //7
    
    // hack
    RpcResponse res;
    
    public RpcHeader()
    {
        
    }
    
    RpcHeader reset()
    {
        // ff data
        data = null;
        
        service = 0;
        method = 0;
        inCodec = 0;
        outCodec = 0;
        
        remoteAddress = null;
        authToken = null;
        
        res = null;
        
        return this;
    }
    
    // message method

    public Schema<RpcHeader> cachedSchema()
    {
        return SCHEMA;
    }

    static final Schema<RpcHeader> SCHEMA = new Schema<RpcHeader>()
    {
        // schema methods

        public RpcHeader newMessage()
        {
            return new RpcHeader();
        }

        public Class<RpcHeader> typeClass()
        {
            return RpcHeader.class;
        }

        public String messageName()
        {
            return RpcHeader.class.getSimpleName();
        }

        public String messageFullName()
        {
            return RpcHeader.class.getName();
        }

        public boolean isInitialized(RpcHeader message)
        {
            return true;
        }

        public void mergeFrom(Input input, RpcHeader message) throws IOException
        {
            for(int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch(number)
                {
                    case 0:
                        return;
                    case 1:
                        message.data = input.readByteArray();
                        break;
                    case 2:
                        message.service = input.readUInt32();
                        break;
                    case 3:
                        message.method = input.readUInt32();
                        break;
                    case 4:
                        message.inCodec = input.readUInt32();
                        break;
                    case 5:
                        message.outCodec = input.readUInt32();
                        break;
                    case 6:
                        message.remoteAddress = input.readByteArray();
                        break;
                    case 7:
                        message.authToken = input.readByteArray();
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }


        public void writeTo(Output output, RpcHeader message) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public String getFieldName(int number)
        {
            return Integer.toString(number);
        }

        public int getFieldNumber(String name)
        {
            return Integer.parseInt(name);
        }
    };
}
