//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import static com.dyuproject.protostuffdb.Validation.isValidCreditCardNumber;
import static com.dyuproject.protostuffdb.Validation.isValidDate;
import static com.dyuproject.protostuffdb.Validation.isValidDecimalMax;
import static com.dyuproject.protostuffdb.Validation.isValidDecimalMin;
import static com.dyuproject.protostuffdb.Validation.isValidDigits;
import static com.dyuproject.protostuffdb.Validation.isValidEmail;
import static com.dyuproject.protostuffdb.Validation.isValidFuture;
import static com.dyuproject.protostuffdb.Validation.isValidFutureOrToday;
import static com.dyuproject.protostuffdb.Validation.isValidFutureTS;
import static com.dyuproject.protostuffdb.Validation.isValidGT;
import static com.dyuproject.protostuffdb.Validation.isValidLength;
import static com.dyuproject.protostuffdb.Validation.isValidLuhn;
import static com.dyuproject.protostuffdb.Validation.isValidMax;
import static com.dyuproject.protostuffdb.Validation.isValidMin;
import static com.dyuproject.protostuffdb.Validation.isValidNotBlank;
import static com.dyuproject.protostuffdb.Validation.isValidNotEmpty;
import static com.dyuproject.protostuffdb.Validation.isValidNotNull;
import static com.dyuproject.protostuffdb.Validation.isValidNotNullNotEmpty;
import static com.dyuproject.protostuffdb.Validation.isValidNull;
import static com.dyuproject.protostuffdb.Validation.isValidPast;
import static com.dyuproject.protostuffdb.Validation.isValidPastOrToday;
import static com.dyuproject.protostuffdb.Validation.isValidPastTS;
import static com.dyuproject.protostuffdb.Validation.isValidPattern;
import static com.dyuproject.protostuffdb.Validation.isValidRange;
import static com.dyuproject.protostuffdb.Validation.isValidSafeHtml;
import static com.dyuproject.protostuffdb.Validation.isValidScriptAssert;
import static com.dyuproject.protostuffdb.Validation.isValidSize;
import static com.dyuproject.protostuffdb.Validation.isValidTime;
import static com.dyuproject.protostuffdb.Validation.isValidURL;
import static com.dyuproject.protostuffdb.Validation.isValidUniqueIds;
import static com.dyuproject.protostuffdb.Validation.isValidUniqueKeys;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuffdb.HasId;
import com.dyuproject.protostuffdb.HasIds;
import com.dyuproject.protostuffdb.HasKey;
import com.dyuproject.protostuffdb.HasKeys;
import com.dyuproject.protostuffdb.HasParentKey;
import com.dyuproject.protostuffdb.KeyUtil;
import com.dyuproject.protostuffdb.Validation;

/**
 * FFValidation
 *
 * @author David Yu
 * @created Jul 19, 2011
 */
public final class RpcValidation
{
    
    public static final boolean FAIL_FAST = Boolean.parseBoolean(
            System.getProperty("dsrpc.validation.fail_fast", "true"));
    
    private static final HashMap<String,Pattern> __uniqueRegExps = 
        new HashMap<String,Pattern>();
    
    static final ThreadLocal<ArrayList<String>> localV = 
        new ThreadLocal<ArrayList<String>>()
    {
        protected ArrayList<String> initialValue()
        {
            return new ArrayList<String>();
        }
    };
    
    public static String msg(String msg)
    {
        // return raw msg to have same gwt behavior.
        return msg;
        //return ByteString.stringDefaultValue(msg);
        /*for(int i = 0, len = msg.length(); i < len; i++)
        {
            if(msg.charAt(i) > 0x7f)
                return ByteString.stringDefaultValue(msg);
        }
        
        return msg;*/
    }
    
    public static synchronized Pattern pattern(String regex)
    {
        regex = msg(regex);
        
        // TODO synchronize
        Pattern p = __uniqueRegExps.get(regex);
        if(p == null)
        {
            __uniqueRegExps.put(regex, (p = Pattern.compile(regex)));
        }
        
        return p;
    }
    
    private static boolean addMessage(String msg)
    {
        if(FAIL_FAST)
            throw RpcRuntimeExceptions.validation(msg);
        
        localV.get().add(msg);
        
        return false;
    }
    
    public static ArrayList<String> getMessages()
    {
        return localV.get();
    }
    
    /**
     * Returns true if there are error messsages on the current validation context.
     */
    public static boolean hasErrors()
    {
        return !getMessages().isEmpty();
    }
    
    /* ================================================== */
    // utility for custom/complex validation
    
    public static boolean validIf(boolean expression)
    {
        return validIf(expression, RpcProtocol.INVALID_REQUEST);
    }
    
    public static boolean validIf(boolean expression, String message)
    {
        return expression || addMessage(message);
    }
    
    public static boolean invalidIf(boolean expression)
    {
        return invalidIf(expression, RpcProtocol.INVALID_REQUEST);
    }
    
    public static boolean invalidIf(boolean expression, String message)
    {
        return !expression || addMessage(message);
    }
    
    /* ================================================== */
    
    public static boolean verifyAsciiOnly(String message, String value)
    {
        return Validation.isAsciiOnly(value) || addMessage(message);
    }
    
    public static boolean verifyAsciiOnly(String message, Collection<String> value)
    {
        for (String s : value)
        {
            if (!Validation.isAsciiOnly(s))
                return addMessage(message);
        }
        return true;
    }
    
    public static boolean verifyAsciiPrintable(String message, String value)
    {
        return Validation.isAsciiPrintable(value) || addMessage(message);
    }
    
    public static boolean verifyAsciiPrintable(String message, Collection<String> value)
    {
        for (String s : value)
        {
            if (!Validation.isAsciiPrintable(s))
                return addMessage(message);
        }
        return true;
    }
    
    public static boolean verifyAsciiSafeHtml(String message, String value)
    {
        return Validation.isAsciiSafeHtml(value) || addMessage(message);
    }
    
    public static boolean verifyAsciiSafeHtml(String message, Collection<String> value)
    {
        for (String s : value)
        {
            if (!Validation.isAsciiSafeHtml(s))
                return addMessage(message);
        }
        return true;
    }
    
    public static boolean verifyAssertFalse(String message, boolean value)
    {
        return !value || addMessage(message);
    }
    
    public static boolean verifyAssertTrue(String message, boolean value)
    {
        return value || addMessage(message);
    }
    
    public static boolean verifyId(String message, int value)
    {
        return Validation.isValidId(value) || addMessage(message);
    }
    
    public static boolean verifyId(String message, List<Integer> list)
    {
        if (list == null || list.isEmpty())
            return true;
        
        for (Integer v : list)
        {
            if (!Validation.isValidId(v))
                return addMessage(message);
        }
        
        return true;
    }
    
    public static boolean verifyKey(String message, byte[] value)
    {
        return KeyUtil.isKey(value) || addMessage(message);
    }
    
    public static boolean verifyKey(String message, byte[] value, int kind)
    {
        return KeyUtil.isKey(value, kind) || addMessage(message);
    }
    
    public static boolean verifyKey(String message, List<byte[]> list)
    {
        if (list == null || list.isEmpty())
            return true;
        
        for(byte[] key : list)
        {
            if(!KeyUtil.isKey(key))
                return addMessage(message);
        }
        
        return true;
    }
    
    public static boolean verifyKey(String message, List<byte[]> list, int kind)
    {
        if (list == null || list.isEmpty())
            return true;
        
        for(byte[] key : list)
        {
            if(!KeyUtil.isKey(key, kind))
                return addMessage(message);
        }
        
        return true;
    }

    public static boolean verifyUnique(final String message, 
            final List<byte[]> list)
    {
        return Validation.isValidUnique(list) || addMessage(message);
    }
    
    public static boolean verifyUnique(final String message, 
            final List<byte[]> list, final int kind)
    {
        return Validation.isValidUnique(list, kind) || addMessage(message);
    }
    
    public static boolean verifyUnique(final String message, 
            final List<String> list, String dummy)
    {
        return Validation.isValidUnique(list, dummy) || addMessage(message);
    }
    
    public static boolean verifyUnique(final String message, 
            final List<Integer> list, Integer dummy)
    {
        return Validation.isValidUnique(list, dummy) || addMessage(message);
    }
    
    public static boolean verifyUnique(final String message, 
            final List<Integer> list, final int min, Integer dummy)
    {
        return Validation.isValidUnique(list, min, dummy) || addMessage(message);
    }
    
    public static boolean verifyUnique(final String message, 
            final List<Long> list, Long dummy)
    {
        return Validation.isValidUnique(list, dummy) || addMessage(message);
    }
    
    public static boolean verifyUnique(final String message, 
            final List<Long> list, final long min, Long dummy)
    {
        return Validation.isValidUnique(list, min, dummy) || addMessage(message);
    }
    
    public static <T extends HasId> boolean verifyUnique(final String message, 
            final List<T> list, final int min, HasId dummy)
    {
        return Validation.isValidUnique(list, min, dummy) || addMessage(message);
    }
    
    /**
     * If kind is zero, it is assumed that the key was validated with a kind before this 
     * call.
     */
    public static <T extends HasKey> boolean verifyUnique(final String message, 
            final List<T> list, final int kind, HasKey dummy)
    {
        return Validation.isValidUnique(list, kind, dummy) || addMessage(message);
    }
    
    /**
     * If kind is zero, it is assumed that the key was validated with a kind before this 
     * call.
     */
    public static <T extends HasParentKey> boolean verifyUnique(final String message, 
            final List<T> list, final int kind, HasParentKey dummy)
    {
        return Validation.isValidUnique(list, kind, dummy) || addMessage(message);
    }
    
    /**
     * If kind is zero, it is assumed that the key was validated with a kind before this 
     * call.
     */
    public static <T extends HasId, K extends HasKey> boolean verifyUnique(final String message, 
            final List<T> idlist, final int min, 
            final List<K> keylist, final int kind)
    {
        return Validation.isValidUnique(idlist, min, keylist, kind) || addMessage(message);
    }
    
    public static <T extends HasKeys> boolean verifyUniqueKeys(final String message, 
            final List<T> list, final int ... indices)
    {
        return isValidUniqueKeys(list, indices) || addMessage(message);
    }
    
    public static <T extends HasKeys> boolean verifyUniqueKeys(final String message, 
            final List<T> list, final int index)
    {
        return isValidUniqueKeys(list, index) || addMessage(message);
    }
    
    public static <T extends HasIds> boolean verifyUniqueIds(final String message, 
            final List<T> list, final int ... indices)
    {
        return isValidUniqueIds(list, indices) || addMessage(message);
    }
    
    public static <T extends HasIds> boolean verifyUniqueIds(final String message, 
            final List<T> list, final int index)
    {
        return isValidUniqueIds(list, index) || addMessage(message);
    }
    
    public static boolean verifyTime(String message, int value)
    {
        return isValidTime(value) || addMessage(message);
    }
    
    public static boolean verifyDate(String message, long value)
    {
        return isValidDate(value) || addMessage(message);
    }
    
    public static boolean verifyDecimalMax(String message, BigDecimal value, BigDecimal maxValue)
    {
        return isValidDecimalMax(value, maxValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMax(String message, String value, BigDecimal maxValue)
    {
        return isValidDecimalMax(value, maxValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMax(String message, float value, float maxValue)
    {
        return isValidDecimalMax(value, maxValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMax(String message, double value, double maxValue)
    {
        return isValidDecimalMax(value, maxValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMin(String message, BigDecimal value, BigDecimal minValue)
    {
        return isValidDecimalMin(value, minValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMin(String message, String value, BigDecimal minValue)
    {
        return isValidDecimalMin(value, minValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMin(String message, float value, float minValue)
    {
        return isValidDecimalMin(value, minValue) || addMessage(message);
    }
    
    public static boolean verifyDecimalMin(String message, double value, double minValue)
    {
        return isValidDecimalMin(value, minValue) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, BigDecimal value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, String value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, int value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, long value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, float value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, double value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyDigits(String message, Number value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(value, maxIntegerLength, maxFractionLength) || addMessage(message);
    }
    
    public static boolean verifyEmail(String message, CharSequence value)
    {
        return isValidEmail(value) || addMessage(message);
    }
    
    public static boolean verifyFutureTS(String message, long value) // timestamp
    {
        return isValidFutureTS(value) || addMessage(message);
    }
    
    public static boolean verifyFutureTS(String message, long value, int unit, int min, int max) // timestamp
    {
        return isValidFutureTS(value, unit, min, max) || addMessage(message);
    }
    
    public static boolean verifyFutureOrToday(String message, long value, int min, int max) // utc date
    {
        return isValidFutureOrToday(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyFuture(String message, long value, int min, int max) // utc date
    {
        return isValidFuture(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyFuture(String message, Date value)
    {
        return isValidFuture(value) || addMessage(message);
    }
    
    /*public static boolean verifyFuture(String message, Calendar value)
    {
        return isValidFuture(value) || addMessage(message);
    }*/
    
    public static boolean verifyLength(String message, CharSequence value, int min, int max)
    {
        return isValidLength(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyLuhn(String message, String value, int multiplicator)
    {
        return isValidLuhn(value, multiplicator) || addMessage(message);
    }
    
    /* ================================================== */
    
    public static boolean verifyMax(String message, int value, int max)
    {
        return isValidMax(value, max) || addMessage(message);
    }
    
    public static boolean verifyMax(String message, long value, long max)
    {
        return isValidMax(value, max) || addMessage(message);
    }
    
    public static boolean verifyMax(String message, float value, float max)
    {
        return isValidMax(value, max) || addMessage(message);
    }
    
    public static boolean verifyMax(String message, double value, double max)
    {
        return isValidMax(value, max) || addMessage(message);
    }
    
    public static boolean verifyMax(String message, String value, long max)
    {
        return isValidMax(value, max) || addMessage(message);
    }
    
    /* ================================================== */
    
    public static boolean verifyMin(String message, int value, int min)
    {
        return isValidMin(value, min) || addMessage(message);
    }
    
    public static boolean verifyMin(String message, long value, long min)
    {
        return isValidMin(value, min) || addMessage(message);
    }
    
    public static boolean verifyMin(String message, float value, float min)
    {
        return isValidMin(value, min) || addMessage(message);
    }
    
    public static boolean verifyMin(String message, double value, double min)
    {
        return isValidMin(value, min) || addMessage(message);
    }
    
    public static boolean verifyMin(String message, String value, long min)
    {
        return isValidMin(value, min) || addMessage(message);
    }
    
    /* ================================================== */
    
    public static boolean verifyGT(String message, int value, int param)
    {
        return isValidGT(value, param) || addMessage(message);
    }
    
    public static boolean verifyGT(String message, long value, long param)
    {
        return isValidGT(value, param) || addMessage(message);
    }
    
    public static boolean verifyGT(String message, float value, float param)
    {
        return isValidGT(value, param) || addMessage(message);
    }
    
    public static boolean verifyGT(String message, double value, double param)
    {
        return isValidGT(value, param) || addMessage(message);
    }
    
    public static boolean verifyGT(String message, String value, long param)
    {
        return isValidGT(value, param) || addMessage(message);
    }
    
    /* ================================================== */
    
    public static boolean verifyNotBlank(String message, String value)
    {
        return isValidNotBlank(value) || addMessage(message);
    }
    
    // add-on
    public static boolean verifyNotBlank(String message, CharSequence value)
    {
        return isValidNotBlank(value) || addMessage(message);
    }
    
    public static boolean verifyNotEmpty(String message, String value)
    {
        return isValidNotEmpty(value) || addMessage(message);
    }
    
    // add-on
    public static boolean verifyNotEmpty(String message, CharSequence value)
    {
        return isValidNotEmpty(value) || addMessage(message);
    }
    
    public static boolean verifyNotEmpty(String message, CAS value)
    {
        return isValidNotEmpty(value) || addMessage(message);
    }
    
    public static boolean verifyNotNull(String message, Object value)
    {
        return isValidNotNull(value) || addMessage(message);
    }
    
    public static boolean verifyNull(String message, Object value)
    {
        return isValidNull(value) || addMessage(message);
    }
    
    public static boolean verifyNotNullNotEmpty(String message, List<?> value)
    {
        return isValidNotNullNotEmpty(value) || addMessage(message);
    }
    
    public static boolean verifyPastTS(String message, long value) // timestamp
    {
        return isValidPastTS(value) || addMessage(message);
    }
    
    public static boolean verifyPastTS(String message, long value, int unit, int min, int max) // timestamp
    {
        return isValidPastTS(value, unit, min, max) || addMessage(message);
    }
    
    public static boolean verifyPastOrToday(String message, long value, int min, int max) // utc
    {
        return isValidPastOrToday(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyPast(String message, long value, int min, int max) // utc
    {
        return isValidPast(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyPast(String message, Date value)
    {
        return isValidPast(value) || addMessage(message);
    }
    
    /*public static boolean verifyPast(String message, Calendar value)
    {
        return isValidPast(value) || addMessage(message);
    }*/
    
    public static boolean verifyPattern(String message, CharSequence value, Pattern pattern)
    {
        return isValidPattern(value, pattern) || addMessage(message);
    }
    
    public static boolean verifySafeHtml(String message, CharSequence value) // needs a whitelist arg
    {
        return isValidSafeHtml(value) || addMessage(message);
    }
    
    public static boolean verifyScriptAssert(String message, Object value)
    {
        return isValidScriptAssert(value) || addMessage(message);
    }
    
    public static boolean verifySize(String message, Collection<?> value, int min, int max)
    {
        return isValidSize(value, min, max) || addMessage(message);
    }
    
    public static boolean verifySize(String message, CharSequence value, int min, int max)
    {
        return isValidSize(value, min, max) || addMessage(message);
    }
    
    // port should be passed as -1 if not configured
    public static boolean verifyURL(String message, String value, String protocol, String host, int port)
    {
        return isValidURL(value, protocol, host, port) || addMessage(message);
    }
    
    public static boolean verifyURL(String message, String value, String protocol, String host)
    {
        return isValidURL(value, protocol, host, -1) || addMessage(message);
    }
    
    public static boolean verifyRange(String message, int value, int min, int max)
    {
        return isValidRange(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyRange(String message, long value, long min, long max)
    {
        return isValidRange(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyRange(String message, float value, float min, float max)
    {
        return isValidRange(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyRange(String message, double value, double min, double max)
    {
        return isValidRange(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyRange(String message, String value, long min, long max)
    {
        return isValidRange(value, min, max) || addMessage(message);
    }
    
    public static boolean verifyCreditCardNumber(String message, String value)
    {
        return isValidCreditCardNumber(value) || addMessage(message);
    }

}
