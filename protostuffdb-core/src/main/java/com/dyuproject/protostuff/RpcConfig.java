//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import static com.dyuproject.protostuffdb.DatastoreManager.CREATE_IF_MISSING;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import protostuffdb.Jni;


import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.DatastoreManager;
import com.dyuproject.protostuffdb.LsmdbDatastoreManager;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * Rpc config parsing via {@link Properties}.
 * 
 * @author David Yu
 * @created Sep 1, 2012
 */
public final class RpcConfig
{
    private RpcConfig() {}
    
    static final Pattern COMMA = Pattern.compile(",");
    
    static final String DATA_DIR = "data_dir",
        MODULES = "modules", 
        FF_HANDLERS = "ff_handlers";
    
    public static int sysprop(String name, int defaultValue, Properties props)
    {
        String v = System.getProperty(name);
        if (v == null && (props == null || null == (v = props.getProperty(name))))
            return defaultValue;
        
        return Integer.parseInt(v);
    }
    
    public static long sysprop(String name, long defaultValue, Properties props)
    {
        String v = System.getProperty(name);
        if (v == null && (props == null || null == (v = props.getProperty(name))))
            return defaultValue;
        
        return Long.parseLong(v);
    }
    
    public static Class<?> loadClass(String className)
    {
        try
        {
            return Thread.currentThread().getContextClassLoader().loadClass(
                    className);
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    static RpcServiceProvider resolveProvider(Properties props, String name, 
            Datastore store, WriteContext context)
    {
        String sp = props.getProperty(name + ".provider");
        if(sp == null)
            throw new IllegalStateException("Required param: " + name + ".provider");
        
        Class<?> clazz = loadClass(sp);
        if(!RpcServiceProvider.class.isAssignableFrom(clazz))
        {
            throw new IllegalStateException(sp + 
                    " is not a subclass of RpcServiceProvider.");
        }
        
        final Constructor<?> c;
        try
        {
            c = (Constructor<?>)clazz.getConstructor(Datastore.class, WriteContext.class);
        }
        catch(Exception e)
        {
            // use default constructor
            try
            {
                return (RpcServiceProvider)clazz.newInstance();
            }
            catch (Exception e1)
            {
                throw new RuntimeException(e1);
            }
        }
        
        try
        {
            return (RpcServiceProvider)c.newInstance(store, context);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    static List<RpcFastFunc> loadFFHandlersFrom(Properties props, 
            RpcFastFunc[] fastfuncs)
    {
        String ffCsv = props.getProperty(FF_HANDLERS);
        if(ffCsv == null)
            return Collections.emptyList();
        
        ArrayList<RpcFastFunc> list = new ArrayList<RpcFastFunc>();
        for(String fqcn : COMMA.split(ffCsv))
        {
            Class<?> clazz = loadClass(fqcn.trim());
            if(!RpcFastFunc.class.isAssignableFrom(clazz))
            {
                throw new IllegalStateException(clazz.getName() + 
                        " is not a subclass of RpcFastFunc.");
            }
            
            final RpcFastFunc ff;
            try
            {
                ff = (RpcFastFunc)clazz.newInstance();
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
            
            if(fastfuncs[ff.id] != null)
            {
                throw new IllegalStateException("Shared ff id: " + ff.id + 
                        " between " + fastfuncs[ff.id].getClass().getName() + 
                        " and " + ff.getClass().getName());
            }
            
            fastfuncs[ff.id] = ff;
            
            list.add(ff);
        }
        
        return list;
    }
    
    /**
     * Returns the data dir based from the config in {@code props}.
     */
    public static String getDataDirParam(Properties props)
    {
        final String dataDirParam = props.getProperty(DATA_DIR);
        
        if(dataDirParam == null)
            throw new IllegalStateException("Required param: " + DATA_DIR);
        
        return dataDirParam;
    }
    
    public static void init(RpcServer server, WriteContext context)
    {
        final Properties props = server.props;
        final String[] modules = server.modules;
        final RpcServiceProvider[] providers = server.providers;
        final Datastore[] stores = server.stores;
        final HashMap<String, Datastore> storeMap = server.storeMap;
        final ArrayList<Integer> ids = new ArrayList<Integer>();
        final DatastoreManager manager = server.storeManager;
        if (manager instanceof LsmdbDatastoreManager)
            ((LsmdbDatastoreManager)manager).stream.ptr = context.stream.ptr;
        
        for (int i = 0; i < providers.length; i++)
        {
            String module = modules[i].trim();
            
            Datastore store = manager.getStore(module, CREATE_IF_MISSING);
            
            RpcServiceProvider provider = resolveProvider(props, module, store, context);
            
            storeMap.put(module, store);
            provider.name = module;
            
            providers[i] = provider;
            
            provider.fill(server.services, ids, store, context, props, manager);
            
            // map the store to the service
            for(int id : ids)
                stores[id] = store;
            
            ids.clear();
            
            if(i == 0)
            {
                // the first provider's datastore should contain the auth data
                stores[0] = store;
            }
        }
        
        for(RpcFastFunc ff : loadFFHandlersFrom(props, server.fastfuncs))
        {
            // init fastfuncs
            ff.init(server, context);
        }
    }
    
    /**
     * Returns a new {@link RpcServer} based from the config read from {@code props}.
     */
    public static RpcServer getServerFrom(Properties props, File baseDir,  
            DatastoreManager manager)
    {
        // modules
        final String modulesParam = props.getProperty(MODULES);
        if (modulesParam == null)
            throw new IllegalArgumentException("missing modules param");
        
        if (manager == null)
        {
            // data dir
            final String dataDirParam = getDataDirParam(props);
            File dataDir = new File(dataDirParam);
            if(!dataDir.exists() || !dataDir.isDirectory())
            {
                if(baseDir == null || 
                    !(dataDir = new File(baseDir, dataDirParam)).exists() || 
                    !dataDir.isDirectory())
                {
                    throw new IllegalStateException(dataDirParam + 
                            " is not an existing directory");
                }
            }
            
            // use the db buffer of the first writer
            manager = new LsmdbDatastoreManager(Jni.bufDb(0), dataDir, true);
        }
        
        // any context will do for initialization
        final String[] modules = COMMA.split(modulesParam);
        final RpcServer server = new RpcServer(Jni.WORKERS, 
                modules, 
                new RpcServiceProvider[modules.length], 
                new RpcService[128], 
                new RpcFastFunc[256], 
                new Datastore[128], 
                new HashMap<String, Datastore>(), 
                manager, props);
        
        return server;
    }
}
