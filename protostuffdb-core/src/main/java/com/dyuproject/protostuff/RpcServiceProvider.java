//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Properties;

import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.DatastoreManager;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * Responsible for providing the services.
 *
 * @author David Yu
 * @created Jul 16, 2011
 */
public abstract class RpcServiceProvider
{

    
    String name;
    
    /**
     * Returns the name of this provider
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Fill up the services indexed by service id (0-255).
     */
    public abstract void fill(RpcService[] services, List<Integer> ids, 
            Datastore store, WriteContext context, Properties options, 
            DatastoreManager manager);
    
    public byte[] authenticate(RpcLogin login, Datastore store, 
            WriteContext context)
    {
        // external authentication by default
        return null;
    }
    
    /**
     * Sync from remote agents.
     */
    public boolean handlePkvSync(RpcWorker worker, 
            byte[] k, int koffset, int klen,
            byte[] v, int voffset, int vlen) throws IOException
    {
        return false;
    }
    
    /**
     * To publish a message, call {@link WriteContext#pub(Object, Schema)}.
     */
    public void handleLogUpdates(RpcWorker worker, 
            byte[] buf, int offset, int len) throws IOException
    {
        
    }
    
    /**
     * For subclass override.
     */
    protected void processLogEntity(RpcWorker worker, int kind, 
            byte[] k, int koffset, 
            byte[] v, int voffset, int vlen) throws IOException 
    {
        
    }
    
    /**
     * For subclass override.
     */
    protected void processLogPKV(RpcWorker worker, 
            byte[] k, int koffset, int klen, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        
    }
    
    protected final void processLogUpdates(RpcWorker worker, 
            byte[] buf, int offset, int len) throws IOException
    {
        int total = 0, klen, vlen, kind;
        while (total < len)
        {
            klen = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
            vlen = ((buf[offset++] & 0xFF) | (buf[offset++] & 0xFF) << 8);
            
            kind = buf[offset] & 0xFF;
            
            if (kind == 128)
            {
                processLogPKV(worker, buf, offset, klen, buf, offset + klen, vlen);
            }
            else if (klen == 9)
            {
                processLogEntity(worker, kind, buf, offset, buf, offset + klen, vlen);
            }
            else
            {
                System.err.println("Corrupt log update on kind: " + kind);
                System.exit(1);
            }
            
            offset += (klen + vlen);
            total += (4 + klen + vlen);
        }
    }
    
    protected static void fill(RpcService[] services, List<Integer> ids, 
            Class<?> wrapperClass)
    {
        for(Field f : wrapperClass.getDeclaredFields())
        {
            if ((f.getModifiers() & Modifier.STATIC) == 0)
                continue;
            
            f.setAccessible(true);
            try
            {
                Object o = f.get(null);
                if(o instanceof RpcService)
                {
                    RpcService rs = (RpcService)o;
                    if (services[rs.id] != null)
                        throw new RuntimeException("Non-unique service id: " + rs.id);
                    
                    services[rs.id] = rs;
                    
                    ids.add(rs.id);
                }
            }
            catch (IllegalArgumentException e)
            {
                throw new RuntimeException(e);
            }
            catch (IllegalAccessException e)
            {
                throw new RuntimeException(e);
            }
            
        }
    }
}
