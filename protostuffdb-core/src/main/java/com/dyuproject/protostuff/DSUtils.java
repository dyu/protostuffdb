//================================================================================
//Copyright (c) 2011, David Yu
//All rights reserved.
//--------------------------------------------------------------------------------
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of protostuff nor the names of its contributors may be used
//    to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//================================================================================


package com.dyuproject.protostuff;

import java.io.IOException;
import java.io.OutputStream;

/**
 * DS utilities for accessing package-private fields/methods.
 *
 * @author David Yu
 * @created Jan 3, 2012
 */
public final class DSUtils
{
    private DSUtils() {}
    
    public static boolean isAuthorized(RpcService service, RpcHeader header)
    {
        return service.isAuthorized(header);
    }
    
    public static ByteString wrap(byte[] data)
    {
        return ByteString.wrap(data);
    }
    
    public static byte[] getByteArray(ByteString bs)
    {
        return bs.getBytes();
    }

    public static byte[] getByteArray(LinkedBuffer buffer)
    {
        return buffer.buffer;
    }
    
    public static byte[] getBuffer(DSByteArrayInput input)
    {
        return input.buffer;
    }
    
    public static int getOffset(DSByteArrayInput input)
    {
        return input.offset;
    }
    
    public static int getLimit(DSByteArrayInput input)
    {
        return input.limit;
    }
    
    public static int getLength(DSByteArrayInput input)
    {
        return input.limit - input.offset;
    }
    
    public static boolean writeTip(WriteSession session, byte last)
    {
        byte[] buf = session.head.buffer;
        if (session.head.offset == buf.length)
            return false;
        
        buf[session.head.offset++] = last;
        session.size++;
        return true;
    }
    
    public static <T> void writeStartTo(JsonXOutput output) throws IOException
    {
        output.writeStartObject();
    }
    
    public static <T> void writeEndTo(JsonXOutput output) throws IOException
    {
        if (output.isLastRepeated())
            output.writeEndArray();
        
        output.writeEndObject();
    }
    
    public static <T> void writeTo(JsonXOutput output, T message, Schema<T> schema)
    {
        output.use(schema);
        try
        {
            output.writeStartObject();
            
            schema.writeTo(output, message);
            
            if (output.isLastRepeated())
                output.writeEndArray();
            
            output.writeEndObject();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Serializing to a byte array threw an IOException " + 
                    "(should never happen).", e);
        }
    }
    
    public static void clear(RpcWorker worker)
    {
        worker.clear();
    }
    
    public static boolean isBufferOverflow(WriteSession session)
    {
        return session.head != session.tail;
    }
    
    static final class BufferOverflow extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public BufferOverflow(){}
        public BufferOverflow(String msg){super(msg);}
        
        @Override
        public Throwable fillInStackTrace()
        {
            return this;
        }
    }
    
    static final OutputStream DUMMY_OUT = new OutputStream()
    {
        @Override
        public void write(int b) throws IOException
        {
            throw new BufferOverflow();
        }

        @Override
        public void write(byte[] b) throws IOException
        {
            throw new BufferOverflow();
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException
        {
            throw new BufferOverflow();
        }
    };
    
    public static final Output VOID_OUTPUT = new Output()
    {
        @Override
        public boolean isEnumsByName()
        {
            return false;
        }
        
        @Override
        public void writeUInt64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeUInt32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeString(int fieldNumber, String value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeSInt64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeSInt32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeSFixed64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeSFixed32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public <T> void writeObject(int fieldNumber, T value, Schema<T> schema, boolean repeated)
                throws IOException
        {
            
        }
        
        @Override
        public void writeInt64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeInt32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeFloat(int fieldNumber, float value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeFixed64(int fieldNumber, long value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeFixed32(int fieldNumber, int value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeEnum(int fieldNumber, int value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeEnumFromIdx(int fieldNumber, int idx, EnumMapping mapping,
                boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeDouble(int fieldNumber, double value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeBytes(int fieldNumber, ByteString value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeByteRange(boolean utf8String, int fieldNumber, byte[] value, int offset,
                int length, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeByteArray(int fieldNumber, byte[] value, boolean repeated) throws IOException
        {
            
        }
        
        @Override
        public void writeBool(int fieldNumber, boolean value, boolean repeated) throws IOException
        {
            
        }
    };
}
