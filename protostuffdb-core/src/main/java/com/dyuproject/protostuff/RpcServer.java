//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.DatastoreManager;

import java.util.HashMap;
import java.util.Properties;


/**
 * FastFunc daemon.
 *
 * @author David Yu
 * @created Jul 1, 2011
 */
public final class RpcServer
{
    
    public final RpcWorker[] workers;
    public final String[] modules;
    public final RpcServiceProvider[] providers;
    public final RpcService[] services;
    public final RpcFastFunc[] fastfuncs;
    public final Datastore[] stores;
    public final HashMap<String,Datastore> storeMap;
    //public final WriteContext[] contexts;
    public final DatastoreManager storeManager;
    public final Properties props;
    
    public RpcServer(RpcWorker[] workers, String[] modules, RpcServiceProvider[] providers, 
            RpcService[] services, RpcFastFunc[] fastfuncs, 
            Datastore[] stores, HashMap<String,Datastore> storeMap, 
            //WriteContext[] contexts,
            DatastoreManager storeManager,
            Properties props)
    {
        this.workers = workers;
        this.modules = modules;
        this.providers = providers;
        this.services = services;
        this.fastfuncs = fastfuncs;
        this.stores = stores;
        this.storeMap = storeMap;
        //this.contexts = contexts;
        this.storeManager = storeManager;
        this.props = props;
    }
    
    public Datastore getDatastore(String name)
    {
        return storeMap.get(name);
    }
    
    public Datastore getDatastore(int idx)
    {
        return stores[idx];
    }
    
    /*public WriteContext getWriteContext(int idx)
    {
        return contexts[idx];
    }*/
    
    public RpcFastFunc getRpcFastFunc(int id)
    {
        return fastfuncs[id];
    }
}
