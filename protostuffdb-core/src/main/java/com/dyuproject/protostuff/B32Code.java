//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

/*
Copyright & License
    Copyright (c) 2010, Taobao Inc., Alibaba Group ( http://www.taobao.com
    ).

    Copyright (c) 2009, agentzh <agentzh@gmail.com>.

    This module is licensed under the terms of the BSD license.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    *   Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

    *   Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.dyuproject.protostuff;


/**
 * Fast Base 32 encoding based from agentzh's set-misc-nginx-module.  See  
 * https://github.com/agentzh/set-misc-nginx-module/blob/master/src/ngx_http_set_base32.c 
 *
 * @author agentzh
 * @author David Yu
 * @created Sep 6, 2011
 */
public final class B32Code
{
    
    private static final byte[] basis32 = "0123456789abcdefghijklmnopqrstuv".getBytes();
    
    private static final int[] padCount = new int[]{0, 6, 4, 3, 1};
    
    private B32Code() {}
    
    /**
     * Returns the bytes containing the encoded input (padding enabled).
     */
    public static byte[] encode(byte[] input)
    {
        return encode(input, 0, input.length, true);
    }
    
    /**
     * Returns the bytes containing the encoded input (padding enabled).
     */
    public static byte[] encode(byte[] input, int inOffset, int inLen)
    {
        return encode(input, inOffset, inLen, true);
    }
    
    /**
     * Returns the bytes containing the encoded input.
     */
    public static byte[] encode(byte[] input, boolean padding)
    {
        return encode(input, 0, input.length, padding);
    }
    
    /**
     * Returns the bytes containing the encoded input.
     */
    public static byte[] encode(byte[] input, int inOffset, int inLen, boolean padding)
    {
        int len = (((inLen)+4)/5)*8;
        if(!padding)
            len -= padCount[inLen%5];
        
        final byte[] output = new byte[len];
        encode(input, inOffset, inLen, output , 0, padding);
        return output;
    }
    
    /**
     * Returns the size of the encoded input.
     */
    static int encode(final byte[] input, int inOffset, final int inLen, 
            final byte[] output, int outOffset, boolean padding)
    {
        int len = inLen;
        final int origOutOffset = outOffset;
        
        while (len > 4) {
            output[outOffset++] = basis32[(input[inOffset] & 0xff) >>> 3];
            output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 0x07) << 2) | ((input[inOffset] & 0xff) >>> 6)];
            output[outOffset++] = basis32[((input[inOffset] & 0xff) >>> 1) & 0x1f];
            output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 1) << 4) | ((input[inOffset] & 0xff) >>> 4)];
            output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 0x0f) << 1) | ((input[inOffset] & 0xff) >>> 7)];
            output[outOffset++] = basis32[((input[inOffset] & 0xff) >>> 2) & 0x1f];
            output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 0x03) << 3) | ((input[inOffset] & 0xff) >>> 5)];
            output[outOffset++] = basis32[(input[inOffset++] & 0xff) & 0x1f];

            len -= 5;
        }

        if (len != 0) {
            output[outOffset++] = basis32[(input[inOffset] & 0xff) >>> 3];

            if (len == 1) {
                /* 1 byte left */
                output[outOffset++] = basis32[((input[inOffset] & 0xff) & 0x07) << 2];

                /* pad six '='s to the end */
                if (padding) {
                    output[outOffset++] = '=';
                    output[outOffset++] = '=';
                    output[outOffset++] = '=';
                    output[outOffset++] = '=';
                    output[outOffset++] = '=';
                }

            } else {
                output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 0x07) << 2) | ((input[inOffset] & 0xff) >>> 6)];
                output[outOffset++] = basis32[((input[inOffset] & 0xff) >>> 1) & 0x1f];

                if (len == 2) {
                    /* 2 bytes left */
                    output[outOffset++] = basis32[((input[inOffset] & 0xff) & 1) << 4];

                    /* pad four '='s to the end */
                    if (padding) {
                        output[outOffset++] = '=';
                        output[outOffset++] = '=';
                        output[outOffset++] = '=';
                    }

                } else {
                    output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 1) << 4) | ((input[inOffset] & 0xff) >>> 4)];

                    if (len == 3) {
                        /* 3 bytes left */
                        output[outOffset++] = basis32[((input[inOffset] & 0xff) & 0x0f) << 1];

                        if (padding) {
                            /* pad three '='s to the end */
                            output[outOffset++] = '=';
                            output[outOffset++] = '=';
                        }

                    } else {
                        /* 4 bytes left */
                        output[outOffset++] = basis32[(((input[inOffset++] & 0xff) & 0x0f) << 1) | ((input[inOffset] & 0xff) >>> 7)];
                        output[outOffset++] = basis32[((input[inOffset] & 0xff) >>> 2) & 0x1f];
                        output[outOffset++] = basis32[((input[inOffset] & 0xff) & 0x03) << 3];

                        /* pad one '=' to the end */
                    }
                }
            }

            if (padding) {
                output[outOffset++] = '=';
            }
        }

        return outOffset - origOutOffset;
    }
    
    /*static byte[] decode(byte[] s)
    {
        return decode(s, 0, s.length, true);
    }
    
    static byte[] decode(byte[] s, boolean padding)
    {
        return decode(s, 0, s.length, padding);
    }
    
    static byte[] decode(byte[] s, int soff, int slen, boolean padding)
    {
        int len = (((slen)+7)/8)*5;
        
        byte[] d = new byte[len];
        int actualLen = decode(s, soff, slen, d, 0, len);
        if (actualLen == -1)
            return null;
        
        if (len == actualLen)
            return d;
        
        byte[] actual = new byte[actualLen];
        System.arraycopy(d, 0, actual, 0, actualLen);
        return actual; 
    }
    
    static int decode(byte[] s, int soff, int slen, 
            byte[] d, int doff, int dlen)
    {
        int i, mod, dorig = doff;

        for (i = 0; i < slen; i++) {
            //System.err.println((soff+i) + " " + slen + " " + s[soff+i]);
            if (s[soff+i] == '=') {
                break;
            }

            if (basis32[s[soff+i]] == 77) {
                return -1;
            }
        }

        mod = i % 8;

        if (mod == 1 || mod == 3 || mod == 6) {
             bad Base32 digest length 
            return -1;
        }

        while (i > 7) {
            d[doff++] = (byte)((basis32[s[soff]] << 3) | ((basis32[s[soff+1]] >> 2) & 0x07));

            d[doff++] = (byte)(((basis32[s[soff+1]] & 0x03) << 6) | (basis32[s[soff+2]] << 1) |
                ((basis32[s[soff+3]] >> 4) & 1));

            d[doff++] = (byte)(((basis32[s[soff+3]] & 0x0f) << 4) | ((basis32[s[soff+4]] >> 1) & 0x0f));

            d[doff++] = (byte)(((basis32[s[soff+4]] & 1) << 7) | ((basis32[s[soff+5]] & 0x1f) << 2) |
                ((basis32[s[soff+6]] >> 3) & 0x03));
            d[doff++] = (byte)(((basis32[s[soff+6]] & 0x07) << 5) | (basis32[s[soff+7]] & 0x1f));

            soff += 8;
            i -= 8;
        }

        if (i != 0) {
             2 bytes left 
            d[doff++] = (byte)((basis32[s[soff]] << 3) | ((basis32[s[soff+1]] >> 2) & 0x07));

            if (i > 2) {
                 4 bytes left 
                d[doff++] = (byte)(((basis32[s[soff+1]] & 0x03) << 6) | ((basis32[s[soff+2]] & 0x1f) << 1)
                    | ((basis32[s[soff+3]] >> 4) & 1));

                if (i > 4) {
                     5 bytes left 
                    d[doff++] = (byte)(((basis32[s[soff+3]] & 0x0f) << 4) |
                        ((basis32[s[soff+4]] >> 1) & 0x0f));

                    if (i > 5) {
                         7 bytes left 
                        d[doff++] = (byte)(((basis32[s[soff+4]] & 1) << 7) |
                            ((basis32[s[soff+5]] & 0x1f) << 2) |
                            ((basis32[s[soff+6]] >> 3) & 0x03));
                    }
                }
            }
        }

        //*dlen = (size_t) (d - dst);

        return doff - dorig;
    }*/
    
    /*static void test(boolean padding)
    {
        for(String s : new String[]{
                "1", "12", "123", "1234", "12345", 
                "123456", "1234567", "12345678", "123456789", "1234567890", 
                //"12345678901", "123456789012", "1234567890123", "12345678901234", "123456789012345"
                })
        {
            byte[] b = s.getBytes();
            byte[] e = encode(b, padding);
            System.err.println(new String(e));
            //byte[] d = decode(e, padding);
            //System.err.println(new String(d) + " == " + new String(b));
            //System.err.println(s.length() + " | " + count);
        }
    }
    
    public static void main(String[] args)
    {
        test(true);
        test(false);
    }*/

}
