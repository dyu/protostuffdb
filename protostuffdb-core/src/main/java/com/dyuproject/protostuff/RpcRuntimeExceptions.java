//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

import protostuffdb.Jni;

/**
 * Runtime exceptions that could occur during RPC operations.
 *
 * @author David Yu
 * @created Sep 9, 2011
 */
public final class RpcRuntimeExceptions
{
    private RpcRuntimeExceptions() {}
    
    static final boolean
            ST_VALIDATION = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_validation"),
            ST_FAILURE = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_failure"),
            ST_PIPE = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_pipe"),
            ST_IO = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_io");
    
    public static class IO extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        
        public IO(IOException e)
        {
            super(e);
        }
        
        public Throwable fillInStackTrace()
        {
            return !ST_IO ? this : super.fillInStackTrace();
        }
    }
    
    public static class Failure extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        
        public Failure(String msg)
        {
            super(msg);
        }
        
        @Override
        public Throwable fillInStackTrace()
        {
            return !ST_FAILURE ? this : super.fillInStackTrace();
        }
    }
    
    public static class Validation extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        
        public Validation(String msg)
        {
            super(msg);
        }
        
        @Override
        public Throwable fillInStackTrace()
        {
            return !ST_VALIDATION ? this : super.fillInStackTrace();
        }
    }
    
    public static class Pipe extends IO
    {
        private static final long serialVersionUID = 1L;
        
        public Pipe(IOException e)
        {
            super(e);
        }
        
        @Override
        public Throwable fillInStackTrace()
        {
            return !ST_PIPE ? this : super.fillInStackTrace();
        }
    }
    
    public static IO io(IOException e)
    {
        return new IO(e);
    }
    
    public static Pipe pipe(IOException e)
    {
        return new Pipe(e);
    }
    
    public static Validation validation(String msg)
    {
        return new Validation(msg);
    }
    
    public static Failure failure(String msg)
    {
        return new Failure(msg);
    }
    
    public static Failure fail(String msg)
    {
        return new Failure(msg);
    }

}
