//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

/**
 * <pre>
 * message RpcLogin {
 *   required bytes username = 2;
 *   required bytes password = 3;
 *   optional bytes remote_address = 4; 
 * }
 * </pre>
 * 
 * @author David Yu
 * @created Sep 6, 2011
 */
public final class RpcLogin
{
    
    public static Schema<RpcLogin> getSchema()
    {
        return SCHEMA;
    }
    
    public byte[] username; //2
    public byte[] password; //3
    public byte[] remoteAddress; //4

    public RpcLogin()
    {
        
    }
    
    public RpcLogin reset()
    {
        username = null;
        password = null;
        remoteAddress = null;
        
        return this;
    }

    // message method

    public Schema<RpcLogin> cachedSchema()
    {
        return SCHEMA;
    }

    static final Schema<RpcLogin> SCHEMA = new Schema<RpcLogin>()
    {
        // schema methods

        public RpcLogin newMessage()
        {
            return new RpcLogin();
        }

        public Class<RpcLogin> typeClass()
        {
            return RpcLogin.class;
        }

        public String messageName()
        {
            return RpcLogin.class.getSimpleName();
        }

        public String messageFullName()
        {
            return RpcLogin.class.getName();
        }

        public boolean isInitialized(RpcLogin message)
        {
            return message.username != null && message.username.length != 0 
                && message.password != null && message.password.length != 0;
        }

        public void mergeFrom(Input input, RpcLogin message) throws IOException
        {
            for(int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch(number)
                {
                    case 0:
                        return;
                    case 2:
                        message.username = input.readByteArray();
                        break;
                    case 3:
                        message.password = input.readByteArray();
                        break;
                    case 4:
                        message.remoteAddress = input.readByteArray();
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }


        public void writeTo(Output output, RpcLogin message) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public String getFieldName(int number)
        {
            return Integer.toString(number);
        }

        public int getFieldNumber(String name)
        {
            return Integer.parseInt(name);
        }
    };


}
