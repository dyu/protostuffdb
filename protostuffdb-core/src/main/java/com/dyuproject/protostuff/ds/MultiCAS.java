//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds;

import java.io.IOException;
import java.util.ArrayList;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.DSUtils;
import com.dyuproject.protostuff.EnumMapping;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuffdb.HasValue;

/**
 * Mutliple CAS operations (multiple fields).
 *
 * @author David Yu
 * @created Mar 25, 2011
 */
public class MultiCAS extends CAS implements Message<MultiCAS>, HasValue
{
    
    public final ArrayList<Op<?>> ops = new ArrayList<Op<?>>();
    public byte[] value;
    // fields 3 to 33 (bits 1 to 31)
    public int set;
    
    public MultiCAS()
    {
        
    }

    public MultiCAS addOp(Op<?> op)
    {
        ops.add(op);
        
        return this;
    }
    
    /**
     * Public modifier for unit test access.
     */
    public MultiCAS mergeOp(Op<?> op)
    {
        ops.add(op);
        
        // persistent entity field starts at 3
        set |= (1 << (op.f - 3));
        
        return this;
    }
    
    /**
     * Returns true if the op with the {@code field} was seen during deserialization.
     */
    public boolean isSet(int field)
    {
        return 0 != (set & (1 << (field - 3)));
    }
    
    /**
     * Opposite of {@link #isSet(int)}.
     */
    public boolean isMissing(int field)
    {
        return 0 == (set & (1 << (field - 3)));
    }
    
    public byte[] getValue() { return value; }
    public void setValue(byte[] value) { this.value = value; }
    
    public Op<?> findOp(int field)
    {
        for (Op<?> op : ops)
        {
            if (op.f == field)
                return op;
        }
        
        return null;
    }
    
    public int indexOf(int field)
    {
        int i = 0;
        for (Op<?> op : ops)
        {
            if (op.f == field)
                return i;
            i++;
        }
        
        return -1;
    }
    
    public Op<?> getOp(int index)
    {
        return index < ops.size() ? ops.get(index) : null;
    }
    
    public void fill(int[] fieldIndex)
    {
        for(int i = 0, size = ops.size(); i < size; i++)
        {
            Op<?> op = ops.get(i);
            if(op.f > 0 && op.f < 128)
                fieldIndex[op.f] = i;
        }
    }
    
    public boolean isEmpty()
    {
        return ops.isEmpty();
    }

    // message method

    public Schema<MultiCAS> cachedSchema()
    {
        return SCHEMA;
    }
    
    public static final Schema<MultiCAS> SCHEMA = new Schema<MultiCAS>()
    {

        // schema methods

        public MultiCAS newMessage()
        {
            return new MultiCAS();
        }

        public Class<MultiCAS> typeClass()
        {
            return MultiCAS.class;
        }

        public String messageName()
        {
            return MultiCAS.class.getSimpleName();
        }

        public String messageFullName()
        {
            return MultiCAS.class.getName();
        }

        public boolean isInitialized(MultiCAS message)
        {
            return true;
        }
        
        public void mergeFrom(Input input, MultiCAS message) throws IOException
        {
            CAS.mergeFrom(input, message, this);
        }
        
        @SuppressWarnings({ "unchecked", "rawtypes" })
        public void writeTo(Output output, MultiCAS message) throws IOException
        {
            if(message.ops != null)
            {
                for(Op op : message.ops)
                {
                    if(op != null)
                    {
                        output.writeObject(op.casFieldNumber(), op, op.cachedSchema(), 
                                true);
                    }
                }
            }
        }

        public String getFieldName(int number)
        {
            return CAS.getFieldName(number);
        }

        public int getFieldNumber(String name)
        {
            return CAS.getFieldNumber(name);
        }
        
    };

    public static Schema<MultiCAS> getSchema()
    {
        return SCHEMA;
    }
    
    public static Pipe.Schema<MultiCAS> getPipeSchema()
    {
        throw new UnsupportedOperationException();
    }
    
    public <T> void mergeTo(T message, Schema<T> schema)
    {
        final MCInput input = new MCInput(this);
        try
        {
            schema.mergeFrom(input, message);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.", e);
        }
    }
    
    static final class MCInput implements Input
    {
        
        private int i = 0;
        final MultiCAS mc;
        
        MCInput(MultiCAS mc)
        {
            this.mc = mc;
        }

        public <T> void handleUnknownField(int fieldNumber, Schema<T> schema) throws IOException
        {
            i++;
        }

        public <T> int readFieldNumber(Schema<T> schema) throws IOException
        {
            return i == mc.ops.size() ? 0 : mc.ops.get(i).f;
        }

        public int readInt32() throws IOException
        {
            return ((CAS.Int32Op)mc.ops.get(i++)).s;
        }

        public int readUInt32() throws IOException
        {
            return ((CAS.UInt32Op)mc.ops.get(i++)).s;
        }

        public int readSInt32() throws IOException
        {
            return ((CAS.SInt32Op)mc.ops.get(i++)).s;
        }

        public int readFixed32() throws IOException
        {
            return ((CAS.Fixed32Op)mc.ops.get(i++)).s;
        }

        public int readSFixed32() throws IOException
        {
            return ((CAS.SFixed32Op)mc.ops.get(i++)).s;
        }

        public long readInt64() throws IOException
        {
            return ((CAS.Int64Op)mc.ops.get(i++)).s;
        }

        public long readUInt64() throws IOException
        {
            return ((CAS.UInt64Op)mc.ops.get(i++)).s;
        }

        public long readSInt64() throws IOException
        {
            return ((CAS.SInt64Op)mc.ops.get(i++)).s;
        }

        public long readFixed64() throws IOException
        {
            return ((CAS.Fixed64Op)mc.ops.get(i++)).s;
        }

        public long readSFixed64() throws IOException
        {
            return ((CAS.SFixed64Op)mc.ops.get(i++)).s;
        }

        public float readFloat() throws IOException
        {
            return ((CAS.FloatOp)mc.ops.get(i++)).s;
        }

        public double readDouble() throws IOException
        {
            return ((CAS.DoubleOp)mc.ops.get(i++)).s;
        }

        public boolean readBool() throws IOException
        {
            return ((CAS.BoolOp)mc.ops.get(i++)).s;
        }

        public int readEnum() throws IOException
        {
            // TODO handling?
            return readInt32();
        }
        
        public int readEnumIdx(EnumMapping mapping) throws IOException
        {
            Integer idx = mapping.numberIdxMap.get(readEnum());
            return idx == null ? -1 : idx.intValue();
        }

        public String readString() throws IOException
        {
            return ((CAS.StringOp)mc.ops.get(i++)).s;
        }

        public ByteString readBytes() throws IOException
        {
            return DSUtils.wrap(readByteArray());
        }

        public byte[] readByteArray() throws IOException
        {
            return ((CAS.BytesOp)mc.ops.get(i++)).s;
        }

        public <T> T mergeObject(T value, Schema<T> schema) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public void transferByteRangeTo(Output output, boolean utf8String, int fieldNumber,
                boolean repeated) throws IOException
        {
            throw new UnsupportedOperationException();
        }
        
        public void transferEnumTo(Output output, EnumMapping mapping, int fieldNumber,
                boolean repeated) throws IOException
        {
            throw new UnsupportedOperationException();
        }
    }
}
