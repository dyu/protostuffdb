//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff.ds;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;

/**
 * A CAS operation on a single field.
 *
 * @author David Yu
 * @created Mar 25, 2011
 */
public final class SingleCAS extends CAS implements Message<SingleCAS>
{
    
    /**
     * Creates a new single cas with the specified op.
     */
    public static SingleCAS create(Op<?> op)
    {
        return new SingleCAS(op);
    }

    private Op<?> op;
    
    public SingleCAS()
    {
        
    }
    
    public SingleCAS(Op<?> op)
    {
        this.op = op;
    }
    
    public Op<?> getOp()
    {
        return op;
    }
    
    public SingleCAS setOp(Op<?> op)
    {
        this.op = op;
        return this;
    }
    
    protected SingleCAS mergeOp(Op<?> op)
    {
        this.op = op;
        return this;
    }
    
    public Op<?> getOp(int index)
    {
        return index == 0 ? op : null;
    }
    
    public void fill(int[] fieldIndex)
    {
        // TODO field number validation
        fieldIndex[op.f] = 0;
    }
    
    public boolean isEmpty()
    {
        return op == null;
    }

    // message method

    public Schema<SingleCAS> cachedSchema()
    {
        return SCHEMA;
    }
    
    public static final Schema<SingleCAS> SCHEMA = new Schema<SingleCAS>()
    {

        // schema methods

        public SingleCAS newMessage()
        {
            return new SingleCAS();
        }

        public Class<SingleCAS> typeClass()
        {
            return SingleCAS.class;
        }

        public String messageName()
        {
            return SingleCAS.class.getSimpleName();
        }

        public String messageFullName()
        {
            return SingleCAS.class.getName();
        }

        public boolean isInitialized(SingleCAS message)
        {
            return true;
        }
        
        public void mergeFrom(Input input, SingleCAS message) throws IOException
        {
            CAS.mergeFrom(input, message, this);
        }
        
        @SuppressWarnings("unchecked")
        public void writeTo(Output output, SingleCAS message) throws IOException
        {
            @SuppressWarnings("rawtypes")
            final Op op = message.op;
            if(op != null)
            {
                output.writeObject(op.casFieldNumber(), op, op.cachedSchema(), 
                        false);
            }
        }

        public String getFieldName(int number)
        {
            return CAS.getFieldName(number);
        }

        public int getFieldNumber(String name)
        {
            return CAS.getFieldNumber(name);
        }
        
    };
    
    public static Schema<SingleCAS> getSchema()
    {
        return SCHEMA;
    }
    
    public static Pipe.Schema<SingleCAS> getPipeSchema()
    {
        throw new UnsupportedOperationException();
    }
}
