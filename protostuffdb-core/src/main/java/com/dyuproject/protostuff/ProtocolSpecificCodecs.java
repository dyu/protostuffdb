//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import static com.dyuproject.protostuff.DSUtils.DUMMY_OUT;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_END_GROUP;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_START_GROUP;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_TAIL_DELIMITER;
import static com.dyuproject.protostuff.WireFormat.makeTag;

import java.io.IOException;
import java.io.InputStream;

import org.codehaus.jackson.JsonParser;

import com.dyuproject.protostuff.DSUtils.BufferOverflow;
import com.dyuproject.protostuff.WriteSession.FlushHandler;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * Codecs that are tied to a protocol.
 * 
 * @author David Yu
 * @created Jan 23, 2014
 */
public final class ProtocolSpecificCodecs
{
    private ProtocolSpecificCodecs() {}
    
    // TODO disallow?
    private static final boolean ALLOW_QUOTED_INT64 = true;
    
    static final byte[] YAML_START_DIRECTIVE = new byte[]{'-', '-', '-', ' '}, 
            JSON_PREFIX = new byte[]{')', ']', '}', '\'', ',', '\n'};
    
    public static RpcCodec[] create(RpcProtocol protocol, 
            RpcWorker worker, WriteSession session, 
            int nextBufferSize)
    {
        return fill(new RpcCodec[5], protocol, 
                worker, session, 
                nextBufferSize);
    }
    
    static RpcCodec[] fill(RpcCodec[] codecs, 
            RpcProtocol protocol, 
            RpcWorker worker, WriteSession session, 
            int nextBufferSize)
    {
        codecs[0] = new Json(protocol, worker, session, 
                nextBufferSize, ALLOW_QUOTED_INT64, JsonFlags.NUMERIC);
        
        codecs[1] = new Json(protocol, worker, session, 
                nextBufferSize, ALLOW_QUOTED_INT64, JsonFlags.NUMERIC | JsonFlags.ENUMS_BY_NAME);
        
        codecs[2] = new Json(protocol, worker, session, 
                nextBufferSize, ALLOW_QUOTED_INT64, JsonFlags.ENUMS_BY_NAME);
        
        codecs[3] = new Protostuff(protocol, worker, session, 
                nextBufferSize);
        
        codecs[4] = new Yaml(protocol, worker, session, 
                nextBufferSize);
                
        return codecs;
    }
    
    /*static void consume(JsonParser parser) throws IOException
    {
        while (parser.nextToken() != null);
    }
    
    static void consume(CodedInput input) throws IOException
    {
        for(int tag = input.readRawVarint32();; tag = input.readRawVarint32())
        {
            if (input.isAtEnd())
                break;
            
            if (tag >>> TAG_TYPE_BITS == 0)
            {
                if (WIRETYPE_TAIL_DELIMITER == (tag & TAG_TYPE_MASK))
                    break;
                
                // field number is zero, which is invalid
                throw ProtobufException.invalidTag();
            }
            
            input.skipField(tag);
        }
    }*/
    
    public static final class Json extends RpcCodec implements FlushHandler
    {
        public static final class Response extends RpcResponse
        {
            public final RpcWorker worker;
            public final JsonXOutput jo;
            //private Schema<?> schema;
            
            public Response(RpcWorker worker, 
                    JsonXOutput output, RpcProtocol protocol)
            {
                super(output, worker.context, protocol);
                
                this.worker = worker;
                this.jo = output;
            }
            
            public RpcWorker getWorker()
            {
                return worker;
            }

            protected void end() throws IOException
            {
                if (jo.isLastRepeated())
                    jo.writeEndArray();
                
                jo.writeEndObject();
                // redundancy
                //worker.buf[RpcProtocol.BODY_OFFSET] = '{';
            }
            
            public <T> boolean fail(RpcError error, T message, Schema<T> fSchema) throws IOException
            {
                protocol.updateError(error, worker, jo);
                
                fSchema.writeTo(jo.use(fSchema), message);
                
                return false;
            }
            
            @Override
            public <T> Schema<?> scope(int fieldNumber, boolean push, Schema<T> schema, 
                    boolean repeated) throws IOException
            {
                return jo.scope(fieldNumber, push, schema, repeated);
            }
        };

        final JsonXOutput output;
        final Response response;
        final boolean allowQuotedInt64, numeric;
        final byte[] inBuffer;
        final char[] inCharBuffer;
        final JsonXByteArrayInput input;

        public Json(RpcProtocol protocol, 
                RpcWorker worker, WriteSession session, 
                int nextBufferSize,
                boolean allowQuotedInt64, 
                int flags)
        {
            super("json", true);
            
            this.allowQuotedInt64 = allowQuotedInt64;
            this.numeric = 0 != (flags & JsonFlags.NUMERIC);
            
            output = new JsonXOutput(session.head, DUMMY_OUT, 
                    this, nextBufferSize, flags, null);
            
            response = new Response(worker, output, protocol);
            
            inBuffer = session.head.buffer;
            
            inCharBuffer = worker.cbuf;
            
            input = new JsonXByteArrayInput(inBuffer, 0, 0, allowQuotedInt64,
                    inCharBuffer, 0, inCharBuffer.length, true, 0);
        }

        public <T> String mergeFrom(InputStream in, 
                byte[] inBuffer, int offset, int limit, 
                T message, Schema<T> schema) throws IOException
        {
            if (numeric && in == null)
            {
                switch (limit - offset)
                {
                    case 0:
                        return RpcService.ERR_MSG_MISSING_REQUEST_MESSAGE;
                    case 1:
                        return RpcService.ERR_MSG_INVALID_REQUEST_MESSAGE;
                    case 2:
                        if ('{' != inBuffer[offset] || '}' != inBuffer[offset + 1])
                            return RpcService.ERR_MSG_INVALID_REQUEST_MESSAGE;
                }
                    
                // re-use if applicable
                final JsonXByteArrayInput input = inBuffer == this.inBuffer ?
                        this.input.setBounds(offset, limit) :
                        new JsonXByteArrayInput(inBuffer, offset, limit - offset, allowQuotedInt64,
                                inCharBuffer, 0, inCharBuffer.length, true, 0);
                
                try
                {
                    JsonXIOUtil.mergeFrom(input, message, schema);
                    
                    if (!schema.isInitialized(message))
                        return RpcService.ERR_MSG_INVALID_REQUEST_MESSAGE;
                }
                catch(NumberFormatException e)
                {
                    return e.getMessage();
                }
                catch(UninitializedMessageException e)
                {
                    return e.getMessage();
                }
                catch(RpcRuntimeExceptions.Validation e)
                {
                    return e.getMessage();
                }
                
                return null;
            }
            
            // TODO re-use the parser
            final JsonParser parser = !allowQuotedInt64 ?
                    JsonIOUtil.newJsonParser(in, inBuffer, offset, limit) :
                    new QuotedDoubleToLongBitsJsonParser(JsonIOUtil.newJsonParser(in, inBuffer, offset, limit));
            
            try
            {
                JsonIOUtil.mergeFrom(parser, message, schema, numeric);
                
                if (!schema.isInitialized(message))
                    return RpcService.ERR_MSG_INVALID_REQUEST_MESSAGE;
            }
            catch(NumberFormatException e)
            {
                // consume the stream
                //if (in != null)
                //    consume(parser);
                
                return e.getMessage();
            }
            catch(UninitializedMessageException e)
            {
                // consume the stream
                //if (in != null)
                //    consume(parser);
                
                return e.getMessage();
            }
            catch(RpcRuntimeExceptions.Validation e)
            {
                // consume the stream
                //if (in != null)
                //    consume(parser);
                
                return e.getMessage();
            }
            finally
            {
                parser.close();
            }
            
            return null;
        }
        
        public <T> RpcError writeError(RpcError error, T message, Schema<T> schema, 
                RpcHeader header, RpcProtocol protocol, 
                RpcWorker worker, WriteSession session) throws IOException
        {
            final JsonXOutput output = this.output;
            response.worker.currentOutput = output;
            
            output.use(schema).reset();
            output.size = session.size;
            
            protocol.writeHeader(error, header, this, worker, output);
            
            output.writeStartObject();
            
            schema.writeTo(output, message);
            
            if (output.isLastRepeated())
                output.writeEndArray();
            
            output.writeEndObject();
            
            return error;
        }
        
        public RpcResponse newRpcResponse(final Schema<?> schema, WriteContext context, 
                RpcHeader header, RpcProtocol protocol, 
                final RpcWorker worker, WriteSession session) throws IOException
        {
            final JsonXOutput output = this.output;
            response.worker.currentOutput = output;
            
            output.use(schema).reset();
            output.size = session.size;
            
            protocol.writeHeader(RpcError.NONE, header, this, worker, output);
            
            output.writeStartObject();
            
            return response;
        }

        @Override
        public int flush(WriteSession session, byte[] buf, int offset, int len) throws IOException
        {
            throw new BufferOverflow();
        }

        @Override
        public int flush(WriteSession session, byte[] buf, int offset, int len, 
                byte[] next, int nextoffset, int nextlen) throws IOException
        {
            if (buf == next)
            {
                // utf8 string serialization that needs to move 1 space to the left
                System.arraycopy(next, nextoffset, next, nextoffset-1, nextlen);
                return nextoffset + nextlen - 1;
            }
            
            throw new BufferOverflow();
        }

        @Override
        public int flush(WriteSession session, LinkedBuffer lb, 
                byte[] buf, int offset, int len) throws IOException
        {
            if (lb == output.head)
            {
                // utf8 string serialization that needs to move 1 space to the left
                System.arraycopy(buf, offset, buf, offset-1, len);
                return offset + len - 1;
            }
            
            throw new BufferOverflow();
        }
    }

    public static final class Protostuff extends RpcCodec implements FlushHandler
    {
        public static final class Response extends RpcResponse
        {
            public final RpcWorker worker;
            public final ProtostuffOutput po;

            public Response(RpcWorker worker, 
                    ProtostuffOutput output, RpcProtocol protocol)
            {
                super(output, worker.context, protocol);
                
                this.worker = worker;
                this.po = output;
            }
            
            public RpcWorker getWorker()
            {
                return worker;
            }
            
            protected void end() throws IOException
            {
                po.tail = po.sink.writeByte(
                        (byte)WIRETYPE_TAIL_DELIMITER, 
                        po, po.tail);
            }
            
            public <T> boolean fail(RpcError error, T message, Schema<T> fSchema) throws IOException
            {
                protocol.updateError(error, worker, po);
                
                fSchema.writeTo(po, message);
                
                return false;
            }
            
            @Override
            public <T> Schema<?> scope(int fieldNumber, boolean push, Schema<T> schema, 
                    boolean repeated) throws IOException
            {
                if (push)
                {
                    po.tail = po.sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_START_GROUP), 
                            po, 
                            po.tail);
                }
                else
                {
                    po.tail = po.sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_END_GROUP), 
                            po, 
                            po.tail);
                }
                
                return null;
            }
        }
        
        final ProtostuffOutput output;
        final Response response;
        final byte[] inBuffer;
        final ByteArrayInput input;
        
        public Protostuff(RpcProtocol protocol, 
                RpcWorker worker, WriteSession session, 
                int nextBufferSize)
        {
            super("protostuff", true);
            
            output = new ProtostuffOutput(session.head, DUMMY_OUT,
                    this, nextBufferSize);
            
            response = new Response(worker, output, protocol);
            
            inBuffer = session.head.buffer;
            
            input = new ByteArrayInput(inBuffer, 0, 0, true);
        }

        public <T> String mergeFrom(InputStream in, 
                byte[] inBuffer, int offset, int limit, 
                T message, Schema<T> schema) throws IOException
        {
            if (in == null)
            {
                // re-use if applicable
                final ByteArrayInput input = inBuffer == this.inBuffer ? 
                        this.input.setBounds(offset, limit) : 
                            new ByteArrayInput(inBuffer, offset, limit, true);
                
                try
                {
                    schema.mergeFrom(input, message);
                    input.checkLastTagWas(0);
                    
                    if (!schema.isInitialized(message))
                        return RpcService.ERR_MSG_INVALID_REQUEST_MESSAGE;
                }
                catch(UninitializedMessageException e)
                {
                    return e.getMessage();
                }
                catch(RpcRuntimeExceptions.Validation e)
                {
                    return e.getMessage();
                }
                
                return null;
            }

            final CodedInput input = new CodedInput(in, inBuffer, offset, limit, 
                    true);
            try
            {
                schema.mergeFrom(input, message);
                input.checkLastTagWas(0);
                
                if (!schema.isInitialized(message))
                    return RpcService.ERR_MSG_INVALID_REQUEST_MESSAGE;
            }
            catch(UninitializedMessageException e)
            {
                // consume the stream
                //if (!input.isAtEnd())
                //    consume(input);
                
                return e.getMessage();
            }
            catch(RpcRuntimeExceptions.Validation e)
            {
                // consume the stream
                //if (!input.isAtEnd())
                //    consume(input);
                
                return e.getMessage();
            }
            
            return null;
        }
        
        public <T> RpcError writeError(RpcError error, T message, Schema<T> schema, 
                RpcHeader header, RpcProtocol protocol, 
                RpcWorker worker, WriteSession session) throws IOException
        {
            final ProtostuffOutput output = this.output;
            response.worker.currentOutput = output;
            
            output.size = session.size;
            
            protocol.writeHeader(error, header, this, worker, output);
            
            schema.writeTo(output, message);
            
            output.tail = output.sink.writeByte(
                    (byte)WIRETYPE_TAIL_DELIMITER, 
                    output, 
                    output.tail);
            
            return error;
        }

        public RpcResponse newRpcResponse(Schema<?> schema, WriteContext context, 
                RpcHeader header, RpcProtocol protocol, 
                final RpcWorker worker, WriteSession session) throws IOException
        {
            final ProtostuffOutput output = this.output;
            response.worker.currentOutput = output;
            
            output.size = session.size;
            
            protocol.writeHeader(RpcError.NONE, header, this, worker, output);
            
            return response;
        }
        
        @Override
        public int flush(WriteSession session, byte[] buf, int offset, int len) throws IOException
        {
            throw new BufferOverflow();
        }

        @Override
        public int flush(WriteSession session, byte[] buf, int offset, int len, 
                byte[] next, int nextoffset, int nextlen) throws IOException
        {
            if (buf == next)
            {
                // utf8 string serialization that needs to move 1 space to the left
                System.arraycopy(next, nextoffset, next, nextoffset-1, nextlen);
                return nextoffset + nextlen - 1;
            }
            
            throw new BufferOverflow();
        }

        @Override
        public int flush(WriteSession session, LinkedBuffer lb, 
                byte[] buf, int offset, int len) throws IOException
        {
            if (lb == output.head)
            {
                // utf8 string serialization that needs to move 1 space to the left
                System.arraycopy(buf, offset, buf, offset-1, len);
                return offset + len - 1;
            }
            
            throw new BufferOverflow();
        }
    }
    
    public static final class Yaml extends RpcCodec implements FlushHandler
    {
        public static final class Response extends RpcResponse
        {
            public final RpcWorker worker;
            public final YamlOutput yo;
            //private Schema<?> schema;
            
            public Response(RpcWorker worker, YamlOutput output, RpcProtocol protocol)
            {
                super(output, worker.context, protocol);
                
                this.worker = worker;
                this.yo = output;
            }
            
            public RpcWorker getWorker()
            {
                return worker;
            }

            protected void end() throws IOException
            {
                
            }
            
            public <T> boolean fail(RpcError error, T message, Schema<T> fSchema) throws IOException
            {
                protocol.updateError(error, worker, yo);
                
                fSchema.writeTo(yo.use(fSchema), message);
                
                return false;
            }
            
            @Override
            public <T> Schema<?> scope(int fieldNumber, boolean push, Schema<T> schema, 
                    boolean repeated) throws IOException
            {
                return yo.scope(fieldNumber, push, schema, repeated);
            }
        }
        
        final YamlOutput output;
        final Response response;

        public Yaml(RpcProtocol protocol, 
                RpcWorker worker, WriteSession session, 
                int nextBufferSize)
        {
            super("yaml", false);
            
            output = new YamlOutput(session.head, DUMMY_OUT,
                    this, nextBufferSize, null);
            
            response = new Response(worker, output, protocol);
        }
        
        public <T> String mergeFrom(InputStream in, 
                byte[] inBuffer, int offset, int len, 
                T message, Schema<T> schema) throws IOException
        {
            throw new UnsupportedOperationException();
        }
        
        public <T> RpcError writeError(RpcError error, T message, Schema<T> schema, 
                RpcHeader header, RpcProtocol protocol, 
                RpcWorker worker, WriteSession session) throws IOException
        {
            final YamlOutput output = this.output;
            response.worker.currentOutput = output;
            
            output.use(schema).reset();
            output.size = session.size;
            
            protocol.writeHeader(error, header, this, worker, output);
            
            output.tail = YamlOutput.writeTag(
                    schema.messageName(), 
                    false, 
                    output.sink, 
                    output, 
                    output.sink.writeByteArray(
                            YAML_START_DIRECTIVE, 
                            output, 
                            output.tail));
            
            schema.writeTo(output, message);
            
            return error;
        }
        
        public RpcResponse newRpcResponse(final Schema<?> schema, WriteContext context, 
                RpcHeader header, RpcProtocol protocol, 
                final RpcWorker worker, WriteSession session) throws IOException
        {
            final YamlOutput output = this.output;
            response.worker.currentOutput = output;
            
            output.use(schema).reset();
            output.size = session.size;
            
            protocol.writeHeader(RpcError.NONE, header, this, worker, output);
            
            output.tail = YamlOutput.writeTag(
                    schema.messageName(), 
                    false, 
                    output.sink, 
                    output, 
                    output.sink.writeByteArray(
                            YAML_START_DIRECTIVE, 
                            output, 
                            output.tail));
            
            return response;
        }
        
        @Override
        public int flush(WriteSession session, byte[] buf, int offset, int len) throws IOException
        {
            throw new BufferOverflow();
        }

        @Override
        public int flush(WriteSession session, byte[] buf, int offset, int len, 
                byte[] next, int nextoffset, int nextlen) throws IOException
        {
            if (buf == next)
            {
                // utf8 string serialization that needs to move 1 space to the left
                System.arraycopy(next, nextoffset, next, nextoffset-1, nextlen);
                return nextoffset + nextlen - 1;
            }
            
            throw new BufferOverflow();
        }

        @Override
        public int flush(WriteSession session, LinkedBuffer lb, 
                byte[] buf, int offset, int len) throws IOException
        {
            if (lb == output.head)
            {
                // utf8 string serialization that needs to move 1 space to the left
                System.arraycopy(buf, offset, buf, offset-1, len);
                return offset + len - 1;
            }
            
            throw new BufferOverflow();
        }
    }
    
    /*
    public static void main(String[] args) throws Exception
    {
        ProtostuffOutput output = new ProtostuffOutput(LinkedBuffer.allocate(0xFFFF));
        output.writeByteArray(1, new byte[0xFFFB], false);
        System.err.println("OUTPUT SIZE: " + output.getSize());
    }
    */
}
