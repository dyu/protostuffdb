package com.dyuproject.protostuff;

import static com.dyuproject.protostuff.NumberParser.parseInt;
import static com.dyuproject.protostuff.NumberParser.parseLong;

import java.io.IOException;

import com.dyuproject.protostuff.StringSerializer.STRING;
import com.dyuproject.protostuffdb.ValueUtil;

/**
 * Kev value pair where the field numbers are in text form.
 * <pre>
 * Synopsis:
 *     text    data with length delimiter    last entry which could be empty
 *     1:text, 2.23:hello-world-foo-bar-baz, 3::last
 * </pre> 
 */
public final class KvpTextByteArrayInput implements Input
{
    final byte[] buffer;
    private int offset, limit;
    
    private boolean empty, delim, base64;
    private int valueSize;
    
    public KvpTextByteArrayInput(byte[] buffer, int offset, int len)
    {
        this.buffer = buffer;
        this.offset = offset;
        this.limit = offset + len;
    }
    
    public KvpTextByteArrayInput reset(int offset, int limit)
    {
        this.offset = offset;
        this.limit = limit;
        return this;
    }

    public <T> int readFieldNumber(Schema<T> schema) throws IOException
    {
        if (offset == limit)
            return 0;
        
        empty = false;
        delim = false;
        base64 = false;
        
        int start = offset, end = limit, delimOffset = 0;
        byte b;
        while (offset != limit)
        {
            b = buffer[offset++];
            if (b == ',')
            {
                if (delimOffset == 0)
                    end = offset - 1;
                empty = true;
                break;
            }
            
            if (b == '.')
            {
                end = offset - 1;
                delimOffset = offset;
                continue;
            }
            
            if (b != ':')
                continue;
            
            if (offset == (delimOffset + 1))
            {
                // 7.:foo
                base64 = true;
                break;
            }
            
            if (delimOffset != 0)
            {
                delim = true;
                valueSize = parseInt(buffer, delimOffset, offset - delimOffset - 1, 10, true);
                if (limit < (offset + valueSize))
                    throw new ProtostuffException("Invalid delim size: " + valueSize);
                
                break;
            }
            
            end = offset - 1;
            if (offset != limit && buffer[offset] == ':')
            {
                delim = true;
                offset++;
                valueSize = 0;
            }
            break;
        }
        
        final int number = parseInt(buffer, start, end - start, 10, true);
        
        if (number == 0)
            throw new ProtostuffException("0 is not a valid field number.");
        
        return number;
    }

    public <T> void handleUnknownField(int fieldNumber, Schema<T> schema) throws IOException
    {
        while (offset != limit && buffer[offset++] != ',');
    }

    public <T> T mergeObject(T value, Schema<T> schema) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public boolean readBool() throws IOException
    {
        if (empty)
            return true;
        
        boolean result = buffer[offset++] != 0;
        if (offset != limit && buffer[offset++] != ',')
            throw new ProtostuffException("Malformed input.");
        
        return result;
    }

    private static boolean isValidFieldNumber(byte c)
    {
        // excludes 0
        return c > 48 && c < 58;
    }
    
    public byte[] readByteArray() throws IOException
    {
        if (empty || offset == limit)
            return ByteString.EMPTY_BYTE_ARRAY;
        
        int start = offset, end = limit;
        if (delim)
        {
            if (valueSize == 0)
                offset = limit;
            else
                offset = end = start + valueSize;
            
            if (offset != limit && buffer[offset] == ',')
                offset++;
            
            return ValueUtil.copy(buffer, start, end - start);
        }
        
        while (offset != limit)
        {
            // search for comma and peek next char to check if its a valid field number
            if (buffer[offset++] == ',' && 
                    offset != limit && isValidFieldNumber(buffer[offset]))
            {
                end = offset - 1;
                break;
            }
        }
        
        if (end == start)
            return ByteString.EMPTY_BYTE_ARRAY;
        
        if (base64)
            return B64Code.decode(buffer, start, end - start);
        
        return ValueUtil.copy(buffer, start, end - start);
    }

    public ByteString readBytes() throws IOException
    {
        return ByteString.wrap(readByteArray());
    }

    public double readDouble() throws IOException
    {
        // TODO efficiency
        
        return Double.parseDouble(readString());
    }
    
    public float readFloat() throws IOException
    {
        // TODO efficiency
        
        return Float.parseFloat(readString());
    }
    
    public int readUInt32() throws IOException
    {
        return readInt32();
    }

    public long readUInt64() throws IOException
    {
        return readInt64();
    }

    public int readInt32() throws IOException
    {
        if (empty)
            return 1;
        
        int start = offset, end = limit;
        while (offset != limit)
        {
            if (buffer[offset++] == ',')
            {
                end = offset - 1;
                break;
            }
        }
        
        return end == start ? 1 : parseInt(buffer, start, end - start, 10);
    }

    public long readInt64() throws IOException
    {
        if (empty)
            return 1;
        
        int start = offset, end = limit;
        while (offset != limit)
        {
            if (buffer[offset++] == ',')
            {
                end = offset - 1;
                break;
            }
        }
        
        return end == start ? 1 : parseLong(buffer, start, end - start, 10);
    }

    public int readEnum() throws IOException
    {
        return readInt32();
    }
    
    public int readEnumIdx(EnumMapping mapping) throws IOException
    {
        Integer idx = mapping.numberIdxMap.get(readInt32());
        return idx == null ? -1 : idx.intValue();
    }

    public int readFixed32() throws IOException
    {
        return readUInt32();
    }

    public long readFixed64() throws IOException
    {
        return readUInt64();
    }

    public int readSFixed32() throws IOException
    {
        return readInt32();
    }

    public long readSFixed64() throws IOException
    {
        return readInt64();
    }

    public int readSInt32() throws IOException
    {
        return readInt32();
    }

    public long readSInt64() throws IOException
    {
        return readInt64();
    }

    public String readString() throws IOException
    {
        if (empty || offset == limit)
            return ByteString.EMPTY_STRING;
        
        int start = offset, end = limit;
        if (delim)
        {
            if (valueSize == 0)
                offset = limit;
            else
                offset = end = start + valueSize;
            
            if (offset != limit && buffer[offset] == ',')
                offset++;
            
            return STRING.deser(buffer, start, end - start);
        }
        
        while (offset != limit)
        {
            if (buffer[offset++] == ',')
            {
                end = offset - 1;
                break;
            }
        }
        
        return end == start ? ByteString.EMPTY_STRING : 
                STRING.deser(buffer, start, end - start);
    }

    public void transferByteRangeTo(Output output, boolean utf8String, int fieldNumber, 
            boolean repeated) throws IOException
    {
        throw new UnsupportedOperationException();
    }
    
    public void transferEnumTo(Output output, EnumMapping mapping,
            int fieldNumber, boolean repeated) throws IOException
    {
        throw new UnsupportedOperationException();
    }
}
