//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

/**
 * Builds a byte string and stores them in a stack internally when you call {@link #push()}.
 * 
 * ArrayIndexOutOfBoundException will be thrown on stack overflow/underflow 
 * (The latter happens when you popped more than you pushed).
 * 
 * @author David Yu
 * @created Oct 4, 2012
 */
public final class ByteStringBuilder extends WriteSession
{

    private static RuntimeException snh()
    {
        return new RuntimeException("Should not happen.");
    }
    
    private static RuntimeException bo()
    {
        return new RuntimeException("Buffer overflow.");
    }
    
    private final int[] offsetStack;
    
    private int count;
    
    public ByteStringBuilder(LinkedBuffer buffer)
    {
        this(buffer, 15);
    }

    public ByteStringBuilder(LinkedBuffer buffer, int capacity)
    {
        super(buffer);
        
        offsetStack = new int[capacity+1];
        
        clear();
    }
    
    public ByteStringBuilder clear()
    {
        super.clear();
        
        offsetStack[0] = head.offset = head.start + 2;
        
        size = 2;
        
        // the first index is reserved
        count = 1;
        
        return this;
    }
    
    /**
     * Returns true if the stack is full.
     */
    public boolean isFull()
    {
        return count == offsetStack.length;
    }
    
    /**
     * Returns true if the stack is empty.
     */
    public boolean isEmpty()
    {
        return count == 1;
    }
    
    /**
     * Returns the internal buffer storing the keys.
     */
    public byte[] buf()
    {
        return head.buffer;
    }
    
    /**
     * Returns a copy of the entry at the top of the stack.
     * 
     * This is here for debugging purposes.
     */
    public byte[] copy()
    {
        final int len = len();
        final byte[] copy = new byte[len];
        System.arraycopy(buf(), offset(), copy, 0, len);
        return copy;
    }
    
    /**
     * Returns a copy of a previous entry from the stack.
     * 
     * This is here for debugging purposes.
     */
    public byte[] copy(int seek)
    {
        final int len = len(seek);
        final byte[] copy = new byte[len];
        System.arraycopy(buf(), offset(seek), copy, 0, len);
        return copy;
    }
    
    /**
     * Returns the start offset of the latest pushed entry.
     */
    public int offset()
    {
        return offsetStack[count - 2];
    }
    
    /**
     * Returns the start offset of a previous entry from the stack.
     * 
     * If you want to get the previous offset, pass -1.
     * If you want to get the offset before that, pass -2.
     */
    public int offset(int seek)
    {
        return offsetStack[count - 2 + seek];
    }
    
    /**
     * Returns the size and pops the stack.
     */
    public int popLen()
    {
        // the actual length
        final int len = offsetStack[count - 1] - offsetStack[count - 2];
        
        // shrink
        head.offset = offsetStack[--count - 1];
        size -= len;
        
        // the entry length
        return len - 2;
    }
    
    /**
     * Returns the size of the last pushed key.
     */
    public int len()
    {
        return offsetStack[count - 1] - offsetStack[count - 2] - 2;
    }
    
    /**
     * Returns the size of a previous entry from the stack.
     * 
     * If you want to get the previous size, pass -1.
     * If you want to get the size before that, pass -2.
     */
    public int len(int seek)
    {
        return offsetStack[count - 1 + seek] - offsetStack[count - 2 + seek] - 2;
    }
    
    /**
     * Persists the key to the stack.
     */
    public ByteStringBuilder push()
    {
        // leave room (2 bytes) for the receiver to manipulate.
        if(head.offset + 2 > head.buffer.length)
            throw bo();
        
        // limit + 2
        count += 2;
        head.offset += 2;
        offsetStack[count] = head.offset;
        
        size += 2;
        
        return this;
    }
    
    /**
     * Appends a byte.
     */
    public ByteStringBuilder append(byte value)
    {
        try
        {
            if(head != sink.writeByte(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a 8-bit int (big endian).
     */
    public ByteStringBuilder append8(int value)
    {
        return append((byte)value);
    }
    
    /**
     * Appends a 16-bit int (big endian).
     */
    public ByteStringBuilder append16(int value)
    {
        try
        {
            // big endian
            if(head != sink.writeByte((byte)(value), this, sink.writeByte((byte)((value >>> 8) & 0xFF), this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a 32-bit int (big endian).
     */
    public ByteStringBuilder append32(int value)
    {
        return append(value);
    }
    
    /**
     * Appends an int.
     */
    public ByteStringBuilder append(int value)
    {
        try
        {
            if(head != sink.writeInt32(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a long.
     */
    public ByteStringBuilder append(long value)
    {
        try
        {
            if(head != sink.writeInt64(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a float.
     */
    public ByteStringBuilder append(float value)
    {
        try
        {
            if(head != sink.writeFloat(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a double.
     */
    public ByteStringBuilder append(double value)
    {
        try
        {
            if(head != sink.writeDouble(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a boolean.
     */
    public ByteStringBuilder append(boolean value)
    {
        try
        {
            if(head != sink.writeByte(value ? (byte)1 : 0, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw byte array.
     */
    public ByteStringBuilder append(byte[] value)
    {
        return append(value, 0, value.length);
    }
    
    /**
     * Appends a raw byte array.
     */
    public ByteStringBuilder append(byte[] value, int offset, int len)
    {
        try
        {
            if(head != sink.writeByteArray(value, offset, len, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Apppends a string.
     */
    public ByteStringBuilder append(String value)
    {
        try
        {
            if(head != sink.writeStrUTF8(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
}
