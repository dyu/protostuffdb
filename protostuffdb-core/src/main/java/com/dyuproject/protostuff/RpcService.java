//========================================================================
//Copyright 2007-2010 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.dyuproject.protostuffdb.DSRuntimeExceptions;
import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * Rpc service.
 *
 * @author David Yu
 * @created Nov 27, 2010
 */
public abstract class RpcService
{
    
    public static final String METHOD_NOT_FOUND = "Method not found.",
        ERR_MSG_UNAUTHORIZED = "Unauthorized.",
        ERR_MSG_MISSING_REQUEST_MESSAGE = "Missing request message.",
        ERR_MSG_INVALID_REQUEST_MESSAGE = "Invalid request message.";
    
    /*protected static RpcError failAndEnd(RpcResponse res, 
            RpcError error, String message) throws IOException
    {
        res.fail(error, message, STATUS_MSG_SCHEMA);
        
        end(res);
        
        return error;
    }
    
    protected static RpcError failAndEnd(RpcResponse res, 
            String message, boolean runtime) throws IOException
    {
        final RpcError error = runtime ? RpcError.RUNTIME : RpcError.FAILED;
        
        res.fail(error, message, STATUS_MSG_SCHEMA);
        
        end(res);
        
        return error;
    }*/
    
    public final int id;
    
    public RpcService(int id)
    {
        this.id = id;
    }
    
    /**
     * Must be overriden by subclasses if authorization is required.
     */
    protected boolean isAuthorized(RpcHeader header)
    {
        return true;
    }
    
    /*protected void init(Datastore store, WriteContext context)
    {
        
    }*/
    
    /*protected static void end(RpcResponse response) throws IOException
    {
        response.end();
    }*/
    
    protected static RpcError end(RpcResponse response, RpcError error) 
            throws IOException
    {
        response.end();
        return error;
    }
    
    protected static RpcError err(RpcResponse response, RpcError error, String message) 
            throws IOException
    {
        response.fail(error, message, RpcService.STATUS_MSG_SCHEMA);
        response.end();
        return error;
    }
    
    public final RpcError dispatch(RpcHeader header, 
            InputStream in, byte[] inBuffer, int inOffset, int inLimit, 
            RpcCodec inCodec, 
            RpcProtocol protocol, 
            RpcCodec outCodec, 
            RpcWorker worker, WriteSession session, 
            Datastore store, WriteContext context) throws IOException
    {
        try
        {
            return handle(header, in, inBuffer, inOffset, inLimit, inCodec, protocol, 
                    outCodec, worker, session, store, context);
        }
        catch(DSRuntimeExceptions.Unauthorized e)
        {
            return header.res != null ?
                    err(header.res, RpcError.UNAUTHORIZED, e.getMessage()) :
                    
                    outCodec.writeError(RpcError.UNAUTHORIZED, 
                    e.getMessage(), RpcService.STATUS_MSG_SCHEMA, 
                    header, protocol, worker, session);
        }
        catch(DSRuntimeExceptions.Validation e)
        {
            return header.res != null ?
                    err(header.res, RpcError.INVALID, e.getMessage()) :
                    
                    outCodec.writeError(RpcError.INVALID, 
                    e.getMessage(), RpcService.STATUS_MSG_SCHEMA, 
                    header, protocol, worker, session);
        }
        catch(DSRuntimeExceptions.Runtime e)
        {
            RpcError error = DSRuntimeExceptions.Runtime.class == e.getClass() ? 
                    RpcError.RUNTIME : RpcError.FAILED;
            
            return header.res != null ?
                    err(header.res, error, e.getMessage()) :
                    
                    outCodec.writeError(
                    error, 
                    e.getMessage(), RpcService.STATUS_MSG_SCHEMA, 
                    header, protocol, worker, session);
        }
        catch(RpcRuntimeExceptions.Validation e)
        {
            return header.res != null ?
                    err(header.res, RpcError.INVALID, e.getMessage()) :
                    
                    outCodec.writeError(RpcError.INVALID, 
                    e.getMessage(), RpcService.STATUS_MSG_SCHEMA, 
                    header, protocol, worker, session);
        }
    }
    
    protected abstract RpcError handle(RpcHeader header, 
            InputStream in, byte[] inBuffer, int inOffset, int inLimit, 
            RpcCodec inCodec, 
            RpcProtocol protocol, 
            RpcCodec outCodec, 
            RpcWorker worker, WriteSession session, 
            Datastore store, WriteContext context) throws IOException;
    
    
    public static final Schema<ArrayList<String>> VALIDATION_MSG_SCHEMA = 
        new Schema<ArrayList<String>>()
    {
        // schema methods

        public Class<ArrayList<String>> typeClass()
        {
            throw new UnsupportedOperationException();
        }

        public String messageName()
        {
            return "Validation";
        }

        public String messageFullName()
        {
            return messageName();
        }

        public ArrayList<String> newMessage()
        {
            throw new UnsupportedOperationException();
        }

        public boolean isInitialized(ArrayList<String> message)
        {
            throw new UnsupportedOperationException();
        }

        public void mergeFrom(Input input, ArrayList<String> message) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public void writeTo(Output output, ArrayList<String> message) throws IOException
        {
            for(String value : message)
                output.writeString(1, value, true);
        }

        public String getFieldName(int number)
        {
            return number == 1 ? "m" : null;
        }

        public int getFieldNumber(String name)
        {
            return name.length() == 1 && name.charAt(0) == 'm' ? 1 : 0;
        }
    };

    public static final Schema<String> STATUS_MSG_SCHEMA = 
        new Schema<String>()
    {
        // schema methods

        public Class<String> typeClass()
        {
            throw new UnsupportedOperationException();
        }

        public String messageName()
        {
            return "Failure";
        }

        public String messageFullName()
        {
            return messageName();
        }

        public String newMessage()
        {
            throw new UnsupportedOperationException();
        }

        public boolean isInitialized(String message)
        {
            throw new UnsupportedOperationException();
        }

        public void mergeFrom(Input input, String message) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public void writeTo(Output output, String message) throws IOException
        {
            output.writeString(1, message, false);
        }

        public String getFieldName(int number)
        {
            return number == 1 ? "m" : null;
        }

        public int getFieldNumber(String name)
        {
            return name.length() == 1 && name.charAt(0) == 'm' ? 1 : 0;
        }
    };
    
    /*public static final CustomSchema<String> SINGLE_MSG_VALIDATION_SCHEMA = 
        new CustomSchema<String>(STATUS_MSG_SCHEMA)
    {
        public String messageName()
        {
            return SINGLE_MSG_VALIDATION_SCHEMA.messageName();
        }
        
        public void writeTo(Output output, String message) throws IOException
        {
            output.writeString(1, message, true);
        }
    };*/
    
    /*public static <Req, Res> RpcError mergeFrom(InputStream in, 
            byte[] inBuffer, int inOffset, int inLimit, RpcCodec inCodec, 
            Req request, Schema<Req> requestSchema, 
            OutputStream out, LinkedBuffer buffer, RpcCodec outCodec, 
            RpcHeader header, RpcProtocol protocol) throws IOException
    {
        final ArrayList<String> messages = RpcValidation.getMessages();
        boolean success = false;
        try
        {
            inCodec.mergeFrom(in, inBuffer, inOffset, inLimit, request, requestSchema);
            success = requestSchema.isInitialized(request);
        }
        catch(NumberFormatException e)
        {
            if(in != null)
            {
                // TODO better handling.
                throw new RuntimeException("Cannot have fail-fast validation on streaming mode.");
            }
            
            return outCodec.writeError(RpcError.FAILED, 
                    RpcProtocol.INVALID_REQUEST, STATUS_MSG_SCHEMA, 
                    header, protocol, out, buffer);
        }
        catch(UninitializedMessageException e)
        {
            if(in != null)
            {
                // TODO better handling.
                throw new RuntimeException("Cannot have fail-fast validation on streaming mode.");
            }
            
            return outCodec.writeError(RpcError.FAILED, 
                    RpcProtocol.INVALID_REQUEST, STATUS_MSG_SCHEMA, 
                    header, protocol, out, buffer);
        }
        catch(RpcRuntimeExceptions.Validation e)
        {
            if(in != null)
            {
                // TODO better handling.
                throw new RuntimeException("Cannot have fail-fast validation on streaming mode.");
            }
                
            return outCodec.writeError(RpcError.FAILED, 
                    e.getMessage(), STATUS_MSG_SCHEMA, 
                    header, protocol, out, buffer);
        }
        finally
        {
            if(!messages.isEmpty())
            {
                // the initial invalid field(s) gets priority. 
                try
                {
                    return outCodec.writeError(RpcError.INVALID, messages, 
                            VALIDATION_MSG_SCHEMA, header, protocol, out, buffer);
                }
                finally
                {
                    messages.clear();
                }
            }
        }
        
        return success ? RpcError.NONE : outCodec.writeError(RpcError.FAILED, 
                RpcProtocol.INVALID_REQUEST, STATUS_MSG_SCHEMA, 
                header, protocol, out, buffer);
    }*/

}
