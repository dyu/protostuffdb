//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

/**
 * Used for index-collection and compare-and-set operations.
 * 
 * @author David Yu
 * @created Sep 9, 2012
 */
public final class DSPipe extends Pipe
{
    
    public final DSByteArrayInput input = new DSByteArrayInput(null, 0, 0, true);

    @Override
    protected Input begin(Schema<?> pipeSchema) throws IOException
    {
        return input;
    }

    @Override
    protected void end(Schema<?> pipeSchema, Input input, boolean cleanupOnly) throws IOException
    {
        // reset
        output = null;
        
        this.input.buffer = null;
    }

}
