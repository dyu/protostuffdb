//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.ProtostuffPipe;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;


/**
 * Rpc response.
 *
 * @author David Yu
 * @created Aug 24, 2011
 */
public abstract class RpcResponse// implements Schema<RpcResponse>
{
    public final Output output;
    public final WriteContext context;
    
    protected final RpcProtocol protocol;
    
    public RpcResponse(Output output, WriteContext context, RpcProtocol protocol)
    {
        this.output = output;
        this.context = context;
        this.protocol = protocol;
    }
    
    /**
     * Call this to issue {@link RpcError} responses.
     */
    public boolean fail(String message) throws IOException
    {
        return fail(RpcError.FAILED, message, RpcService.STATUS_MSG_SCHEMA);
    }
    
    public boolean unauthorized() throws IOException
    {
        return fail(RpcError.UNAUTHORIZED, "Unauthorized.", RpcService.STATUS_MSG_SCHEMA);
    }
    
    public boolean unauthorized(String message) throws IOException
    {
        return fail(RpcError.UNAUTHORIZED, message, RpcService.STATUS_MSG_SCHEMA);
    }
    
    /**
     * Call this to issue {@link RpcError} responses.
     */
    public boolean fail(String message, boolean runtime) throws IOException
    {
        return fail(runtime ? RpcError.RUNTIME : RpcError.FAILED, message, 
                RpcService.STATUS_MSG_SCHEMA);
    }
    
    public abstract RpcWorker getWorker();
    
    protected abstract <T> boolean fail(RpcError error, T message, Schema<T> fSchema) throws IOException;
    
    public abstract <T> Schema<?> scope(int fieldNumber, boolean push, Schema<T> schema, 
            boolean repeated) throws IOException;
    
    protected abstract void end() throws IOException;
    
    public <T> void pipe(ProtostuffPipe pipe, 
            EntityMetadata<T> em, 
            byte[] key, byte[] value) throws IOException
    {
        pipe(pipe, em, em.pipeSchema, key, value, 0, value.length);
    }
    
    public <T> void pipe(ProtostuffPipe pipe, 
            EntityMetadata<T> em, Pipe.Schema<?> pipeSchema, 
            byte[] key, byte[] value) throws IOException
    {
        pipe(pipe, em, pipeSchema, key, value, 0, value.length);
    }
    
    public <T> void pipe(ProtostuffPipe pipe, 
            EntityMetadata<T> em, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        pipe(pipe, em, em.pipeSchema, key, v, voffset, vlen);
    }
    
    public <T> void pipe(ProtostuffPipe pipe, 
            EntityMetadata<T> em, Pipe.Schema<?> pipeSchema, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        try
        {
            pipeSchema.writeTo(output, pipe.em(em).set(key, v, voffset, vlen));
        }
        finally
        {
            pipe.clear();
        }
    }
    
    public <T> void pipeNested(ProtostuffPipe pipe, 
            EntityMetadata<T>em, 
            int fieldNumber, boolean repeated, 
            byte[] key, byte[] value) throws IOException
    {
        pipeNested(pipe, em, em.pipeSchema, fieldNumber, repeated, 
                key, value, 0, value.length);
    }
    
    public <T> void pipeNested(ProtostuffPipe pipe, 
            EntityMetadata<T>em, Pipe.Schema<?> pipeSchema, 
            int fieldNumber, boolean repeated, 
            byte[] key, byte[] value) throws IOException
    {
        pipeNested(pipe, em, pipeSchema, fieldNumber, repeated, 
                key, value, 0, value.length);
    }
    
    public <T> void pipeNested(ProtostuffPipe pipe, 
            EntityMetadata<T>em,
            int fieldNumber, boolean repeated, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        pipeNested(pipe, em, em.pipeSchema, fieldNumber, repeated, 
                key, v, voffset, vlen);
    }
    
    public <T> void pipeNested(ProtostuffPipe pipe, 
            EntityMetadata<T>em, Pipe.Schema<?> pipeSchema, 
            int fieldNumber, boolean repeated, 
            byte[] key, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        try
        {
            output.writeObject(fieldNumber, 
                    pipe.em(em).set(key, v, voffset, vlen), pipeSchema, 
                    repeated);
        }
        finally
        {
            pipe.clear();
        }
    }
    
    public static final Visitor<RpcResponse> PIPED_VISITOR = new Visitor<RpcResponse>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                RpcResponse res, int index)
        {
            final ProtostuffPipe pipe = res.context.pipe.set(key, v, voffset, vlen);
            try
            {
                res.output.writeObject(pipe.fieldNumber(), pipe, pipe.schema(), pipe.repeated());
            }
            catch (IOException e)
            {
                throw RpcRuntimeExceptions.pipe(e);
            }
            
            return false;
        }
    };
}
