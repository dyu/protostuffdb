//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

/**
 * The rpc error codes.
 *
 * @author David Yu
 * @created Aug 23, 2011
 */
public enum RpcError implements EnumLite<RpcError>
{
    
    /**
     * No error, operation successful.
     */
    NONE(0), 
    
    /**
     * Invalid request (message validation failed).
     */
    INVALID(1), 
    
    /**
     * Failed operation.
     */
    FAILED(2), 
    
    /**
     * Not authorized to access the resource.
     */
    UNAUTHORIZED(3), 
    
    /**
     * Runtime errors.
     */
    RUNTIME(4);
    
    public final int number;
    
    private RpcError(int number)
    {
        this.number = number;
    }
    
    public int getNumber()
    {
        return number;
    }

}
