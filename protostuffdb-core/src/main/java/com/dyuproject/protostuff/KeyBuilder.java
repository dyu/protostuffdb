//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

import com.dyuproject.protostuffdb.EntityMetadata;

/**
 * Builds keys and stores them in a stack internally when you call {@link #push()}.
 * 
 * ArrayIndexOutOfBoundException will be thrown on stack overflow/underflow 
 * (The latter happens when you popped more than you pushed).
 * 
 * @author David Yu
 * @created Sep 12, 2012
 */
public final class KeyBuilder extends WriteSession
{
    
    private static RuntimeException snh()
    {
        return new RuntimeException("Should not happen.");
    }
    
    private static RuntimeException bo()
    {
        return new RuntimeException("Buffer overflow.");
    }
    
    private final int[] offsetStack;
    
    private int count;
    
    public KeyBuilder(LinkedBuffer buffer)
    {
        this(buffer, 15);
    }

    public KeyBuilder(LinkedBuffer buffer, int capacity)
    {
        super(buffer);
        
        offsetStack = new int[capacity+1];
        
        clear();
    }
    
    public KeyBuilder clear()
    {
        super.clear();
        
        offsetStack[0] = head.offset = head.start + 2;
        
        size = 2;
        
        // the first index is reserved
        count = 1;
        
        return this;
    }
    
    /**
     * Returns true if the stack is full.
     */
    public boolean isFull()
    {
        return count == offsetStack.length;
    }
    
    /**
     * Returns true if the stack is empty.
     */
    public boolean isEmpty()
    {
        return count == 1;
    }
    
    /**
     * Returns the internal buffer storing the keys.
     */
    public byte[] buf()
    {
        return head.buffer;
    }
    
    /**
     * Returns a copy of the entry at the top of the stack.
     */
    public byte[] copy()
    {
        final int len = len();
        final byte[] copy = new byte[len];
        System.arraycopy(buf(), offset(), copy, 0, len);
        return copy;
    }
    
    /**
     * Returns a copy of a previous entry from the stack.
     */
    public byte[] copy(int seek)
    {
        final int len = len(seek);
        final byte[] copy = new byte[len];
        System.arraycopy(buf(), offset(seek), copy, 0, len);
        return copy;
    }
    
    /**
     * Returns a copy of a previous entry from the stack.
     */
    public byte[] copy(int seek, byte[] toAppend)
    {
        final int len = len(seek);
        final byte[] copy = new byte[len + toAppend.length];
        System.arraycopy(buf(), offset(seek), copy, 0, len);
        System.arraycopy(toAppend, 0, copy, len, toAppend.length);
        return copy;
    }
    
    /**
     * Returns a copy of a previous entry from the stack.
     */
    public byte[] copy(int seek, byte[] toAppend, int toRemove)
    {
        final int len = len(seek) - toRemove;
        final byte[] copy = new byte[len + toAppend.length];
        System.arraycopy(buf(), offset(seek), copy, 0, len);
        System.arraycopy(toAppend, 0, copy, len, toAppend.length);
        return copy;
    }
    
    /**
     * Returns a copy of a previous entry from the stack.
     */
    private byte[] copy(int seek, int overwriteEndSize, byte[] toAppend)
    {
        final int len = len(seek);
        final byte[] copy = new byte[len + toAppend.length];
        System.arraycopy(buf(), offset(seek), copy, 0, len - overwriteEndSize);
        System.arraycopy(toAppend, 0, copy, len, toAppend.length);
        return copy;
    }
    
    /**
     * Overwrite copy with {@code value}.
     */
    public byte[] ocopy8(int seek, int value, byte[] toAppend)
    {
        byte[] copy = copy(seek, 1, toAppend);
        copy[copy.length - toAppend.length - 1] = (byte)value;
        return copy;
    }
    
    /**
     * Overwrite copy with {@code value}.
     */
    public byte[] ocopy(int seek, int value, byte[] toAppend)
    {
        byte[] copy = copy(seek, 4, toAppend);
        IntSerializer.writeInt32(value, copy, copy.length - toAppend.length - 4);
        return copy;
    }
    
    /**
     * Overwrite copy with {@code value}.
     */
    public byte[] ocopy(int seek, long value, byte[] toAppend)
    {
        byte[] copy = copy(seek, 8, toAppend);
        IntSerializer.writeInt64(value, copy, copy.length - toAppend.length - 8);
        return copy;
    }
    
    /**
     * Overwrite copy with {@code value}.
     */
    public byte[] ocopy6Bytes(int seek, long value, byte[] toAppend)
    {
        byte[] copy = copy(seek, 6, toAppend);
        write6Bytes(value, copy, copy.length - toAppend.length - 6);
        return copy;
    }
    
    /**
     * Overwrite copy with {@code value}.
     */
    public byte[] ocopy(int seek, byte[] value, int lastValueSize, byte[] toAppend)
    {
        final int len = len(seek),
                diff = value.length - lastValueSize;
        
        final byte[] copy = new byte[len + diff + toAppend.length];
        System.arraycopy(buf(), offset(seek), copy, 0, len - lastValueSize);
        
        System.arraycopy(value, 0, 
                copy, copy.length - toAppend.length - value.length, 
                value.length);
        
        System.arraycopy(toAppend, 0, copy, copy.length - toAppend.length, toAppend.length);
        
        return copy;
    }
    
    public byte[] ocopy(int seek, String value, int lastValueSize, byte[] toAppend)
    {
        return ocopy(seek, value, lastValueSize, false, toAppend);
    }
    
    /**
     * Overwrite copy with {@code value}.
     */
    public byte[] ocopy(int seek, String value, int lastValueSize, 
            boolean nopad, byte[] toAppend)
    {
        final int len = len(seek);
        
        // push state
        final int headSize = size,
                headOffset = head.offset;
        
        // write temp value
        if (nopad)
            $append(value);
        else
            append(value);
        
        final int valueSize = size - headSize,
                diff = valueSize - lastValueSize;
        
        // restore state
        size = headSize;
        head.offset = headOffset;
        
        final byte[] copy = new byte[len + diff + toAppend.length];
        System.arraycopy(buf(), offset(seek), copy, 0, len - lastValueSize);
        
        System.arraycopy(head.buffer, headOffset, 
                copy, copy.length - toAppend.length - valueSize, 
                valueSize);
        
        System.arraycopy(toAppend, 0, copy, copy.length - toAppend.length, toAppend.length);
        
        return copy;
    }
    
    /**
     * Returns the start offset of the latest pushed entry.
     */
    public int offset()
    {
        return offsetStack[count - 2];
    }
    
    /**
     * Returns the start offset of a previous entry from the stack.
     * 
     * If you want to get the previous offset, pass -1.
     * If you want to get the offset before that, pass -2.
     */
    public int offset(int seek)
    {
        return offsetStack[count - 2 + seek];
    }
    
    /**
     * Returns the size and pops the stack.
     */
    public int popLen()
    {
        // the actual length
        final int len = offsetStack[count - 1] - offsetStack[count - 2];
        
        // shrink
        head.offset = offsetStack[--count - 1];
        size -= len;
        
        // the entry length
        return len - 2;
    }
    
    /**
     * Returns the size of the last pushed key.
     */
    public int len()
    {
        return offsetStack[count - 1] - offsetStack[count - 2] - 2;
    }
    
    /**
     * Returns the size of a previous entry from the stack.
     * 
     * If you want to get the previous size, pass -1.
     * If you want to get the size before that, pass -2.
     */
    public int len(int seek)
    {
        return offsetStack[count - 1 + seek] - offsetStack[count - 2 + seek] - 2;
    }
    
    /**
     * Persists the key to the stack.
     */
    public KeyBuilder push()
    {
        // leave room (2 bytes) for the receiver to manipulate.
        if(head.offset == head.buffer.length)
            throw bo();
        
        // limit + 2
        offsetStack[count++] = ++head.offset;
        
        size++;
        
        return this;
    }
    
    /**
     * Persists the key to the stack.
     */
    public KeyBuilder $push()
    {
        // leave room (2 bytes) for the receiver to manipulate.
        if(head.offset + 2 > head.buffer.length)
            throw bo();
        
        // limit + 2
        head.offset += 2;
        size += 2;
        
        offsetStack[count++] = head.offset;
        
        return this;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $pushRange()
    {
        return $pushRange(0xFF);
    }
    
    /**
     * Persists the key to the stack and appends the byte range {@code end}.
     */
    private KeyBuilder $pushRange(int end)
    {
        // leave room (2 bytes) for the receiver to manipulate.
        final int len = head.offset - offsetStack[count - 1];
        
        if (head.offset + 2 + len + 3 > head.buffer.length)
            throw bo();
        
        int offset = head.offset - len,
                headOffset = head.offset + 2;
        
        offsetStack[count++] = headOffset;
        
        final byte[] buffer = head.buffer;
        System.arraycopy(buffer, offset, buffer, headOffset, len);
        
        headOffset += len;
        
        buffer[headOffset++] = (byte)end;
        
        offsetStack[count++] = head.offset = headOffset + 2;
        size += (2 + len + 3);
        
        return this;
    }
    
    private static int write6Bytes(long value, byte[] buffer, int offset)
    {
        buffer[offset++] = (byte)(value >>> 40);
        buffer[offset++] = (byte)(value >>> 32);
        buffer[offset++] = (byte)(value >>> 24);
        buffer[offset++] = (byte)(value >>> 16);
        buffer[offset++] = (byte)(value >>>  8);
        buffer[offset++] = (byte)(value);
        
        return offset;
    }
    
    /**
     * Persists the key to the stack and appends the date range end.
     */
    public KeyBuilder $pushRange(long endDate)
    {
        // leave room (2 bytes) for the receiver to manipulate.
        final int len = head.offset - offsetStack[count - 1];
        
        if (head.offset + 2 + len + 3 > head.buffer.length)
            throw bo();
        
        int offset = head.offset - len,
                headOffset = head.offset + 2;
        
        offsetStack[count++] = headOffset;
        
        final byte[] buffer = head.buffer;
        System.arraycopy(buffer, offset, buffer, headOffset, len - 6);
        
        headOffset += (len - 6);
        
        headOffset = write6Bytes(endDate, buffer, headOffset);
        
        buffer[headOffset++] = (byte)0xFF;
        
        offsetStack[count++] = head.offset = headOffset + 2;
        size += (2 + len + 3);
        
        return this;
    }
    
    /**
     * Returns the offset
     */
    private int $opushRange(int overwriteEndSize, int end)
    {
        // leave room (2 bytes) for the receiver to manipulate.
        final int len = head.offset - offsetStack[count - 1];
        
        if (head.offset + 2 + len + 3 > head.buffer.length)
            throw bo();
        
        int offset = head.offset - len,
                headOffset = head.offset + 2;
        
        offsetStack[count++] = headOffset;
        
        final byte[] buffer = head.buffer;
        System.arraycopy(buffer, offset, buffer, headOffset, len - overwriteEndSize);
        
        headOffset += len;
        
        buffer[headOffset++] = (byte)end;
        
        offsetStack[count++] = head.offset = headOffset + 2;
        size += (2 + len + 3);
        
        return headOffset - overwriteEndSize - 1;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange8(int value)
    {
        head.buffer[$opushRange(1, 0xFF)] = (byte)value;
        return this;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange(int value)
    {
        IntSerializer.writeInt32(value, head.buffer, $opushRange(4, 0xFF));
        return this;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange(long value)
    {
        IntSerializer.writeInt64(value, head.buffer, $opushRange(8, 0xFF));
        return this;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange6Bytes(long value)
    {
        write6Bytes(value, head.buffer, $opushRange(6, 0xFF));
        return this;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange(byte[] value, int lastValueSize)
    {
        // leave room (2 bytes) for the receiver to manipulate.
        final int len = head.offset - offsetStack[count - 1],
                diff = value.length - lastValueSize;
        
        if (head.offset + 2 + len + 3 + diff > head.buffer.length)
            throw bo();
        
        int offset = head.offset - len,
                headOffset = head.offset + 2;
        
        offsetStack[count++] = headOffset;
        
        final byte[] buffer = head.buffer;
        System.arraycopy(buffer, offset, buffer, headOffset, len - lastValueSize);
        
        headOffset += (len - lastValueSize);
        
        System.arraycopy(value, 0, buffer, headOffset, value.length);
        headOffset += value.length;
        
        buffer[headOffset++] = (byte)0xFF;
        
        offsetStack[count++] = head.offset = headOffset + 2;
        size += (2 + len + 3);
        
        return this;
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange(String value, int lastValueSize)
    {
        return $opushRange(value, lastValueSize, false);
    }
    
    /**
     * Persists the key to the stack and appends the byte range end.
     */
    public KeyBuilder $opushRange(String value, int lastValueSize, boolean nopad)
    {
        // leave room (2 bytes) for the receiver to manipulate.
        final int len = head.offset - offsetStack[count - 1],
                copyLen = len - lastValueSize;
        
        if (head.offset + 2 + copyLen > head.buffer.length)
            throw bo();
        
        int offset = head.offset - len,
                headOffset = head.offset + 2;
        
        offsetStack[count++] = headOffset;
        
        final byte[] buffer = head.buffer;
        System.arraycopy(buffer, offset, buffer, headOffset, copyLen);
        
        head.offset = headOffset + copyLen;
        size += (2 + copyLen);
        
        if (nopad)
            $append(value);
        else
            append(value);
        
        headOffset = head.offset;
        
        if (headOffset + 3 > head.buffer.length)
            throw bo();
        
        buffer[headOffset++] = (byte)0xFF;
        
        offsetStack[count++] = head.offset = headOffset + 2;
        size += 3;
        
        return this;
    }
    
    public int headOffset()
    {
        return head.offset;
    }
    
    public int pushLen()
    {
        // leave room (2 bytes) for the receiver to manipulate.
        if(head.offset == head.buffer.length)
            throw bo();
        
        final int len = head.offset - offsetStack[count - 1];
        
        // limit + 2
        offsetStack[count++] = ++head.offset;
        
        size++;
        
        return len;
    }
    
    public int $pushLen()
    {
        // leave room (2 bytes) for the receiver to manipulate.
        if(head.offset + 2 > head.buffer.length)
            throw bo();
        
        final int len = head.offset - offsetStack[count - 1];
        
        // limit + 2
        head.offset += 2;
        size += 2;
        
        offsetStack[count++] = head.offset;
        
        return len;
    }
    
    /**
     * Appends the index prefix.
     */
    public KeyBuilder begin(int indexId, EntityMetadata<?> em)
    {
        try
        {
            if(indexId < 224)
            {
                tail = sink.writeByte((byte)indexId, this, 
                        sink.writeByte((byte)0, this, tail));
            }
            else
            {
                tail = sink.writeByte((byte)em.kind, this, 
                        sink.writeByte((byte)indexId, this, 
                                sink.writeByte((byte)0, this, tail)));
            }
            
            if(head != tail)
                throw bo();
                
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends the index prefix with a key in front.
     */
    public KeyBuilder begin(int indexId, EntityMetadata<?> em, 
            byte[] key, int keyOffset)
    {
        try
        {
            if(indexId < 224)
            {
                tail = sink.writeByteArray(key, keyOffset, 9, this, 
                        sink.writeByte((byte)indexId, this, 
                                sink.writeByte((byte)0, this, tail)));
            }
            else
            {
                tail = sink.writeByteArray(key, keyOffset, 9, this, 
                        sink.writeByte((byte)em.kind, this, 
                                sink.writeByte((byte)indexId, this, 
                                        sink.writeByte((byte)0, this, tail))));
            }
            
            if(head != tail)
                throw bo();
                
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a byte.
     */
    public KeyBuilder append(byte value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeByte(value, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a 8-bit int (big endian).
     */
    public KeyBuilder append8(int value)
    {
        return append((byte)value);
    }
    
    /**
     * Appends a 32-bit int (big endian).
     */
    public KeyBuilder append32(int value)
    {
        return append(value);
    }
    
    /**
     * Appends an int.
     */
    public KeyBuilder append(int value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeInt32(value, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a long.
     */
    public KeyBuilder append(long value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeInt64(value, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a float.
     */
    public KeyBuilder append(float value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeFloat(value, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a double.
     */
    public KeyBuilder append(double value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeDouble(value, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a boolean.
     */
    public KeyBuilder append(boolean value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeByte(value ? (byte)1 : 0, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a byte array.
     */
    public KeyBuilder append(byte[] value)
    {
        return append(value, 0, value.length);
    }
    
    /**
     * Appends a byte array.
     */
    public KeyBuilder append(byte[] value, int offset, int len)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeByteArray(value, offset, len, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a byte array from a ByteStringBuilder slice (popped).
     */
    public KeyBuilder append(ByteStringBuilder bsb)
    {
        return append(bsb.buf(), bsb.offset(), bsb.popLen());
    }
    
    /**
     * Appends a byte array from a ByteStringBuilder slice.
     */
    public KeyBuilder append(ByteStringBuilder bsb, boolean peekOnly)
    {
        return append(bsb.buf(), bsb.offset(), peekOnly ? bsb.len() : bsb.popLen());
    }
    
    /**
     * Apppends a string (converted to lower-case).
     */
    public KeyBuilder append(String value)
    {
        // TODO optimized toLowerCase?
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeStrUTF8(value.toLowerCase(), this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Apppends a string (converted to lower-case).
     */
    public int appendAndGetSize(String value)
    {
        final int headOffset = head.offset;
        append(value);
        return head.offset - headOffset;
    }
    
    /**
     * Apppends a string, no lower case.
     */
    public KeyBuilder appendNOLC(String value)
    {
        try
        {
            if(head != sink.writeByte((byte)0, this, sink.writeStrUTF8(value, this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a 6-byte long.  Caller is responsible that the most significant two bytes 
     * are not needed (usually because they are zero).
     */
    public KeyBuilder append6Bytes(long value)
    {
        final byte[] buffer = head.buffer;
        int offset = head.offset;
        if(offset + 7 > buffer.length)
            throw bo();
        
        // separator
        buffer[write6Bytes(value, buffer, offset)] = 0;
        
        head.offset += 7;
        size += 7;
        
        return this;
    }
    
    /**
     * Appends a 6-byte ts extracted from the key.
     */
    public KeyBuilder append6BTS(byte[] key)
    {
        return append6BTS(key, 0);
    }
    
    /**
     * Appends a 6-byte ts extracted from the key.
     */
    public KeyBuilder append6BTS(byte[] key, int keyOffset)
    {
        if(head.offset + 7 > head.buffer.length)
            throw bo();
        
        System.arraycopy(key, keyOffset+1, head.buffer, head.offset, 6);

        // separator
        head.buffer[head.offset+6] = 0;

        head.offset += 7;
        size += 7;
        
        return this;
    }
    
    // =================== raw ops ===================
    
    /**
     * Appends a raw byte.
     */
    public KeyBuilder $append(byte value)
    {
        try
        {
            if(head != sink.writeByte(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw 8-bit int (big endian).
     */
    public KeyBuilder $append8(int value)
    {
        return $append((byte)value);
    }
    
    /**
     * Appends a raw 16-bit int (big endian).
     */
    public KeyBuilder $append16(int value)
    {
        try
        {
            // big endian
            if(head != sink.writeByte((byte)(value), this, sink.writeByte((byte)((value >>> 8) & 0xFF), this, tail)))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw 32-bit int (big endian).
     */
    public KeyBuilder $append32(int value)
    {
        return $append(value);
    }
    
    /**
     * Appends a raw int.
     */
    public KeyBuilder $append(int value)
    {
        try
        {
            if(head != sink.writeInt32(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw long.
     */
    public KeyBuilder $append(long value)
    {
        try
        {
            if(head != sink.writeInt64(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw float.
     */
    public KeyBuilder $append(float value)
    {
        try
        {
            if(head != sink.writeFloat(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw double.
     */
    public KeyBuilder $append(double value)
    {
        try
        {
            if(head != sink.writeDouble(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw boolean.
     */
    public KeyBuilder $append(boolean value)
    {
        try
        {
            if(head != sink.writeByte(value ? (byte)1 : 0, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw byte array.
     */
    public KeyBuilder $append(byte[] value, byte[] next)
    {
        return next == null ? $append(value, 0, value.length) : 
            $append(value, 0, value.length).$append(next, 0, next.length);
    }
    
    /**
     * Appends a raw byte array.
     */
    public KeyBuilder $append(byte[] value)
    {
        return $append(value, 0, value.length);
    }
    
    /**
     * Appends a raw byte array if it is not null.
     */
    public KeyBuilder $appendIfNotNull(byte[] value)
    {
        return value == null ? this : $append(value, 0, value.length);
    }
    
    /**
     * Appends a raw byte array.
     */
    public KeyBuilder $append(byte[] value, int offset, int len)
    {
        try
        {
            if(head != sink.writeByteArray(value, offset, len, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Appends a raw byte array from a ByteStringBuilder slice (popped).
     */
    public KeyBuilder $append(ByteStringBuilder bsb)
    {
        return $append(bsb.buf(), bsb.offset(), bsb.popLen());
    }
    
    /**
     * Appends a raw byte array from a ByteStringBuilder slice.
     */
    public KeyBuilder $append(ByteStringBuilder bsb, boolean peekOnly)
    {
        return $append(bsb.buf(), bsb.offset(), peekOnly ? bsb.len() : bsb.popLen());
    }
    
    /**
     * Apppends a raw string (converted to lower-case).
     */
    public KeyBuilder $append(String value)
    {
        // TODO optimized toLowerCase?
        try
        {
            if(head != sink.writeStrUTF8(value.toLowerCase(), this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    /**
     * Apppends a string (converted to lower-case).
     */
    public int $appendAndGetSize(String value)
    {
        final int headOffset = head.offset;
        $append(value);
        return head.offset - headOffset;
    }
    
    /**
     * Apppends a raw string, no lower case.
     */
    public KeyBuilder $appendNOLC(String value)
    {
        try
        {
            if(head != sink.writeStrUTF8(value, this, tail))
                throw bo();
        }
        catch (IOException e)
        {
            throw snh();
        }
        
        return this;
    }
    
    // extras
    
    /**
     * Appends a 6-byte long.  Caller is responsible that the most significant two bytes 
     * are not needed (usually because they are zero).
     */
    public KeyBuilder $append6Bytes(long value, byte[] parentKey)
    {
        return parentKey == null ? $append6Bytes(value) : 
            $append(parentKey).$append6Bytes(value);
    }
    
    /**
     * Appends a 6-byte long.  Caller is responsible that the most significant two bytes 
     * are not needed (usually because they are zero).
     */
    public KeyBuilder $append6Bytes(long value)
    {
        final byte[] buffer = head.buffer;
        int offset = head.offset;
        if(offset + 6 > buffer.length)
            throw bo();
        
        write6Bytes(value, buffer, offset);
        
        head.offset += 6;
        size += 6;
        
        return this;
    }
    
    /**
     * Appends a 6-byte ts extracted from the key.
     */
    public KeyBuilder $append6BTS(byte[] key)
    {
        return $append6BTS(key, 0);
    }
    
    /**
     * Appends a 6-byte ts extracted from the key.
     */
    public KeyBuilder $append6BTS(byte[] key, int keyOffset)
    {
        if(head.offset + 6 > head.buffer.length)
            throw bo();
        
        System.arraycopy(key, keyOffset+1, head.buffer, head.offset, 6);
        
        head.offset += 6;
        size += 6;
        
        return this;
    }
    
    /**
     * Appends a simulated key.
     */
    public KeyBuilder $appendSK(int kind, long ts)
    {
        final byte[] buffer = head.buffer;
        int offset = head.offset;
        if(offset + 9 > buffer.length)
            throw bo();
        
        buffer[offset++] = (byte)kind;
        
        buffer[offset++] = (byte)(ts >>> 40);
        buffer[offset++] = (byte)(ts >>> 32);
        buffer[offset++] = (byte)(ts >>> 24);
        buffer[offset++] = (byte)(ts >>> 16);
        buffer[offset++] = (byte)(ts >>>  8);
        buffer[offset++] = (byte)(ts);
        
        buffer[offset++] = 0;
        buffer[offset] = 0;
        
        head.offset += 9;
        size += 9;
        
        return this;
    }
    
    /**
     * Chain utility.
     */
    public KeyBuilder $(int expr)
    {
        return this;
    }
}
