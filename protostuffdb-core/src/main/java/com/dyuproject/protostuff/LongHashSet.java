/*
  Based on CompactHashSet Copyright 2011 Ontopia Project

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
// Taken from https://github.com/Bukkit/CraftBukkit
package com.dyuproject.protostuff;

import java.util.Arrays;

public final class LongHashSet
{
    /*public static long toLong(int msw, int lsw)
    {
        return ((long)msw << 32) + lsw - Integer.MIN_VALUE;
    }

    public static int msw(long l)
    {
        return (int)(l >> 32);
    }

    public static int lsw(long l)
    {
        return (int)(l & 0xFFFFFFFF) + Integer.MIN_VALUE;
    }*/

    private final static int INITIAL_SIZE = 3;
    private final static double LOAD_FACTOR = 0.75;

    private final static long FREE = 0;
    private final static long REMOVED = Long.MIN_VALUE;

    private int freeEntries;
    private int elements;
    private long[] values;
    //private int modCount;

    public LongHashSet()
    {
        this(INITIAL_SIZE);
    }

    public LongHashSet(int size)
    {
        values = new long[(size < 1 ? 1 : size)];
        elements = 0;
        freeEntries = values.length;
        //modCount = 0;
    }
    
    public int capacity()
    {
        return values.length;
    }

    public int size()
    {
        return elements;
    }

    public boolean isEmpty()
    {
        return elements == 0;
    }

    /**
     * Returns true if the value already exists.
     */
    public boolean contains(long value)
    {
        int hash = hash(value);
        int index = (hash & 0x7FFFFFFF) % values.length;
        int offset = 1;

        // search for the object (continue while !null and !this object)
        while (values[index] != FREE && !(hash(values[index]) == hash && values[index] == value))
        {
            index = ((index + offset) & 0x7FFFFFFF) % values.length;
            offset = offset * 2 + 1;

            if (offset == -1)
            {
                offset = 2;
            }
        }

        return values[index] != FREE;
    }

    /**
     * Returns false if the value already exists.
     */
    public boolean add(long value)
    {
        int hash = hash(value);
        int index = (hash & 0x7FFFFFFF) % values.length;
        int offset = 1;
        int deletedix = -1;

        // search for the object (continue while !null and !this object)
        while (values[index] != FREE && !(hash(values[index]) == hash && values[index] == value))
        {
            // if there's a deleted object here we can put this object here,
            // provided it's not in here somewhere else already
            if (values[index] == REMOVED)
            {
                deletedix = index;
            }

            index = ((index + offset) & 0x7FFFFFFF) % values.length;
            offset = offset * 2 + 1;

            if (offset == -1)
            {
                offset = 2;
            }
        }

        if (values[index] == FREE)
        {
            if (deletedix != -1)
            { // reusing a deleted cell
                index = deletedix;
            }
            else
            {
                freeEntries--;
            }

            //modCount++;
            elements++;
            values[index] = value;

            if (1 - (freeEntries / (double)values.length) > LOAD_FACTOR)
            {
                rehash();
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Returns false when nothing is removed.
     */
    public boolean remove(long value)
    {
        int hash = hash(value);
        int index = (hash & 0x7FFFFFFF) % values.length;
        int offset = 1;

        // search for the object (continue while !null and !this object)
        while (values[index] != FREE && !(hash(values[index]) == hash && values[index] == value))
        {
            index = ((index + offset) & 0x7FFFFFFF) % values.length;
            offset = offset * 2 + 1;

            if (offset == -1)
            {
                offset = 2;
            }
        }

        if (values[index] != FREE)
        {
            values[index] = REMOVED;
            //modCount++;
            elements--;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void clear()
    {
        elements = 0;
        /*for (int ix = 0; ix < values.length; ix++)
        {
            values[ix] = FREE;
        }*/
        Arrays.fill(values, FREE);

        freeEntries = values.length;
        //modCount++;
    }

    // This method copied from Murmur3, written by Austin Appleby released under
    // Public Domain
    private static int hash(long value)
    {
        value ^= value >>> 33;
        value *= 0xff51afd7ed558ccdL;
        value ^= value >>> 33;
        value *= 0xc4ceb9fe1a85ec53L;
        value ^= value >>> 33;
        return (int)value;
    }

    private void rehash()
    {
        int gargagecells = values.length - (elements + freeEntries);
        if (gargagecells / (double)values.length > 0.05)
        {
            rehash(values.length);
        }
        else
        {
            rehash(values.length * 2 + 1);
        }
    }

    private void rehash(int newCapacity)
    {
        long[] newValues = new long[newCapacity];

        for (long value : values)
        {
            if (value == FREE || value == REMOVED)
            {
                continue;
            }

            int hash = hash(value);
            int index = (hash & 0x7FFFFFFF) % newCapacity;
            int offset = 1;

            // search for the object
            while (newValues[index] != FREE)
            {
                index = ((index + offset) & 0x7FFFFFFF) % newCapacity;
                offset = offset * 2 + 1;

                if (offset == -1)
                {
                    offset = 2;
                }
            }

            newValues[index] = value;
        }

        values = newValues;
        freeEntries = values.length - elements;
    }
}
