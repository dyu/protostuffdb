//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import static com.dyuproject.protostuff.XmlXIOUtil.HEADER;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Zip io utils.
 * 
 * @author David Yu
 * @created Sep 4, 2012
 */
public final class ZipUtil
{
    
    /**
     * Writes the contents of the map to the output stream zipped.
     */
    public static void zipAndWriteTo(OutputStream out, Map<String,byte[]> map) 
            throws IOException
    {
        final ZipOutputStream zout = new ZipOutputStream(out);
        try
        {
            writeTo(zout, map);
        }
        finally
        {
            zout.close();
        }
    }
    
    /**
     * Writes the {@code value} to the output stream zipped with the specified name.
     */
    public static void zipAndWriteTo(OutputStream out, String name, 
            byte[] data, int offset, int len) throws IOException
    {
        final ZipOutputStream zout = new ZipOutputStream(out);
        try
        {
            writeTo(zout, name, data, offset, len);
        }
        finally
        {
            zout.close();
        }
    }
    
    public static void writeTo(ZipOutputStream zout, String fileName, 
            byte[] data, int offset, int len) throws IOException
    {
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipEntry.setSize(data.length);
        zout.putNextEntry(zipEntry);
        zout.write(data, offset, len);
        zout.closeEntry();
    }
    
    public static void writeTo(ZipOutputStream zout, Map<String,byte[]> map) 
            throws IOException
    {
        for(Map.Entry<String, byte[]> entry : map.entrySet())
        {
            ZipEntry zipEntry = new ZipEntry(entry.getKey());
            byte[] data = entry.getValue();
            zipEntry.setSize(data.length);
            zout.putNextEntry(zipEntry);
            zout.write(data);
            zout.closeEntry();
        }
    }

    
    /**
     * Serializes the {@code message} to a {@link ZipOutputStream}.
     * 
     * This explicitly resets the buffer provided before and after the 
     * write to OutputStream.
     * 
     * @return the size of the message
     */
    public static <T> int writeXmlTo(ZipOutputStream zout, T message, Schema<T> schema, 
            String fileName, LinkedBuffer lb) throws IOException
    {
        try
        {
            return doWriteXmlTo(zout, message, schema, fileName, new XmlXOutput(lb, schema));
        }
        finally
        {
            lb.clear();
        }
    }
    
    /**
     * Serializes the {@code message} to a {@link ZipOutputStream}.
     * 
     * This explicitly resets the output provided before and after the 
     * write to OutputStream.
     * 
     * @return the size of the message
     */
    public static <T> int writeXmlTo(ZipOutputStream zout, T message, Schema<T> schema, 
            String fileName, XmlXOutput output) throws IOException
    {
        try
        {
            return doWriteXmlTo(zout, message, schema, fileName, output.clear());
        }
        finally
        {
            output.clear();
        }
    }
    
    /**
     * Serializes the {@code message} to a {@link ZipOutputStream}.
     * 
     * @return the size of the message
     */
    private static <T> int doWriteXmlTo(ZipOutputStream zout, T message, Schema<T> schema, 
            String fileName, XmlXOutput output) throws IOException
    {
        final String name = schema.messageName();
        // header and start root element
        output.tail = output.sink.writeByte(XmlXOutput.END_TAG, output, 
                output.sink.writeStrAscii(name, output, 
                        output.sink.writeByte(XmlXOutput.START_TAG, output, 
                                output.sink.writeByteArray(HEADER, output, output.tail))));
        
        schema.writeTo(output.use(schema), message);
        
        // end root element
        output.tail = output.sink.writeByte(XmlXOutput.END_TAG, output, 
                output.sink.writeStrAscii(name, output, 
                        output.sink.writeByteArray(XmlXOutput.START_SLASH_TAG, output, output.tail)));
        
        if(output.head == output.tail)
        {
            // single flush
            ZipUtil.writeTo(zout, fileName, output.head.buffer, 
                    output.head.start, output.size);
        }
        else
        {
            // To be on the safe side 
            // not sure how zip outputstream works if data cannot 
            // be written in a single chunk
            byte[] data = output.toByteArray();
            ZipUtil.writeTo(zout, fileName, data, 0, data.length);
        }
        
        return output.size;
    }
}
