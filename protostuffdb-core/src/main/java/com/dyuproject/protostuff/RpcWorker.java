//================================================================================
//Copyright (c) 2011, David Yu
//All rights reserved.
//--------------------------------------------------------------------------------
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of protostuff nor the names of its contributors may be used
//    to endorse or promote products derived from this software without
//    specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//================================================================================


package com.dyuproject.protostuff;

import java.text.SimpleDateFormat;
import java.util.Date;

import protostuffdb.Jni;

import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.LsmdbDatastore;
import com.dyuproject.protostuffdb.LsmdbDatastoreManager;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * The rpc worker.
 *
 * @author David Yu
 * @created Jan 6, 2012
 */
public final class RpcWorker
{
    
    public static final boolean USE_SOCKET_JNI = Boolean.parseBoolean(
            System.getProperty("protostuff.use_socket_jni", "true"));
    
    static final ThreadLocal<RpcWorker> __local = new ThreadLocal<RpcWorker>();
    
    /**
     * For testing purposes only.
     */
    public static void set(RpcWorker worker)
    {
        __local.set(worker);
    }
    
    public static RpcWorker get()
    {
        return __local.get();
    }
    
    /**
     * Formatters.
     */
    public final SimpleDateFormat 
            sdfPath = new SimpleDateFormat("YYYY-MM-dd_HH-mm-ss"),
            sdfPrintable = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    
    public final int id;
    public final byte[] buf;
    public final char[] cbuf;
    public final RpcServer server;
    public final RpcCodec[] jniCodecs;
    
    public final WriteSession session;
    
    // re-use these components
    public final KvpTextByteArrayInput kvpInput;
    
    public final RpcLogin login = new RpcLogin();
    
    public final RpcHeader header = new RpcHeader();
    public final WriteContext context;
    
    /**
     * Current output.
     */
    public WriteSession currentOutput;
    
    /**
     * Pointer to native worker's JniContext
     */
    long ptr;
    
    //final RpcProtocol.Delegate protocolDelegate;
    
    //final long[] stats = new long[RpcError.values().length];
    
    //int sfd = 0;
    
    public RpcWorker(int id, byte[] buf, char[] cbuf, RpcServer server, WriteContext context)
    {
        this.id = id;
        this.buf = buf;
        this.cbuf = cbuf;
        
        // start at 7
        // 0xFF 0xFF 0xFF 0xFF [ 0 ,
        this.session = new WriteSession(LinkedBuffer.use(buf, RpcProtocol.BODY_OFFSET));
        
        this.server = server;
        this.context = context;
        
        kvpInput = new KvpTextByteArrayInput(buf, 0, 0);
        
        jniCodecs = ProtocolSpecificCodecs.create(RpcProtocol.DEFAULT, 
                this, session, 
                session.nextBufferSize);
        
        //protocolDelegate = null;
    }
    
    public void init(long ptr)
    {
        if (this.ptr != 0)
            throw new RuntimeException("ptr already set!");
        
        this.ptr = ptr;
        context.stream.ptr = ptr;
    }
    
    public long getPtr()
    {
        return ptr;
    }
    
    /**
     * Returns the backup name if successful, otherwise null.
     */
    public String backup(String storeName)
    {
        String backupName = sdfPath.format(new Date());
        return backup(storeName, backupName) ? backupName : null;
    }
    
    public byte[] $decrypt(byte[] data)
    {
        return $decrypt(data, 0, data.length);
    }
    
    public byte[] $decrypt(byte[] data, int offset, int len)
    {
        return Jni.decryptData(data, offset, len, this);
    }
    
    public byte[] $encrypt(byte[] data)
    {
        return $encrypt(data, 0, data.length);
    }
    
    public byte[] $encrypt(byte[] data, int offset, int len)
    {
        return Jni.encryptData(data, offset, len, this);
    }
    
    /**
     * Returns true if the store with the provided name is backed up successfully.
     */
    public boolean backup(String storeName, String backupName)
    {
        return backup(server.storeManager.getStore(storeName), backupName);
    }
    
    /**
     * Returns the backup name if successful, otherwise null.
     */
    public String backup(Datastore store)
    {
        String backupName = sdfPath.format(new Date());
        return backup(store, backupName) ? backupName : null;
    }
    
    /**
     * Returns true if the store is backed up successfully.
     */
    public boolean backup(Datastore store, String backupName)
    {
        return LsmdbDatastoreManager.backup((LsmdbDatastore)store, backupName, 
                context.stream);
    }
    
    void clear()
    {
        header.reset();
        session.clear();
        
        if (currentOutput != null)
        {
            currentOutput.clear();
            currentOutput = null;
        }
    }
}