//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;
import java.io.OutputStream;

import com.dyuproject.protostuff.WriteSession.FlushHandler;
import com.dyuproject.protostuffdb.ValueUtil;

/**
 * For reading and writing data from/to native code.
 * 
 * @author David Yu
 * @created Jan 3, 2014
 */
public final class JniStream extends WriteSession implements FlushHandler
{
    static final boolean USE_UNSAFE_BAO = Boolean.getBoolean("jni.unsafe_bao");
    
    public static final sun.misc.Unsafe UNSAFE = initUnsafe();
    
    public static final int BYTE_ARRAY_OFFSET = UNSAFE.arrayBaseOffset(byte[].class);
    public static final int BAO = USE_UNSAFE_BAO ? BYTE_ARRAY_OFFSET : 0;
    
    /* ================================================== */
    
    public static final int TERMINATOR = 0xFFFF;
    
    /* ================================================== */
    // wire types
    
    public static final int WIRETYPE_STRING           = 0;
    public static final int WIRETYPE_BYTES            = 1;
    public static final int WIRETYPE_UINT8            = 2;
    public static final int WIRETYPE_UINT16           = 3;
    public static final int WIRETYPE_UINT32           = 4;
    public static final int WIRETYPE_UINT64           = 5;
    public static final int WIRETYPE_FLOAT            = 6;
    public static final int WIRETYPE_DOUBLE           = 7;
    
    public static final int WIRETYPE_K                = 8;
    //public static final int WIRETYPE_V                = 9;
    public static final int WIRETYPE_KV               = 9;
    public static final int WIRETYPE_KVAO             = 10; // kv with absolute offset
    public static final int WIRETYPE_KVRO             = 11; // kv with relative offset
    public static final int WIRETYPE_KVRKP            = 12; // kv with relative offset, replace key prefix
    
    public static final int WIRETYPE_CHUNK            = 13;
    public static final int WIRETYPE_LIMIT            = 14;
    public static final int WIRETYPE_END              = 15;
    
    static final int TAG_TYPE_BITS = 4;
    static final int TAG_TYPE_MASK = (1 << TAG_TYPE_BITS) - 1;
    
    /* ================================================== */
    // error types
    
    public static final int ERR_RUNTIME                       = 1;
    public static final int ERR_OPERATION                     = 2;
    public static final int ERR_NOT_ENOUGH_SPACE_TO_WRITE     = 3;
    
    public static final int ERR_NOT_EXPECTING_MORE_ARGS       = 4;
    public static final int ERR_EXPECTED_MORE_ARGS            = 5;
    public static final int ERR_EXPECTED_END_OR_LIMIT         = 6;
    public static final int ERR_EXPECTED_STRING               = 7;
    public static final int ERR_EXPECTED_BYTES                = 8;
    public static final int ERR_EXPECTED_UINT8                = 9;
    public static final int ERR_EXPECTED_UINT16               = 10;
    public static final int ERR_EXPECTED_UINT32               = 11;
    public static final int ERR_EXPECTED_UINT64               = 12;
    public static final int ERR_EXPECTED_FLOAT                = 13;
    public static final int ERR_EXPECTED_DOUBLE               = 14;

    public static final int ERR_EXPECTED_K                    = 15;
    public static final int ERR_EXPECTED_V                    = 16;
    public static final int ERR_EXPECTED_KV_VARIANT           = 17;
    
    public static final int ERR_EMPTY_STRING                  = 18;
    public static final int ERR_EMPTY_BYTES                   = 19;
    
    /* ================================================== */
    
    private static sun.misc.Unsafe initUnsafe()
    {
        try
        {
            java.lang.reflect.Field f = 
                sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
            
            f.setAccessible(true);
            
            return (sun.misc.Unsafe)f.get(null);
        }
        catch(Exception e)
        {
            // ignore
            
            /* android 3.x
            try
            {
                java.lang.reflect.Field f = 
                        sun.misc.Unsafe.class.getDeclaredField("THE_ONE");
                    
                f.setAccessible(true);
                
                return (sun.misc.Unsafe)f.get(null);
            }
            catch(Exception e1)
            {
                // ignore
            }*/
        }
        
        return sun.misc.Unsafe.getUnsafe();
    }
    
    /* ================================================== */
    
    private static final byte[] CLEAR = new byte[8], 
            // extra zero for null terminator
            EMPTY_STRING = new byte[]{WIRETYPE_STRING, 0, 0, 0}, 
            EMPTY_BYTES = new byte[]{WIRETYPE_BYTES, 0, 0};
    
    public static final int OFFSET_CTRL = 1, 
            OFFSET_BUFLEN = OFFSET_CTRL + 3, 
            OFFSET_PTRS = OFFSET_BUFLEN + 2, 
            OFFSET_RC = OFFSET_PTRS + 24, 
            OFFSET_WC = OFFSET_RC + 2, 
            OFFSET_KVAO = OFFSET_WC + 2, 
            OFFSET_START = OFFSET_KVAO + 4;
    
    int errType, headerOffset, previousOffset, reservedSize;
    JniOp op;
    
    /**
     * Pointer to native worker's JniContext
     */
    long ptr;

    public JniStream(byte[] buf, OutputStream out)
    {
        // the first byte is reserved
        // the next byte is for the end/continue writetype
        // the next 2 bytes is the end offset (int16)
        // the next 2 bytes is the length of the buffer (int16)
        // the next three 8s are for the ptrs: db, writebatch, iterator
        // the next 4 bytes is state written by native code
        // the last 4 bytes is for kvao
        super(LinkedBuffer.use(buf, OFFSET_START), out);
        
        // write the buf len
        IntSerializer.writeInt16LE(buf.length, buf, OFFSET_BUFLEN);
    }
    
    public void setReservedSize(int reservedSize)
    {
        this.reservedSize = reservedSize;
    }
    
    public int getErrType()
    {
        return errType;
    }
    
    public byte[] buf()
    {
        return head.buffer;
    }
    
    public int offset()
    {
        return head.offset;
    }
    
    public boolean isHeadPointingTo(byte[] buf, int start)
    {
        return head.buffer == buf && head.start == start;
    }
    
    public int readWcOffset()
    {
        return ValueUtil.toInt16LE(head.buffer, OFFSET_WC);
    }
    
    public long readPtr(int offset)
    {
        return ValueUtil.toInt64LE(head.buffer, offset);
    }
    
    public JniStream writePtr(long ptr, int offset)
    {
        IntSerializer.writeInt64LE(ptr, head.buffer, offset);
        return this;
    }
    
    public int read8(int offset)
    {
        return ValueUtil.toInt16LE(head.buffer, offset);
    }
    
    public int read16(int offset)
    {
        return ValueUtil.toInt16LE(head.buffer, offset);
    }
    
    public int read32(int offset)
    {
        return ValueUtil.toInt32LE(head.buffer, offset);
    }
    
    public long read64(int offset)
    {
        return ValueUtil.toInt64LE(head.buffer, offset);
    }
    
    public WriteSession clear()
    {
        throw new UnsupportedOperationException();
    }
    
    @Override
    protected int flush(byte[] buf, int offset, int len) 
            throws IOException
    {
        //System.err.println((offset + len) + " == " + head.offset);
        
        final int sizeToMove = offset + len - previousOffset, 
                resumeOffset = headerOffset + sizeToMove;
        
        /*System.err.println(previousOffset + 
                " | sizeToMove: " + sizeToMove + 
                " | co: " + (offset + len) + 
                " | ro: " + resumeOffset + 
                " | ho: " + headerOffset);*/
        
        chunk(previousOffset);
        
        if (!op.exec(buf, ptr))
        {
            errType = head.buffer[0];
            head.buffer[0] = 0;
            
            throw new RuntimeException("Operation failed: " + op + " " + errType);
        }
        
        // move to the left
        System.arraycopy(buf, previousOffset, buf, headerOffset, sizeToMove);
        previousOffset = headerOffset;
        
        return resumeOffset;
    }

    @Override
    protected int flush(byte[] buf, int offset, int len, 
            byte[] next, int nextoffset, int nextlen)
            throws IOException
    {
        if(buf == next)
        {
            // utf8 string serialization that needs to move 1 space to the left
            System.arraycopy(next, nextoffset, next, nextoffset-1, nextlen);
            return nextoffset + nextlen - 1;
        }
        
        // TODO validate that nextlen must be less than 8192
        
        //System.err.println((offset + len) + " == " + head.offset);
        
        // next is either the key or the value
        final int sizeToMove = offset + len - previousOffset, 
                resumeOffset = headerOffset + sizeToMove;
        
        if(resumeOffset + nextlen > buf.length)
            throw new RuntimeException("value too large: " + nextlen + " | " + op);
        
        chunk(previousOffset);
        
        if (!op.exec(buf, ptr))
        {
            errType = head.buffer[0];
            head.buffer[0] = 0;
            
            throw new RuntimeException("Operation failed: " + op + " " + errType);
        }
        
        // move to the left
        System.arraycopy(buf, previousOffset, buf, headerOffset, sizeToMove);
        previousOffset = headerOffset;
        
        // copy
        System.arraycopy(next, nextoffset, buf, resumeOffset, nextlen);
        
        return resumeOffset + nextlen;
    }

    @Override
    protected int flush(LinkedBuffer lb, byte[] buf, int offset, int len) 
            throws IOException
    {
        if(lb == head)
        {
            // utf8 string serialization that needs to move 1 space to the left
            System.arraycopy(buf, offset, buf, offset-1, len);
            return offset + len - 1;
        }
        
        // string serialized via a session buffer
        if(head.offset + len > head.buffer.length)
            throw new RuntimeException("value too large: " + len + " | " + op);
        
        System.arraycopy(buf, offset, head.buffer, head.offset, len);
        head.offset += len;
        
        return offset;
    }
    
    public int flush(WriteSession session, 
            byte[] buf, int offset, int len) throws IOException
    {
        if(session.head != head)
            throw new RuntimeException("Misconfiguration.");
        
        chunk(offset + len);
        
        if (!op.exec(buf, ptr))
        {
            errType = head.buffer[0];
            head.buffer[0] = 0;
            
            throw new RuntimeException("Operation failed: " + op + " " + errType);
        }
        
        return offset;
    }

    public int flush(WriteSession session, 
            byte[] buf, int offset, int len, 
            byte[] next, int nextoffset, int nextlen) throws IOException
    {
        if(session.head != head)
            throw new RuntimeException("Misconfiguration.");
        
        if(buf == next)
        {
            // utf8 string serialization that needs to move 1 space to the left
            System.arraycopy(next, nextoffset, next, nextoffset-1, nextlen);
            return nextoffset + nextlen - 1;
        }
        
        if(offset + nextlen > buf.length)
            throw new RuntimeException("value too large: " + nextlen + " | " + op);
        
        chunk(offset + len);
        
        if (!op.exec(buf, ptr))
        {
            errType = head.buffer[0];
            head.buffer[0] = 0;
            
            throw new RuntimeException("Operation failed: " + op + " " + errType);
        }
        
        // move to the left
        System.arraycopy(next, nextoffset, buf, offset, nextlen);
        
        return offset + nextlen;
    }

    public int flush(WriteSession session, 
            LinkedBuffer lb, 
            byte[] buf, int offset, int len) throws IOException
    {
        if(session.head != head)
            throw new RuntimeException("Misconfiguration.");
        
        return flush(lb, buf, offset, len);
    }
    
    public JniStream start(JniOp op, int kvaoSize)
    {
        //super.clear();
        head.next = null;
        tail = head;
        size = kvaoSize;
        
        // update head.offset
        previousOffset = headerOffset = head.offset = head.start + kvaoSize;
        
        // clear (except the last 2 bytes)
        System.arraycopy(CLEAR, 0, head.buffer, OFFSET_RC, 6);
        
        // write the kvao value size
        IntSerializer.writeInt16LE(kvaoSize, head.buffer, head.start - 2);
        
        this.op = op;
        
        return this;
    }
    
    public JniStream start(JniOp op)
    {
        if (reservedSize != 0)
            return start(op, reservedSize);
        
        //super.clear();
        tail = head.clear();
        size = 0;
        
        // reset
        previousOffset = headerOffset = head.start;
        
        // clear
        System.arraycopy(CLEAR, 0, head.buffer, OFFSET_RC, 8);
        
        this.op = op;
        
        return this;
    }
    
    public JniStream end()
    {
        head.buffer[OFFSET_CTRL] = (byte)WIRETYPE_END;
        IntSerializer.writeInt16LE(head.offset, head.buffer, OFFSET_CTRL + 1);
        
        return this;
    }
    
    public JniStream limit()
    {
        head.buffer[OFFSET_CTRL] = (byte)WIRETYPE_LIMIT;
        IntSerializer.writeInt16LE(head.offset, head.buffer, OFFSET_CTRL + 1);
        
        return this;
    }
    
    private JniStream chunk(int offset)
    {
        head.buffer[OFFSET_CTRL] = (byte)WIRETYPE_CHUNK;
        IntSerializer.writeInt16LE(offset, head.buffer, OFFSET_CTRL + 1);
        
        return this;
    }
    
    public JniStream endHeader()
    {
        headerOffset = head.offset;
        return this;
    }
    
    public boolean push()
    {
        if (op.exec(head.buffer, ptr))
            return true;
        
        errType = head.buffer[0];
        head.buffer[0] = 0;
        
        return false;
    }
    
    /*public JniStream writeBool(boolean value) throws IOException
    {
        return write8(value ? 1 : 0);
    }*/
    
    public JniStream write8(int value) throws IOException
    {
        tail = sink.writeByte(
                (byte)value, 
                this, 
                sink.writeByte(
                        (byte)WIRETYPE_UINT8, 
                        this, 
                        tail));
        
        return this;
    }
    
    public JniStream write16(int value) throws IOException
    {
        tail = sink.writeInt16LE(
                value, 
                this, 
                sink.writeByte(
                        (byte)WIRETYPE_UINT16, 
                        this, 
                        tail));
        
        return this;
    }
    
    public JniStream write32(int value) throws IOException
    {
        tail = sink.writeInt32LE(
                value, 
                this, 
                sink.writeByte(
                        (byte)WIRETYPE_UINT32, 
                        this, 
                        tail));
        
        return this;
    }
    
    public JniStream write64(long value) throws IOException
    {
        tail = sink.writeInt64LE(
                value, 
                this, 
                sink.writeByte(
                        (byte)WIRETYPE_UINT64, 
                        this, 
                        tail));
        
        return this;
    }
    
    public JniStream writeString(String value) throws IOException
    {
        if(0 == value.length())
        {
            tail = sink.writeByteArray(EMPTY_STRING, this, tail);
            return this;
        }
        
        tail = sink.writeByte(
                (byte)0, // null character
                this, 
                sink.writeStrUTF8FixedDelimited(
                        value, true, 
                        this, 
                        sink.writeByte(
                                (byte)WIRETYPE_STRING, 
                                this, 
                                tail)));
        
        return this;
    }
    
    public JniStream writeString(byte[] value) throws IOException
    {
        if(0 == value.length)
        {
            tail = sink.writeByteArray(EMPTY_BYTES, this, tail);
            return this;
        }
        
        return writeString(value, 0, value.length);
    }
    
    public JniStream writeString(byte[] value, int offset, int len) throws IOException
    {
        tail = sink.writeByte(
                (byte)0, // null character
                this, 
                sink.writeByteArray(
                        value, offset, len,
                        this, 
                        sink.writeInt16LE(
                                len, 
                                this, 
                                sink.writeByte(
                                        (byte)WIRETYPE_STRING, 
                                        this, 
                                        tail))));
        return this;
    }
    
    public JniStream write(byte[] value) throws IOException
    {
        if(0 == value.length)
        {
            tail = sink.writeByteArray(EMPTY_BYTES, this, tail);
            return this;
        }
        
        return write(value, 0, value.length);
    }
    
    public JniStream write(byte[] value, int offset, int len) throws IOException
    {
        tail = sink.writeByteArray(
                value, offset, len,
                this, 
                sink.writeInt16LE(
                        len, 
                        this, 
                        sink.writeByte(
                                (byte)WIRETYPE_BYTES, 
                                this, 
                                tail)));
        
        return this;
    }
    
    public JniStream writeK(byte[] k) throws IOException
    {
        return writeK(k, 0, k.length);
    }
    
    public JniStream writeK(byte[] k, int koffset, int klen) throws IOException
    {
        previousOffset = head.offset;
        
        sink.writeByteArray(k, koffset, klen, 
                this, 
                sink.writeByte((byte)klen, 
                        this, 
                        sink.writeByte((byte)WIRETYPE_K, 
                                this, 
                                head)));
        
        return this;
    }
    
    public JniStream $writeK(byte[] k)
    {
        try
        {
            return writeK(k, 0, k.length);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
    }
    
    public JniStream $writeK(byte[] k, int koffset, int klen)
    {
        try
        {
            return writeK(k, koffset, klen);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
    }
    
    public JniStream writeKV(byte[] k, byte[] v) throws IOException
    {
        return writeKV(k, 0, k.length, 
                v, 0, v.length);
    }
    
    public JniStream writeKV(byte[] k, int koffset, int klen, 
            byte[] v, int voffset, int vlen) throws IOException
    {
        previousOffset = head.offset;
        
        sink.writeByteArray(v, voffset, vlen, 
                this, 
                sink.writeByteArray(k, koffset, klen, 
                        this, 
                        sink.writeInt16LE(vlen, 
                                this, 
                                sink.writeInt16LE(klen, 
                                        this, 
                                        sink.writeByte((byte)WIRETYPE_KV, 
                                                this, 
                                                head)))));
        
        return this;
    }
    
    public JniStream $writeKV(byte[] k, byte[] v)
    {
        try
        {
            return writeKV(k, 0, k.length, 
                    v, 0, v.length);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
    }
    
    public JniStream $writeKV(byte[] k, int koffset, int klen, 
            byte[] v, int voffset, int vlen)
    {
        try
        {
            return writeKV(k, koffset, klen, v, voffset, vlen);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
    }
    
    public JniStream writeKVAO(byte[] k, int koffset, int klen, 
            int kvAbsoluteOffset) throws IOException
    {
        previousOffset = head.offset;
        
        sink.writeByteArray(k, koffset, klen, 
                this, 
                sink.writeInt16LE(kvAbsoluteOffset, 
                        this, 
                        sink.writeInt16LE(klen, 
                                this, 
                                sink.writeByte((byte)WIRETYPE_KVAO, 
                                        this, 
                                        head))));
        
        return this;
    }
    
    public JniStream $writeKVAO(byte[] k, int kvAbsoluteOffset)
    {
        try
        {
            return writeKVAO(k, 0, k.length, kvAbsoluteOffset);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
    }
    
    public JniStream $writeKVAO(byte[] k, int koffset, int klen, 
            int kvAbsoluteOffset)
    {
        try
        {
            return writeKVAO(k, koffset, klen, kvAbsoluteOffset);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
    }
}
