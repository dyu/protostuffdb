//========================================================================
//Copyright 2007-2010 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;
import java.io.InputStream;

import com.dyuproject.protostuffdb.WriteContext;


/**
 * Fastfunc codec.
 *
 * @author David Yu
 * @created Nov 29, 2010
 */
public abstract class RpcCodec
{
    
    public final String format;
    public final boolean readable;
    
    public RpcCodec(String format, boolean readable)
    {
        this.format = format;
        this.readable = readable;
    }
    
    public RpcResponse getRpcResponse(Schema<?> schema, WriteContext context, 
            RpcHeader header, RpcProtocol protocol, 
            RpcWorker worker, WriteSession session) throws IOException
    {
        return header.res = newRpcResponse(schema, context, header, protocol, worker, 
                session);
    }
    
    protected abstract RpcResponse newRpcResponse(Schema<?> schema, WriteContext context, 
            RpcHeader header, RpcProtocol protocol, 
            RpcWorker worker, WriteSession session) throws IOException;
    
    public abstract <T> RpcError writeError(RpcError error, T message, Schema<T> schema, 
            RpcHeader header, RpcProtocol protocol, 
            RpcWorker worker, WriteSession session) throws IOException;
    
    public abstract <T> String mergeFrom(InputStream in, 
            byte[] inBuffer, int offset, int limit, 
            T message, Schema<T> schema) throws IOException;
    
    /*public abstract <T> void writeTo(OutputStream out, LinkedBuffer buffer, 
            T message, Schema<T> schema) throws IOException;*/

}
