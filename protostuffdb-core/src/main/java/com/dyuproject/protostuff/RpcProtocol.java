//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Rpc protocol.
 *
 * @author David Yu
 * @created Aug 24, 2011
 */
public enum RpcProtocol
{
    DEFAULT
    {
        public RpcError authenticate(InputStream in, byte[] uwsgiBuf, int len, 
                RpcWorker worker, WriteSession session) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public RpcError handle(InputStream in, byte[] uwsgiBuf, int len, 
                RpcWorker worker, WriteSession session, boolean fastfunc) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        public void updateError(RpcError error, 
                RpcWorker worker, WriteSession session)
        {
            session.head.buffer[5] = (byte)(error.number + 48);
        }

        public void writeHeader(RpcError error, 
                RpcHeader header, RpcCodec outCodec, 
                RpcWorker worker, WriteSession session) throws IOException
        {
            byte[] buf = session.head.buffer;
            buf[4] = '[';
            buf[5] = (byte)(error.number + 48);
            buf[6] = ',';
        }
        
        public void writeHeaderZeroContent(final RpcWorker worker, 
                final WriteSession session) throws IOException
        {
            byte[] buf = session.head.buffer;
            buf[4] = '[';
            buf[5] = '0';
            buf[6] = ',';
            
            buf[7] = '0';
            session.head.offset++;
            session.size++;
        }

        public <T> T parseFrom(RpcCodec inCodec, 
                InputStream in, byte[] inBuffer, int offset, int limit, 
                Schema<T> schema, 
                RpcHeader header, RpcCodec outCodec, 
                RpcWorker worker, WriteSession session) throws IOException
        {
            if (offset == limit)
            {
                outCodec.writeError(RpcError.INVALID, 
                        MISSING_REQUEST_BODY, RpcService.STATUS_MSG_SCHEMA, 
                        header, this, worker, session);
                
                return null;
            }
            
            final T message = schema.newMessage();
            String errMsg = inCodec.mergeFrom(in, inBuffer, offset, limit, 
                    message, schema);
            
            if (errMsg != null)
            {
                outCodec.writeError(RpcError.INVALID, 
                        errMsg, RpcService.STATUS_MSG_SCHEMA, 
                        header, this, worker, session);
                
                return null;
            }
            
            if (!RpcValidation.FAIL_FAST)
            {
                final ArrayList<String> validationMessages = RpcValidation.getMessages();
                if(!validationMessages.isEmpty())
                {
                    try
                    {
                        outCodec.writeError(RpcError.INVALID, 
                                validationMessages, 
                                RpcService.VALIDATION_MSG_SCHEMA, 
                                header, this, worker, session);
                    }
                    finally
                    {
                        validationMessages.clear();
                    }
                    
                    return null;
                }
            }
            
            return message;
        }
    },
    /*DELEGATE
    {

        @Override
        public RpcError authenticate(InputStream in, byte[] buf, int len, RpcWorker worker,
                WriteSession session) throws IOException
        {
            return worker.protocolDelegate.authenticate(in, buf, len, worker, session);
        }

        @Override
        public RpcError handle(InputStream in, byte[] buf, int len, RpcWorker worker,
                WriteSession session, boolean fastfunc) throws IOException
        {
            return worker.protocolDelegate.handle(in, buf, len, worker, session, fastfunc);
        }

        @Override
        public void updateError(RpcError error, RpcWorker worker, WriteSession session)
        {
            worker.protocolDelegate.updateError(error, worker, session);
        }

        @Override
        public <T extends WriteSession> void writeHeader(RpcError error, RpcHeader header,
                RpcCodec outCodec, RpcWorker worker, T session) throws IOException
        {
            worker.protocolDelegate.writeHeader(error, header, outCodec, worker, session);
        }

        @Override
        public void writeHeaderZeroContent(RpcWorker worker, WriteSession session)
                throws IOException
        {
            worker.protocolDelegate.writeHeaderZeroContent(worker, session);
        }

        @Override
        public <T> T parseFrom(RpcCodec inCodec, InputStream in, byte[] inBuffer, int offset,
                int limit, Schema<T> schema, RpcHeader header, RpcCodec outCodec, RpcWorker worker,
                WriteSession session) throws IOException
        {
            return worker.protocolDelegate.parseFrom(inCodec, 
                    in, inBuffer, offset, limit, schema, 
                    header, outCodec, worker, session);
        }
        
    }*/
    ;
    
    public abstract RpcError authenticate(InputStream in, byte[] buf, int len, 
            RpcWorker worker, WriteSession session) throws IOException;
    
    public abstract RpcError handle(InputStream in, byte[] buf, int len, 
            RpcWorker worker, WriteSession session, boolean fastfunc) throws IOException;
    
    public abstract void updateError(RpcError error, 
            RpcWorker worker, WriteSession session);
    
    public abstract <T extends WriteSession> void writeHeader(RpcError error, 
            RpcHeader header, RpcCodec outCodec, 
            RpcWorker worker, T session) throws IOException;
    
    public abstract void writeHeaderZeroContent(
            final RpcWorker worker, final WriteSession session) throws IOException;
    
    public abstract <T> T parseFrom(RpcCodec inCodec, 
            InputStream in, byte[] inBuffer, int offset, int limit, 
            Schema<T> schema, 
            RpcHeader header, RpcCodec outCodec, 
            RpcWorker worker, WriteSession session) throws IOException;
    
    public interface Delegate
    {
        public RpcError authenticate(InputStream in, byte[] buf, int len, 
                RpcWorker worker, WriteSession session) throws IOException;
        
        public RpcError handle(InputStream in, byte[] buf, int len, 
                RpcWorker worker, WriteSession session, boolean fastfunc) throws IOException;
        
        public void updateError(RpcError error, 
                RpcWorker worker, WriteSession session);
        
        public <T extends WriteSession> void writeHeader(RpcError error, 
                RpcHeader header, RpcCodec outCodec, 
                RpcWorker worker, T session) throws IOException;
        
        public void writeHeaderZeroContent(
                final RpcWorker worker, final WriteSession session) throws IOException;
        
        public <T> T parseFrom(RpcCodec inCodec, 
                InputStream in, byte[] inBuffer, int offset, int limit, 
                Schema<T> schema, 
                RpcHeader header, RpcCodec outCodec, 
                RpcWorker worker, WriteSession session) throws IOException;
    }
    // start at 7
    // 0xFF 0xFF 0xFF 0xFF [ 0 ,
    static final int BODY_OFFSET = 7;
    
    static final String SERVICE_UNAVAILABLE = "Service unavailable.", 
            SERVICE_NOT_FOUND = "Service not found.", 
            UNAUTHORIZED = "Unauthorized.", 
            INVALID_REQUEST = "Invalid request.",
            MISSING_REQUEST_BODY = "Missing request body";
}
