//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuff;

import java.io.IOException;

import com.dyuproject.protostuff.StringSerializer.STRING;
import com.dyuproject.protostuffdb.EntityMetadata;
import com.dyuproject.protostuffdb.ProtostuffPipe;
import com.dyuproject.protostuffdb.WriteContext;

/**
 * Rpc fast func.
 * 
 * @author David Yu
 * @created Aug 31, 2012
 */
public abstract class RpcFastFunc
{
    
    public static final String ERR_UNAUTHORIZED = "401";
    
    protected static final byte[] DL_HEADER_PREFIX = 
            STRING.ser("HTTP/1.1 200 OK\r\nContent-Disposition: attachment; filename=\""),
            DL_HEADER_DELIMITER = STRING.ser("\"\r\nContent-Type: application/zip\r\n\r\n");
    
    public final int id;
    
    public final boolean authRequired;

    public RpcFastFunc(int id, boolean authRequired)
    {
        if(id < 1 || id > 255)
            throw new IllegalArgumentException("fastfunc id must be be 1 to 255.");
        
        this.id = id;
        this.authRequired = authRequired;
    }

    /**
     * Override this if you need to do something on initialization.
     */
    protected void init(RpcServer server, WriteContext context)
    {
        
    }
    
    /**
     * Handle the request.
     * 
     * Example:
     * <pre>
     *   session.sink.writeStrAscii("hello", session, 
     *          session.sink.writeByteArray(outPrefix, session, session.head));
     * </pre>
     */
    public abstract String handle(RpcHeader header, 
            byte[] body, int offset, int len, 
            byte[] outPrefix, WriteSession session, 
            RpcServer server, WriteContext context) throws IOException;
    
    public static void writeYamlTo(WriteSession session, WriteContext context, 
            final byte[] key, final EntityMetadata<Object> em, 
            byte[] outPrefix, String errMsg) throws IOException
    {
        session.sink.writeByteArray(outPrefix, session, session.head);
        try
        {
            YamlIOUtil.writeTo(new WriteSessionOutputStream(session), 
                    errMsg, RpcService.STATUS_MSG_SCHEMA, context.lb);
        }
        finally
        {
            context.lb.clear();
        }
    }

    public static void writeYamlTo(WriteSession session, WriteContext context, 
            final byte[] key, final EntityMetadata<Object> em, 
            byte[] outPrefix, byte[] value) throws IOException
    {
        session.sink.writeByteArray(outPrefix, session, session.head);
        final ProtostuffPipe pipe = context.pipe.set(key, value, 0, value.length);
        try
        {
            YamlIOUtil.writeTo(new WriteSessionOutputStream(session), 
                    pipe, em.pipeSchema, context.lb);
        }
        finally
        {
            context.lb.clear();
            pipe.clear();
        }
    }
    
}
