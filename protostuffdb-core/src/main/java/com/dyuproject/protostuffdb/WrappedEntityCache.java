//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.RpcResponse;
import com.dyuproject.protostuff.Schema;

/**
 * The cache's item wraps an entity.
 * 
 * @author David Yu
 * @created Aug 25, 2014
 */
public abstract class WrappedEntityCache<T,W extends CachedEntity<W>> implements Cache,
    AbstractAppendOnlyList.Listener<T>, Visitor<WriteContext>
{
    
    public final EntityMetadata<W> em;
    public final AbstractAppendOnlyList<T> list;
    
    public WrappedEntityCache(EntityMetadata<W> em, boolean serial, int segmentSize)
    {
        this.em = em;
        this.list = serial ? new SerialAppendOnlyList<T>(segmentSize, this) : 
            new ConcurrentAppendOnlyList<T>(segmentSize, this);
    }
    
    protected abstract T wrap(W entity);
    
    protected abstract W unwrap(T wrapper);
    
    public abstract int pipeTo(RpcResponse res) throws IOException;
    
    public void assignIndexTo(T item, int index)
    {
        unwrap(item).setId(index + 1);
    }
    
    public int newId()
    {
        return 1 + list.size();
    }
    
    public T get(int id)
    {
        return list.get(id - 1);
    }
    
    public int add(W entity, byte[] key, long now, WriteContext context)
    {
        final int idx = list.add(wrap(entity));
        context.fillIndexedEntityKey(key, em.kind, now, idx);
        return idx;
    }
    
    public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
            WriteContext context, int index)
    {
        final byte[] value = ValueUtil.copy(v, voffset, vlen);
        final W entity = context.parseFrom(value, em);
        entity.setKV(key, value);
        if (!em.seq)
            entity.setTs(KeyUtil.extractTsFrom(key));
        
        list.fill(wrap(entity));
        
        /*final int id = list.fill(entity);
        if (id != index + 1)
        {
            throw new RuntimeException("The id: " + id + 
                    " is not derived from the index: " + index + " on " + 
                    em.pipeSchema.wrappedSchema.messageFullName());
        }*/
        
        return false;
    }
    
    public int writeTo(final Output output, 
            final Schema<T> schema, int fieldNumber, int lastSeenId) throws IOException
    {
        return AbstractAppendOnlyList.writeTo(output, schema, fieldNumber, list, lastSeenId);
    }
    
    public int size()
    {
        return list.size();
    }
    
    public void clear()
    {
        list.clear();
    }
}
