//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.DateTimeUtil.MILLIS_PER_DAY;
import static com.dyuproject.protostuffdb.DateTimeUtil.startOfTodayMS;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Pattern;

import com.dyuproject.protostuff.LongHashSet;
import com.dyuproject.protostuff.ds.CAS;

/**
 * Validation for scalar fields.
 * 
 * @author David Yu
 * @created Oct 22, 2012
 */
public final class Validation
{
    
    
    
    static final Pattern PATTERN_EMAIL = 
            Pattern.compile("^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+[.][a-zA-Z]+$");
    
    
    // start misc
    
    public static boolean isAsciiOnly(String value)
    {
        for (int i = 0, len = value.length(); i < len; i++)
        {
            if (value.charAt(i) > 127)
                return false;
        }
        
        return true;
    }
    
    public static boolean isAsciiPrintable(String value)
    {
        char c;
        for (int i = 0, len = value.length(); i < len; i++)
        {
            c = value.charAt(i);
            if (c < 32 || c > 127)
                return false;
        }
        
        return true;
    }
    
    public static boolean isAsciiSafeHtml(String value)
    {
        char c;
        for (int i = 0, len = value.length(); i < len; i++)
        {
            c = value.charAt(i);
            if (c < 32 || c > 127 || c == '&' || c == '<' || c == '>' || c == '\\')
                return false;
        }
        
        return true;
    }
    
    /**
     * Returns true if null or empty.
     */
    public static boolean isEmpty(CAS cas)
    {
        return cas == null || cas.isEmpty();
    }
    
    /**
     * Returns true if null or empty.
     */
    public static boolean isEmpty(List<?> list)
    {
        return list == null || list.isEmpty();
    }
    
    /**
     * Returns true if null or empty.
     */
    public static boolean isEmpty(String value)
    {
        return value == null || 0 == value.length();
    }
    
    /**
     * Returns true if null or empty.
     */
    public static boolean isEmpty(byte[] value)
    {
        return value == null || 0 == value.length;
    }
    
    /**
     * Returns true if its an id.
     */
    public static boolean isValidId(int id)
    {
        return id > 0;
    }
    
    /**
     * Returns true if its a key.
     */
    public static boolean isValidKey(byte[] key)
    {
        return key != null && KeyUtil.isKey(key);
    }
    
    // end misc
    
    /*static
    {
        String atom = "[a-z0-9!#$%&'*+/=?^_`{|}~-]";
        String domain = "(" + atom + "+(\\." + atom + "+)*";
        String ip_domain = "\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\]";
        
        PATTERN_EMAIL = Pattern.compile(
                "^" + atom + "+(\\." + atom + "+)*@"
                                + domain
                                + "|"
                                + ip_domain
                                + ")$",
                Pattern.CASE_INSENSITIVE);
    }*/
    
    // TODO optimize
    public static boolean isValidUnique(
            final List<byte[]> list)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return true;
            case 2:
                return !Arrays.equals(list.get(0), list.get(1));
        }
        
        final TreeMap<byte[],Boolean> map = new TreeMap<byte[],Boolean>(Comparators.DEFAULT);
        for (int i = 0; i < len; i++)
        {
            if (null != map.put(list.get(i), Boolean.TRUE))
                return false;
        }
        /*final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            byte[] key = list.get(i);
            long val = ValueUtil.toInt64LE(key, 1);
            if (!set.add(val))
                return addMessage(message);
        }*/
        
        return true;
    }
    
    public static boolean isValidUnique(
            final List<byte[]> list, final int kind)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return KeyUtil.isKey(list.get(0), kind);
            case 2:
                return KeyUtil.isKey(list.get(0), kind) && 
                        KeyUtil.isKey(list.get(1), kind) && 
                        !Arrays.equals(list.get(0), list.get(1));
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            byte[] key = list.get(i);
            if (!KeyUtil.isKey(key, kind) || !set.add(ValueUtil.toInt64LE(key, 1)))
                return false;
        }
        
        return true;
    }
    
    public static boolean isValidUnique(
            final List<String> list, String dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return true;
            case 2:
                return !list.get(0).equals(list.get(1));
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final HashMap<String,Boolean> map = new HashMap<String, Boolean>(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            if (null != map.put(list.get(i), Boolean.TRUE))
                return false;
        }
        
        return true;
    }
    
    public static boolean isValidUnique(
            final List<Integer> list, Integer dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return true;
            case 2:
                return list.get(0).intValue() != list.get(1).intValue();
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            if (!set.add(list.get(i).longValue()))
                return false;
        }
        
        return true;
    }
    
    public static boolean isValidUnique(
            final List<Integer> list, final int min, Integer dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return list.get(0).intValue() >= min;
            case 2:
            {
                final int val0 = list.get(0).intValue(), 
                        val1 = list.get(1).intValue();
                
                return val0 >= min && val1 >= min && val0 != val1;
            }
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            int val = list.get(i).intValue();
            if (val < min || !set.add((long)val))
                return false;
        }
        
        return true;
    }
    
    public static boolean isValidUnique(
            final List<Long> list, Long dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return true;
            case 2:
                return list.get(0).longValue() != list.get(1).longValue();
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            if (!set.add(list.get(i)))
                return false;
        }
        
        return true;
    }
    
    public static boolean isValidUnique(
            final List<Long> list, final long min, Long dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return list.get(0).longValue() >= min;
            case 2:
            {
                final long val0 = list.get(0), 
                        val1 = list.get(1);
                
                return val0 >= min && val1 >= min && val0 != val1;
            }
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            long val = list.get(i);
            if (val < min || !set.add(val))
                return false;
        }
        
        return true;
    }
    
    public static <T extends HasId> boolean isValidUnique(
            final List<T> list, final int min, HasId dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return list.get(0).getId() >= min;
            case 2:
            {
                final int val0 = list.get(0).getId(), 
                        val1 = list.get(1).getId();
                
                return val0 >= min && val1 >= min && val0 != val1;
            }
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        for (int i = 0; i < len; i++)
        {
            int val = list.get(i).getId();
            if (val < min || !set.add((long)val))
                return false;
        }
        
        return true;
    }
    
    /**
     * If kind is zero, it is assumed that the key was validated with a kind before this 
     * call.
     */
    public static <T extends HasKey> boolean isValidUnique(
            final List<T> list, final int kind, HasKey dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return kind == 0 || KeyUtil.isKey(list.get(0).getKey(), kind);
            case 2:
            {
                if (kind == 0)
                    return !Arrays.equals(list.get(0).getKey(), list.get(1).getKey());
                
                final byte[] key0 = list.get(0).getKey(), 
                        key1 = list.get(1).getKey();
                
                return KeyUtil.isKey(key0, kind) && KeyUtil.isKey(key1, kind) && 
                        !Arrays.equals(key0, key1);
            }
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        
        if (kind == 0)
        {
            for (int i = 0; i < len; i++)
            {
                if (!set.add(ValueUtil.toInt64LE(list.get(i).getKey(), 1)))
                    return false;
            }
        }
        else
        {
            for (int i = 0; i < len; i++)
            {
                byte[] key = list.get(i).getKey();
                if (!KeyUtil.isKey(key, kind) || !set.add(ValueUtil.toInt64LE(key, 1)))
                    return false;
            }
        }
        
        return true;
    }
    
    /**
     * If kind is zero, it is assumed that the key was validated with a kind before this 
     * call.
     */
    public static <T extends HasParentKey> boolean isValidUnique(
            final List<T> list, final int kind, HasParentKey dummy)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        switch (len)
        {
            case 1:
                return kind == 0 || KeyUtil.isKey(list.get(0).getParentKey(), kind);
            case 2:
            {
                if (kind == 0)
                    return !Arrays.equals(list.get(0).getParentKey(), list.get(1).getParentKey());
                
                final byte[] key0 = list.get(0).getParentKey(), 
                        key1 = list.get(1).getParentKey();
                
                return KeyUtil.isKey(key0, kind) && KeyUtil.isKey(key1, kind) && 
                        !Arrays.equals(key0, key1);
            }
        }
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        
        if (kind == 0)
        {
            for (int i = 0; i < len; i++)
            {
                if (!set.add(ValueUtil.toInt64LE(list.get(i).getParentKey(), 1)))
                    return false;
            }
        }
        else
        {
            for (int i = 0; i < len; i++)
            {
                byte[] key = list.get(i).getParentKey();
                if (!KeyUtil.isKey(key, kind) || !set.add(ValueUtil.toInt64LE(key, 1)))
                    return false;
            }
        }
        
        return true;
    }
    
    /**
     * If kind is zero, it is assumed that the key was validated with a kind before this 
     * call.
     */
    public static <T extends HasId, K extends HasKey> boolean isValidUnique(
            final List<T> idlist, final int min, 
            final List<K> keylist, final int kind)
    {
        if (idlist == null || idlist.isEmpty() || keylist == null || keylist.isEmpty())
            return false;
        
        final int idlen = idlist.size(), keylen = keylist.size();
        LongHashSet set = null;
        switch (idlen)
        {
            case 1:
                if (idlist.get(0).getId() >= min)
                    break;
                
                return false;
            case 2:
            {
                final int val0 = idlist.get(0).getId(), 
                        val1 = idlist.get(1).getId();
                
                if (val0 >= min && val1 >= min && val0 != val1)
                    break;
                
                return false;
            }
            default:
                set = new LongHashSet(2 * Math.max(idlen, keylen));
                for (int i = 0; i < idlen; i++)
                {
                    int val = idlist.get(i).getId();
                    if (val < min || !set.add((long)val))
                        return false;
                }
        }
        
        switch (keylen)
        {
            case 1:
                return kind == 0 || KeyUtil.isKey(keylist.get(0).getKey(), kind);
            case 2:
            {
                if (kind == 0)
                    return !Arrays.equals(keylist.get(0).getKey(), keylist.get(1).getKey());
                
                final byte[] key0 = keylist.get(0).getKey(), 
                        key1 = keylist.get(1).getKey();
                
                return KeyUtil.isKey(key0, kind) && KeyUtil.isKey(key1, kind) && 
                        !Arrays.equals(key0, key1);
            }
        }
        
        if (set == null)
            set = new LongHashSet(2 * keylen);
        else
            set.clear();
        
        if (kind == 0)
        {
            for (int i = 0; i < keylen; i++)
            {
                if (!set.add(ValueUtil.toInt64LE(keylist.get(i).getKey(), 1)))
                    return false;
            }
        }
        else
        {
            for (int i = 0; i < keylen; i++)
            {
                byte[] key = keylist.get(i).getKey();
                if (!KeyUtil.isKey(key, kind) || !set.add(ValueUtil.toInt64LE(key, 1)))
                    return false;
            }
        }
        
        return true;
    }
    
    public static <T extends HasKeys> boolean isValidUniqueKeys(
            final List<T> list, final int ... indices)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final HasKeys first = list.get(0);
        LongHashSet set = null;
        
        for (int index : indices)
        {
            List<byte[]> keys = first.keysAt(index);
            
            if (keys == null)
            {
                if (len == 1)
                    continue;
                
                if (len == 2)
                {
                    if (Arrays.equals(first.keyAt(index), list.get(1).keyAt(index)))
                        return false;
                    
                    continue;
                }
                
                if (set == null)
                    set = new LongHashSet(len + extraCapacity);
                else
                    set.clear();
                
                if (!set.add(ValueUtil.toInt64LE(first.keyAt(index), 1)))
                    return false;
                
                for (int i = 1; i < len; i++)
                {
                    if (!set.add(ValueUtil.toInt64LE(list.get(i).keyAt(index), 1)))
                        return false;
                }
                
                continue;
            }
            
            if (set == null)
            {
                // used as multiplier (configurable from annotations)
                set = new LongHashSet(len + extraCapacity);
                
                /*int totalLen = keys.size();
                for (int i = 1; i < len; i++)
                    totalLen = totalLen + extraCapacity + list.get(i).keysAt(index).size();
                
                set = new LongHashSet(totalLen);*/
            }
            else
            {
                set.clear();
            }

            for (byte[] key : keys)
            {
                if (!set.add(ValueUtil.toInt64LE(key, 1)))
                    return false;
            }
            
            for (int i = 1; i < len; i++)
            {
                keys = list.get(i).keysAt(index);
                for (byte[] key : keys)
                {
                    if (!set.add(ValueUtil.toInt64LE(key, 1)))
                        return false;
                }
            }
        }
        
        return true;
    }
    
    public static <T extends HasKeys> boolean isValidUniqueKeys(
            final List<T> list, final int index)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final HasKeys first = list.get(0);
        List<byte[]> keys = first.keysAt(index);
        
        if (keys == null)
        {
            if (len == 1)
                return true;
            
            if (len == 2)
            {
                if (Arrays.equals(first.keyAt(index), list.get(1).keyAt(index)))
                    return false;
                
                return true;
            }
            
            final LongHashSet set = new LongHashSet(len + extraCapacity);
            if (!set.add(ValueUtil.toInt64LE(first.keyAt(index), 1)))
                return false;
            
            for (int i = 1; i < len; i++)
            {
                if (!set.add(ValueUtil.toInt64LE(list.get(i).keyAt(index), 1)))
                    return false;
            }
            
            return true;
        }
        
        // used as multiplier (configurable from annotations)
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        
        /*int totalLen = keys.size();
        for (int i = 1; i < len; i++)
            totalLen = totalLen + extraCapacity + list.get(i).keysAt(index).size();
        
        final LongHashSet set = new LongHashSet(totalLen);*/
        for (byte[] key : keys)
        {
            if (!set.add(ValueUtil.toInt64LE(key, 1)))
                return false;
        }
        
        for (int i = 1; i < len; i++)
        {
            keys = list.get(i).keysAt(index);
            for (byte[] key : keys)
            {
                if (!set.add(ValueUtil.toInt64LE(key, 1)))
                    return false;
            }
        }
        
        return true;
    }
    
    public static <T extends HasIds> boolean isValidUniqueIds(
            final List<T> list, final int ... indices)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final HasIds first = list.get(0);
        LongHashSet set = null;
        
        for (int index : indices)
        {
            List<Integer> ids = first.idsAt(index);
            
            if (ids == null)
            {
                if (len == 1)
                    continue;
                
                if (len == 2)
                {
                    if (first.idAt(index) == list.get(1).idAt(index))
                        return false;
                    
                    continue;
                }
                
                if (set == null)
                    set = new LongHashSet(len + extraCapacity);
                else
                    set.clear();
                
                if (!set.add((long)first.idAt(index)))
                    return false;
                
                for (int i = 1; i < len; i++)
                {
                    if (!set.add((long)list.get(i).idAt(index)))
                        return false;
                }
                
                continue;
            }
            
            if (set == null)
            {
                // used as multiplier (configurable from annotations)
                set = new LongHashSet(len + extraCapacity);
                
                /*int totalLen = ids.size();
                for (int i = 1; i < len; i++)
                    totalLen = totalLen + extraCapacity + list.get(i).idsAt(index).size();
                
                set = new LongHashSet(totalLen);*/
            }
            else
            {
                set.clear();
            }

            for (Integer id : ids)
            {
                if (!set.add(id.longValue()))
                    return false;
            }
            
            for (int i = 1; i < len; i++)
            {
                ids = list.get(i).idsAt(index);
                for (Integer id : ids)
                {
                    if (!set.add(id.longValue()))
                        return false;
                }
            }
        }
        
        return true;
    }
    
    public static <T extends HasIds> boolean isValidUniqueIds(
            final List<T> list, final int index)
    {
        if (list == null || list.isEmpty())
            return true;
        
        final int len = list.size();
        
        // TODO should really be twice the len?
        final int extraCapacity = len;
        
        final HasIds first = list.get(0);
        List<Integer> ids = first.idsAt(index);
        
        if (ids == null)
        {
            if (len == 1)
                return true;
            
            if (len == 2)
            {
                if (first.idAt(index) == list.get(1).idAt(index))
                    return false;
                
                return true;
            }
            
            final LongHashSet set = new LongHashSet(len + extraCapacity);
            if (!set.add((long)first.idAt(index)))
                return false;
            
            for (int i = 1; i < len; i++)
            {
                if (!set.add((long)list.get(i).idAt(index)))
                    return false;
            }
            
            return true;
        }
        
        // used as multiplier (configurable from annotations)
        final LongHashSet set = new LongHashSet(len + extraCapacity);
        
        /*int totalLen = ids.size();
        for (int i = 1; i < len; i++)
            totalLen = totalLen + extraCapacity + list.get(i).idsAt(index).size();
        
        final LongHashSet set = new LongHashSet(totalLen);*/
        for (Integer id : ids)
        {
            if (!set.add(id.longValue()))
                return false;
        }
        
        for (int i = 1; i < len; i++)
        {
            ids = list.get(i).idsAt(index);
            for (Integer id : ids)
            {
                if (!set.add(id.longValue()))
                    return false;
            }
        }
        
        return true;
    }
    
    public static boolean isValidTime(int value)
    {
        return isValidRange(value, 0, DateTimeUtil.MAX_TIME);
    }
    
    public static boolean isValidDate(long value)
    {
        return 0 == value % MILLIS_PER_DAY;
    }
    
    public static boolean isValidDecimalMax(BigDecimal value, BigDecimal maxValue)
    {
        return value.compareTo(maxValue) != 1;
    }
    
    public static boolean isValidDecimalMax(String value, BigDecimal maxValue)
    {
        try
        {
            return isValidDecimalMax(new BigDecimal(value), maxValue);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidDecimalMax(float value, float maxValue)
    {
        return value <= maxValue;
    }
    
    public static boolean isValidDecimalMax(double value, double maxValue)
    {
        return value <= maxValue;
    }
    
    public static boolean isValidDecimalMin(BigDecimal value, BigDecimal minValue)
    {
        return value.compareTo(minValue) != -1;
    }
    
    public static boolean isValidDecimalMin(String value, BigDecimal minValue)
    {
        try
        {
            return isValidDecimalMin(new BigDecimal(value), minValue);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidDecimalMin(float value, float minValue)
    {
        return value >= minValue;
    }
    
    public static boolean isValidDecimalMin(double value, double minValue)
    {
        return value >= minValue;
    }
    
    public static boolean isValidDigits(BigDecimal value, 
            int maxIntegerLength, int maxFractionLength)
    {
        int integerPartLength = value.precision() - value.scale();
        int fractionPartLength = value.scale() < 0 ? 0 : value.scale();

        return ( maxIntegerLength >= integerPartLength 
                && maxFractionLength >= fractionPartLength );
    }
    
    public static boolean isValidDigits(String value, 
            int maxIntegerLength, int maxFractionLength)
    {
        try
        {
            return isValidDigits(new BigDecimal(value), maxIntegerLength, 
                    maxFractionLength);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidDigits(int value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(new BigDecimal(value), 
                maxIntegerLength, maxFractionLength);
    }
    
    public static boolean isValidDigits(long value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(new BigDecimal(value), 
                maxIntegerLength, maxFractionLength);
    }
    
    public static boolean isValidDigits(float value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(new BigDecimal(value), 
                maxIntegerLength, maxFractionLength);
    }
    
    public static boolean isValidDigits(double value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(new BigDecimal(value), 
                maxIntegerLength, maxFractionLength);
    }
    
    public static boolean isValidDigits(Number value, 
            int maxIntegerLength, int maxFractionLength)
    {
        return isValidDigits(new BigDecimal(value.toString()).stripTrailingZeros(), 
                maxIntegerLength, maxFractionLength);
    }
    
    public static boolean isValidEmail(CharSequence value)
    {
        return PATTERN_EMAIL.matcher(value).matches();
    }
    
    public static boolean isValidFutureTS(long value)
    {
        return value > System.currentTimeMillis();
    }
    
    public static boolean isValidFutureTS(long value, int unit, int min, int max) // already utc
    {
        final long today = System.currentTimeMillis(), multiplier;
        switch(unit)
        {
            case 1: // day
                multiplier = MILLIS_PER_DAY;
                break;
            case 2: // hour
                multiplier = 1000 * 60 * 60;
                break;
            case 3: // minute
                multiplier = 1000 * 60;
                break;
            case 4: // second
                multiplier = 1000;
                break;
            default: // millisecond
                multiplier = 1;
        }
        
        // no limit
        if(max == 0)
            return min == 0 ? value > today : value >= (today + (multiplier * min));
        
        return value >= (today + (multiplier * min)) && 
                value <= (today + (multiplier * max));
    }
    
    /**
     * Day is the unit of min and max.
     */
    public static boolean isValidFutureOrToday(long value, int min, int max) // already utc
    {
        if(0 != value % MILLIS_PER_DAY)
            return false;
        
        final long today = startOfTodayMS();
        if(value == today)
            return true;
        
        // no limit
        if(max == 0)
            return min == 0 ? value > today : value >= (today + (MILLIS_PER_DAY * min));
        
        return value >= (today + (MILLIS_PER_DAY * min)) && 
                value <= (today + (MILLIS_PER_DAY * max));
    }
    
    /**
     * Day is the unit of min and max.
     */
    public static boolean isValidFuture(long value, int min, int max) // already utc
    {
        if(0 != value % MILLIS_PER_DAY)
            return false;
        
        final long today = startOfTodayMS();
        
        // no limit
        if(max == 0)
            return min == 0 ? value > today : value >= (today + (MILLIS_PER_DAY * min));
        
        return value >= (today + (MILLIS_PER_DAY * min)) && 
                value <= (today + (MILLIS_PER_DAY * max));
    }
    
    public static boolean isValidFuture(Date value)
    {
        return value.after(new Date());
    }
    
    /*public static boolean isValidFuture(Calendar value)
    {
        return value.after(Calendar.getInstance());
    }*/
    
    public static boolean isValidLength(CharSequence value, int min, int max)
    {
        int length = value.length();
        return length >= min && length <= max;
    }
    
    public static boolean isValidLuhn(String value, int multiplicator)
    {
        char[] chars = value.toCharArray();
        // TODO use HPPC
        ArrayList<Integer> digits = new ArrayList<Integer>();
        for ( char c : chars ) {
                if ( Character.isDigit( c ) ) {
                        digits.add( c - '0' );
                }
        }
        int length = digits.size();
        int sum = 0;
        boolean even = false;
        for ( int index = length - 1; index >= 0; index-- ) {
                int digit = digits.get( index );
                if ( even ) {
                        digit *= multiplicator;
                }
                if ( digit > 9 ) {
                        digit = digit / 10 + digit % 10;
                }
                sum += digit;
                even = !even;
        }
        return sum % 10 == 0;
    }
    
    public static boolean isValidMax(int value, int max)
    {
        return value <= max;
    }
    
    public static boolean isValidMax(long value, long max)
    {
        return value <= max;
    }
    
    public static boolean isValidMax(float value, float max)
    {
        return value <= max;
    }
    
    public static boolean isValidMax(double value, double max)
    {
        return value <= max;
    }
    
    public static boolean isValidMax(String value, long max)
    {
        try
        {
            return isValidMax(new BigDecimal(value), BigDecimal.valueOf(max));
        }
        catch(NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidMax(BigDecimal value, BigDecimal max)
    {
        return value.compareTo(max) <= 0;
    }
    
    public static boolean isValidMin(int value, int min)
    {
        return value >= min;
    }
    
    public static boolean isValidMin(long value, long min)
    {
        return value >= min;
    }
    
    public static boolean isValidMin(float value, float min)
    {
        return value >= min;
    }
    
    public static boolean isValidMin(double value, double min)
    {
        return value >= min;
    }
    
    public static boolean isValidMin(String value, long min)
    {
        try
        {
            return isValidMin(new BigDecimal(value), BigDecimal.valueOf(min));
        }
        catch(NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidMin(BigDecimal value, BigDecimal min)
    {
        return value.compareTo(min) >= 0;
    }
    
    public static boolean isValidGT(int value, int param)
    {
        return value > param;
    }
    
    public static boolean isValidGT(long value, long param)
    {
        return value > param;
    }
    
    public static boolean isValidGT(float value, float param)
    {
        return value > param;
    }
    
    public static boolean isValidGT(double value, double param)
    {
        return value > param;
    }
    
    public static boolean isValidGT(String value, long param)
    {
        try
        {
            return isValidGT(new BigDecimal(value), BigDecimal.valueOf(param));
        }
        catch(NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidGT(BigDecimal value, BigDecimal param)
    {
        return value.compareTo(param) > 0;
    }
    
    // TODO isValidMax/isValidMin for BigInteger, BigDecimal and String
    
    public static boolean isValidNotBlank(String value)
    {
        return value.trim().length() != 0;
    }
    
    // add-on
    public static boolean isValidNotBlank(CharSequence value)
    {
        return value.length() != 0;
    }
    
    public static boolean isValidNotEmpty(String value)
    {
        return value.trim().length() != 0;
    }
    
    // add-on
    public static boolean isValidNotEmpty(CharSequence value)
    {
        return value.length() != 0;
    }
    
    public static boolean isValidNotEmpty(CAS value)
    {
        return !value.isEmpty();
    }
    
    public static boolean isValidNotNull(Object value)
    {
        return value != null;
    }
    
    public static boolean isValidNull(Object value)
    {
        return value == null;
    }
    
    public static boolean isValidNotNullNotEmpty(List<?> value)
    {
        return value != null && !value.isEmpty();
    }
    
    public static boolean isValidPastTS(long value)
    {
        return value < System.currentTimeMillis();
    }
    
    public static boolean isValidPastTS(long value, int unit, int min, int max) // already utc
    {
        final long today = System.currentTimeMillis(), multiplier;
        switch(unit)
        {
            case 1: // day
                multiplier = MILLIS_PER_DAY;
                break;
            case 2: // hour
                multiplier = 1000 * 60 * 60;
                break;
            case 3: // minute
                multiplier = 1000 * 60;
                break;
            case 4: // second
                multiplier = 1000;
                break;
            default: // millisecond
                multiplier = 1;
        }
        
        // no limit
        if(max == 0)
            return min == 0 ? value < today : value <= (today - (multiplier * min));
        
        return value <= (today - (multiplier * min)) && 
                value >= (today - (multiplier * max));
    }
    
    /**
     * Day is the unit of min and max.
     */
    public static boolean isValidPastOrToday(long value, int min, int max) // already utc
    {
        if(0 != value % MILLIS_PER_DAY)
            return false;
        
        final long today = startOfTodayMS();
        if(value == today)
            return true;
        
        // no limit
        if(max == 0)
            return min == 0 ? value < today : value <= (today - (MILLIS_PER_DAY * min));
        
        return value <= (today - (MILLIS_PER_DAY * min)) && 
                value >= (today - (MILLIS_PER_DAY * max));
    }
    
    /**
     * Day is the unit of min and max.
     */
    public static boolean isValidPast(long value, int min, int max) // already utc
    {
        if(0 != value % MILLIS_PER_DAY)
            return false;
        
        final long today = startOfTodayMS();
        
        // no limit
        if(max == 0)
            return min == 0 ? value < today : value <= (today - (MILLIS_PER_DAY * min));
        
        return value <= (today - (MILLIS_PER_DAY * min)) && 
                value >= (today - (MILLIS_PER_DAY * max));
    }
    
    public static boolean isValidPast(Date value)
    {
        return value.before(new Date());
    }
    
    /*public static boolean isValidPast(Calendar value)
    {
        return value.before(Calendar.getInstance());
    }*/
    
    public static boolean isValidPattern(CharSequence value, Pattern pattern)
    {
        return pattern.matcher(value).matches();
    }
    
    public static boolean isValidSafeHtml(CharSequence value) // needs a whitelist arg
    {
        // TODO
        return false;
    }
    
    public static boolean isValidScriptAssert(Object value)
    {
        // TODO
        return false;
    }
    
    public static boolean isValidSize(Collection<?> collection, int min, int max)
    {
        int length = collection.size();
        return length >= min && length <= max;
    }
    
    public static boolean isValidSize(CharSequence value, int min, int max)
    {
        int length = value.length();
        return length >= min && length <= max;
    }
    
    public static boolean isValidSize(String value, int min, int max)
    {
        int length = value.trim().length();
        return length >= min && length <= max;
    }
    
    // port should be passed as -1 if not configured
    public static boolean isValidURL(String value, String protocol, String host, int port)
    {
        java.net.URL url;
        try
        {
            url = new java.net.URL(value);
        }
        catch (java.net.MalformedURLException e)
        {
            return false;
        }

        if (protocol != null && protocol.length() > 0 && !url.getProtocol().equals(protocol))
        {
            return false;
        }

        if (host != null && host.length() > 0 && !url.getHost().equals(host))
        {
            return false;
        }

        if (port != -1 && url.getPort() != port)
        {
            return false;
        }

        return true;
    }
    
    public static boolean isValidRange(int value, int min, int max)
    {
        return isValidMin(value, min) && isValidMax(value, max);
    }
    
    public static boolean isValidRange(long value, long min, long max)
    {
        return isValidMin(value, min) && isValidMax(value, max);
    }
    
    public static boolean isValidRange(float value, float min, float max)
    {
        return isValidMin(value, min) && isValidMax(value, max);
    }
    
    public static boolean isValidRange(double value, double min, double max)
    {
        return isValidMin(value, min) && isValidMax(value, max);
    }
    
    public static boolean isValidRange(String value, long min, long max)
    {
        try
        {
            return isValidRange(new BigDecimal(value), BigDecimal.valueOf(min), 
                    BigDecimal.valueOf(max));
        }
        catch(NumberFormatException e)
        {
            return false;
        }
    }
    
    public static boolean isValidRange(BigDecimal value, BigDecimal min, BigDecimal max)
    {
        return value.compareTo(min) != -1 && value.compareTo(max) != 1;
    }
    
    public static boolean isValidCreditCardNumber(String value)
    {
        return isValidLuhn(value, 2);
    }

}
