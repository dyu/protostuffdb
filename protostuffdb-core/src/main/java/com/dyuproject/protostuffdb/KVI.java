//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Message;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.UninitializedMessageException;

/**
 * <pre>
 * message KVI {
 *   required bytes key = 1;
 *   required bytes value = 2;
 *   optional fixed32 id = 3;
 * }
 * </pre>
 */
public final class KVI implements Message<KVI>, HasIdAndKV
{
    
    public static final int FN_KEY = 1;
    public static final int FN_VALUE = 2;
    public static final int FN_ID = 3;

    /** Required. */
    public byte[] key;
    /** Required. */
    public volatile byte[] value;
    /** Optional. */
    public int id;

    public KVI()
    {
        
    }

    public KVI(
        byte[] key,
        byte[] value
    )
    {
        this.key = key;
        this.value = value;
    }
    
    // HasKV
    
    public byte[] getKey()
    {
        return key;
    }

    public void setKey(byte[] key)
    {
        this.key = key;
    }

    public byte[] getValue()
    {
        return value;
    }

    public void setValue(byte[] value)
    {
        this.value = value;
    }

    public void setKV(byte[] key, byte[] value)
    {
        this.key = key;
        this.value = value;
    }
    
    // HasId
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }

    // message method

    public Schema<KVI> cachedSchema()
    {
        return SCHEMA;
    }

    
    static final Schema<KVI> SCHEMA = new Schema<KVI>()
    {
        // schema methods

        public KVI newMessage()
        {
            return new KVI();
        }

        public Class<KVI> typeClass()
        {
            return KVI.class;
        }

        public String messageName()
        {
            return KVI.class.getSimpleName();
        }

        public String messageFullName()
        {
            return KVI.class.getName();
        }

        public boolean isInitialized(KVI message)
        {
            return message.key != null && message.value != null;
        }

        public void mergeFrom(Input input, KVI message) throws IOException
        {
            for(int number = input.readFieldNumber(this);; number = input.readFieldNumber(this))
            {
                switch(number)
                {
                    case 0:
                        return;
                    case 1:
                        message.key = input.readByteArray();
                        break;
                    case 2:
                        message.value = input.readByteArray();
                        break;
                    case 3:
                        message.id = input.readFixed32();
                        break;
                    default:
                        input.handleUnknownField(number, this);
                }   
            }
        }


        public void writeTo(Output output, KVI message) throws IOException
        {
            if(message.key == null)
                throw new UninitializedMessageException("key", message);
            output.writeByteArray(1, message.key, false);

            if(message.value == null)
                throw new UninitializedMessageException("value", message);
            output.writeByteArray(2, message.value, false);
            
            output.writeFixed32(3, message.id, false);
        }

        public String getFieldName(int number)
        {
            switch(number)
            {
                case 1: return "k";
                case 2: return "v";
                case 3: return "i";
                default: return null;
            }
        }

        public int getFieldNumber(String name)
        {
            if(name.length() != 1)
                return 0;
            
            switch(name.charAt(0))
            {
                case 'k': return 1;
                case 'v': return 2;
                case 'i': return 3;
                default: return 0;
            }
        }
    };
    
    static final Pipe.Schema<KVI> PIPE_SCHEMA = new Pipe.Schema<KVI>(SCHEMA)
    {
        protected void transfer(Pipe pipe, Input input, Output output) throws IOException
        {
            for(int number = input.readFieldNumber(wrappedSchema);; number = input.readFieldNumber(wrappedSchema))
            {
                switch(number)
                {
                    case 0:
                        return;
                    case 1:
                        input.transferByteRangeTo(output, false, number, false);
                        break;
                    case 2:
                        input.transferByteRangeTo(output, false, number, false);
                        break;
                    case 3:
                        output.writeFixed32(number, input.readFixed32(), false);
                        break;
                    default:
                        input.handleUnknownField(number, wrappedSchema);
                }
            }
        }
    };


    public static Schema<KVI> getSchema()
    {
        return SCHEMA;
    }

    public static Pipe.Schema<KVI> getPipeSchema()
    {
        return PIPE_SCHEMA;
    }

}
