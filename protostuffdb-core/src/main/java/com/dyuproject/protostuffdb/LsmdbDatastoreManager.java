//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import protostuffdb.JniOps;

import com.dyuproject.protostuff.JniStream;

/**
 * Manages the lsmdb data stores.
 * 
 * @author David Yu
 * @created Jan 4, 2014
 */
public final class LsmdbDatastoreManager implements DatastoreManager
{
    
    static final int WRITE_FLAGS = Boolean.getBoolean("protostuff.ds.lsmdb.sync") ? 
            JniOps.FOP_CLOSE_SYNC : 0;
    
    private static final OutputStream DUMMY = new OutputStream()
    {
        @Override
        public void write(int arg0) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException
        {
            throw new UnsupportedOperationException();
        }
    };
    
    public final File dataDir;
    final HashMap<String, LsmdbDatastore> created = new HashMap<String, LsmdbDatastore>();
    public final JniStream stream;
    
    //private boolean deleteDataDirOnDestroy;
    
    public LsmdbDatastoreManager(byte[] buf, File dataDir)
    {
        this(buf, dataDir, false);
    }
    
    public LsmdbDatastoreManager(byte[] buf, File dataDir, boolean addShutdownHook)
    {
        stream = new JniStream(buf, DUMMY);
        
        if (!dataDir.exists())
        {
            //deleteDataDirOnDestroy = true;
            dataDir.mkdir();
        }
        else if(!dataDir.isDirectory())
            throw new IllegalArgumentException(dataDir + " is not a dir.");
        
        this.dataDir = dataDir;
        
        if(!addShutdownHook)
            return;
        
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
        {
            public void run()
            {
                close();
            }
        }));
    }
    
    private long newDb(int flags, File dir)
    {
        try
        {
            stream.start(JniOps.NEW_DB)
                .write8(flags) // flags
                .writeString(dir.getAbsolutePath()) // path (required)
                .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should never happen.");
        }
        
        LsmdbJniUtil.push(stream.end());
        
        return stream.readPtr(JniOps.PTR_DB);
    }
    
    private void close(LsmdbDatastore store)
    {
        // ignore errors?
        stream.start(JniOps.DELETE_DB)
            .writePtr(store.ptrDb, JniOps.PTR_DB)
            .endHeader()
            .end()
            .push();
    }
    
    public synchronized boolean backup(String storeName, String backupName)
    {
        return backup(created.get(storeName), backupName, stream);
    }
    
    public static boolean backup(LsmdbDatastore store, String backupName, 
            JniStream stream)
    {
        try
        {
            return store != null && stream.start(JniOps.BACKUP_DB)
                .writePtr(store.ptrDb, JniOps.PTR_DB)
                .write8(0) // flags
                .writeString("live/" + backupName) // backup dir
                .endHeader()
                .end()
                .push();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should never happen.");
        }
    }
    
    public synchronized boolean pipe(String storeFrom, int flags, String storeTo, File dir)
    {
        final LsmdbDatastore store = getStore(storeFrom, flags),
                storeTarget = getStore(storeTo, CREATE_IF_MISSING|ERROR_IF_EXISTS, dir);
        try
        {
            return store != null && stream.start(JniOps.PIPE_DB)
                .writePtr(store.ptrDb, JniOps.PTR_DB)
                .write8(flags) // flags
                .write64(storeTarget.ptrDb) // ptrTo
                .endHeader()
                .end()
                .push();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should never happen.");
        }
    }
    
    /**
     * Returns an existing datastore if it exists.
     * Otherwise, null is returned.
     */
    public synchronized LsmdbDatastore getStore(String path)
    {
        return created.get(path);
    }
    
    /**
     * Returns a new datastore one the first call, which will create a new dir 
     * (if it does not exist) with the provided {@code path} under the {@link dataDir}.
     */
    public synchronized LsmdbDatastore getStore(String path, int flags)
    {
        LsmdbDatastore store = created.get(path);
        if(store != null)
            return store;
        
        File dir = new File(dataDir, path); 
        File backupDir = new File(dir, "backup-live");
        if (!backupDir.exists())
            backupDir.mkdirs();

        store = new LsmdbDatastore(
                path, 
                newDb(flags, dir), 
                dir, 
                WRITE_FLAGS, 
                stream);
        
        created.put(path, store);
        
        return store;
    }
    
    /**
     * Returns a new datastore one the first call, which will be located on the existing 
     * {@code dir} provided.
     */
    public synchronized LsmdbDatastore getStore(String path, int flags, File dir)
    {
        LsmdbDatastore store = created.get(path);
        if(store != null)
            return store;
        
        if(!dir.exists() || !dir.isDirectory())
            throw new IllegalArgumentException(dir + " is not an existing dir.");
        
        File backupDir = new File(dir, "backup-live");
        if (!backupDir.exists())
            backupDir.mkdir();

        store = new LsmdbDatastore(
                path, 
                newDb(flags, dir), 
                dir, 
                WRITE_FLAGS, 
                stream);
        
        created.put(path, store);
        
        return store;
    }
    
    /**
     * Close the database.
     */
    public synchronized void close(String path)
    {
        LsmdbDatastore store = created.remove(path);
        if(store != null)
        {
            try
            {
                close(store);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Close all the databases created.
     */
    public synchronized void close()
    {
        if(created.isEmpty())
            return;
        
        for(LsmdbDatastore store : created.values())
        {
            try
            {
                close(store);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
        created.clear();
    }

}
