//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;


/**
 * Utilities for number manipulation.
 * 
 * @author David Yu
 * @created Dec 12, 2012
 */
public final class NumberUtil
{
    private NumberUtil() {}
    
    /**
     * Uses round half up, or bankers' rounding.
     */
    public static double r2(double d)
    {
        return Math.round(d * 100) / 100.0;
    }
    
    public static float fr2(float f)
    {
        return Math.round(f * 100) / 100;
    }
    
    public static double incrementAmount(double totalAmount, int quantity, double price)
    {
        if(quantity > 0)
        {
            // increase
            return quantity == 1 ? r2(totalAmount + price) : 
                r2(totalAmount + r2(price * quantity));
        }
        
        // decrease
        quantity = -quantity;
        return quantity == 1 ? r2(totalAmount - price) : 
            r2(totalAmount - r2(price * quantity));
    }
    
    /**
     * Double to cents (long).
     */
    public static long d2c(double d)
    {
        return (long)(r2(d) * 10000);
    }
    
    /**
     * Cents (long) to double.
     */
    public static double c2d(long l)
    {
        return ((double)l / 10000);
    }
    
    /**
     * Increment the amount (cents).
     */
    public static long incrementAmount(long totalAmount, int quantity, double price)
    {
        return totalAmount + (d2c(price) * quantity);
    }
    
    public static void main(String[] args)
    {
        double d = 100.0225d;
        long l = d2c(d);
        double back = c2d(l);
        System.err.println(d);
        System.err.println(l);
        System.err.println(back);
        System.err.println(d2c(back));
    }
}
