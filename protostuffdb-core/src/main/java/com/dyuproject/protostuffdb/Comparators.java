//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Comparator;

/**
 * Pre-defined comparators for byte arrays.
 *
 * @author David Yu
 * @created Mar 22, 2011
 */
public enum Comparators implements Comparator<byte[]>
{
    
    DEFAULT
    {
        public int compare(byte[] v1, byte[] v2)
        {
            for (int i = 0, len = Math.min(v1.length, v2.length); i < len; i++)
            {
                byte b1 = v1[i], b2 = v2[i];
                if(b1 == b2)
                    continue;
                
                return (0xFF & b1) - (0xFF & b2);
            }
            
            return v1.length - v2.length;
        }
    };

    
    /**
     * Util for comparing a byte array and a slice.
     */
    public static int compare(final byte[] data1, 
            final byte[] data2, int offset2, final int len2)
    {
        for (int i = 0, len = Math.min(data1.length, len2); i < len;)
        {
            byte b1 = data1[i++], b2 = data2[offset2++];
            if (b1 == b2)
                continue;
            
            return (0xFF & b1) - (0xFF & b2);
        }

        return data1.length - len2;
    }
}
