//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import protostuffdb.JniOps;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.JniStream;
import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.CAS;

/**
 * Lsmdb datastore.
 * 
 * @author David Yu
 * @created Dec 27, 2013
 */
public final class LsmdbDatastore extends Datastore
{
    public final long ptrDb;
    public final int writeFlags;
    
    public final long createTs;
    
    public LsmdbDatastore(String name, long ptrDb, File dir, int writeFlags, 
            final JniStream stream)
    {
        super(name, dir);
        
        if(ptrDb == 0)
            throw new RuntimeException("Invalid ptr to db.");
        
        this.ptrDb = ptrDb;
        this.writeFlags = writeFlags;
        
        createTs = initialize(stream);
    }
    
    private static final byte[] END_KEY = new byte[]{
        (byte)0xff, (byte)0xff,
        (byte)0xff, (byte)0xff, 
        (byte)0xff, (byte)0xff, 
        (byte)0xff, (byte)0xff, 
        (byte)0xff, (byte)0xff
    };
    
    public static boolean isEndKey(byte[] key)
    {
        return Arrays.equals(key, END_KEY);
    }
    
    private long initialize(final JniStream stream)
    {
        final byte[] key = END_KEY;
        
        long createTs = 0;
        
        byte[] data = LsmdbJniUtil.get(stream, true, 0, 
                key, null);
        
        if(data == null)
        {
            createTs = System.currentTimeMillis();
            data = new byte[6];
            KeyUtil.writeTimestamp(createTs, data, 0);
            
            LsmdbJniUtil.sput(stream, 0, key, data);
        }
        else if(data.length != 6)
        {
            throw new RuntimeException(
                    "Are you sure this is the right database file?");
        }
        else
        {
            createTs = KeyUtil.readTimestamp(data, 0);
        }
        
        return createTs;
    }
    
    public long getCreateTs()
    {
        return createTs;
    }
    
    public boolean isVisitorSessionIsolated()
    {
        return true;
    }
    
    public <T,P,V> boolean chain(EntityMetadata<T> root, Op<P> op, P param, 
            int retries, WriteContext context, byte[] key, 
            VisitorSession.Handler<V> vhandler, V vparam, VisitorSession vs)
    {
        context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        // TODO verify if we need to retry
        // we are having serialized writes so retries shouldn't be needed.
        boolean cleanup = true;
        final LsmdbOpChain chain = new LsmdbOpChain(root, context, vs);
        try
        {
            if(!op.handle(chain, key, null, 0, 0, param))
                return false;
            
            cleanup = false;
            
            if(vhandler != null)
                vhandler.handle(chain.vs(), vparam);
        }
        finally
        {
            // guaranteed to not throw errors which is why it should be first
            context.clearData();
            
            chain.close(cleanup ? JniOps.FCLOSE_CLEANUP_ONLY : 0);
        }
        
        return true;
    }
    
    public <P> void session(WriteContext context, 
            VisitorSession.Handler<P> handler, P param)
    {
        context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        final LsmdbVisitorSession session = new LsmdbVisitorSession(context);
        try
        {
            handler.handle(session, param);
        }
        finally
        {
            session.close();
        }
    }
    
    public void rawPut(byte[] key, int koffset, int klen, 
            byte[] value, int voffset, int vlen, WriteContext context)
    {
        // was:
        /*LsmdbJniUtil.put(context.stream.writePtr(ptrDb, JniOps.PTR_DB), true, 0, 
                key, koffset, klen, 
                value, voffset, vlen);*/
        
        LsmdbJniUtil.sput(
                context.stream.writePtr(ptrDb, JniOps.PTR_DB), 0, 
                key, koffset, klen, 
                value, voffset, vlen);
    }

    public void rawDelete(byte[] key, int koffset, int klen, WriteContext context)
    {
        // was:
        /*LsmdbJniUtil.delete(context.stream.writePtr(ptrDb, JniOps.PTR_DB), true, 0, 
                key, koffset, klen);*/
        
        LsmdbJniUtil.sdelete(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 0, 
                key, koffset, klen);
    }

    public byte[] rawGet(byte[] key, int koffset, int klen, WriteContext context)
    {
        // was:
        /*return LsmdbJniUtil.get(context.stream.writePtr(ptrDb, JniOps.PTR_DB), true, 0, 
                key, koffset, klen, null);*/
        
        return LsmdbJniUtil.sget(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 0, 
                key, koffset, klen, null);
    }
    
    public boolean rawExists(byte[] key, int koffset, int klen, WriteContext context)
    {
        // was:
        /*return ByteString.EMPTY_BYTE_ARRAY == LsmdbJniUtil.get(
                context.stream.writePtrDB(ptrDb), true, 0, 
                key, koffset, klen, ByteString.EMPTY_BYTE_ARRAY);*/
        
        return ByteString.EMPTY_BYTE_ARRAY == LsmdbJniUtil.sget(
                context.stream.writePtr(ptrDb, JniOps.PTR_DB), 0, 
                key, koffset, klen, ByteString.EMPTY_BYTE_ARRAY);
    }
    
    
    // ======================================================================
    
    
    public <T> boolean delete(byte[] key, EntityMetadata<T> em, byte[] parentKey, 
            WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(key);
                
                KeyUtil.validateSameKind(key, em);
                
                /*final Iterator c1 = db.iterator(rNoCache);
                try
                {
                    return delete(c1, db, wSync, key, em, context);
                }
                finally
                {
                    c1.delete();
                }*/
                return delete(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                        key, em, context);
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.parentNull(key);
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                /*final Iterator c2 = db.iterator(rNoCache);
                try
                {
                    return deleteWithParent(c2, db, wSync, key, em, context, 
                            parentKey);
                }
                finally
                {
                    c2.delete();
                }*/
                return deleteWithParent(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                        key, em, context, 
                        parentKey);
        }
        
        throw DSRuntimeExceptions.invalidArg("Delete operation not applicable for type: " + 
                em.type);
    }

    private static <T> boolean delete(
            //final Iterator cursor, final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, final EntityMetadata<T> em, final WriteContext context)
    {
        // TODO optimize
        if(!em.indexOnFields && 0 == em.linkIndex.length)
        {
            byte[] value = LsmdbJniUtil.get(stream, true, 0, 
                    key, null);
            
            if(value == null)
                return false;
            
            LsmdbJniUtil.delete(stream, true, 0, 
                    key);
            
            return true;
        }
        
        boolean cleanup = true;
        try
        {
            byte[] value = LsmdbJniUtil.get(stream, false, 0, 
                    key, null);
            
            if(value == null)
                return false;
            
            LsmdbJniUtil.writeHeaderDelete(stream, JniOps.FOP_CLOSE);
            
            stream.$writeKV(key, ByteString.EMPTY_BYTE_ARRAY);
            
            if(em.indexOnFields)
                indexOnDelete(stream, key, 0, em, context, value);
            
            if(0 != em.linkIndex.length)
                linkIndexOnDelete(stream, key, 0, em);
            
            LsmdbJniUtil.push(stream.end());
            
            cleanup = false;
        }
        finally
        {
            if(cleanup)
                LsmdbJniUtil.close(stream, JniOps.FCLOSE_CLEANUP_ONLY);
        }
        
        return true;
    }
    
    private static <T> boolean deleteWithParent(
            //final Iterator cursor, final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, final EntityMetadata<T> em, final WriteContext context, 
            final byte[] parentKey)
    {
        // TODO optimize
        final byte[] oneToManyKey = KeyUtil.newOneToManyKey(key, parentKey);
        
        boolean cleanup = true;
        try
        {
            byte[] value = LsmdbJniUtil.get(stream, false, 0, 
                    oneToManyKey, null);
            
            if(value == null)
                return false;
            
            LsmdbJniUtil.writeHeaderDelete(stream, JniOps.FOP_CLOSE);
            
            stream.$writeKV(key, ByteString.EMPTY_BYTE_ARRAY)
                .$writeKV(oneToManyKey, ByteString.EMPTY_BYTE_ARRAY);
            
            if(em.indexOnFields)
                indexOnDelete(stream, oneToManyKey, 9, em, context, value);
            
            if(0 != em.linkIndex.length)
                linkIndexOnDelete(stream, oneToManyKey, 9, em);
            
            LsmdbJniUtil.push(stream.end());
            
            cleanup = false;
        }
        finally
        {
            if(cleanup)
                LsmdbJniUtil.close(stream, JniOps.FCLOSE_CLEANUP_ONLY);
        }
        
        return true;
    }
    
    public <T> boolean update(byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener, 
            byte[] parentKey, WriteContext context)
    {
        if(cas.isEmpty())
            throw DSRuntimeExceptions.invalidArg("CAS has no operations to perform");
        
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(key);
                
                KeyUtil.validateSameKind(key, em);
                
                return update(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                        key, 
                        key, 
                        cas, listener, em, context);
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.parentNull(key);
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return update(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                        key, 
                        KeyUtil.newOneToManyKey(key, parentKey), 
                        cas, listener, em, context);
        }
        
        throw DSRuntimeExceptions.invalidArg("Delete operation not applicable for type: " + 
                em.type);
    }
    
    private static <T> boolean update(
            //final Iterator cursor, final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, final byte[] searchKey, 
            final CAS cas, final CAS.Listener listener, 
            final EntityMetadata<T> em, final WriteContext context)
    {
        boolean cleanup = true;
        try
        {
            byte[] value = LsmdbJniUtil.get(stream, false, 0, 
                    searchKey, null);
            
            if(value == null)
                return false;
            
            if(listener != null && !listener.onBeforeApply(searchKey, value, null))
                return false;
            
            final int size = context.casAndIndexCollect(value, 0, value.length, em, cas);
            final CASAndIndexCollectOutput output = context.casAndIndexCollectOutput;
            
            if (output.casCount == 0)
                return false;
            switch (em.updateFlags)
            {
                case 1:
                    if (output.casCount == 1 && output.isUpdated(em.revField, cas))
                        return false;
                    break;
                case 2:
                    if (output.casCount == 1 && output.isUpdated(em.updateTsField, cas))
                        return false;
                    break;
                case 3:
                    if (output.casCount == 2 && 
                        output.isUpdated(em.revField, cas) && 
                        output.isUpdated(em.updateTsField, cas))
                    {
                        return false;
                    }
                    break;
            }
            
            final byte[] entityBuffer = context.entityBuffer;
            final int entityOffset = context.entityOffset;
            
            if (listener != null && !listener.onApply(searchKey, value, cas, output, null, 
                    entityBuffer, entityOffset, size))
            {
                return false;
            }
            
            final boolean kvAbsoluteOffset = stream.isHeadPointingTo(
                    entityBuffer, entityOffset);
            
            if(kvAbsoluteOffset)
            {
                LsmdbJniUtil.writeHeaderPut(stream, JniOps.FOP_CLOSE, size);
                
                stream.$writeKVAO(searchKey, 0, searchKey.length, 
                        entityOffset - 4);
            }
            else
            {
                LsmdbJniUtil.writeHeaderPut(stream, JniOps.FOP_CLOSE);
                
                stream.$writeKV(searchKey, 0, searchKey.length, 
                        entityBuffer, entityOffset, size);
            }
            
            if(em.indexOnFields)
            {
                indexOnUpdate(stream, output, 
                        searchKey, searchKey.length == 9 ? 0 : 9, 
                        em, context, 
                        entityBuffer, entityOffset, size, 
                        value, 
                        kvAbsoluteOffset);
            }
            
            if(0 != em.linkIndex.length)
            {
                linkIndexOnWrite(stream, 
                        searchKey, searchKey.length == 9 ? 0 : 9, 
                        em, 
                        entityBuffer, entityOffset, size, 
                        kvAbsoluteOffset);
            }
            
            LsmdbJniUtil.push(stream.end());
            
            cleanup = false;
        }
        finally
        {
            if(cleanup)
                LsmdbJniUtil.close(stream, JniOps.FCLOSE_CLEANUP_ONLY);
        }
        
        return true;
    }
    
    /* ================================================== */
    
    @Override
    public boolean exists(boolean prefix, WriteContext context, KeyBuilder kb, boolean pop)
    {
        if (!prefix)
        {
            return ByteString.EMPTY_BYTE_ARRAY == LsmdbJniUtil.sget(
                    context.stream.writePtr(ptrDb, JniOps.PTR_DB), 0, 
                    kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), 
                    ByteString.EMPTY_BYTE_ARRAY);
        }
        
        return LsmdbJniUtil.iget(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                JniOps.FGET_PREFIX | JniOps.FOP_CLOSE, 
                kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), 
                null, false, false, false);
    }
    
    @Override
    public boolean get(HasKV kv, WriteContext context, KeyBuilder kb, boolean pop)
    {
        byte[] value = LsmdbJniUtil.sget(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 0, 
                kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), null);
        
        if (value == null)
            return false;
        
        kv.setValue(value);
        return true;
    }
    
    @Override
    public boolean pget(HasKV kv, boolean includeValue, boolean extractEntityKey, 
            WriteContext context, KeyBuilder kb, boolean pop)
    {
        return LsmdbJniUtil.iget(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                JniOps.FGET_PREFIX | JniOps.FOP_CLOSE, 
                kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), 
                kv, includeValue, true, extractEntityKey);
    }

    /* ================================================== */
    
    public <T> boolean exists(byte[] key, EntityMetadata<T> em, byte[] parentKey, 
            WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(key);
                
                KeyUtil.validateSameKind(key, em);
                
                return rawExists(key, context);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw new InvalidArgException("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    
                    return rawExists(key, context);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return rawExists(KeyUtil.newOneToManyKey(key, parentKey), context);
        }
        
        throw DSRuntimeExceptions.invalidArg("Exists operation not applicable for type: " + 
                em.type);
    }
    
    public <T> byte[] get(byte[] key, EntityMetadata<T> em, byte[] parentKey, 
            WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(key);
                
                KeyUtil.validateSameKind(key, em);
                
                return rawGet(key, context);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw new InvalidArgException("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    return rawGet(key, context);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return rawGet(KeyUtil.newOneToManyKey(key, parentKey), context);
        }
        
        throw DSRuntimeExceptions.invalidArg("Get operation not applicable for type: " + 
                em.type);
    }
    
    protected <T> void insert(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context, 
            SerAndIndexCollectOutput output, 
            byte[] entityBuffer, int entityOffset, int size)
    {
        if(parentKey == null)
        {
            insert(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                    key, em, context, 
                    output, entityBuffer, entityOffset, size);
        }
        else
        {
            insertWithParent(context.stream.writePtr(ptrDb, JniOps.PTR_DB), 
                    key, em, context, parentKey, 
                    output, entityBuffer, entityOffset, size);
        }
    }

    private static <T> void insert(//final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, 
            final EntityMetadata<T> em, final WriteContext context, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size)
    {
        final boolean kvAbsoluteOffset = stream.isHeadPointingTo(
                entityBuffer, entityOffset);
        
        if(!kvAbsoluteOffset && !em.indexOnFields && 0 == em.linkIndex.length)
        {
            // regular operation with no index
            
            LsmdbJniUtil.put(stream, true, 0, 
                    key, 0, key.length, 
                    entityBuffer, entityOffset, size);
            
            return;
        }
        
        if(kvAbsoluteOffset)
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0, size);
            
            stream.$writeKVAO(key, 0, key.length, 
                    entityOffset - 4);
        }
        else
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0);
            
            stream.$writeKV(key, 0, key.length, 
                    entityBuffer, entityOffset, size);
        }
        
        if(em.indexOnFields)
        {
            indexOnInsert(stream, 
                    key, 0, 
                    em, context, 
                    output, entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        if(0 != em.linkIndex.length)
        {
            linkIndexOnWrite(stream, key, 0, em, 
                    entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        LsmdbJniUtil.push(stream.end());
    }

    private static <T> void insertWithParent(//final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, 
            final EntityMetadata<T> em, final WriteContext context, 
            final byte[] parentKey, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size)
    {
        final byte[] oneToManyKey = KeyUtil.newOneToManyKey(key, parentKey);
        
        final boolean kvAbsoluteOffset = stream.isHeadPointingTo(
                entityBuffer, entityOffset);
        
        if(kvAbsoluteOffset)
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0, size);
            
            stream.$writeKVAO(oneToManyKey, 0, oneToManyKey.length, 
                    entityOffset - 4);
        }
        else
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0);
            
            stream.$writeKV(oneToManyKey, 0, oneToManyKey.length, 
                    entityBuffer, entityOffset, size);
        }
        
        stream.$writeKV(key, parentKey);
        
        if(em.indexOnFields)
        {
            indexOnInsert(stream, 
                    oneToManyKey, 9, 
                    em, context, 
                    output, entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        if(0 != em.linkIndex.length)
        {
            linkIndexOnWrite(stream, oneToManyKey, 9, em, 
                    entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        LsmdbJniUtil.push(stream.end());
    }
    
    static <T> void linkIndexOnWrite(//final WriteBatch batch, 
            final JniStream stream, 
            final byte[] key, final int keyOffset,
            final EntityMetadata<T> em, 
            final byte[] entityBuffer, int entityOffset, int size, 
            final boolean kvAbsoluteOffset)
    {
        // the last one is the kind
        final byte[] linkIndexKey = new byte[10];
        linkIndexKey[0] = (byte)(0x80 | key[keyOffset]);
        System.arraycopy(key, keyOffset+1, linkIndexKey, 1, 8);
        
        for(int kind : em.linkIndex)
        {
            linkIndexKey[9] = (byte)kind;
            
            if(kvAbsoluteOffset)
            {
                stream.$writeKVAO(linkIndexKey, 0, linkIndexKey.length, 
                        entityOffset - 4);
            }
            else
            {
                stream.$writeKV(linkIndexKey, 0, linkIndexKey.length, 
                        entityBuffer, entityOffset, size);
            }
        }
    }
    
    static <T> void linkIndexOnDelete(//final WriteBatch batch, 
            final JniStream stream, 
            byte[] key, int keyOffset,
            EntityMetadata<T> em)
    {
        final byte[] linkIndexKey = new byte[10];
        linkIndexKey[0] = (byte)(0x80 | key[keyOffset]);
        System.arraycopy(key, keyOffset+1, linkIndexKey, 1, 8);
        
        for(int kind : em.linkIndex)
        {
            linkIndexKey[9] = (byte)kind;
            
            stream.$writeKV(linkIndexKey, ByteString.EMPTY_BYTE_ARRAY);
        }
    }
    
    private static void deleteSecondaryTags(//WriteBatch batch, 
            final JniStream stream, 
            int id, int secondaryTags, 
            byte[] buf, int rmOffset, int rmKeySize)
    {
        if(id > 223)
        {
            // entity secondary index
            buf[++rmOffset] = 0;
            rmKeySize--;
        }
        
        for(int shift = 0; shift <= 24; shift+=8)
        {
            int tag = 0xFF & (secondaryTags >>> shift);
            if(tag == 0)
                break;
            
            buf[rmOffset+1] = (byte)tag;
            
            stream.$writeKV(buf, rmOffset, rmKeySize, 
                    ByteString.EMPTY_BYTE_ARRAY, 0, 0);
        }
    }
    
    static <T> void indexOnDelete(//final WriteBatch batch, 
            final JniStream stream, 
            byte[] key, int keyOffset, 
            EntityMetadata<T> em, WriteContext context, 
            byte[] value)
    {
        final IndexCollectOutput output = context.indexCollectOutput;
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        
        final int[] result = context.intset();
        
        for(int i = 0, count = context.indexCollect(
                value, 0, value.length, em), 
                field = 0, oldKeySize = 0;
                i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                oldKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                        em, context, id, output.fieldOffsetRemoveIdx, 
                        output.varint32Idx, output.varint64Idx, 
                        key, keyOffset, 
                        value, 0, value.length, 
                        result, true);
                
                stream.$writeKV(buf, bufOffset, oldKeySize, 
                        ByteString.EMPTY_BYTE_ARRAY, 0, 0);
                
                final int secondaryTags = em.index.secondaryTagEntries[id];
                if(secondaryTags != 0)
                {
                    deleteSecondaryTags(stream, id, secondaryTags, 
                            buf, bufOffset, oldKeySize);
                }
            }
        }
        
        // value dependencies
        for(int i = 0, id = 0, oldKeySize = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            oldKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                    em, context, id, output.fieldOffsetRemoveIdx, 
                    output.varint32Idx, output.varint64Idx, 
                    key, keyOffset, 
                    value, 0, value.length, 
                    result, true);
            
            stream.$writeKV(buf, bufOffset, oldKeySize, 
                    ByteString.EMPTY_BYTE_ARRAY, 0, 0);
            
            final int secondaryTags = em.index.secondaryTagEntries[id];
            if(secondaryTags != 0)
            {
                deleteSecondaryTags(stream, id, secondaryTags, 
                        buf, bufOffset, oldKeySize);
            }
        }
    }
    
    private static <T> void writeIndexOnInsert(//final WriteBatch batch, 
            final JniStream stream, 
            final SerAndIndexCollectOutput output, final int id, 
            final byte[] key, final int keyOffset, 
            final EntityMetadata<T> em, final WriteContext context, 
            final byte[] buf, int bufOffset, 
            final int[] result, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            final byte[] ts, int tsoffset, int tslen, 
            boolean kvAbsoluteOffset)
    {
        final int secondaryTags = em.index.secondaryTagEntries[id];
        // key
        final int newKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetAddIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                entityBuffer, entityOffset, size, 
                result, false);
        
        // value
        final int newValueSize = result[0];
        final byte[] w;
        final int woffset, wlen;
        if(newValueSize == 0)
        {
            // timestamp
            w = ts;
            woffset = tsoffset;
            wlen = tslen;
            
            kvAbsoluteOffset = false;
        }
        else if(newValueSize == -1)
        {
            result[0] = 0; // reset
            
            // the message itself
            w = entityBuffer;
            woffset = entityOffset;
            wlen = size;
        }
        else
        {
            result[0] = 0; // reset
            
            // custom value
            w = buf;
            woffset = bufOffset + newKeySize;
            wlen = newValueSize;
            
            kvAbsoluteOffset = false;
        }
        
        if(kvAbsoluteOffset)
        {
            stream.$writeKVAO(buf, bufOffset, newKeySize, 
                    entityOffset - 4);
        }
        else
        {
            stream.$writeKV(buf, bufOffset, newKeySize, 
                    w, woffset, wlen);
        }

        
        if(secondaryTags == 0)
            return;
        
        int tagKeyOffset = bufOffset, tagKeySize = newKeySize;
        if(id > 223)
        {
            // entity secondary index
            buf[++tagKeyOffset] = 0;
            tagKeySize--;
        }
        
        for(int shift = 0; shift <= 24; shift+=8)
        {
            int tag = 0xFF & (secondaryTags >>> shift);
            if(tag == 0)
                break;
            
            buf[tagKeyOffset+1] = (byte)tag;
            
            if(kvAbsoluteOffset)
            {
                stream.$writeKVAO(buf, tagKeyOffset, tagKeySize, 
                        entityOffset - 4);
            }
            else
            {
                stream.$writeKV(buf, tagKeyOffset, tagKeySize, 
                        w, woffset, wlen);
            }
        }
    }
    
    static <T> void indexOnInsert(//final WriteBatch batch, 
            final JniStream stream, 
            final byte[] key, final int keyOffset, 
            final EntityMetadata<T> em, final WriteContext context, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            final boolean kvAbsoluteOffset)
    {
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        final int[] result = context.intset();
        
        for(int i = 0, count = output.count, field = 0; i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                writeIndexOnInsert(stream, output, id, 
                        key, keyOffset, 
                        em, context, 
                        buf, bufOffset, 
                        result, 
                        entityBuffer, entityOffset, size, 
                        key, keyOffset+1, 6, 
                        kvAbsoluteOffset);
            }
        }
        
        // value dependencies
        for(int i = 0, id = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            writeIndexOnInsert(stream, output, id, 
                    key, keyOffset, 
                    em, context, 
                    buf, bufOffset, 
                    result, 
                    entityBuffer, entityOffset, size, 
                    key, keyOffset+1, 6, 
                    kvAbsoluteOffset);
        }
    }
    
    private static <T> void writeIndexOnUpdate(//final WriteBatch batch, 
            final JniStream stream, 
            final CASAndIndexCollectOutput output, 
            final int id, //final boolean valueDependency, 
            final byte[] key, final int keyOffset, 
            final EntityMetadata<T> em, final WriteContext context, 
            final byte[] buf, final int bufOffset, 
            final int[] result, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            final byte[] oldValue, 
            final byte[] ts, 
            boolean kvAbsoluteOffset)
    {
        final int secondaryTags = em.index.secondaryTagEntries[id];
        
        // key
        final int newKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                em, context, id, output.fieldOffsetAddIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                entityBuffer, entityOffset, size, 
                result, false);
        
        // value
        final int newValueSize = result[0];
        final byte[] w;
        final int woffset, wlen;
        
        if(newValueSize == 0)
        {
            // timestamp
            w = ts;
            woffset = 0;
            wlen = ts.length;
            
            kvAbsoluteOffset = false;
        }
        else if(newValueSize == -1)
        {
            result[0] = 0; // reset
            
            // the message itself.
            w = entityBuffer;
            woffset = entityOffset;
            wlen = size;
        }
        else
        {
            result[0] = 0; // reset
            
            // custom value
            w = buf;
            woffset = bufOffset + newKeySize;
            wlen = newValueSize;
            
            kvAbsoluteOffset = false;
        }
        
        if(kvAbsoluteOffset)
            stream.$writeKVAO(buf, bufOffset, newKeySize, entityOffset - 4);
        else
            stream.$writeKV(buf, bufOffset, newKeySize, w, woffset, wlen);
        
        byte b0 = 0, b1 = 0;
        if(secondaryTags != 0)
        {
            int tagKeyOffset = bufOffset, tagKeySize = newKeySize;
            if(id > 223)
            {
                // entity secondary index
                
                // save
                b0 = buf[++tagKeyOffset];
                
                buf[tagKeyOffset] = 0;
                tagKeySize--;
            }
            else
            {
                // save
                b0 = buf[tagKeyOffset];
            }
            
            // save
            b1 = buf[tagKeyOffset+1];
            
            for(int shift = 0; shift <= 24; shift+=8)
            {
                int tag = 0xFF & (secondaryTags >>> shift);
                if(tag == 0)
                    break;
                
                buf[tagKeyOffset+1] = (byte)tag;
                
                if(kvAbsoluteOffset)
                {
                    stream.$writeKVAO(buf, tagKeyOffset, tagKeySize, 
                            entityOffset - 4);
                }
                else
                {
                    stream.$writeKV(buf, tagKeyOffset, tagKeySize, 
                            w, woffset, wlen);
                }
            }
        }
        
        // remove the old keys
        
        if(0 < em.index.offsetEntries[id])
        {
            // the fields in the index key are all immutable
            // don't bother removing since we already replaced the value
            return;
        }
        
        if (secondaryTags != 0)
        {
            // restore
            if (id > 223)
            {
                buf[bufOffset+1] = b0;
                buf[bufOffset+2] = b1;
            }
            else
            {
                buf[bufOffset] = b0;
                buf[bufOffset+1] = b1;
            }
        }
        
        final int rmOffset = bufOffset + newKeySize;
        
        // old key
        final int rmKeySize = IndexUtil.writeIndexKeyTo(buf, rmOffset, 
                em, context, id, output.fieldOffsetRemoveIdx, 
                output.varint32Idx, output.varint64Idx, 
                key, keyOffset, 
                oldValue, 0, oldValue.length, 
                result, true);
        
        if(0 == ValueUtil.compare(buf, bufOffset, newKeySize, buf, rmOffset, rmKeySize))
        {
            // nothing changed in the index key
            // don't bother removing since we already replaced the value
            return;
        }
        
        stream.$writeKV(buf, rmOffset, rmKeySize, 
                ByteString.EMPTY_BYTE_ARRAY, 0, 0);
        
        if(secondaryTags != 0)
            deleteSecondaryTags(stream, id, secondaryTags, buf, rmOffset, rmKeySize);
    }
    
    static <T> void indexOnUpdate(//final WriteBatch batch, 
            final JniStream stream, 
            final CASAndIndexCollectOutput output, 
            final byte[] key, final int keyOffset, 
            final EntityMetadata<T> em, final WriteContext context, 
            final byte[] entityBuffer, final int entityOffset, final int size, 
            final byte[] value, 
            final boolean kvAbsoluteOffset)
    {
        final byte[] ts = KeyUtil.new6ByteTimestamp();
        final byte[] buf = context.udfBuffer;
        final int bufOffset = context.udfBufOffset;
        
        final int[] result = context.intset();
        
        // value dependencies
        for(int i = 0, id = 0, count = em.index.valueCount; i < count;)
        {
            id = em.index.valueEntries[i++];
            
            if(0 != result[id]++)
                continue;
            
            writeIndexOnUpdate(stream, output, id, //true, 
                    key, keyOffset, 
                    em, context, 
                    buf, bufOffset, 
                    result, 
                    entityBuffer, entityOffset, size, 
                    value, ts, 
                    kvAbsoluteOffset);
        }
        
        for(int i = 0, count = output.trackCount, field = 0; 
                i < count;)
        {
            field = output.fieldsTracked[i++];

            for(int id : em.index.getIndices(field))
            {
                if(id == 0)
                    break;
                
                if(0 != result[id]++)
                    continue;
                
                writeIndexOnUpdate(stream, output, id, //false, 
                        key, keyOffset, 
                        em, context, 
                        buf, bufOffset, 
                        result, 
                        entityBuffer, entityOffset, size, 
                        value, ts, 
                        kvAbsoluteOffset);
            }
        }
    }
    
    public <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            boolean extractEntityKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys (list) empty");
        
        final JniStream stream = context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        LsmdbJniUtil.writeHeaderGet(stream, 0);
        
        byte[] k;
        int koffset = 0, klen = 9;
        if(reverse)
        {
            for(int i = keys.size(); i-- > 0;)
            {
                k = keys.get(i);
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i);
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        
        return LsmdbJniUtil.multiGetKV(stream.end(), 
                visitor, param);
    }
    
    public <V> int visitHasKeys(List<? extends HasKey> keys, boolean reverse, 
            boolean extractEntityKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys (list) empty");
        
        final JniStream stream = context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        LsmdbJniUtil.writeHeaderGet(stream, 0);
        
        byte[] k;
        int koffset = 0, klen = 9;
        if(reverse)
        {
            for(int i = keys.size(); i-- > 0;)
            {
                k = keys.get(i).getKey();
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i).getKey();
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        
        return LsmdbJniUtil.multiGetKV(stream.end(), 
                visitor, param);
    }
    
    public <T, V> int visitKind(EntityMetadata<T> em, int limit, boolean desc, 
            byte[] startKey, byte[] parentKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        final JniStream stream = context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        switch(em.type)
        {
            case DEFAULT:
            {
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(null);
                
                if(startKey == null)
                {
                    // TODO single byte array + len arg
                    final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
                    final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
                    
                    return desc ? visitRangeDesc(stream, true, 0, limit, 
                            visitor, param, sk, 0, sk.length, ek, 0, ek.length, false) : visitRangeAsc(stream, true, 0, 
                                    limit, visitor, param, 
                                    sk, 0, sk.length, ek, 0, ek.length, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                if(desc)
                {
                    final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
                    
                    return visitRangeWithIndexKeyDesc(stream, true, 0, limit, startKey, 
                            visitor, param, 
                            sk, 0, sk.length, false);
                }
                
                final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
                
                return visitRangeWithIndexKeyAsc(stream, true, 0, limit, startKey, 
                        visitor, param, 
                        ek, 0, ek.length, false);
            }
                
            case LINKED_CHILD:
            {
                if(parentKey == null)
                    throw DSRuntimeExceptions.parentNull(null);
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                if(startKey == null)
                {
                    // TODO single byte array + len arg
                    final byte[] sk = KeyUtil.newLinkRangeKeyStart(em.kind, parentKey);
                    final byte[] ek = KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey);
                    
                    return desc ? visitRangeDesc(stream, true, 0, limit, 
                            visitor, param, sk, 0, sk.length, ek, 0, ek.length, false) : visitRangeAsc(stream, true, 0,
                                    limit, visitor, param, sk, 0, sk.length, ek, 0, ek.length, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                final byte[] ik = KeyUtil.newLinkIndexKey(em.kind, parentKey, startKey);
                
                if(desc)
                {
                    final byte[] sk = KeyUtil.newLinkRangeKeyStart(em.kind, parentKey);
                    
                    return visitRangeWithIndexKeyDesc(stream, true, 0, limit, ik, 
                            visitor, param, 
                            sk, 0, sk.length, 
                            false);
                }
                
                final byte[] ek = KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey);
                
                return visitRangeWithIndexKeyAsc(stream, true, 0, limit, ik, 
                        visitor, param, 
                        ek, 0, ek.length,  
                        false);
            }
        }
        
        throw DSRuntimeExceptions.invalidArg("Visit operation not applicable for type: " + 
                em.type);
    }
    
    public <V> int scan(boolean keysOnly, int limit, boolean desc, 
            byte[] limitKey, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        // TODO incremental jni scan
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        
        return LsmdbJniUtil.scan(context.stream.writePtr(ptrDb, JniOps.PTR_DB), true, 0, 
                keysOnly, 
                limited ? limit : 0, 
                desc, 
                limitKey == null ? ByteString.EMPTY_BYTE_ARRAY : limitKey, 
                visitor, param);
    }
    
    public <V> int visitRange(
            boolean keysOnly, 
            int limit, boolean desc, 
            byte[] rawStartKey,
            WriteContext context, 
            Visitor<V> visitor, V param,
            boolean valueAsKey, boolean skRelativeLimit, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen)
    {
        final JniStream stream = context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        int flags = 0;
        if (valueAsKey)
            flags |= JniOps.FSCAN_VALUE_AS_KEY;
        if (skRelativeLimit)
            flags |= JniOps.FSCAN_RELATIVE_LIMIT;
        
        if(rawStartKey == null)
        {
            return desc ? visitRangeDesc(stream, true, flags, limit, 
                    visitor, param, sk, skoffset, sklen, ek, ekoffset, eklen, keysOnly) : visitRangeAsc(stream, true, flags, 
                            limit, visitor, param, sk, skoffset, sklen, ek, ekoffset, eklen, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, sk, skoffset, sklen) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, ek, ekoffset, eklen) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            return visitRangeWithIndexKeyDesc(stream, true, flags, limit, rawStartKey, 
                    visitor, param, sk, skoffset, sklen, keysOnly);
        }
        
        return visitRangeWithIndexKeyAsc(stream, true, flags, limit, rawStartKey, 
                visitor, param, ek, ekoffset, eklen, keysOnly);
    }
    
    public <V> int visitIndex(
            boolean keysOnly, 
            int limit, boolean desc, 
            byte[] rawStartKey,
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop)
    {
        final JniStream stream = context.stream.writePtr(ptrDb, JniOps.PTR_DB);
        
        final int offset = kb.offset(), size = pop ? kb.popLen() : kb.len();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            // append 0x00
            buf[offset+size] = 0;
            final byte[] sk = new byte[size+1];
            System.arraycopy(buf, offset, sk, 0, size+1);
            
            // append 0x01
            buf[offset+size] = (byte)1;
            //final byte[] ek = new byte[size+1];
            //System.arraycopy(buf, offset, ek, 0, size+1);
            
            return desc ? visitRangeDesc(stream, true, 0, limit, 
                    visitor, param, sk, 0, sk.length, buf, offset, size+1, keysOnly) : visitRangeAsc(stream, true, 0, 
                            limit, visitor, param, sk, 0, sk.length, buf, offset, size+1, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            // append 0x00
            buf[offset+size] = 0;
            //final byte[] sk = new byte[size+1];
            //System.arraycopy(buf, offset, sk, 0, size+1);
            
            return visitRangeWithIndexKeyDesc(stream, true, 0, limit, rawStartKey, 
                    visitor, param, buf, offset, size+1, keysOnly);
        }
        
        // append 0x01
        buf[offset+size] = (byte)1;
        //final byte[] ek = new byte[size+1];
        //System.arraycopy(buf, offset, ek, 0, size+1);
        
        return visitRangeWithIndexKeyAsc(stream, true, 0, limit, rawStartKey, 
                visitor, param, buf, offset, size+1, keysOnly);
    }

    /*public <V> int visitIndexPrefixMatch(
            boolean keysOnly, 
            int limit, boolean desc,
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        final int offset = kb.offset(), size = kb.popLen();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            //final byte[] sk = new byte[size];
            //System.arraycopy(buf, offset, sk, 0, size);
            
            // append 0xFF
            buf[offset+size] = (byte)0xFF;
            //final byte[] ek = new byte[size+1];
            //System.arraycopy(buf, offset, ek, 0, size+1);
            
            final Iterator cursor = db.iterator(rNoCache);
            try
            {
                return desc ? visitRangeDesc(cursor, limit, 
                        visitor, param, buf, offset, size, buf, offset, size+1, keysOnly) : visitRangeAsc(cursor, 
                                limit, visitor, param, buf, offset, size, buf, offset, size+1, keysOnly);
            }
            finally
            {
                cursor.delete();
            }
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            //final byte[] sk = new byte[size];
            //System.arraycopy(buf, offset, sk, 0, size);
            
            final Iterator cursor = db.iterator(rNoCache);
            try
            {
                return visitRangeWithIndexKeyDesc(cursor, limit, rawStartKey, 
                        false, visitor, param, buf, offset, size, keysOnly);
            }
            finally
            {
                cursor.delete();
            }
        }
        
        // append 0xFF
        buf[offset+size] = (byte)0xFF;
        //final byte[] ek = new byte[size+1];
        //System.arraycopy(buf, offset, ek, 0, size+1);
        
        final Iterator cursor = db.iterator(rNoCache);
        try
        {
            return visitRangeWithIndexKeyAsc(cursor, limit, rawStartKey, 
                    false, visitor, param, buf, offset, size+1, keysOnly);
        }
        finally
        {
            cursor.delete();
        }
    }*/
    
    static <T> int visitRangeAsc(//Iterator cursor, 
            final JniStream stream, final boolean end, final int flags,
            final int limit, 
            Visitor<T> visitor, T param, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        
        return LsmdbJniUtil.rangeScan(stream, end, flags, 
                keysOnly, limited ? limit : 0, false, 
                sk, skoffset, sklen, 
                ek, ekoffset, eklen, 
                visitor, 
                param);
    }
    
    static <T> int visitRangeDesc(//Iterator cursor, 
            final JniStream stream, final boolean end, final int flags,
            final int limit, 
            Visitor<T> visitor, T param, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        
        return LsmdbJniUtil.rangeScan(stream, end, flags, 
                keysOnly, limited ? limit : 0, true, 
                sk, skoffset, sklen, 
                ek, ekoffset, eklen, 
                visitor, 
                param);
    }
    
    static <T> int visitRangeWithIndexKeyAsc(//Iterator cursor, 
            final JniStream stream, final boolean end, final int flags,
            final int limit, 
            byte[] ik, //boolean inclusiveIndexKey, 
            Visitor<T> visitor, T param, 
            byte[] ek, int ekoffset, int eklen, 
            boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        
        return LsmdbJniUtil.rangeScan(stream, end, flags | JniOps.FSCAN_EXCLUDE_START_KEY, 
                keysOnly, limited ? limit : 0, false, 
                ik, 0, ik.length, 
                ek, ekoffset, eklen, 
                visitor, 
                param);
    }
    
    static <T> int visitRangeWithIndexKeyDesc(//Iterator cursor, 
            final JniStream stream, final boolean end, final int flags,
            final int limit, 
            byte[] ik, //boolean inclusiveIndexKey, 
            Visitor<T> visitor, T param,
            byte[] sk, int skoffset, int sklen, 
            boolean keysOnly)
    {
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        
        return LsmdbJniUtil.rangeScan(stream, end, flags, 
                keysOnly, limited ? limit : 0, true, 
                sk, skoffset, sklen, 
                ik, 0, ik.length, 
                visitor, 
                param);
    }

}
