//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.LsmdbDatastore.indexOnDelete;
import static com.dyuproject.protostuffdb.LsmdbDatastore.indexOnInsert;
import static com.dyuproject.protostuffdb.LsmdbDatastore.indexOnUpdate;
import static com.dyuproject.protostuffdb.LsmdbDatastore.linkIndexOnDelete;
import static com.dyuproject.protostuffdb.LsmdbDatastore.linkIndexOnWrite;

import java.util.ArrayList;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.JniStream;
import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuff.ds.CAS.Listener;

/**
 * Chained operations for native dbs.
 * 
 * @author David Yu
 * @created Dec 27, 2013
 */
public final class LsmdbOpChain extends OpChain// implements Visitor<byte[]>
{
    
    final JniStream stream;
    
    public LsmdbOpChain(EntityMetadata<?> root, WriteContext context, VisitorSession vs)
    {
        super(root, context, vs);
        stream = context.stream;
    }
    
    void close(int flags)
    {
        // TODO do not close the iterator if you do not own it.
        LsmdbJniUtil.close(stream, flags);
    }

    // Can't do this yet since the rangeDelete might overflow and the data we've 
    // just written for put will get overwritten.
    /*public boolean visit(byte[] key, byte[] value, byte[] prefix, int index)
    {
        if(index == 0)
            LsmdbJniUtil.writeHeaderPut(stream, 0);
        
        System.arraycopy(prefix, 0, key, 0, key.length);
        
        stream.$writeKV(key, value);
        
        return false;
    }*/

    @Override
    protected VisitorSession newVisitorSession()
    {
        return new LsmdbVisitorSession(context);
    }
    
    @Override
    public void rawPut(byte[] key, int koffset, int klen, 
            byte[] value, int voffset, int vlen)
    {
        LsmdbJniUtil.put(stream, false, 0, 
                key, koffset, klen, 
                value, voffset, vlen);
    }

    @Override
    public void rawDelete(byte[] key, int koffset, int klen)
    {
        LsmdbJniUtil.delete(stream, false, 0, 
                key, koffset, klen);
    }

    @Override
    public int rangeRemove(byte[] startKey, byte[] endKey)
    {
        return LsmdbJniUtil.rangeDelete(stream, false, 0, 
                true, 0, false, 
                startKey, endKey, Visitor.NOOP, null);
    }

    @Override
    public int rangeReplace(byte[] startKey, byte[] endKey, 
            byte[] prefix, int offset, int len)
    {
        final ArrayList<HasKV> param = new ArrayList<HasKV>();
        
        LsmdbJniUtil.rangeDelete(stream, false, 0, 
                false, 0, false, 
                startKey, endKey, 
                Visitor.APPEND_KV, param);
        
        LsmdbJniUtil.writeHeaderPut(stream, 0);
        
        for(HasKV kv : param)
        {
            byte[] k = kv.getKey();
            
            System.arraycopy(prefix, 0, k, 0, len);
            
            stream.$writeKV(k, kv.getValue());
        }
        
        LsmdbJniUtil.push(stream.limit());
        
        return param.size();
    }

    @Override
    protected <T, P> boolean insert(byte[] key, EntityMetadata<T> em, byte[] parentKey,
            SerAndIndexCollectOutput output, byte[] entityBuffer, int entityOffset, int size,
            Op<P> nextOp, P param)
    {
        if(parentKey == null)
        {
            insert(stream, key, em, context, 
                    output, entityBuffer, entityOffset, size);
        }
        else
        {
            insertWithParent(stream, key, em, context, parentKey, 
                    output, entityBuffer, entityOffset, size);
        }
        
        return nextOp == null || nextOp.handle(this, key, 
                entityBuffer, entityOffset, size, param);
    }
    
    private static <T> void insert(//final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, 
            final EntityMetadata<T> em, final WriteContext context, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size)
    {
        final boolean kvAbsoluteOffset = stream.isHeadPointingTo(
                entityBuffer, entityOffset);
        
        if(kvAbsoluteOffset)
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0, size);
            
            stream.$writeKVAO(key, 0, key.length, 
                    entityOffset - 4);
        }
        else
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0);
            
            stream.$writeKV(key, 0, key.length, 
                    entityBuffer, entityOffset, size);
        }
        
        if(em.indexOnFields)
        {
            indexOnInsert(stream, 
                    key, 0, 
                    em, context, 
                    output, entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        if(0 != em.linkIndex.length)
        {
            linkIndexOnWrite(stream, 
                    key, 0, 
                    em, 
                    entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        LsmdbJniUtil.push(stream.limit());
    }

    private static <T> void insertWithParent(//final DB db, final WriteOptions wSync, 
            final JniStream stream, 
            final byte[] key, 
            final EntityMetadata<T> em, final WriteContext context, 
            final byte[] parentKey, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size)
    {
        final byte[] oneToManyKey = KeyUtil.newOneToManyKey(key, parentKey);
        
        final boolean kvAbsoluteOffset = stream.isHeadPointingTo(
                entityBuffer, entityOffset);
        
        if(kvAbsoluteOffset)
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0, size);
            
            stream.$writeKVAO(oneToManyKey, 0, oneToManyKey.length, 
                    entityOffset - 4);
        }
        else
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0);
            
            stream.$writeKV(oneToManyKey, 0, oneToManyKey.length, 
                    entityBuffer, entityOffset, size);
        }
        
        stream.$writeKV(key, parentKey);
        
        if(em.indexOnFields)
        {
            indexOnInsert(stream, 
                    oneToManyKey, 9, 
                    em, context, 
                    output, entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        if(0 != em.linkIndex.length)
        {
            linkIndexOnWrite(stream, 
                    oneToManyKey, 9, 
                    em, 
                    entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        LsmdbJniUtil.push(stream.limit());
    }
    
    @Override
    protected <T, P> boolean doDeleteWithValue(final byte[] value, 
            byte[] key, EntityMetadata<T> em, byte[] parentKey,
            Op<P> nextOp, P param)
    {
        if (parentKey == null)
        {
            return delete(value,
                    this, stream, 
                    key, 
                    em, context, 
                    nextOp, param, 
                    null, null, null);
        }
        else
        {
            return deleteWithParent(KeyUtil.newOneToManyKey(key, parentKey),
                    value,
                    this, stream, 
                    key, 
                    em, context, 
                    parentKey, 
                    nextOp, param, 
                    null, null, null);
        }
    }

    @Override
    protected <T, P> boolean doDelete(byte[] key, EntityMetadata<T> em, byte[] parentKey,
            Op<P> nextOp, P param)
    {
        if (parentKey == null)
        {
            return delete(LsmdbJniUtil.get(stream, false, 0, key, null),
                    this, stream, 
                    key, 
                    em, context, 
                    nextOp, param, 
                    null, null, null);
        }
        
        final byte[] oneToManyKey = KeyUtil.newOneToManyKey(key, parentKey);
        return deleteWithParent(oneToManyKey,
                LsmdbJniUtil.get(stream, false, 0, oneToManyKey, null),
                this, stream, 
                key, 
                em, context, 
                parentKey, 
                nextOp, param, 
                null, null, null);
    }
    
    private static <T,P,C> boolean delete(final byte[] value,
            final LsmdbOpChain chain, final JniStream stream, 
            final byte[] key, final EntityMetadata<T> em, final WriteContext context, 
            final Op<P> nextOp, final P param, 
            final EntityMetadataResource cascadeResource, 
            final Op<C> cascadeOp, C cascadeParam)
    {
        if (value == null)
            return false;
        
        LsmdbJniUtil.writeHeaderDelete(stream, 0);
        
        stream.$writeKV(key, ByteString.EMPTY_BYTE_ARRAY);
        
        if(em.indexOnFields)
            indexOnDelete(stream, key, 0, em, context, value);
        
        if(cascadeResource == null && 0 != em.linkIndex.length)
            linkIndexOnDelete(stream, key, 0, em);
        
        LsmdbJniUtil.push(stream.limit());
        
        if(cascadeResource != null)
        {
            if(0 != em.linkIndex.length && 0 == em.linkIndex[0])
                context.data(em.kind, value);
            
            final ArrayList<byte[]> keysToDelete = new ArrayList<byte[]>();
            
            if(!deleteRange(chain, stream, context, 
                    keysToDelete, 
                    cascadeResource, cascadeOp, cascadeParam, 
                    null, null, null, 
                    new ArrayList<byte[]>(), 0, 
                    KeyUtil.newLinkRangeKeyStart(key), 
                    KeyUtil.newLinkRangeKeyEnd(key)))
            {
                return false;
            }
            
            // delete the keys
            LsmdbJniUtil.writeHeaderDelete(stream, 0);
            
            for(byte[] ktd : keysToDelete)
                stream.$writeKV(ktd, ByteString.EMPTY_BYTE_ARRAY);
            
            LsmdbJniUtil.push(stream.limit());
        }
        
        return nextOp == null || nextOp.handle(chain, key, 
                value, 0, value.length, param);
    }
    
    private static <T,P,C> boolean deleteWithParent(
            final byte[] oneToManyKey, final byte[] value,
            final LsmdbOpChain chain, final JniStream stream, 
            final byte[] key, final EntityMetadata<T> em, final WriteContext context, 
            final byte[] parentKey, 
            final Op<P> nextOp, final P param, 
            final EntityMetadataResource cascadeResource, 
            final Op<C> cascadeOp, C cascadeParam)
    {
        if (value == null)
            return false;
        
        LsmdbJniUtil.writeHeaderDelete(stream, 0);
        
        stream.$writeKV(key, ByteString.EMPTY_BYTE_ARRAY)
            .$writeKV(oneToManyKey, ByteString.EMPTY_BYTE_ARRAY);
        
        if(em.indexOnFields)
            indexOnDelete(stream, oneToManyKey, 9, em, context, value);
        
        if(cascadeResource == null && 0 != em.linkIndex.length)
            linkIndexOnDelete(stream, oneToManyKey, 9, em);
        
        LsmdbJniUtil.push(stream.limit());
        
        if(cascadeResource != null)
        {
            if(0 != em.linkIndex.length && 0 == em.linkIndex[0])
                context.data(em.kind, value);
            
            final ArrayList<byte[]> keysToDelete = new ArrayList<byte[]>();
            
            if(!deleteRange(chain, stream, context, 
                    keysToDelete, 
                    cascadeResource, cascadeOp, cascadeParam, 
                    null, null, null, 
                    new ArrayList<byte[]>(), 0, 
                    KeyUtil.newLinkRangeKeyStart(key), 
                    KeyUtil.newLinkRangeKeyEnd(key)))
            {
                return false;
            }
            
            // delete the keys
            LsmdbJniUtil.writeHeaderDelete(stream, 0);
            
            for(byte[] ktd : keysToDelete)
                stream.$writeKV(ktd, ByteString.EMPTY_BYTE_ARRAY);
            
            LsmdbJniUtil.push(stream.limit());
        }
        
        return nextOp == null || nextOp.handle(chain, key, 
                value, 0, value.length, param);
    }
    
    private static <C> boolean deleteRange(
            final LsmdbOpChain chain, final JniStream stream, 
            final WriteContext context, 
            final ArrayList<byte[]> keysToDelete, 
            final EntityMetadataResource cascadeResource, 
            final Op<C> cascadeOp, C cascadeParam, 
            EntityMetadata<?> emTD, byte[] keyTD, byte[] parentKeyTD, 
            final ArrayList<byte[]> cascadeKeys, int cascadeKeysStart, 
            final byte[] startCompare, final byte[] endCompare)
    {
        CascadeDelete<C> cd = new CascadeDelete<C>(keysToDelete, 
                cascadeResource, 
                cascadeOp, cascadeParam, 
                emTD, keyTD, parentKeyTD, 
                cascadeKeys, cascadeKeysStart);
        
        LsmdbJniUtil.rangeDelete(stream, false, 0, 
                false, 0, false, 
                startCompare, endCompare, 
                cd, 
                chain);
        
        if(emTD != null && cd.iterCount == 0)
        {
            // nothing was deleted
            return false;
        }
        
        // update the local var with the actual value
        cascadeKeysStart = cd.cascadeKeysStart;
        
        for(int i = 0, cascadeCount = cd.cascadeCount; i < cascadeCount; i++)
        {
            byte[] oneToManyKey = cascadeKeys.get(cascadeKeysStart++);
            
            if(!deleteRange(chain, stream, context, 
                    keysToDelete, 
                    cascadeResource, cascadeOp, cascadeParam, 
                    null, null, null, 
                    cascadeKeys, cascadeKeys.size(), 
                    KeyUtil.newLinkRangeKeyStart(oneToManyKey), 
                    KeyUtil.newLinkRangeKeyEnd(oneToManyKey)))
            {
                return false;
            }
        }
        
        return true;
    }
    
    @Override
    protected <T, P, C> boolean doCascadeDelete(byte[] key, EntityMetadata<T> em, byte[] parentKey,
            Op<P> nextOp, P param, EntityMetadataResource cascadeResource, Op<C> cascadeOp,
            C cascadeParam)
    {
        final boolean deleteOnScan = cascadeResource != null && 
                0 != em.linkIndex.length && 0 == em.linkIndex[0];
        
        if(deleteOnScan)
        {
            ArrayList<byte[]> cascadeKeys = new ArrayList<byte[]>(), 
                    keysToDelete = new ArrayList<byte[]>();
            
            if(!deleteRange(this, stream, context, 
                    keysToDelete, 
                    cascadeResource, cascadeOp, cascadeParam, 
                    em, key, parentKey, 
                    cascadeKeys, 0, 
                    KeyUtil.newLinkRangeKeyStart(key), 
                    KeyUtil.newLinkRangeKeyEnd(key)))
            {
                return false;
            }
            
            // delete the keys
            LsmdbJniUtil.writeHeaderDelete(stream, 0);
            
            for(byte[] ktd : keysToDelete)
                stream.$writeKV(ktd, ByteString.EMPTY_BYTE_ARRAY);
            
            LsmdbJniUtil.push(stream.limit());
            
            final byte[] value = cascadeKeys.get(0);
            
            return nextOp == null || nextOp.handle(this, key, 
                    value, 0, value.length, param);
        }
        
        if (parentKey == null)
        {
            return delete(
                    LsmdbJniUtil.get(stream, false, 0, key, null),
                    this, stream, 
                    key, 
                    em, context, 
                    nextOp, param, 
                    cascadeResource, cascadeOp, cascadeParam);
        }
        
        final byte[] oneToManyKey = KeyUtil.newOneToManyKey(key, parentKey);
        return deleteWithParent(oneToManyKey,
                LsmdbJniUtil.get(stream, false, 0, oneToManyKey, null),
                this, stream, 
                key, 
                em, context, 
                parentKey, 
                nextOp, param, 
                cascadeResource, cascadeOp, cascadeParam);
    }

    @Override
    protected <T, P> boolean doUpdate(byte[] value, byte[] key, EntityMetadata<T> em, CAS cas, Listener listener,
            byte[] parentKey, Op<P> nextOp, P param)
    {
        return update(this, stream, 
                value, 
                key, 
                parentKey == null ? key : KeyUtil.newOneToManyKey(key, parentKey), 
                cas, listener, em, context, 
                nextOp, param);
    }
    
    private static <T,P> boolean update(
            //final Iterator cursor, final DB db, final WriteOptions wSync, 
            final LsmdbOpChain chain, final JniStream stream, 
            final byte[] value, 
            final byte[] key, final byte[] searchKey, 
            final CAS cas, final CAS.Listener listener, 
            final EntityMetadata<T> em, final WriteContext context, 
            final Op<P> nextOp, final P param)
    {
        if(listener != null && !listener.onBeforeApply(searchKey, value, null))
            return false;
        
        final int size = context.casAndIndexCollect(value, 0, value.length, em, cas);
        final CASAndIndexCollectOutput output = context.casAndIndexCollectOutput;
        
        if (output.casCount == 0)
            return false;
        switch (em.updateFlags)
        {
            case 1:
                if (output.casCount == 1 && output.isUpdated(em.revField, cas))
                    return false;
                break;
            case 2:
                if (output.casCount == 1 && output.isUpdated(em.updateTsField, cas))
                    return false;
                break;
            case 3:
                if (output.casCount == 2 && 
                    output.isUpdated(em.revField, cas) && 
                    output.isUpdated(em.updateTsField, cas))
                {
                    return false;
                }
                break;
        }
        
        final byte[] entityBuffer = context.entityBuffer;
        final int entityOffset = context.entityOffset;
        
        if (listener != null)
        {
            // don't overwrite the serialized entity
            stream.setReservedSize(size);
            
            try
            {
                if (!listener.onApply(searchKey, value, cas, output, chain, 
                        entityBuffer, entityOffset, size))
                {
                    return false;
                }
            }
            finally
            {
                stream.setReservedSize(0);
            }
        }
        
        final boolean kvAbsoluteOffset = stream.isHeadPointingTo(
                entityBuffer, entityOffset);
        
        //final boolean noIndexOnFields = !em.indexOnFields || 
        //        (output.count == 0 && em.index.valueCount == 0);
        
        if(kvAbsoluteOffset)
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0, size);
            
            stream.$writeKVAO(searchKey, 0, searchKey.length, 
                    entityOffset - 4);
        }
        else
        {
            LsmdbJniUtil.writeHeaderPut(stream, 0);
            
            stream.$writeKV(searchKey, 0, searchKey.length, 
                    entityBuffer, entityOffset, size);
        }
        
        if(em.indexOnFields)
        {
            indexOnUpdate(stream, output, 
                    searchKey, searchKey.length == 9 ? 0 : 9, 
                    em, context, 
                    entityBuffer, entityOffset, size, 
                    value, 
                    kvAbsoluteOffset);
        }
        
        if(0 != em.linkIndex.length)
        {
            linkIndexOnWrite(stream, 
                    searchKey, searchKey.length == 9 ? 0 : 9, 
                    em, 
                    entityBuffer, entityOffset, size, 
                    kvAbsoluteOffset);
        }
        
        LsmdbJniUtil.push(stream.limit());
        
        return nextOp == null || nextOp.handle(chain, key, 
                entityBuffer, entityOffset, size, param);
    }

    static final class CascadeDelete<C> implements LsmdbJniUtil.VisitorKV<LsmdbOpChain>
    {
        
        final EntityMetadataResource cascadeResource;
        final Op<C> cascadeOp;
        final C cascadeParam;
        
        EntityMetadata<?> emTD;
        byte[] keyTD, parentKeyTD;
        
        final ArrayList<byte[]> cascadeKeys;
        int cascadeKeysStart;
        
        final ArrayList<byte[]> keysToDelete;
        
        int iterCount = 0, cascadeCount = 0, lastKind = 0;
        EntityMetadata<?> em = null;
        
        CascadeDelete(ArrayList<byte[]> keysToDelete, 
                EntityMetadataResource cascadeResource, 
                Op<C> cascadeOp, C cascadeParam, 
                EntityMetadata<?> emTD, byte[] keyTD, byte[] parentKeyTD, 
                final ArrayList<byte[]> cascadeKeys, int cascadeKeysStart)
        {
            this.keysToDelete = keysToDelete;
            
            this.cascadeResource = cascadeResource;
            this.cascadeOp = cascadeOp;
            this.cascadeParam = cascadeParam;
            
            this.emTD = emTD;
            this.keyTD = keyTD;
            this.parentKeyTD = parentKeyTD;
            
            this.cascadeKeys = cascadeKeys;
            this.cascadeKeysStart = cascadeKeysStart;
        }
    
        public boolean visit(
                //byte[] oneToManyKey, byte[] value, 
                byte[] k, int koffset, int klen, 
                byte[] v, int voffset, int vlen, 
                LsmdbOpChain chain, int index)
        {
            iterCount++;
            
            if(klen == 10)
            {
                // link index
                if(emTD != null)
                {
                    if(0 != k[koffset + 9])
                        throw DSRuntimeExceptions.runtime("Corrupt db.");
                    
                    byte[] valueTD = ValueUtil.copy(v, voffset, vlen);
                    cascadeKeys.add(valueTD);
                    cascadeKeysStart++;
                    
                    // link index (already deleted)
                    //keysToDelete.add(oneToManyKey);
                    
                    // entry or link-to-parent entry
                    keysToDelete.add(keyTD);
                    
                    int offsetTD = 0;
                    
                    if(parentKeyTD != null)
                    {
                        keyTD = KeyUtil.newOneToManyKey(keyTD, parentKeyTD);
                        offsetTD = 9;
                        
                        // actual entry
                        keysToDelete.add(keyTD);
                    }
                    
                    if(emTD.indexOnFields)
                        $indexOnDelete(keysToDelete, keyTD, offsetTD, emTD, chain.context, valueTD, 0, valueTD.length);
                    
                    if(0 != emTD.linkIndex.length && 0 == emTD.linkIndex[0])
                        chain.context.data(emTD.kind, valueTD);
                    
                    // unset
                    emTD = null;
                    return false;
                }
                
                // already deleted
                //batch.delete(oneToManyKey);
                return false;
            }
            
            int kind = KeyUtil.getKind(k, koffset, klen);
            assert kind != 0;
            
            if(kind != lastKind)
            {
                em = cascadeResource.getEntityMetadata(kind);
                if(em == null)
                {
                    throw DSRuntimeExceptions.runtime("Kind: " + kind + 
                            " not found on EntityMetadataResource.");
                }
                lastKind = kind;
            }
            
            // already deleted
            //keysToDelete.add(oneToManyKey);
            
            // TODO must encapsulate in KeyUtil
            keysToDelete.add(ValueUtil.copy(k, koffset+9, 9));
            
            if(em.indexOnFields)
                $indexOnDelete(keysToDelete, k, koffset+9, em, chain.context, v, voffset, vlen);
            
            byte[] oneToManyKey = null;
            // cascade handler
            if(cascadeOp != null && !cascadeOp.handle(chain, 
                    (oneToManyKey=ValueUtil.copy(k, koffset, klen)), 
                    v, voffset, vlen, cascadeParam))
            {
                //return false;
                // TODO optimize (not throw an error)
                // or optimize the throw to not contain stacktrace 
                throw DSRuntimeExceptions.operationFailure("cascade delete failed.");
            }
            
            if(em.hasChildren())
            {
                if(0 != em.linkIndex.length && 0 == em.linkIndex[0])
                    chain.context.data(em.kind, ValueUtil.copy(v, voffset, vlen));
                
                if(oneToManyKey == null)
                    oneToManyKey = ValueUtil.copy(k, koffset, klen);
                
                cascadeCount++;
                cascadeKeys.add(oneToManyKey);
            }
            
            return false;
        }
        
        private static <T> void $indexOnDelete( 
                final ArrayList<byte[]> keysToDelete, 
                byte[] key, int keyOffset, 
                EntityMetadata<T> em, WriteContext context, 
                byte[] v, int voffset, int vlen)
        {
            final IndexCollectOutput output = context.indexCollectOutput;
            final byte[] buf = context.udfBuffer;
            final int bufOffset = context.udfBufOffset;
            
            final int[] result = context.intset();
            
            for(int i = 0, count = context.indexCollect(
                    v, voffset, vlen, em), 
                    field = 0, oldKeySize = 0;
                    i < count;)
            {
                field = output.fieldsTracked[i++];
    
                for(int id : em.index.getIndices(field))
                {
                    if(id == 0)
                        break;
                    
                    if(0 != result[id]++)
                        continue;
                    
                    oldKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                            em, context, id, output.fieldOffsetRemoveIdx, 
                            output.varint32Idx, output.varint64Idx, 
                            key, keyOffset, 
                            v, voffset, vlen, 
                            result, true);
                    
                    //stream.$writeKV(buf, bufOffset, oldKeySize, 
                    //        ByteString.EMPTY_BYTE_ARRAY, 0, 0);
                    keysToDelete.add(ValueUtil.copy(buf, bufOffset, oldKeySize));
                    
                    final int secondaryTags = em.index.secondaryTagEntries[id];
                    if(secondaryTags != 0)
                    {
                        $deleteSecondaryTags(keysToDelete, id, secondaryTags, 
                                buf, bufOffset, oldKeySize);
                    }
                }
            }
            
            // value dependencies
            for(int i = 0, id = 0, oldKeySize = 0, count = em.index.valueCount; i < count;)
            {
                id = em.index.valueEntries[i++];
                
                if(0 != result[id]++)
                    continue;
                
                oldKeySize = IndexUtil.writeIndexKeyTo(buf, bufOffset, 
                        em, context, id, output.fieldOffsetRemoveIdx, 
                        output.varint32Idx, output.varint64Idx, 
                        key, keyOffset, 
                        v, voffset, vlen, 
                        result, true);
                
                //stream.$writeKV(buf, bufOffset, oldKeySize, 
                //        ByteString.EMPTY_BYTE_ARRAY, 0, 0);
                keysToDelete.add(ValueUtil.copy(buf, bufOffset, oldKeySize));
                
                final int secondaryTags = em.index.secondaryTagEntries[id];
                if(secondaryTags != 0)
                {
                    $deleteSecondaryTags(keysToDelete, id, secondaryTags, 
                            buf, bufOffset, oldKeySize);
                }
            }
        }
        
        private static void $deleteSecondaryTags(
                final ArrayList<byte[]> keysToDelete, 
                int id, int secondaryTags, 
                byte[] buf, int rmOffset, int rmKeySize)
        {
            if(id > 223)
            {
                // entity secondary index
                buf[++rmOffset] = 0;
                rmKeySize--;
            }
            
            for(int shift = 0; shift <= 24; shift+=8)
            {
                int tag = 0xFF & (secondaryTags >>> shift);
                if(tag == 0)
                    break;
                
                buf[rmOffset+1] = (byte)tag;
                
                //stream.$writeKV(buf, rmOffset, rmKeySize, 
                //        ByteString.EMPTY_BYTE_ARRAY, 0, 0);
                keysToDelete.add(ValueUtil.copy(buf, rmOffset, rmKeySize));
            }
        }
        
    }
}
