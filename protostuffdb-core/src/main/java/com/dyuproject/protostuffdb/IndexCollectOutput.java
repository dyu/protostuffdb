//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

//import static com.dyuproject.protostuffdb.EntityMetadata.SCALAR;

import java.io.IOException;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.EnumMapping;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;

/**
 * Collects all the fields that need to be indexed from an entity.
 *
 * @author David Yu
 * @created Mar 2, 2011
 */
public final class IndexCollectOutput implements Output, Input
{
    
    final DSByteArrayInput input;
    final int[] fieldOffsetRemoveIdx;
    
    final int[] fieldsTracked;
    int count;
    
    final int[] varint32Idx;
    final long[] varint64Idx;
    
    private int offset32;
    private int offset64;

    private EntityMetadata<?> root;
    private boolean entityField;
    
    private int start;
    
    final Pipe pipe = new Pipe()
    {
        @Override
        protected Input begin(Pipe.Schema<?> pipeSchema) throws IOException
        {
            // delegation
            return IndexCollectOutput.this;
        }
        
        @Override
        protected void end(Pipe.Schema<?> pipeSchema, Input input,
                boolean cleanupOnly) throws IOException
        {
            // reset
            output = null;
            
            IndexCollectOutput.this.clear();
        }
    };
    
    public IndexCollectOutput(
            final DSByteArrayInput input, 
            int[] fieldOffsetRemoveIdx, 
            int[] fieldsTracked,
            
            int[] varint32Idx,
            long[] varint64Idx)
    {
        this.input = input;
        this.fieldOffsetRemoveIdx = fieldOffsetRemoveIdx;
        this.fieldsTracked = fieldsTracked;
        
        this.varint32Idx = varint32Idx;
        this.varint64Idx = varint64Idx;
    }
    
    public IndexCollectOutput init(EntityMetadata<?> root, 
            byte[] data, int offset, int len)
    {
        this.root = root;
        
        count = 0;
        
        offset32 = 1;
        offset64 = 0;
        
        entityField = true;
        
        start = offset;
        
        input.reset(data, offset, len);
        
        return this;
    }
    
    public IndexCollectOutput clear()
    {
        // unset for gc
        root = null;
        
        return this;
    }
    
    public boolean isEnumsByName()
    {
        return false;
    }
    
    private void indexVarInt32(int fieldNumber, int value)
    {
        // this field is needed by an index
        fieldOffsetRemoveIdx[fieldNumber] = -offset32;
        
        // indicates that this is an int
        varint32Idx[offset32++] = -1;
        
        // the tag offset
        varint32Idx[offset32++] = input.previousOffset - start;
        
        // the value stored
        varint32Idx[offset32++] = value;
        
        fieldsTracked[count++] = fieldNumber;
    }
    
    private void indexVarInt64(int fieldNumber, long value)
    {
        // this field is needed by an index
        fieldOffsetRemoveIdx[fieldNumber] = -offset32;
        
        // pointer to the value
        varint32Idx[offset32++] = offset64;
        
        // the tag offset
        varint32Idx[offset32++] = input.previousOffset - start;

        // the value
        varint64Idx[offset64++] = value;
        
        fieldsTracked[count++] = fieldNumber;
    }

    public void writeInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            if(root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
                
                fieldsTracked[count++] = fieldNumber;
                
                // trust the dev
                input.offset++;
            }
            else
            {
                indexVarInt32(fieldNumber, input.readInt32());
            }
            return;
        }
        
        input.skipRawVarint();
    }
    
    public void writeUInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            if(root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
                
                fieldsTracked[count++] = fieldNumber;
                
                // trust the dev
                input.offset++;
            }
            else
            {
                indexVarInt32(fieldNumber, input.readUInt32());
            }
            return;
        }

        input.skipRawVarint();
    }
    
    public void writeSInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt32(fieldNumber, input.readSInt32());
        else
            input.skipRawVarint();
    }
    
    public void writeFixed32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        input.offset += 4;
    }
    
    public void writeSFixed32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        input.offset += 4;
    }

    public void writeInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt64(fieldNumber, input.readInt64());
        else
            input.skipRawVarint();
    }
    
    public void writeUInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt64(fieldNumber, input.readUInt64());
        else
            input.skipRawVarint();
    }
    
    public void writeSInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt64(fieldNumber, input.readSInt64());
        else
            input.skipRawVarint();
    }
    
    public void writeFixed64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        input.offset += 8;
    }
    
    public void writeSFixed64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        input.offset += 8;
    }

    public void writeFloat(int fieldNumber, float value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        input.offset += 4;
    }

    public void writeDouble(int fieldNumber, double value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        input.offset += 8;
    }

    public void writeBool(int fieldNumber, boolean value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
    }

    public void writeEnum(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            if(root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
                
                fieldsTracked[count++] = fieldNumber;
                
                // trust the dev
                input.offset++;
            }
            else
            {
                indexVarInt32(fieldNumber, input.readEnum());
            }
            return;
        }
        
        input.skipRawVarint();
    }
    
    public void writeEnumFromIdx(int fieldNumber, int idx, EnumMapping mapping,
            boolean repeated) throws IOException
    {
        writeEnum(fieldNumber, mapping.numbers[idx], repeated);
    }

    public void writeString(int fieldNumber, String value, boolean repeated) throws IOException
    {
        throw new IllegalStateException("Should not happen (we're using protostuff pipes).");
    }

    public void writeBytes(int fieldNumber, ByteString value, boolean repeated) throws IOException
    {
        throw new IllegalStateException("Should not happen (we're using protostuff pipes).");
    }
    
    public void writeByteArray(int fieldNumber, byte[] bytes, boolean repeated) throws IOException
    { 
        throw new IllegalStateException("Should not happen (we're using protostuff pipes).");
    }
    
    public void writeByteRange(boolean utf8String, int fieldNumber, byte[] value, 
            int offset, int length, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset - start;
            
            fieldsTracked[count++] = fieldNumber;
        }
    }
    
    public <T> void writeObject(final int fieldNumber, final T value, final Schema<T> schema, 
            final boolean repeated) throws IOException
    {
        final boolean lastEntityField = entityField;
        
        entityField = false;
        
        schema.writeTo(this, value);
        
        entityField = lastEntityField;
    }
    
    // lazy deserialization

    public <T> void handleUnknownField(int fieldNumber, Schema<T> schema) throws IOException
    {
        input.handleUnknownField(fieldNumber, schema);
    }

    public <T> int readFieldNumber(Schema<T> schema) throws IOException
    {
        return input.readFieldNumber(schema);
    }

    public int readInt32() throws IOException
    {
        return 0;
    }

    public int readUInt32() throws IOException
    {
        return 0;
    }

    public int readSInt32() throws IOException
    {
        return 0;
    }

    public int readFixed32() throws IOException
    {
        return 0;
    }

    public int readSFixed32() throws IOException
    {
        return 0;
    }

    public long readInt64() throws IOException
    {
        return 0;
    }

    public long readUInt64() throws IOException
    {
        return 0;
    }

    public long readSInt64() throws IOException
    {
        return 0;
    }

    public long readFixed64() throws IOException
    {
        return 0;
    }

    public long readSFixed64() throws IOException
    {
        return 0;
    }

    public float readFloat() throws IOException
    {
        return 0;
    }

    public double readDouble() throws IOException
    {
        return 0;
    }

    public boolean readBool() throws IOException
    {
        return input.readBool();
    }

    public int readEnum() throws IOException
    {
        return 0;
    }
    
    public int readEnumIdx(EnumMapping mapping) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public String readString() throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public ByteString readBytes() throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public byte[] readByteArray() throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public <T> T mergeObject(T value, Schema<T> schema) throws IOException
    {
        return input.mergeObject(value, schema);
    }

    public void transferByteRangeTo(Output output, boolean utf8String, int fieldNumber,
            boolean repeated) throws IOException
    {
        input.transferByteRangeTo(output, utf8String, fieldNumber, repeated);
    }
    
    public void transferEnumTo(Output output, EnumMapping mapping, int fieldNumber,
            boolean repeated) throws IOException
    {
        output.writeEnum(fieldNumber, readEnum(), repeated);
    }
}
