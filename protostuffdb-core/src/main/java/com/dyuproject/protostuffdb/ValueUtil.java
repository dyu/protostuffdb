//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.EntityMetadata.ZERO_KEY;

import java.io.UnsupportedEncodingException;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.IntSerializer;

/**
 * Utility for field values.
 * 
 * TODO: optimize toBytes (use an existing buffer)
 *
 * @author David Yu
 * @created Mar 18, 2011
 */
public final class ValueUtil
{
    
    public static byte[] copy(byte[] data)
    {
        return copy(data, 0, data.length);
    }
    
    public static byte[] copy(byte[] data, int offset, int len)
    {
        byte[] copy = new byte[len];
        System.arraycopy(data, offset, copy, 0, len);
        return copy;
    }
    
    public static byte[] concat(byte[] b1, byte[] b2)
    {
        byte[] data = new byte[b1.length + b2.length];
        System.arraycopy(b1, 0, data, 0, b1.length);
        System.arraycopy(b2, 0, data, b1.length, b2.length);
        return data;
    }
    
    public static byte[] concat(byte[] b1, int b1offset, int b1len, 
            byte[] b2, int b2offset, int b2len)
    {
        byte[] data = new byte[b1len + b2len];
        System.arraycopy(b1, b1offset, data, 0, b1len);
        System.arraycopy(b2, b2offset, data, b1len, b2len);
        return data;
    }
    
    public static boolean isZeroKey(byte[] buf)
    {
        return isEqual(ZERO_KEY, buf);
    }
    
    public static boolean isZeroKey(byte[] buf, int offset, int len)
    {
        return isEqual(ZERO_KEY, buf, offset, len);
    }
    
    public static boolean isEqual(byte[] buf, int offset, int length, String value)
    {
        char c, lowSurrogate;
        int i = 0, len = value.length(), codePoint;
        final int limit = offset + length;
        while (i < len)
        {
            c = value.charAt(i++);
            if (c < 0x0080)
            {
                // ascii
                if (offset == limit
                        || buf[offset++] != (byte)c)
                {
                    return false;
                }
            }
            else if (c < 0x0800)
            {
                if (offset+2 > limit
                        || buf[offset++] != (byte) (0xC0 | ((c >>  6) & 0x1F))
                        || buf[offset++] != (byte) (0x80 | ((c >>  0) & 0x3F)))
                {
                    return false;
                }
            }
            else if (i != len &&
                    Character.isHighSurrogate(c) &&
                    Character.isLowSurrogate((lowSurrogate = value.charAt(i))))
            {
                codePoint = Character.toCodePoint(c, lowSurrogate);
                if (offset + 4 > limit ||
                        buf[offset++] != (byte) (0xF0 | ((codePoint >> 18) & 0x07)) ||
                        buf[offset++] != (byte) (0x80 | ((codePoint >> 12) & 0x3F)) ||
                        buf[offset++] != (byte) (0x80 | ((codePoint >> 6) & 0x3F)) ||
                        buf[offset++] != (byte) ((codePoint >> 0) & 0x3F))
                {
                    return false;
                }
                i++;
            }
            else
            {
                if (offset + 3 > limit
                        || buf[offset++] != (byte) (0xE0 | ((c >> 12) & 0x0F))
                        || buf[offset++] != (byte) (0x80 | ((c >>  6) & 0x3F))
                        || buf[offset++] != (byte) (0x80 | ((c >>  0) & 0x3F)))
                {
                    return false;
                }
            }
        }
        
        return offset == limit;
    }
    
    public static boolean isEqual(byte[] buf, String value)
    {
        return isEqual(buf, 0, buf.length, value);
    }
    
    public static int compare(byte[] buf, int offset, int length, String value)
    {
        byte b;
        char c, lowSurrogate;
        int i = 0, len = value.length(), codePoint;
        final int limit = offset + length;
        while (i < len)
        {
            if (offset == limit)
                return -1;

            c = value.charAt(i++);
            if(c < 0x0080)
            {
                // ascii
                if (buf[offset++] != (b = (byte)c))
                    return (buf[offset-1] & 0xFF) - (0xFF & b);
            }
            else if(c < 0x0800)
            {
                b = (byte) (0xC0 | ((c >>  6) & 0x1F));
                if(buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);
                
                if(offset == limit)
                    return -1;
                b = (byte) (0x80 | ((c >>  0) & 0x3F));
                if(buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);
            }
            else if (i != len &&
                    Character.isHighSurrogate(c) &&
                    Character.isLowSurrogate((lowSurrogate = value.charAt(i))))
            {
                codePoint = Character.toCodePoint(c, lowSurrogate);

                b = (byte) (0xF0 | ((codePoint >> 18) & 0x07));
                if (buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);

                if (offset == limit)
                    return -1;
                b = (byte) (0x80 | ((codePoint >> 12) & 0x3F));
                if (buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);

                if (offset == limit)
                    return -1;
                b = (byte) (0x80 | ((codePoint >> 6) & 0x3F));
                if (buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);

                if (offset == limit)
                    return -1;
                b = (byte) ((codePoint >> 0) & 0x3F);
                if (buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);

                i++;
            }
            else
            {
                b = (byte) (0xE0 | ((c >> 12) & 0x0F));
                if(buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);
                
                if(offset == limit)
                    return -1;
                b = (byte) (0x80 | ((c >>  6) & 0x3F));
                if(buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);
                
                if(offset == limit)
                    return -1;
                b = (byte) (0x80 | ((c >>  0) & 0x3F));
                if(buf[offset++] != b)
                    return (buf[offset-1] & 0xFF) - (0xFF & b);
            }
        }
        
        return offset == limit ? 0 : 1;
    }
    
    public static boolean isEqual(byte[] data1, byte[] data2)
    {
        if(data2.length != data1.length)
            return false;
        
        for(int i = 0, len2 = data2.length; i < len2; i++)
            if(data1[i] != data2[i])
                return false;
        
        return true;
    }
    
    public static boolean isEqual(final byte[] data1, int offset1, final int len1, 
            final byte[] data2, int offset2, final int len2)
    {
        if(len2 != len1)
            return false;
        
        for(int i = 0; i < len2; i++)
            if(data1[offset1++] != data2[offset2++])
                return false;
        
        return true;
    }
    
    public static boolean isEqual(final byte[] data1, 
            final byte[] data2, int offset2, final int len2)
    {
        if(len2 != data1.length)
            return false;
        
        for(int i = 0; i < len2;)
            if(data1[i++] != data2[offset2++])
                return false;
        
        return true;
    }
    
    public static int compare(final byte[] data1, int offset1, final int len1, 
            final byte[] data2, int offset2, final int len2)
    {
        for (int i = 0, len = Math.min(len1, len2); i < len; i++)
        {
            byte b1 = data1[offset1++];
            byte b2 = data2[offset2++];
            if (b1 == b2)
                continue;
            
            return (b1 & 0xff) - (b2 & 0xff);
        }

        return (len1 - len2);
    }
    
    public static int compare(byte[] v1, byte[] v2)
    {
        return compare(v1, 0, v1.length, v2, 0, v2.length);
    }
    
    public static int compare(String v1, String v2)
    {
        return v1.compareTo(v2);
    }
    
    // TODO use local toBytes
    public static byte[] toBytes(String value)
    {
        try
        {
            return value.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static byte[] toBytes(ByteString value)
    {
        // TODO avoid double copy
        return value.toByteArray();
    }
    
    public static byte[] toBytes(boolean value)
    {
        return new byte[]{value ? (byte)1 : (byte)0};
    }
    
    public static byte[] toBytes(int value)
    {
        byte[] buffer = new byte[4];
        IntSerializer.writeInt32(value, buffer, 0);
        return buffer;
    }
    
    public static byte[] toBytes(long value)
    {
        byte[] buffer = new byte[8];
        IntSerializer.writeInt64(value, buffer, 0);
        return buffer;
    }
    
    public static byte[] toBytes(float value)
    {
        return toBytes(Float.floatToRawIntBits(value));
    }
    
    public static byte[] toBytes(double value)
    {
        return toBytes(Double.doubleToRawLongBits(value));
    }
    
    public static int writeInt32(int value, byte[] buf, int offset)
    {
        IntSerializer.writeInt32(value, buf, offset);
        return 4;
    }
    
    public static int writeInt64(long value, byte[] buf, int offset)
    {
        IntSerializer.writeInt64(value, buf, offset);
        return 8;
    }
    
    public static int writeFloat(float value, byte[] buf, int offset)
    {
        return writeInt32(Float.floatToRawIntBits(value), buf, offset);
    }
    
    public static int writeDouble(double value, byte[] buf, int offset)
    {
        return writeInt64(Double.doubleToRawLongBits(value), buf, offset);
    }
    
    public static int writeDatetime(long value, byte[] buf, int offset)
    {
        KeyUtil.writeTimestamp(value, buf, offset);
        return 6;
    }
    
    public static int writeDatetimeFrom(byte[] value, int fvoffset, 
            byte[] buf, int offset)
    {
        buf[offset++] = value[fvoffset+5];
        buf[offset++] = value[fvoffset+4];
        buf[offset++] = value[fvoffset+3];
        buf[offset++] = value[fvoffset+2];
        buf[offset++] = value[fvoffset+1];
        buf[offset] = value[fvoffset];
        
        return 6;
    }
    
    public static int toInt16(byte[] buffer, int offset)
    {
        final byte b1 = buffer[offset++];
        final byte b2 = buffer[offset];
        
        return (((int)b2 & 0xff)    ) |
             (((int)b1 & 0xff) <<  8);
    }
    
    public static int toInt32(byte[] buffer, int offset)
    {
        final byte b1 = buffer[offset++];
        final byte b2 = buffer[offset++];
        final byte b3 = buffer[offset++];
        final byte b4 = buffer[offset];
        
        return (((int)b4 & 0xff)    ) |
             (((int)b3 & 0xff) <<  8) |
             (((int)b2 & 0xff) << 16) |
             (((int)b1 & 0xff) << 24);
    }
    
    public static long toInt64(byte[] buffer, int offset)
    {
        final byte b1 = buffer[offset++];
        final byte b2 = buffer[offset++];
        final byte b3 = buffer[offset++];
        final byte b4 = buffer[offset++];
        final byte b5 = buffer[offset++];
        final byte b6 = buffer[offset++];
        final byte b7 = buffer[offset++];
        final byte b8 = buffer[offset];
        
        return (((long)b8 & 0xff)    ) |
             (((long)b7 & 0xff) <<  8) |
             (((long)b6 & 0xff) << 16) |
             (((long)b5 & 0xff) << 24) |
             (((long)b4 & 0xff) << 32) |
             (((long)b3 & 0xff) << 40) |
             (((long)b2 & 0xff) << 48) |
             (((long)b1 & 0xff) << 56);
    }
    
    public static int toInt16LE(byte[] buffer, int offset)
    {
        final byte b1 = buffer[offset++];
        final byte b2 = buffer[offset];
        
        return (((int)b1 & 0xff)    ) |
             (((int)b2 & 0xff) <<  8);
    }
    
    public static int toInt32LE(byte[] buffer, int offset)
    {
        final byte b1 = buffer[offset++];
        final byte b2 = buffer[offset++];
        final byte b3 = buffer[offset++];
        final byte b4 = buffer[offset];
        
        return (((int)b1 & 0xff)    ) |
             (((int)b2 & 0xff) <<  8) |
             (((int)b3 & 0xff) << 16) |
             (((int)b4 & 0xff) << 24);
    }
    
    public static long toInt64LE(byte[] buffer, int offset)
    {
        final byte b1 = buffer[offset++];
        final byte b2 = buffer[offset++];
        final byte b3 = buffer[offset++];
        final byte b4 = buffer[offset++];
        final byte b5 = buffer[offset++];
        final byte b6 = buffer[offset++];
        final byte b7 = buffer[offset++];
        final byte b8 = buffer[offset];
        
        return (((long)b1 & 0xff)    ) |
             (((long)b2 & 0xff) <<  8) |
             (((long)b3 & 0xff) << 16) |
             (((long)b4 & 0xff) << 24) |
             (((long)b5 & 0xff) << 32) |
             (((long)b6 & 0xff) << 40) |
             (((long)b7 & 0xff) << 48) |
             (((long)b8 & 0xff) << 56);
    }
    
    /**
     * Reads the buffer in big-endian.
     */
    public static float toFloat(byte[] buffer, int offset)
    {
        return Float.intBitsToFloat(toInt32(buffer, offset));
    }
    
    /**
     * Reads the buffer in big-endian.
     */
    public static double toDouble(byte[] buffer, int offset)
    {
        return Double.longBitsToDouble(toInt64(buffer, offset));
    }
    
    /**
     * Reads a var int from the buffer.
     * A runtime exception is thrown if the content contains a malformed var int.
     */
    public static int readVarInt32(byte[] buffer, int offset)
    {
        byte tmp = buffer[offset++];
        if (tmp >= 0)
        {
            return tmp;
        }
        int result = tmp & 0x7f;
        if ((tmp = buffer[offset++]) >= 0)
        {
            result |= tmp << 7;
        }
        else
        {
            result |= (tmp & 0x7f) << 7;
            if ((tmp = buffer[offset++]) >= 0)
            {
                result |= tmp << 14;
            }
            else
            {
                result |= (tmp & 0x7f) << 14;
                if ((tmp = buffer[offset++]) >= 0)
                {
                    result |= tmp << 21;
                }
                else
                {
                    result |= (tmp & 0x7f) << 21;
                    result |= (tmp = buffer[offset++]) << 28;
                    if (tmp < 0)
                    {
                        // Discard upper 32 bits.
                        for (int i = 0; i < 5; i++)
                        {
                            if (buffer[offset++] >= 0)
                            {
                                return result;
                            }
                        }
                        throw DSRuntimeExceptions.runtime("Invalid var int.");
                    }
                }
            }
        }
        return result;
    }
}
