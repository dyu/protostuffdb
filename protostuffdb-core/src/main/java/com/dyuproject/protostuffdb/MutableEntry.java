//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Map;

/**
 * Represents a mutable key-value pair that implements {@link Map.Entry}.
 *
 * @author David Yu
 * @created Feb 25, 2011
 */
public class MutableEntry<K,V> implements Map.Entry<K,V>
{
    
    public K key;
    public V value;
    
    public MutableEntry(K key, V value)
    {
        this.key = key;
        this.value = value;
    }

    public K getKey()
    {
        return key;
    }

    public V getValue()
    {
        return value;
    }

    public V setValue(V value)
    {
        final V oldValue = this.value;
        this.value = value;
        return oldValue;
    }

}
