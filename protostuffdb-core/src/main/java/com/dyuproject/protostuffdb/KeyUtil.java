//========================================================================
//Copyright 2007-2010 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Arrays;

import com.dyuproject.protostuff.IntSerializer;

/**
 * Utilities for internal and external keys.
 * The external keys are the base64 encoded representaton of the internal keys.
 *
 * @author David Yu
 * @created Dec 31, 2010
 */
public final class KeyUtil
{
    
    // Prefixes (MS = Most Significant)
    
    //1 Entity kind (1-127)
    //  0(MSBit)
    
    //2 Entity parent-child link
    //  1(MSBit)
    
    //3 Tag Index
    //  0(MSByte) - tagKind(1-223)
    
    //4 Index
    //  0(MSByte) - indexId(224-255) - kind
    
    //5 custom keys are prefixed with 0x80.
    
    // --------------------------------------------------------------
    
    // The physical order will be:
    // 0     5     key(9) tagname(n) tagval(n) <-3 a tag-index(5) associated to a key
    
    // 0     224   1      val(n)     key(9)    <-4 a kind(1) with an index id(224)
    //---------entity keys---------
    // 1(9)                                    <-1 a kind(1)
    // 128    foo                              <-5 a regular key "foo"
    // 129(9) 7(9)                             <-2 a kind(7) whose parent is kind(1)
    
    // 7(9) will have a value of 1(9) with the actual value stored in 129(9) 7(9) *above*
    
    // (n) means the variable length of the type
    // key(9) means its an entity key
    
    private KeyUtil() {}
    
    /**
     * Extracts the key from the index/tag key.
     */
    public static byte[] extractFrom(byte[] keyOrTagKey)
    {
        return extractFrom(keyOrTagKey, new byte[9]);
    }
    
    /**
     * Extracts the key from the index/tag key and copies to dest.
     */
    public static byte[] extractFrom(byte[] keyOrTagKey, byte[] dest)
    {
        final int offset = keyOrTagKey.length - 9;
        if(offset < 0)
            throw DSRuntimeExceptions.invalidArg("Invalid key."); 
        
        System.arraycopy(keyOrTagKey, offset, dest, 0, 9);
        return dest;
    }
    
    /**
     * Extracts the timestamp from the index/tag key.
     */
    public static long extractTsFrom(byte[] keyOrTagKey)
    {
        return readTimestamp(keyOrTagKey, keyOrTagKey.length - 8);
    }
    
    /**
     * Appends the {@code key} to the {@code indexKey} (does not create a new copy).
     */
    public static void ncAppendTo(byte[] indexKey, byte[] key)
    {
        System.arraycopy(key, 0, indexKey, indexKey.length - key.length, key.length);
    }
    
    public static byte[] append0XFF(byte[] key)
    {
        return append0XFF(key, 0, key.length);
    }
    
    public static byte[] append0XFF(byte[] key, int offset, int len)
    {
        byte[] endKey = new byte[len+1];
        
        System.arraycopy(key, offset, endKey, 0, len);
        
        endKey[len] = (byte)0xFF;
        
        return endKey;
    }
    
    static byte[] newPrefixedKey(String key, WriteContext context)
    {
        if(key.length() == 0)
            throw DSRuntimeExceptions.invalidArg("Invalid key.");
        
        try
        {
            return context.kb.$append((byte)0x80).$append(key).$push().copy();
        }
        finally
        {
            context.kb.popLen();
        }
    }
    
    static byte[] newPrefixedKey(byte[] key)
    {
        if(key.length == 0)
            throw DSRuntimeExceptions.invalidArg("Invalid key.");
        
        byte[] prefixed = new byte[key.length + 1];
        prefixed[0] = (byte)0x80;
        System.arraycopy(key, 0, prefixed, 1, key.length);
        return prefixed;
    }
    
    static byte[] newPrefixedKeyEnd(byte[] key)
    {
        if(key.length == 0)
            throw DSRuntimeExceptions.invalidArg("Invalid key.");
        
        byte[] prefixed = new byte[key.length + 3];
        prefixed[0] = (byte)0x80;
        System.arraycopy(key, 0, prefixed, 1, key.length);
        prefixed[prefixed.length-1] = (byte)0xFF;
        prefixed[prefixed.length-2] = (byte)0xFF;
        return prefixed;
    }
    
    public static byte[] newEntityRangeKeyStart(int kind)
    {
        return new byte[]{(byte)kind};
    }
    
    public static byte[] newEntityRangeKeyEnd(int kind)
    {
        return new byte[]{(byte)kind, (byte)0xff};
    }
    
    static byte[] newLinkRangeKeyStart(byte[] parentKey)
    {
        return newLinkRangeKeyStart(parentKey, parentKey.length == 9 ? 0 : 9);
    }
    
    static byte[] newLinkRangeKeyStart(byte[] parentKey, int pkoffset)
    {
        final byte[] k = new byte[10];
        
        k[0] = (byte)(0x80 | parentKey[pkoffset]);
        System.arraycopy(parentKey, pkoffset+1, k, 1, 8);
        k[9] = 0; // link index included
        
        return k;
    }
    
    static byte[] newLinkRangeKeyEnd(byte[] parentKey)
    {
        return newLinkRangeKeyEnd(parentKey, parentKey.length == 9 ? 0 : 9);
    }
    
    static byte[] newLinkRangeKeyEnd(byte[] parentKey, int pkoffset)
    {
        final byte[] k = new byte[10];
        
        k[0] = (byte)(0x80 | parentKey[pkoffset]);
        System.arraycopy(parentKey, pkoffset+1, k, 1, 8);
        k[9] = (byte)0xff;
        
        return k;
    }
    
    static byte[] newLinkRangeKeyStart(int kind, byte[] parentKey)
    {
        return newLinkRangeKeyStart(kind, parentKey, 0);
    }
    
    static byte[] newLinkRangeKeyStart(int kind, byte[] parentKey, int pkoffset)
    {
        final byte[] k = new byte[11];
        
        k[0] = (byte)(0x80 | parentKey[pkoffset]);
        System.arraycopy(parentKey, pkoffset+1, k, 1, 8);
        k[9] = (byte)kind;
        k[10] = 0; // link index excluded
        
        return k;
    }
    
    static byte[] newLinkRangeKeyEnd(int kind, byte[] parentKey)
    {
        return newLinkRangeKeyEnd(kind, parentKey, 0);
    }
    
    static byte[] newLinkRangeKeyEnd(int kind, byte[] parentKey, int pkoffset)
    {
        final byte[] k = new byte[11];
        
        k[0] = (byte)(0x80 | parentKey[pkoffset]);
        System.arraycopy(parentKey, pkoffset+1, k, 1, 8);
        k[9] = (byte)kind;
        k[10] = (byte)0xff;
        
        return k;
    }
    
    static byte[] newLinkIndexKey(int kind, byte[] parentKey, byte[] startKey)
    {
        return newLinkIndexKey(kind, parentKey, 0, startKey, 0);
    }
    
    static byte[] newLinkIndexKey(int kind, byte[] parentKey, int pkoffset, 
            byte[] startKey, int skoffset)
    {
        final byte[] ik = new byte[18];
        
        ik[0] = (byte)(0x80 | parentKey[pkoffset]);
        System.arraycopy(parentKey, pkoffset+1, ik, 1, 8);
        System.arraycopy(startKey, skoffset, ik, 9, 9);
        
        return ik;
    }
    
    public static byte[] newParentLinkKey(int childKind, byte[] key)
    {
        return newParentLinkKey(childKind, key, 0);
    }
    
    public static byte[] newParentLinkKey(int childKind, byte[] key, int keyOffset)
    {
        final byte[] linkKey = new byte[10];
        linkKey[0] = (byte)(0x80 | key[keyOffset]);
        System.arraycopy(key, keyOffset+1, linkKey, 1, 8);
        linkKey[9] = (byte)childKind;
        
        return linkKey;
    }
    
    public static byte[] newTagIndexRangeKeyStart(int tagKind)
    {
        return new byte[]{0, (byte)tagKind};
    }
    
    public static byte[] newTagIndexRangeKeyEnd(int tagKind)
    {
        return new byte[]{0, (byte)tagKind, (byte)0xFF};
    }
    
    static byte[] newTagIndexRangeKeyStart(int tagKind, byte[] key)
    {        
        return newTagIndexRangeKeyStart(tagKind, key, 0);
    }
    
    static byte[] newTagIndexRangeKeyStart(int tagKind, byte[] key, int koffset)
    {        
        final byte[] startCompare = new byte[1 // 0
                                             + 1 // tagKind 
                                             + 9]; // key
            
        startCompare[0] = (byte)0;
        startCompare[1] = (byte)tagKind;
        System.arraycopy(key, koffset, startCompare, 2, 9);
       
        return startCompare;
    }
    
    static byte[] newTagIndexRangeKeyEnd(int tagKind, byte[] key) 
            
    {
        return newTagIndexRangeKeyEnd(tagKind, key, 0);
    }
    
    static byte[] newTagIndexRangeKeyEnd(int tagKind, byte[] key, int koffset) 
    
    {
        final byte[] endCompare = new byte[1 // 0
                                             + 1 // tagKind
                                             + 9 // key
                                             + 1]; // 1 or 0xFF
            
        endCompare[0] = (byte)0;
        endCompare[1] = (byte)tagKind;
        System.arraycopy(key, koffset, endCompare, 2, 9);
        endCompare[endCompare.length-1] = (byte)0xFF;
        
        return endCompare;
    }
    
    public static byte[] newTagIndexKey(int tagKind, byte[] key, 
            byte[] tagName)
    {
        return newTagIndexKey(tagKind, key, 0, tagName, 0, tagName.length);
    }
    
    public static byte[] newTagIndexKey(int tagKind, byte[] key, int offset, 
            byte[] tagName, int toffset, int tlen)
    {
        final byte[] indexKey = new byte[2 // 0 and tagKind
                                         + 9
                                         + tlen];
        
        indexKey[0] = (byte)0;
        indexKey[1] = (byte)tagKind;
        System.arraycopy(key, offset, indexKey, 2, 9);
        
        // allow empty tagName for searching simulated (by date) keys
        if(tlen != 0)
            System.arraycopy(tagName, toffset, indexKey, 9+2, tlen);
        
        return indexKey;
    }
    
    public static byte[] valueFromTagIndexKey(byte[] indexKey)
    {
        int length = indexKey.length - 9 - 2;
        byte[] data = new byte[length];
        System.arraycopy(indexKey, indexKey.length-length, data, 0, length);
        return data;
    }
    
    public static int valueLengthFromTagIndexKey(byte[] indexKey)
    {
        return indexKey.length - 9 - 2;
    }
    
    public static int valueFromTagIndexKeyAsInt32(byte[] indexKey)
    {
        // TODO matching testcase
        int length = indexKey.length - 9 - 2;
        if(length != 4)
            throw DSRuntimeExceptions.invalidArg("Expect length of int is 4, not " + length);
        
        return ValueUtil.toInt32(indexKey, indexKey.length-length);
    }
    
    public static byte[] keyFromTagIndexKey(byte[] indexKey)
    {
        byte[] data = new byte[9];
        System.arraycopy(indexKey, 2, data, 0, 9);
        return data;
    }
    
    public static int keyOffsetFromTagIndexKey(byte[] indexKey)
    {
        return 2;
    }
    
    static byte[] newIndexRangeKeyStart(int indexId, EntityMetadata<?> em, 
            byte[] value, boolean prefixMatch)
    {
        return newIndexRangeKeyStart(indexId, em, value, 0, value.length, prefixMatch);
    }
    
    static byte[] newIndexRangeKeyStart(int indexId, EntityMetadata<?> em, 
            byte[] value, int offset, int len, 
            boolean prefixMatch)
    {
        // 0 indexId kind $value null?
        int length = 3 + len;
        
        if(!prefixMatch)
            length++; // space fo the null byte
        
        final byte[] startCompare = new byte[length];
            
        startCompare[0] = (byte)0;
        startCompare[1] = (byte)indexId;
        startCompare[2] = (byte)em.kind;
        System.arraycopy(value, offset, startCompare, 3, len);
       
        return startCompare;
    }
    
    static byte[] newIndexRangeKeyEnd(int indexId, EntityMetadata<?> em, 
            byte[] value, boolean prefixMatch)
    {
        return newIndexRangeKeyEnd(indexId, em, value, 0, value.length, prefixMatch);
    }
    
    static byte[] newIndexRangeKeyEnd(int indexId, EntityMetadata<?> em, 
            byte[] value, int offset, int len, 
            boolean prefixMatch)
    {
        // 0 indexId kind $value null
        final byte[] endCompare = new byte[4 + len];
            
        endCompare[0] = (byte)0;
        endCompare[1] = (byte)indexId;
        endCompare[2] = (byte)em.kind;
        System.arraycopy(value, offset, endCompare, 3, len);
        endCompare[endCompare.length-1] = prefixMatch ? (byte)0xFF : (byte)1;
        
        return endCompare;
    }
    
    static byte[] newIndexKey(int indexId, EntityMetadata<?> em, 
            byte[] value, byte[] key)
    {
        return newIndexKey(indexId, em, value, 0, value.length, key, 0);
    }
    
    static byte[] newIndexKey(int indexId, EntityMetadata<?> em, 
            byte[] value, int offset, int len, 
            byte[] key, int koffset)
    {
        // 0 indexId kind $value null $key
        final byte[] indexKey = new byte[4 + len + 9];
        
        indexKey[0] = 0;
        indexKey[1] = (byte)indexId;
        indexKey[2] = (byte)em.kind;
        System.arraycopy(value, offset, indexKey, 3, len);
        System.arraycopy(key, koffset, indexKey, indexKey.length-9, 9);
        
        return indexKey;
    }
    
    public static byte[] valueFromIndexKey(byte[] indexKey)
    {
        // length - $key(9) - 1(0) - 1(indexId) - 1(kind) - 1(null)
        final int length = indexKey.length - 13;
        byte[] data = new byte[length];
        // 0 indexId kind $value(3rd index)
        System.arraycopy(indexKey, 3, data, 0, length);
        return data;
    }
    
    public static int valueFromIndexKeyAsInt32(byte[] indexKey)
    {
        // TODO matching testcase
        final int length = indexKey.length - 13;
        if(length != 4)
            throw DSRuntimeExceptions.invalidArg("Expect length of int is 4, not " + length);
        
        return ValueUtil.toInt32(indexKey, 3);
    }
    
    public static byte[] keyFromIndexKey(byte[] indexKey)
    {
        byte[] data = new byte[9];
        System.arraycopy(indexKey, indexKey.length-9, data, 0, 9);
        return data;
    }
    
    public static int keyOffsetFromIndexKey(byte[] indexKey)
    {
        return indexKey.length - 9;
    }
    
    /*static int writeIndexRangeKeyStart(int indexId, EntityMetadata<?> em, 
            byte[] value, 
            boolean prefixMatch, byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        System.arraycopy(value, 0, buf, offset, value.length);
        offset += value.length;

        if(!prefixMatch)
            buf[offset++] = 0; // null byte
        
        return offset - start;
    }
    
    static int writeIndexRangeKeyEnd(int indexId, EntityMetadata<?> em, 
            byte[] value, 
            boolean prefixMatch, byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        System.arraycopy(value, 0, buf, offset, value.length);
        offset += value.length;

        buf[offset++] = prefixMatch ? (byte)0xFF : (byte)1;
        
        return offset - start;
    }*/
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            byte[] value, byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        return writeIndexKey(indexId, em, false, value, 0, value.length, key, koffset, 
                buf, offset);
    }
    
    /*static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            byte[] value, int voffset, int vlen, 
            byte[] key, int koffset, int klen, 
            byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        
        System.arraycopy(value, voffset, buf, offset, vlen);
        offset += vlen;
        
        buf[offset++] = 0; // null byte
        
        System.arraycopy(key, koffset, buf, offset, klen);
        offset += klen;
        
        return offset - start;
    }*/
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            boolean utf8String, 
            byte[] value, int voffset, int vlen, 
            byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        
        if(utf8String)
        {
            for(int i = 0; i < vlen; i++)
            {
                byte b = value[voffset++];
                // convert to lowercase if uppercased alphabet
                buf[offset++] = b > 64 && b < 91 ? (byte)(b + 32) : b;
            }
        }
        else
        {
            System.arraycopy(value, voffset, buf, offset, vlen);
            offset += vlen;
        }
        
        buf[offset++] = 0; // null byte
        System.arraycopy(key, koffset, buf, offset, 9);
        offset += 9;
        
        return offset - start;
    }
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            String value, 
            byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;

        final int len = value.length();
        int i = 0, codePoint;
        char c, lowSurrogate;
        while (i < len)
        {
            c = value.charAt(i++);
            if(c < 0x0080)
            {
                // ascii
                // convert to lowercase if uppercased alphabet
                buf[offset++] = c > 64 && c < 91 ? (byte)(c + 32) : (byte)c;
            }
            else if(c < 0x0800)
            {
                buf[offset++] = (byte) (0xC0 | ((c >>  6) & 0x1F));
                buf[offset++] = (byte) (0x80 | ((c >>  0) & 0x3F));
            }
            else if (i != len &&
                    Character.isHighSurrogate(c) &&
                    Character.isLowSurrogate((lowSurrogate = value.charAt(i))))
            {
                codePoint = Character.toCodePoint(c, lowSurrogate);
                buf[offset++] = (byte) (0xF0 | ((codePoint >> 18) & 0x07));
                buf[offset++] = (byte) (0x80 | ((codePoint >> 12) & 0x3F));
                buf[offset++] = (byte) (0x80 | ((codePoint >> 6) & 0x3F));
                buf[offset++] = (byte) (0x80 | ((codePoint >> 0) & 0x3F));

                i++;
            }
            else
            {
                buf[offset++] = (byte) (0xE0 | ((c >> 12) & 0x0F));
                buf[offset++] = (byte) (0x80 | ((c >>  6) & 0x3F));
                buf[offset++] = (byte) (0x80 | ((c >>  0) & 0x3F));
            }
        }
        
        buf[offset++] = 0; // null byte
        System.arraycopy(key, koffset, buf, offset, 9);
        offset += 9;
        
        return offset - start;
    }
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            boolean value, 
            byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        buf[offset++] = value ? (byte)1 : (byte)0;
        buf[offset++] = 0; // null byte
        System.arraycopy(key, koffset, buf, offset, 9);
        offset += 9;
        
        return offset - start;
    }
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            int value, 
            byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        IntSerializer.writeInt32(value, buf, offset);
        offset += 4;
        buf[offset++] = 0; // null byte
        System.arraycopy(key, koffset, buf, offset, 9);
        offset += 9;
        
        return offset - start;
    }
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            long value, 
            byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        final int start = offset;
        
        buf[offset++] = 0;
        buf[offset++] = (byte)indexId;
        buf[offset++] = (byte)em.kind;
        IntSerializer.writeInt64(value, buf, offset);
        offset += 8;
        buf[offset++] = 0; // null byte
        System.arraycopy(key, koffset, buf, offset, 9);
        offset += 9;
        
        return offset - start;
    }
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            float value, byte[] key, int koffset, 
            byte[] buf, int offset)
    {
        return writeIndexKey(indexId, em, Float.floatToIntBits(value), key, koffset, 
                buf, offset);
    }
    
    static int writeIndexKey(int indexId, EntityMetadata<?> em, 
            double value, byte[] key, int koffset,  
            byte[] buf, int offset)
    {
        return writeIndexKey(indexId, em, Double.doubleToLongBits(value), key, koffset, 
                buf, offset);
    }
    
    /**
     * Returns the key kind.
     */
    public static int getKind(byte[] key)
    {
        switch(key.length)
        {
            case 9:
                return key[0];
            case 18:
                return key[9];
            default:
                throw DSRuntimeExceptions.invalidKeyLen(key.length);
        }
    }
    
    /**
     * Returns the key kind.
     */
    public static int getKind(byte[] key, int koffset, int klen)
    {
        switch(klen)
        {
            case 9:
                return key[koffset];
            case 18:
                return key[koffset+9];
            default:
                throw DSRuntimeExceptions.invalidKeyLen(klen);
        }
    }
    
    static void validateKind(int kind)
    {
        if(kind < 1 || kind > 127)
            throw DSRuntimeExceptions.invalidKind(kind);
    }
    
    static void validateTagKind(int tagKind)
    {
        if(tagKind < 1 || tagKind > 223)
            throw DSRuntimeExceptions.invalidTagKind(tagKind);
    }
    
    public static byte[] keyFromLinkKey(byte[] linkKey)
    {
        byte[] key = new byte[9];
        System.arraycopy(linkKey, 9, key, 0, 9);
        return key;
    }
    
    public static int keyOffsetFromLinkKey(byte[] linkKey)
    {
        return 9;
    }
    
    /**
     * Returns true if it is indeed a key.
     * Allows zero key like {@link EntityMetadata#ZERO_KEY}.
     */
    public static boolean isKey(byte[] key)
    {
        if (key.length != 9)
            return false;
        
        int kind = key[0];
        if (kind == 0)
            return Arrays.equals(key, EntityMetadata.ZERO_KEY);
        
        return kind > 0 && kind < 128;
    }
    
    /**
     * Returns true if it is indeed a key with the specified kind.
     */
    public static boolean isKey(byte[] key, int kind)
    {
        return key.length == 9 && kind == key[0];
    }
    
    /**
     * Returns the current timestamp w/c could be useful for db modification.
     */
    public static int validate(byte[] key)
    {
        return validate(key, 0);
    }
    
    /**
     * Returns the current timestamp w/c could be useful for db modification.
     */
    public static int validate(byte[] key, int offset)
    {
        if(key == null)
            throw DSRuntimeExceptions.invalidArg("key == null");
        
        if(key.length - offset < 9)
            throw DSRuntimeExceptions.invalidKey(key);

        int kind = key[offset];
        if(kind < 1 || kind > 127)
            throw DSRuntimeExceptions.invalidKey(key);
        
        return kind;
    }
    
    /**
     * Returns the current timestamp w/c could be useful for db modification.
     */
    public static void validateSameKind(byte[] key, EntityMetadata<?> em)
    {
        validateSameKind(key, 0, em);
    }
    
    /**
     * Returns the current timestamp w/c could be useful for db modification.
     */
    public static void validateSameKind(byte[] key, int koffset, EntityMetadata<?> em)
    {
        if(em == null)
            throw DSRuntimeExceptions.invalidArg("em == null");
        
        if(key == null)
            throw DSRuntimeExceptions.invalidArg("key == null");
        
        if(key.length - koffset < 9)
            throw DSRuntimeExceptions.invalidKey(key);

        if(key[koffset] != em.kind)
        {
            throw DSRuntimeExceptions.invalidKey(key, "The key kind did not match : " + 
                    key[koffset] + " != "+ em.kind);
        }
    }
    
    /**
     * Push seq id for rollback.
     */
    public static long pushSeqId(EntityMetadata<?> em)
    {
        return (em.prevId = em.seqId);
    }
    
    /**
     * Rollback seq id.
     */
    public static boolean popSeqId(EntityMetadata<?> em, long prevId)
    {
        boolean ret = prevId != -1 && prevId == em.prevId;
        if (ret)
            em.seqId = prevId;
        
        return ret;
    }
    
    static void fillSequenceKey(final byte[] key, int offset, EntityMetadata<?> em)
    {
        key[offset++] = (byte)em.kind;
        ValueUtil.writeInt64((++em.seqId << 2) | 1, key, offset);
    }
    
    /**
     * Returns an empty entity key to be filled later.
     */
    public static byte[] newEntityKey()
    {
        return new byte[9];
    }
    
    public static byte[] newEntityKey(final int kind, final long timestamp, 
            int modCount)
    {
        // kinds are limited to 127.
        //if(kind < 1 || kind > 127)
        //    throw new InvalidKindException(kind);
        
        // the first 2 bits are reserved
        modCount = (modCount << 2) | 1;
        
        final byte[] internalKey = new byte[9];
        internalKey[0] = (byte)kind;
        writeTimestamp(timestamp, internalKey, 1);
        internalKey[7] = (byte)((modCount >>> 8) & 0xFF);
        internalKey[8] = (byte)modCount;
        
        return internalKey;
    }
    
    static long fillEntityKey(final byte[] key, 
            final int kind, final long timestamp, 
            int modCount)
    {
        // the first 2 bits are reserved
        modCount = (modCount << 2) | 1;
        
        key[0] = (byte)kind;
        writeTimestamp(timestamp, key, 1);
        key[7] = (byte)((modCount >>> 8) & 0xFF);
        key[8] = (byte)modCount;
        return timestamp;
    }
    
    /**
     * Returns a parent-child key.
     */
    public static byte[] newOneToManyKey(byte[] key, byte[] parentKey)
    {
        return newOneToManyKey(key, 0, parentKey, 0);
    }
    
    /**
     * Returns a parent-child key.
     */
    public static byte[] newOneToManyKey(byte[] key, int koffset, byte[] parentKey, int pkoffset)
    {
        final byte[] oneToManyKey = new byte[18];
        oneToManyKey[0] = (byte)(0x80 | parentKey[pkoffset]);
        System.arraycopy(parentKey, pkoffset+1, oneToManyKey, 1, 8);
        System.arraycopy(key, koffset, oneToManyKey, 9, 9);
        return oneToManyKey;
    }
    
    /**
     * Returns the timestamp embedded in the internalKey.
     */
    /*public static long getTs(final byte[] key)
    {
        return readTimestamp(key, key.length - 8);
    }*/
    
    /**
     * Returns the timestamp embedded in the internalKey.
     */
    /*public static long getTimestamp(final byte[] key)
    {
        return readTimestamp(key, key.length - 8);
    }*/
    
    /**
     * Returns a timestamp in 6-byte form.
     */
    public static byte[] new6ByteTimestamp()
    {
        byte[] buffer = new byte[6];
        writeTimestamp(System.currentTimeMillis(), buffer, 0);
        return buffer;
    }
    
    public static byte[] to6ByteTimestamp(byte[] key)
    {
        byte[] buffer = new byte[6];
        System.arraycopy(key, key.length - 8, buffer, 0, 6);
        return buffer;
    }
    
    public static byte[] to6ByteTimestamp(long timestamp)
    {
        byte[] buffer = new byte[6];
        writeTimestamp(timestamp, buffer, 0);
        return buffer;
    }
    
    /**
     * 6-byte timestamp.
     */
    public static long readTimestamp(final byte[] buffer, int offset) 
    {
        // exclude the first 2 msbs (not needed)
        
        return (((long)buffer[offset++] & 0xff) << 40 ) |
             (((long)buffer[offset++] & 0xff) <<  32) |
             (((long)buffer[offset++] & 0xff) << 24) |
             (((long)buffer[offset++] & 0xff) << 16) |
             (((long)buffer[offset++] & 0xff) << 8) |
             (((long)buffer[offset] & 0xff));
    }
    
    /**
     * 6-byte timestamp.
     */
    public static void writeTimestamp(final long value, final byte[] buffer, int offset)
    {
        // exclude the first 2 msbs (not needed)
        buffer[offset++] = (byte)(value >>> 40);
        buffer[offset++] = (byte)(value >>> 32);
        buffer[offset++] = (byte)(value >>> 24);
        buffer[offset++] = (byte)(value >>> 16);
        buffer[offset++] = (byte)(value >>>  8);
        buffer[offset] = (byte)(value);
    }
    
    /**
     * 3-byte int.
     */
    static int readInt24(final byte[] buffer, int offset) 
    {
        // exclude the first 2 msbs (not needed)
        
        return ((buffer[offset++] & 0xff) << 40 ) |
             ((buffer[offset++] & 0xff) <<  32) |
             ((buffer[offset++] & 0xff) << 24) |
             ((buffer[offset++] & 0xff) << 16) |
             ((buffer[offset++] & 0xff) << 8) |
             ((buffer[offset] & 0xff));
    }
    
    /**
     * 3-byte int.
     */
    static void writeInt24(final int value, final byte[] buffer, int offset)
    {
        // exclude the first 2 msbs (not needed)
        buffer[offset++] = (byte)(value >>> 40);
        buffer[offset++] = (byte)(value >>> 32);
        buffer[offset++] = (byte)(value >>> 24);
        buffer[offset++] = (byte)(value >>> 16);
        buffer[offset++] = (byte)(value >>>  8);
        buffer[offset] = (byte)(value);
    }
    
    /*public static void main(String[] args)
    {
        System.err.println((int)'a');
        System.err.println((char)0x7a);
        System.err.println((int)'z');
        for(int i=123; i< 127; i++)
            System.err.println((char)i);
        
        byte[] by = new byte[1];
        by[0] = (byte)128;
        
        int k = by[0];
        System.err.println(k);
        
        for(int i = 128 ; i < 256; i++)
        {
            byte b = (byte)i;
            int g = b;
            System.err.println(b + " " + (b<0) + " " + (g<0));
        }
        
    }*/

}
