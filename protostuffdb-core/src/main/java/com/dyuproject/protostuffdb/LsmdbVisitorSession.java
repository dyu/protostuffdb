//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.LsmdbDatastore.visitRangeAsc;
import static com.dyuproject.protostuffdb.LsmdbDatastore.visitRangeDesc;
import static com.dyuproject.protostuffdb.LsmdbDatastore.visitRangeWithIndexKeyAsc;
import static com.dyuproject.protostuffdb.LsmdbDatastore.visitRangeWithIndexKeyDesc;

import java.util.List;

import protostuffdb.JniOps;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.JniStream;
import com.dyuproject.protostuff.KeyBuilder;

/**
 * Uses a single native db iterator for the entire session.
 * 
 * @author David Yu
 * @created Dec 27, 2013
 */
public final class LsmdbVisitorSession extends VisitorSession
{
    
    final WriteContext context;
    final JniStream stream;
    
    LsmdbVisitorSession(WriteContext context)
    {
        this.context = context;
        this.stream = context.stream;
    }
    
    void close()
    {
        LsmdbJniUtil.close(stream, 0);
    }

    @Override
    public byte[] rawGet(byte[] key, int koffset, int klen)
    {
        return LsmdbJniUtil.get(stream, false, 0, 
                key, koffset, klen, null);
    }

    @Override
    public boolean rawExists(byte[] key, int koffset, int klen)
    {
        return ByteString.EMPTY_BYTE_ARRAY == LsmdbJniUtil.get(stream, false, 0, 
                key, koffset, klen, ByteString.EMPTY_BYTE_ARRAY);
    }
    
    /* ================================================== */
    
    @Override
    public boolean exists(boolean prefix, KeyBuilder kb, boolean pop)
    {
        return LsmdbJniUtil.iget(stream, prefix ? JniOps.FGET_PREFIX : 0, 
                kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), 
                null, false, false, false);
    }
    
    @Override
    public boolean get(HasKV kv, KeyBuilder kb, boolean pop)
    {
        return LsmdbJniUtil.iget(stream, 0, 
                kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), 
                kv, true, false, false);
    }
    
    @Override
    public boolean pget(HasKV kv, boolean includeValue, boolean extractEntityKey, 
            KeyBuilder kb, boolean pop)
    {
        return LsmdbJniUtil.iget(stream, JniOps.FGET_PREFIX, 
                kb.buf(), kb.offset(), pop ? kb.popLen() : kb.len(), 
                kv, includeValue, true, extractEntityKey);
    }

    /* ================================================== */
    
    @Override
    public <T> boolean exists(byte[] key, EntityMetadata<T> em, byte[] parentKey)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(key);
                
                KeyUtil.validateSameKind(key, em);
                
                return rawExists(key);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw new InvalidArgException("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    
                    return rawExists(key);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return rawExists(KeyUtil.newOneToManyKey(key, parentKey));
        }
        
        throw DSRuntimeExceptions.invalidArg("Exists operation not applicable for type: " + 
                em.type);
    }

    @Override
    public <T> byte[] get(byte[] key, EntityMetadata<T> em, byte[] parentKey)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(key);
                
                KeyUtil.validateSameKind(key, em);
                
                return rawGet(key);
                
            case LINKED_CHILD:
                
                KeyUtil.validateSameKind(key, em);
                
                if(parentKey == null)
                {
                    //throw new InvalidArgException("parentKey null");
                    // retrieves the parent key.
                    // caller must already know this.
                    return rawGet(key);
                }
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                return rawGet(KeyUtil.newOneToManyKey(key, parentKey));
        }
        
        throw DSRuntimeExceptions.invalidArg("Get operation not applicable for type: " + 
                em.type);
    }

    @Override
    public <V> int visitKeys(List<byte[]> keys, boolean reverse, boolean extractEntityKey,
            Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys (list) empty");
        
        LsmdbJniUtil.writeHeaderGet(stream, 0);
        
        byte[] k;
        int koffset = 0, klen = 9;
        if(reverse)
        {
            for(int i = keys.size(); i-- > 0;)
            {
                k = keys.get(i);
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i);
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        
        return LsmdbJniUtil.multiGetKV(stream.limit(), 
                visitor, param);
    }
    
    @Override
    public <V> int visitHasKeys(List<? extends HasKey> keys, boolean reverse, boolean extractEntityKey,
            Visitor<V> visitor, V param)
    {
        if(keys == null || keys.isEmpty())
            throw DSRuntimeExceptions.invalidArg("keys (list) empty");
        
        LsmdbJniUtil.writeHeaderGet(stream, 0);
        
        byte[] k;
        int koffset = 0, klen = 9;
        if(reverse)
        {
            for(int i = keys.size(); i-- > 0;)
            {
                k = keys.get(i).getKey();
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        else
        {
            for(int i = 0, len = keys.size(); i < len; i++)
            {
                k = keys.get(i).getKey();
                if (extractEntityKey)
                    koffset = k.length - 9;
                else
                    klen = k.length;
                stream.$writeK(k, koffset, klen);
            }
        }
        
        return LsmdbJniUtil.multiGetKV(stream.limit(), 
                visitor, param);
    }

    @Override
    public <T, V> int visitKind(EntityMetadata<T> em, int limit, boolean desc, byte[] startKey,
            byte[] parentKey, Visitor<V> visitor, V param)
    {
        switch(em.type)
        {
            case DEFAULT:
            {
                if(parentKey != null)
                    throw DSRuntimeExceptions.parentNotNull(null);
                
                if(startKey == null)
                {
                    // TODO single byte array + len arg
                    final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
                    final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
                    
                    return desc ? visitRangeDesc(stream, false, 0, limit, 
                            visitor, param, sk, 0, sk.length, ek, 0, ek.length, false) : visitRangeAsc(stream, false, 0,
                                    limit, visitor, param, 
                                    sk, 0, sk.length, ek, 0, ek.length, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                if(desc)
                {
                    final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
                    
                    return visitRangeWithIndexKeyDesc(stream, false, 0, limit, startKey, 
                            visitor, param, 
                            sk, 0, sk.length, false);
                }
                
                final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
                
                return visitRangeWithIndexKeyAsc(stream, false, 0, limit, startKey, 
                        visitor, param, 
                        ek, 0, ek.length, false);
            }
                
            case LINKED_CHILD:
            {
                if(parentKey == null)
                    throw DSRuntimeExceptions.parentNull(null);
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                
                if(startKey == null)
                {
                    // TODO single byte array + len arg
                    final byte[] sk = KeyUtil.newLinkRangeKeyStart(em.kind, parentKey);
                    final byte[] ek = KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey);
                    
                    return desc ? visitRangeDesc(stream, false, 0, limit, 
                            visitor, param, sk, 0, sk.length, ek, 0, ek.length, false) : visitRangeAsc(stream, false, 0,
                                    limit, visitor, param, sk, 0, sk.length, ek, 0, ek.length, false);
                }
                
                KeyUtil.validateSameKind(startKey, em);
                
                final byte[] ik = KeyUtil.newLinkIndexKey(em.kind, parentKey, startKey);
                
                if(desc)
                {
                    final byte[] sk = KeyUtil.newLinkRangeKeyStart(em.kind, parentKey);
                    
                    return visitRangeWithIndexKeyDesc(stream, false, 0, limit, ik, 
                            visitor, param, 
                            sk, 0, sk.length, 
                            false);
                }
                
                final byte[] ek = KeyUtil.newLinkRangeKeyEnd(em.kind, parentKey);
                
                return visitRangeWithIndexKeyAsc(stream, false, 0, limit, ik, 
                        visitor, param, 
                        ek, 0, ek.length,  
                        false);
            }
        }
        
        throw DSRuntimeExceptions.invalidArg("Visit operation not applicable for type: " + 
                em.type);
    }
    
    @Override
    public <V> int scan(boolean keysOnly, int limit, boolean desc, Visitor<V> visitor, V param)
    {
        // TODO incremental jni scan
        final boolean limited = limit > 0 && limit != Integer.MAX_VALUE;
        
        return LsmdbJniUtil.scan(stream, false, 0, 
                keysOnly, limited ? limit : 0, desc, 
                ByteString.EMPTY_BYTE_ARRAY, 
                visitor, param);
    }

    @Override
    public <V> int visitRange(boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey,
            Visitor<V> visitor, V param,
            boolean valueAsKey, boolean skRelativeLimit,
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen)
    {
        int flags = 0;
        if (valueAsKey)
            flags |= JniOps.FSCAN_VALUE_AS_KEY;
        if (skRelativeLimit)
            flags |= JniOps.FSCAN_RELATIVE_LIMIT;
        
        if(rawStartKey == null)
        {
            return desc ? visitRangeDesc(stream, false, flags, limit, 
                    visitor, param, sk, skoffset, sklen, ek, ekoffset, eklen, keysOnly) : visitRangeAsc(stream, false, flags,
                            limit, visitor, param, sk, skoffset, sklen, ek, ekoffset, eklen, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, sk, skoffset, sklen) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, ek, ekoffset, eklen) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            return visitRangeWithIndexKeyDesc(stream, false, flags, limit, rawStartKey, 
                    visitor, param, sk, skoffset, sklen, keysOnly);
        }
        
        return visitRangeWithIndexKeyAsc(stream, false, flags, limit, rawStartKey, 
                visitor, param, ek, ekoffset, eklen, keysOnly);
    }

    @Override
    public <V> int visitIndex(boolean keysOnly, int limit, boolean desc, byte[] rawStartKey,
            Visitor<V> visitor, V param, KeyBuilder kb, boolean pop)
    {
        final int offset = kb.offset(), size = pop ? kb.popLen() : kb.len();
        final byte[] buf = kb.buf();
        if(rawStartKey == null)
        {
            // append 0x00
            buf[offset+size] = 0;
            final byte[] sk = new byte[size+1];
            System.arraycopy(buf, offset, sk, 0, size+1);
            
            // append 0x01
            buf[offset+size] = (byte)1;
            //final byte[] ek = new byte[size+1];
            //System.arraycopy(buf, offset, ek, 0, size+1);
            
            return desc ? visitRangeDesc(stream, false, 0, limit, 
                    visitor, param, sk, 0, sk.length, buf, offset, size+1, keysOnly) : visitRangeAsc(stream, false, 0,
                            limit, visitor, param, sk, 0, sk.length, buf, offset, size+1, keysOnly);
        }
        
        // verify upper bounds
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size) < 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        // verify lower bounds
        buf[offset+size] = (byte)0xFF;
        if(ValueUtil.compare(rawStartKey, 0, rawStartKey.length, buf, offset, size+1) >= 0)
            throw DSRuntimeExceptions.invalidArg("rawStartKey not in range.");
        
        if(desc)
        {
            // append 0x00
            buf[offset+size] = 0;
            //final byte[] sk = new byte[size+1];
            //System.arraycopy(buf, offset, sk, 0, size+1);
            
            return visitRangeWithIndexKeyDesc(stream, false, 0, limit, rawStartKey, 
                    visitor, param, buf, offset, size+1, keysOnly);
        }
        
        // append 0x01
        buf[offset+size] = (byte)1;
        //final byte[] ek = new byte[size+1];
        //System.arraycopy(buf, offset, ek, 0, size+1);
        
        return visitRangeWithIndexKeyAsc(stream, false, 0, limit, rawStartKey, 
                visitor, param, buf, offset, size+1, keysOnly);
    }

}
