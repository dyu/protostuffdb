//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import com.dyuproject.protostuff.RpcResponse;

/**
 * {@link KV} cache.
 * 
 * @author David Yu
 * @created Aug 25, 2014
 */
public final class KVCache implements Cache, AbstractAppendOnlyList.Listener<KV>, 
        Visitor<WriteContext>
{
    public final int kind;
    public final AbstractAppendOnlyList<KV> list;
    
    public KVCache(EntityMetadata<?> em, boolean serial, int segmentSize)
    {
        this(em.kind, serial, segmentSize);
    }
    
    public KVCache(int kind, boolean serial, int segmentSize)
    {
        this.kind = kind;
        this.list = serial ? new SerialAppendOnlyList<KV>(segmentSize, this) : 
            new ConcurrentAppendOnlyList<KV>(segmentSize, this);
    }
    
    public KV[] newArray(int size)
    {
        return new KV[size];
    }
    
    public void assignIndexTo(KV item, int index)
    {
        //item.id = index + 1;
    }
    
    public KV get(int id)
    {
        return list.get(id - 1);
    }
    
    /*public int add(KV entity, byte[] key, long now, WriteContext context)
    {
        final int idx = list.add(entity);
        context.fillIndexedEntityKey(key, kind, now, idx);
        return idx;
    }*/
    
    public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
            WriteContext param, int index)
    {
        list.fill(new KV(key, ValueUtil.copy(v, voffset, vlen)));
        
        return false;
    }
    
    public <V> int visit(Visitor<V> visitor, V v)
    {
        return AbstractAppendOnlyList.visit(list, visitor, v);
    }

    public <V> int visit(Visitor<V> visitor, V v, int index)
    {
        return AbstractAppendOnlyList.visit(list, visitor, v, index);
    }
    
    public int pipeTo(RpcResponse res)
    {
        return visit(RpcResponse.PIPED_VISITOR, res);
    }
    
    public int pipeTo(RpcResponse res, int index)
    {
        return visit(RpcResponse.PIPED_VISITOR, res, index);
    }
    
    public int size()
    {
        return list.size();
    }
    
    public void clear()
    {
        list.clear();
    }
}
