//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;
import java.util.Map.Entry;

/**
 * A mechanism to traverse the store's keys and values.
 *
 * @author David Yu
 * @created Feb 25, 2011
 */
public interface Visitor<P>
{
    
    /**
     * Returning true will stop the traversal.
     */
    public boolean visit(byte[] key, 
            byte[] v, int voffset, int vlen, 
            P param, int index);
    
    
    
    /**
     * Appends a key/value pair to the list.
     */
    public static final Visitor<List<Entry<byte[], byte[]>>> APPEND_ENTRY = 
            new Visitor<List<Entry<byte[],byte[]>>>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                List<Entry<byte[], byte[]>> param, int index)
        {
            param.add(new MutableEntry<byte[],byte[]>(key, 
                    ValueUtil.copy(v, voffset, vlen)));
            
            return false;
        }
    };
    
    /**
     * Appends the key to the list.
     */
    public static final Visitor<List<byte[]>> APPEND_KEY = new Visitor<List<byte[]>>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                List<byte[]> param, int index)
        {
            param.add(key);
            
            return false;
        }
    };
    
    /**
     * Appends the key (extracted from the index key) to the list.
     */
    public static final Visitor<List<byte[]>> APPEND_EXTRACTED_KEY = 
            new Visitor<List<byte[]>>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                List<byte[]> param, int index)
        {
            param.add(KeyUtil.extractFrom(key));
            
            return false;
        }
    };
    
    /**
     * Appends the value to the list.
     */
    public static final Visitor<List<byte[]>> APPEND_VALUE = new Visitor<List<byte[]>>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                List<byte[]> param, int index)
        {
            param.add(ValueUtil.copy(v, voffset, vlen));
            
            return false;
        }
    };
    
    /**
     * Appends a key-value object to the list.
     */
    public static final Visitor<List<HasKV>> APPEND_KV = new Visitor<List<HasKV>>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                List<HasKV> param, int index)
        {
            param.add(new KV(key, ValueUtil.copy(v, voffset, vlen)));
            
            return false;
        }
    };
    
    /**
     * Appends a key-value (key extracted from index key) object to the list.
     */
    public static final Visitor<List<HasKV>> APPEND_EXTRACTED_KV = 
            new Visitor<List<HasKV>>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                List<HasKV> param, int index)
        {
            param.add(new KV(KeyUtil.extractFrom(key), ValueUtil.copy(v, voffset, vlen)));
            
            return false;
        }
    };
    
    /**
     * Sets the key.
     */
    public static final Visitor<HasKey> SET_KEY = new Visitor<HasKey>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                HasKey param, int index)
        {
            param.setKey(key);
            
            return false;
        }
    };
    
    /**
     * Sets the key (extracted from the index key).
     */
    public static final Visitor<HasKey> SET_EXTRACTED_KEY = new Visitor<HasKey>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                HasKey param, int index)
        {
            param.setKey(KeyUtil.extractFrom(key));
            
            return false;
        }
    };
    
    /**
     * Sets the value.
     */
    public static final Visitor<HasValue> SET_VALUE = new Visitor<HasValue>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                HasValue param, int index)
        {
            param.setValue(ValueUtil.copy(v, voffset, vlen));
            
            return false;
        }
    };
    
    /**
     * Sets the key and value.
     */
    public static final Visitor<HasKV> SET_KV = new Visitor<HasKV>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                HasKV param, int index)
        {
            param.setKV(key, ValueUtil.copy(v, voffset, vlen));
            
            return false;
        }
    };
    
    /**
     * Sets the key (extracted from index key) and value.
     */
    public static final Visitor<HasKV> SET_EXTRACTED_KV = new Visitor<HasKV>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                HasKV param, int index)
        {
            param.setKV(KeyUtil.extractFrom(key), ValueUtil.copy(v, voffset, vlen));
            
            return false;
        }
    };
    
    /**
     * Sets the sequence id of the EntityMetadata.
     * The visit should be in reverse order to retrieve the latest sequence id.
     */
    public static final Visitor<EntityMetadata<?>> SET_SEQ_ID = new Visitor<EntityMetadata<?>>()
    {
        @Override
        public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                EntityMetadata<?> em, int index)
        {
            em.seqId = ValueUtil.toInt64(key, 1) >>> 2;
            return true;
        }
    };
    
    /**
     * No operation.
     */
    public static final Visitor<Void> NOOP = new Visitor<Void>()
    {
        public boolean visit(byte[] key, 
                byte[] v, int voffset, int vlen, 
                Void param, int index)
        {
            return false;
        }
    };
}
