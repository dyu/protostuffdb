//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Pipe;

/**
 * The pipe between the datastore and the socket.
 *
 * @author David Yu
 * @created Aug 24, 2011
 */
public final class ProtostuffPipe extends Pipe
{
    
    public final DSByteArrayInput protostuffInput = new DSByteArrayInput(null, 0, 0, true);
    byte[] key;
    Pipe.Schema<?> pipeSchema;
    
    int fieldNumber;
    boolean repeated;
    
    EntityMetadata<?> em;
    byte[] parentKey;
    
    int voffset, vlen;
    
    public ProtostuffPipe()
    {
        
    }
    
    public EntityMetadata<?> em()
    {
        return em;
    }
    
    public ProtostuffPipe em(EntityMetadata<?> em)
    {
        this.em = em;
        return this;
    }
    
    public Pipe.Schema<?> schema()
    {
        return pipeSchema;
    }
    
    public ProtostuffPipe schema(Pipe.Schema<?> val)
    {
        pipeSchema = val;
        return this;
    }
    
    public byte[] k()
    {
        return key;
    }
    
    public byte[] v()
    {
        return protostuffInput.buffer;
    }
    
    public int voffset()
    {
        return voffset;
    }
    
    public int vlen()
    {
        return vlen;
    }
    
    public boolean repeated()
    {
        return repeated;
    }
    
    public ProtostuffPipe repeated(boolean val)
    {
        repeated = val;
        return this;
    }
    
    public int fieldNumber()
    {
        return fieldNumber;
    }
    
    public ProtostuffPipe fieldNumber(int val)
    {
        fieldNumber = val;
        return this;
    }
    
    /*public Pipe.Schema<?> currentSchema()
    {
        return pipeSchema;
    }
    
    public byte[] currentKey()
    {
        return key;
    }
    
    public byte[] currentValue()
    {
        return DSUtils.getBuffer(protostuffInput);
    }
    
    public int currentValueOffset()
    {
        return DSUtils.getOffset(protostuffInput);
    }
    
    public int currentValueLimit()
    {
        return DSUtils.getLimit(protostuffInput);
    }
    
    public int currentValueLength()
    {
        return DSUtils.getLength(protostuffInput);
    }
    
    public int currentFieldNumber()
    {
        return fieldNumber;
    }
    
    public boolean currentRepeated()
    {
        return repeated;
    }*/
    
    public ProtostuffPipe init(EntityMetadata<?> em, 
            int fieldNumber, boolean repeated)
    {
        return init(em, em.pipeSchema, fieldNumber, repeated);
    }
    
    public ProtostuffPipe init(EntityMetadata<?> em, byte[] parentKey, int fieldNumber)
    {
        this.parentKey = parentKey;
        return init(em, em.pipeSchema, fieldNumber, true);
    }
    
    public ProtostuffPipe init(EntityMetadata<?> em, Pipe.Schema<?> pipeSchema, 
            int fieldNumber, boolean repeated)
    {
        this.em = em;
        this.pipeSchema = pipeSchema;
        this.fieldNumber = fieldNumber;
        this.repeated = repeated;
        
        return this;
    }
    
    public ProtostuffPipe clear()
    {
        key = null;
        pipeSchema = null;
        protostuffInput.reset(null, 0, 0);
        
        fieldNumber = 0;
        repeated = false;
        
        input = null;
        
        em = null;
        parentKey = null;
        
        return this;
    }
    
    public void dc()
    {
        // so we don't cache this from failed ops
        parentKey = null;
    }
    
    public ProtostuffPipe setKey(byte[] key)
    {
        this.key = key;
        
        return this;
    }
    
    public ProtostuffPipe set(byte[] key, byte[] value)
    {
        return set(key, value, 0, value.length);
    }
    
    public ProtostuffPipe set(byte[] key, byte[] value, int offset, int len)
    {
        this.key = key;
        if (key == null)
        {
            // the key is right after the value
            len -= 9;
        }
        
        protostuffInput.reset(value, offset, len);
        
        voffset = offset;
        vlen = len;
        
        return this;
    }

    protected Input begin(Schema<?> pipeSchema) throws IOException
    {
        if (em == null)
            return protostuffInput;
        
        int offset;
        byte[] key = this.key;
        if (key == null)
        {
            // the key is right after the value
            offset = voffset + vlen;
            key = v();
        }
        else
        {
            offset = key.length - 9;
        }
        
        output.writeByteRange(false, 1, key, offset, 9, false);
        if (!em.seq)
            output.writeFixed64(2, KeyUtil.readTimestamp(key, offset + 1), false);
        
        return protostuffInput;
    }

    protected void end(Schema<?> pipeSchema, Input input, boolean cleanupOnly) 
    throws IOException
    {
        // must be set to null so that this method gets called back after writing 
        // the first nested message.
        this.output = null;

        this.input = null;
    }

}
