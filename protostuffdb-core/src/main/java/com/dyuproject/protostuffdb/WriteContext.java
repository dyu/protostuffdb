//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.DSPipe;
import com.dyuproject.protostuff.DSUtils;
import com.dyuproject.protostuff.JniStream;
import com.dyuproject.protostuff.JsonFlags;
import com.dyuproject.protostuff.JsonXOutput;
import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.ProtostuffOutput;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.WriteSession;
import com.dyuproject.protostuff.ds.CAS;

/**
 * Contains all resources/buffers that are owned by a single thread.
 * 
 * No sharing.
 *
 * @author David Yu
 * @created Mar 2, 2011
 */
public final class WriteContext
{
    
    /**
     * A handler for the situation where the data does not fit in the buffer 
     * on serialization.
     */
    public interface OverflowHandler
    {
        int flush(byte[] buf, int offset, int len) throws IOException;
        
        int flush(byte[] buf, int offset, int len, 
                byte[] next, int nextoffset, int nextlen) throws IOException;
        
        int flush(LinkedBuffer lb, 
                byte[] buf, int offset, int len) throws IOException;
    }

    /**
     * When this limit is reached, System.exit(0) is called.
     * 
     * If this value is zero, the system will continue to operate even with 
     * ntp being out-of-sync.
     * 
     * The ntp clock must be corrected (by admin) ASAP.
     */
    static final int NTP_ERROR_LIMIT = 
            Math.max(0, Integer.getInteger("protostuff.ds.ntp_error_limit", 1000));
    
    // TODO merge into a single buffer
    public final byte[] bufTmp, udfBuffer;
    public final byte[] entityBuffer;
    public final int entityOffset;
    // placeholder for now
    // indexBuffer and udfBuffer could be just one byte array and simply slice then up.
    public final int udfBufOffset = 0;
    public final LinkedBuffer lb, lbUdf;
    
    public final WriteSession sessionUdf;
    
    final KeyBuilder kb;
    
    final ProtostuffOutput serOutput;
    final ProtostuffOutput serUdfOutput;
    public final JsonXOutput pubOutput;
    final SerAndIndexCollectOutput serAndIndexCollectOutput;
    final IndexCollectOutput indexCollectOutput;
    public final CASAndIndexCollectOutput casAndIndexCollectOutput;
    
    final SchemaUpdateInput schemaUpdateInput;
    //final DSByteArrayInput input1, input2;
    
    public final ProtostuffPipe pipe = new ProtostuffPipe();
    
    final DSPipe dsPipe = new DSPipe();
    final int[] fieldOffsetAddIdx = new int[128], fieldOffsetRemoveIdx = new int[128], 
            fieldsTracked = new int[128], 
            fieldsApplied = new int[128], 
            fieldCASIdx = new int[128], fieldUpdateIdx = new int[128], 
            varint32Idx = new int[256];
    
    final long[] varint64Idx = new long[64];
    
    /* ================================================== */
    // shadow
    
    final SerAndIndexCollectOutput _serAndIndexCollectOutput;
    
    private final int[] _fieldOffsetAddIdx = new int[128],  
            _fieldsTracked = new int[128], 
            _varint32Idx = new int[256];
    
    private final long[] _varint64Idx = new long[64];
    
    /* ================================================== */
    
    private final int[] intset = new int[512], // for the index set
            // for generating entity keys
            // 128 = current
            // 128 = to clear the current
            countIdx = new int[128 + 128];
    
    private final byte[][] data = new byte[128][];
    private final int[] dataList = new int[128];
    private int dataCount = 0;
    
    public final int first, exclusiveLast, partitionSize;
    
    private int prevKind;
    private int ntpErrorCount;
    private long ts;
    
    public final JniStream stream;
    
    private OverflowHandler delegate;
    private final OverflowHandler handler = new OverflowHandler()
    {
        public int flush(LinkedBuffer lb, byte[] buf, int offset, int len) throws IOException
        {
            if(delegate == null)
                throw DSRuntimeExceptions.operationFailure("Entity too large.");
            
            return delegate.flush(lb, buf, offset, len);
        }
        
        public int flush(byte[] buf, int offset, int len, byte[] next, int nextoffset, int nextlen)
                throws IOException
        {
            if(delegate == null)
                throw DSRuntimeExceptions.operationFailure("Entity too large.");
            
            return delegate.flush(buf, offset, len, next, nextoffset, nextlen);
        }
        
        public int flush(byte[] buf, int offset, int len) throws IOException
        {
            if(delegate == null)
                throw DSRuntimeExceptions.operationFailure("Entity too large.");
            
            return delegate.flush(buf, offset, len);
        }
    };
    
    final OutputStream out = new OutputStream()
    {
        @Override
        public void write(int arg0) throws IOException
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void close() throws IOException
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void flush() throws IOException
        {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void write(byte[] arg0) throws IOException
        {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void write(byte[] buf, int offset, int len) throws IOException
        {
            throw DSRuntimeExceptions.operationFailure("Entity too large.");
        }
    };
    
    /**
     * Mutable integers for any usecase.
     */
    public int $field = 0, $count = 0, $offset = 0, $len = 0;
    
    /**
     * A mutable parameter that can be used by any handler/visitor to customize the pipe schema.
     */
    public Pipe.Schema<?> ps;
    
    public WriteContext(byte[] streamBuf, byte[] bufTmp,
            int first, int exclusiveLast)
    {
        this(streamBuf, bufTmp, first, exclusiveLast, System.currentTimeMillis());
    }
    
    public WriteContext(byte[] streamBuf, byte[] bufTmp, 
            int first, int exclusiveLast, long currentTs)
    {
        this.bufTmp = bufTmp;
        this.first = first;
        this.exclusiveLast = exclusiveLast;
        this.partitionSize = exclusiveLast - first;
        
        this.stream = new JniStream(streamBuf, out);
        
        // shares the buffer with the stream
        entityOffset = stream.offset();
        entityBuffer = streamBuf;
        
        udfBuffer = new byte[8192];
        lbUdf = LinkedBuffer.use(udfBuffer);
        sessionUdf = new WriteSession(lbUdf);
        
        lb = LinkedBuffer.use(entityBuffer, entityOffset);
        kb = new KeyBuilder(LinkedBuffer.use(udfBuffer));
        
        serOutput = new ProtostuffOutput(lb);
        serUdfOutput = new ProtostuffOutput(lbUdf);
        
        pubOutput = new JsonXOutput(LinkedBuffer.use(bufTmp, 4), JsonFlags.NUMERIC, null);
        
        serAndIndexCollectOutput = new SerAndIndexCollectOutput(
                fieldOffsetAddIdx, 
                fieldsTracked, 
                
                varint32Idx, 
                varint64Idx, 
                lb, out, handler);
        
        _serAndIndexCollectOutput = new SerAndIndexCollectOutput(
                _fieldOffsetAddIdx, 
                _fieldsTracked, 
                
                _varint32Idx, 
                _varint64Idx, 
                lb, out, handler);
        
        indexCollectOutput = new IndexCollectOutput(
                dsPipe.input, 
                // operate on the same array so we can create indexes on insert
                // without having to serialize
                fieldOffsetAddIdx, // (was fieldOffsetRemoveIdx)
                fieldsTracked,
                
                varint32Idx, 
                varint64Idx);
        
        casAndIndexCollectOutput = new CASAndIndexCollectOutput(
                dsPipe.input, 
                fieldCASIdx, 
                fieldUpdateIdx, 
                
                fieldOffsetAddIdx, 
                fieldOffsetRemoveIdx, 
                fieldsTracked, 
                fieldsApplied,
                
                varint32Idx, 
                varint64Idx, 
                lb, out, handler);
        
        ts = currentTs;
        
        schemaUpdateInput = new SchemaUpdateInput(
                new DSByteArrayInput(null, 0, 0, true), 
                new DSByteArrayInput(null, 0, 0, true));
        
        // initialize
        Arrays.fill(countIdx, first);
    }
    
    public WriteContext setOverflowHandler(OverflowHandler handler)
    {
        this.delegate = handler;
        return this;
    }
    
    public CAS.Query getCASQuery()
    {
        return casAndIndexCollectOutput;
    }
    
    public KeyBuilder kb()
    {
        return kb.clear();
    }
    
    public int[] intset()
    {
        System.arraycopy(intset, 256, intset, 0, 256);
        return intset;
    }
    
    public byte[] data(int kind)
    {
        return data[kind];
    }
    
    /**
     * Called by RpcWorker after every request.
     */
    public void clear()
    {
        ps = null;
        pipe.dc();
    }
    
    public WriteContext data(int kind, byte[] value)
    {
        // only increment if its the first time for the kind
        if(null == data[kind])
            dataList[dataCount++] = kind;
        
        data[kind] = value;
        
        return this;
    }
    
    public WriteContext clearData()
    {
        if(dataCount == 0)
            return this;
        
        for(int i = 0; i < dataCount; i++)
            data[dataList[i]] = null;
        
        dataCount = 0;
        
        return this;
    }
    
    /**
     * Returns a new entity key that is guaranteed to be unique for a single 
     * machine/shard.
     */
    /*public byte[] newEntityKey(final int kind)
    {
        final byte[] key = new byte[9];
        fillEntityKey(key, kind, System.currentTimeMillis(), -1);
        return key;
    }*/
    
    /**
     * Returns a new entity key that is guaranteed to be unique for a single 
     * machine/shard.
     */
    public byte[] newEntityKey(final EntityMetadata<?> em)
    {
        final byte[] key = new byte[9];
        if (em.seq)
            KeyUtil.fillSequenceKey(key, 0, em);
        else
            fillEntityKey(key, em.kind, ts(em), -1);
        return key;
    }
    
    /**
     * Returns the ts of the filled entity key (guaranteed to be unique for a single 
     * machine/shard).
     */
    /*public long fillEntityKey(final byte[] key, final EntityMetadata<?> em)
    {
        if (em.seq)
        {
            KeyUtil.fillSequenceKey(key, 0, em);
            return System.currentTimeMillis();
        }
        
        return fillEntityKey(key, em.kind, System.currentTimeMillis(), -1);
    }*/
    
    /**
     * Returns the ts of the filled entity key (guaranteed to be unique for a single 
     * machine/shard).
     */
    public long fillEntityKey(final byte[] key, final EntityMetadata<?> em, 
            long now)
    {
        if (em.seq)
        {
            KeyUtil.fillSequenceKey(key, 0, em);
            return now;
        }
        
        return fillEntityKey(key, em.kind, now, -1);
    }
    
    /**
     * Returns the ts of the filled entity key (guaranteed to be unique for a single 
     * machine/shard) and resets the internal count of {@code emEntry}.
     */
    public long fillEntityKey(final byte[] key, final EntityMetadata<?> em, 
            final EntityMetadata<?> entryEm, final int entryCount)
    {
        long now = System.currentTimeMillis();
        
        if (em.seq)
        {
            KeyUtil.fillSequenceKey(key, 0, em);
            return now;
        }
        
        if (now <= ts && (exclusiveLast == countIdx[em.kind] || 
                entryCount > (exclusiveLast - countIdx[entryEm.kind])))
        {
            // we need the timestamp to be newer, to allow the entries to 
            // have the same timestamp
            now = ts + 1;
        }
        
        return fillEntityKey(key, em.kind, now, -1);
    }
    
    /**
     * Returns the ts of the filled entity key (guaranteed to be unique for a single 
     * machine/shard) and resets the internal count of {@code emEntry}.
     */
    public long fillEntityKey(final byte[] key, 
            final EntityMetadata<?> entryEm, final int entryCount)
    {
        long now = System.currentTimeMillis();
        
        if (entryEm.seq)
        {
            KeyUtil.fillSequenceKey(key, 0, entryEm);
            return now;
        }
        
        if (now <= ts && entryCount > (exclusiveLast - countIdx[entryEm.kind]))
        {
            // we need the timestamp to be newer, to allow the entries to 
            // have the same timestamp
            now = ts + 1;
        }
        
        return fillEntityKey(key, entryEm.kind, now, -1);
    }
    
    /**
     * Returns the ts of the filled entity key (guaranteed to be unique for a single 
     * machine/shard) and resets the internal count of {@code emEntry}.
     */
    long fillIndexedEntityKey(final byte[] key, final int kind, 
            final long now, final int idx)
    {
        return fillEntityKey(key, kind, now, idx);
    }
    
    /**
     * Gets the current timestamp that will actually be used when it is time to 
     * create the key.
     */
    public long ts(final EntityMetadata<?> em)
    {
        long now = System.currentTimeMillis();
        
        if (now <= ts && exclusiveLast == countIdx[em.kind])
        {
            // return the actual timestamp that will be used
            now = ts + 1;
        }
        
        return now;
    }
    
    /**
     * Gets the current timestamp that will actually be used when it is time to 
     * create the keys.
     */
    public long ts(final EntityMetadata<?> em, 
            final EntityMetadata<?> entryEm, final int entryCount)
    {
        long now = System.currentTimeMillis();
        
        if (now <= ts && (exclusiveLast == countIdx[em.kind] || 
                entryCount > (exclusiveLast - countIdx[entryEm.kind])))
        {
            // return the actual timestamp that will be used
            now = ts + 1;
        }
        
        return now;
    }
    
    /**
     * Returns the ts of the filled entity key (guaranteed to be unique for a single 
     * machine/shard).
     */
    long fillEntityKey(final byte[] key, final int kind, long now, final int idx)
    {
        if (now > ts)
        {
            // update timestamp
            ts = now;
            
            if (NTP_ERROR_LIMIT != 0)
            {
                // reset
                ntpErrorCount = 0;
            }
            
            if (prevKind == 0)
            {
                // other kinds have had stale ts, so reset the counts for the them as well
                System.arraycopy(countIdx, 128, countIdx, 0, 128);
                // set this kind
                prevKind = kind;
            }
            
            // move to next
            countIdx[kind] = first + 1;
            
            return KeyUtil.fillEntityKey(key, kind, now, idx == -1 ? first : idx % partitionSize);
        }
        
        if ((NTP_ERROR_LIMIT != 0) && (ntpErrorCount != 0 || now != ts) && 
                (NTP_ERROR_LIMIT == ++ntpErrorCount))
        {
            // ntp went backwards and limit reached
            System.err.println("NTP clock out of sync! Limit reached: " + 
                    NTP_ERROR_LIMIT);
            new Throwable().printStackTrace();
            System.exit(0);
        }
        
        if (prevKind != 0 && prevKind != kind)
        {
            // indicate that this kind needs to be reset as well
            prevKind = 0;
        }
        
        // increment, reset if it reaches the limit
        int count = countIdx[kind]++;
        if (count == exclusiveLast)
        {
            countIdx[kind] = count = first;
            
            // System.currentTimeMillis() refresh rate on the machine is probably 10ms 
            now = ++ts;
        }
        
        return KeyUtil.fillEntityKey(key, kind, now, idx == -1 ? count : idx % partitionSize);
    }
    
    /**
     * Returns the serialized size of the entity.
     */
    <T> int serAndIndexCollect(T entity, EntityMetadata<T> em)
    {
        return serAndIndexCollect(entity, em, serAndIndexCollectOutput);
    }
    
    private static int resolveMaxSize(int maxSize)
    {
        return maxSize > 0 ? maxSize : EntityMetadata.ENTITY_MAX_SIZE;
    }
    
    /**
     * Returns the serialized size of the entity.
     */
    <T> int serAndIndexCollect(T entity, EntityMetadata<T> em, 
            final SerAndIndexCollectOutput output)
    {
        final Schema<T> schema = em.pipeSchema.wrappedSchema;
        try
        {
            schema.writeTo(output.init(em), entity);
            
            final int size = output.getSize();
            
            // TODO comment out DSUtils.isBufferOverflow? (handled by OverflowHandler)
            if (DSUtils.isBufferOverflow(output) || size > resolveMaxSize(em.maxSize))
            {
                throw DSRuntimeExceptions.invalidArg("Entity too large: " + 
                        schema.messageFullName());
            }
            
            return size;
        }
        catch (IOException e)
        {
            throw new RuntimeException("Serializing to a LinkedBuffer threw an IOException " + 
                    "(should never happen).", e);
        }
        finally
        {
            output.clear();
        }
    }
    
    /**
     * Returns the number of index entries.
     */
    <T> int indexCollect(byte[] data, int offset, int len, EntityMetadata<T> em)
    {
        final IndexCollectOutput output = indexCollectOutput.init(em, 
                data, offset, len);
        try
        {
            em.pipeSchema.writeTo(output, output.pipe);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Serializing to a LinkedBuffer threw an IOException " + 
                    "(should never happen).", e);
        }

        return output.count;
    }

    /**
     * Returns the serialized size of the entity being modified.
     */
    <T> int casAndIndexCollect(byte[] data, int offset, int len, 
            EntityMetadata<T> em, CAS cas)
    {
        final CASAndIndexCollectOutput output = casAndIndexCollectOutput.init(em, cas, 
                data, offset, len);
        try
        {
            em.pipeSchema.writeTo(output, output.pipe);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Serializing to a LinkedBuffer threw an IOException " + 
                    "(should never happen).", e);
        }
        final int size = output.getSize();
        
        // TODO comment out `size == -1`? (handled by OverflowHandler)
        if (size == -1 || size > resolveMaxSize(em.maxSize))
        {
            throw DSRuntimeExceptions.invalidArg("Entity too large: " + 
                    em.pipeSchema.wrappedSchema.messageFullName());
        }
        
        return size;
    }
    
    /**
     * Returns the serialized size of the message.
     */
    private static <T> int serTo(ProtostuffOutput output, T message, Schema<T> schema)
    {
        try
        {
            schema.writeTo(output, message);
            
            if(DSUtils.isBufferOverflow(output))
            {
                throw DSRuntimeExceptions.invalidArg("Entity too large: " + 
                        schema.messageFullName());
            }
            
            return output.getSize();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Serializing to a LinkedBuffer threw an IOException " + 
                    "(should never happen).", e);
        }
        finally
        {
            output.clear();
        }
    }
    
    /**
     * Returns the serialized size of the message.
     */
    public <T> int ser(T message, Schema<T> schema)
    {
        return serTo(serOutput, message, schema);
    }
    
    /**
     * Returns the serialized size of the message.
     */
    public <T> int serUdf(T message, Schema<T> schema)
    {
        return serTo(serUdfOutput, message, schema);
    }
    
    public <T> boolean pub(T message, Schema<T> schema)
    {
        boolean ok = 0 == pubOutput.getSize();
        if (ok)
            DSUtils.writeTo(pubOutput, message, schema);
        
        return ok;
    }
    
    /**
     * Returns the serialized size of the entity.
     */
    public <T> int ser(T entity, EntityMetadata<T> em)
    {
        final ProtostuffOutput output = serOutput;
        final Schema<T> schema = em.pipeSchema.wrappedSchema;
        try
        {
            schema.writeTo(output, entity);
            
            final int size = output.getSize();
            if (DSUtils.isBufferOverflow(output) || size > resolveMaxSize(em.maxSize))
            {
                throw DSRuntimeExceptions.invalidArg("Entity too large: " + 
                        schema.messageFullName());
            }
            
            return size;
        }
        catch (IOException e)
        {
            throw new RuntimeException("Serializing to a LinkedBuffer threw an IOException " + 
                    "(should never happen).", e);
        }
        finally
        {
            output.clear();
        }
    }
    
    // UTILS FOR READING DATA
    
    /**
     * Returns a new message that will be merged from the input.
     */
    public <T> T parseFrom(byte[] data, EntityMetadata<T> em)
    {
        return parseFrom(data, 0, data.length, em.pipeSchema.wrappedSchema);
    }
    
    /**
     * Returns a new message that will be merged from the input.
     */
    public <T> T parseFrom(byte[] data, Schema<T> schema)
    {
        return parseFrom(data, 0, data.length, schema);
    }
    
    /**
     * Returns a new message that will be merged from the input.
     */
    public <T> T parseFrom(byte[] data, int offset, int len, EntityMetadata<T> em)
    {
        return parseFrom(data, offset, len, em.pipeSchema.wrappedSchema);
    }
    
    /**
     * Returns a new message that will be merged from the input.
     */
    public <T> T parseFrom(byte[] data, int offset, int len, Schema<T> schema)
    {
        T message = schema.newMessage();
        mergeFrom(data, offset, len, message, schema);
        return message;
    }
    
    /**
     * Fill the message with the data.
     */
    public <T> void mergeFrom(byte[] data, int offset, int len, 
            T message, Schema<T> schema)
    {
        final DSByteArrayInput input = dsPipe.input.reset(data, offset, len);
        try
        {
            schema.mergeFrom(input, message);
            input.checkLastTagWas(0);
        }
        catch(ArrayIndexOutOfBoundsException ae)
        {
            throw new RuntimeException("Truncated.", ae);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Reading from a byte array threw an IOException (should " + 
                    "never happen).",e);
        }
        finally
        {
            input.clear();
        }
    }
    
    /**
     * Fill the message with the data.
     */
    public <T> void mergeFrom(byte[] data, T message, Schema<T> schema)
    {
        mergeFrom(data, 0, data.length, message, schema);
    }
    
    /**
     * Fill the entity with the data.
     */
    public <T> void mergeFrom(byte[] data, int offset, int len, 
            T entity, EntityMetadata<T> em)
    {
        mergeFrom(data, offset, len, entity, em.pipeSchema.wrappedSchema);
    }
    
    /**
     * Fill the entity with the data.
     */
    public <T> void mergeFrom(byte[] data, T entity, EntityMetadata<T> em)
    {
        mergeFrom(data, 0, data.length, entity, em.pipeSchema.wrappedSchema);
    }
}
