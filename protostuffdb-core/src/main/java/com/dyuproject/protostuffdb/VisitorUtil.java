//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.ArrayList;

/**
 * Visitor utils.
 * 
 * @author David Yu
 * @created Feb 19, 2013
 */
public final class VisitorUtil
{
    private VisitorUtil() {}
    
    public static <T> ArrayList<HasKV> listKV(EntityMetadata<T> em, 
            int limit, boolean desc, 
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        ArrayList<HasKV> list = new ArrayList<HasKV>();
        store.visitKind(em, limit, desc, null, parentKey, 
                context, Visitor.APPEND_KV, list);
        
        return list;
    }
    
    public static <T> ArrayList<byte[]> listK(EntityMetadata<T> em, 
            int limit, boolean desc,  
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        ArrayList<byte[]> list = new ArrayList<byte[]>();
        store.visitKind(em, limit, desc, null, parentKey, 
                context, Visitor.APPEND_KEY, list);
        
        return list;
    }
    
    public static <T> ArrayList<byte[]> listV(EntityMetadata<T> em, 
            int limit, boolean desc,  
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        ArrayList<byte[]> list = new ArrayList<byte[]>();
        store.visitKind(em, limit, desc, null, parentKey, 
                context, Visitor.APPEND_VALUE, list);
        
        return list;
    }
    
    public static <T> KV getKV(EntityMetadata<T> em, 
            boolean desc,  
            byte[] parentKey, 
            Datastore store, WriteContext context)
    {
        KV hkv = new KV();
        store.visitKind(em, 1, desc, null, parentKey, 
                context, Visitor.SET_KV, hkv);
        
        return hkv;
    }
    
    public static KV getKVLast(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return getKV(em, true, null, store, context);
    }
    
    public static KV getKVFirst(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return getKV(em, false, null, store, context);
    }
    
    public static byte[] getKLast(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return getKVLast(em, store, context).getKey();
    }
    
    public static byte[] getKFirst(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return getKVFirst(em, store, context).getKey();
    }
    
    public static byte[] getVLast(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return getKVLast(em, store, context).getValue();
    }
    
    public static byte[] getVFirst(EntityMetadata<?> em, 
            Datastore store, WriteContext context)
    {
        return getKVFirst(em, store, context).getValue();
    }
    
    // chain ops
    
    public static <T> ArrayList<HasKV> listKV(EntityMetadata<T> em, 
            int limit, boolean desc, 
            byte[] parentKey, 
            OpChain chain)
    {
        ArrayList<HasKV> list = new ArrayList<HasKV>();
        chain.vs().visitKind(em, limit, desc, null, parentKey, Visitor.APPEND_KV, list);
        
        return list;
    }
    
    public static <T> ArrayList<byte[]> listK(EntityMetadata<T> em, 
            int limit, boolean desc,  
            byte[] parentKey, 
            OpChain chain)
    {
        ArrayList<byte[]> list = new ArrayList<byte[]>();
        chain.vs().visitKind(em, limit, desc, null, parentKey, Visitor.APPEND_KEY, list);
        
        return list;
    }
    
    public static <T> ArrayList<byte[]> listV(EntityMetadata<T> em, 
            int limit, boolean desc,  
            byte[] parentKey, 
            OpChain chain)
    {
        ArrayList<byte[]> list = new ArrayList<byte[]>();
        chain.vs().visitKind(em, limit, desc, null, parentKey, Visitor.APPEND_VALUE, list);
        
        return list;
    }
    
    public static <T> KV getKV(EntityMetadata<T> em, 
            boolean desc,  
            byte[] parentKey, 
            OpChain chain)
    {
        KV hkv = new KV();
        chain.vs().visitKind(em, 1, desc, null, parentKey, Visitor.SET_KV, hkv);
        
        return hkv;
    }
    
    public static KV getKVLast(EntityMetadata<?> em, 
            OpChain chain)
    {
        return getKV(em, true, null, chain);
    }
    
    public static KV getKVFirst(EntityMetadata<?> em, 
            OpChain chain)
    {
        return getKV(em, false, null, chain);
    }
    
    public static byte[] getKLast(EntityMetadata<?> em, 
            OpChain chain)
    {
        return getKVLast(em, chain).getKey();
    }
    
    public static byte[] getKFirst(EntityMetadata<?> em, 
            OpChain chain)
    {
        return getKVFirst(em, chain).getKey();
    }
    
    public static byte[] getVLast(EntityMetadata<?> em, 
            OpChain chain)
    {
        return getKVLast(em, chain).getValue();
    }
    
    public static byte[] getVFirst(EntityMetadata<?> em, 
            OpChain chain)
    {
        return getKVFirst(em, chain).getValue();
    }
    
    // filters
    
    /*public static <T> KV getKVFromFilteredIndex(boolean filter, boolean desc, 
            EntityMetadata<T> em, 
            Datastore store, WriteContext context, int indexId)
    {
        KV hkv = new KV();
        store.visitIndexPrefixMatch(false, 1, desc, null, 
                context, Visitor.SET_KEY, hkv,  
                context.kb()
                .begin(indexId, em)
                .$append(filter)
                .$append(true)
                .push());
        
        return hkv;
    }
    
    public static <T> KV getKVFromFilteredIndex(boolean filter, boolean desc, 
            EntityMetadata<T> em, 
            OpChain chain, int indexId)
    {
        KV hkv = new KV();
        chain.vs().visitIndexPrefixMatch(false, 1, desc, null, 
                Visitor.SET_KEY, hkv,  
                chain.context.kb()
                .begin(indexId, em)
                .$append(filter)
                .$append(true)
                .push());
        
        return hkv;
    }*/

}
