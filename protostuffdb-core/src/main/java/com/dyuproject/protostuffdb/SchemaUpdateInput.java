//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;
import java.util.Arrays;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.EnumMapping;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;

/**
 * Compares the two inputs and writes the diff.
 * 
 * @author David Yu
 * @created Sep 3, 2012
 */
public final class SchemaUpdateInput implements Input
{
    
    /**
     * The exception thrown before/during schema update.
     */
    public static final class UpdateException extends RuntimeException
    {

        private static final long serialVersionUID = 1L;
        
        public UpdateException(String msg)
        {
            super(msg);
        }
        
        public UpdateException(String msg, Throwable cause)
        {
            super(msg, cause);
        }
    }
    
    final DSByteArrayInput input1, input2;
    
    final int[] //fields1 = new int[128], fields2 = new int[128], 
            fieldOffsetAddIdx = new int[128], 
            fieldVerifyAddIdx = new int[128], 
            fieldOffsetRemoveIdx = new int[128], 
            fieldVerifyRemoveIdx = new int[128];
    
    int fields1Count, fields2Count, addCount, removeCount, changes;
    
    private int state = 0, f1, f2;
    
    public final Pipe pipe = new Pipe()
    {

        @Override
        protected Input begin(Schema<?> pipeSchema) throws IOException
        {
            return SchemaUpdateInput.this;
        }

        @Override
        protected void end(Schema<?> pipeSchema, Input input, boolean cleanupOnly)
                throws IOException
        {
            input1.reset(null, 0, 0);
            input2.reset(null, 0, 0);
            
            reset();
        }
        
    };
    
    public SchemaUpdateInput(DSByteArrayInput input1, DSByteArrayInput input2)
    {
        this.input1 = input1;
        this.input2 = input2;
    }
    
    public SchemaUpdateInput init(EntityMetadata<?> root1, EntityMetadata<?> root2)
    {
        if(root1.kind != root2.kind)
            throw new UpdateException("Both kinds do not match.");
        
        fields1Count = 0;
        fields2Count = 0;
        addCount = 0;
        removeCount = 0;
        changes = 0;
        
        return this;
    }
    
    public SchemaUpdateInput clear()
    {
        
        return this;
    }
    
    public boolean isAdded(int field)
    {
        return field == fieldVerifyAddIdx[fieldOffsetAddIdx[field]];
    }
    
    public boolean isRemoved(int field)
    {
        return field == fieldVerifyRemoveIdx[fieldOffsetRemoveIdx[field]];
    }
    
    public <T> void handleUnknownField(int fieldNumber, Schema<T> schema) throws IOException
    {
        if(state != 1)
        {
            throw new UpdateException("The current schema (v1) provided is not " +
                    "compatible with the data from datastore.");
        }
        
        // we are removing this field
        changes++;
        
        fieldOffsetRemoveIdx[fieldNumber] = removeCount;
        fieldVerifyRemoveIdx[removeCount++] = fieldNumber;
        
        input1.handleUnknownField(fieldNumber, schema);
    }
    
    public <T> int readFieldNumber(Schema<T> schema) throws IOException
    {
        switch(state)
        {
            case 0:
                // happens on:
                // - init
                // - after merge
                f1 = input1.readFieldNumber(schema);
                f2 = input2.readFieldNumber(schema);
                
                //fields1[fields1Count++] = f1;
                //fields2[fields2Count++] = f2;
                break;
                
            case 1:
                // when input1 has a smaller number
                f1 = input1.readFieldNumber(schema);
                
                //fields1[fields1Count++] = f1;
                break;
                
            case 2:
                // when input2 has a smaller number
                f2 = input2.readFieldNumber(schema);
                
                //fields2[fields2Count++] = f2;
                break;
                 
            default:
                throw new UpdateException("Should not happen.");
        }
        
        if(f1 == f2)
        {
            state = 0;
            return f2;
        }
        
        if(f1 < f2 || f2 == 0)
        {
            state = 1;
            return f1;
        }
        
        state = 2;
        return f2;
    }

    public int readInt32() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final int v1 = input1.readInt32(), v2 = input2.readInt32();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                		"have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readInt32();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public int readUInt32() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final int v1 = input1.readUInt32(), v2 = input2.readUInt32();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readUInt32();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public int readSInt32() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final int v1 = input1.readSInt32(), v2 = input2.readSInt32();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readSInt32();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public int readFixed32() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final int v1 = input1.readFixed32(), v2 = input2.readFixed32();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readFixed32();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public int readSFixed32() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final int v1 = input1.readSFixed32(), v2 = input2.readSFixed32();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readSFixed32();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public long readInt64() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final long v1 = input1.readInt64(), v2 = input2.readInt64();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readInt64();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public long readUInt64() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final long v1 = input1.readUInt64(), v2 = input2.readUInt64();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readUInt64();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public long readSInt64() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final long v1 = input1.readSInt64(), v2 = input2.readSInt64();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readSInt64();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public long readFixed64() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final long v1 = input1.readFixed64(), v2 = input2.readFixed64();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readFixed64();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public long readSFixed64() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final long v1 = input1.readSFixed64(), v2 = input2.readSFixed64();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readSFixed64();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public float readFloat() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final float v1 = input1.readFloat(), v2 = input2.readFloat();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readFloat();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public double readDouble() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final double v1 = input1.readDouble(), v2 = input2.readDouble();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readDouble();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public boolean readBool() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final boolean v1 = input1.readBool(), v2 = input2.readBool();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readBool();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public int readEnum() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final int v1 = input1.readEnum(), v2 = input2.readEnum();
                if(v1 != v2)
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readEnum();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }
    
    public int readEnumIdx(EnumMapping mapping) throws IOException
    {
        Integer idx = mapping.numberIdxMap.get(readEnum());
        return idx == null ? -1 : idx.intValue();
    }

    public String readString() throws IOException
    {
        throw new UpdateException("Update schema only works with byte array values.");
    }

    public ByteString readBytes() throws IOException
    {
        throw new UpdateException("Update schema only works with byte array values.");
    }

    public byte[] readByteArray() throws IOException
    {
        switch(state)
        {
            case 0:
            {
                // field could be updated
                final byte[] v1 = input1.readByteArray(), v2 = input2.readByteArray();
                if(!Arrays.equals(v1, v2))
                {
                    changes++;
                    
                    fieldOffsetRemoveIdx[f2] = removeCount;
                    fieldVerifyRemoveIdx[removeCount++] = f2;
                    
                    fieldOffsetAddIdx[f2] = addCount;
                    fieldVerifyAddIdx[addCount++] = f2;
                }
                
                return v2;
            }
                
            case 1:
                throw new UpdateException("The current schema (v2) provided did not " +
                        "have sorted fields for serialization.");
            
            case 2:
                // field added
                changes++;
                
                fieldOffsetAddIdx[f2] = addCount;
                fieldVerifyAddIdx[addCount++] = f2;
                
                return input2.readByteArray();
            
            default:
                throw new RuntimeException("Should not happen.");
        }
    }

    public <T> T mergeObject(T value, Schema<T> schema) throws IOException
    {
        throw new UpdateException("SchemaUpdateInput::mergeObject not supported.");
    }

    public void transferByteRangeTo(Output output, boolean utf8String, int fieldNumber,
            boolean repeated) throws IOException
    {
        final byte[] value = readByteArray();
        output.writeByteRange(utf8String, fieldNumber, value, 0, value.length, repeated);
    }
    
    public void transferEnumTo(Output output, EnumMapping mapping, int fieldNumber,
            boolean repeated) throws IOException
    {
        output.writeEnum(fieldNumber, readEnum(), repeated);
    }
}
