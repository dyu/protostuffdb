//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.RpcResponse;
import com.dyuproject.protostuff.Schema;

/**
 * TODO
 * 
 * @author David Yu
 * @created Jul 15, 2015
 */
public abstract class HasIdAndKVCache<T extends HasIdAndKV> implements Cache,
    AbstractAppendOnlyList.Listener<T>, Visitor<WriteContext> 
{
    public final int kind;
    public final AbstractAppendOnlyList<T> list;
    
    public HasIdAndKVCache(EntityMetadata<?> em, boolean serial, int segmentSize)
    {
        this(em.kind, serial, segmentSize);
    }
    
    public HasIdAndKVCache(int kind, boolean serial, int segmentSize)
    {
        this.kind = kind;
        this.list = serial ? new SerialAppendOnlyList<T>(segmentSize, this) : 
            new ConcurrentAppendOnlyList<T>(segmentSize, this);
    }
    
    public abstract T newEntry(byte[] key, byte[] value);
    
    public void assignIndexTo(T item, int index)
    {
        item.setId(index + 1);
    }
    
    public int newId()
    {
        return 1 + list.size();
    }
    
    public T get(int id)
    {
        return list.get(id - 1);
    }
    
    public int add(T entity, byte[] key, long now, WriteContext context)
    {
        final int idx = list.add(entity);
        context.fillIndexedEntityKey(key, kind, now, idx);
        return idx;
    }
    
    public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
            WriteContext param, int index)
    {
        list.fill(newEntry(KeyUtil.extractFrom(key), ValueUtil.copy(v, voffset, vlen)));
        
        return false;
    }
    
    public <V> int visit(Visitor<V> visitor, V v)
    {
        return AbstractAppendOnlyList.visit(list, visitor, v);
    }

    public <V> int visit(Visitor<V> visitor, V v, int index)
    {
        return AbstractAppendOnlyList.visit(list, visitor, v, index);
    }
    
    public int writeTo(final Output output, 
            final Schema<T> schema, int fieldNumber) throws IOException
    {
        return AbstractAppendOnlyList.writeTo(output, schema, fieldNumber, list);
    }
    
    public int writeTo(final Output output, 
            final Schema<T> schema, int fieldNumber, int lastSeenId) throws IOException
    {
        return AbstractAppendOnlyList.writeTo(output, schema, fieldNumber, list, lastSeenId);
    }
    
    public int pipeTo(RpcResponse res)
    {
        return visit(RpcResponse.PIPED_VISITOR, res);
    }
    
    public int pipeTo(RpcResponse res, int index)
    {
        return visit(RpcResponse.PIPED_VISITOR, res, index);
    }

    public int size()
    {
        return list.size();
    }
    
    public void clear()
    {
        list.clear();
    }
}
