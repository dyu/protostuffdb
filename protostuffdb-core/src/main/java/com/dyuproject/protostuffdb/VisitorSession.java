//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;

import com.dyuproject.protostuff.KeyBuilder;

/**
 * Bulk data collection via the Visitor interface.
 *
 * @author David Yu
 * @created Aug 19, 2011
 */
public abstract class VisitorSession
{
    
    /**
     * The procedure that contains the logic in collecting data.
     */
    public interface Handler<P>
    {
        public void handle(VisitorSession session, P param);
    }
    
    /**
     * Basic get.
     */
    public final byte[] get(String key, WriteContext context)
    {
        byte[] prefixedKey = KeyUtil.newPrefixedKey(key, context);
        return rawGet(prefixedKey, 0, prefixedKey.length);
    }
    
    /**
     * Basic get.
     */
    public final byte[] get(byte[] key)
    {
        byte[] prefixedKey = KeyUtil.newPrefixedKey(key);
        return rawGet(prefixedKey, 0, prefixedKey.length);
    }
    
    /**
     * Basic check if key exists.
     */
    public final boolean exists(String key, WriteContext context)
    {
        byte[] prefixedKey = KeyUtil.newPrefixedKey(key, context);
        return rawExists(prefixedKey, 0, prefixedKey.length);
    }
    
    /**
     * Basic check if key exists.
     */
    public final boolean exists(byte[] key)
    {
        return rawExists(KeyUtil.newPrefixedKey(key), 0, 1+key.length);
    }
    
    /**
     * Raw get.
     */
    public final byte[] rawGet(byte[] key)
    {
        return rawGet(key, 0, key.length);
    }
    
    /**
     * Raw check if key exists.
     */
    public final boolean rawExists(byte[] key)
    {
        return rawExists(key, 0, key.length);
    }
    
    /**
     * Raw get.
     */
    public abstract byte[] rawGet(byte[] key, int koffset, int klen);
    
    /**
     * Raw check if key exists.
     */
    public abstract boolean rawExists(byte[] key, int koffset, int klen);
    
    /* ================================================== */
    
    /**
     * Raw check if key exists via iterator.
     */
    public final boolean exists(boolean prefix, KeyBuilder kb)
    {
        return exists(prefix, kb, true);
    }
    
    /**
     * Returns true if key exists.
     */
    public final boolean get(HasKV kv, KeyBuilder kb)
    {
        return get(kv, kb, true);
    }
    
    /**
     * Returns true if key prefix exists.
     */
    public final boolean pget(HasKV kv, KeyBuilder kb)
    {
        return pget(kv, true, false, kb, true);
    }
    
    /**
     * Returns true if key prefix exists.
     */
    public final boolean pget(HasKV kv, boolean includeValue, boolean extractEntityKey, 
            KeyBuilder kb)
    {
        return pget(kv, includeValue, extractEntityKey, kb, true);
    }
    
    /**
     * Raw check if key exists via iterator.
     */
    public abstract boolean exists(boolean prefix, KeyBuilder kb, boolean pop);
    
    /**
     * Returns true if key exists.
     */
    public abstract boolean get(HasKV kv, 
            KeyBuilder kb, boolean pop);
    
    /**
     * Returns true if key exists.
     */
    public abstract boolean pget(HasKV kv, 
            boolean includeValue, boolean extractEntityKey, 
            KeyBuilder kb, boolean pop);
    
    /* ================================================== */
    
    /**
     * Returns true if the entity key exists.
     * 
     * @param parentKey is optional.
     */
    public abstract <T> boolean exists(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey);
    
    /**
     * Returns the raw value.
     * 
     * @param parentKey is optional.
     */
    public abstract <T> byte[] get(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey);
    
    /**
     * Visits the raw value.
     * 
     * Returns false if the key does not exist.
     * 
     * @param parentKey is optional.
     */
    public <T,V> boolean visitKey(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            Visitor<V> visitor, V param)
    {
        final byte[] value = get(key, em, parentKey);
        if(value == null)
            return false;
        
        visitor.visit(key, value, 0, value.length, param, 0);
        return true;
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * @param reverse visits the keys starting from the last index (if true).
     * @param parentKey is optional.
     */
    public final <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            Visitor<V> visitor, V param)
    {
        return visitKeys(keys, reverse, false, visitor, param);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * @param reverse visits the keys starting from the last index (if true).
     * @param extractEntityKey extracts the entity key and use that.
     * @param parentKey is optional.
     */
    public abstract <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            boolean extractEntityKey, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities visited.
     * 
     * @param reverse visits the keys starting from the last index (if true).
     * @param extractEntityKey extracts the entity key and use that.
     * @param parentKey is optional.
     */
    public abstract <V> int visitHasKeys(List<? extends HasKey> keys, boolean reverse, 
            boolean extractEntityKey, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities (by kind) visited.
     * 
     * @param startKey is optional.
     * @param parentKey is optional.
     */
    public abstract <T,V> int visitKind(EntityMetadata<T> em, 
            int limit, boolean desc, 
            byte[] startKey, byte[] parentKey, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities visited.
     * 
     * Scans the database in either asc or desc order.
     */
    public abstract <V> int scan(boolean keysOnly, int limit, boolean desc, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public final <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        return visitRange(keysOnly, limit, desc, rawStartKey, visitor, param, kb, true);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public final <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop)
    {
        final int skoffset = kb.offset(), 
                sklen = pop ? kb.popLen() : kb.len();
        final byte[] sk = kb.buf(), 
                ek = new byte[sklen + 1];
        
        System.arraycopy(sk, skoffset, ek, 0, sklen);
        // include 1 byte for the range
        sk[skoffset + sklen] = 0;
        ek[sklen] = (byte)0xFF;
        
        return visitRange(keysOnly, limit, desc, 
                rawStartKey, 
                visitor, param, 
                sk, skoffset, ek.length, 
                ek, 0, ek.length);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public final <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            byte[] sk, 
            byte[] ek)
    {
        return visitRange(keysOnly, limit, desc, 
                rawStartKey, 
                visitor, param, 
                sk, 0, sk.length, 
                ek, 0, ek.length);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen)
    {
        return visitRange(keysOnly, limit, desc,
                rawStartKey,
                visitor, param,
                false, false,
                sk, skoffset, sklen,
                ek, ekoffset, eklen);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * If skRelativeLimit is true, only entry keys that have the same length as the 
     * start key would count towards the limit, if limited.
     * 
     * rawStartKey is optional.
     */
    public abstract <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param,
            boolean valueAsKey, boolean skRelativeLimit,
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen);
    
    /**
     * Visit the index that matches the composite key.
     */
    public final <V> int visitIndex(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        return visitIndex(keysOnly, limit, desc, rawStartKey, visitor, param, kb, true);
    }
    
    /**
     * Visit the index that matches the composite key.
     */
    public abstract <V> int visitIndex(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop);
    /*{
        final byte[] buf = kb.buf();
        final int offset = kb.offset(), len = kb.popLen();
        
        // append 0x00
        buf[offset+len] = 0;
        final byte[] sk = ValueUtil.copy(buf, offset, len+1);
        
        // append 0x01
        buf[offset+len] = (byte)1;
        //final byte[] ek = ValueUtil.copy(buf, offset, len+1);
        
        return visitRange(keysOnly, limit, desc, rawStartKey, visitor, param, 
                sk, 0, sk.length, 
                buf, offset, len+1);
    }*/
    
    /*public final <V> int visitIndexPrefixMatch(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        final byte[] buf = kb.buf();
        final int offset = kb.offset(), len = kb.popLen();
        
        // append 0x0FF
        buf[offset+len] = (byte)0xFF;
        
        return visitRange(keysOnly, limit, desc, rawStartKey, visitor, param, 
                buf, offset, len, 
                buf, offset, len+1);
    }*/
}
