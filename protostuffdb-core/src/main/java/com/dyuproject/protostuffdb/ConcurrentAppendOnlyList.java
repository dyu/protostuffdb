//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Append only list for n threads that synchronizes every time the remaining slots 
 * of a segment is less than or equal to the {@link #threadCount} (defaults to 2).
 * 
 * TODO make {@link #segments} into a volatile array and incorporate 
 * double-check-before-sync? Or make {@link #segments} at atomic reference?
 * 
 * @author David Yu
 * @created Aug 23, 2014
 */
public final class ConcurrentAppendOnlyList<E> extends AbstractAppendOnlyList<E>
{
    
    final AtomicInteger size = new AtomicInteger(0);
    final int threadCount;
    
    public ConcurrentAppendOnlyList(int segmentSize, Listener<E> listener)
    {
        this(segmentSize, listener, 2);
    }
    
    public ConcurrentAppendOnlyList(int segmentSize, Listener<E> listener, int threadCount)
    {
        super(segmentSize, listener);
        this.threadCount = threadCount;
        segments[0] = listener.newArray(segmentSize);
        segmentsLen = 1;
    }
    
    public int size()
    {
        return size.get();
    }

    public int add(E item)
    {
        final int index = size.getAndIncrement(),
                segmentIdx = index / segmentSize,
                offset = index % segmentSize,
                remaining = segmentSize - offset;
        
        if (remaining <= threadCount)
        {
            // either of the last two slots
            // we sync both threads so that the slot on the 
            // next new segment is readily writable since we
            // pre-create it here.
            synchronized (this)
            {
                if (segmentIdx + 1 == segmentsLen)
                {
                    // this is the first thread to get the sync
                    // append new segment (the other thread won't have to)
                    Object[] segments = this.segments,
                            newSegments = appendTo(segments, segmentsLen++, 
                                    listener.newArray(segmentSize));
                    
                    if (segments != newSegments)
                        this.segments = newSegments;
                }
            }
        }
        
        set(segmentIdx, offset, item);
        
        listener.assignIndexTo(item, index);
        
        return index;
    }
    
    /**
     * Only use this on startup (not at runtime) and only one thread should call 
     * this at a time.
     */
    int fill(E item)
    {
        final int index = size.get(),
                segmentIdx = index / segmentSize,
                offset = index % segmentSize,
                remaining = segmentSize - offset;
        
        size.set(index + 1);
        
        if (remaining == threadCount)
        {
            // append new segment (the other thread won't have to)
            Object[] segments = this.segments,
                    newSegments = appendTo(segments, segmentsLen++, 
                            listener.newArray(segmentSize));
            
            if (segments != newSegments)
                this.segments = newSegments;
        }
        
        set(segmentIdx, offset, item);
        
        listener.assignIndexTo(item, index);
        
        return index;
    }

    @Override
    public void clear()
    {
        size.set(0);
        segments[0] = listener.newArray(segmentSize);
        // or clear only the first element for tests
        //set(0, 0, null);
    }
}
