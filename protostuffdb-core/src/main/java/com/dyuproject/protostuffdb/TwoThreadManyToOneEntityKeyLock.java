//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

/**
 * Many to One entity key lock.
 * When 2 threads operate on a single key, a {@link TwoThreadEntityKeyLock} must be 
 * acquired before this.
 * 
 * @author David Yu
 * @created Feb 24, 2013
 */
public final class TwoThreadManyToOneEntityKeyLock implements ManyToOneEntityKeyLock
{
    
    // 10 by default
    static final int BUSY_SPIN_CYCLE_LIMIT = Math.max(10, 
            Integer.getInteger("ttc.busy_spin_cycles", 0));

    static final int PARK_CYCLE_LIMIT = BUSY_SPIN_CYCLE_LIMIT + 5;
    
    static final int YIELD_CYCLE_LIMIT = PARK_CYCLE_LIMIT + 5;
    
    static final int KEY = 1, KEY2 = 2, MULTI_KEY = 3;//, MULTI_KEY2 = 4;
    
    /**
     * Put the 64-bit value of the keys to the long array.
     */
    private static int putKeysTo(final long[] values, int offset, 
            final List<byte[]> keys, final List<byte[]> moreKeys)
    {
        for(byte[] key : keys)
            values[offset++] = ValueUtil.toInt64(key, key.length-8);
        
        if(moreKeys == null)
            return keys.size();
        
        for(byte[] key : moreKeys)
            values[offset++] = ValueUtil.toInt64(key, key.length-8);
        
        return keys.size() + moreKeys.size();
    }
    
    /**
     * Put the 64-bit value of the keys to the long array.
     */
    private static int putHasKeysTo(final long[] values, int offset, 
            final List<? extends HasKey> keys, final List<? extends HasKey> moreKeys)
    {
        byte[] key;
        for(HasKey hk : keys)
        {
            key = hk.getKey();
            values[offset++] = ValueUtil.toInt64(key, key.length-8);
        }
        
        if(moreKeys == null)
            return keys.size();
        
        for(HasKey hk : moreKeys)
        {
            key = hk.getKey();
            values[offset++] = ValueUtil.toInt64(key, key.length-8);
        }
        
        return keys.size() + moreKeys.size();
    }
    
    /**
     * Put the 64-bit value of the keys to the long array.
     */
    private static int putHasParentKeysTo(final long[] values, int offset, 
            final List<? extends HasParentKey> keys, final List<? extends HasParentKey> moreKeys)
    {
        byte[] key;
        for(HasParentKey hk : keys)
        {
            key = hk.getParentKey();
            values[offset++] = ValueUtil.toInt64(key, key.length-8);
        }
        
        if(moreKeys == null)
            return keys.size();
        
        for(HasParentKey hk : moreKeys)
        {
            key = hk.getParentKey();
            values[offset++] = ValueUtil.toInt64(key, key.length-8);
        }
        
        return keys.size() + moreKeys.size();
    }
    
    private final AtomicInteger atomicType = new AtomicInteger(0), 
            atomicLen = new AtomicInteger(0);
    
    private volatile long value = 0;
    
    /**
     * Same length as {@link WriteContext#varint64Idx}.
     */
    private final long[] values = new long[64];

    // TODO higher loop count on this scheme? (waiting on multi key updates)
    public int acquire(byte[] key)
    {
        return acquire(ValueUtil.toInt64(key, key.length-8));
    }
    
    /**
     * @param value should be deserialized from a key (LSB 8 bytes of the key).
     */
    public int acquire(final long value)
    {
        outer:
        for(int count = 0;;)
        {
            if(atomicType.compareAndSet(0, KEY))
            {
                this.value = value;
                return KEY;
            }
            
            if(count == 0)
            {
                if(atomicType.compareAndSet(KEY, KEY2))
                {
                    // the other thread is not yet finished
                    // but proceed anyway because the other is pre-filtered
                    // to not be equal to this key
                    this.value = value;
                    return KEY2;
                }
                
                final int len = atomicLen.get();
                if(atomicLen.compareAndSet(len, len) && len == 0)
                {
                    // the other thread is either:
                    // - almost finished (multi-key pending update to atomicType)
                    // - starting (multi-key)
                    // - finished (key)
                    continue;
                }
                
                // the other thread not finished
                final long[] values = this.values;
                for(int j = 0; j < len; j++)
                {
                    if(value == values[j])
                    {
                        // clash
                        count++;
                        continue outer;
                    }
                }
                
                // no clash
                if(atomicType.compareAndSet(MULTI_KEY, KEY))
                {
                    // other thread is not finished but proceed anyway since there is 
                    // no clash
                    this.value = value;
                    return KEY;
                }
                
                // the other thread just finished
                // so we start all over again to see if we get to the atomicType first
                count = 0;
                continue;
            }
            
            if(++count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if(count > YIELD_CYCLE_LIMIT)
                    count = 0;
                else if(count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }
    
    /**
     * Downcast it again if the receiver used a long value.
     */
    public void release(final long type)
    {
        release((int)type);
    }
    
    public void release(final int type)
    {
        if(type < 0)
        {
            // its the length
            atomicLen.compareAndSet(-type, 0);
            atomicType.compareAndSet(MULTI_KEY, 0);
        }
        else
        {
            // single type
            atomicType.compareAndSet(type, 0);
        }
    }
    
    private int acquire(int len, WriteContext context)
    {
        outer:
        for(int count = 0;;)
        {
            if(atomicType.compareAndSet(0, MULTI_KEY))
            {
                // no concurrent ops
                
                // happens before
                System.arraycopy(values, 0, context.varint64Idx, 0, len);
                
                if(!atomicLen.compareAndSet(0, len))
                    throw new IllegalStateException(getClass().getSimpleName());
                
                return -len;
            }
            
            if(count == 0)
            {
                if(atomicType.compareAndSet(KEY, MULTI_KEY))
                {
                    // other thread not yet finished
                    
                    // happens before
                    final long[] values = this.values;
                    System.arraycopy(values, 0, context.varint64Idx, 0, len);
                    
                    if(!atomicLen.compareAndSet(0, len))
                        throw new IllegalStateException(getClass().getSimpleName());
                    
                    final long value = this.value;
                    for(int j = 0; j < len; j++)
                    {
                        if(value == values[j])
                        {
                            // clash
                            count++;
                            continue outer;
                        }
                    }
                    
                    // no clash
                    return -len;
                }
                
                /*if(atomicType.compareAndSet(MULTI_KEY, MULTI_KEY2))
                {
                    // might be too complex to implement
                    // needs to return an ORed compound integer (len and type)
                    count++;
                    continue;
                }*/
                
                if(atomicType.compareAndSet(MULTI_KEY, MULTI_KEY))
                {
                    // serial ... wait for the other thread to finish
                    count++; // TODO set to PARK_CYCLE_LIMIT to start yielding?
                    continue;
                }
                
                // the other thread just finished
                // so we start all over again to see if we get to the atomicType first
                count = 0;
                continue;
            }
            
            if(++count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if(count > YIELD_CYCLE_LIMIT) // TODO endless yield?
                    count = 0;
                else if(count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }
    
    /**
     * @param moreKeys optional.
     */
    public int acquire(List<byte[]> keys, List<byte[]> moreKeys, WriteContext context)
    {
        return acquire(putKeysTo(context.varint64Idx, 0, keys, moreKeys), context);
    }
    
    public int acquireHK(HasKey hk)
    {
        return acquire(hk.getKey());
    }
    
    public int acquireHPK(HasParentKey hpk)
    {
        return acquire(hpk.getParentKey());
    }
    
    /**
     * @param moreKeys optional.
     */
    public int acquireHK(List<? extends HasKey> keys, List<? extends HasKey> moreKeys, WriteContext context)
    {
        return acquire(putHasKeysTo(context.varint64Idx, 0, keys, moreKeys), context);
    }
    
    /**
     * @param moreKeys optional.
     */
    public int acquireHPK(List<? extends HasParentKey> keys, List<? extends HasParentKey> moreKeys, WriteContext context)
    {
        return acquire(putHasParentKeysTo(context.varint64Idx, 0, keys, moreKeys), context);
    }
    
    /*public static void main(String[] args)
    {
        int x = -2;
        long y = x;
        int z = (int)y;
        System.err.println(z);
        
        long a = 0xFFFFFFFFFFl;
        int b = (int)a;
        System.err.println(b);
    }*/
    
}
