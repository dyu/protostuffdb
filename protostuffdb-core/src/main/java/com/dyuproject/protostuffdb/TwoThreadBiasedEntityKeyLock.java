//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.TwoThreadEntityKeyLock.BUSY_SPIN_CYCLE_LIMIT;
import static com.dyuproject.protostuffdb.TwoThreadEntityKeyLock.PARK_CYCLE_LIMIT;
import static com.dyuproject.protostuffdb.TwoThreadEntityKeyLock.YIELD_CYCLE_LIMIT;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.LockSupport;

/**
 * The thread that calls {@link #acquire()} will cause the other thread to wait until 
 * finished.
 * 
 * The other thread waiting on a thread that called the same method: {@link #acquire(long)} 
 * does not have to wait for finish if they are not operating on the same entity. 
 * 
 * @author David Yu
 * @created May 17, 2016
 */
public final class TwoThreadBiasedEntityKeyLock implements BiasedEntityKeyLock
{
    private final AtomicLong atomicValue = new AtomicLong(0);
    
    private static final long BIASED = Long.MAX_VALUE - 1; 

    public long acquire()
    {
        final long value = Long.MAX_VALUE;
        
        if (atomicValue.compareAndSet(0, value))
            return value;
        
        int count = 0;
        for (long expect = atomicValue.get();; expect = atomicValue.get())
        {
            // waits for the others to completely finish
            if (expect == 0 && atomicValue.compareAndSet(expect, value))
                return value;
            
            if (++count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if (count > YIELD_CYCLE_LIMIT)
                    count = 0;
                else if (count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }
    
    /**
     * Only waits for the thread that called {@link #acquire()}.
     * This should be called by a thread that does not perform updates on a key.
     */
    public long acquireBiased()
    {
        final long value = BIASED;
        
        if (atomicValue.compareAndSet(0, value) || atomicValue.compareAndSet(value, value))
            return value;
        
        int count = 0;
        for (long expect = atomicValue.get();; expect = atomicValue.get())
        {
            if (expect != Long.MAX_VALUE && atomicValue.compareAndSet(expect, value))
                return value;
            
            if (++count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if (count > YIELD_CYCLE_LIMIT)
                    count = 0;
                else if (count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }
    
    public long acquire(byte[] key)
    {
        return acquire(ValueUtil.toInt64(key, key.length-8));
    }
    
    public long acquire(final long value)
    {
        if (atomicValue.compareAndSet(0, value))
            return value;
        
        int count = 0;
        for (long expect = atomicValue.get();; expect = atomicValue.get())
        {
            if (expect != Long.MAX_VALUE && value != expect && atomicValue.compareAndSet(expect, value))
                return value;
            
            // could be operating on same key
            // wait for (expect != hash) <- wait for the other thread to finish
            
            if (++count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if (count > YIELD_CYCLE_LIMIT)
                    count = 0;
                else if (count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }

    public boolean release(long value)
    {
        return atomicValue.compareAndSet(value, 0);
    }

}
