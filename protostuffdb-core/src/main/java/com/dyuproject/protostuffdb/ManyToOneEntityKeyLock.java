//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;

/**
 * TODO
 * 
 * @author David Yu
 * @created Apr 3, 2017
 */
public interface ManyToOneEntityKeyLock
{
    
    /**
     * Acquire the lock.
     */
    int acquire(byte[] key);

    /**
     * Acquire the lock.
     */
    int acquire(long value);
    
    /**
     * Release the lock.
     */
    void release(long type);
    
    /**
     * Acquire the lock for multiple keys.
     */
    int acquire(List<byte[]> keys, List<byte[]> moreKeys, WriteContext context);
    
    /**
     * Release the lock.
     */
    void release(int type);
    
    int acquireHK(HasKey hk);
    
    int acquireHPK(HasParentKey hpk);
    
    int acquireHK(List<? extends HasKey> keys, 
            List<? extends HasKey> moreKeys, WriteContext context);
    
    int acquireHPK(List<? extends HasParentKey> keys, 
            List<? extends HasParentKey> moreKeys, WriteContext context);
}
