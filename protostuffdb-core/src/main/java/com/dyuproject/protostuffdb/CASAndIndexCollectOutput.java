//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuff.ProtobufOutput.encodeZigZag32;
import static com.dyuproject.protostuff.ProtobufOutput.encodeZigZag64;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_FIXED32;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_FIXED64;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_LENGTH_DELIMITED;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_VARINT;
import static com.dyuproject.protostuff.WireFormat.makeTag;

import java.io.IOException;
import java.io.OutputStream;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.EnumMapping;
import com.dyuproject.protostuff.Input;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.WriteSession;
import com.dyuproject.protostuff.ds.CAS;
import com.dyuproject.protostuffdb.WriteContext.OverflowHandler;

/**
 * A protostuff output which keeps track of the fields that have been 
 * modified (possibly re-index them), and at the same time serializing the message.
 *
 * @author David Yu
 * @created Mar 16, 2011
 */
public final class CASAndIndexCollectOutput extends WriteSession implements Output, 
    Input, CAS.Query
{
    
    final DSByteArrayInput input;
    private final int[] fieldCASIdx;
    private final int[] fieldUpdateIdx;
    
    final int[] fieldOffsetAddIdx;
    final int[] fieldOffsetRemoveIdx;
    
    final int[] fieldsTracked;
    int trackCount;
    
    final int[] fieldsApplied;
    int casCount;
    
    final int[] varint32Idx;
    final long[] varint64Idx;
    
    private int offset32;
    private int offset64;
    
    final OverflowHandler handler;
    
    private EntityMetadata<?> root;
    private CAS cas;
    private boolean entityField;
    
    final Pipe pipe = new Pipe()
    {
        @Override
        protected Input begin(Pipe.Schema<?> pipeSchema) throws IOException
        {
            // delegation
            return CASAndIndexCollectOutput.this;
        }
        
        @Override
        protected void end(Pipe.Schema<?> pipeSchema, Input input,
                boolean cleanupOnly) throws IOException
        {
            // reset
            output = null;
            
            CASAndIndexCollectOutput.this.clear();
        }
    };
    
    public CASAndIndexCollectOutput(
            final DSByteArrayInput input,
            int[] fieldCASIdx, 
            int[] fieldUpdateIdx, 
            
            int[] fieldOffsetAddIdx, 
            int[] fieldOffsetRemoveIdx, 
            int[] fieldsTracked, 
            int[] fieldsApplied, 
            
            int[] varint32Idx,
            long[] varint64Idx, 
            LinkedBuffer buffer, OutputStream out, 
            OverflowHandler handler)
    {
        super(buffer, out);
        
        this.input = input;
        this.fieldCASIdx = fieldCASIdx;
        this.fieldUpdateIdx = fieldUpdateIdx;
        
        this.fieldOffsetAddIdx = fieldOffsetAddIdx;
        this.fieldOffsetRemoveIdx = fieldOffsetRemoveIdx;
        this.fieldsTracked = fieldsTracked;
        this.fieldsApplied = fieldsApplied;
        
        this.varint32Idx = varint32Idx;
        this.varint64Idx = varint64Idx;
        
        this.handler = handler;
    }
    
    public CASAndIndexCollectOutput init(EntityMetadata<?> root, CAS cas, 
            byte[] data, int offset, int len)
    {
        this.root = root;
        this.cas = cas;
        
        trackCount = 0;
        
        offset32 = 1;
        offset64 = 0;
        
        entityField = true;
        
        casCount = 0;
        
        cas.fill(fieldCASIdx);
        
        input.reset(data, offset, len);
        
        // reset on init
        size = 0;
        
        return this;
    }
    
    public CASAndIndexCollectOutput clear()
    {
        if(tail != head)
        {
            // overflow
            size = -1;
        }
        
        // what super.clear() does, except the size is not reset
        tail = head.clear();
        
        // unset for gc
        
        root = null;
        cas = null;
        
        return this;
    }
    
    public boolean isEnumsByName()
    {
        return false;
    }
    
    public int[] getAppliedFields()
    {
        return fieldsApplied;
    }
    
    public int getAppliedFieldCount()
    {
        return casCount;
    }
    
    public boolean isUpdated(int field, CAS cas)
    {
        final CAS.Op<?> op = cas.getOp(fieldUpdateIdx[field]);
        return op != null && op.f == field && !op.noop && !op.isPrecondition(cas, this);
    }
    
    public boolean isPrecondition(int field, CAS cas)
    {
        final CAS.Op<?> op = cas.getOp(fieldUpdateIdx[field]);
        return op != null && op.f == field && (op.noop || op.isPrecondition(cas, this));
    }
    
    @SuppressWarnings("unchecked")
    public <T extends CAS.Op<?>> T getAppliedOp(int field, CAS cas)
    {
        final T op = (T)cas.getOp(fieldUpdateIdx[field]);
        return op != null && op.f == field && !op.noop && !op.isPrecondition(cas, this) ? 
                op : null;
    }
    
    @SuppressWarnings("unchecked")
    public <T extends CAS.Op<?>> T rawOp(int field, CAS cas)
    {
        return (T)cas.getOp(fieldUpdateIdx[field]);
    }
    
    private void indexVarInt32(final int fieldNumber, final int value)
    {
        // even if this field did not change, it could be needed by an index
        fieldOffsetAddIdx[fieldNumber] = -offset32;
        
        // indicates that this is an int
        varint32Idx[offset32++] = -1;
        
        // the tag offset
        varint32Idx[offset32++] = size;
        
        // the value stored
        varint32Idx[offset32++] = value;
    }
    
    private void indexVarInt64(final int fieldNumber, final long value)
    {
        // even if this field did not change, it could be needed by an index
        fieldOffsetAddIdx[fieldNumber] = -offset32;
        
        // pointer to the value
        varint32Idx[offset32++] = offset64;
        
        // the tag offset
        varint32Idx[offset32++] = size;

        // the value
        varint64Idx[offset64++] = value;
    }
    
    private int casVarInt32(final int fieldNumber, final int value, 
            final CAS.AbstractInt32Op<?> op)
    {
        if(!op.compare(value, cas, this))
            throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
        
        if(op.isPrecondition(cas, this))
            return value;
        
        final int newValue = op.swap(value, cas, this);
        if(newValue == value)
            return value;
        
        if(!root.isValid(fieldNumber, newValue))
            throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
        
        fieldsApplied[casCount++] = fieldNumber;
        // mark
        fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
        
        if(root.index.isDependency(fieldNumber))
        {
            // marked for key update
            fieldsTracked[trackCount++] = fieldNumber;
        }
        
        // for removal
        fieldOffsetRemoveIdx[fieldNumber] = -offset32;
        
        // indicates that this is an int
        varint32Idx[offset32++] = -1;
        
        // the tag offset
        varint32Idx[offset32++] = input.previousOffset;
        
        // the value stored
        varint32Idx[offset32++] = value;
        
        // set
        return newValue;
    }
    
    private long casVarInt64(final int fieldNumber, final long value, 
            final CAS.AbstractInt64Op<?> op)
    {
        if(!op.compare(value, cas, this))
            throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
        
        if(op.isPrecondition(cas, this))
            return value;
        
        final long newValue = op.swap(value, cas, this);
        if(newValue == value)
            return value;
        
        if(!root.isValid(fieldNumber, newValue))
            throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
        
        fieldsApplied[casCount++] = fieldNumber;
        // mark
        fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
        
        if(root.index.isDependency(fieldNumber))
        {
            // marked for key update
            fieldsTracked[trackCount++] = fieldNumber;
        }
        
        // for removal
        fieldOffsetRemoveIdx[fieldNumber] = -offset32;
        
        // pointer to the value
        varint32Idx[offset32++] = offset64;
        
        // the tag offset
        varint32Idx[offset32++] = input.previousOffset;

        // the value
        varint64Idx[offset64++] = value;
        
        // set
        return newValue;
    }

    public void writeInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            
            if (root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                if (value < 0 || value > 127)
                {
                    throw DSRuntimeExceptions.runtime("The field " + 
                            root.pipeSchema.getFieldName(fieldNumber) + 
                            "of " + root.pipeSchema.messageName() + 
                            " has an invalid onebyte value: " + value);
                }
                
                writeOneByteInt(fieldNumber, value, o);
                return;
            }
            
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt32(fieldNumber, value, (CAS.Int32Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt32(fieldNumber, value);
        }
        
        if(value < 0)
        {
            tail = sink.writeVarInt64(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
        else
        {
            tail = sink.writeVarInt32(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
    }
    
    public void writeUInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            
            if(root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                if(value < 0 || value > 127)
                {
                    throw DSRuntimeExceptions.runtime("The field " + 
                            root.pipeSchema.getFieldName(fieldNumber) + 
                            "of " + root.pipeSchema.messageName() + 
                            " has an invalid onebyte value: " + value);
                }
                
                writeOneByteInt(fieldNumber, value, o);
                return;
            }
            
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt32(fieldNumber, value, (CAS.UInt32Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt32(fieldNumber, value);
        }
        
        tail = sink.writeVarInt32(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeSInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt32(fieldNumber, value, (CAS.SInt32Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt32(fieldNumber, value);
        }
        
        tail = sink.writeVarInt32(
                encodeZigZag32(value), 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    private int casFixed32(final int fieldNumber, final int value, 
            final CAS.AbstractInt32Op<?> op)
    {
        if(!op.compare(value, cas, this))
            throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
        
        if(op.isPrecondition(cas, this))
            return value;
        
        final int newValue = op.swap(value, cas, this);
        if(newValue == value)
            return value;
        
        if(!root.isValid(fieldNumber, newValue))
            throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
        
        fieldsApplied[casCount++] = fieldNumber;
        // mark
        fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
        
        if(root.index.isDependency(fieldNumber))
        {
            // marked for key update
            fieldsTracked[trackCount++] = fieldNumber;
        }
        
        // set
        return newValue;
    }
    
    public void writeFixed32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                // lazy deser
                value = casFixed32(fieldNumber, input.readFixed32(), (CAS.Fixed32Op)o);
                
                // rewrite
                tail = sink.writeInt32LE(
                        value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_FIXED32), 
                                this, 
                                tail));
                return;
            }
        }
        
        // simple array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                (input.offset - input.previousOffset) + 4, // taglen + valuelen
                this, tail);
        input.offset += 4;
    }
    
    public void writeSFixed32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                // lazy deser
                value = casFixed32(fieldNumber, input.readSFixed32(), (CAS.SFixed32Op)o);
                
                // rewrite
                tail = sink.writeInt32LE(
                        value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_FIXED32), 
                                this, 
                                tail));
                return;
            }
        }
        
        // simple array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                (input.offset - input.previousOffset) + 4, // taglen + valuelen
                this, tail);
        input.offset += 4;
    }
    
    public void writeInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt64(fieldNumber, value, (CAS.Int64Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt64(fieldNumber, value);
        }
        
        tail = sink.writeVarInt64(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeUInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt64(fieldNumber, value, (CAS.UInt64Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt64(fieldNumber, value);
        }
        
        tail = sink.writeVarInt64(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeSInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt64(fieldNumber, value, (CAS.SInt64Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt64(fieldNumber, value);
        }
        
        tail = sink.writeVarInt64(
                encodeZigZag64(value), 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    private long casFixed64(final int fieldNumber, final long value, 
            final CAS.AbstractInt64Op<?> op)
    {
        if(!op.compare(value, cas, this))
            throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
        
        if(op.isPrecondition(cas, this))
            return value;
        
        long newValue = op.swap(value, cas, this);
        if(newValue == value && op.i && op.s == 0)
        {
            // TODO find a better way to detect an update ts op?
            // an update timestamp where the refresh rate is low
            newValue++;
        }
        
        if(!root.isValid(fieldNumber, newValue))
            throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
        
        fieldsApplied[casCount++] = fieldNumber;
        // mark
        fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
        
        if(root.index.isDependency(fieldNumber))
        {
            // marked for key update
            fieldsTracked[trackCount++] = fieldNumber;
        }
        
        // set
        return newValue;
    }
    
    public void writeFixed64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                // lazy deser
                value = casFixed64(fieldNumber, input.readFixed64(), (CAS.Fixed64Op)o);
                
                //rewrite
                tail = sink.writeInt64LE(
                        value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_FIXED64), 
                                this, 
                                tail));
                return;
            }
        }
        
        // simple array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                (input.offset - input.previousOffset) + 8, // taglen + valuelen
                this, tail);
        input.offset += 8;
    }
    
    public void writeSFixed64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
                    
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                // lazy deser
                value = casFixed64(fieldNumber, input.readSFixed64(), (CAS.SFixed64Op)o);
                
                // rewrite
                tail = sink.writeInt64LE(
                        value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_FIXED64), 
                                this, 
                                tail));
                return;
            }
        }
        
        // simple array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                (input.offset - input.previousOffset) + 8, // taglen + valuelen
                this, tail);
        input.offset += 8;
    }

    public void writeFloat(int fieldNumber, float value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                final CAS.FloatOp op = (CAS.FloatOp)o;
                
                // lazy deser
                value = input.readFloat();
                
                if(!op.compare(value, cas, this))
                    throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
                
                float newValue;
                if(!op.isPrecondition(cas, this) && 
                        value != (newValue = op.swap(value, cas, this)))
                {
                    if(!root.isValid(fieldNumber, newValue))
                        throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
                    
                    fieldsApplied[casCount++] = fieldNumber;
                    // mark
                    fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
                    
                    if(root.index.isDependency(fieldNumber))
                    {
                        // marked for key update
                        fieldsTracked[trackCount++] = fieldNumber;
                    }
                    
                    // set
                    value = newValue;
                }
                
                // rewrite
                tail = sink.writeInt32LE(
                        Float.floatToRawIntBits(value), 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_FIXED32), 
                                this, 
                                tail));
                return;
            }
        }
        
        // simple array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                (input.offset - input.previousOffset) + 4, // taglen + valuelen
                this, tail);
        input.offset += 4;
    }

    public void writeDouble(int fieldNumber, double value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                final CAS.DoubleOp op = (CAS.DoubleOp)o;
                
                // lazy deser
                value = input.readDouble();
                
                if(!op.compare(value, cas, this))
                    throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
                
                double newValue;
                if(!op.isPrecondition(cas, this) && 
                        value != (newValue = op.swap(value, cas, this)))
                {
                    if(!root.isValid(fieldNumber, newValue))
                        throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
                    
                    fieldsApplied[casCount++] = fieldNumber;
                    // mark
                    fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
                    
                    if(root.index.isDependency(fieldNumber))
                    {
                        // marked for key update
                        fieldsTracked[trackCount++] = fieldNumber;
                    }
                    
                    // set
                    value = newValue;
                }
                
                // rewrite
                tail = sink.writeInt64LE(
                        Double.doubleToRawLongBits(value), 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_FIXED64), 
                                this, 
                                tail));
                return;
            }
        }
        
        // simple array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                (input.offset - input.previousOffset) + 8, // taglen + valuelen
                this, tail);
        input.offset += 8;
    }

    public void writeBool(int fieldNumber, boolean value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                final CAS.BoolOp op = (CAS.BoolOp)o;
                
                if(!op.compare(value, cas, this))
                    throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
                
                if(!op.isPrecondition(cas, this) && value != op.swap(value, cas, this))
                {
                    // set
                    value = !value;
                    
                    if(!root.isValid(fieldNumber, value))
                        throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
                    
                    fieldsApplied[casCount++] = fieldNumber;
                    // mark
                    fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
                    
                    if(root.index.isDependency(fieldNumber))
                    {
                        // marked for key update
                        fieldsTracked[trackCount++] = fieldNumber;
                    }
                }
            }
        }
        
        tail = sink.writeByte(
                value ? (byte)0x01 : 0x00, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    private void writeOneByteInt(final int fieldNumber, int value, 
            final CAS.Op<?> o) throws IOException
    {
        // even if this field did not change, it could be needed by an index
        fieldOffsetAddIdx[fieldNumber] = size;
        fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
        
        if(o != null && o.f == fieldNumber && !o.noop)
        {
            final CAS.AbstractInt32Op<?> op = (CAS.AbstractInt32Op<?>)o;
            
            if(!op.compare(value, cas, this))
                throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
            
            int newValue;
            if(!op.isPrecondition(cas, this) && 
                    value != (newValue = op.swap(value, cas, this)))
            {
                if(!root.isValid(fieldNumber, newValue))
                    throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
                
                // protection for when the validator fails to do its job
                if(newValue < 0 || newValue > 255)
                    throw DSRuntimeExceptions.invalidOneByteValue(root.pipeSchema, fieldNumber);
                
                fieldsApplied[casCount++] = fieldNumber;
                // mark
                fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
                
                if(root.index.isDependency(fieldNumber))
                {
                    // marked for key update
                    fieldsTracked[trackCount++] = fieldNumber;
                }
                
                // set
                value = newValue;
            }
        }
        
        tail = sink.writeByte(
                (byte)value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }

    public void writeEnum(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField)
        {
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            
            if(root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                writeOneByteInt(fieldNumber, value, o);
                return;
            }
            
            if(o != null && o.f == fieldNumber && !o.noop)
                value = casVarInt32(fieldNumber, value, (CAS.Int32Op)o);
            
            // even if this field did not change, it could be needed by an index
            indexVarInt32(fieldNumber, value);
        }
        
        if(value < 0)
        {
            tail = sink.writeVarInt64(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
        else
        {
            tail = sink.writeVarInt32(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
    }
    
    public void writeEnumFromIdx(int fieldNumber, int idx, EnumMapping mapping,
            boolean repeated) throws IOException
    {
        writeEnum(fieldNumber, mapping.numbers[idx], repeated);
    }
    
    public void writeString(int fieldNumber, final String value, boolean repeated) throws IOException
    {
        throw new IllegalStateException("Should not happen (we're using protostuff pipes).");
    }

    public void writeBytes(int fieldNumber, final ByteString value, boolean repeated) throws IOException
    {
        throw new IllegalStateException("Should not happen (we're using protostuff pipes).");
    }
    
    public void writeByteArray(int fieldNumber, final byte[] bytes, boolean repeated) throws IOException
    { 
        throw new IllegalStateException("Should not happen (we're using protostuff pipes).");
    }
    
    public void writeByteRange(final boolean utf8String, final int fieldNumber, 
            final byte[] value, final int offset, final int length, 
            boolean repeated) throws IOException
    {
        if(entityField)
        {
            // even if this field did not change, it could be needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            fieldOffsetRemoveIdx[fieldNumber] = input.previousOffset;
            
            final CAS.Op<?> o = root.isMutable(fieldNumber) ? 
                    cas.getOp(fieldCASIdx[fieldNumber]) : null;
            if(o != null && o.f == fieldNumber && !o.noop)
            {
                if(utf8String)
                {
                    final CAS.StringOp op = (CAS.StringOp)o;
                    
                    if(!op.compare(value, offset, length, cas, this))
                        throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
                    
                    String newValue = null;
                    if (!op.isPrecondition(cas, this) && 
                            null != (newValue = op.swap(value, offset, length, cas, this)) &&
                            (!op.computed || !ValueUtil.isEqual(value, offset, length, newValue)))
                    {
                        if (!root.isValid(fieldNumber, newValue))
                            throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
                        
                        fieldsApplied[casCount++] = fieldNumber;
                        // mark
                        fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
                        
                        if (root.index.isDependency(fieldNumber))
                        {
                            // marked for key update
                            fieldsTracked[trackCount++] = fieldNumber;
                        }
                        
                        // set
                        tail = sink.writeStrUTF8VarDelimited(
                                newValue, 
                                this, 
                                sink.writeVarInt32(
                                        makeTag(fieldNumber, WIRETYPE_LENGTH_DELIMITED), 
                                        this, 
                                        tail));
                        
                        return;
                    }
                }
                else
                {
                    final CAS.BytesOp op = (CAS.BytesOp)o;
                    
                    if(!op.compare(value, offset, length, cas, this))
                        throw DSRuntimeExceptions.failureOnCompare(root.pipeSchema, fieldNumber);
                    
                    byte[] newValue = null;
                    if (!op.isPrecondition(cas, this) && 
                            null != (newValue = op.swap(value, offset, length, cas, this)) &&
                            (!op.computed || !ValueUtil.isEqual(newValue, value, offset, length)))
                    {
                        if (!root.isValid(fieldNumber, newValue, 0, newValue.length))
                            throw DSRuntimeExceptions.failureOnValidate(root.pipeSchema, fieldNumber);
                        
                        fieldsApplied[casCount++] = fieldNumber;
                        // mark
                        fieldUpdateIdx[fieldNumber] = fieldCASIdx[fieldNumber];
                        
                        if (root.index.isDependency(fieldNumber))
                        {
                            // marked for key update
                            fieldsTracked[trackCount++] = fieldNumber;
                        }
                        
                        // set
                        tail = sink.writeByteArray(
                                newValue, 0, newValue.length, 
                                this, 
                                sink.writeVarInt32(
                                        newValue.length, 
                                        this, 
                                        sink.writeVarInt32(
                                                makeTag(fieldNumber, WIRETYPE_LENGTH_DELIMITED), 
                                                this, 
                                                tail)));
                        
                        return;
                    }
                }
            }
        }
        
        // a special case where the offset is pre-incremented before this method call
        // to allow one-shot array copy
        tail = sink.writeByteArray(input.buffer, 
                input.previousOffset, // tagoffset
                input.offset - input.previousOffset, // taglen + delimlen + valuelen
                this, tail);
        
        /*tail = sink.writeByteArray(
                value, offset, length, 
                this, 
                sink.writeVarInt32(
                        length, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_LENGTH_DELIMITED), 
                                this, 
                                tail)));*/
    }
    
    public <T> void writeObject(final int fieldNumber, final T value, final Schema<T> schema, 
            final boolean repeated) throws IOException
    {
        final boolean lastEntityField = entityField;
        
        entityField = false;
        
        schema.writeTo(this, value);
        
        entityField = lastEntityField;
    }
    
    // lazy deserialization

    public <T> void handleUnknownField(int fieldNumber, Schema<T> schema) throws IOException
    {
        input.handleUnknownField(fieldNumber, schema);
    }

    public <T> int readFieldNumber(Schema<T> schema) throws IOException
    {
        return input.readFieldNumber(schema);
    }

    public int readInt32() throws IOException
    {
        return input.readInt32();
    }

    public int readUInt32() throws IOException
    {
        return input.readUInt32();
    }

    public int readSInt32() throws IOException
    {
        return input.readSInt32();
    }

    public int readFixed32() throws IOException
    {
        return 0;
    }

    public int readSFixed32() throws IOException
    {
        return 0;
    }

    public long readInt64() throws IOException
    {
        return input.readInt64();
    }

    public long readUInt64() throws IOException
    {
        return input.readUInt64();
    }

    public long readSInt64() throws IOException
    {
        return input.readSInt64();
    }

    public long readFixed64() throws IOException
    {
        return 0;
    }

    public long readSFixed64() throws IOException
    {
        return 0;
    }

    public float readFloat() throws IOException
    {
        return 0;
    }

    public double readDouble() throws IOException
    {
        return 0;
    }

    public boolean readBool() throws IOException
    {
        return input.readBool();
    }

    public int readEnum() throws IOException
    {
        return input.readEnum();
    }
    
    public int readEnumIdx(EnumMapping mapping) throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public String readString() throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public ByteString readBytes() throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public byte[] readByteArray() throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public <T> T mergeObject(T value, Schema<T> schema) throws IOException
    {
        return input.mergeObject(value, schema);
    }

    public void transferByteRangeTo(Output output, boolean utf8String, int fieldNumber,
            boolean repeated) throws IOException
    {
        input.transferByteRangeTo(output, utf8String, fieldNumber, repeated);
    }
    
    public void transferEnumTo(Output output, EnumMapping mapping, int fieldNumber,
            boolean repeated) throws IOException
    {
        output.writeEnum(fieldNumber, readEnum(), repeated);
    }

    @Override
    protected int flush(byte[] buf, int offset, int len) throws IOException
    {
        return handler.flush(buf, offset, len);
    }

    @Override
    protected int flush(byte[] buf, int offset, int len, byte[] next, int nextoffset, int nextlen)
            throws IOException
    {
        return handler.flush(buf, offset, len, next, nextoffset, nextlen);
    }

    @Override
    protected int flush(LinkedBuffer lb, byte[] buf, int offset, int len) throws IOException
    {
        return handler.flush(lb, buf, offset, len);
    }
}
