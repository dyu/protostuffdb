//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

/**
 * Key Locking.
 * 
 * @author David Yu
 * @created Feb 23, 2013
 */
public interface KeyLock
{
    
    /**
     * Acuires the lock and the hash of the key is returned.
     */
    int acquire(byte[] key);
    
    /**
     * Release the lock using the hash returned from {@link #acquire(byte[])}.
     * Returns true if there was no concurrent key executing.
     */
    boolean release(int hash);

}
