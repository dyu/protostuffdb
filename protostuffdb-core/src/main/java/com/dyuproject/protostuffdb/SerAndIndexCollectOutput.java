//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuff.ProtobufOutput.encodeZigZag32;
import static com.dyuproject.protostuff.ProtobufOutput.encodeZigZag64;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_FIXED32;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_FIXED64;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_LENGTH_DELIMITED;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_VARINT;
import static com.dyuproject.protostuff.WireFormat.makeTag;

import java.io.IOException;
import java.io.OutputStream;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.DSUtils;
import com.dyuproject.protostuff.EnumMapping;
import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.WriteSession;
import com.dyuproject.protostuffdb.WriteContext.OverflowHandler;

/**
 * A protostuff output w/c keeps track of fields that need to be indexed, and at the 
 * same time serializing the message.
 *
 * @author David Yu
 * @created Feb 28, 2011
 */
public final class SerAndIndexCollectOutput extends WriteSession implements Output
{
    
    final int[] fieldOffsetAddIdx;
    
    final int[] fieldsTracked;
    int count;
    
    final int[] varint32Idx;
    final long[] varint64Idx;
    
    final OverflowHandler handler;
    
    private int offset32;
    private int offset64;
    
    private EntityMetadata<?> root;
    private boolean entityField;

    public SerAndIndexCollectOutput(
            int[] fieldOffsetAddIdx, 
            int[] fieldsTracked, 
            
            int[] varint32Idx,
            long[] varint64Idx, 
            LinkedBuffer buffer,
            OutputStream out, 
            OverflowHandler handler)
    {
        super(buffer, out);
        
        this.fieldOffsetAddIdx = fieldOffsetAddIdx;
        this.fieldsTracked = fieldsTracked;
        
        this.varint32Idx = varint32Idx;
        this.varint64Idx = varint64Idx;
        
        this.handler = handler;
    }
    
    public SerAndIndexCollectOutput init(EntityMetadata<?> root)
    {
        this.root = root;
        
        count = 0;
        
        offset32 = 1;
        offset64 = 0;
        
        entityField = true;
        
        return this;
    }
    
    public SerAndIndexCollectOutput clear()
    {
        super.clear();
        
        // unset for gc
        root = null;
        
        return this;
    }
    
    public boolean isEnumsByName()
    {
        return false;
    }
    
    private void indexVarInt32(int fieldNumber, int value)
    {
        // this field is needed by an index
        fieldOffsetAddIdx[fieldNumber] = -offset32;
        
        // indicates that this is an int
        varint32Idx[offset32++] = -1;
        
        // the tag offset
        varint32Idx[offset32++] = size;
        
        // the value stored
        varint32Idx[offset32++] = value;
        
        fieldsTracked[count++] = fieldNumber;
    }

    private void indexVarInt64(int fieldNumber, long value)
    {
        // this field is needed by an index
        fieldOffsetAddIdx[fieldNumber] = -offset32;
        
        // pointer to the value
        varint32Idx[offset32++] = offset64;
        
        // the tag offset
        varint32Idx[offset32++] = size;

        // the value
        varint64Idx[offset64++] = value;
        
        fieldsTracked[count++] = fieldNumber;
    }
    
    public void writeInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            if (root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                fieldOffsetAddIdx[fieldNumber] = size;
                
                fieldsTracked[count++] = fieldNumber;
                
                tail = sink.writeByte(
                        (byte)value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_VARINT), 
                                this, 
                                tail));
                return;
            }
            
            indexVarInt32(fieldNumber, value);
        }
        
        if(value < 0)
        {
            tail = sink.writeVarInt64(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
        else
        {
            tail = sink.writeVarInt32(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
    }
    
    public void writeUInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            if(root.isOneByteValue(fieldNumber))
            {
                // takes one byte to serialize (configured)
                fieldOffsetAddIdx[fieldNumber] = size;
                
                fieldsTracked[count++] = fieldNumber;
                
                tail = sink.writeByte(
                        (byte)value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_VARINT), 
                                this, 
                                tail));
                return;
            }
            
            indexVarInt32(fieldNumber, value);
        }
        
        tail = sink.writeVarInt32(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeSInt32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt32(fieldNumber, value);
        
        tail = sink.writeVarInt32(
                encodeZigZag32(value), 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeFixed32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeInt32LE(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_FIXED32), 
                        this, 
                        tail));
    }
    
    public void writeSFixed32(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeInt32LE(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_FIXED32), 
                        this, 
                        tail));
    }
    
    

    public void writeInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt64(fieldNumber, value);
        
        tail = sink.writeVarInt64(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeUInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt64(fieldNumber, value);
        
        tail = sink.writeVarInt64(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeSInt64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
            indexVarInt64(fieldNumber, value);
        
        tail = sink.writeVarInt64(
                encodeZigZag64(value), 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }
    
    public void writeFixed64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeInt64LE(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_FIXED64), 
                        this, 
                        tail));
    }
    
    public void writeSFixed64(int fieldNumber, long value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeInt64LE(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_FIXED64), 
                        this, 
                        tail));
    }

    public void writeFloat(int fieldNumber, float value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeInt32LE(
                Float.floatToRawIntBits(value), 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_FIXED32), 
                        this, 
                        tail));
    }

    public void writeDouble(int fieldNumber, double value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeInt64LE(
                Double.doubleToRawLongBits(value), 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_FIXED64), 
                        this, 
                        tail));
    }

    public void writeBool(int fieldNumber, boolean value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeByte(
                value ? (byte)0x01 : 0x00, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_VARINT), 
                        this, 
                        tail));
    }

    public void writeEnum(int fieldNumber, int value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            if(root.isOneByteValue(fieldNumber))
            {
                if(value < 0 || value > 127)
                {
                    throw DSRuntimeExceptions.runtime("The field " + 
                            root.pipeSchema.getFieldName(fieldNumber) + 
                            "of " + root.pipeSchema.messageName() + 
                            " has an invalid onebyte value: " + value);
                }
                
                // takes one byte to serialize (configured)
                fieldOffsetAddIdx[fieldNumber] = size;
                
                fieldsTracked[count++] = fieldNumber;
                
                tail = sink.writeByte(
                        (byte)value, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_VARINT), 
                                this, 
                                tail));
                return;
            }
            
            indexVarInt32(fieldNumber, value);
        }
        
        if(value < 0)
        {
            tail = sink.writeVarInt64(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
        else
        {
            tail = sink.writeVarInt32(
                    value, 
                    this, 
                    sink.writeVarInt32(
                            makeTag(fieldNumber, WIRETYPE_VARINT), 
                            this, 
                            tail));
        }
    }
    
    public void writeEnumFromIdx(int fieldNumber, int idx, EnumMapping mapping,
            boolean repeated) throws IOException
    {
        writeEnum(fieldNumber, mapping.numbers[idx], repeated);
    }

    public void writeString(int fieldNumber, String value, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeStrUTF8VarDelimited(
                value, 
                this, 
                sink.writeVarInt32(
                        makeTag(fieldNumber, WIRETYPE_LENGTH_DELIMITED), 
                        this, 
                        tail));
    }

    public void writeBytes(int fieldNumber, ByteString value, boolean repeated) throws IOException
    {
        writeByteArray(fieldNumber, DSUtils.getByteArray(value), repeated);
    }
    
    public void writeByteArray(int fieldNumber, byte[] bytes, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeByteArray(
                bytes, 0, bytes.length,
                this, 
                sink.writeVarInt32(
                        bytes.length, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_LENGTH_DELIMITED), 
                                this, 
                                tail)));
    }
    
    public void writeByteRange(boolean utf8String, int fieldNumber, byte[] value, 
            int offset, int length, boolean repeated) throws IOException
    {
        if(entityField && root.index.isDependency(fieldNumber))
        {
            // this field is needed by an index
            fieldOffsetAddIdx[fieldNumber] = size;
            
            fieldsTracked[count++] = fieldNumber;
        }
        
        tail = sink.writeByteArray(
                value, offset, length, 
                this, 
                sink.writeVarInt32(
                        length, 
                        this, 
                        sink.writeVarInt32(
                                makeTag(fieldNumber, WIRETYPE_LENGTH_DELIMITED), 
                                this, 
                                tail)));
    }
    
    public <T> void writeObject(final int fieldNumber, final T value, final Schema<T> schema, 
            final boolean repeated) throws IOException
    {
        final boolean lastEntityField = entityField;
        
        entityField = false;
        
        schema.writeTo(this, value);
        
        entityField = lastEntityField;
    }

    @Override
    protected int flush(byte[] buf, int offset, int len) throws IOException
    {
        return handler.flush(buf, offset, len);
    }

    @Override
    protected int flush(byte[] buf, int offset, int len, byte[] next, int nextoffset, int nextlen)
            throws IOException
    {
        return handler.flush(buf, offset, len, next, nextoffset, nextlen);
    }

    @Override
    protected int flush(LinkedBuffer lb, byte[] buf, int offset, int len) throws IOException
    {
        return handler.flush(lb, buf, offset, len);
    }
    
    
}
