//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.WriteContext.NTP_ERROR_LIMIT;

/**
 * Entity key generator.
 * 
 * @author David Yu
 * @created Aug 23, 2014
 */
public final class KeyGen
{
    
    public final int kind, exclusiveLast;
    
    private long ts;
    private int count, lastCount, ntpErrorCount;
    
    public KeyGen(int kind)
    {
        this(kind, 2048);
    }
    
    public KeyGen(int kind, int exclusiveLast)
    {
        this.kind = kind;
        this.exclusiveLast = exclusiveLast;
    }

    
    /**
     * Returns a new entity key that is guaranteed to be unique for a single machine.
     */
    public byte[] newEntityKey()
    {
        return newEntityKey(System.currentTimeMillis());
    }
    
    /**
     * Returns a new entity key that is guaranteed to be unique for a single machine.
     */
    public byte[] newEntityKey(long now)
    {
        if(now > ts)
        {
            // update timestamp
            ts = now;
            
            if(NTP_ERROR_LIMIT != 0)
            {
                // reset
                ntpErrorCount = 0;
            }
            
            // always increment, reset if it reaches the limit
            if(++count == exclusiveLast)
                count = 0;
            
            // reset last count
            return KeyUtil.newEntityKey(kind, now, (lastCount = count));
        }
        
        if((NTP_ERROR_LIMIT != 0) && (ntpErrorCount != 0 || now != ts) && 
                (NTP_ERROR_LIMIT == ++ntpErrorCount))
        {
            // ntp went backwards and limit reached
            System.err.println("NTP clock out of sync! Limit reached: " + 
                    NTP_ERROR_LIMIT);
            System.exit(0);
        }
        
        // always increment, reset if it reaches the limit
        if(++count == exclusiveLast)
            count = 0;
        
        if(count == lastCount)
        {
            // System.currentTimeMillis() refresh rate on the machine is probably 10ms 
            now = ++ts;
        }
        
        return KeyUtil.newEntityKey(kind, now, count);
    }

}
