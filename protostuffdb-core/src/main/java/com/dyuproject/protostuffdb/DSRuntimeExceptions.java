//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import protostuffdb.Jni;

import com.dyuproject.protostuff.Pipe;

/**
 * Datastore runtime exceptions.
 *
 * @author David Yu
 * @created Jun 3, 2011
 */
public final class DSRuntimeExceptions
{
    
    static final boolean
            ST_UNAUTHORIZED = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_unauthorized"),
            ST_VALIDATION = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_validation"),
            ST_OPERATION = Jni.PRINT_STACK_TRACE || Boolean.getBoolean("protostuffdb.st_operation");
            
    
    public static class Environment extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public Environment(){}
        public Environment(String msg){super(msg);}
    }
    
    public static class Runtime extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public Runtime(){}
        public Runtime(String msg){super(msg);}
        public Runtime(String msg, Exception cause){super(msg, cause);}
    }
    
    public static class Unauthorized extends Runtime
    {
        private static final long serialVersionUID = 1L;
        public Unauthorized(){}
        public Unauthorized(String msg){super(msg);}
        
        @Override
        public Throwable fillInStackTrace()
        {
            return !ST_UNAUTHORIZED ? this : super.fillInStackTrace();
        }
    }
    
    public static class Validation extends Runtime
    {
        private static final long serialVersionUID = 1L;
        public Validation(){}
        public Validation(String msg){super(msg);}
        
        @Override
        public Throwable fillInStackTrace()
        {
            return !ST_VALIDATION ? this : super.fillInStackTrace();
        }
    }
    
    public static class Operation extends Runtime
    {
        private static final long serialVersionUID = 1L;
        public Operation(){}
        public Operation(String msg){super(msg);}
        public Operation(String msg, Exception cause){super(msg, cause);}
        
        @Override
        public Throwable fillInStackTrace()
        {
            return !ST_OPERATION ? this : super.fillInStackTrace();
        }
    }
    
    public static final class Retry extends Runtime
    {
        private static final long serialVersionUID = 1L;
        public Retry(){}
        public Retry(String msg){super(msg);}
        
        @Override
        public Throwable fillInStackTrace()
        {
            return this;
        }
    }
    
    public static RuntimeException unauthorized()
    {
        return unauthorized("Unauthorized");
    }
    
    public static RuntimeException unauthorized(String msg)
    {
        return new Unauthorized(msg);
    }

    public static RuntimeException unknownState(String msg)
    {
        return new Runtime(msg);
    }
    
    public static RuntimeException retry(String msg)
    {
        return new Retry(msg);
    }
    
    public static RuntimeException runtime(String msg)
    {
        return new Runtime(msg);
    }
    
    public static RuntimeException operationFailure(String msg)
    {
        return new Operation(msg);
    }
    
    public static RuntimeException operationFailure(String msg, Exception cause)
    {
        return new Operation(msg, cause);
    }
    
    public static RuntimeException failureOnCompare(Pipe.Schema<?> ps, 
            int fieldNumber)
    {
        return new Operation("CAS failed on " + ps.wrappedSchema.messageName() + 
                "::" + ps.wrappedSchema.getFieldName(fieldNumber));
    }
    
    public static RuntimeException failureOnValidate(Pipe.Schema<?> ps, 
            int fieldNumber)
    {
        return new Validation("Validation failed on " + ps.wrappedSchema.messageName() + 
                "::" + ps.wrappedSchema.getFieldName(fieldNumber));
    }
    
    public static RuntimeException invalidOneByteValue(Pipe.Schema<?> ps, 
            int fieldNumber)
    {
        return new Validation("Validation failed on " + ps.wrappedSchema.messageName() + 
                "::" + ps.wrappedSchema.getFieldName(fieldNumber) + " (Invalid one-byte value)");
    }
    
    public static RuntimeException parentNull(byte[] key)
    {
        return new Validation("parentKey must not be null.");
    }
    
    public static RuntimeException parentNotNull(byte[] key)
    {
        return new Validation("This entity does not have a parent.");
    }
    
    public static RuntimeException invalidKind(int kind)
    {
        return new Validation("Invalid kind: " + kind);
    }
    
    public static RuntimeException invalidTagKind(int tagKind)
    {
        return new Validation("Invalid tag kind: " + tagKind);
    }
    
    public static RuntimeException invalidKey(byte[] key)
    {
        return invalidKeyLen(key.length);
    }
    
    public static RuntimeException invalidKeyLen(int klen)
    {
        return new Validation("Invalid key length: " + klen);
    }
    
    public static RuntimeException invalidKey(byte[] key, String message)
    {
        return new Validation(message);
    }
    
    public static RuntimeException invalidArg(String message)
    {
        return new Validation(message);
    }
    
    // TODO find
    /*public static RuntimeException invalidIndexValue(byte[] value)
    {
        return null;
    }*/

}
