//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.File;
import java.util.List;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.CAS;



/**
 * Storage for data in key-value format with support for simple indexing 
 * and schema evolution. 
 *
 * @author David Yu
 * @created Feb 25, 2011
 */
public abstract class Datastore
{
    public final String name;
    public final File dir;
    
    public Datastore(String name, File dir)
    {
        this.name = name;
        this.dir = dir;
    }
    
    /**
     * Returns the timestamp (persisted) when this datastore was created.
     */
    public abstract long getCreateTs();
    
    /**
     * Returns true if the visitor session does not see new writes from the time 
     * the session started.
     */
    public abstract boolean isVisitorSessionIsolated();
    
    /**
     * Returnes true if the transactional writes are successful.
     * 
     * The initial op will receive a null key and a null value.
     */
    public final <T,P> boolean chain(EntityMetadata<T> root, 
            Op<P> op, P param, int retries, 
            WriteContext context)
    {
        return chain(root, op, param, retries, context, null, null, null, null);
    }
    
    /**
     * Returnes true if the transactional writes are successful.
     * 
     * The initial op will receive a null value.
     */
    public final <T,P> boolean chain(EntityMetadata<T> root, 
            Op<P> op, P param, int retries, 
            WriteContext context, byte[] key)
    {
        return chain(root, op, param, retries, context, key, null, null, null);
    }
    
    /**
     * Returnes true if the transactional writes are successful.
     * 
     * The initial op will receive a null value.
     */
    public final <T,P,V> boolean chain(EntityMetadata<T> root, 
            Op<P> op, P param, int retries, 
            WriteContext context, byte[] key, 
            VisitorSession.Handler<V> vhandler, V vparam)
    {
        return chain(root, op, param, retries, context, key, vhandler, vparam, null);
    }
    
    /**
     * Returnes true if the transactional writes are successful.
     * 
     * The initial op will receive a null value.
     */
    public abstract <T,P,V> boolean chain(EntityMetadata<T> root, 
            Op<P> op, P param, int retries, 
            WriteContext context, byte[] key, 
            VisitorSession.Handler<V> vhandler, V vparam, VisitorSession vs);
    
    /**
     * Allows execution of multiple queries (via visitor) in a single session.
     */
    public abstract <P> void session(WriteContext context, 
            VisitorSession.Handler<P> handler, P param);
    
    /* ================================================== */
    
    /**
     * Raw check if key exists via iterator.
     */
    public final boolean exists(boolean prefix, WriteContext context, KeyBuilder kb)
    {
        return exists(prefix, context, kb, true);
    }
    
    /**
     * Returns true if key exists.
     */
    public final boolean get(HasKV kv, WriteContext context, KeyBuilder kb)
    {
        return get(kv, context, kb, true);
    }
    
    /**
     * Returns true if key prefix exists.
     */
    public final boolean pget(HasKV kv, WriteContext context, KeyBuilder kb)
    {
        return pget(kv, true, false, context, kb, true);
    }
    
    /**
     * Returns true if key prefix exists.
     */
    public final boolean pget(HasKV kv, boolean includeValue, boolean extractEntityKey, 
            WriteContext context, KeyBuilder kb)
    {
        return pget(kv, includeValue, extractEntityKey, context, kb, true);
    }
    
    /**
     * Raw check if key exists via iterator.
     */
    public abstract boolean exists(boolean prefix, 
            WriteContext context, KeyBuilder kb, boolean pop);
    
    /**
     * Returns true if key exists.
     */
    public abstract boolean get(HasKV kv, 
            WriteContext context, KeyBuilder kb, boolean pop);
    
    /**
     * Returns true if key exists.
     */
    public abstract boolean pget(HasKV kv, 
            boolean includeValue, boolean extractEntityKey, 
            WriteContext context, KeyBuilder kb, boolean pop);
    
    /* ================================================== */
    
    /**
     * Basic get.
     */
    public final byte[] get(String key, WriteContext context)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        return rawGet(kbuf, koffset, klen, context);
    }
    
    /**
     * Basic get.
     */
    public final byte[] get(byte[] key, WriteContext context)
    {
        byte[] prefixedKey = KeyUtil.newPrefixedKey(key);
        return rawGet(prefixedKey, 0, prefixedKey.length, context);
    }
    
    /**
     * Basic check if key exists.
     */
    public final boolean exists(String key, WriteContext context)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        return rawExists(kbuf, koffset, klen, context);
    }
    
    /**
     * Basic check if key exists.
     */
    public final boolean exists(byte[] key, WriteContext context)
    {
        return rawExists(KeyUtil.newPrefixedKey(key), 0, 1+key.length, context);
    }
    
    /**
     * Basic put.
     */
    public final void put(String key, byte[] value, WriteContext context)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        rawPut(kbuf, koffset, klen, value, 0, value.length, context);
    }
    
    /**
     * Basic put.
     */
    public final void put(String key, byte[] value, int offset, int len, 
            WriteContext context)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        rawPut(kbuf, koffset, klen, value, offset, len, context);
    }
    
    /**
     * Basic put.
     */
    public final void put(byte[] key, byte[] value, WriteContext context)
    {
        rawPut(KeyUtil.newPrefixedKey(key), 0, 1+key.length, value, 0, value.length, context);
    }
    
    /**
     * Basic put.
     */
    public final void put(byte[] key, byte[] value, int offset, int len, 
            WriteContext context)
    {
        rawPut(KeyUtil.newPrefixedKey(key), 0, 1+key.length, value, offset, len, context);
    }
    
    /**
     * Basic delete.
     */
    public final void delete(String key, WriteContext context)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        rawDelete(kbuf, koffset, klen, context);
    }
    
    /**
     * Basic delete.
     */
    public final void delete(byte[] key, WriteContext context)
    {
        rawDelete(KeyUtil.newPrefixedKey(key), 0, 1+key.length, context);
    }
    
    /**
     * Raw put.
     */
    public final void rawPut(byte[] key, byte[] value, WriteContext context)
    {
        rawPut(key, 0, key.length, value, 0, value.length, context);
    }
    
    /**
     * Raw delete.
     */
    public final void rawDelete(byte[] key, WriteContext context)
    {
        rawDelete(key, 0, key.length, context);
    }
    
    /**
     * Raw get.
     */
    public final byte[] rawGet(byte[] key, WriteContext context)
    {
        return rawGet(key, 0, key.length, context);
    }
    
    /**
     * Raw check if key exists.
     */
    public final boolean rawExists(byte[] key, WriteContext context)
    {
        return rawExists(key, 0, key.length, context);
    }
    
    /**
     * Raw put.
     */
    public abstract void rawPut(byte[] key, int koffset, int klen, 
            byte[] value, int voffset, int vlen, WriteContext context);
    
    /**
     * Raw delete.
     */
    public abstract void rawDelete(byte[] key, int koffset, int klen, WriteContext context);
    
    /**
     * Raw get.
     */
    public abstract byte[] rawGet(byte[] key, int koffset, int klen, WriteContext context);
    
    /**
     * Raw check if key exists.
     */
    public abstract boolean rawExists(byte[] key, int koffset, int klen, WriteContext context);
    
    // ======================================================================
    
    
    /**
     * Returns the unique key for the new entity.
     * 
     * @param parentKey is optional.
     */
    public <T> byte[] insert(T entity, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context)
    {
        byte[] key = context.newEntityKey(em);
        insert(key, entity, em, parentKey, context);
        return key;
    }
    
    /**
     * Inserts the entity with the specified key.
     * 
     * @param parentKey is optional.
     */
    public final <T> void insertWithKey(byte[] key, T entity, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context)
    {
        KeyUtil.validateSameKind(key, em);
        insert(key, entity, em, parentKey, context);
    }
    
    /**
     * Inserts the entity with the specified key.
     * 
     * @param parentKey is optional.
     */
    protected final <T> void insert(byte[] key, T entity, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context)
    {
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                break;
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null");
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                break;
                
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Insert operation not applicable for type: " + em.type);
        }
        
        final int size = em.indexOnFields ? context.serAndIndexCollect(entity, em) : 
            context.ser(entity, em);
        
        insert(key, em, parentKey, context, 
                context.serAndIndexCollectOutput, 
                context.entityBuffer, context.entityOffset, size);
    }
    
    /**
     * Inserts the entity with the specified key.
     * 
     * @param parentKey is optional.
     */
    protected abstract <T> void insert(byte[] key, final EntityMetadata<T> em, 
            final byte[] parentKey, final WriteContext context, 
            final SerAndIndexCollectOutput output, 
            final byte[] entityBuffer, final int entityOffset, final int size);
    
    /**
     * Returns true when the key is deleted or have previously been deleted.
     * 
     * @param parentKey is optional.
     */
    public abstract <T> boolean delete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context);
    
    /**
     * Returns true if the atomic CAS operations are successful.
     * 
     * @param parentKey is optional.
     */
    public final <T> boolean update(byte[] key, EntityMetadata<T> em, 
            CAS cas, 
            byte[] parentKey, WriteContext context)
    {
        return update(key, em, cas, null, parentKey, context);
    }
    
    /**
     * Returns true if the atomic CAS operations are successful.
     * 
     * @param listener is optional.
     * @param parentKey is optional.
     */
    public abstract <T> boolean update(byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener, 
            byte[] parentKey, WriteContext context);
    
    /**
     * Returns true if the entity exists.
     * 
     * @param parentKey is optional.
     */
    public abstract <T> boolean exists(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context);
    
    /**
     * Returns the raw value.
     * 
     * @param parentKey is optional.
     */
    public abstract <T> byte[] get(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, WriteContext context);
    
    /**
     * Returns the total number of keys visited.
     * 
     * @param reverse visits the keys starting from the last index (if true).
     * @param parentKey is optional.
     */
    public final <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            WriteContext context, 
            Visitor<V> visitor, V param)
    {
        return visitKeys(keys, reverse, false, context, visitor, param);
    }
    
    /**
     * Returns the total number of keys visited.
     * 
     * @param reverse visits the keys starting from the last index (if true).
     * @param extractEntityKey extracts the entity key and use that.
     * @param parentKey is optional.
     */
    public abstract <V> int visitKeys(List<byte[]> keys, boolean reverse, 
            boolean extractEntityKey, 
            WriteContext context, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of keys visited.
     * 
     * @param reverse visits the keys starting from the last index (if true).
     * @param extractEntityKey extracts the entity key and use that.
     * @param parentKey is optional.
     */
    public abstract <V> int visitHasKeys(List<? extends HasKey> keys, boolean reverse, 
            boolean extractEntityKey, 
            WriteContext context, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities (by kind) visited.
     * 
     * @param startKey is optional.
     * @param parentKey is optional.
     */
    public abstract <T,V> int visitKind(EntityMetadata<T> em, 
            int limit, boolean desc, 
            byte[] startKey, byte[] parentKey, 
            WriteContext context, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities visited.
     * 
     * Scans the database in either asc or desc order.
     */
    public abstract <V> int scan(boolean keysOnly, int limit, boolean desc, 
            byte[] limitKey, 
            WriteContext context, 
            Visitor<V> visitor, V param);
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public final <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        return visitRange(keysOnly, limit, desc, rawStartKey, context, visitor, param, 
                kb, true);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public final <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop)
    {
        final int skoffset = kb.offset(), 
                sklen = pop ? kb.popLen() : kb.len();
        final byte[] sk = kb.buf(), 
                ek = new byte[sklen + 1];
        
        System.arraycopy(sk, skoffset, ek, 0, sklen);
        // include 1 byte for the range
        sk[skoffset + sklen] = 0;
        ek[sklen] = (byte)0xFF;
        
        return visitRange(keysOnly, limit, desc, 
                rawStartKey, 
                context, visitor, param, 
                sk, skoffset, ek.length, 
                ek, 0, ek.length);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public final <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            byte[] sk, 
            byte[] ek)
    {
        return visitRange(keysOnly, limit, desc, 
                rawStartKey, 
                context, visitor, param, 
                sk, 0, sk.length, 
                ek, 0, ek.length);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * rawStartKey is optional.
     */
    public <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen)
    {
        return visitRange(keysOnly, limit, desc,
                rawStartKey, context,
                visitor, param,
                false, false,
                sk, skoffset, sklen,
                ek, ekoffset, eklen);
    }
    
    /**
     * Returns the total number of entities visited.
     * 
     * If skRelativeLimit is true, only entry keys that have the same length as the 
     * start key would count towards the limit, if limited.
     * 
     * rawStartKey is optional.
     */
    public abstract <V> int visitRange(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param,
            boolean valueAsKey, boolean skRelativeLimit,
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen);
    
    /**
     * Visit the index that matches the composite key.
     */
    public final <V> int visitIndex(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        return visitIndex(keysOnly, limit, desc, 
                rawStartKey, context, visitor, param, kb, true);
    }
    
    /**
     * Visit the index that matches the composite key.
     */
    public abstract <V> int visitIndex(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb, boolean pop);
    
    /*public final <V> int visitIndexPrefixMatch(
            boolean keysOnly, int limit, boolean desc, 
            byte[] rawStartKey, 
            WriteContext context, 
            Visitor<V> visitor, V param, 
            KeyBuilder kb)
    {
        final byte[] buf = kb.buf();
        final int offset = kb.offset(), len = kb.popLen();
        
        // append 0x0FF
        buf[offset+len] = (byte)0xFF;
        
        return visitRange(keysOnly, limit, desc, rawStartKey, 
                context, visitor, param, 
                buf, offset, len, 
                buf, offset, len+1);
    }*/
}
