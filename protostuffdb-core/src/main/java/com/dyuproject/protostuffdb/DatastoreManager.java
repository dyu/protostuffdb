//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.File;

import protostuffdb.JniOps;

/**
 * Manages the datastore and its lifetime.
 * 
 * @author David Yu
 * @created Jan 25, 2014
 */
public interface DatastoreManager
{
    
    public static final int CREATE_IF_MISSING = JniOps.FNEWDB_CREATE_IF_MISSING;
    public static final int ERROR_IF_EXISTS   = JniOps.FNEWDB_ERROR_IF_EXISTS;
    public static final int PARANOID_CHECKS   = JniOps.FNEWDB_PARANOID_CHECKS;
    
    /**
     * Returns an existing datastore if it exists.
     * Otherwise, null is returned.
     */
    Datastore getStore(String name);
    
    /**
     * Returns a new store if it doesn't already exist.
     */
    Datastore getStore(String name, int flags);

    /**
     * Returns a new store residing at {@code dir} if it doesn't already exist.
     */
    Datastore getStore(String name, int flags, File dir);
    
    /**
     * Backup the data store at storeName.
     */
    boolean backup(String storeName, String backupName);
    
    /**
     * Copy the data from one store to another.
     */
    boolean pipe(String storeFrom, int flags, String storeTo, File dirTo);
}
