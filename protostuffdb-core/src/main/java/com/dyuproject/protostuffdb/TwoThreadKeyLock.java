//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

/**
 * A key lock for two threads.
 * 
 * <pre>
 * Usage:
 * 
 * final int hash = lock.acquire(key);
 * try
 * {
 *     // do something involving the key
 * }
 * finally
 * {
 *     lock.release(hash);
 * }
 * </pre>
 * 
 * @author David Yu
 * @created Feb 23, 2013
 */
public final class TwoThreadKeyLock implements KeyLock
{
    
    // 10 by default
    static final int BUSY_SPIN_CYCLE_LIMIT = Math.max(10, 
            Integer.getInteger("ttc.busy_spin_cycles", 0));

    static final int PARK_CYCLE_LIMIT = BUSY_SPIN_CYCLE_LIMIT + 5;
    
    static final int YIELD_CYCLE_LIMIT = PARK_CYCLE_LIMIT + 5;
    
    private final AtomicInteger atomicHash = new AtomicInteger(0);
    

    public int acquire(byte[] key)
    {
        final int hash = MurmurHash3.murmurhash3_x86_32(key, 0, key.length, 1);
        if(atomicHash.compareAndSet(0, hash))
            return hash;
        
        for(int count = 1, expect = atomicHash.get();; 
                count++, expect = atomicHash.get())
        {
            if(hash != expect && atomicHash.compareAndSet(expect, hash))
                return hash;
            
            // could be operating on same key
            // wait for (expect != hash) <- wait for the other thread to finish
            
            if(count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if(count > YIELD_CYCLE_LIMIT)
                    count = 0;
                else if(count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }

    public boolean release(int hash)
    {
        return atomicHash.compareAndSet(hash, 0);
    }
}
