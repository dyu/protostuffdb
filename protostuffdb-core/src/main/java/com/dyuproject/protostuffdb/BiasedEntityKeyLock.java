//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

/**
 * The thread that calls {@link #acquire()} will cause the other thread to wait until 
 * finished.
 * 
 * @author David Yu
 * @created Apr 3, 2017
 */
public interface BiasedEntityKeyLock extends EntityKeyLock
{
    /**
     * The thread that calls this will cause the other thread
     * to wait until released.
     */
    long acquire();
    
    /**
     * Only waits for the thread that called {@link #acquire()}.
     * 
     * If the other thread called {@link #acquire(long))} or {@link #acquire(byte[]))},  
     * this will not wait and simply proceed.
     * 
     * This should only be called by a thread that does not delete the key nor 
     * perform updates on a key.
     */
    long acquireBiased();
}
