//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import com.dyuproject.protostuff.RpcResponse;

/**
 * In-memory representation of a particular entity's data.
 * 
 * @author David Yu
 * @created Jul 13, 2017
 */
public interface Cache
{
    
    /**
     * Pipe to the socket starting at the index.
     */
    int pipeTo(RpcResponse res, int index);
    
    /**
     * Visit the entries starting at the index.
     */
    <V> int visit(Visitor<V> visitor, V v, int index);

}
