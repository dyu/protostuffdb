//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;

import protostuffdb.Jni;

/**
 * Creates key locks according to the number of writers.
 * 
 * @author David Yu
 * @created Apr 3, 2017
 */
public final class KeyLockFactory
{
    private KeyLockFactory() {}
    
    /**
     * Creates a {@link TwoThreadKeyLock}.
     */
    public static KeyLock create()
    {
        return Jni.WRITERS == 1 ? NOOP : new TwoThreadKeyLock();
    }
    
    /**
     * Creates a {@link TwoThreadEntityKeyLock}.
     */
    public static EntityKeyLock createE()
    {
        return Jni.WRITERS == 1 ? NOOP_E : new TwoThreadEntityKeyLock();
    }
    
    /**
     * Creates a {@link BiasedEntityKeyLock}.
     */
    public static BiasedEntityKeyLock createBE()
    {
        return Jni.WRITERS == 1 ? NOOP_BE : new TwoThreadBiasedEntityKeyLock();
    }
    
    /**
     * Creates a {@link ManyToOneEntityKeyLock}.
     */
    public static ManyToOneEntityKeyLock createM21E()
    {
        return Jni.WRITERS == 1 ? NOOP_M21E : new TwoThreadManyToOneEntityKeyLock();
    }
    
    static final KeyLock NOOP = new KeyLock()
    {
        @Override
        public boolean release(int hash)
        {
            return true;
        }
        
        @Override
        public int acquire(byte[] key)
        {
            return 1;
        }
    };
    
    static final EntityKeyLock NOOP_E = new EntityKeyLock()
    {
        @Override
        public boolean release(long value)
        {
            return true;
        }
        
        @Override
        public long acquire(byte[] key)
        {
            return 1;
        }
        
        @Override
        public long acquire(long value)
        {
            return 1;
        }
    };
    
    static final BiasedEntityKeyLock NOOP_BE = new BiasedEntityKeyLock()
    {
        @Override
        public boolean release(long value)
        {
            return true;
        }
        
        @Override
        public long acquire(byte[] key)
        {
            return 1;
        }
        
        @Override
        public long acquireBiased()
        {
            return 1;
        }
        
        @Override
        public long acquire(long value)
        {
            return 1;
        }
        
        @Override
        public long acquire()
        {
            return 1;
        }
    };
    
    static final ManyToOneEntityKeyLock NOOP_M21E = new ManyToOneEntityKeyLock()
    {
        
        @Override
        public void release(int type)
        {
            
        }
        
        @Override
        public void release(long type)
        {
            
        }
        
        @Override
        public int acquire(byte[] key)
        {
            return 1;
        }
        
        @Override
        public int acquireHPK(List<? extends HasParentKey> keys, 
                List<? extends HasParentKey> moreKeys, WriteContext context)
        {
            return 1;
        }
        
        @Override
        public int acquireHPK(HasParentKey hpk)
        {
            return 1;
        }
        
        @Override
        public int acquireHK(List<? extends HasKey> keys, 
                List<? extends HasKey> moreKeys, WriteContext context)
        {
            return 1;
        }
        
        @Override
        public int acquireHK(HasKey hk)
        {
            return 1;
        }
        
        @Override
        public int acquire(List<byte[]> keys, 
                List<byte[]> moreKeys, WriteContext context)
        {
            return 1;
        }
        
        @Override
        public int acquire(long value)
        {
            return 1;
        }
    };
}
