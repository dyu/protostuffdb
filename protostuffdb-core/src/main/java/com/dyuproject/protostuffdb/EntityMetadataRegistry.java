//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

/**
 * Entity metadata registry.
 *
 * @author David Yu
 * @created Feb 26, 2011
 */
public final class EntityMetadataRegistry implements EntityMetadataResource
{
    
    
    private final EntityMetadata<?>[] emIdx;
    
    private EntityMetadataRegistry(EntityMetadata<?>[] emIdx)
    {
        this.emIdx = emIdx;
    }
    
    public EntityMetadata<?> getEntityMetadata(int kind)
    {
        return emIdx[kind];
    }
    
    public static Builder newBuilder()
    {
        return new Builder();
    }
    
    public static final class Builder
    {
        private EntityMetadata<?>[] emIdx = new EntityMetadata<?>[128];
        
        public Builder add(EntityMetadata<?> em)
        {
            if(emIdx[em.kind] != null)
            {
                throw new IllegalStateException("Duplicate kind: " + em.kind + ". " + 
                        emIdx[em.kind].pipeSchema.messageFullName() + "(existing) vs " + 
                        em.pipeSchema.messageFullName());
            }
            
            emIdx[em.kind] = em;
            
            return this;
        }
        
        public EntityMetadataRegistry build()
        {
            final EntityMetadata<?>[] emIdx = this.emIdx;
            this.emIdx = null;
            
            return new EntityMetadataRegistry(emIdx);
        }
    }
    

}
