//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

/**
 * The type of entity.
 *
 * @author David Yu
 * @created Feb 28, 2011
 */
public enum EntityType
{
    
    /**
     * The entity resides in its own key.
     */
    DEFAULT,
    /**
     * This entity resides in a link-key (one-to-many/parent-child key).
     * Can be efficiently queried when its parent key is provided.
     * 
     * Can be queried independently by kind (the data will be the parent key).  
     * To fetch the entity's data, you'll have to do a second query and 
     * do a multi-get using the values from the first query.
     */
    LINKED_CHILD;

}
