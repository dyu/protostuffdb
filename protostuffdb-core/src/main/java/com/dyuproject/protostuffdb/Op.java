//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;


/**
 * An operation that is executed with a chain of other operations.
 *
 * @author David Yu
 * @created Mar 22, 2011
 */
public interface Op<P>
{
    
    /**
     * Handle the result from the previous chained operation.
     */
    public boolean handle(OpChain chain, 
            byte[] key, 
            byte[] value, int offset, int len, 
            P param);
    
    /**
     * An op that sets the key on a {@link HasKey} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKey> SET_KEY = new Op<HasKey>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKey param)
        {
            param.setKey(key);
            return true;
        }
    };
    
    /**
     * An op that sets the key (extracted from the index key) on a {@link HasKey} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKey> SET_EXTRACTED_KEY = new Op<HasKey>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKey param)
        {
            param.setKey(KeyUtil.extractFrom(key));
            return true;
        }
    };
    
    /**
     * An op that sets the key and timestamp on a {@link HasKeyAndTs} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKeyAndTs> SET_KEY_AND_TS = new Op<HasKeyAndTs>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKeyAndTs param)
        {
            param.setKey(key);
            param.setTs(KeyUtil.extractTsFrom(key));
            return true;
        }
    };
    
    /**
     * An op that sets the key (extracted from the index key) and timestamp 
     * on a {@link HasKeyAndTs} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKeyAndTs> SET_EXTRACTED_KEY_AND_TS = new Op<HasKeyAndTs>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKeyAndTs param)
        {
            param.setKey(KeyUtil.extractFrom(key));
            param.setTs(KeyUtil.extractTsFrom(key));
            return true;
        }
    };
    
    /**
     * An op that sets the value on a {@link HasValue} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasValue> SET_VALUE = new Op<HasValue>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasValue param)
        {
            param.setValue(ValueUtil.copy(value, offset, len));
            return true;
        }
    };
    
    /**
     * An op that sets the value on a {@link HasKey} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKey> SET_VALUE_ON_KEY = new Op<HasKey>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKey param)
        {
            param.setKey(ValueUtil.copy(value, offset, len));
            return true;
        }
    };
    
    /**
     * An op that sets the key and value on a {@link HasKV} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKV> SET_KV = new Op<HasKV>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKV param)
        {
            param.setKV(key, ValueUtil.copy(value, offset, len));
            return true;
        }
    };
    
    /**
     * An op that sets the key (extracted from the index key) and value 
     * on a {@link HasKV} after an operation.
     * Use this as an arg on some of the {@link OpChain} methods.
     */
    public static final Op<HasKV> SET_EXTRACTED_KV = new Op<HasKV>()
    {
        public boolean handle(OpChain chain, 
                byte[] key, byte[] value, int offset, int len,
                HasKV param)
        {
            param.setKV(KeyUtil.extractFrom(key), ValueUtil.copy(value, offset, len));
            return true;
        }
    };
    
    /**
     * An op that appends the key to the list param.
     */
    public static final Op<List<byte[]>> APPEND_KEY = new Op<List<byte[]>>()
    {
        public boolean handle(OpChain chain, byte[] key, 
                byte[] value, int offset, int len,
                List<byte[]> param)
        {
            param.add(key);
            return true;
        }
    };
    
    /**
     * An op that appends the key (extracted from the index key) to the list param.
     */
    public static final Op<List<byte[]>> APPEND_EXTRACTED_KEY = new Op<List<byte[]>>()
    {
        public boolean handle(OpChain chain, byte[] key, 
                byte[] value, int offset, int len,
                List<byte[]> param)
        {
            param.add(KeyUtil.extractFrom(key));
            return true;
        }
    };
    
    /**
     * An op that appends the tag key to the list param.
     */
    public static final Op<List<byte[]>> APPEND_TAG_KEY = new Op<List<byte[]>>()
    {
        public boolean handle(OpChain chain, byte[] key, 
                byte[] value, int offset, int len,
                List<byte[]> param)
        {
            param.add(KeyUtil.keyFromTagIndexKey(key));
            return true;
        }
    };
    
    /**
     * An op that appends the tag name to the list param.
     */
    public static final Op<List<byte[]>> APPEND_TAG_NAME = new Op<List<byte[]>>()
    {
        public boolean handle(OpChain chain, byte[] key, 
                byte[] value, int offset, int len,
                List<byte[]> param)
        {
            param.add(KeyUtil.valueFromTagIndexKey(key));
            return true;
        }
    };
    
    /**
     * An op that appends the key and value (copied).
     */
    public static final Op<List<KV>> APPEND_KV = new Op<List<KV>>()
    {
        public boolean handle(OpChain chain, byte[] key, 
                byte[] value, int offset, int len,
                List<KV> param)
        {
            param.add(new KV(key, ValueUtil.copy(value, offset, len)));
            return true;
        }
    };
    
    /**
     * An op that appends the key (extracted from the index key) and value (copied).
     */
    public static final Op<List<KV>> APPEND_EXTRACTED_KV = new Op<List<KV>>()
    {
        public boolean handle(OpChain chain, byte[] key, 
                byte[] value, int offset, int len,
                List<KV> param)
        {
            param.add(new KV(KeyUtil.extractFrom(key), ValueUtil.copy(value, offset, len)));
            return true;
        }
    };
    
    /**
     * An alias to {@link #APPEND_KV}. 
     */
    public static final Op<List<KV>> APPEND_BOTH = APPEND_KV;

}
