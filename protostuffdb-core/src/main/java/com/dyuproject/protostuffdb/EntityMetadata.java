//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Arrays;

import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.Schema;
import com.dyuproject.protostuff.WireFormat.FieldType;

/**
 * The metadata for entities (persistent pojos).
 *
 * @author David Yu
 * @created Feb 25, 2011
 */
public abstract class EntityMetadata<T>
{
    /**
     * The max serialized size of an entity.
     */
    static final int ENTITY_MAX_SIZE = 1024 * 63; // 63k (64512)
    
    static final int FLAG_SEQ = 1;
    
    /**
     * The key in an entity/tag index.
     */
    protected static final int $KEY = 0x80, $PARENT_KEY = -$KEY;
    
    /**
     * Extracted from a key, which can be included in an entity/tag index.
     */
    protected static final int $DATE = 1, $PARENT_DATE = -$DATE, 
            $DATETIME = 2, $PARENT_DATETIME = -$DATETIME;
    
    /**
     * Field types used on parent-field indexing.
     */
    protected static final int 
            FT_INT32 = 1, // can be enum 
            FT_UINT32 = 2, 
            FT_SINT32 = 3, 
            FT_FIXED32 = 4, 
            FT_SFIXED32 = 5, 
            FT_FLOAT = 6, 
            
            FT_BYTES = 7, 
            FT_BOOL = 8, 
                    
            FT_INT64 = 9, 
            FT_UINT64 = 10, 
            FT_SINT64 = 11, 
            FT_FIXED64 = 12, 
            FT_SFIXED64 = 13,
            FT_DOUBLE = 14, 
            FT_STRING = 15;
    
    /**
     * Alias.
     */
    protected static final int FT_INT8 = FT_BOOL;
    
    /**
     * Acts as a null key (orphan) in the datastore.
     */
    public static final byte[] ZERO_KEY = new byte[9];
    
    static <T> void verify(Pipe.Schema<T> pipeSchema, int kind, 
            EntityType type, EntityMetadata<?> parent)
    {
        if(pipeSchema == null)
            throw new IllegalArgumentException("pipeSchema == null");
        
        if(kind < 1 || kind > 127)
            throw new IllegalArgumentException("kind < 1 || kind > 127");
        
        if(type == null)
            throw new IllegalArgumentException("type == null");
        
        switch(type)
        {
            case DEFAULT:
                if(parent != null)
                    throw new IllegalArgumentException("type DEFAULT cannot have a parent.");
                break;
            case LINKED_CHILD:
                if(parent == null)
                {
                    throw new IllegalArgumentException(" " + type.name() + " " +
                                    "must have a parent.");
                }
                
                if(parent.kind == kind)
                    throw new IllegalArgumentException("parent.kind == kind");
                
                if (parent.kind == 127)
                    throw new IllegalArgumentException("A parent kind can only be within 1-126");
                
                break;
        }
    }
    
    static EntityMetadata<?> getRoot(EntityMetadata<?> em)
    {
        while(em.parent != null)
            em = em.parent;
        
        return em;
    }
    
    private static int[] verify(int[] linkIndex)
    {
        if(linkIndex.length == 0)
            return linkIndex;
        
        int[] set = new int[128];
        for(int kind : linkIndex)
        {
            if(0 != set[kind]++)
                throw new RuntimeException("Duplicate kind: " + kind + " on the link index.");
            
            if(0 == kind && 0 != linkIndex[0])
                throw new RuntimeException("The kind 0 must be first on the link index.");
        }
        
        return linkIndex;
    }
    
    public final Pipe.Schema<T> pipeSchema;
    public final int kind, flags;
    public final boolean seq;
    public final EntityType type;
    /**
     * Max serialized size of the entity.
     */
    public final int maxSize;
    /**
     * The revision of an entity is stored in this field (must be non-zero if specified).
     */
    public final int revField;
    /**
     * The timestamp of an updated entity is stored in this field (must be non-zero if specified).
     */
    public final int updateTsField;
    
    /**
     * {@link #revField} = 1, {@link #updateTsField} = 2 
     */
    public final int updateFlags;
    
    public final EntityMetadata<?> root, parent;
    
    public final Index index;
    public final boolean indexOnFields;
    public final int[] linkIndex;
    
    private final long left, right;
    
    long seqId = 0, prevId = -1;
    
    public EntityMetadata(
            Pipe.Schema<T> pipeSchema, int kind, int flags, 
            EntityType type, EntityMetadata<?> parent, 
            int maxSize,
            int revField, int updateTsField, 
            int[] children, int ... linkIndex)
    {
        verify(pipeSchema, kind, type, parent);
        
        this.pipeSchema = pipeSchema;
        this.kind = kind;
        this.flags = flags;
        this.seq = 0 != (flags & FLAG_SEQ);
        this.type = type;
        this.maxSize = Math.min(maxSize, ENTITY_MAX_SIZE);
        this.revField = revField;
        this.updateTsField = updateTsField;
        
        int updateFlags = 0;
        if (revField != 0)
            updateFlags |= 1;
        if (updateTsField != 0)
            updateFlags |= 2;
        
        this.updateFlags = updateFlags;
        
        this.parent = parent;
        // TODO check self ref if appropriate
        root = parent == null ? this : getRoot(parent);
        
        index = newIndex();
        
        this.indexOnFields = NONE != index;
        this.linkIndex = verify(linkIndex);
        
        long left = 0, right = 0;
        if(children != null && children.length != 0)
        {
            for(int childKind : children)
            {
                if(childKind < 1 || childKind > 127)
                {
                    throw new RuntimeException("The entity: " + 
                            pipeSchema.messageFullName() + 
                            " has an invalid child kind: " + childKind);
                }
                
                if(childKind > 63)
                    left |= (1 << (childKind - 64));
                else
                    right |= (1 << childKind);
            }
        }
        
        this.left = left;
        this.right = right;
    }
    
    public final boolean hasChildren()
    {
        return right != 0 || left != 0;
    }
    
    public final boolean isChild(final int kind)
    {
        if(kind > 63)
            return 0 != (left & (1 << (kind - 64)));
        
        return kind > 0 && 0 != (right & (1 << kind));
    }
    
    /**
     * Returns true.  Override this if you have immutable fields.
     */
    public boolean isMutable(int field)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, String value)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, byte[] value, int offset, int len)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, int value)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, long value)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, float value)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, double value)
    {
        return true;
    }
    
    /**
     * Returns true if valid.
     */
    public boolean isValid(int field, boolean value)
    {
        return true;
    }

    /**
     * Return true if the field is an enum/uint32 whose values are in the 
     * range 0 to 127.
     */
    public boolean isOneByteValue(int field)
    {
        return false;
    }
    
    /**
     * Return true if its a fixed64 date/datetime field.
     */
    public boolean isDateOrDatetime(int field)
    {
        return false;
    }
    
    /**
     * Returns the type of this field.
     */
    public abstract FieldType getFieldType(int field);
    
    /**
     * Returns the index that is tied to this EntityMetadata.
     */
    protected Index newIndex()
    {
        return NONE;
    }
    
    static int[] newOffsetEntries()
    {
        int[] offsetEntries = new int[256];
        // fill with a value that is out of range (TODO)
        Arrays.fill(offsetEntries, Integer.MAX_VALUE);
        
        return offsetEntries;
    }
    
    static final Index NONE = new Index(null, 0, null, 0, 
            new int[128][], newOffsetEntries(), null);
    
    public static final class Index
    {
        /**
         * Creates a new index builder based from the given EntityMetadata.
         */
        public static Builder newBuilder(EntityMetadata<?> em)
        {
            return new Builder(em);
        }
        
        // maps a field to its dependent indices
        final int[][] fieldMapping;// = new int[128][];
        
        // points to the offset of delimitedEntries
        final int[] offsetEntries;// = new int[256]; 
        
        // if an index would like to be written with a tag
        final int[] secondaryTagEntries;// = new int[256];
        
        // Contains the index definition prefixed with the length (delimiter)
        final int[] delimitedEntries;
        
        final int delimitedEntriesSize;
        
        // points to the offset entries
        final int[] valueEntries;
        
        final int valueCount;
        
        public Index(int[] delimitedEntries, int delimitedEntriesSize, 
                int[] valueEntries, int valueCount, 
                int[][] fieldMapping, 
                int[] offsetEntries,
                int[] secondaryTagEntries)
        {
            this.delimitedEntries = delimitedEntries;
            this.delimitedEntriesSize = delimitedEntriesSize;
            
            this.valueEntries = valueEntries;
            this.valueCount = valueCount;
            
            this.fieldMapping = fieldMapping;
            this.offsetEntries = offsetEntries;
            this.secondaryTagEntries = secondaryTagEntries;
        }
        
        public boolean isDependency(int field)
        {
            return null != fieldMapping[field];
        }
        
        int[] getIndices(int field)
        {
            return fieldMapping[field];
        }
        
        public static final class Builder
        {
            static final int INITIAL_DELIMITED_ENTRY_SIZE = 16;
                            
            static final int INITIAL_VALUE_ENTRY_SIZE = 4;
            
            static final int INITIAL_CAPACITY_PER_FIELD_MAPPING = 4;
            
            private final EntityMetadata<?> em;
            private boolean built;
            
            private final int[][] fieldMapping = new int[128][];
            
            private final int[] offsetEntries = newOffsetEntries();
            
            private final int[] secondaryTagEntries = new int[256];
            
            // Assuming an entity has an average of 42 fields with all of those fields
            // being indexed in a 2-field composite key, it will fit with 128 as default.
            private int[] delimitedEntries = new int[INITIAL_DELIMITED_ENTRY_SIZE];
            
            // start at one since we want this to be have a negative equivalent
            private int delimitedEntriesSize = 1;
            
            // points to the offset entries
            // assumes a max of 8 indices store the message as the value
            private int[] valueEntries = new int[INITIAL_VALUE_ENTRY_SIZE];
            
            private int valueCount = 0;
            
            public Builder(EntityMetadata<?> em)
            {
                this.em = em;
            }
            
            static void verify(int field, Schema<?> schema)
            {
                if(field < 3 || field > 127)
                {
                    throw new IllegalArgumentException(
                            "The indexed fields must be 3-127. (" + field + " on " + 
                                    schema.messageName() + ")");
                }
                    
                if(null == schema.getFieldName(field))
                {
                    throw new IllegalArgumentException("The field: " + field + 
                            " does not exist for " + schema.messageName());
                }
            }
            
            static int[] growAndAppend(int[] existing, int toAppend)
            {
                // power-of-two increase
                int[] grow = new int[existing.length << 1];
                System.arraycopy(existing, 0, grow, 0, existing.length);
                
                // fill the slot
                grow[existing.length] = toAppend;
                
                return grow;
            }
            
            /**
             * Add a composite index entry into the {@link Index}.
             */
            public Builder add(int ... entry)
            {
                if(entry.length < 2)
                {
                    throw new IllegalArgumentException(
                            "First arg is index id(tag:1-223, secondary:224-255), 2nd arg (and onwards) is a field.");
                }
                
                final int id = entry[0];
                if(id < 1 || id > 255)
                    throw new IllegalArgumentException("The index id (first arg) must be [1-255] (tag:1-223, secondary:224-255).");
                
                //final Index index = this.index;
                
                if(offsetEntries[id] != Integer.MAX_VALUE)
                    throw new IllegalArgumentException("Duplicate index id: " + id);
                
                // the offset
                offsetEntries[id] = delimitedEntriesSize;
                
                int entryStart = 1, entryCount = entry.length - entryStart;
                if(0 == entry[1])
                {
                    int offsetOfZeroTailDelimiter = 0;
                    // with dependent entries
                    for(int i = 2, shift = 0, secondaryTag; i < entry.length; i++, shift+=8)
                    {
                        if(0 == (secondaryTag = entry[i]))
                        {
                            offsetOfZeroTailDelimiter = i;
                            break;
                        }
                        
                        if(secondaryTag < 1 || secondaryTag > 223)
                        {
                            throw new IllegalArgumentException("The secondary tag of " + 
                                    id + " must be from 1 to 223.");
                        }
                        
                        secondaryTagEntries[id] |= (secondaryTag << shift);
                    }
                    
                    // no zero tail delimiter or there is no field in-between
                    if(offsetOfZeroTailDelimiter == 0 || offsetOfZeroTailDelimiter == 2)
                        throw new IllegalArgumentException("Invalid index definition: " + id);
                    
                    if(offsetOfZeroTailDelimiter - 2 > 4)
                    {
                        throw new IllegalArgumentException(
                                "Too many secondary tags (max 4) for the index: " + id);
                    }
                    
                    entryStart = offsetOfZeroTailDelimiter + 1;
                    entryCount = entry.length - entryStart;
                    
                    // no more fields after the zero tail delimiter
                    if(entryCount == 0)
                        throw new IllegalArgumentException("Invalid index definition: " + id);
                }
                
                // the delimiter for this dependent index
                if(++delimitedEntriesSize > delimitedEntries.length)
                    delimitedEntries = growAndAppend(delimitedEntries, entryCount);
                else
                    delimitedEntries[delimitedEntriesSize - 1] = entryCount;
                //index.delimitedEntries[index.delimitedEntriesSize++] = entry.length - 1;
                
                final Schema<?> schema = em.pipeSchema.wrappedSchema;
                
                // TODO implement this
                /*final boolean globalDependency;
                switch(entry.length - 1)
                {
                    // number of fields
                    case 1:
                        globalDependency = false;
                        break;
                    case 2:
                        globalDependency = 0 == entry[entry.length-1];
                        break;
                    default:
                        globalDependency = 0 == entry[entry.length-1] 
                                && 0 != entry[entry.length-2];
                }*/
                
                if(0 != defineIndex(schema, entry, id, entryStart/*, globalDependency*/))
                {
                    // the key contains mutable fields (mark as negative)
                    offsetEntries[id] = -offsetEntries[id];
                }
                
                return this;
            }
            
            private int defineIndex(final Schema<?> schema, 
                    final int[] entry, final int id, int i/*, boolean globalDependency*/)
            {
                final int start = i;
                int field = entry[i], entityFieldCount = 0, mutableFieldInKeyCount = 0;
                
                // the first part after the parent key (if the latter exists) 
                // does not contain separators
                while(field != 0)
                {
                    if(++delimitedEntriesSize > delimitedEntries.length)
                        delimitedEntries = growAndAppend(delimitedEntries, field);
                    else
                        delimitedEntries[delimitedEntriesSize - 1] = field;
                    //index.delimitedEntries[index.delimitedEntriesSize++] = field;
                    
                    // negate to get actual field
                    boolean positive = true;
                    if(field < 0)
                    {
                        field = -field;
                        positive = false;
                    }
                    
                    if(0 != (0xFF00 & field))
                    {
                        if(em.parent == null)
                        {
                            throw new IllegalArgumentException(schema.messageName() + 
                                    " does not have a parent entity.");
                        }
                        
                        final int voLen = field & 0x0F, parentField = field >>> 8;
                        if(voLen == 0 && null == em.parent.pipeSchema.getFieldName(
                                parentField))
                        {
                            throw new IllegalArgumentException(schema.messageName() + 
                                    " with the parent: " + 
                                    em.parent.pipeSchema.messageName() + 
                                    " contains a foreign field that does not exist: " + 
                                    parentField);
                        }
                        
                        // not added as a dependency because the dependency is external
                        // the dependency field value must also be immutable
                        if(++i == entry.length)
                        {
                            // without a tag value.
                            return mutableFieldInKeyCount;
                        }
                        
                        field = entry[i];
                        continue;
                    }
                    
                    if(field == $KEY
                            || field == $DATE 
                            || field == $DATETIME)
                    {
                        if(!positive && em.parent == null)
                        {
                            throw new IllegalArgumentException(schema.messageName() + 
                                    " does not have a parent entity.");
                        }
                        
                        // not added as a dependency because the key/date/datetime is external
                        if(++i == entry.length)
                        {
                            // without a tag value.
                            return mutableFieldInKeyCount;
                        }
                        
                        field = entry[i];
                        continue;
                    }
                    
                    verify(field, schema);
                    entityFieldCount++;
                    
                    if(em.isMutable(field))
                        mutableFieldInKeyCount++;
                    
                    /*if(globalDependency)
                    {
                        if(0 == (field = entry[++i]))
                        {
                            // we reached zero and expect it to be the last
                            if(++i != entry.length)
                            {
                                throw new IllegalArgumentException("The index id " + id +  
                                        "for " + schema.messageName() + " has misplaced zeroes.");
                            }
                            
                            // separator between the index key and index value
                            if(++delimitedEntriesSize > delimitedEntries.length)
                                delimitedEntries = growAndAppend(delimitedEntries, 0);
                            else
                                delimitedEntries[delimitedEntriesSize - 1] = 0;
                            //index.delimitedEntries[index.delimitedEntriesSize++] = 0;
                            
                            if(++valueCount > valueEntries.length)
                                valueEntries = growAndAppend(valueEntries, id);
                            else
                                valueEntries[valueCount - 1] = id;
                            //index.valueEntries[index.valueCount++] = id;
                            return;
                        }
                        
                        continue;
                    }*/
                    
                    int[] indices = fieldMapping[field];
                    if(indices == null)
                    {
                        // first entry dependent on the field
                        indices = new int[INITIAL_CAPACITY_PER_FIELD_MAPPING];
                        fieldMapping[field] = indices;
                        
                        indices[0] = id;
                        
                        if(++i == entry.length)
                        {
                            // without a tag value.
                            return mutableFieldInKeyCount;
                        }
                        
                        field = entry[i];
                        continue;
                    }

                    // another dependent entry was already added, so start at 1
                    for(int j = 1;;)
                    {
                        if(0 == indices[j])
                        {
                            // fill this slot.
                            indices[j] = id;
                            break;
                        }
                        
                        if(++j == indices.length)
                        {
                            // too many dependent indices on a field that it 
                            // overflows INITIAL_INDEX_COUNT_PER_FIELD
                            fieldMapping[field] = growAndAppend(indices, id);
                            break;
                        }
                    }
                    
                    if(++i == entry.length)
                    {
                        // without a tag value.
                        return mutableFieldInKeyCount;
                    }
                    
                    field = entry[i];
                }
                
                // field is zero
                
                if(i == 1)
                {
                    throw new IllegalArgumentException("The index id " + id +  
                            "for " + schema.messageName() + " has misplaced zeroes.");
                }
                
                // separator between the index key and index value
                if(++delimitedEntriesSize > delimitedEntries.length)
                    delimitedEntries = growAndAppend(delimitedEntries, 0);
                else
                    delimitedEntries[delimitedEntriesSize - 1] = 0;
                //index.delimitedEntries[index.delimitedEntriesSize++] = 0;
                
                if(++i == entry.length)
                {
                    // user intends to have the message as the value
                    
                    // add the id as global dependency (updates index on any change)
                    if(++valueCount > valueEntries.length)
                        valueEntries = growAndAppend(valueEntries, id);
                    else
                        valueEntries[valueCount - 1] = id;
                    //index.valueEntries[index.valueCount++] = id;
                    
                    return mutableFieldInKeyCount;
                }
                
                // the offset of zero
                final int separatorOffset = i - 1;
                
                // user defined custom values
                boolean includesFieldTag = false;
                if(entry[i] == 0)
                {
                    // two succeeding zeroes
                    includesFieldTag = true;
                    
                    if(++i == entry.length)
                    {
                        throw new IllegalArgumentException("Two succeeding separators " +
                            "need to be followed by fields who tags you wish to " + 
                            "be included.");
                    }
                    
                    if(++delimitedEntriesSize > delimitedEntries.length)
                        delimitedEntries = growAndAppend(delimitedEntries, 0);
                    else
                        delimitedEntries[delimitedEntriesSize - 1] = 0;
                    //index.delimitedEntries[index.delimitedEntriesSize++] = 0;
                }
                
                defineIndexValue(schema, entry, id, i, 
                        start, separatorOffset, 
                        includesFieldTag, entityFieldCount);
                
                return mutableFieldInKeyCount;
            }
            
            private void defineIndexValue(final Schema<?> schema, 
                    final int[] entry, final int id, int i, 
                    final int start, final int separatorOffset, 
                    final boolean includesFieldTag, int entityFieldCount)
            {
                for(int field = 0; i < entry.length; i++)
                {
                    if(0 == (field = entry[i]))
                    {
                        throw new IllegalArgumentException(
                                "Misplaced zero for: " + schema.messageName());
                    }
                    
                    if(field == $KEY || field == $PARENT_KEY 
                            || field == $DATE || field == $PARENT_DATE 
                            || field == $DATETIME || field == $PARENT_DATETIME)
                    {
                        if(field < 0 && em.parent == null)
                        {
                            throw new IllegalArgumentException(schema.messageName() + 
                                    " does not have a parent entity.");
                        }
                        
                        if(includesFieldTag)
                        {
                            throw new IllegalArgumentException("The key/date/datetime must not be " +
                                    "in an index value that includes the field tags.");
                        }
                        
                        // not added as a dependency because the key/date/datetime is external
                        // simply add it to the entry
                        if(++delimitedEntriesSize > delimitedEntries.length)
                            delimitedEntries = growAndAppend(delimitedEntries, field);
                        else
                            delimitedEntries[delimitedEntriesSize - 1] = field;
                        //index.delimitedEntries[index.delimitedEntriesSize++] = field;
                        continue;
                    }
                    
                    verify(field, schema);
                    entityFieldCount++;
                    
                    if(++delimitedEntriesSize > delimitedEntries.length)
                        delimitedEntries = growAndAppend(delimitedEntries, field);
                    else
                        delimitedEntries[delimitedEntriesSize - 1] = field;
                    //index.delimitedEntries[index.delimitedEntriesSize++] = field;
                    
                    int[] indices = fieldMapping[field];
                    if(indices == null)
                    {
                        // first entry dependent on the field
                        indices = new int[INITIAL_CAPACITY_PER_FIELD_MAPPING];
                        fieldMapping[field] = indices;
                        
                        indices[0] = id;
                        continue;
                    }
                    
                    // check if the field is also present in the key.
                    int o = start;
                    
                    for(int f; o < separatorOffset; o++)
                    {
                        f = entry[o];
                        if(f < 0)
                            f = -f;
                        
                        if(field == f)
                            break;
                    }
                    
                    if(o != separatorOffset)
                    {
                        // the index key shares common fields with the index value
                        continue;
                    }
                    
                    // a different dependent entry from elsewhere was already added
                    // so start at the second slot
                    for(int slot = 1;;)
                    {
                        if(0 == indices[slot])
                        {
                            // fill this slot.
                            indices[slot] = id;
                            break;
                        }
                        
                        if(++slot == indices.length)
                        {
                            // too many dependent indices on a field that it 
                            // overflows INITIAL_INDEX_COUNT_PER_FIELD
                            fieldMapping[field] = growAndAppend(indices, id);
                            break;
                        }
                    }
                }
                
                if(entityFieldCount == 0)
                {
                    throw new IllegalArgumentException("The index " + id + 
                    		"cannot contain only $KEY/$DATE/$DATETIME.  " +
                    		"At least one entity field must be present or " +
                    		"the index value could be 0(the message itself).");
                }
            }
            
            /**
             * Returns the {@link Index} (this method should only be called once).
             */
            public Index build()
            {
                if(built)
                    throw new IllegalStateException("This builder has already been built previously.");
                
                int[] secondaryTagDupIndex = null;
                for(int i = 1, secondaryTags; i < secondaryTagEntries.length; i++)
                {
                    if(0 == (secondaryTags = secondaryTagEntries[i]))
                        continue;
                    
                    // has secondary tags
                    for(int shift = 0; shift <= 24; shift+=8)
                    {
                        int tag = 0xFF & (secondaryTags >>> shift);
                        if(tag == 0)
                        {
                            if(shift == 0)
                                throw new RuntimeException("Should not happen.");
                            
                            break;
                        }
                        
                        if(offsetEntries[tag] != Integer.MAX_VALUE)
                        {
                            throw new IllegalArgumentException("The tag index: " + tag + 
                                    " cannot be at the same time defined as a secondary tag.");
                        }
                        
                        if(secondaryTagDupIndex == null)
                            secondaryTagDupIndex = new int[224];
                        
                        secondaryTagDupIndex[tag]++;
                    }
                }
                
                if(secondaryTagDupIndex != null)
                {
                    for(int i = 1; i < secondaryTagDupIndex.length; i++)
                    {
                        if(secondaryTagDupIndex[i] > 1)
                        {
                            throw new IllegalArgumentException("The secondar tag: " + i + 
                                    " cannot be referenced more than once.");
                        }
                    }
                }
                
                built = true;
                
                return new Index(delimitedEntries, delimitedEntriesSize, 
                        valueEntries, valueCount, 
                        fieldMapping, offsetEntries, secondaryTagEntries);
            }
        }
    }
}
