//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuff.JniStream.TERMINATOR;

import java.io.IOException;

import protostuffdb.JniOps;

import com.dyuproject.protostuff.ByteString;
import com.dyuproject.protostuff.JniStream;

/**
 * Lsmdb jni util.
 * 
 * @author David Yu
 * @created Jan 9, 2014
 */
public final class LsmdbJniUtil
{
    private LsmdbJniUtil() {}
    
    public interface VisitorKV<P>
    {
        /**
         * Returning true will stop the traversal.
         */
        public boolean visit(byte[] k, int koffset, int klen, 
                byte[] v, int voffset, int vlen, 
                P param, int index);
    }
    
    private static RuntimeException err(int errType)
    {
        // TODO
        
        return new RuntimeException("Jni error: " + errType);
    }
    
    /* ================================================== */
    
    public static byte[] sget(final JniStream stream, int flags, 
            byte[] key, 
            byte[] toReturnIfFound)
    {
        return sget(stream, flags, 
                key, 0, key.length, 
                toReturnIfFound);
    }
    
    public static byte[] sget(final JniStream stream, int flags, 
            byte[] key, int keyoffset, int keylen, 
            byte[] toReturnIfFound)
    {
        try
        {
            stream.start(JniOps.SGET)
                    .write8(flags) // flags
                    .write(key, keyoffset, keylen) // key
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        stream.end();
        
        if(!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        final int offset = stream.offset(), 
                vlen = ValueUtil.toInt16LE(buf, offset);
        
        if(vlen == 0)
            return null;
        
        return toReturnIfFound != null ? toReturnIfFound : 
            ValueUtil.copy(buf, offset+2, vlen);
    }
    
    /* ================================================== */
    
    public static boolean iget(final JniStream stream, int flags, 
            byte[] key, int keyoffset, int keylen, 
            HasKV kv, boolean includeValue, boolean includeKey, boolean extractEntityKey)
    {
        if (!includeValue)
            flags |= JniOps.FGET_KEY_ONLY;
        
        try
        {
            stream.start(JniOps.IGET)
                    .write8(flags) // flags
                    .write(key, keyoffset, keylen) // key
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        stream.end();
        
        if (!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        final int offset = stream.offset(),
                klen = ValueUtil.toInt16LE(buf, offset);
        
        if (klen == 0)
            return false;
        
        if (kv == null)
            return true;
        
        int delimSize = 4;
        if (includeValue)
            kv.setValue(ValueUtil.copy(buf, offset + 4 + klen, ValueUtil.toInt16LE(buf, offset + 2)));
        else
            delimSize = 2;
        
        if (!includeKey)
            return true;
        
        if (extractEntityKey)
            kv.setKey(ValueUtil.copy(buf, offset + delimSize + klen - 9, 9));
        else
            kv.setKey(ValueUtil.copy(buf, offset + delimSize, klen));
        
        return true;
    }
    
    /* ================================================== */
    
    public static byte[] get(final JniStream stream, boolean end, int flags, 
            byte[] key, 
            byte[] toReturnIfFound)
    {
        return get(stream, end, flags, 
                key, 0, key.length, 
                toReturnIfFound);
    }
    
    public static byte[] get(final JniStream stream, boolean end, int flags, 
            byte[] key, int keyoffset, int keylen, 
            byte[] toReturnIfFound)
    {
        try
        {
            stream.start(JniOps.GET)
                    .write8(flags) // flags
                    .endHeader()
                    .writeK(key, keyoffset, keylen);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        if(end)
            stream.end();
        else
            stream.limit();
        
        if(!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        int offset = stream.offset(), 
                klen = ValueUtil.toInt16LE(buf, offset);
        
        offset += 2;
        
        if(klen == TERMINATOR)
        {
            // TODO prevent this from happening
            throw new RuntimeException("Not enough space for the native code to write.");
        }
        
        if(klen == 0)
        {
            // key not found
            
            // return index if end of writes
            if(offset + 2 > buf.length)
            {
                if(0 == stream.readWcOffset())
                    return null;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
            }
            
            // onto the next key
            klen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
            
            if(klen == TERMINATOR)
                return null;
            
            throw new RuntimeException("Expecting terminator.");
        }
        
        int vlen = ValueUtil.toInt16LE(buf, offset);
        offset += 2;
        
        byte[] value = toReturnIfFound != null ? toReturnIfFound : 
            ValueUtil.copy(buf, offset+klen, vlen);
        
        offset += (klen + vlen);
        
        // return index if end of writes
        if(offset + 2 > buf.length)
        {
            if(0 == stream.readWcOffset())
                return value;
            
            if(!stream.push())
                throw err(stream.getErrType());
            
            offset = stream.offset();
        }
        
        // onto the next key
        klen = ValueUtil.toInt16LE(buf, offset);
        offset += 2;
        
        if(klen == TERMINATOR)
            return value;
        
        throw new RuntimeException("Expecting terminator.");
    }
    
    /* ================================================== */
    
    public static <P> int multiGetKV(final JniStream stream, 
            final Visitor<P> visitor, final P param)
    {
        return visitKV(stream, visitor, param, true);
    }
    
    private static <P> int visitKV(final JniStream stream, 
            final Visitor<P> visitor, final P param, boolean multiGet)
    {
        if(!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        
        boolean continueVisit = true;
        
        for(int sessionCount = 0, 
                count = 0, 
                // read key len
                klen = ValueUtil.toInt16LE(buf, stream.offset()), 
                // update offset
                offset = 2 + stream.offset(), vlen;;)
        {
            if(klen == TERMINATOR)
            {
                if(multiGet && sessionCount == 0)
                {
                    // TODO prevent this from happening
                    throw new RuntimeException("Not enough space for the native code to write.");
                }
                
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                // reset
                sessionCount = 0;
                continue;
            }
            
            sessionCount++;
            
            if(klen == 0)
            {
                // key not found
                
                // return index if end of writes
                if(offset + 2 > buf.length)
                {
                    if(0 == stream.readWcOffset())
                        return count;
                    
                    if(!stream.push())
                        throw err(stream.getErrType());
                    
                    offset = stream.offset();
                }
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                continue;
            }
            
            vlen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
            
            if(continueVisit)
            {
                /*if(param == null)
                {
                    continueVisit = !visitor.visit(null, null, param, index++);
                }
                else if(extractEntityKey)
                {
                    continueVisit = !visitor.visit(ValueUtil.copy(buf, offset+klen-9, 9), 
                            buf, offset+klen, vlen, 
                            param, count++);
                }
                else
                {*/
                    continueVisit = !visitor.visit(ValueUtil.copy(buf, offset, klen), 
                            buf, offset+klen, vlen, 
                            param, count++);
                //}
            }
            
            offset += (klen + vlen);
            
            // return index if end of writes
            if(offset + 2 > buf.length)
            {
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
            }
            
            // onto the next key
            klen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
        }
    }
    
    /* ================================================== */
    
    public static <P> int multiGetKV(final JniStream stream, 
            final VisitorKV<P> visitor, final P param, boolean extractEntityKey)
    {
        return visitKV(stream, visitor, param, extractEntityKey, true);
    }
    
    private static <P> int visitKV(final JniStream stream, 
            final VisitorKV<P> visitor, final P param, boolean extractEntityKey, 
            boolean multiGet)
    {
        if(!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        
        boolean continueVisit = true;
        
        for(int sessionCount = 0, 
                count = 0, 
                // read key len
                klen = ValueUtil.toInt16LE(buf, stream.offset()), 
                // update offset
                offset = 2 + stream.offset(), vlen;;)
        {
            if(klen == TERMINATOR)
            {
                if(multiGet && sessionCount == 0)
                {
                    // TODO prevent this from happening
                    throw new RuntimeException("Not enough space for the native code to write.");
                }
                
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                // reset
                sessionCount = 0;
                continue;
            }
            
            sessionCount++;
            
            if(klen == 0)
            {
                // key not found
                
                // return index if end of writes
                if(offset + 2 > buf.length)
                {
                    if(0 == stream.readWcOffset())
                        return count;
                    
                    if(!stream.push())
                        throw err(stream.getErrType());
                    
                    offset = stream.offset();
                }
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                continue;
            }
            
            vlen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
            
            if(continueVisit)
            {
                if(extractEntityKey)
                {
                    continueVisit = !visitor.visit(buf, offset+klen-9, 9, 
                            buf, offset+klen, vlen, 
                            param, count++);
                }
                else
                {
                    continueVisit = !visitor.visit(buf, offset, klen, 
                            buf, offset+klen, vlen, 
                            param, count++);
                }
            }
            
            offset += (klen + vlen);
            
            // return index if end of writes
            if(offset + 2 > buf.length)
            {
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
            }
            
            // onto the next key
            klen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
        }
    }
    
    /* ================================================== */
    
    public static <P> int multiGetK(final JniStream stream, 
            final Visitor<P> visitor, final P param, boolean extractEntityKey)
    {
        return visitK(stream, visitor, param, extractEntityKey, true);
    }
    
    private static <P> int visitK(final JniStream stream, 
            final Visitor<P> visitor, final P param, boolean extractEntityKey, 
            boolean multiGet)
    {
        if(!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        
        boolean continueVisit = true;
        
        for(int sessionCount = 0, 
                count = 0, 
                // read key len
                klen = ValueUtil.toInt16LE(buf, stream.offset()), 
                // update offset
                offset = 2 + stream.offset();;)
        {
            if(klen == TERMINATOR)
            {
                if(multiGet && sessionCount == 0)
                {
                    // TODO prevent this from happening
                    throw new RuntimeException("Not enough space for the native code to write.");
                }
                
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                // reset
                sessionCount = 0;
                continue;
            }
            
            sessionCount++;
            
            if(klen == 0)
            {
                // key not found
                
                // return false if end of writes
                if(offset + 2 > buf.length)
                {
                    if(0 == stream.readWcOffset())
                        return count;
                    
                    if(!stream.push())
                        throw err(stream.getErrType());
                    
                    offset = stream.offset();
                }
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                continue;
            }
            
            if(continueVisit)
            {
                /*if(param == null)
                {
                    continueVisit = !visitor.visit(null, null, param, index++);
                }
                else */if(extractEntityKey)
                {
                    continueVisit = !visitor.visit(ValueUtil.copy(buf, offset+klen-9, 9), 
                            null, 0, 0, 
                            param, count++);
                }
                else
                {
                    continueVisit = !visitor.visit(ValueUtil.copy(buf, offset, klen), 
                            null, 0, 0, 
                            param, count++);
                }
            }
            
            offset += klen;
            
            // return false if end of writes
            if(offset + 2 > buf.length)
            {
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
            }
            
            // onto the next key
            klen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
        }
    }
    
    /* ================================================== */
    
    public static <P> int multiGetK(final JniStream stream, 
            final VisitorKV<P> visitor, final P param, boolean extractEntityKey)
    {
        return visitK(stream, visitor, param, extractEntityKey, true);
    }
    
    private static <P> int visitK(final JniStream stream, 
            final VisitorKV<P> visitor, final P param, boolean extractEntityKey, 
            boolean multiGet)
    {
        if(!stream.push())
            throw err(stream.getErrType());
        
        final byte[] buf = stream.buf();
        
        boolean continueVisit = true;
        
        for(int sessionCount = 0, 
                count = 0, 
                // read key len
                klen = ValueUtil.toInt16LE(buf, stream.offset()), 
                // update offset
                offset = 2 + stream.offset();;)
        {
            if(klen == TERMINATOR)
            {
                if(multiGet && sessionCount == 0)
                {
                    // TODO prevent this from happening
                    throw new RuntimeException("Not enough space for the native code to write.");
                }
                
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                // reset
                sessionCount = 0;
                continue;
            }
            
            sessionCount++;
            
            if(klen == 0)
            {
                // key not found
                
                // return false if end of writes
                if(offset + 2 > buf.length)
                {
                    if(0 == stream.readWcOffset())
                        return count;
                    
                    if(!stream.push())
                        throw err(stream.getErrType());
                    
                    offset = stream.offset();
                }
                
                // onto the next key
                klen = ValueUtil.toInt16LE(buf, offset);
                offset += 2;
                
                continue;
            }
            
            if(continueVisit)
            {
                if(extractEntityKey)
                {
                    continueVisit = !visitor.visit(
                            buf, offset+klen-9, 9, 
                            null, 0, 0, 
                            param, count++);
                }
                else
                {
                    continueVisit = !visitor.visit(
                            buf, offset, klen, 
                            null, 0, 0, 
                            param, count++);
                }
            }
            
            offset += klen;
            
            // return false if end of writes
            if(offset + 2 > buf.length)
            {
                if(0 == stream.readWcOffset())
                    return count;
                
                if(!stream.push())
                    throw err(stream.getErrType());
                
                offset = stream.offset();
            }
            
            // onto the next key
            klen = ValueUtil.toInt16LE(buf, offset);
            offset += 2;
        }
    }
    
    /* ================================================== */
    
    public static <P> int rangeDelete(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, 
            byte[] ek, 
            Visitor<P> visitor, P param)
    {
        return rangeDelete(stream, end, flags, 
                keysOnly, limit, desc, 
                sk, 0, sk.length, 
                ek, 0, ek.length, 
                visitor, param);
    }
    
    public static <P> int rangeDelete(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            Visitor<P> visitor, P param)
    {
        return range(stream, end, flags | JniOps.FSCAN_DELETE_RESULT, 
                keysOnly, limit, desc, 
                sk, skoffset, sklen, 
                ek, ekoffset, eklen, 
                visitor, param);
    }
    
    /* ================================================== */
    
    public static <P> int rangeDelete(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, 
            byte[] ek, 
            VisitorKV<P> visitor, P param)
    {
        return rangeDelete(stream, end, flags, 
                keysOnly, limit, desc, 
                sk, 0, sk.length, 
                ek, 0, ek.length, 
                visitor, param);
    }
    
    public static <P> int rangeDelete(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            VisitorKV<P> visitor, P param)
    {
        return range(stream, end, flags | JniOps.FSCAN_DELETE_RESULT, 
                keysOnly, limit, desc, 
                sk, skoffset, sklen, 
                ek, ekoffset, eklen, 
                visitor, param);
    }
    
    /* ================================================== */
    
    public static <P> int rangeScan(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, 
            byte[] ek, 
            Visitor<P> visitor, P param)
    {
        return rangeScan(stream, end, flags, 
                keysOnly, limit, desc, 
                sk, 0, sk.length, 
                ek, 0, ek.length, 
                visitor, param);
    }
    
    public static <P> int rangeScan(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            Visitor<P> visitor, P param)
    {
        return range(stream, end, flags, 
                keysOnly, limit, desc, 
                sk, skoffset, sklen, 
                ek, ekoffset, eklen, 
                visitor, param);
    }
    
    /* ================================================== */
    
    public static <P> int rangeScan(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, 
            byte[] ek, 
            VisitorKV<P> visitor, P param)
    {
        return rangeScan(stream, end, flags, 
                keysOnly, limit, desc, 
                sk, 0, sk.length, 
                ek, 0, ek.length, 
                visitor, param);
    }
    
    public static <P> int rangeScan(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            VisitorKV<P> visitor, P param)
    {
        return range(stream, end, flags, 
                keysOnly, limit, desc, 
                sk, skoffset, sklen, 
                ek, ekoffset, eklen, 
                visitor, param);
    }
    
    private static <P> int range(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            Visitor<P> visitor, P param)
    {
        if(keysOnly)
            flags |= JniOps.FSCAN_KEYS_ONLY;
        if(desc)
            flags |= JniOps.FSCAN_DESC;
        
        try
        {
            stream.start(JniOps.SCAN)
                .write8(flags) // flags
                .write16(limit); // limit
    
            if(desc)
            {
                stream.write(sk, skoffset, sklen) // limitKey
                    .write(ek, ekoffset, eklen); // startKey
            }
            else
            {
                stream.write(ek, ekoffset, eklen) // limitKey
                    .write(sk, skoffset, sklen); // startKey
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }

        stream.endHeader();
        
        if(end)
            stream.end();
        else
            stream.limit();
        
        return keysOnly ? visitK(stream, visitor, param, false, false) : 
            visitKV(stream, visitor, param, false);
    }
    
    /* ================================================== */
    
    private static <P> int range(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, 
            byte[] sk, int skoffset, int sklen, 
            byte[] ek, int ekoffset, int eklen, 
            VisitorKV<P> visitor, P param)
    {
        if(keysOnly)
            flags |= JniOps.FSCAN_KEYS_ONLY;
        if(desc)
            flags |= JniOps.FSCAN_DESC;
        
        try
        {
            stream.start(JniOps.SCAN)
                .write8(flags) // flags
                .write16(limit); // limit
    
            if(desc)
            {
                stream.write(sk, skoffset, sklen) // limitKey
                    .write(ek, ekoffset, eklen); // startKey
            }
            else
            {
                stream.write(ek, ekoffset, eklen) // limitKey
                    .write(sk, skoffset, sklen); // startKey
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }

        stream.endHeader();
        
        if(end)
            stream.end();
        else
            stream.limit();
        
        return keysOnly ? visitK(stream, visitor, param, false, false) : 
            visitKV(stream, visitor, param, false, false);
    }
    
    public static <P> int scan(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, byte[] limitKey, 
            Visitor<P> visitor, P param)
    {
        if(keysOnly)
            flags |= JniOps.FSCAN_KEYS_ONLY;
        if(desc)
            flags |= JniOps.FSCAN_DESC;
        
        try
        {
            stream.start(JniOps.SCAN)
                .write8(flags) // flags
                .write16(limit) // limit
                .write(limitKey) // limitKey
                .write(ByteString.EMPTY_BYTE_ARRAY); // startKey
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        stream.endHeader();
        
        if(end)
            stream.end();
        else
            stream.limit();
        
        return keysOnly ? visitK(stream, visitor, param, false, false) : 
            visitKV(stream, visitor, param, false);
    }
    
    public static <P> int scan(final JniStream stream, boolean end, int flags, 
            boolean keysOnly, int limit, boolean desc, byte[] limitKey, 
            VisitorKV<P> visitor, P param)
    {
        if(keysOnly)
            flags |= JniOps.FSCAN_KEYS_ONLY;
        if(desc)
            flags |= JniOps.FSCAN_DESC;
        
        try
        {
            stream.start(JniOps.SCAN)
                .write8(flags) // flags
                .write16(limit) // limit
                .write(limitKey) // limitKey
                .write(ByteString.EMPTY_BYTE_ARRAY); // startKey
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        stream.endHeader();
        
        if(end)
            stream.end();
        else
            stream.limit();
        
        return keysOnly ? visitK(stream, visitor, param, false, false) : 
            visitKV(stream, visitor, param, false, false);
    }
    
    public static void delete(final JniStream stream, boolean end, int flags, 
            byte[] key)
    {
        put(stream, end, flags, 
                key, 0, key.length, 
                ByteString.EMPTY_BYTE_ARRAY, 0, 0);
    }
    
    public static void delete(final JniStream stream, boolean end, int flags, 
            final byte[] k, int koffset, int klen)
    {
        put(stream, end, flags, 
                k, koffset, klen, 
                ByteString.EMPTY_BYTE_ARRAY, 0, 0);
    }
    
    public static void put(final JniStream stream, boolean end, int flags, 
            final byte[] k, final byte[] v)
    {
        put(stream, end, flags, 
                k, 0, k.length, 
                v, 0, v.length);
    }
    
    public static void put(final JniStream stream, boolean end, int flags, 
            final byte[] k, int koffset, int klen, 
            final byte[] v, int voffset, int vlen)
    {
        // flags
        //int flags = 0;
        /*boolean sync = false;
        if(sync)
            flags |= JniOps.FPUT_SYNC;*/
        
        try
        {
            stream.start(JniOps.PUT)
                    .write8(flags) // flags
                    .endHeader()
                    .writeKV(k, koffset, klen, v, voffset, vlen);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        if(end)
            stream.end();
        else
            stream.limit();
        
        if(!stream.push())
            throw err(stream.getErrType());
    }
    
    public static void sdelete(final JniStream stream, int flags, 
            byte[] key)
    {
        sput(stream, flags, 
                key, 0, key.length, 
                ByteString.EMPTY_BYTE_ARRAY, 0, 0);
    }
    
    public static void sdelete(final JniStream stream, int flags, 
            final byte[] k, int koffset, int klen)
    {
        sput(stream, flags, 
                k, koffset, klen, 
                ByteString.EMPTY_BYTE_ARRAY, 0, 0);
    }
    
    public static void sput(final JniStream stream, int flags, 
            final byte[] k, final byte[] v)
    {
        sput(stream, flags, 
                k, 0, k.length, 
                v, 0, v.length);
    }
    
    public static void sput(final JniStream stream, int flags, 
            final byte[] k, int koffset, int klen, 
            final byte[] v, int voffset, int vlen)
    {
        // flags
        //int flags = 0;
        /*boolean sync = false;
        if(sync)
            flags |= JniOps.FPUT_SYNC;*/
        
        try
        {
            stream.start(JniOps.SPUT)
                    .write8(flags) // flags
                    .write(k, koffset, klen) // key
                    .write(v, voffset, vlen) // value
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        stream.end();
        
        if(!stream.push())
            throw err(stream.getErrType());
    }
    
    public static JniStream writeHeaderDelete(final JniStream stream, int flags)
    {
        return writeHeaderPut(stream, flags);
    }
    
    public static JniStream writeHeaderPut(final JniStream stream, int flags)
    {
        try
        {
            stream.start(JniOps.PUT)
                    .write8(flags) // flags
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        return stream;
    }
    
    public static JniStream writeHeaderPut(final JniStream stream, int flags, 
            int kvaoSize)
    {
        try
        {
            stream.start(JniOps.PUT, kvaoSize)
                    .write8(flags) // flags
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        return stream;
    }
    
    public static void push(final JniStream stream)
    {
        if(!stream.push())
            throw err(stream.getErrType());
    }
    
    public static void close(final JniStream stream, int flags)
    {
        // flags
        //int flags = 0;
        /*boolean sync = false;
        if(sync)
            flags |= JniOps.FOP_CLOSE_SYNC;*/
        
        try
        {
            stream.start(JniOps.CLOSE)
                    .write8(flags) // flags
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        stream.end();
        
        if(!stream.push())
            throw err(stream.getErrType());
    }
    
    public static JniStream writeHeaderGet(final JniStream stream, int flags)
    {
        try
        {
            stream.start(JniOps.GET)
                    .write8(flags) // flags
                    .endHeader();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Should not happen.");
        }
        
        return stream;
    }
}
