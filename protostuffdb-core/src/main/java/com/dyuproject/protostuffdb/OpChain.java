//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import com.dyuproject.protostuff.KeyBuilder;
import com.dyuproject.protostuff.ds.CAS;


/**
 * Chainable operations that can be performed (write operations are atomic but must be 
 * performed within a single entity group)
 * 
 * @author David Yu
 * @created Mar 22, 2011
 */
public abstract class OpChain
{
    
    public final EntityMetadata<?> root;
    public final WriteContext context;
    private VisitorSession vs;
    
    public OpChain(EntityMetadata<?> root, WriteContext context, VisitorSession vs)
    {
        //assert root != null;
        assert context != null;
        
        this.root = root;
        this.context = context;
        this.vs = vs;
    }
    
    /**
     * Returns the visitor session.
     */
    public final VisitorSession vs()
    {
        VisitorSession vs = this.vs;
        
        if(vs == null)
            this.vs = vs = newVisitorSession();
        
        return vs;
    }
    
    /**
     * Returns a new visitor session which could be used 
     * to do conditional (atomic) writes.
     */
    protected abstract VisitorSession newVisitorSession();
    
    /**
     * Basic put.
     */
    public final void put(String key, byte[] value)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        rawPut(kbuf, koffset, klen, value, 0, value.length);
    }
    
    /**
     * Basic put.
     */
    public final void put(String key, byte[] value, int offset, int len)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        rawPut(kbuf, koffset, klen, value, offset, len);
    }
    
    /**
     * Basic put.
     */
    public final void put(byte[] key, byte[] value)
    {
        rawPut(KeyUtil.newPrefixedKey(key), 0, 1+key.length, value, 0, value.length);
    }
    
    /**
     * Basic put.
     */
    public final void put(byte[] key, byte[] value, int offset, int len)
    {
        rawPut(KeyUtil.newPrefixedKey(key), 0, 1+key.length, value, offset, len);
    }
    
    /**
     * Basic delete.
     */
    public final void delete(String key)
    {
        final KeyBuilder kb = context.kb.appendNOLC(key).push();
        final byte[] kbuf = kb.buf();
        // prefix with 0x80
        int koffset = kb.offset(), klen = 1 + kb.popLen();
        kbuf[--koffset] = (byte)0x80;
        
        rawDelete(kbuf, koffset, klen);
    }
    
    /**
     * Basic delete.
     */
    public final void delete(byte[] key)
    {
        rawDelete(KeyUtil.newPrefixedKey(key), 0, 1+key.length);
    }
    
    /**
     * Raw put.
     */
    public final void rawPut(byte[] key, byte[] value)
    {
        rawPut(key, 0, key.length, value, 0, value.length);
    }
    
    /**
     * Raw delete.
     */
    public final void rawDelete(byte[] key)
    {
        rawDelete(key, 0, key.length);
    }
    
    /**
     * Raw put.
     */
    public abstract void rawPut(byte[] key, int koffset, int klen, 
            byte[] value, int voffset, int vlen);
    
    /**
     * Raw delete.
     */
    public abstract void rawDelete(byte[] key, int koffset, int klen);
    
    
    /**
     * Remove/Delete the results returned by the range.
     */
    public abstract int rangeRemove(byte[] startKey, byte[] endKey);
    
    /**
     * Replace the prefix of the results returned by the range.
     */
    public abstract int rangeReplace(byte[] startKey, byte[] endKey, 
            byte[] prefix, int offset, int len);
    
    // ======================================================================
    
    /**
     * Moves on to the next operation if the entity is successfully inserted.
     * 
     * @param parentKey is optional.
     */
    public final <T,P> boolean insert(T entity, EntityMetadata<T> em, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        return insert(context.newEntityKey(em), entity, em, parentKey, 
                nextOp, param);
    }
    
    /**
     * Moves on to the next operation if the entity is successfully inserted.
     * 
     * @param parentKey is optional.
     */
    public final <T,P> boolean insertWithKey(byte[] key, T entity, EntityMetadata<T> em, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        KeyUtil.validateSameKind(key, em);
        return insert(key, entity, em, parentKey, 
                nextOp, param);
    }
    
    /**
     * Moves on to the next operation if the entity is successfully inserted.
     * 
     * @param parentKey is optional.
     */
    private final <T,P> boolean insert(byte[] key, T entity, EntityMetadata<T> em, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                break;
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null");
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                break;
                
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Insert operation not applicable for type: " + em.type);
        }
        
        final int size = em.indexOnFields ? context.serAndIndexCollect(entity, em) : 
            context.ser(entity, em);
        
        return insert(key, em, parentKey, 
                context.serAndIndexCollectOutput, 
                context.entityBuffer, context.entityOffset, size, 
                nextOp, param);
    }
    
    /**
     * Moves on to the next operation if the entity is successfully inserted.
     * 
     * @param parentKey is optional.
     */
    protected abstract <T,P> boolean insert(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, 
            SerAndIndexCollectOutput output, 
            byte[] entityBuffer, int entityOffset, int size, 
            Op<P> nextOp, P param);
    
    /**
     * Returns the serialized value being referenced internally.
     * 
     * @param parentKey is optional.
     */
    public final <T> byte[] parentRef(T entity, EntityMetadata<T> em)
    {
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        final int size = em.indexOnFields ? context.serAndIndexCollect(entity, em, 
                context._serAndIndexCollectOutput) : context.ser(entity, em);
        
        final byte[] data = ValueUtil.copy(context.entityBuffer, context.entityOffset, size);
        context.data(em.kind, data);
        
        return data;
    }
    
    /**
     * Moves on to the next operation if the previously serialized value is inserted.
     */
    public final <T,P> boolean parentInsert(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param)
    {
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null");
                
                break;
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null");
                
                KeyUtil.validateSameKind(parentKey, em.parent);
                break;
                
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Insert operation not applicable for type: " + em.type);
        }
        
        final byte[] data = context.data(em.kind);
        if(data == null)
        {
            throw DSRuntimeExceptions.invalidArg(
                    "#parentRef() needs to be called before calling #parentInsert().");
        }
        
        return insert(key, em, parentKey, 
                context._serAndIndexCollectOutput, 
                data, 0, data.length, 
                nextOp, param);
    }
    
    /**
     * Moves on to the next operation if the entry is deleted or have previously 
     * been deleted.
     * 
     * @param cascadeResource deletes will cascade if provided.
     * @param parentKey is optional.
     */
    public final <T,P> boolean delete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param)
    {
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null.");
                
                KeyUtil.validateSameKind(key, em);
                break;
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null.");
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                break;
            
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Delete operation not applicable for type: " + em.type);
        }
        
        return doDelete(key, em, parentKey, nextOp, param);
    }
    
    /**
     * Moves on to the next operation if the entry is deleted or have previously 
     * been deleted.
     * 
     * @param cascadeResource deletes will cascade if provided.
     * @param parentKey is optional.
     */
    public final <T,P> boolean deleteWithValue(byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param)
    {
        if (root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        switch(em.type)
        {
            case DEFAULT:
                if (parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null.");
                
                KeyUtil.validateSameKind(key, em);
                break;
                
            case LINKED_CHILD:
                if (parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null.");
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                break;
            
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Delete operation not applicable for type: " + em.type);
        }
        
        return doDeleteWithValue(value, key, em, parentKey, nextOp, param);
    }
    
    /**
     * Moves on to the next operation if the entry is deleted or have previously 
     * been deleted.
     * 
     * @param cascadeResource deletes will cascade if provided.
     * @param parentKey is optional.
     */
    protected abstract <T,P> boolean doDelete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param);
    
    /**
     * Moves on to the next operation if the entry is deleted or have previously 
     * been deleted.
     * 
     * @param cascadeResource deletes will cascade if provided.
     * @param parentKey is optional.
     */
    protected abstract <T,P> boolean doDeleteWithValue(final byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param);
    
    /**
     * Moves on to the next operation if the entry is deleted or have previously 
     * been deleted.
     * 
     * Cascade ops should not use the cursor (VisitorSession).
     * Its a good way to remove tags where RMW is not required.
     * 
     * @param cascadeResource is required.
     * @param parentKey is optional.
     */
    public final <T,P,C> boolean cascadeDelete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param, 
            EntityMetadataResource cascadeResource, Op<C> cascadeOp, C cascadeParam)
    {
        if(!em.hasChildren())
        {
            throw DSRuntimeExceptions.invalidArg("The entity: " + 
                    em.pipeSchema.messageFullName() + " does not have children.");
        }
        
        if(cascadeResource == null)
            throw DSRuntimeExceptions.invalidArg("cascadeResource must not be null.");
        
        //if(root == null)
        //    throw DSRuntimeExceptions.invalidArg("cascade ops cannot be operated without an entity root.");
        
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null.");
                
                KeyUtil.validateSameKind(key, em);
                break;
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null.");
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                break;
            
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Delete operation not applicable for type: " + em.type);
        }
        
        return doCascadeDelete(key, em, parentKey, nextOp, param, 
                cascadeResource, cascadeOp, cascadeParam);
    }
    
    /**
     * Moves on to the next operation if the entry is deleted or have previously 
     * been deleted.
     * 
     * Cascade ops should not use the cursor (VisitorSession).
     * Its a good way to remove tags where RMW is not required.
     * 
     * @param cascadeResource is required.
     * @param parentKey is optional.
     */
    protected abstract <T,P,C> boolean doCascadeDelete(byte[] key, EntityMetadata<T> em, 
            byte[] parentKey, Op<P> nextOp, P param, 
            EntityMetadataResource cascadeResource, Op<C> cascadeOp, C cascadeParam);
    
    /**
     * Moves on to the next operation if the atomic CAS operations are successful.
     * 
     * @param parentKey is optional.
     */
    public final <T,P> boolean update(byte[] key, EntityMetadata<T> em, 
            CAS cas, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        return update(key, em, cas, null, parentKey, nextOp, param);
    }
    
    /**
     * Updates the entity with the pre-fetched (caller validation most likely) value.
     * Moves on to the next operation if the atomic CAS operations are successful.
     * 
     * @param parentKey is optional.
     */
    public final <T,P> boolean updateWithValue(byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            CAS cas, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        return updateWithValue(value, key, em, cas, null, parentKey, nextOp, param);
    }
    
    /**
     * Updates the entity with the pre-fetched (caller validation most likely) value.
     * Moves on to the next operation if the atomic CAS operations are successful.
     * 
     * @param listener is optional.
     * @param parentKey is optional.
     */
    public final <T,P> boolean updateWithValue(byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        if(cas.isEmpty())
            throw DSRuntimeExceptions.invalidArg("CAS has no operations to perform");
        
        try
        {
            return doUpdate(value, key, em, cas, listener, parentKey, nextOp, param);
        }
        catch (ClassCastException e)
        {
            throw DSRuntimeExceptions.invalidArg(
                    "CAS fields not in sync with the schema: " + 
                    em.pipeSchema.wrappedSchema.messageFullName());
        }
    }
    
    /**
     * Moves on to the next operation if the atomic CAS operations are successful.
     * 
     * @param listener is optional.
     * @param parentKey is optional.
     */
    public final <T,P> boolean update(byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener, 
            byte[] parentKey, 
            Op<P> nextOp, P param)
    {
        if(root != null && root != em.root)
            throw DSRuntimeExceptions.invalidArg("Operation not performed within entity group.");
        
        if(cas.isEmpty())
            throw DSRuntimeExceptions.invalidArg("CAS has no operations to perform");
        
        final byte[] value;
        switch(em.type)
        {
            case DEFAULT:
                if(parentKey != null)
                    throw DSRuntimeExceptions.invalidArg("parentKey not null.");
                
                KeyUtil.validateSameKind(key, em);
                value = vs().rawGet(key);
                break;
                
            case LINKED_CHILD:
                if(parentKey == null)
                    throw DSRuntimeExceptions.invalidArg("parentKey null.");
                
                KeyUtil.validateSameKind(key, em);
                KeyUtil.validateSameKind(parentKey, em.parent);
                value = vs().rawGet(KeyUtil.newOneToManyKey(key, parentKey));
                break;
                
            default:
                throw DSRuntimeExceptions.invalidArg(
                        "Update operation not applicable for type: " + em.type);
        }
        
        if (value == null)
            return false;
        
        try
        {
            return doUpdate(value, key, em, cas, listener, parentKey, nextOp, param);
        }
        catch(ClassCastException e)
        {
            throw DSRuntimeExceptions.invalidArg(
                    "CAS fields not in sync with the schema: " + 
                    em.pipeSchema.wrappedSchema.messageFullName());
        }
    }
    
    /**
     * Moves on to the next operation if the atomic CAS operations are successful.
     * 
     * @param listener is optional.
     * @param parentKey is optional.
     */
    protected abstract <T,P> boolean doUpdate(byte[] value, 
            byte[] key, EntityMetadata<T> em, 
            CAS cas, CAS.Listener listener, 
            byte[] parentKey, 
            Op<P> nextOp, P param);

}
