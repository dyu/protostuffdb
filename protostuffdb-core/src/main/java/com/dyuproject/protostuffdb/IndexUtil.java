//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuff.WireFormat.WIRETYPE_FIXED32;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_FIXED64;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_LENGTH_DELIMITED;
import static com.dyuproject.protostuff.WireFormat.WIRETYPE_VARINT;
import static com.dyuproject.protostuff.WireFormat.getTagWireType;
import static com.dyuproject.protostuffdb.DateTimeUtil.startOfDayMS;
import static com.dyuproject.protostuffdb.EntityMetadata.$DATE;
import static com.dyuproject.protostuffdb.EntityMetadata.$DATETIME;
import static com.dyuproject.protostuffdb.EntityMetadata.FT_BOOL;
import static com.dyuproject.protostuffdb.EntityMetadata.FT_FIXED32;

import java.io.IOException;

import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuff.WireFormat.FieldType;
import com.dyuproject.protostuffdb.EntityMetadata.Index;

/**
 * Index utils.
 * 
 * @author David Yu
 * @created Sep 9, 2012
 */
public final class IndexUtil
{
    
    private IndexUtil() {}
    
    // 0 tag_id (field separator?)* field           <-- tag index key
    
    // 0 index_id kind (field separator?)* field    <-- entity composite index key
    
    /**
     * Writes the index key to the buffer and returns the size.
     * 
     * The value size (if keysOnly is false) will be written to the 
     * {@code valueSizeResult} array.
     */
    public static int writeIndexKeyTo(final byte[] buf, int offset, 
            final EntityMetadata<?> em, final WriteContext context, 
            final int id, final int[] fieldOffsetIdx, 
            final int[] varint32Idx, final long[] varint64Idx, 
            final byte[] key, final int koffset, 
            final byte[] value, final int voffset, final int vlen, 
            final int[] valueSizeResult, final boolean keysOnly) // result (store the size on the array)
    {
        final Index index = em.index;
        int i = 0, eoffset = index.offsetEntries[id]; // pointer
        
        // negative eoffset means the key consists of mutable fields
        if(eoffset < 0)
            eoffset = -eoffset;
        
        final int start = offset, count = index.delimitedEntries[eoffset++]; //fields to include in the index
        int field = index.delimitedEntries[eoffset];
        
        buf[offset++] = 0;
        buf[offset++] = (byte)id;
        
        if(id > 223)
        {
            // entity composite index (belongs to an entity)
            buf[offset++] = (byte)em.kind;
        }
        
        while(field != 0)
        {
            boolean positive = true;
            if(field < 0)
            {
                // negate to get actual field
                field = -field;
                positive = false;
            }
            
            if(0 != (0xFF00 & field))
            {
                // parent data
                final byte[] data = context.data(em.parent.kind);
                if(data == null)
                {
                    throw new RuntimeException(em.pipeSchema.messageName() + 
                            " needs the data set from its parent: " + 
                            em.parent.pipeSchema.messageName());
                }
                final int dataOffset = 0, dataLen = data.length;
                
                final int voLen = field & 0x0F;
                switch(voLen)
                {
                    case 0:
                        break;
                    case 1: // bool/onebyte-uint32/onebyte-enum
                        buf[offset++] = data[dataOffset + dataLen - (field >>> 8)];
                        break;
                        
                    case 4: // fixed32
                    case 6: // date/datetime
                    case 8: // fixed64
                        // little endian to big endian
                        for(int j = 0, //vo = field >>> 8, 
                                // + voLen moves the vstart to last + 1 (which is why: --vstart)
                                vstart = dataOffset + dataLen - (field >>> 8) + voLen; 
                                j < voLen; j++)
                        {
                            buf[offset++] = data[--vstart];
                        }
                        break;
                        
                    case 9: // bytes field that ends with _key
                        System.arraycopy(data, dataOffset + dataLen - (field >>> 8), buf, offset, 9);
                        offset += 9;
                        break;
                    
                    default:
                        throw new RuntimeException("Invalid vo config");
                }
                
                if(0 != voLen)
                {
                    if(positive)
                        buf[offset++] = 0;
                    if(++i == count)
                        return offset - start;
                    
                    field = index.delimitedEntries[++eoffset];
                    continue;
                }

                // parent field number
                final int fieldType = 0x0F & (field >>> 4);
                field = field >>> 8;
                                  
                boolean found = false;
                final DSByteArrayInput input = context.dsPipe.input.reset(data, 
                        dataOffset, dataLen);
                try
                {
                    for(int number = input.readFieldNumber(null); number != 0; 
                            number = input.readFieldNumber(null))
                    {
                        if(number == field)
                        {
                            offset += writeFieldTo(buf, offset, em.parent, field, 
                                    input.lastTag, true, false, fieldType, 
                                    data, input.offset);
                            
                            found = true;
                            break;
                        }
                        
                        input.handleUnknownField(number, null);
                    }
                }
                catch(IOException e)
                {
                    throw new RuntimeException("Should never happen.", e);
                }
                finally
                {
                    input.clear();
                }
                
                if(!found)
                {
                    throw new RuntimeException(em.parent.pipeSchema.messageFullName() + 
                            " does not have a field: " + field);
                }
                
                if(positive)
                    buf[offset++] = 0;
                if(++i == count)
                    return offset - start;
                
                field = index.delimitedEntries[++eoffset];
                continue;
            }
            
            switch(field & 0x7f)
            {
                case 0:
                    if(positive)
                    {
                        // key
                        System.arraycopy(key, koffset, buf, offset, 9);
                        offset += 9;
                    }
                    else
                    {
                        // parent key
                        
                        // parent kind
                        buf[offset++] = (byte)(key[koffset-9] & 0x7f);
                        
                        // parent timestamp and counter
                        System.arraycopy(key, koffset-8, buf, offset, 8);
                        offset += 8;
                    }
                    
                    if(++i == count)
                        return offset - start;
                    
                    field = index.delimitedEntries[++eoffset];
                    
                    continue;
                    
                case $DATE:
                    // extract the 6-byte datetime, convert to date and write it
                    KeyUtil.writeTimestamp(startOfDayMS(KeyUtil.readTimestamp(key, 
                            // parent's date if negative
                            positive ? (koffset+1) : (koffset-8))), 
                            buf, offset);
                    offset += 6;
                    
                    if(++i == count)
                        return offset - start;
                    
                    field = index.delimitedEntries[++eoffset];
                    
                    continue;
                    
                case $DATETIME:
                    // extract the 6-byte datetime and write it
                    System.arraycopy(key, 
                            // parent's datetime if negative
                            positive ? (koffset+1) : (koffset-8), 
                            buf, offset, 6);
                    offset += 6;
                    
                    if(++i == count)
                        return offset - start;
                    
                    field = index.delimitedEntries[++eoffset];
                    
                    continue;
            }
            
            int fvoffset = fieldOffsetIdx[field];
            
            if(fvoffset < 0)
            {
                // varint, relative offset not used
                int off = -fvoffset, off64 = varint32Idx[off++];
                if(off64 == -1)
                {
                    // int32
                    IntSerializer.writeInt32(varint32Idx[++off], buf, offset);
                    offset += 4;
                }
                else
                {
                    // int64
                    IntSerializer.writeInt64(varint64Idx[off64], buf, offset);
                    offset += 8;
                }
                
                if(positive)
                    buf[offset++] = 0;
                if(++i == count)
                    return offset - start;
                
                field = index.delimitedEntries[++eoffset];
                continue;
            }
            
            // relative offset
            fvoffset += voffset;
            
            int tag = value[fvoffset++] & 0xFF;
            if(tag > 0x7f)
                tag = (tag & 0x7f) | (value[fvoffset++] << 7);
            
            offset += writeFieldTo(buf, offset, em, field, tag, true, false, FT_BOOL, 
                    value, fvoffset);
            
            if(positive)
                buf[offset++] = 0;
            if(++i == count)
                return offset - start;
            
            field = index.delimitedEntries[++eoffset];
        }
        
        // zero field
        
        if(keysOnly)
            return offset - start;
        
        if(++i == count)
        {
            // the message itself
            valueSizeResult[0] = -1;
            return offset - start;
        }
        
        // has custom values.
        
        // move to the value field
        eoffset++;
        
        final int valueStart = offset;
        final boolean includeTag = 0 == index.delimitedEntries[eoffset];
        
        if(includeTag)
        {
            // current offset at the second zero
            i++;
            eoffset++;
        }
        
        for(int fvoffset = 0, tag = 0; i < count; i++) // no separator
        {
            field = index.delimitedEntries[eoffset++];
            
            switch(field & 0x7f)
            {
                case 0:
                    // key
                    assert !includeTag;
                    
                    if(field > 0)
                    {
                        // key
                        System.arraycopy(key, koffset, buf, offset, 9);
                        offset += 9;
                    }
                    else
                    {
                        // parent key
                        
                        // parent kind
                        buf[offset++] = (byte)(key[koffset-9] & 0x7f);
                        
                        // parent timestamp and counter
                        System.arraycopy(key, koffset-8, buf, offset, 8);
                        offset += 8;
                    }
                    
                    continue;
                    
                case $DATE:
                    assert !includeTag;
                    
                    // extract the 6-byte datetime, convert to date and write it
                    KeyUtil.writeTimestamp(startOfDayMS(em.seq ? ValueUtil.toInt64LE(
                            value, voffset + 1) : KeyUtil.readTimestamp(key, koffset+1)), 
                            buf, offset);
                    offset += 6;
                    
                    continue;
                    
                case $DATETIME:
                    assert !includeTag;
                    
                    // extract the 6-byte datetime and write it
                    if (em.seq)
                    {
                        // ts is the first field with the value at index 1 (after the field number)
                        KeyUtil.writeTimestamp(ValueUtil.toInt64LE(value, voffset + 1), 
                                buf, offset);
                    }
                    else
                    {
                        System.arraycopy(key, koffset+1, buf, offset, 6);
                    }
                    offset += 6;
                    
                    continue;
            }
            
            fvoffset = fieldOffsetIdx[field];
            
            if(fvoffset < 0)
            {
                // varint
                offset += writeVarIntTo(buf, offset, includeTag, 
                        varint32Idx, varint64Idx, value, voffset, fvoffset);
                continue;
            }
            
            // relative offset
            fvoffset += voffset;
            
            if(includeTag)
            {
                buf[offset++] = value[fvoffset];
                tag = value[fvoffset++] & 0xFF;
                
                if(tag > 0x7f)
                {
                    buf[offset++] = value[fvoffset];
                    tag = (tag & 0x7f) | (value[fvoffset++] << 7);
                }
            }
            else
            {
                tag = value[fvoffset++] & 0xFF;
                if(tag > 0x7f)
                    tag = (tag & 0x7f) | (value[fvoffset++] << 7);
            }
            
            offset += writeFieldTo(buf, offset, em, field, tag, false, includeTag, FT_BOOL,  
                    value, fvoffset);
        }
        
        valueSizeResult[0] = offset - valueStart;
        
        return valueStart - start;
    }
    
    static int writeVarIntTo(final byte[] buf, int offset, boolean includeTag, 
            final int[] varint32Idx, final long[] varint64Idx, 
            final byte[] value, int voffset, int fvoffset)
    {
        int off = -fvoffset, off64 = varint32Idx[off++];
        if(!includeTag)
        {
            if(off64 == -1)
            {
                // int32
                IntSerializer.writeInt32(varint32Idx[++off], buf, offset);
                return 4;
            }
            
            // int64
            IntSerializer.writeInt64(varint64Idx[off64], buf, offset);
            return 8;
        }
        
        final int start = offset;
        
        // tag offset (relative offset)
        fvoffset = voffset + varint32Idx[off];
        
        buf[offset++] = value[fvoffset];
        int tag = value[fvoffset++] & 0xFF;
        
        if(tag > 0x7f)
        {
            buf[offset++] = value[fvoffset];
            tag = (tag & 0x7f) | (value[fvoffset++] << 7);
        }
        
        offset += copyVarIntTo(buf, offset, value, fvoffset);
        
        return offset - start;
    }
    
    static int writeFieldTo(final byte[] buf, int offset, 
            final EntityMetadata<?> em, final int field, final int tag, 
            final boolean indexKey, final boolean includeFieldTag, 
            final int fieldTypeVarInt, 
            final byte[] value, int fvoffset)
    {
        switch(getTagWireType(tag))
        {
            case WIRETYPE_VARINT:
                if(fieldTypeVarInt == FT_BOOL)
                {
                    buf[offset] = value[fvoffset];
                    return 1;
                }
                
                if(fieldTypeVarInt < FT_FIXED32)
                {
                    IntSerializer.writeInt32(readRawVarint32(value, fvoffset), buf, offset);
                    return 4;
                }
                
                IntSerializer.writeInt64(readRawVarint64(value, fvoffset), buf, offset);
                return 8;
                
            case WIRETYPE_FIXED64:
            {
                int len = 8;
                // little endian
                // so we write it as big endian
                if(includeFieldTag || !em.isDateOrDatetime(field))
                {
                    // include the upper 2 bytes
                    buf[offset++] = value[fvoffset+7];
                    buf[offset++] = value[fvoffset+6];
                }
                else
                {
                    len -= 2;
                }
                
                buf[offset++] = value[fvoffset+5];
                buf[offset++] = value[fvoffset+4];
                buf[offset++] = value[fvoffset+3];
                buf[offset++] = value[fvoffset+2];
                buf[offset++] = value[fvoffset+1];
                buf[offset] = value[fvoffset];
                
                return len;
            }
                
            case WIRETYPE_LENGTH_DELIMITED:
            {
                // strings must be lesser than 16kb
                int size, delimSize = 0;
                if(includeFieldTag)
                {
                    buf[offset++] = value[fvoffset];
                    size = value[fvoffset++] & 0xFF;
                    
                    delimSize++;
                    
                    if(size > 0x7f)
                    {
                        buf[offset++] = value[fvoffset];
                        size = (size & 0x7f) | (value[fvoffset++] << 7);
                        
                        delimSize++;
                    }
                }
                else
                {
                    size = value[fvoffset++] & 0xFF;
                    if(size > 0x7f)
                        size = (size & 0x7f) | (value[fvoffset++] << 7);
                }

                
                if(!indexKey || FieldType.STRING != em.getFieldType(field))
                {
                    System.arraycopy(value, fvoffset, buf, offset, size);
                    return delimSize + size;
                }
                
                for(int j = 0; j < size; j++)
                {
                    byte b = value[fvoffset++];
                    // convert to lower case for index keys
                    buf[offset++] = b > 64 && b < 91 ? (byte)(b + 32) : b;
                }
                
                return delimSize + size;
            }
                
            case WIRETYPE_FIXED32:
                // little endian
                // so we write it as big endian
                buf[offset++] = value[fvoffset+3];
                buf[offset++] = value[fvoffset+2];
                buf[offset++] = value[fvoffset+1];
                buf[offset] = value[fvoffset];
                
                return 4;
                
            default:
                throw new RuntimeException(
                        "Groups, references and tail delimiters are not supported");
        }
    }
    
    /**
     * Copies the serialized varint to the buffer and returns the size.
     */
    static int copyVarIntTo(final byte[] buf, int offset, 
            final byte[] value, int voffset)
    {
        for(int i = 0; 10 > i++;)
        {
            if(0 <= (buf[offset++] = value[voffset++]))
                return i;
        }
        
        throw DSRuntimeExceptions.runtime("Malformed varint.");
    }
    
    /**
     * Reads a var int 32 from the internal byte buffer.
     */
    static int readRawVarint32(byte[] buf, int offset)
    {
        byte tmp = buf[offset++];
        if (tmp >= 0)
        {
            return tmp;
        }
        int result = tmp & 0x7f;
        if ((tmp = buf[offset++]) >= 0)
        {
            result |= tmp << 7;
        }
        else
        {
            result |= (tmp & 0x7f) << 7;
            if ((tmp = buf[offset++]) >= 0)
            {
                result |= tmp << 14;
            }
            else
            {
                result |= (tmp & 0x7f) << 14;
                if ((tmp = buf[offset++]) >= 0)
                {
                    result |= tmp << 21;
                }
                else
                {
                    result |= (tmp & 0x7f) << 21;
                    result |= (tmp = buf[offset++]) << 28;
                    if (tmp < 0)
                    {
                        // Discard upper 32 bits.
                        for (int i = 0; i < 5; i++)
                        {
                            if (buf[offset++] >= 0)
                            {
                                return result;
                            }
                        }
                        throw new RuntimeException("Invalid varint32.");
                    }
                }
            }
        }
        return result;
    }
    
    /** Reads a var int 64 from the internal byte buffer. */
    static long readRawVarint64(byte[] buf, int offset)
    {
        long result = 0;
        for (int shift = 0; shift < 64; shift+=7)
        {
            final byte b = buf[offset++];
            result |= (long)(b & 0x7F) << shift;
            if (0 == (0x80 & b))
                return result;
        }
        throw new RuntimeException("Invalid varint64.");
    }
    
}
