//========================================================================
//Copyright 2017 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Arrays;

import com.dyuproject.protostuff.RpcResponse;

/**
 * Stores the entries in a contiguous byte array and grows by segment.
 * If the entity has variable-length fields, they must be immutable.
 * 
 * The entry format is: length(value + key) + value + key
 * 
 * @author David Yu
 * @created Jul 10, 2017
 */
public final class SerialArrayCache implements Cache
{
    public static final int VALUE_SIZE_LIMIT = 0xFFFF - 2 - 2 - 9; // buf delim size, val delim size, key size

    static int[][] appendTo(int[][] array, int index, int[] value)
    {
        if (index == array.length)
            array = Arrays.copyOf(array, (array.length * 3)/2 + 1);
        
        array[index] = value;
        
        return array;
    }
    
    public final int voId;
    public final int segmentSize;
    public final int maxValueSize;
    private volatile int[][] offsets = new int[10][];
    private volatile byte[][] entries = new byte[10][];
    private volatile int size = 0, offsetsLen = 1, entriesIdx = 0, entriesLen = 1;
    
    public SerialArrayCache(int voId)
    {
        this(voId, 16384/*0x4000*/, 0);
    }

    public SerialArrayCache(int voId, int segmentSize)
    {
        this(voId, segmentSize, 0);
    }

    public SerialArrayCache(int voId, int segmentSize, int maxValueSize)
    {
        if (segmentSize < 8)
            throw new IllegalArgumentException("Min segment size is 8.");
        if (maxValueSize < 0)
            throw new IllegalArgumentException("Invalid max value size.");
        if (maxValueSize > VALUE_SIZE_LIMIT)
        {
            throw new IllegalArgumentException("Max value size exceeds limit: " +
                    maxValueSize + " > " + VALUE_SIZE_LIMIT);
        }
        
        this.voId = voId;
        this.segmentSize = segmentSize;
        this.maxValueSize = maxValueSize;

        offsets[0] = new int[segmentSize];
        entries[0] = new byte[0xFFFF];
    }
    
    public int newId()
    {
        return 1 + size;
    }
    
    public int size()
    {
        return size;
    }
    
    public void clear()
    {
        size = 0;
        entriesIdx = 0;
        byte[] buf = entries[0];
        buf[0] = 0;
        buf[1] = 0;
        // zero out the initial offsets
        Arrays.fill(offsets[0], 0);
    }
    
    /**
     * Gets the key of the entry and stores the offset into {@link WriteContext#vo}.
     * Returns null if not found.
     */
    public byte[] $k(int id, WriteContext context)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        if (segmentIdx >= offsetsLen)
            return null;
        
        final int offset = offsets[segmentIdx][index % segmentSize];
        
        if (offset == 0)
            return null;
        
        final byte[] buf = entries[offset >>> 16]; // slot
        final int bufOffset = offset & 0xFFFF,
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
        
        context.$offset = bufOffset + entryLen - 9;
        return buf;
    }
    
    /**
     * Gets the value of the entry and stores the offset into {@link WriteContext#vo}
     * as well the size into {@link WriteContext#type}.
     * Returns null if not found.
     */
    public byte[] $v(int id, WriteContext context)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        if (segmentIdx >= offsetsLen)
            return null;
        
        final int offset = offsets[segmentIdx][index % segmentSize];
        
        if (offset == 0)
            return null;
        
        final byte[] buf = entries[offset >>> 16]; // slot
        final int bufOffset = offset & 0xFFFF,
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
        
        context.$offset = bufOffset + 2;
        context.$len = entryLen - 2 - 9;
        return buf;
    }
    
    /**
     * Gets the value and key (concatenated in that order) of the entry 
     * and stores the offset into {@link WriteContext#vo}
     * as well as the size into {@link WriteContext#type}.
     * Returns null if not found.
     */
    public byte[] $vk(int id, WriteContext context)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        if (segmentIdx >= offsetsLen)
            return null;
        
        final int offset = offsets[segmentIdx][index % segmentSize];
        
        if (offset == 0)
            return null;
        
        final byte[] buf = entries[offset >>> 16]; // slot
        final int bufOffset = offset & 0xFFFF,
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
        
        context.$offset = bufOffset + 2;
        context.$len = entryLen - 2;
        return buf;
    }
    
    public boolean exists(int id)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        return segmentIdx < offsetsLen && 0 != offsets[segmentIdx][index % segmentSize];
    }
    
    public boolean get(int id, KV kv)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        if (segmentIdx >= offsetsLen)
            return false;
        
        final int offset = offsets[segmentIdx][index % segmentSize];
        
        if (offset == 0)
            return false;
        if (kv == null)
            return true;
        
        final byte[] buf = entries[offset >>> 16]; // slot
        final int bufOffset = offset & 0xFFFF,
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
        
        kv.key = ValueUtil.copy(buf, bufOffset + entryLen - 9, 9);
        kv.value = ValueUtil.copy(buf, bufOffset + 2, entryLen - 2 - 9);
        
        return true;
    }
    
    public boolean get(int id, KVI kv)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        if (segmentIdx >= offsetsLen)
            return false;
        
        final int offset = offsets[segmentIdx][index % segmentSize];
        
        if (offset == 0)
            return false;
        if (kv == null)
            return true;
        
        final byte[] buf = entries[offset >>> 16]; // slot
        final int bufOffset = offset & 0xFFFF,
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8),
                valueSize = entryLen - 2 - 9;
        
        kv.key = ValueUtil.copy(buf, bufOffset + entryLen - 9, 9);
        kv.id = SerializedValueUtil.asInt32(voId, buf, bufOffset + 2, valueSize);
        kv.value = ValueUtil.copy(buf, bufOffset + 2, valueSize);
        
        return true;
    }
    
    public void update(int id, byte[] value)
    {
        update(id, value, 0, value.length);
    }
    
    public void update(int id, byte[] v, int voffset, int vlen)
    {
        final int index = id - 1,
                segmentIdx = index / segmentSize;
        
        if (segmentIdx >= offsetsLen)
            throw new RuntimeException("Not found: " + id);
        
        final int offset = offsets[segmentIdx][index % segmentSize];
        
        if (offset == 0)
            throw new RuntimeException("Not found: " + id);

        final byte[] buf = entries[offset >>> 16]; // slot
        final int bufOffset = offset & 0xFFFF,
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8),
                valueSize = entryLen - 2 - 9;

        int updatedEntryLen;
        if (maxValueSize != 0)
        {
            if (vlen > maxValueSize)
            {
                throw new RuntimeException("Updated entity " +
                        KeyUtil.getKind(buf, bufOffset + entryLen - 9, 9) +
                        " exceeds configured max size: " + vlen + " > " + maxValueSize);
            }
            if (vlen != valueSize)
            {
                updatedEntryLen = 2 + vlen + 9;
                // update delim
                buf[bufOffset] = (byte)(updatedEntryLen & 0xFF);
                buf[bufOffset + 1] = (byte)((updatedEntryLen >>> 8) & 0xFF);
            }
        }
        else if (vlen != valueSize)
        {
            throw new RuntimeException("Invalid update length: " + vlen + " != " + valueSize);
        }

        System.arraycopy(v, voffset, buf, bufOffset + 2, vlen);
    }

    /**
     * Returns the index.
     */
    public int add(byte[] key, byte[] value)
    {
        return add(key, 0, value, 0, value.length);
    }
    
    /**
     * Returns the index.
     */
    public int add(byte[] key, byte[] v, int voffset, int vlen)
    {
        return add(key, 0, v, voffset, vlen);
    }
    
    /**
     * Returns the index.
     */
    public int add(byte[] key, int koffset, byte[] v, int voffset, int vlen)
    {
        final int entryLen = 2 + vlen + 9,
                index = size++,
                offsetsIdx = index / segmentSize,
                offsetsPos = index % segmentSize,
                remaining = segmentSize - offsetsPos;
        int occupiedLen = entryLen;
        if (maxValueSize != 0)
        {
            if (vlen > maxValueSize)
            {
                throw new RuntimeException("Added entity " +
                        KeyUtil.getKind(key, koffset, 9) +
                        " exceeds configured max size: " + vlen + " > " + maxValueSize);
            }
            // padding to make it fixed size
            occupiedLen += (maxValueSize - vlen);
        }

        final int[] segment = offsets[offsetsIdx];

        byte[][] entries = this.entries;
        int entriesIdx = this.entriesIdx;
        byte[] buf = entries[entriesIdx];
        int bufSize = ((buf[0] & 0xFF) | (buf[1] & 0xFF) << 8),
                bufOffset = 2 + bufSize;
        if (bufOffset + occupiedLen > 0xFFFF)
        {
            bufSize = 0;
            bufOffset = 2;
            if (entriesIdx + 1 == entriesLen)
            {
                entries = Arrays.copyOf(entries, (entries.length * 3)/2 + 1);
                entries[++entriesIdx] = buf = new byte[0xFFFF];
                
                this.entries = entries;
                ++entriesLen;
                this.entriesIdx = entriesIdx;
            }
            else
            {
                buf = entries[++entriesIdx];
                this.entriesIdx = entriesIdx;
            }
        }
        
        // write delim
        buf[bufOffset] = (byte)(entryLen & 0xFF);
        buf[bufOffset + 1] = (byte)((entryLen >>> 8) & 0xFF);
        // copy value
        System.arraycopy(v, voffset, buf, bufOffset + 2, vlen);
        // copy key
        System.arraycopy(key, koffset, buf, bufOffset + 2 + vlen, 9);
        
        // write new buf size
        bufSize += occupiedLen;
        buf[0] = (byte)(bufSize & 0xFF);
        buf[1] = (byte)((bufSize >>> 8) & 0xFF);
        
        // write offset
        segment[offsetsPos] = bufOffset | (entriesIdx << 16);
        
        if (remaining == 1 && offsetsIdx + 1 == offsetsLen)
        {
            // append new segment 
            int[][] segments = this.offsets,
                    newSegments = appendTo(segments, offsetsLen++, 
                            new int[segmentSize]);
            
            if (segments != newSegments)
                this.offsets = newSegments;
        }
        
        return index;
    }
    
    public <V> int visit(final Visitor<V> visitor, final V v)
    {
        // visit until you see a zero offset.
        final int[][] segments = offsets;
        final int segmentSize = this.segmentSize, 
                totalSegments = offsetsLen;
        final byte[][] entries = this.entries;
        byte[] buf = null;
        int[] segment;
        int count = 0, lastSlot = -1, slot, offset, bufOffset, entryLen;
        for (int i = 0; i < totalSegments; i++)
        {
            segment = segments[i];
            
            for (int j = 0; j < segmentSize; j++)
            {
                offset = segment[j];
                if (offset == 0)
                    return count;
                
                slot = offset >>> 16;
                if (slot != lastSlot)
                {
                    lastSlot = slot;
                    buf = entries[slot];
                }
                bufOffset = offset & 0xFFFF;
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
                
                if (visitor.visit(ValueUtil.copy(buf, bufOffset + entryLen - 9, 9), 
                        buf, bufOffset + 2, entryLen - 2 - 9, 
                        v, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public <V> int visit(final Visitor<V> visitor, final V v, int index)
    {
        if (index <= 0)
            return visit(visitor, v);
        
        // visit until you see a zero offset.
        final int[][] segments = offsets;
        final int segmentSize = this.segmentSize, 
                totalSegments = offsetsLen;
        final byte[][] entries = this.entries;
        byte[] buf = null;
        int[] segment;
        int count = 0, lastSlot = -1, slot, offset, bufOffset, entryLen;
        for (int i = index / segmentSize, pos = index % segmentSize;
                i < totalSegments; // must be less than total segments
                i++, pos = 0)
        {
            segment = segments[i];
            
            for (int j = pos; j < segmentSize; j++)
            {
                offset = segment[j];
                if (offset == 0)
                    return count;
                
                slot = offset >>> 16;
                if (slot != lastSlot)
                {
                    lastSlot = slot;
                    buf = entries[slot];
                }
                bufOffset = offset & 0xFFFF;
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
                
                if (visitor.visit(ValueUtil.copy(buf, bufOffset + entryLen - 9, 9), 
                        buf, bufOffset + 2, entryLen - 2 - 9, 
                        v, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public int pipeTo(final RpcResponse res)
    {
        // visit until you see a zero offset.
        final int[][] segments = offsets;
        final int segmentSize = this.segmentSize, 
                totalSegments = offsetsLen;
        final byte[][] entries = this.entries;
        byte[] buf = null;
        int[] segment;
        int count = 0, lastSlot = -1, slot, offset, bufOffset, entryLen;
        for (int i = 0; i < totalSegments; i++)
        {
            segment = segments[i];
            
            for (int j = 0; j < segmentSize; j++)
            {
                offset = segment[j];
                if (offset == 0)
                    return count;
                
                slot = offset >>> 16;
                if (slot != lastSlot)
                {
                    lastSlot = slot;
                    buf = entries[slot];
                }
                bufOffset = offset & 0xFFFF;
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
                
                if (RpcResponse.PIPED_VISITOR.visit(null, // set key to null since
                        buf, bufOffset + 2, entryLen - 2, // key is included at the end
                        res, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public int pipeTo(final RpcResponse res, int index)
    {
        if (index <= 0)
            return pipeTo(res);
        
        // visit until you see a zero offset.
        final int[][] segments = offsets;
        final int segmentSize = this.segmentSize, 
                totalSegments = offsetsLen;
        final byte[][] entries = this.entries;
        
        byte[] buf = null;
        int[] segment;
        int count = 0, lastSlot = -1, slot, offset, bufOffset, entryLen;
        for (int i = index / segmentSize, pos = index % segmentSize;
                i < totalSegments; // must be less than total segments
                i++, pos = 0)
        {
            segment = segments[i];
            
            for (int j = pos; j < segmentSize; j++)
            {
                offset = segment[j];
                if (offset == 0)
                    return count;
                
                slot = offset >>> 16;
                if (slot != lastSlot)
                {
                    lastSlot = slot;
                    buf = entries[slot];
                }
                bufOffset = offset & 0xFFFF;
                entryLen = ((buf[bufOffset] & 0xFF) | (buf[bufOffset + 1] & 0xFF) << 8);
                
                if (RpcResponse.PIPED_VISITOR.visit(null, // set key to null since
                        buf, bufOffset + 2, entryLen - 2, // key is included at the end
                        res, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public <T> long fill(EntityMetadata<T> em, Datastore store, WriteContext context)
    {
        if (size != 0)
            throw new RuntimeException("Cannot fill a cache that is not empty.");
        
        final byte[] sk = KeyUtil.newEntityRangeKeyStart(em.kind);
        final byte[] ek = KeyUtil.newEntityRangeKeyEnd(em.kind);
        
        final Filler filler = new Filler(this);
        
        return fill(em, filler, context, sk, ek,
                store.visitRange(false, -1, false, null, context, filler, context,
                        sk, 0, sk.length, ek, 0, ek.length));
    }
    
    public <T> long fill(EntityMetadata<T> em, Datastore store, WriteContext context, 
            byte[] sk, byte[] ek)
    {
        if (size != 0)
            throw new RuntimeException("Cannot fill a cache that is not empty.");
        
        final Filler filler = new Filler(this);
        
        return fill(em, filler, context, sk, ek,
                store.visitRange(false, -1, false, null, context, filler, context,
                        sk, 0, sk.length, ek, 0, ek.length));
    }
    
    public <T> long fill(EntityMetadata<T> em, VisitorSession session, WriteContext context, 
            byte[] sk, byte[] ek)
    {
        if (size != 0)
            throw new RuntimeException("Cannot fill a cache that is not empty.");
        
        final Filler filler = new Filler(this);
        
        return fill(em, filler, context, sk, ek,
                session.visitRange(false, -1, false, null, filler, context,
                        sk, 0, sk.length, ek, 0, ek.length));
    }
    
    private <T> long fill(EntityMetadata<T> em, Filler filler, WriteContext context, 
            byte[] sk, byte[] ek, int size)
    {
        // check empty
        if (size == 0)
            return size;
        
        this.size = size;
        this.offsets = filler.offsets;
        this.offsetsLen = filler.offsetsLen;
        this.entries = filler.entries;
        this.entriesIdx = filler.entriesIdx;
        this.entriesLen = filler.entriesLen;
        
        // persist bufSize
        byte[] buf = filler.buf;
        int bufSize = filler.bufSize;
        buf[0] = (byte)(bufSize & 0xFF);
        buf[1] = (byte)((bufSize >>> 8) & 0xFF);
        
        if (!em.seq)
            return 0;
        
        // get the sequence id of the last key
        int lastKeyOffset = 2 + bufSize - 9;
        return em.seqId = ValueUtil.toInt64(buf, 1 + lastKeyOffset) >>> 2;
    }
    
    public <T> long fillSeqId(EntityMetadata<T> em, Datastore store, WriteContext context, 
            byte[] sk, byte[] ek)
    {
        if (!em.seq)
            throw new IllegalArgumentException("Not a seq.");
        
        int size = this.size;
        if (0 != size)
        {
            // get the sequence id of the last key
            return em.seqId = ValueUtil.toInt64($k(size, context), 1 + context.$offset) >>> 2;
        }
        
        final Filler filler = new Filler(this);
        
        return fill(em, filler, context, sk, ek,
                store.visitRange(false, -1, false, null, context, filler, context,
                        sk, 0, sk.length, ek, 0, ek.length));
    }
    
    public <T> long fillSeqId(EntityMetadata<T> em, VisitorSession session, WriteContext context, 
            byte[] sk, byte[] ek)
    {
        if (!em.seq)
            throw new IllegalArgumentException("Not a seq.");
        
        int size = this.size;
        if (0 != size)
        {
            // get the sequence id of the last key
            return em.seqId = ValueUtil.toInt64($k(size, context), 1 + context.$offset) >>> 2;
        }
        
        final Filler filler = new Filler(this);
        
        return fill(em, filler, context, sk, ek,
                session.visitRange(false, -1, false, null, filler, context,
                        sk, 0, sk.length, ek, 0, ek.length));
    }
    
    private static final class Filler implements Visitor<WriteContext>
    {
        final int segmentSize;
        final int maxValueSize;
        // persisted per visit
        int bufSize;
        byte[] buf;
        // non-volatile copies
        int[][] offsets = new int[10][];
        byte[][] entries = new byte[10][];
        int offsetsLen = 1, entriesIdx = 0, entriesLen = 1;
        
        Filler(SerialArrayCache cache)
        {
            segmentSize = cache.segmentSize;
            maxValueSize = cache.maxValueSize;
            offsets = cache.offsets;
            entries = cache.entries;
        }
        
        @Override
        public boolean visit(byte[] key, byte[] v, int voffset, int vlen, 
                WriteContext context, final int index)
        {
            final int entryLen = 2 + vlen + 9,
                    offsetsIdx = index / segmentSize,
                    offsetsPos = index % segmentSize,
                    remaining = segmentSize - offsetsPos;
            int occupiedLen = entryLen;
            if (maxValueSize != 0)
            {
                if (vlen > maxValueSize)
                {
                    throw new RuntimeException("Existing entity " +
                            KeyUtil.getKind(key) +
                            " exceeds configured max size: " + vlen + " > " + maxValueSize);
                }
                // padding to make it fixed size
                occupiedLen += (maxValueSize - vlen);
            }

            final int[] segment = offsets[offsetsIdx];
            
            byte[][] entries = this.entries;
            int entriesIdx = this.entriesIdx;
            byte[] buf = entries[entriesIdx];
            int bufSize = this.bufSize,
                    bufOffset = 2 + bufSize;
            //int bufSize = ((buf[0] & 0xFF) | (buf[1] & 0xFF) << 8),
            //        bufOffset = 2 + bufSize;
            if (bufOffset + occupiedLen > 0xFFFF)
            {
                // persist bufSize
                buf[0] = (byte)(bufSize & 0xFF);
                buf[1] = (byte)((bufSize >>> 8) & 0xFF);
                
                bufSize = 0;
                bufOffset = 2;
                if (entriesIdx + 1 == entriesLen)
                {
                    entries = Arrays.copyOf(entries, (entries.length * 3)/2 + 1);
                    entries[++entriesIdx] = buf = new byte[0xFFFF];
                    
                    this.entries = entries;
                    ++entriesLen;
                    this.entriesIdx = entriesIdx;
                }
                else
                {
                    buf = entries[++entriesIdx];
                    this.entriesIdx = entriesIdx;
                }
            }
            
            // write delim
            buf[bufOffset] = (byte)(entryLen & 0xFF);
            buf[bufOffset + 1] = (byte)((entryLen >>> 8) & 0xFF);
            // copy value
            System.arraycopy(v, voffset, buf, bufOffset + 2, vlen);
            // copy key
            System.arraycopy(key, 0, buf, bufOffset + 2 + vlen, 9);
            
            // write new buf size
            bufSize += occupiedLen;
            this.buf = buf;
            this.bufSize = bufSize;
            //buf[0] = (byte)(bufSize & 0xFF);
            //buf[1] = (byte)((bufSize >>> 8) & 0xFF);
            
            // write offset
            segment[offsetsPos] = bufOffset | (entriesIdx << 16);
            
            if (remaining == 1 && offsetsIdx + 1 == offsetsLen)
            {
                // append new segment 
                int[][] segments = this.offsets,
                        newSegments = appendTo(segments, offsetsLen++, 
                                new int[segmentSize]);
                
                if (segments != newSegments)
                    this.offsets = newSegments;
            }
            
            return false;
        }
    }
    
    public final Visitor<WriteContext> visitor = new Visitor<WriteContext>()
    {
        @Override
        public boolean visit(byte[] key, byte[] v, int voffset, int vlen, WriteContext param, int index)
        {
            add(key, v, voffset, vlen);
            return false;
        }
    };
}
