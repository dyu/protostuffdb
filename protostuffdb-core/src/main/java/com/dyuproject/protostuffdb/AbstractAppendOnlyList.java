//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;
import java.util.Arrays;

import com.dyuproject.protostuff.Output;
import com.dyuproject.protostuff.Schema;

/**
 * TODO
 * 
 * @author David Yu
 * @created Jul 9, 2015
 */
public abstract class AbstractAppendOnlyList<E> // extends HasKV>// implements List<E>
{
    
    static Object[] appendTo(Object[] array, int index, Object value)
    {
        if (index == array.length)
            array = Arrays.copyOf(array, (array.length * 3)/2 + 1);
        
        array[index] = value;
        
        return array;
    }
    
    public interface Listener<E>
    {
        public E[] newArray(int size);
        public void assignIndexTo(E item, int index);
    }
    
    volatile Object[] segments = new Object[10];
    volatile int segmentsLen;
    final int segmentSize;
    final Listener<E> listener;
    
    AbstractAppendOnlyList(int segmentSize, Listener<E> listener)
    {
        if (segmentSize < 8)
            throw new IllegalArgumentException("Min segment size is 8.");
        
        this.segmentSize = segmentSize;
        this.listener = listener;
    }
    
    protected void set(int segmentIdx, int offset, E item)
    {
        @SuppressWarnings("unchecked")
        E[] array = (E[])segments[segmentIdx];
        array[offset] = item;
    }
    
    protected E get(int segmentIdx, int offset)
    {
        @SuppressWarnings("unchecked")
        E[] array = (E[])segments[segmentIdx];
        return array[offset];
    }
    
    public E get(int index)
    {
        final int segmentIdx = index / segmentSize,
                offset = index % segmentSize;
        
        return segmentIdx < segmentsLen ? get(segmentIdx, offset) : null;
    }

    public abstract int size();
    
    public abstract int add(E item);
    
    public abstract void clear();
    
    abstract int fill(E item);
    
    public static <T extends HasKV> int visit(AbstractAppendOnlyList<T> list, 
            final Visitor<T> visitor)
    {
        // visit until you see a null value.
        
        final Object[] segments = list.segments;
        final int segmentSize = list.segmentSize, 
                totalSegments = list.segmentsLen;
        int count = 0;
        for (int i = 0; i < totalSegments; i++)
        {
            @SuppressWarnings("unchecked")
            T[] segment = (T[])segments[i];
            
            for (int j = 0; j < segmentSize; j++)
            {
                T item = segment[j];
                if (item == null)
                    return count;
                
                byte[] value = item.getValue();
                if (visitor.visit(item.getKey(), 
                        value, 0, value == null ? 0 : value.length, 
                        item, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public static <T extends HasKV, V> int visit(final AbstractAppendOnlyList<T> list, 
            final Visitor<V> visitor, final V v)
    {
        // visit until you see a null value.
        
        final Object[] segments = list.segments;
        final int segmentSize = list.segmentSize, 
                totalSegments = list.segmentsLen;
        int count = 0;
        for (int i = 0; i < totalSegments; i++)
        {
            @SuppressWarnings("unchecked")
            T[] segment = (T[])segments[i];
            
            for (int j = 0; j < segmentSize; j++)
            {
                T item = segment[j];
                if (item == null)
                    return count;
                
                byte[] value = item.getValue();
                if (visitor.visit(item.getKey(), 
                        value, 0, value == null ? 0 : value.length, 
                        v, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public static <T extends HasKV, V> int visit(final AbstractAppendOnlyList<T> list, 
            final Visitor<V> visitor, final V v, int index)
    {
        if (index <= 0)
            return visit(list, visitor, v);
        // visit until you see a null value.
        
        final Object[] segments = list.segments;
        final int segmentSize = list.segmentSize, 
                totalSegments = list.segmentsLen;
        
        int segmentIdx = index / segmentSize,
                offset = index % segmentSize;
        
        int count = 0;
        for (int i = segmentIdx; i < totalSegments; i++, offset = 0)
        {
            @SuppressWarnings("unchecked")
            T[] segment = (T[])segments[i];
            
            for (int j = offset; j < segmentSize; j++)
            {
                T item = segment[j];
                if (item == null)
                    return count;
                
                byte[] value = item.getValue();
                if (visitor.visit(item.getKey(), 
                        value, 0, value == null ? 0 : value.length, 
                        v, count++))
                {
                    return count;
                }
            }
        }
        
        return count;
    }
    
    public static <T> int writeTo(final Output output, 
            final Schema<T> schema, final int fieldNumber, 
            final AbstractAppendOnlyList<T> list) throws IOException
    {
        final Object[] segments = list.segments;
        final int segmentSize = list.segmentSize, 
                totalSegments = list.segmentsLen;
        
        int count = 0;
        for (int i = 0; i < totalSegments; i++)
        {
            @SuppressWarnings("unchecked")
            T[] segment = (T[])segments[i];
            
            for (int j = 0; j < segmentSize; j++)
            {
                T item = segment[j];
                if (item == null)
                    return count;
                
                output.writeObject(fieldNumber, item, schema, true);
                count++;
            }
        }
        
        return count;
    }
    
    public static <T> int writeTo(final Output output, 
            final Schema<T> schema, final int fieldNumber, 
            final AbstractAppendOnlyList<T> list, int index) throws IOException
    {
        if (index <= 0)
            return writeTo(output, schema, fieldNumber, list);
        
        final Object[] segments = list.segments;
        final int segmentSize = list.segmentSize, 
                totalSegments = list.segmentsLen;
        
        int segmentIdx = index / segmentSize,
                offset = index % segmentSize;
        
        int count = 0;
        for (int i = segmentIdx; i < totalSegments; i++, offset = 0)
        {
            @SuppressWarnings("unchecked")
            T[] segment = (T[])segments[i];
            
            for (int j = offset; j < segmentSize; j++)
            {
                T item = segment[j];
                if (item == null)
                    return count;
                
                output.writeObject(fieldNumber, item, schema, true);
                count++;
            }
        }
        
        return count;
    }
}
