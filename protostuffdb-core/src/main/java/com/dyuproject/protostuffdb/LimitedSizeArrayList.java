//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.RandomAccess;

/**
 * A array list whose size is limited to the {@link #size} provided in the constructor.
 * 
 * @author David Yu
 * @created Jan 12, 2013
 */
@SuppressWarnings("serial")
public final class LimitedSizeArrayList<E> extends AbstractList<E> implements 
    RandomAccess, Serializable
{
    private final E[] array;
    private final int size;
    private final boolean readOnly;

    @SuppressWarnings("unchecked")
    public LimitedSizeArrayList(E[] array, int size, boolean readOnly)
    {
        this.array = readOnly ? array : 
            Arrays.copyOf(array, size, (Class<? extends E[]>)array.getClass());
        this.size = size;
        this.readOnly = readOnly;
    }

    public int size()
    {
        return size;
    }

    @SuppressWarnings("unchecked")
    public Object[] toArray()
    {
        return Arrays.copyOf(array, size, (Class<? extends E[]>)array.getClass());
    }

    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a)
    {
        if (a == null || a.length < size)
            return Arrays.copyOf(array, size, (Class<? extends T[]>)a.getClass());
        
        System.arraycopy(array, 0, a, 0, size);
        
        if (a.length > size)
            a[size] = null;
        
        return a;
    }

    public E get(int index)
    {
        return array[index];
    }

    public E set(int index, E element)
    {
        if(readOnly)
            throw new UnsupportedOperationException();
        
        E oldValue = array[index];
        array[index] = element;
        return oldValue;
    }

    public int indexOf(Object o)
    {
        if (o == null)
        {
            for (int i = 0; i < size; i++)
            {
                if (array[i] == null)
                    return i;
            }
        }
        else
        {
            for (int i = 0; i < size; i++)
            {
                if (o.equals(array[i]))
                    return i;
            }
        }
        return -1;
    }

    public boolean contains(Object o)
    {
        return indexOf(o) != -1;
    }

}
