//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;


/**
 * Utils for date and time.
 * All ops are UTC based.
 * 
 * @author David Yu
 * @created Oct 10, 2012
 */
public final class DateTimeUtil
{
    
    /**
     * The millis that amount to 1 day.
     */
    public static final long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
    
    /**
     * The max time per day in millis.
     */
    public static final int MAX_TIME_IN_MILLIS_PER_DAY = (1000 * 60 * 60 * 24) - 1;
    
    /**
     * The millis that amount to 1 hour.
     */
    public static final long MILLIS_PER_HOUR = 1000 * 60 * 60;
    
    /**
     * The timezone raw offset of this host.
     */
    public static final int HOST_RAW_OFFSET = java.util.TimeZone.getDefault().getRawOffset();
    
    /**
     * The utc timezone.
     */
    public static final TimeZone TZ_UTC = new SimpleTimeZone(0, "Etc/GMT");
    
    
    static final int SECONDS_PER_HOUR = 60 * 60;
    
    
    public static final int MAX_TIME = hour(23) + minute(59) + 59;
    
    /**
     * Returns the time difference between a utc ts and a date (pre-converted to utc).
     */
    public static int extractTime(long ts, long date)
    {
        return (int)((utc(ts) - date) / 1000);
    }
    
    /**
     * Returns the equivalent seconds of the provided hour.
     */
    public static int hour(int h)
    {
        return h * SECONDS_PER_HOUR;
    }
    
    /**
     * Returns the equivalent seconds of the provided minute.
     */
    public static int minute(int m)
    {
        return m * 60;
    }
    
    /**
     * Returns the start of day in millis (with time stripped).
     */
    public static long startOfDayMS(long ts)
    {
        // convert to utc
        ts += HOST_RAW_OFFSET;
        
        return ts - (ts % MILLIS_PER_DAY);
    }
    
    /**
     * Returns the start of today in millis (with time stripped).
     */
    public static long startOfTodayMS()
    {
        return startOfDayMS(System.currentTimeMillis());
    }
    
    public static long utcNow()
    {
        return utc(System.currentTimeMillis());
    }
    
    public static long utc(long now)
    {
        return now + HOST_RAW_OFFSET;
    }
    
    /**
     * Formats the date in yyyy-mm-dd format when separator is '-'.
     */
    public static byte[] toFormattedByteArray(final long date, final char separator)
    {
        return toFormattedByteArray(new Date(date), separator);
    }
    
    /**
     * Formats the date in yyyy-mm-dd format when separator is '-'.
     */
    public static byte[] toFormattedByteArray(final Date date, final char separator)
    {
        @SuppressWarnings("deprecation")
        final int year = date.getYear() + 1900, 
                month = date.getMonth() + 1, 
                day = date.getDate();
        
        return new byte[]{
                (byte)Character.forDigit(year/1000,10), 
                (byte)Character.forDigit((year/100)%10,10), 
                (byte)Character.forDigit((year/10)%10,10), 
                (byte)Character.forDigit(year%10,10), 
                (byte)separator, 
                (byte)Character.forDigit(month/10,10), 
                (byte)Character.forDigit(month%10,10),
                (byte)separator, 
                (byte)Character.forDigit(day/10,10), 
                (byte)Character.forDigit(day%10,10)
        };
    }
    
    /**
     * Formats the date in yyyy-mm-dd format when separator is '-'.
     */
    public static String format(long date, final char separator)
    {
        return format(new Date(date), separator);
    }
    
    /**
     * Formats the date in yyyy-mm-dd format when separator is '-'.
     */
    public static String format(final Date date, final char separator)
    {
        @SuppressWarnings("deprecation")
        final int year = date.getYear() + 1900, 
                month = date.getMonth() + 1, 
                day = date.getDate();
        
        return new String(new char[]{
                Character.forDigit(year/1000,10), 
                Character.forDigit((year/100)%10,10), 
                Character.forDigit((year/10)%10,10), 
                Character.forDigit(year%10,10), 
                separator, 
                Character.forDigit(month/10,10), 
                Character.forDigit(month%10,10),
                separator, 
                Character.forDigit(day/10,10), 
                Character.forDigit(day%10,10)
        });
    }
    
    /**
     * Returns true if the timestamp is more than 24 hrs old.
     */
    public static boolean isExpired24hrs(long ts)
    {
        return System.currentTimeMillis() > (ts + MILLIS_PER_DAY);
    }
    
    /**
     * Returns true if both timestamps occured on the same day.
     */
    public static boolean isSameDay(long ts1, long ts2)
    {
        return startOfDayMS(ts1) == startOfDayMS(ts2);
    }
    
    /**
     * Add days to the provided timestamp.
     */
    public static long addDaysTo(long ts, int days)
    {
        return ts + (MILLIS_PER_DAY * days);
    }
    
    /**
     * Returns the days multipled by the number of millis in a day.
     */
    public static long dayToMillis(int days)
    {
        return MILLIS_PER_DAY * days;
    }
}
