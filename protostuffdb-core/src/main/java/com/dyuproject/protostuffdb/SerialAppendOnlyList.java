//========================================================================
//Copyright 2015 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;


/**
 * Append only list for for a single writer thread.
 * 
 * TODO make {@link #segments} into a volatile array and incorporate 
 * double-check-before-sync? Or make {@link #segments} at atomic reference?
 * 
 * @author David Yu
 * @created Jul 9, 2015
 */
public final class SerialAppendOnlyList<E> extends AbstractAppendOnlyList<E>
{
    
    private volatile int size = 0;
    
    public SerialAppendOnlyList(int segmentSize, Listener<E> listener)
    {
        super(segmentSize, listener);
        segments[0] = listener.newArray(segmentSize);
        segmentsLen = 1;
    }

    public int size()
    {
        return size;
    }

    public int add(E item)
    {
        final int index = size++,
                segmentIdx = index / segmentSize,
                offset = index % segmentSize,
                remaining = segmentSize - offset;
        
        // If segmentsLen was greater than 1 before clear() was called,
        // then we can re-use the segments created starting at index 1.
        if (remaining == 1 && segmentIdx + 1 == segmentsLen)
        //if (remaining == 1)
        {
            // append new segment 
            Object[] segments = this.segments,
                    newSegments = appendTo(segments, segmentsLen++, 
                            listener.newArray(segmentSize));
            
            if (segments != newSegments)
                this.segments = newSegments;
        }
        
        set(segmentIdx, offset, item);
        
        listener.assignIndexTo(item, index);
        
        return index;
    }
    
    int fill(E item)
    {
        return add(item);
    }

    @Override
    public void clear()
    {
        size = 0;
        segments[0] = listener.newArray(segmentSize);
        // or clear only the first element for tests
        //set(0, 0, null);
    }
}
