//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.io.IOException;

import com.dyuproject.protostuff.DSByteArrayInput;
import com.dyuproject.protostuff.IntSerializer;
import com.dyuproject.protostuff.Output;

/**
 * IO Utils for serialized values.
 * 
 * @author David Yu
 * @created Dec 31, 2012
 */
public final class SerializedValueUtil
{
    
    private SerializedValueUtil() {}
    
    /**
     * Convert to int8.
     */
    public static int i1(boolean p)
    {
        return p ? 1 : 0;
    }
    
    /**
     * Convert to int32.
     */
    public static int i4(float p)
    {
        return Float.floatToRawIntBits(p);
    }
    
    /**
     * Convert to int64.
     */
    public static long i8(double p)
    {
        return Double.doubleToRawLongBits(p);
    }
    
    /* ================================================== */
    
    public static void putKey(byte[] key, 
            int valueOffset, byte[] buf)
    {
        System.arraycopy(key, 0, buf, buf.length - valueOffset, 9);
    }
    
    public static void putKey(byte[] key, int koffset, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        System.arraycopy(key, koffset, buf, offset + len - valueOffset, 9);
    }
    
    public static void putBool(boolean value, 
            int valueOffset, byte[] buf)
    {
        putInt8(value ? 1 : 0, valueOffset, buf, 0, buf.length);
    }
    
    public static void putBool(boolean value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        putInt8(value ? 1 : 0, valueOffset, buf, offset, len);
    }
    
    public static void putInt8(int value, 
            int valueOffset, byte[] buf)
    {
        putInt8(value, valueOffset, buf, 0, buf.length);
    }
    
    public static void putInt8(int value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        buf[offset + len - valueOffset] = (byte)value;
    }
    
    public static void putFixed32(int value, 
            int valueOffset, byte[] buf)
    {
        putFixed32(value, valueOffset, buf, 0, buf.length);
    }
    
    public static void putFixed32(int value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        IntSerializer.writeInt32LE(value, buf, offset + len - valueOffset);
    }
    
    public static void putFloat(float value, 
            int valueOffset, byte[] buf)
    {
        putFloat(value, valueOffset, buf, 0, buf.length);
    }

    public static void putFloat(float value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        IntSerializer.writeInt32LE(i4(value), buf, offset + len - valueOffset);
    }
    
    public static void putFixed64(long value, 
            int valueOffset, byte[] buf)
    {
        putFixed64(value, valueOffset, buf, 0, buf.length);
    }
    
    public static void putFixed64(long value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        IntSerializer.writeInt64LE(value, buf, offset + len - valueOffset);
    }
    
    public static void putDouble(double value, 
            int valueOffset, byte[] buf)
    {
        putDouble(value, valueOffset, buf, 0, buf.length);
    }

    public static void putDouble(double value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        IntSerializer.writeInt64LE(i8(value), buf, offset + len - valueOffset);
    }
    
    public static void putDate(long value, 
            int valueOffset, byte[] buf)
    {
        putDate(value, valueOffset, buf, 0, buf.length);
    }

    public static void putDate(long value, 
            int valueOffset, byte[] buf, int offset, int len)
    {
        putFixed64(value, valueOffset, buf, offset, len);
    }
    
    /* ================================================== */
    
    /**
     * Returns the date embedded in the value.
     */
    public static long asDate(int valueOffset, 
            byte[] buf)
    {
        return asInt64(valueOffset, buf);
        //return KeyUtil.readTimestamp(buf, buf.length - valueOffset);
    }
    
    /**
     * Returns the date embedded in the value.
     */
    public static long asDate(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return asInt64(valueOffset, buf, offset, len);
        //return KeyUtil.readTimestamp(buf, offset + len - valueOffset);
    }
    
    /**
     * Returns the boolean embedded in the value.
     */
    public static boolean asBool(int valueOffset, 
            byte[] buf)
    {
        return 0 != buf[buf.length - valueOffset];
    }
    
    /**
     * Returns the bool embedded in the value.
     */
    public static boolean asBool(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return 0 != buf[offset + len - valueOffset];
    }

    /**
     * Returns the int embedded in the value.
     */
    public static int asInt8(int valueOffset, 
            byte[] buf)
    {
        return buf[buf.length - valueOffset];
    }
    
    /**
     * Returns the int embedded in the value.
     */
    public static int asInt8(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return buf[offset + len - valueOffset];
    }
    
    /**
     * Returns the int embedded in the value.
     */
    public static int asInt32(int valueOffset, 
            byte[] buf)
    {
        return ValueUtil.toInt32LE(buf, buf.length - valueOffset);
    }
    
    /**
     * Returns the int embedded in the value.
     */
    public static int asInt32(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return ValueUtil.toInt32LE(buf, offset + len - valueOffset);
    }
    
    /**
     * Returns the long embedded in the value.
     */
    public static long asInt64(int valueOffset, 
            byte[] buf)
    {
        return ValueUtil.toInt64LE(buf, buf.length - valueOffset);
    }
    
    /**
     * Returns the long embedded in the value.
     */
    public static long asInt64(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return ValueUtil.toInt64LE(buf, offset + len - valueOffset);
    }
    
    /**
     * Returns the float embedded in the value.
     */
    public static float asFloat(int valueOffset, 
            byte[] buf)
    {
        return Float.intBitsToFloat(ValueUtil.toInt32LE(buf, buf.length - valueOffset));
    }
    
    /**
     * Returns the float embedded in the value.
     */
    public static float asFloat(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return Float.intBitsToFloat(ValueUtil.toInt32LE(buf, offset + len - valueOffset));
    }
    
    /**
     * Returns the double embedded in the value.
     */
    public static double asDouble(int valueOffset, 
            byte[] buf)
    {
        return Double.longBitsToDouble(ValueUtil.toInt64LE(buf, buf.length - valueOffset));
    }
    
    /**
     * Returns the double embedded in the value.
     */
    public static double asDouble(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return Double.longBitsToDouble(ValueUtil.toInt64LE(buf, offset + len - valueOffset));
    }
    
    /**
     * Returns the key embedded in the value.
     */
    public static byte[] asKey(int valueOffset, 
            byte[] buf)
    {
        byte[] key = new byte[9];
        System.arraycopy(buf, buf.length - valueOffset, key, 0, 9);
        return key;
    }
    
    /**
     * Returns the key embedded in the value.
     */
    public static byte[] asKey(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        byte[] key = new byte[9];
        System.arraycopy(buf, offset + len - valueOffset, key, 0, 9);
        return key;
    }
    
    /**
     * Returns the key offset in the value.
     */
    public static int asKeyOffset(int valueOffset, 
            byte[] buf)
    {
        return buf.length - valueOffset;
    }
    
    /**
     * Returns the key offset in the value.
     */
    public static int asKeyOffset(int valueOffset, 
            byte[] buf, int offset, int len)
    {
        return offset + len - valueOffset;
    }
    
    /**
     * Returns the size of the byte/string field.
     */
    public static int readByteArraySize(int field, 
            byte[] buf, WriteContext context)
    {
        return readByteArraySize(field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns the size of the byte/string field.
     */
    public static int readByteArraySize(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    return size;
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return -1;
    }
    
    public static void transferByteArrayTo(Output output, 
            boolean utf8String, int fieldNumber, boolean repeated, 
            int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    
                    output.writeByteRange(utf8String, fieldNumber, 
                            buf, input.offset, size, repeated);
                    
                    return;
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
    }
    
    /**
     * Sets the size to {@link WriteContext#$len} and returns the offset.
     */
    public static int readBAO$len(int field, 
            byte[] buf, WriteContext context)
    {
        return readBAO$len(field, buf, 0, buf.length, context);
    }
    
    /**
     * Sets the size to {@link WriteContext#$len} and returns the offset.
     */
    public static int readBAO$len(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    
                    context.$len = size;
                    return input.offset;
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return -1;
    }
    
    /**
     * Returns an 2-element int array containing the offset and length respectively.
     * Returns null if the field is missing.
     */
    public static int[] readByteArrayOffsetAndSize(int field, 
            byte[] buf, WriteContext context)
    {
        return readByteArrayOffsetAndSize(field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns an 2-element int array containing the offset and length respectively.
     * Returns null if the field is missing.
     */
    public static int[] readByteArrayOffsetAndSize(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    return new int[]{input.offset, size};
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return null;
    }
    
    /**
     * Returns 0 if field is missing.
     * The int's upper 16-bit will be the offset, while the lower 16-bit will be the size.
     * 
     * Note that this assumes that your message is not larger than 64kb.
     */
    public static int readByteArrayOffsetAndSizeShifted(int field, 
            byte[] buf, WriteContext context)
    {
        return readByteArrayOffsetAndSizeShifted(field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns 0 if field is missing.
     * The int's upper 16-bit will be the offset, while the lower 16-bit will be the size.
     * 
     * Note that this assumes that your message is not larger than 64kb.
     */
    public static int readByteArrayOffsetAndSizeShifted(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    
                    return input.offset<<16 | size;
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return 0;
    }
    
    /**
     * Returns null if the field is missing.
     */
    public static byte[] readByteArray(int field, 
            byte[] buf, WriteContext context)
    {
        return readByteArray(field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns null if the field is missing.
     */
    public static byte[] readByteArray(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readByteArray();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return null;
    }
    
    /**
     * Returns false if field is missing or if it is not equal to the string value.
     */
    public static boolean readAndCompareString(String value, int field, 
            byte[] buf, WriteContext context)
    {
        return readAndCompareString(value, field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns false if field is missing or if it is not equal to the string value.
     */
    public static boolean readAndCompareString(String value, int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    
                    return ValueUtil.isEqual(buf, input.offset, size, value);
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return false;
    }
    
    /**
     * Returns false if field is missing or if it is not equal to the string value.
     */
    public static boolean readAndCompareString(byte[] v, int voffset, int vlen, 
            int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                {
                    int size = input.readRawVarint32();
                    if(size < 0)
                    {
                        throw new RuntimeException("Corrupt protobuf input" +
                                "(negative delimter size)");
                    }
                    
                    return ValueUtil.isEqual(buf, input.offset, size, v, voffset, vlen);
                }
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return false;
    }
    
    /**
     * Returns null if the field is missing.
     */
    public static String readString(int field, 
            byte[] buf, WriteContext context)
    {
        return readString(field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns null if the field is missing.
     */
    public static String readString(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readString();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return null;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readInt32(int field, int toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readInt32(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readInt32(int field, int toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readInt32();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readUInt32(int field, int toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readUInt32(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readUInt32(int field, int toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readUInt32();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readSInt32(int field, int toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readSInt32(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readSInt32(int field, int toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readSInt32();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readFixed32(int field, int toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readFixed32(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readFixed32(int field, int toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readFixed32();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readSFixed32(int field, int toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readSFixed32(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static int readSFixed32(int field, int toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readSFixed32();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readInt64(int field, long toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readInt64(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readInt64(int field, long toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readInt64();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readUInt64(int field, long toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readUInt64(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readUInt64(int field, long toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readUInt64();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readSInt64(int field, long toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readSInt64(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readSInt64(int field, long toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readSInt64();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readFixed64(int field, long toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readFixed64(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readFixed64(int field, long toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readFixed64();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readSFixed64(int field, long toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readSFixed64(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static long readSFixed64(int field, long toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readSFixed64();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static float readFloat(int field, float toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readFloat(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static float readFloat(int field, float toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readFloat();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static double readDouble(int field, double toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readDouble(field, toReturnIfMissing, buf, 0, buf.length, context);
    }

    /**
     * Returns {@code toReturnIfMissing} if the field is missing.
     */
    public static double readDouble(int field, double toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readDouble();
                
                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns -1 if the field is missing (0 = false, 1 = true).
     */
    public static int readBoolAsInt(int field, int toReturnIfMissing, 
            byte[] buf, WriteContext context)
    {
        return readBoolAsInt(field, toReturnIfMissing, buf, 0, buf.length, context);
    }
    
    /**
     * Returns -1 if the field is missing (0 = false, 1 = true).
     */
    public static int readBoolAsInt(int field, int toReturnIfMissing, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readUInt32();

                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return toReturnIfMissing;
    }
    
    /**
     * Returns null if missing.
     */
    public static Boolean readBool(int field, 
            byte[] buf, WriteContext context)
    {
        return readBool(field, buf, 0, buf.length, context);
    }
    
    /**
     * Returns null if missing.
     */
    public static Boolean readBool(int field, 
            byte[] buf, int offset, int len, WriteContext context)
    {
        final DSByteArrayInput input = context.dsPipe.input.reset(buf, offset, len);
        try
        {
            for(int number = input.readFieldNumber(null); number != 0; 
                    number = input.readFieldNumber(null))
            {
                if(number == field)
                    return input.readBool() ? Boolean.TRUE : Boolean.FALSE;

                input.handleUnknownField(number, null);
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException("Should never happen.", e);
        }
        finally
        {
            input.clear();
        }
        
        return null;
    }
    
}
