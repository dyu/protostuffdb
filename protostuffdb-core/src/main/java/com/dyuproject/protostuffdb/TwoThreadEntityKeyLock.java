//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.LockSupport;

/**
 * TODO
 * 
 * @author David Yu
 * @created Feb 24, 2013
 */
public final class TwoThreadEntityKeyLock implements EntityKeyLock
{
    
    // 10 by default
    static final int BUSY_SPIN_CYCLE_LIMIT = Math.max(10, 
            Integer.getInteger("ttc.busy_spin_cycles", 0));

    static final int PARK_CYCLE_LIMIT = BUSY_SPIN_CYCLE_LIMIT + 5;
    
    static final int YIELD_CYCLE_LIMIT = PARK_CYCLE_LIMIT + 5;
    
    private final AtomicLong atomicValue = new AtomicLong(0);

    public long acquire(byte[] key)
    {
        return acquire(ValueUtil.toInt64(key, key.length-8));
    }
    
    public long acquire(final long value)
    {
        if(atomicValue.compareAndSet(0, value))
            return value;
        
        int count = 0;
        for(long expect = atomicValue.get();; expect = atomicValue.get())
        {
            if(value != expect && atomicValue.compareAndSet(expect, value))
                return value;
            
            // could be operating on same key
            // wait for (expect != hash) <- wait for the other thread to finish
            
            if(++count > BUSY_SPIN_CYCLE_LIMIT)
            {
                if(count > YIELD_CYCLE_LIMIT)
                    count = 0;
                else if(count > PARK_CYCLE_LIMIT)
                    Thread.yield();
                else
                    LockSupport.parkNanos(1);
            }
        }
    }

    public boolean release(long value)
    {
        return atomicValue.compareAndSet(value, 0);
    }

}
