//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import java.util.List;

/**
 * Wraps many ids.
 * 
 * @author David Yu
 * @created Aug 27, 2014
 */
public interface HasIds
{
    /**
     * Returns the ids at index.
     * 
     * This should be called first.
     * If this returns null at index, call {@link #idAt(int)}.
     */
    List<Integer> idsAt(int index);
    
    /**
     * Returns the id at index.
     */
    int idAt(int index);
}
