//========================================================================
//Copyright 2007-2011 David Yu dyuproject@gmail.com
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.protostuffdb;

import static com.dyuproject.protostuffdb.KeyUtil.keyFromIndexKey;
import static com.dyuproject.protostuffdb.KeyUtil.keyFromLinkKey;
import static com.dyuproject.protostuffdb.KeyUtil.keyFromTagIndexKey;
import static com.dyuproject.protostuffdb.KeyUtil.keyOffsetFromIndexKey;
import static com.dyuproject.protostuffdb.KeyUtil.keyOffsetFromLinkKey;
import static com.dyuproject.protostuffdb.KeyUtil.keyOffsetFromTagIndexKey;

/**
 * The type of the key returned from the datastore
 *
 * @author David Yu
 * @created Mar 22, 2011
 */
public enum KeyType
{
    
    ENTITY
    {
        public byte[] keyFrom(byte[] compoundKey)
        {
            return compoundKey;
        }
        public int keyOffsetFrom(byte[] compoundKey)
        {
            return 0;
        }
    },
    LINK
    {
        public byte[] keyFrom(byte[] compoundKey)
        {
            return keyFromLinkKey(compoundKey);
        }
        public int keyOffsetFrom(byte[] compoundKey)
        {
            return keyOffsetFromLinkKey(compoundKey);
        }
    },
    INDEX
    {
        public byte[] keyFrom(byte[] compoundKey)
        {
            return keyFromIndexKey(compoundKey);
        }
        public int keyOffsetFrom(byte[] compoundKey)
        {
            return keyOffsetFromIndexKey(compoundKey);
        }
    },
    TAG_INDEX
    {
        public byte[] keyFrom(byte[] compoundKey)
        {
            return keyFromTagIndexKey(compoundKey);
        }
        public int keyOffsetFrom(byte[] compoundKey)
        {
            return keyOffsetFromTagIndexKey(compoundKey);
        }
    };
    
    /**
     * Extracts the entity key from the compound key.
     */
    public abstract byte[] keyFrom(byte[] compoundKey);
    
    /**
     * Returns the offset of the entity key embedded in the compound key.
     * The length of the entity key will always be 8.
     */
    public abstract int keyOffsetFrom(byte[] compoundKey);
    

}
