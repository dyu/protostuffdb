#!/bin/sh

GIT_CLONE_BRANCH='git clone --depth 1 --single-branch -b'
GTEST_VERSION=1.8.0
SNAPPY_VERSION=1.1.4
LEVELDB_VERSION=1.19
LIBUV_VERSION=1.11.0
JSON_VERSION=2.1.1

CURRENT_DIR=$PWD
# locate
if [ ! -n "$BASH_SOURCE" ]; then
    SCRIPT_DIR=`dirname "$(readlink -f "$0")"`
else
    F=$BASH_SOURCE
    while [ -h "$F" ]; do
        F="$(readlink "$F")"
    done
    SCRIPT_DIR=`dirname "$F"`
fi

cd $SCRIPT_DIR

[ ! -e googletest-release-$GTEST_VERSION ] && \
    wget https://github.com/google/googletest/archive/release-$GTEST_VERSION.tar.gz && \
    tar -xvzf release-$GTEST_VERSION.tar.gz

# https://github.com/google/snappy/releases/download/$SNAPPY_VERSION/snappy-$SNAPPY_VERSION.tar.gz has a different source!
[ ! -e snappy-$SNAPPY_VERSION ] && \
    wget https://github.com/google/snappy/archive/$SNAPPY_VERSION.tar.gz -O snappy-$SNAPPY_VERSION.tar.gz && \
    tar -xvzf snappy-$SNAPPY_VERSION.tar.gz

[ ! -e leveldb-$LEVELDB_VERSION ] && \
    wget https://github.com/google/leveldb/archive/v$LEVELDB_VERSION.tar.gz -O leveldb-$LEVELDB_VERSION.tar.gz && \
    tar -xvzf leveldb-$LEVELDB_VERSION.tar.gz && \
    patch leveldb-$LEVELDB_VERSION/port/port.h < leveldb_port.diff && \
    patch leveldb-$LEVELDB_VERSION/include/leveldb/slice.h < leveldb_slice.diff

[ ! -e libuv-$LIBUV_VERSION ] && \
    wget https://github.com/libuv/libuv/archive/v$LIBUV_VERSION.tar.gz -O libuv-$LIBUV_VERSION.tar.gz && \
    tar -xvzf libuv-$LIBUV_VERSION.tar.gz

[ ! -e uWebSockets ] && $GIT_CLONE_BRANCH cors3 https://github.com/dyu/uWebSockets.git

[ ! -e json/json.hpp ] && mkdir json && \
    wget https://github.com/nlohmann/json/releases/download/v$JSON_VERSION/json.hpp -O json/json.hpp

