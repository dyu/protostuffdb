#!/bin/sh

# locate
if [ ! -n "$BASH_SOURCE" ]; then
    SCRIPT_DIR=`dirname "$(readlink -f "$0")"`
else
    F=$BASH_SOURCE
    while [ -h "$F" ]; do
        F="$(readlink "$F")"
    done
    SCRIPT_DIR=`dirname "$F"`
fi

cd $SCRIPT_DIR

mkdir -p deps && cd deps

GIT_CLONE="git clone --depth 1 --single-branch -b"
SNAPPY_VERSION=1.1.4

# depends on //gn-deps/snappy
[ ! -e leveldb ] && $GIT_CLONE protostuffdb https://github.com/dyu-deploy/leveldb.git

[ "$1" = "1" ] && exit 0

# https://github.com/google/snappy/releases/download/$SNAPPY_VERSION/snappy-$SNAPPY_VERSION.tar.gz has a different source!
[ ! -e snappy-$SNAPPY_VERSION ] && \
    curl https://github.com/google/snappy/archive/$SNAPPY_VERSION.tar.gz -Lso snappy-$SNAPPY_VERSION.tar.gz && \
    tar -xvzf snappy-$SNAPPY_VERSION.tar.gz

if [ ! -e HyperLevelDB ]; then
    $GIT_CLONE protostuffdb https://github.com/dyu-deploy/HyperLevelDB.git
    cd HyperLevelDB
    mkdir build
    autoreconf -i && ./configure --prefix=$PWD/build
    cd ..
fi
