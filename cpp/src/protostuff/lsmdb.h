//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#ifndef PS_LSMDB_H_
#define PS_LSMDB_H_

#include "protostuff.h"

#if defined(REP_MASTER) || defined(REP_SLAVE) || defined(SYNC_NODE)

#include <unordered_map>
#include <vector>

#if defined(HYPER)
#include <hyperleveldb/db.h>
#else
#include <leveldb/db.h>
#endif


#endif

#if defined(REP_MASTER)

#if defined(REP_MT)
#include <atomic>
#endif

#if defined(HYPER)
#include <hyperleveldb/env.h>
#include <hyperleveldb/write_batch.h>
#else
#include <leveldb/env.h>
#include <leveldb/write_batch.h>
#endif

#include <db/log_reader.h>
#include <db/log_writer.h>

#endif

namespace protostuff {

const size_t SCRATCH_RESERVED_SIZE = 65535;
const size_t END_KEY_LEN = 10;

extern int write_buffer_mb;

#if defined(SYNC_NODE)
struct RepEntry {
    const std::string path;
    const std::string name;
    leveldb::DB* db;
    uint8_t idx;
    RepEntry(std::string path, std::string name, leveldb::DB* db, uint8_t idx):
        path(path),
        name(name),
        db(db),
        idx(idx) {}
    
    static std::unordered_map<std::string, RepEntry*>& name_map();
    //static std::unordered_map<leveldb::DB*, RepEntry*>& db_map();
    static std::string scratch;
};
#endif

#if defined(REP_MASTER) || defined(REP_SLAVE) || defined(SYNC_NODE)
void encodeSeq(char* buf, uint64_t value);
uint64_t decodeSeq(const char* buf);
#endif

#if defined(REP_MASTER) || defined(REP_SLAVE)
const size_t SEQ_VAL_LEN = 16; // offset(8) + size(4) + batch_size(4)
const size_t SEQ_KEY_LEN = 9; // prefix(1) + seq_num(8)
const uint8_t SEQ_KEY_START = 0xFF;

#if defined(REP_SLAVE)
struct RepEntry {
    RepEntry(std::string path, std::string name,
            leveldb::DB* db, uint64_t seq_num, uint8_t idx):
        path(path),
        name(name),
        db(db),
        seq_num(seq_num),
        idx(idx),
        queued(false) {}
    const std::string path;
    const std::string name;
    leveldb::DB* db;
    uint64_t seq_num;
    uint8_t idx;
    bool queued;
    
    static std::unordered_map<std::string, RepEntry*>& name_map();
    static std::vector<RepEntry*> list;
    static std::vector<RepEntry*> ack_queue;
    static std::vector<uint16_t> segments;
    static std::string scratch;
};
#else
struct RepEntry;

struct SlaveEntry
{
    void* socket;
    uint64_t seq_num;
    uint8_t entry_id;
    bool pending;
    bool acked;
};

#if defined(REP_MT)
struct MappingEntry {
    std::unordered_map<void*, protostuff::SlaveEntry> slave_map;
    RepEntry* rep_entry;
    leveldb::log::Reader* reader;
    //leveldb::SequentialFile* sf;
    uint64_t rep_num;
    uint8_t idx;
};
struct ReaderEntry {
    ReaderEntry(int db_count, std::atomic<uint64_t>* queued):
        mapping(db_count),
        scratch(),
        #ifdef REP_MT_TIMER
        queued(queued),
        #endif
        update_ticks(0)
    {
        scratch.reserve(SCRATCH_RESERVED_SIZE);
    }
    std::vector<protostuff::MappingEntry> mapping;
    std::string scratch;
    #ifdef REP_MT_TIMER
    std::atomic<uint64_t>* queued;
    #endif
    uint8_t update_ticks;
    
    static std::vector<ReaderEntry*> list;
};
#endif

struct RepEntry
{
    RepEntry(std::string path, std::string name, std::string log_path,
            leveldb::WritableFile* wf, leveldb::SequentialFile* sf, uint64_t file_size, 
            leveldb::DB* db, uint64_t seq_num, uint8_t idx):
        path(path),
        name(name),
        log_path(log_path),
        writer(wf, file_size), 
        reader(sf, nullptr, true, 0),
        //wf(wf), 
        //sf(sf),
        file_size(file_size), 
        db(db),
        oob_num(0),
        seq_num(seq_num),
        rep_num(seq_num),
        idx(idx),
        queued(false) {}
    
    const std::string path;
    const std::string name;
    const std::string log_path;
    leveldb::log::SimpleWriter writer;
    leveldb::log::Reader reader;
    //leveldb::WritableFile* wf;
    //leveldb::SequentialFile* sf;
    uint64_t file_size;
    leveldb::DB* db;
    uint64_t oob_num;
    uint64_t seq_num;
    uint64_t rep_num;
    uint8_t idx;
    bool queued;
    //std::vector<leveldb::WriteBatch*> wb_list;
    std::unordered_map<void*, SlaveEntry> slave_map;
    //std::vector<void*> pub_msgs;
    #ifdef REP_MT
    std::atomic<uint64_t> reader_seq_num;
    #endif
    
    bool write(leveldb::WriteOptions& wo, leveldb::WriteBatch* batch);
    
    static std::unordered_map<std::string, RepEntry*>& name_map();
    static std::unordered_map<leveldb::DB*, RepEntry*>& db_map();
    static std::vector<RepEntry*> queue;
    static std::string scratch;
    //static uint16_t slave_count;
};
#endif
#endif

const uint8_t OFFSET_PTR_DB = OFFSET_PTRS, 
        OFFSET_PTR_WB = OFFSET_PTR_DB + 8, 
        OFFSET_PTR_IT = OFFSET_PTR_WB + 8;

/* ================================================== */
// error types (start at 20)

const uint8_t ERR_MISSING_DB                    = 20;
const uint8_t ERR_MISSING_WRITEBATCH            = 21;
const uint8_t ERR_MISSING_ITERATOR              = 22;

const uint8_t ERR_WRITEBATCH_FLUSH              = 23;

const uint8_t ERR_BUF_TMP_NOT_AVAILABLE         = 24;
const uint8_t ERR_INVALID_VALUE_AS_KEY          = 25;

/* ================================================== */
// newDb flags

const uint8_t FNEWDB_CREATE_IF_MISSING          = 1;
const uint8_t FNEWDB_ERROR_IF_EXISTS            = 1 << 1;
const uint8_t FNEWDB_PARANOID_CHECKS            = 1 << 2;

/* ================================================== */
// common op flags

const uint8_t FOP_CLOSE                         = 1 << 6;
const uint8_t FOP_CLOSE_SYNC                    = 1 << 7; // sync

/* ================================================== */
// close flags

const uint8_t FCLOSE_CLEANUP_ONLY               = 1;

/* ================================================== */
// put flags

const uint8_t FPUT_SYNC                         = 1 << 7; // sync

/* ================================================== */
// get flags

const uint8_t FGET_CACHE                        = 1 << 1;
const uint8_t FGET_KEY_ONLY                     = 1 << 2;
const uint8_t FGET_PREFIX                       = 1 << 5;

/* ================================================== */
// scan flags

// the value of the scanned entry will be treated as the key with its value retrieved.
// this flag requires a start key to be activated
const uint8_t FSCAN_VALUE_AS_KEY                = 1;
const uint8_t FSCAN_CACHE                       = 1 << 1;
const uint8_t FSCAN_KEYS_ONLY                   = 1 << 2;
const uint8_t FSCAN_DESC                        = 1 << 3;
const uint8_t FSCAN_EXCLUDE_START_KEY           = 1 << 4;
const uint8_t FSCAN_RELATIVE_LIMIT              = 1 << 5;
const uint8_t FSCAN_DELETE_RESULT               = 1 << 6;
const uint8_t FSCAN_DELETE_RESULT_SYNC          = 1 << 7; // sync

/* ================================================== */
// utility methods

inline uint64_t
readPtrDB(char* buf)
{
    return *(uint64_t*)(buf + protostuff::OFFSET_PTR_DB);
}

inline uint64_t
readPtrWriteBatch(char* buf)
{
    return *(uint64_t*)(buf + protostuff::OFFSET_PTR_WB);
}

inline uint64_t
readPtrIterator(char* buf)
{
    return *(uint64_t*)(buf + protostuff::OFFSET_PTR_IT);
}

/* ================================================== */
// writes

inline void
writePtrDB(char* buf, uint64_t value)
{
    *(uint64_t*)(buf + protostuff::OFFSET_PTR_DB) = value;
}

inline void
writePtrWriteBatch(char* buf, uint64_t value)
{
    *(uint64_t*)(buf + protostuff::OFFSET_PTR_WB) = value;
}

inline void
writePtrIterator(char* buf, uint64_t value)
{
    *(uint64_t*)(buf + protostuff::OFFSET_PTR_IT) = value;
}

/* ================================================== */
// delegated methods

intptr_t 
newDb(char* const buf, char* in, char* const endPtr, 
        const std::string& path, const uint8_t flags);

void 
deleteDb(const intptr_t ptr);

bool 
backupDb(const intptr_t ptr, 
        const uint8_t flags, 
        const char* str, const uint16_t strlen);

bool 
pipeDb(const intptr_t ptr, 
        const uint8_t flags, 
        const intptr_t ptrTo);

bool 
flushWriteBatch(const intptr_t batchPtr, const intptr_t dbPtr, const bool sync);

void 
deleteWriteBatch(const intptr_t ptr);

void 
deleteIterator(const intptr_t ptr);

bool
sput(const intptr_t dbPtr, 
        const uint8_t flags, 
        const char* k, const uint16_t klen, 
        const char* v, const uint16_t vlen);

bool
put(char* const buf, char* in, char* const endPtr, const bool end, 
        const intptr_t dbPtr, const intptr_t batchPtr, 
        const uint8_t flags);

bool
sget(const intptr_t dbPtr, 
        const uint8_t flags, 
        const char* key, const uint16_t keylen, 
        std::string* value);

bool
iget(const intptr_t dbPtr, 
        char* buf, char* const endPtr, 
        const uint8_t flags, 
        const char* key, const uint16_t keylen);

bool
get(char* const buf, char* in, char* const endPtr, const bool end, const uint16_t buflen, 
        const intptr_t dbPtr, const intptr_t itPtr, 
        const uint8_t flags);

bool
scan(char* const buf, char* const bufTmp, 
        char* in, char* const endPtr, const bool end, const uint16_t buflen, 
        const intptr_t dbPtr, const intptr_t itPtr, const intptr_t batchPtr, 
        const uint8_t flags, 
        const bool keysOnly, const bool desc, const bool deleteResult, 
        const uint16_t limit, 
        const char* limitKey, const uint16_t limitKeyLen, 
        const char* startKey, const uint16_t startKeyLen);

} // PS_LSMDB_H_

#endif

