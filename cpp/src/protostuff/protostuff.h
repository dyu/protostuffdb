//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#ifndef PS_PROTOSTUFF_H_
#define PS_PROTOSTUFF_H_

#include <iostream>
#include <cstring>
#include <cstdint>
#include <jni.h>

namespace protostuff {

// ==================================================
// global flags

extern uint8_t flags;

const uint8_t FGLOBAL_CHECKSUM                  = 1;

// ==================================================
// constants

const uint16_t TERMINATOR = 0xFFFF;

const uint8_t WIRETYPE_STRING           = 0;
const uint8_t WIRETYPE_BYTES            = 1;
const uint8_t WIRETYPE_UINT8            = 2;
const uint8_t WIRETYPE_UINT16           = 3;
const uint8_t WIRETYPE_UINT32           = 4;
const uint8_t WIRETYPE_UINT64           = 5;
const uint8_t WIRETYPE_FLOAT            = 6;
const uint8_t WIRETYPE_DOUBLE           = 7;

const uint8_t WIRETYPE_K                = 8;
//const uint8_t WIRETYPE_V                = 9;
const uint8_t WIRETYPE_KV               = 9;
const uint8_t WIRETYPE_KVAO             = 10; // kv with absolute offset
const uint8_t WIRETYPE_KVRO             = 11; // kv with relative offset
const uint8_t WIRETYPE_KVRKP            = 12; // kv with relative offset, replace key prefix

const uint8_t WIRETYPE_CHUNK            = 13;
const uint8_t WIRETYPE_LIMIT            = 14;
const uint8_t WIRETYPE_END              = 15;

const uint8_t TAG_TYPE_BITS = 4;
const uint8_t TAG_TYPE_MASK = (1 << TAG_TYPE_BITS) - 1;

/* ================================================== */
// error types

const uint8_t ERR_RUNTIME                       = 1;
const uint8_t ERR_OPERATION                     = 2;
const uint8_t ERR_NOT_ENOUGH_SPACE_TO_WRITE     = 3;

const uint8_t ERR_NOT_EXPECTING_MORE_ARGS       = 4;
const uint8_t ERR_EXPECTED_MORE_ARGS            = 5;
const uint8_t ERR_EXPECTED_END_OR_LIMIT         = 6;
const uint8_t ERR_EXPECTED_STRING               = 7;
const uint8_t ERR_EXPECTED_BYTES                = 8;
const uint8_t ERR_EXPECTED_UINT8                = 9;
const uint8_t ERR_EXPECTED_UINT16               = 10;
const uint8_t ERR_EXPECTED_UINT32               = 11;
const uint8_t ERR_EXPECTED_UINT64               = 12;
const uint8_t ERR_EXPECTED_FLOAT                = 13;
const uint8_t ERR_EXPECTED_DOUBLE               = 14;

const uint8_t ERR_EXPECTED_K                    = 15;
const uint8_t ERR_EXPECTED_V                    = 16;
const uint8_t ERR_EXPECTED_KV_VARIANT           = 17;

const uint8_t ERR_EMPTY_STRING                  = 18;
const uint8_t ERR_EMPTY_BYTES                   = 19;

/* ================================================== */
// const offsets

const uint8_t OFFSET_CTRL = 1, 
        OFFSET_BUFLEN = OFFSET_CTRL + 3, 
        OFFSET_PTRS = OFFSET_BUFLEN + 2, 
        OFFSET_RC = OFFSET_PTRS + 24, 
        OFFSET_WC = OFFSET_RC + 2, 
        OFFSET_KVAO = OFFSET_WC + 2, 
        OFFSET_START = OFFSET_KVAO + 4;

/* ================================================== */
// utility functions

inline uint16_t
readRC(char* buf)
{
    return *(uint16_t*)(buf + protostuff::OFFSET_RC);
}

inline void
writeRC(char* buf, uint16_t value)
{
    *(uint16_t*)(buf + protostuff::OFFSET_RC) = value;
}

inline uint16_t
readWC(char* buf)
{
    return *(uint16_t*)(buf + protostuff::OFFSET_WC);
}

inline void
writeWC(char* buf, uint16_t value)
{
    *(uint16_t*)(buf + protostuff::OFFSET_WC) = value;
}

/* ================================================== */
// read functions

bool
readK(char* const buf, char** inPtr, 
        char** k, uint8_t* klen);

/*bool
readV(char* const buf, char** inPtr, 
        char** v, uint16_t* vlen);*/

bool
readKV(char* const buf, char** inPtr, 
        char** k, uint16_t* klen, 
        char** v, uint16_t* vlen, 
        bool* rkp);

bool
readString(char* const buf, char* endPtr, char** inPtr, 
        char** str, uint16_t* strLen, const bool mustNotBeEmpty);

bool
readBytes(char* const buf, char* endPtr, char** inPtr, 
        char** v, uint16_t* vlen, const bool mustNotBeEmpty);

bool
read8(char* const buf, char* endPtr, char** inPtr, 
        uint8_t* target);

bool
read16(char* const buf, char* endPtr, char** inPtr, 
        uint16_t* target);

bool
read32(char* const buf, char* endPtr, char** inPtr, 
        uint32_t* target);

bool
read64(char* const buf, char* endPtr, char** inPtr, 
        uint64_t* target);

/* ================================================== */
// writer functions

inline void
write8(char* buf, uint8_t value)
{
    *(uint8_t*)(buf) = value;
}

inline void
write16(char* buf, uint16_t value)
{
    *(uint16_t*)(buf) = value;
}


inline void
write32(char* buf, uint32_t value)
{
    *(uint32_t*)(buf) = value;
}

inline void
write64(char* buf, uint64_t value)
{
    *(uint64_t*)(buf) = value;
}

inline void
writeKTo(char* out, const char* k, uint16_t klen)
{
    *(uint16_t*)(out) = klen;
    out += 2;

    memcpy(out, k, klen);
    out += klen;
}

inline void
writeKVTo(char* out, const char* k, uint16_t klen, const char* v, uint16_t vlen)
{
    *(uint16_t*)(out) = klen;
    out += 2;

    *(uint16_t*)(out) = vlen;
    out += 2;

    memcpy(out, k, klen);
    out += klen;
    
    memcpy(out, v, vlen);
    out += vlen;
}

/* ================================================== */
// utility methods

inline bool
isChunk(char* buf)
{
    return protostuff::WIRETYPE_CHUNK == *(uint8_t*)(buf + protostuff::OFFSET_CTRL);
}

inline uint16_t
readBufLen(char* buf)
{
    return *(uint16_t*)(buf + protostuff::OFFSET_BUFLEN);
}

/* ================================================== */
// jni

inline char* const
buf_from_bao(jbyteArray data, jint bao)
{
    return (char*)(*(uintptr_t*)data) + bao;
}

char * const 
getBuf(JNIEnv * env, jbyteArray data, bool* end, char** endPtr, 
        char** inPtr);

char * const 
getBufDirect(JNIEnv * env, char* const buf, bool* end, char** endPtr, 
        char** inPtr);

} // PS_PROTOSTUFF_H_

#endif

