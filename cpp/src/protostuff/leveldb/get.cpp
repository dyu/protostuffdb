//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#if defined(HYPER)
#include <hyperleveldb/db.h>
#else
#include <leveldb/db.h>
#endif

#include "../lsmdb.h"

namespace protostuff {

static bool
writeKVTo(char** out, const leveldb::Slice& kseek, 
        const leveldb::Slice& ks, const leveldb::Iterator* it, 
        char* buf, char* const endPtr, uint16_t* remainingWriteLen)
{
    // 0 != kseek.size_ && (kseek.size_ != ks.size_ || 0 != memcmp(kseek.data_, ks.data_, kseek.size_))
    if (0 != kseek.size_ && kseek != ks)
    {
        // key does not match
        
        if (2 > *remainingWriteLen)
            return false;

        *remainingWriteLen -= 2;

        // write zero
        protostuff::write16(*out, 0);
        
        *out += 2;
        
        return true;
    }

    const leveldb::Slice& vs = it->value();
    uint16_t total = 4 + ks.size_ + vs.size_;
    
    if (total > *remainingWriteLen)
        return false;
    
    *remainingWriteLen -= total;

    protostuff::writeKVTo(*out, ks.data_, ks.size_, vs.data_, vs.size_);

    *out += total;

    return true;
}

static bool
get_continue(char* const buf, 
        char* in, char* const endPtr, const bool end, const uint16_t buflen, 
        leveldb::DB* db, leveldb::Iterator* it, 
        char* out)
{
    uint16_t remaining = (uint16_t)(buf + buflen - endPtr);

    char* k;
    uint8_t klen;
    leveldb::Slice kseek;

    // continue where you left off
    if (it->Valid())
    {
        if (!protostuff::writeKVTo(&out, kseek, it->key(), it, buf, endPtr, &remaining))
        {
            *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
            return false;
        }
    }
    else if (remaining >= 2)
    {
        // empty
        *(uint16_t*)(out) = 0;
        out += 2;
        remaining -= 2;
    }
    else
    {
        // empty but not enough space
        *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
        return false;
    }

    if (0 == protostuff::readRC(buf))
    {
        if (remaining >= 2)
        {
            // terminator
            *(uint16_t*)(out) = protostuff::TERMINATOR;

            // TODO signify that this is the end that allows the client's count to be zero.
        }

        // clear the w-continuation
        protostuff::writeWC(buf, 0);

        if (end)
            delete it;

        return true;
    }
    
    // jump to the read-continuation
    in = buf + protostuff::readRC(buf);
    do
    {
        if (!protostuff::readK(buf, &in, &k, &klen))
        {
            // clear
            protostuff::writePtrIterator(buf, 0);
            
            // delete
            delete it;
            
            return false;
        }

        kseek.data_ = k;
        kseek.size_ = klen;
        
        it->Seek(kseek);

        if (it->Valid())
        {
            if (!protostuff::writeKVTo(&out, kseek, it->key(), it, buf, endPtr, &remaining))
            {
                // not enough space
                // write the rw-continuation
                protostuff::writeWC(buf, endPtr - buf);
                
                protostuff::writeRC(buf, endPtr == in ? 0 : in - buf);
                
                if (remaining >= 2)
                {
                    // terminator
                    *(uint16_t*)(out) = protostuff::TERMINATOR;
                }
                
                return true;
            }
        }
        else if (remaining >= 2)
        {
            // key not found
            *(uint16_t*)(out) = 0;
            out += 2;
            remaining -= 2;
        }
        else
        {
            // key not found, but not enough space
            // write the rw-continuation
            protostuff::writeWC(buf, endPtr - buf);
            
            protostuff::writeRC(buf, endPtr == in ? 0 : in - buf);

            return true;
        }
    }
    while (endPtr != in);

    if (remaining >= 2)
    {
        // terminator
        *(uint16_t*)(out) = protostuff::TERMINATOR;
    }

    // clear the rw-continuation
    protostuff::writeRC(buf, 0);
    protostuff::writeWC(buf, 0);
    
    if (!end)
        return true;
    
    // clear
    protostuff::writePtrIterator(buf, 0);
    
    delete it;
    
    return true;
}

bool
get(char* const buf, char* in, char* const endPtr, const bool end, const uint16_t buflen, 
        const intptr_t dbPtr, const intptr_t itPtr, 
        const uint8_t flags)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
        return false;

    /* ================================================== */
    
    leveldb::Iterator* it;
    uint16_t wcOffset = protostuff::readWC(buf);
    if (wcOffset != 0)
    {
        return 0 != itPtr && nullptr != (it = (leveldb::Iterator*)itPtr) && get_continue(
                buf,
                in, endPtr, end, buflen,
                db, it, 
                buf + wcOffset);
    }

    /* ================================================== */
    
    if (itPtr == 0)
    {
        leveldb::ReadOptions ro;
        ro.verify_checksums = 0 != (protostuff::flags & protostuff::FGLOBAL_CHECKSUM);
        ro.fill_cache = 0 != (flags & protostuff::FGET_CACHE);
        
        it = db->NewIterator(ro);
    }
    else
    {
        it = (leveldb::Iterator*)itPtr;
    }
    
    if (it == nullptr)
        return false;
    
    char* out = endPtr;
    uint16_t remaining = (uint16_t)(buf + buflen - endPtr);

    char* k;
    uint8_t klen;
    leveldb::Slice kseek;
    
    do
    {
        if (!protostuff::readK(buf, &in, &k, &klen))
        {
            // clear
            if (itPtr != 0)
                protostuff::writePtrIterator(buf, 0);
            
            // delete
            delete it;
            
            return false;
        }

        kseek.data_ = k;
        kseek.size_ = klen;
        
        it->Seek(kseek);

        if (it->Valid())
        {
            if (!protostuff::writeKVTo(&out, kseek, it->key(), it, buf, endPtr, &remaining))
            {
                if (out == endPtr)
                {
                    if (itPtr == 0)
                        delete it;
                    
                    *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
                    return false;
                }

                // not enough space
                // write the rw-continuation
                protostuff::writeWC(buf, endPtr - buf);
                
                if (endPtr != in)
                    protostuff::writeRC(buf, in - buf);

                if (itPtr == 0)
                    protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);
                
                if (remaining >= 2)
                {
                    // terminator
                    *(uint16_t*)(out) = protostuff::TERMINATOR;
                }
                
                return true;
            }
        }
        else if (remaining >= 2)
        {
            // key not found
            *(uint16_t*)(out) = 0;
            out += 2;
            remaining -= 2;
        }
        else if (out == endPtr)
        {
            if (itPtr == 0)
                delete it;
            
            *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
            return false;
        }
        else
        {
            // key not found, but not enough space
            // write the rw-continuation
            protostuff::writeWC(buf, endPtr - buf);
            
            if (endPtr != in)
                protostuff::writeRC(buf, in - buf);

            if (itPtr == 0)
                protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);

            return true;
        }
    }
    while (endPtr != in);

    if (remaining >= 2)
    {
        // terminator
        *(uint16_t*)(out) = protostuff::TERMINATOR;
    }
    
    if (end)
    {
        // clear
        if (itPtr != 0)
            protostuff::writePtrIterator(buf, 0);
        
        delete it;
    }
    else if (itPtr == 0)
    {
        protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);
    }
    
    return true;
}

} // protostuff

