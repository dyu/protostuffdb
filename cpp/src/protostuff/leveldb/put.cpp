//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#if defined(HYPER)
#include <hyperleveldb/db.h>
#include <hyperleveldb/write_batch.h>
#else
#include <leveldb/db.h>
#include <leveldb/write_batch.h>
#endif

#include "../lsmdb.h"

namespace protostuff {

bool
put(char* const buf, char* in, char* const endPtr, const bool end, 
        const intptr_t dbPtr, const intptr_t batchPtr, 
        const uint8_t flags)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
        return false;
    
    leveldb::WriteBatch* batch = batchPtr == 0 ? new leveldb::WriteBatch : (leveldb::WriteBatch*)batchPtr;
    if (batch == nullptr)
        return false;
    
    char* k;
    char* v;
    char c1, c2;
    bool rkp = false;
    uint16_t klen, vlen;
    leveldb::Slice ks, vs;
    do
    {
        if (!protostuff::readKV(buf, &in, &k, &klen, &v, &vlen, &rkp))
        {
            // clear
            if (batchPtr != 0)
                protostuff::writePtrWriteBatch(buf, 0);
            
            // delete
            delete batch;
            
            return false;
        }
        
        if (rkp)
        {
            // reset
            rkp = false;
            
            // persist old values
            c1 = *(k + 1);
            c2 = *(k + 2);

            // replace
            *(k + 1) = *(in - 2);
            *(k + 2) = *(in - 1);
            
            ks.data_ = k + 1;
            ks.size_ = klen - 1;

            if (vlen == 0)
            {
                // empty value means delete
                batch->Delete(ks);
            }
            else
            {
                vs.data_ = v;
                vs.size_ = vlen;
                
                batch->Put(ks, vs);
            }

            // restore
            *(k + 1) = c1;
            *(k + 2) = c2;
        }
        else
        {
            ks.data_ = k;
            ks.size_ = klen;
            
            if (vlen == 0)
            {
                // empty value means delete
                batch->Delete(ks);
            }
            else
            {
                vs.data_ = v;
                vs.size_ = vlen;
                
                batch->Put(ks, vs);
            }
        }
    }
    while (endPtr != in);
    
    if (!end)
    {
        // store the write batch if not yet already stored
        if (batchPtr == 0)
            protostuff::writePtrWriteBatch(buf, (uint64_t)(intptr_t)batch);
        
        return true;
    }
    
    // clear
    if (batchPtr != 0)
        protostuff::writePtrWriteBatch(buf, 0);
    
    // perform write
    leveldb::WriteOptions wo;
    wo.sync = 0 != (flags & protostuff::FPUT_SYNC);
    
    #ifdef REP_MASTER
    bool ok = RepEntry::db_map()[db]->write(wo, batch);
    #else
    bool ok = db->Write(wo, batch).ok();
    #endif
    
    delete batch;
    if (!ok)
        *buf = protostuff::ERR_WRITEBATCH_FLUSH;
    return ok;
}

} // protostuff

