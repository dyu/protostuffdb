//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#if defined(HYPER)
#include <hyperleveldb/db.h>
#include <hyperleveldb/write_batch.h>
#else
#include <leveldb/db.h>
#include <leveldb/write_batch.h>
#endif

#include "../lsmdb.h"

#ifdef REP_MASTER
#if defined(HYPER)
#include <hyperleveldb/env.h>
#else
#include <leveldb/env.h>
#endif
#endif

namespace protostuff {

intptr_t 
newDb(char* const buf, char* in, char* const endPtr, 
        const std::string& path, const uint8_t flags)
{
    leveldb::Options options;
    
    options.create_if_missing = 0 != (flags & protostuff::FNEWDB_CREATE_IF_MISSING);
    
    options.error_if_exists = 0 != (flags & protostuff::FNEWDB_ERROR_IF_EXISTS);
    
    options.paranoid_checks = 0 != (flags & protostuff::FNEWDB_PARANOID_CHECKS);
    
    options.write_buffer_size = write_buffer_mb << 20;
    
    leveldb::DB* db;
    #if defined(REP_MASTER) || defined(REP_SLAVE)
    if (!leveldb::DB::Open(options, path, &db).ok())
        return 0;
    
    size_t slash = path.rfind('/');
    #ifdef _WIN32
    if (slash == std::string::npos)
        slash = path.rfind('\\');
    #endif
    
    leveldb::ReadOptions ro;
    //ro.verify_checksums = 0 != (protostuff::flags & protostuff::FGLOBAL_CHECKSUM);
    //ro.fill_cache = 0 != (flags & protostuff::FSCAN_CACHE);
    auto it = db->NewIterator(ro);
    if (it == nullptr)
        return 0;
    
    uint64_t seq_num = 0;
    leveldb::Slice ks;
    
    it->SeekToLast();
    
    if (it->Valid() && END_KEY_LEN == (ks = it->key()).size_ && SEQ_KEY_START == *(uint8_t*)ks.data_)
    {
        // end key
        it->Prev();
        if (it->Valid() && SEQ_KEY_LEN == (ks = it->key()).size_ && SEQ_KEY_START == *(uint8_t*)ks.data_)
        {
            // valid seq_num
            seq_num = decodeSeq(ks.data_ + 1);
        }
    }
    
    delete it;
    
    #if defined(REP_SLAVE)
    auto entry = new RepEntry(
        path,
        std::string(path.data() + slash + 1, path.size() - slash - 1),
        db,
        seq_num + 1, // the next one to query
        static_cast<uint8_t>(RepEntry::list.size())
    );
    RepEntry::list.push_back(entry);
    #else
    // create log file
    auto env = leveldb::Env::Default();
    std::string log_path;
    log_path += path;
    log_path += "/binlogs";
    if (!env->FileExists(log_path) && !env->CreateDir(log_path).ok())
        return 0;
    
    log_path += "/rep.log";
    
    leveldb::Status s;
    leveldb::WritableFile* wf;
    uint64_t file_size = 0;
    if (env->FileExists(log_path))
    {
        #if defined(HYPER)
        s = env->NewAppendableFile(log_path, &wf);
        #else
        s = env->NewSharedAppendableFile(log_path, &wf);
        #endif
    }
    
    if (!s.ok())
        return 0;
    
    leveldb::SequentialFile* sf;
    #if defined(HYPER)
    s = env->NewSequentialFile(log_path, &sf);
    #else
    s = env->NewSharedSequentialFile(log_path, &sf);
    #endif
    if (!s.ok())
        return 0;
    
    auto entry = new RepEntry(
        path,
        std::string(path.data() + slash + 1, path.size() - slash - 1),
        log_path,
        wf, sf, file_size,
        db,
        seq_num,
        static_cast<uint8_t>(RepEntry::db_map().size())
    );
    leveldb::Slice record;
    std::string scratch;
    if (file_size != 0)
    {
        // read oob_num
        if (!entry->reader.ReadRecord(&record, &scratch) || record.size_ != 8)
        {
            delete entry;
            return 0;
        }
        
        entry->oob_num = *(uint64_t*)record.data_;
    }
    else if (entry->writer.AddRecord(leveldb::Slice((char*)&seq_num, 8)).ok())
    {
        // wrote the oob_num
        entry->oob_num = seq_num;
    }
    else
    {
        // could not write oob_num
        delete entry;
        return 0;
    }
    
    #ifdef REP_MT
    entry->reader_seq_num = seq_num;
    #endif
    
    RepEntry::db_map()[db] = entry;
    #endif
    
    RepEntry::name_map()[entry->name] = entry;
    return (intptr_t)db; 
    #elif defined(SYNC_NODE)
    if (!leveldb::DB::Open(options, path, &db).ok())
        return 0;
    
    size_t slash = path.rfind('/');
    #ifdef _WIN32
    if (slash == std::string::npos)
        slash = path.rfind('\\');
    #endif
    
    auto entry = new RepEntry(
        path,
        std::string(path.data() + slash + 1, path.size() - slash - 1),
        db,
        static_cast<uint8_t>(RepEntry::name_map().size())
    );
    //RepEntry::db_map()[db] = entry;
    RepEntry::name_map()[entry->name] = entry;
    
    return (intptr_t)db;
    #else
    return leveldb::DB::Open(options, path, &db).ok() ? (intptr_t)db : 0;
    #endif
}

void
deleteDb(const intptr_t ptr)
{
    delete (leveldb::DB*)ptr;
}

bool
backupDb(const intptr_t ptr, 
        const uint8_t flags, 
        const char* v, const uint16_t vlen)
{
    #if defined(HYPER)
    leveldb::DB* db = (leveldb::DB*)ptr;
    
    leveldb::Slice name(v, vlen);
    
    leveldb::Status status = db->LiveBackup(name);
    
    bool ok = status.ok();
    
    if (!ok)
        fprintf(stderr, "%s backup failed: %s\n", v, status.ToString().c_str());
    
    return ok;
    #else
    return false;
    #endif
}

bool
pipeDb(const intptr_t ptrFrom,
        const uint8_t flags, const intptr_t ptrTo)
{
    leveldb::DB* dbFrom = (leveldb::DB*)ptrFrom;
    leveldb::DB* dbTo = (leveldb::DB*)ptrTo;
    
    leveldb::WriteOptions wo;
    leveldb::ReadOptions ro;
    ro.verify_checksums = 0 != (protostuff::flags & protostuff::FGLOBAL_CHECKSUM);
    ro.fill_cache = 0 != (flags & protostuff::FSCAN_CACHE);
    
    leveldb::Iterator* it = dbFrom->NewIterator(ro);
    
    bool ok = true;
    int64_t count = 0;
    for (it->SeekToFirst(); it->Valid(); it->Next())
    {
        if (!dbTo->Put(wo, it->key(), it->value()).ok())
        {
            ok = false;
            break;
        }
        count++;
    }
    
    if (!ok || !(ok = it->status().ok()))
        fprintf(stderr, "pipe failed\n");
    else
        std::cerr << count << " entries were transferred.\n";
    
    delete it;
    
    return ok;
}

bool 
flushWriteBatch(const intptr_t batchPtr, const intptr_t dbPtr, const bool sync)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
        return false;

    leveldb::WriteBatch* batch = (leveldb::WriteBatch*)batchPtr;
    if (batch == nullptr)
        return false;
    
    // perform write
    leveldb::WriteOptions wo;
    wo.sync = sync;
    
    #ifdef REP_MASTER
    bool ok = RepEntry::db_map()[db]->write(wo, batch);
    #else
    bool ok = db->Write(wo, batch).ok();
    #endif
    
    delete batch;
    return ok;
}

void
deleteWriteBatch(const intptr_t ptr)
{
    delete (leveldb::WriteBatch*)ptr;
}

void
deleteIterator(const intptr_t ptr)
{
    delete (leveldb::Iterator*)ptr;
}

bool
sput(const intptr_t dbPtr, 
        const uint8_t flags, 
        const char* k, const uint16_t klen, 
        const char* v, const uint16_t vlen)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
        return false;
    
    leveldb::Slice ks(k, klen);
    
    leveldb::WriteOptions wo;
    wo.sync = (flags & protostuff::FPUT_SYNC);
    #ifdef REP_MASTER
    leveldb::WriteBatch batch;
    if (vlen == 0)
        batch.Delete(ks);
    else
        batch.Put(ks, leveldb::Slice(v, vlen));
    
    return RepEntry::db_map()[db]->write(wo, &batch);
    #else
    return vlen == 0 ? db->Delete(wo, ks).ok() : db->Put(wo, ks, leveldb::Slice(v, vlen)).ok();
    #endif
}

bool
sget(const intptr_t dbPtr, 
        const uint8_t flags, 
        const char* key, const uint16_t keylen, 
        std::string* value)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
        return false;
    
    leveldb::Slice ks(key, keylen);
    
    leveldb::ReadOptions ro;
    ro.verify_checksums = 0 != (protostuff::flags & protostuff::FGLOBAL_CHECKSUM);
    ro.fill_cache = 0 != (flags & protostuff::FGET_CACHE);
    
    leveldb::Status status = db->Get(ro, ks, value);
    
    return status.ok() || status.IsNotFound();
}

bool
iget(const intptr_t dbPtr, 
        char* buf, char* const endPtr, 
        const uint8_t flags, 
        const char* key, const uint16_t keylen)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
    {
        *buf = protostuff::ERR_OPERATION;
        return false;
    }
    
    const bool close = 0 != (flags & protostuff::FOP_CLOSE);
    
    intptr_t itPtr = (intptr_t)protostuff::readPtrIterator(buf);
    
    leveldb::Iterator* it;
    if (itPtr == 0)
    {
        leveldb::ReadOptions ro;
        ro.verify_checksums = 0 != (protostuff::flags & protostuff::FGLOBAL_CHECKSUM);
        ro.fill_cache = 0 != (flags & protostuff::FGET_CACHE);
        
        it = db->NewIterator(ro);
        if (it == nullptr)
        {
            *buf = protostuff::ERR_OPERATION;
            return false;
        }
        
        if (!close)
            protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);
    }
    else
    {
        it = (leveldb::Iterator*)itPtr;
        if (close)
            protostuff::writePtrIterator(buf, 0);
    }
    
    leveldb::Slice kseek(key, keylen);
    
    it->Seek(kseek);
    
    if (!it->Valid())
    {
        if (close)
            delete it;
        
        if (2 > (uint16_t)(buf + protostuff::readBufLen(buf) - endPtr))
        {
            *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
            return false;
        }
        
        *(endPtr) = 0;
        *(endPtr + 1) = 0;
        return true;
    }
    
    const leveldb::Slice& ks = it->key();
    bool precondition = 0 == (flags & protostuff::FGET_PREFIX) ?
            ks.size_ == keylen : ks.size_ >= keylen;
    if (!precondition || 0 != memcmp(key, ks.data_, keylen))
    {
        if (close)
            delete it;
        
        if (2 > (uint16_t)(buf + protostuff::readBufLen(buf) - endPtr))
        {
            *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
            return false;
        }
        
        *(endPtr) = 0;
        *(endPtr + 1) = 0;
        return true;
    }
    
    if (0 != (flags & protostuff::FGET_KEY_ONLY))
    {
        precondition = ks.size_ + 2 <= (uint16_t)(buf + protostuff::readBufLen(buf) - endPtr);
        if (precondition)
            protostuff::writeKTo(endPtr, ks.data_, ks.size_);
        else
            *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
        
        if (close)
            delete it;
        
        return precondition;
    }
    
    const leveldb::Slice& vs = it->value();
    
    precondition = ks.size_ + vs.size_ + 4 <= (uint16_t)(buf + protostuff::readBufLen(buf) - endPtr);
    if (precondition)
        protostuff::writeKVTo(endPtr, ks.data_, ks.size_, vs.data_, vs.size_);
    else
        *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
    
    if (close)
        delete it;
    
    return precondition;
}

} // protostuff

