//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#if defined(HYPER)
#include <hyperleveldb/db.h>
#include <hyperleveldb/write_batch.h>
#else
#include <leveldb/db.h>
#include <leveldb/write_batch.h>
#endif

#include "../lsmdb.h"

namespace protostuff {

static const uint16_t MAX_VALUE_AS_KEY_COUNT = (0xFFFF - 4)/18;

static bool
writeKVTo(char** out, const bool keysOnly, const bool desc, const bool valueAsKey, 
        bool* limitReached, const leveldb::Slice& klimit, 
        const leveldb::Slice& ks, const leveldb::Iterator* it, leveldb::WriteBatch* batch, 
        char* bufTmp, 
        uint16_t* remainingWriteLen)
{
    if (0 != klimit.size_)
    {
        // with limit key
        if (desc)
        {
            if (ks.compare(klimit) > 0)
                goto ok;
        }
        else
        {
            if (ks.compare(klimit) < 0)
                goto ok;
        }
        
        *limitReached = true;
        return true;
    }

    ok:
    
    uint16_t total;
    leveldb::Slice vs;
    
    if (keysOnly)
    {
        vs = valueAsKey ? it->value() : ks;
        
        total = 2 + vs.size_;
        
        if (total > *remainingWriteLen)
            return false;

        *remainingWriteLen -= total;

        protostuff::writeKTo(*out, vs.data_, vs.size_);

        *out += total;
        
        if (batch != nullptr)
            batch->Delete(vs);
        
        return true;
    }
    else if (valueAsKey)
    {
        vs = it->value();
        // check if key is valid
        if (vs.size_ < 9)
            return false;
        
        // as count
        total = *(uint16_t*)bufTmp;
        if (total == MAX_VALUE_AS_KEY_COUNT)
            return false;
        // increment count
        write16(bufTmp, total + 1);
        
        // convert to offset
        total = 2 + (total * 18);
        
        // extract end key from key and copy to bufTmp
        memcpy(bufTmp + total, ks.data_ + ks.size_ - 9, 9);
        // extract end key from val and copy to bufTmp
        memcpy(bufTmp + total + 9, vs.data_ + vs.size_ - 9, 9);
        
        return true;
    }
    
    vs = it->value();
    
    total = 4 + ks.size_ + vs.size_;
    
    if (total > *remainingWriteLen)
        return false;
    
    *remainingWriteLen -= total;

    protostuff::writeKVTo(*out, ks.data_, ks.size_, vs.data_, vs.size_);

    *out += total;
    
    if (batch != nullptr)
        batch->Delete(ks);
    
    return true;
}

static bool
writeKVTo(char** out, const leveldb::Slice& ks, const leveldb::Iterator* it,
        uint16_t* remainingWriteLen)
{
    const leveldb::Slice vs = it->value();
    uint16_t total = 4 + ks.size_ + vs.size_;
    
    if (total > *remainingWriteLen)
        return false;
    
    *remainingWriteLen -= total;
    protostuff::writeKVTo(*out, ks.data_, ks.size_, vs.data_, vs.size_);
    
    *out += total;
    
    return true;
}

static uint16_t
resolveMatchKeyLen(uint16_t startKeyLen, uint8_t flags, uint16_t limitKeyLen, bool desc)
{
    if (0 == startKeyLen || 0 == (flags & protostuff::FSCAN_RELATIVE_LIMIT))
        return 0;
    
    if (!desc)
        limitKeyLen -= 1;
    
    // tag index (2) + key (9)
    if (limitKeyLen < 11)
        limitKeyLen += 9;
    
    return limitKeyLen;
}

static bool
isLimitReached(uint16_t matchKeyLen, uint16_t limit, bool desc, leveldb::Slice& key, uint16_t* count, bool* matchBeforeVisit)
{
    if (matchKeyLen == 0)
        return limit == ++(*count);
    
    if (matchKeyLen != key.size_ || limit > ++(*count))
        return false;
    
    if (desc)
        return true;
    
    *matchBeforeVisit = true;
    return false;
}

static bool
scan_continue(char* const buf, char* const bufTmp, 
        char* const endPtr, const bool end, const uint16_t buflen, 
        const uint8_t flags, 
        const bool keysOnly, const bool desc, const bool deleteResult, 
        const uint16_t limit, 
        const char* limitKey, const uint16_t limitKeyLen, 
        const char* startKey, const uint16_t startKeyLen,
        leveldb::DB* db, leveldb::Iterator* it, leveldb::WriteBatch* batch, 
        char* out)
{
    uint16_t remaining = (uint16_t)(buf + buflen - endPtr);

    leveldb::Slice klimit(limitKey, limitKeyLen);
    
    const bool valueAsKey = !deleteResult && startKeyLen != 0 && 0 != (flags & protostuff::FSCAN_VALUE_AS_KEY);
    const bool writeToBufTmp = valueAsKey && !keysOnly;

    uint16_t count = !writeToBufTmp && limit == 0 ? 0 : protostuff::readRC(buf); // load last count
    bool limitReached = false, matchBeforeVisit = limit != 0 && limit == count;
    
    uint16_t i, matchKeyLen;

    // continue where you left off
    leveldb::Slice key;
    
    if (writeToBufTmp)
    {
        if (!protostuff::writeKVTo(&out, it->key(), it, &remaining))
        {
            *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
            return false;
        }
        // count is the last iteration
        i = count + 1; // go to the next one
        count = *(uint16_t*)bufTmp;
        for (uint16_t offset = 2 + (i * 18); i < count; i++, offset += 18)
        {
            key.data_ = bufTmp + offset + 9;
            key.size_ = 9;
            it->Seek(key);
            
            // check if exact match
            if (!it->Valid() || 9 != (key = it->key()).size_ || 0 != memcmp(key.data_, bufTmp + offset + 9, 9))
                continue;
            
            key.data_ = bufTmp + offset;
            key.size_ = 18;
            if (protostuff::writeKVTo(&out, key, it, &remaining))
                continue;
            
            // store the current iteration
            protostuff::writeRC(buf, i);
            
            if (remaining >= 2)
            {
                // terminator
                *(uint16_t*)(out) = protostuff::TERMINATOR;
            }
            
            return true;
        }
    }
    else if (!protostuff::writeKVTo(&out, keysOnly, desc, valueAsKey, &limitReached, 
            klimit, (key = it->key()), it, batch, bufTmp, &remaining))
    {
        *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
        return false;
    }
    else if (!limitReached && (matchBeforeVisit || limit == 0 || !isLimitReached(
            matchKeyLen = resolveMatchKeyLen(startKeyLen, flags, limitKeyLen, desc), 
            limit, desc, key, &count, &matchBeforeVisit)))
    {
        // proceed
        for (desc ? it->Prev() : it->Next(); it->Valid(); desc ? it->Prev() : it->Next())
        {
            key = it->key();
            if (matchBeforeVisit && matchKeyLen == key.size_)
                break;
            if (!protostuff::writeKVTo(&out, keysOnly, desc, valueAsKey, &limitReached, 
                    klimit, key, it, batch, bufTmp, &remaining))
            {
                if (limit != 0)
                {
                    // store the count
                    protostuff::writeRC(buf, count);
                }
                
                if (remaining >= 2)
                {
                    // terminator
                    *(uint16_t*)(out) = protostuff::TERMINATOR;
                }
                
                return true;
            }
            
            if (limitReached || (!matchBeforeVisit && limit != 0 && 
                    isLimitReached(matchKeyLen, limit, desc, key, &count, &matchBeforeVisit)))
            {
                break;
            }
        }
    }
    
    if (remaining >= 2)
    {
        // terminator
        *(uint16_t*)(out) = protostuff::TERMINATOR;
    }

    // clear the rw-continuation
    protostuff::writeRC(buf, 0);
    protostuff::writeWC(buf, 0);
    
    if (!end)
        return true;
    
    // clear
    protostuff::writePtrIterator(buf, 0);
    
    delete it;

    if (!deleteResult)
        return true;

    // clear
    protostuff::writePtrWriteBatch(buf, 0);
    
    // perform write
    leveldb::WriteOptions wo;
    wo.sync = 0 != (flags & protostuff::FSCAN_DELETE_RESULT_SYNC);
    
    #ifdef REP_MASTER
    bool ok = RepEntry::db_map()[db]->write(wo, batch);
    #else
    bool ok = db->Write(wo, batch).ok();
    #endif
    
    delete batch;
    return ok;
}

bool
scan(char* const buf, char* const bufTmp, 
        char* in, char* const endPtr, const bool end, const uint16_t buflen, 
        const intptr_t dbPtr, const intptr_t itPtr, const intptr_t batchPtr, 
        const uint8_t flags, 
        const bool keysOnly, const bool desc, const bool deleteResult, 
        const uint16_t limit, 
        const char* limitKey, const uint16_t limitKeyLen, 
        const char* startKey, const uint16_t startKeyLen)
{
    leveldb::DB* db = (leveldb::DB*)dbPtr;
    if (db == nullptr)
        return false;
    
    leveldb::WriteBatch* batch = nullptr;
    if (deleteResult)
    {
        batch = batchPtr == 0 ? new leveldb::WriteBatch : (leveldb::WriteBatch*)batchPtr;
        if (batch == nullptr)
            return false;
    }
    
    /* ================================================== */
    
    leveldb::Iterator* it;
    uint16_t wcOffset = protostuff::readWC(buf);
    if (wcOffset != 0)
    {
        // write-continuation
        return 0 != itPtr && nullptr != (it = (leveldb::Iterator*)itPtr) && scan_continue(
                buf, bufTmp, 
                endPtr, end, buflen, 
                flags, 
                keysOnly, desc, deleteResult, 
                limit, 
                limitKey, limitKeyLen, 
                startKey, startKeyLen,
                db, it, batch,
                buf + wcOffset);
    }

    /* ================================================== */
    
    const bool valueAsKey = !deleteResult && startKeyLen != 0 && 0 != (flags & protostuff::FSCAN_VALUE_AS_KEY);
    if (valueAsKey && bufTmp == nullptr)
    {
        *buf = protostuff::ERR_BUF_TMP_NOT_AVAILABLE;
        return false;
    }
    
    if (itPtr == 0)
    {
        leveldb::ReadOptions ro;
        ro.verify_checksums = 0 != (protostuff::flags & protostuff::FGLOBAL_CHECKSUM);
        ro.fill_cache = 0 != (flags & protostuff::FSCAN_CACHE);
        
        it = db->NewIterator(ro);
    }
    else
    {
        it = (leveldb::Iterator*)itPtr;
    }
    
    if (it == nullptr)
        return false;
    
    char* out = endPtr;
    uint16_t remaining = (uint16_t)(buf + buflen - endPtr);

    leveldb::Slice klimit(limitKey, limitKeyLen);
    leveldb::Slice kseek(startKey, startKeyLen);
    
    uint16_t count = 0;
    bool limitReached = false, matchBeforeVisit = false, writeToBufTmp = false;
    
    uint16_t matchKeyLen;
    leveldb::Slice key;
    if (startKeyLen == 0)
    {
        // seek to first or last
        for (desc ? it->SeekToLast() : it->SeekToFirst(); it->Valid(); desc ? it->Prev() : it->Next())
        {
            key = it->key();
            if (!protostuff::writeKVTo(&out, keysOnly, desc, valueAsKey, &limitReached, 
                    klimit, key, it, batch, bufTmp, &remaining))
            {
                if (out == endPtr)
                {
                    if (itPtr == 0)
                        delete it;
                    
                    *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
                    return false;
                }
                
                // not enough space
                // write the write-continuation
                protostuff::writeWC(buf, endPtr - buf);
                
                if (limit != 0)
                {
                    // store the count
                    protostuff::writeRC(buf, count);
                }

                if (itPtr == 0)
                    protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);

                if (deleteResult && batchPtr == 0)
                    protostuff::writePtrWriteBatch(buf, (uint64_t)(intptr_t)batch);
                
                if (remaining >= 2)
                {
                    // terminator
                    *(uint16_t*)(out) = protostuff::TERMINATOR;
                }
                
                return true;
            }

            if (limitReached || (limit != 0 && limit == ++count))
                break;
        }
    }
    else
    {
        // seek to a start key
        it->Seek(kseek);

        if (it->Valid())
        {
            if (desc)
                it->Prev();
            else if (0 != (flags & protostuff::FSCAN_EXCLUDE_START_KEY) && 0 == kseek.compare(it->key()))
                it->Next();
        }
        
        writeToBufTmp = valueAsKey && !keysOnly;
        if (writeToBufTmp)
            write16(bufTmp, 0);
        
        matchKeyLen = resolveMatchKeyLen(startKeyLen, flags, limitKeyLen, desc);
        for (; it->Valid(); desc ? it->Prev() : it->Next())
        {
            key = it->key();
            if (matchBeforeVisit && matchKeyLen == key.size_)
                break;
            if (!protostuff::writeKVTo(&out, keysOnly, desc, valueAsKey, &limitReached, 
                    klimit, key, it, batch, bufTmp, &remaining))
            {
                if (writeToBufTmp)
                {
                    if (itPtr == 0)
                        delete it;
                    
                    *buf = protostuff::ERR_INVALID_VALUE_AS_KEY;
                    return false;
                }
                if (out == endPtr)
                {
                    if (itPtr == 0)
                        delete it;
                    
                    *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
                    return false;
                }
                
                // not enough space
                // write the write-continuation
                protostuff::writeWC(buf, endPtr - buf);
                
                if (limit != 0)
                {
                    // store the count
                    protostuff::writeRC(buf, count);
                }

                if (itPtr == 0)
                    protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);

                if (deleteResult && batchPtr == 0)
                    protostuff::writePtrWriteBatch(buf, (uint64_t)(intptr_t)batch);
                
                if (remaining >= 2)
                {
                    // terminator
                    *(uint16_t*)(out) = protostuff::TERMINATOR;
                }
                
                return true;
            }
            
            if (limitReached || (!matchBeforeVisit && limit != 0 && 
                    isLimitReached(matchKeyLen, limit, desc, key, &count, &matchBeforeVisit)))
            {
                break;
            }
        }
    }
    
    if (writeToBufTmp)
    {
        count = *(uint16_t*)bufTmp;
        for (uint16_t i = 0, offset = 2; i < count; i++, offset += 18)
        {
            key.data_ = bufTmp + offset + 9;
            key.size_ = 9;
            it->Seek(key);
            
            // check if exact match
            if (!it->Valid() || 9 != (key = it->key()).size_ || 0 != memcmp(key.data_, bufTmp + offset + 9, 9))
                continue;
            
            key.data_ = bufTmp + offset;
            key.size_ = 18;
            if (protostuff::writeKVTo(&out, key, it, &remaining))
                continue;
            
            // not enough space
            // write the write-continuation
            protostuff::writeWC(buf, endPtr - buf);
            
            // store the current iteration
            protostuff::writeRC(buf, i);
            
            if (itPtr == 0)
                protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);
            
            if (remaining >= 2)
            {
                // terminator
                *(uint16_t*)(out) = protostuff::TERMINATOR;
            }
            
            return true;
        }
    }

    if (remaining >= 2)
    {
        // terminator
        *(uint16_t*)(out) = protostuff::TERMINATOR;
    }
    
    if (!end)
    {
        if (itPtr == 0)
            protostuff::writePtrIterator(buf, (uint64_t)(intptr_t)it);
        
        if (deleteResult && batchPtr == 0)
            protostuff::writePtrWriteBatch(buf, (uint64_t)(intptr_t)batch);
        
        return true;
    }
    
    // clear
    if (itPtr != 0)
        protostuff::writePtrIterator(buf, 0);
    
    delete it;
    
    if (!deleteResult)
        return true;
    
    // clear
    if (batchPtr != 0)
        protostuff::writePtrWriteBatch(buf, 0);
    
    // perform write
    leveldb::WriteOptions wo;
    wo.sync = 0 != (flags & protostuff::FSCAN_DELETE_RESULT_SYNC);
    
    #ifdef REP_MASTER
    bool ok = RepEntry::db_map()[db]->write(wo, batch);
    #else
    bool ok = db->Write(wo, batch).ok();
    #endif
    
    delete batch;
    return ok;
}

} // protostuff

