//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#include "lsmdb.h"

#ifdef REP_MASTER
#include <snappy.h>
#endif

namespace protostuff {

#ifdef HYPER
int write_buffer_mb = 16;
#else
int write_buffer_mb = 4;
#endif

#ifdef SYNC_NODE
    std::unordered_map<std::string, RepEntry*>& RepEntry::name_map()
    {
        static std::unordered_map<std::string, RepEntry*> name_map;
        return name_map;
    }
    /*std::unordered_map<leveldb::DB*, RepEntry*>& RepEntry::db_map()
    {
        static std::unordered_map<leveldb::DB*, RepEntry*> db_map;
        return db_map;
    }*/
    std::string RepEntry::scratch;// = {};
#endif

#if defined(REP_MASTER) || defined(REP_SLAVE) || defined(SYNC_NODE)
    void encodeSeq(char* buf, uint64_t value)
    {
        buf[7] = value & 0xff;
        buf[6] = (value >> 8) & 0xff;
        buf[5] = (value >> 16) & 0xff;
        buf[4] = (value >> 24) & 0xff;
        buf[3] = (value >> 32) & 0xff;
        buf[2] = (value >> 40) & 0xff;
        buf[1] = (value >> 48) & 0xff;
        buf[0] = (value >> 56) & 0xff;
    }
    uint64_t decodeSeq(const char* buf)
    {
        char tmp[8];
        tmp[0] = buf[7];
        tmp[1] = buf[6];
        tmp[2] = buf[5];
        tmp[3] = buf[4];
        tmp[4] = buf[3];
        tmp[5] = buf[2];
        tmp[6] = buf[1];
        tmp[7] = buf[0];
        return *(uint64_t*)tmp;
    }
#endif

#if defined(REP_MASTER) || defined(REP_SLAVE)
    std::unordered_map<std::string, RepEntry*>& RepEntry::name_map()
    {
        static std::unordered_map<std::string, RepEntry*> name_map;
        return name_map;
    }
    
    #if defined(REP_SLAVE)
    std::vector<RepEntry*> RepEntry::list;// = {};
    std::vector<RepEntry*> RepEntry::ack_queue;// = {};
    std::vector<uint16_t> RepEntry::segments;// = {};
    std::string RepEntry::scratch;// = {};
    #else
    std::unordered_map<leveldb::DB*, RepEntry*>& RepEntry::db_map()
    {
        static std::unordered_map<leveldb::DB*, RepEntry*> db_map;
        return db_map;
    }
    std::vector<RepEntry*> RepEntry::queue;// = {};
    std::string RepEntry::scratch;// = {};
    //uint16_t RepEntry::slave_count = 0;
    
    #ifdef REP_MT
    std::vector<ReaderEntry*> ReaderEntry::list;// = {};
    #endif
    
    bool RepEntry::write(leveldb::WriteOptions& wo, leveldb::WriteBatch* batch)
    {
        char seq_val[SEQ_VAL_LEN];
        char seq_key[SEQ_KEY_LEN];
        uint64_t dest_length = writer.dest_length_;
        uint64_t new_seq_num = seq_num + 1;
        uint32_t abs_size = 0, batch_size = 0;
        
        // fill key
        *seq_key = SEQ_KEY_START;
        encodeSeq(seq_key + 1, new_seq_num);
        
        //print_bytes(std::cout, "seq_key", (const unsigned char*)seq_key, SEQ_KEY_LEN);
        
        // fill val
        memcpy(seq_val, &dest_length, 8);
        // zeroed out
        memcpy(seq_val + 8, &abs_size, 4);
        // zeroed out
        memcpy(seq_val + 12, &batch_size, 4);
        
        batch->Put(leveldb::Slice(seq_key, SEQ_KEY_LEN), leveldb::Slice(seq_val, SEQ_VAL_LEN));
        
        // writable header
        char* const header = (char*)batch->rep_.data();
        // backup header
        uint64_t header_val = *(uint64_t*)header;
        // write new seq num to header
        memcpy(header, &new_seq_num, 8);
        
        // compress
        batch_size = snappy::Compress(batch->rep_.data(), batch->rep_.size(), &RepEntry::scratch);
        
        //std::cout << "compress from " << batch->rep_.size() << " to " << batch_size << std::endl;
        
        // append char for entry_id (uint8_t)
        RepEntry::scratch.push_back('0');
        batch_size++;
        
        if (!writer.AddRecord(leveldb::Slice(RepEntry::scratch.data(), batch_size)).ok())
        {
            RepEntry::scratch.clear();
            return false;
        }
        
        // restore header
        memcpy(header, &header_val, 8);
        
        // persist size
        abs_size = static_cast<uint32_t>(writer.dest_length_ - dest_length);
        
        // write the actual sizes this time
        char* end = (char*)(batch->rep_.data() + batch->rep_.size());
        //std::cout << "must be zero: " << *(uint64_t*)(end - 8) << std::endl;
        memcpy(end - 8, &abs_size, 4);
        memcpy(end - 4, &batch_size, 4);
        
        bool ok = db->Write(wo, batch).ok();
        RepEntry::scratch.clear();
        if (!ok)
            return false;
        
        seq_num = new_seq_num;
        if (!queued)
        {
            RepEntry::queue.push_back(this);
            queued = true;
        }
        
        #ifdef REP_MT
        reader_seq_num = new_seq_num;
        #ifdef REP_MT_TIMER
        uint64_t flag = 1 << idx;
        for (auto reader_entry : ReaderEntry::list)
        {
            if (reader_entry != nullptr)
                reader_entry->queued->fetch_or(flag);
        }
        #endif
        #endif
        
        return true;
    }
    #endif
#endif
}

#ifndef PS_TOOLS
#include "jni_rpc.h"
#else
extern "C" {
#endif

#ifdef PS_TOOLS
// byte array offset
static jint _bao = 0;

/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    setup
 * Signature: (I)V
 */
JNIEXPORT
void JNICALL Java_protostuffdb_LsmdbJni_setup(JNIEnv * env, jclass clazz, jint bao)
{
    _bao = bao;
}
#endif

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    newDb
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - name (string, required)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_newDb(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_newDb(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    char* str;
    uint16_t strLen;

    bool end;
    char* endPtr;
    char* in;
    
    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif

    if (
        // flags
        !protostuff::read8(buf, endPtr, &in, &flags) || 
        // name
        !protostuff::readString(buf, nullptr, &in, &str, &strLen, true)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

        return false;
        #else
        return;
        #endif
    }
    
    std::string path(str, strLen);
    
    intptr_t ptr = protostuff::newDb(buf, in, endPtr, 
            path, flags);
    
    if (ptr == 0)
        *buf = protostuff::ERR_OPERATION;
    else
        protostuff::writePtrDB(buf, (uint64_t)ptr);
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return ptr != 0;
    #endif
}

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    deleteDb
 * Signature: ([B)Z
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_deleteDb(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_deleteDb(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif
    
    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

        return false;
        #else
        return;
        #endif
    }
    
    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

        return false;
        #else
        return;
        #endif
    }
    
    // TODO delete outstanding iterator before deleting db
    
    protostuff::deleteDb(dbPtr);

    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return true;
    #endif
}

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    backupDb
 * Signature: ([B)Z
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_backupDb(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_backupDb(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    char* str;
    uint16_t strLen;
    
    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif

    if (
        // flags
        !protostuff::read8(buf, endPtr, &in, &flags) || 
        // backupNameOrDir
        !protostuff::readString(buf, nullptr, &in, &str, &strLen, true)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    bool success = protostuff::backupDb(dbPtr, flags, str, strLen);
    if (!success)
        *buf = protostuff::ERR_OPERATION;
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
    
    return success;
    #endif
}

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    pipeDb
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - ptrTo (int64)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_pipeDb(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_pipeDb(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    uint64_t ptrTo;
    
    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif

    if (
        // flags
        !protostuff::read8(buf, endPtr, &in, &flags) ||
        // ptrTo
        !protostuff::read64(buf, nullptr, &in, &ptrTo)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    bool success = protostuff::pipeDb(dbPtr, flags, (intptr_t)ptrTo);
    if (!success)
        *buf = protostuff::ERR_OPERATION;
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
    
    return success;
    #endif
}

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    close
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_close(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_close(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    
    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif
    
    // flags
    if (!protostuff::read8(buf, nullptr, &in, &flags))
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    intptr_t itPtr = (intptr_t)protostuff::readPtrIterator(buf);
    intptr_t batchPtr = (intptr_t)protostuff::readPtrWriteBatch(buf);
    
    if (0 != itPtr)
    {
        // clear
        protostuff::writePtrIterator(buf, 0);
        // delete
        protostuff::deleteIterator(itPtr);
    }

    if (0 != batchPtr)
    {
        // clear
        protostuff::writePtrWriteBatch(buf, 0);
        // delete or flush
        if (0 != (flags & protostuff::FCLOSE_CLEANUP_ONLY))
        {
            // delete only 
            protostuff::deleteWriteBatch(batchPtr);
        }
        else if (!protostuff::flushWriteBatch(batchPtr, dbPtr, 0 != (flags & protostuff::FOP_CLOSE_SYNC)))
        {
            *buf = protostuff::ERR_WRITEBATCH_FLUSH;

            #ifdef PS_TOOLS
            if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
            
            return false;
            #else
            return;
            #endif
        }
    }

    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return true;
    #endif
}

/* ================================================== */

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    sput
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - key (bytes)
 *   - value (bytes)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_sput(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_sput(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    char* k;
    uint16_t klen;
    char* v;
    uint16_t vlen;

    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif
    
    if ( // flags
        !protostuff::read8(buf, endPtr, &in, &flags) || 
        // key
        !protostuff::readBytes(buf, endPtr, &in, &k, &klen, true) || 
        // value
        !protostuff::readBytes(buf, nullptr, &in, &v, &vlen, false)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    bool success = protostuff::sput(dbPtr, flags, k, klen, v, vlen);
    if (!success)
        *buf = protostuff::ERR_OPERATION;
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return success;
    #endif
}

/* ================================================== */

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    put
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - kv entries
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_put(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_put(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    
    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif
    
    // flags
    if (!protostuff::read8(buf, nullptr, &in, &flags))
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // writebatch
    intptr_t batchPtr = (intptr_t)protostuff::readPtrWriteBatch(buf);

    if (endPtr == in)
    {
        if (batchPtr == 0)
        {
            *buf = protostuff::ERR_EXPECTED_MORE_ARGS;

            #ifdef PS_TOOLS
            if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
            
            return false;
            #else
            return;
            #endif
        }
        
        // end of session
        if (end && !protostuff::flushWriteBatch(
                dbPtr, batchPtr, 
                0 != (flags & protostuff::FPUT_SYNC)))
        {
            *buf = protostuff::ERR_WRITEBATCH_FLUSH;

            #ifdef PS_TOOLS
            if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
            
            return false;
            #else
            return;
            #endif
        }
        
        if (0 != (flags & protostuff::FOP_CLOSE) && !protostuff::isChunk(buf))
        {
            intptr_t itPtr = (intptr_t)protostuff::readPtrIterator(buf);
            //batchPtr = (intptr_t)protostuff::readPtrWriteBatch(buf);
            
            if (0 != itPtr)
            {
                // clear
                protostuff::writePtrIterator(buf, 0);
                // delete
                protostuff::deleteIterator(itPtr);
            }

            if (!end)
            {
                // clear
                protostuff::writePtrWriteBatch(buf, 0);
                // flush
                if (!protostuff::flushWriteBatch(batchPtr, dbPtr, 0 != (flags & protostuff::FOP_CLOSE_SYNC)))
                {
                    *buf = protostuff::ERR_WRITEBATCH_FLUSH;

                    #ifdef PS_TOOLS
                    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
                    
                    return false;
                    #else
                    return;
                    #endif
                }
            }
        }
        
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

        return true;
        #else
        return;
        #endif
    }

    bool success = protostuff::put(buf, in, endPtr, end, 
            dbPtr, batchPtr, 
            flags);
    
    if (!success)
    {
        *buf = protostuff::ERR_OPERATION;
    }
    else if (0 != (flags & protostuff::FOP_CLOSE) && !protostuff::isChunk(buf))
    {
        intptr_t itPtr = (intptr_t)protostuff::readPtrIterator(buf);
        batchPtr = (intptr_t)protostuff::readPtrWriteBatch(buf);
        
        if (0 != itPtr)
        {
            // clear
            protostuff::writePtrIterator(buf, 0);
            // delete
            protostuff::deleteIterator(itPtr);
        }

        if (0 != batchPtr)
        {
            // clear
            protostuff::writePtrWriteBatch(buf, 0);
            // flush
            if (!protostuff::flushWriteBatch(batchPtr, dbPtr, 0 != (flags & protostuff::FOP_CLOSE_SYNC)))
            {
                *buf = protostuff::ERR_WRITEBATCH_FLUSH;
                success = false;
            }
        }
    }
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return success;
    #endif
}

/* ================================================== */

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    sget
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - key (bytes)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_sget(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_sget(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    char* key;
    uint16_t keylen;

    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif

    if ( // flags
        !protostuff::read8(buf, endPtr, &in, &flags) || 
        // key
        !protostuff::readBytes(buf, nullptr, &in, &key, &keylen, true)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    std::string value;
    
    if (!protostuff::sget(dbPtr, flags, key, keylen, &value))
    {
        *buf = protostuff::ERR_OPERATION;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    // check if has enough space to write
    if (value.length() + 2 > (uint16_t)(buf + protostuff::readBufLen(buf) - endPtr))
    {
        *buf = protostuff::ERR_NOT_ENOUGH_SPACE_TO_WRITE;
    }
    else
    {
        *(uint16_t*)(endPtr) = value.length();
        memcpy(endPtr + 2, value.data(), value.length());
    }
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return true;
    #endif
}

/* ================================================== */

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    iget
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - key (bytes)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_iget(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_iget(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    char* key;
    uint16_t keylen;

    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif

    if ( // flags
        !protostuff::read8(buf, endPtr, &in, &flags) || 
        // key
        !protostuff::readBytes(buf, nullptr, &in, &key, &keylen, true)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    #ifndef PS_TOOLS
    protostuff::iget(dbPtr, buf, endPtr, flags, key, keylen);
    #else
    bool ok = protostuff::iget(dbPtr, buf, endPtr, flags, key, keylen);
    
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return ok;
    #endif
}

/* ================================================== */

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    get
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - k entries
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_get(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_get(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;

    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif
    
    // flags
    if (!protostuff::read8(buf, nullptr, &in, &flags))
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // iterator
    intptr_t itPtr = (intptr_t)protostuff::readPtrIterator(buf);

    if (endPtr == in)
    {
        if (itPtr == 0)
        {
            *buf = protostuff::ERR_EXPECTED_MORE_ARGS;

            #ifdef PS_TOOLS
            if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
            
            return false;
            #else
            return;
            #endif
        }
        
        // end of session
        if (end)
            protostuff::deleteIterator(itPtr);
        
        if (0 != (flags & protostuff::FOP_CLOSE) && !protostuff::isChunk(buf))
        {
            intptr_t batchPtr = (intptr_t)protostuff::readPtrWriteBatch(buf);
            //itPtr = (intptr_t)protostuff::readPtrIterator(buf);
            
            if (!end)
            {
                // clear
                protostuff::writePtrIterator(buf, 0);
                // delete
                protostuff::deleteIterator(itPtr);
            }

            if (0 != batchPtr)
            {
                // clear
                protostuff::writePtrWriteBatch(buf, 0);
                // flush
                if (!protostuff::flushWriteBatch(batchPtr, dbPtr, 0 != (flags & protostuff::FOP_CLOSE_SYNC)))
                {
                    *buf = protostuff::ERR_WRITEBATCH_FLUSH;

                    #ifdef PS_TOOLS
                    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
                    
                    return false;
                    #else
                    return;
                    #endif
                }
            }
        }
        
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

        return true;
        #else
        return;
        #endif
    }

    bool success = protostuff::get(buf, in, endPtr, end, protostuff::readBufLen(buf), 
            dbPtr, itPtr, 
            flags);
    
    if (!success)
    {
        *buf = protostuff::ERR_OPERATION;
    }
    else if (0 != (flags & protostuff::FOP_CLOSE) && !protostuff::isChunk(buf))
    {
        intptr_t batchPtr = (intptr_t)protostuff::readPtrWriteBatch(buf);
        itPtr = (intptr_t)protostuff::readPtrIterator(buf);
        
        if (0 != itPtr)
        {
            // clear
            protostuff::writePtrIterator(buf, 0);
            // delete
            protostuff::deleteIterator(itPtr);
        }

        if (0 != batchPtr)
        {
            // clear
            protostuff::writePtrWriteBatch(buf, 0);
            // flush
            if (!protostuff::flushWriteBatch(batchPtr, dbPtr, 0 != (flags & protostuff::FOP_CLOSE_SYNC)))
            {
                *buf = protostuff::ERR_WRITEBATCH_FLUSH;

                #ifdef PS_TOOLS
                success = false;
                #endif
            }
        }
    }
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
    
    return success;
    #endif
}

/* ================================================== */

#ifdef PS_TOOLS
/*
 * Class:     protostuffdb_LsmdbJni
 * Method:    scan
 * Signature: ([B)Z
 *
 * Required params:
 *   - flags (int8)
 *   - limit (int16)
 *   - limitKey (bytes)
 *   - startKey (bytes)
 */
JNIEXPORT
jboolean JNICALL Java_protostuffdb_LsmdbJni_scan(JNIEnv * env, jclass clazz, jbyteArray data)
#else
void JNICALL Java_protostuffdb_Jni_scan(JNIEnv * env, jclass clazz, jlong contextPtr)
#endif
{
    uint8_t flags;
    uint16_t limit;
    char* limitKey;
    uint16_t limitKeyLen;
    char* startKey;
    uint16_t startKeyLen;

    bool end;
    char* endPtr;
    char* in;

    #ifdef PS_TOOLS
    jint bao = _bao;
    char* const bufTmp = nullptr;
    char* const buf = bao == 0 ? 
            protostuff::getBuf(env, data, &end, &endPtr, &in) :
            protostuff::getBufDirect(env, protostuff::buf_from_bao(data, bao), &end, &endPtr, &in);
    if (buf == nullptr)
        return false;
    #else
    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* const bufTmp = context->bufTmp + 4; // do not write on the first 4 bytes
    char* const buf = protostuff::getBufDirect(env, context->bufDb, &end, &endPtr, &in);
    if (buf == nullptr)
        return;
    #endif
    
    if ( // flags
        !protostuff::read8(buf, endPtr, &in, &flags) || 
        // limit
        !protostuff::read16(buf, endPtr, &in, &limit) ||
        // limitKey
        !protostuff::readBytes(buf, endPtr, &in, &limitKey, &limitKeyLen, false) || 
        // startKey
        !protostuff::readBytes(buf, nullptr, &in, &startKey, &startKeyLen, false)
    )
    {
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    if (endPtr != in)
    {
        *buf = protostuff::ERR_NOT_EXPECTING_MORE_ARGS;

        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }

    // db
    intptr_t dbPtr = (intptr_t)protostuff::readPtrDB(buf);
    if (dbPtr == 0)
    {
        *buf = protostuff::ERR_MISSING_DB;
        
        #ifdef PS_TOOLS
        if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);
        
        return false;
        #else
        return;
        #endif
    }
    
    bool success = protostuff::scan(buf, bufTmp, 
            in, endPtr, end, protostuff::readBufLen(buf), 
            dbPtr, (intptr_t)protostuff::readPtrIterator(buf), (intptr_t)protostuff::readPtrWriteBatch(buf), 
            flags, 
            0 != (flags & protostuff::FSCAN_KEYS_ONLY), 
            0 != (flags & protostuff::FSCAN_DESC), 
            0 != (flags & protostuff::FSCAN_DELETE_RESULT), 
            limit, 
            limitKey, limitKeyLen, 
            startKey, startKeyLen);
    
    if (!success && *buf == 0)
    {
        *buf = protostuff::ERR_OPERATION;
    }
    
    #ifdef PS_TOOLS
    if (bao == 0) env->ReleasePrimitiveArrayCritical(data, buf, 0);

    return success;
    #endif
}

#ifdef PS_TOOLS
} // extern C
#endif

