//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

#include "protostuff.h"

namespace protostuff {

// global flags
uint8_t flags = 0;

char * const 
getBuf(JNIEnv * env, jbyteArray data, bool* end, char** endPtr, 
        char** inPtr)
{
    // required
    if (data == nullptr)
        return nullptr;
    
    char* const buf = (char* const)env->GetPrimitiveArrayCritical(data, 0);
    if (buf == nullptr)
        return nullptr;
    
    switch(*(uint8_t*)(buf + protostuff::OFFSET_CTRL))
    {
        case WIRETYPE_CHUNK:
        case WIRETYPE_LIMIT:
            *end = false;
            break;
        
        case WIRETYPE_END:
            *end = true;
            break;
        
        default:
            *buf = protostuff::ERR_EXPECTED_END_OR_LIMIT;
            env->ReleasePrimitiveArrayCritical(data, buf, 0);
            return nullptr;
    }
    
    // + 1 (after the wiretype, which is the 2-byte end-offset)
    *endPtr = buf + *(uint16_t*)(buf + protostuff::OFFSET_CTRL + 1);

    // jump ahead if kvao value size is set
    *inPtr = buf + protostuff::OFFSET_START + *(uint16_t*)(buf + protostuff::OFFSET_START - 2);
    
    return buf;
}

char * const 
getBufDirect(JNIEnv * env, char* const buf, bool* end, char** endPtr, 
        char** inPtr)
{
    #ifdef PS_TOOLS
    if (buf == nullptr)
        return nullptr;
    #endif
    
    switch(*(uint8_t*)(buf + protostuff::OFFSET_CTRL))
    {
        case WIRETYPE_CHUNK:
        case WIRETYPE_LIMIT:
            *end = false;
            break;
        
        case WIRETYPE_END:
            *end = true;
            break;
        
        default:
            *buf = protostuff::ERR_EXPECTED_END_OR_LIMIT;
            return nullptr;
    }
    
    // + 1 (after the wiretype, which is the 2-byte end-offset)
    *endPtr = buf + *(uint16_t*)(buf + protostuff::OFFSET_CTRL + 1);

    // jump ahead if kvao value size is set
    *inPtr = buf + protostuff::OFFSET_START + *(uint16_t*)(buf + protostuff::OFFSET_START - 2);
    
    return buf;
}

bool
readString(char* const buf, char* endPtr, char** inPtr, 
        char** str, uint16_t* strLen, const bool mustNotBeEmpty)
{
    char* in = *inPtr;
    if (WIRETYPE_STRING != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_STRING;
        return false;
    }

    *strLen = *(uint16_t*)(in);
    in += 2;
    *str = in;

    // move to next (and skip the null pointer)
    in += (*strLen + 1);

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    if (mustNotBeEmpty && 0 == *strLen)
    {
        *buf = protostuff::ERR_EMPTY_STRING;
        return false;
    }

    *inPtr = in;
    return true;
}

bool
readBytes(char* const buf, char* endPtr, char** inPtr, 
        char** v, uint16_t* vlen, const bool mustNotBeEmpty)
{
    char* in = *inPtr;
    if (WIRETYPE_BYTES != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_BYTES;
        return false;
    }

    *vlen = *(uint16_t*)(in);
    in += 2;
    *v = in;

    // move to next
    in += *vlen;

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    if (mustNotBeEmpty && 0 == *vlen)
    {
        *buf = protostuff::ERR_EMPTY_BYTES;
        return false;
    }

    *inPtr = in;
    return true;
}

/*bool
readBool(char* const buf, char* endPtr, char** inPtr, 
        bool* target)
{
    char* in = *inPtr;
    if (WIRETYPE_UINT8 != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_UINT8;
        return false;
    }

    *target = 0 != *in++;

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    *inPtr = in;
    return true;
}*/

bool
read8(char* const buf, char* endPtr, char** inPtr, 
        uint8_t* target)
{
    char* in = *inPtr;
    if (WIRETYPE_UINT8 != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_UINT8;
        return false;
    }

    *target = *in++;

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    *inPtr = in;
    return true;
}

bool
read16(char* const buf, char* endPtr, char** inPtr, 
        uint16_t* target)
{
    char* in = *inPtr;
    if (WIRETYPE_UINT16 != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_UINT16;
        return false;
    }

    *target = *(uint16_t*)(in);
    in += 2;

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    *inPtr = in;
    return true;
}

bool
read32(char* const buf, char* endPtr, char** inPtr, 
        uint32_t* target)
{
    char* in = *inPtr;
    if (WIRETYPE_UINT32 != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_UINT32;
        return false;
    }

    *target = *(uint32_t*)(in);
    in += 4;

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    *inPtr = in;
    return true;
}

bool
read64(char* const buf, char* endPtr, char** inPtr, 
        uint64_t* target)
{
    char* in = *inPtr;
    if (WIRETYPE_UINT64 != *in++)
    {
        *buf = protostuff::ERR_EXPECTED_UINT64;
        return false;
    }

    *target = *(uint64_t*)(in);
    in += 8;

    if (endPtr == in)
    {
        *buf = protostuff::ERR_EXPECTED_MORE_ARGS;
        return false;
    }

    *inPtr = in;
    return true;
}

bool
readK(char* const buf, char** inPtr, 
        char** k, uint8_t* klen)
{
    char* in = *inPtr;
    
    if (WIRETYPE_K != *(uint8_t*)in++)
    {
        *buf = protostuff::ERR_EXPECTED_K;
        return false;
    }

    *klen = *(uint8_t*)in++;

    // trailing key
    *k = in;
    in += *klen;
    
    *inPtr = in;
    return true;
}

/*bool
readV(char* const buf, char** inPtr, 
        char** v, uint16_t* vlen)
{
    char* in = *inPtr;
    
    if (WIRETYPE_V != *(uint8_t*)in++)
    {
        *buf = protostuff::ERR_EXPECTED_V;
        return false;
    }

    *vlen = *(uint16_t*)in;
    in += 2;

    // trailing value
    *v = in;
    in += *vlen;
    
    *inPtr = in;
    return true;
}*/

bool
readKV(char* const buf, char** inPtr, 
        char** k, uint16_t* klen, 
        char** v, uint16_t* vlen, 
        bool* rkp)
{
    char* in = *inPtr;
    
    switch(*(uint8_t*)in++)
    {
        case WIRETYPE_KV:
            *klen = *(uint16_t*)in;
            in += 2;
            
            *vlen = *(uint16_t*)in;
            in += 2;
            
            // trailing key and value
            *k = in;
            in += *klen;

            *v = in;
            in += *vlen;

            *inPtr = in;
            return true;

        case WIRETYPE_KVAO:
            *klen = *(uint16_t*)in;
            in += 2;
            
            // the absolute offset of a serialized WIRETYPE_KV
            *vlen = *(uint16_t*)in;
            // +4 (after the length delimiter) + key len
            *v = buf + *vlen + 4 + *(uint16_t*)(buf + *vlen); // deserialize the key delimiter
            // deserialize the value delimiter
            *vlen = *(uint16_t*)(buf + *vlen + 2);
            in += 2;
            
            // trailing key
            *k = in;
            in += *klen;
            
            *inPtr = in;
            return true;

        case WIRETYPE_KVRO:
            *klen = *(uint16_t*)in;
            in += 2;

            // the length (to deduct) that points to a previous offset of a serialized WIRETYPE_KV
            *vlen = *(uint16_t*)in;
            // +4 (after the length delimiter) + key len
            *v = in - *vlen + 4 + *(uint16_t*)(in - *vlen); // deserialize the key delimiter
            // deserialize the value delimiter
            *vlen = *(uint16_t*)(in - *vlen + 2);
            in += 2;

            // trailing key
            *k = in;
            in += *klen;
            
            *inPtr = in;
            return true;

        case WIRETYPE_KVRKP:
            // the length (to deduct) that points to a previous offset of a serialized WIRETYPE_KV
            *klen = *(uint16_t*)in;

            // +4 (after the length delimiter)
            *k = in - *klen + 4;
            // deserialize the key delimiter
            *klen = *(uint16_t*)(in - *klen);
            
            // deserialize the value delimiter
            *vlen = *(uint16_t*)(*k - 2);
            *v = *k + *klen;
            
            // skip the next 2 bytes
            in += 4;

            // set the flag
            *rkp = true;

            *inPtr = in;
            return true;

        default:
            *buf = protostuff::ERR_EXPECTED_KV_VARIANT;
            return false;
    }
}
   
} // protostuff
