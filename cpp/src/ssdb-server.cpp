/*
Copyright (c) 2012-2015 The SSDB Authors. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.
*/
#include "include.h"
#include "version.h"
#include "net/server.h"
#include "ssdb/ssdb.h"
#include "util/app.h"
#include "serv.h"
#include <jni.h>
#include "protostuff/protostuff.h"

extern "C" {
#include <openssl/evp.h>
#include <openssl/md5.h>
}

/*extern "C" {
    #include <dlfcn.h>
}
typedef jint (*jni_createvm_pt)(JavaVM **pvm, void **penv, void *args);
*/

inline void
write16(char* buf, uint16_t value)
{
    *(uint16_t*)(buf) = value;
}

inline uint16_t read16(char* buf)
{
    return *(uint16_t*)(buf);
}

void JNICALL Java_protostuffdb_Jni_newDb(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_deleteDb(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_backupDb(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_close(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_sput(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_put(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_sget(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_iget(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_get(JNIEnv * env, jclass clazz, jlong contextPtr);
void JNICALL Java_protostuffdb_Jni_scan(JNIEnv * env, jclass clazz, jlong contextPtr);

const std::string dot_jar = ".jar";

// encrypted session
const std::string pfx_es = "-Des.";

inline bool ends_with(std::string const & value, std::string const & suffix)
{
    return suffix.size() <= value.size() && std::equal(suffix.rbegin(), suffix.rend(), value.rbegin());
}

inline bool starts_with(std::string const & value, std::string const & prefix)
{
    return prefix.size() <= value.size() && std::equal(prefix.begin(), prefix.end(), value.begin());
}

static bool ex(JNIEnv *jvm_env)
{
    bool occured = jvm_env->ExceptionOccurred();
    if (occured)
    {
        jvm_env->ExceptionDescribe();
        jvm_env->ExceptionClear();
    }
    return occured;
}

static bool check(bool condition, JNIEnv *jvm_env) {
    if (condition && jvm_env->ExceptionOccurred()) {
        jvm_env->ExceptionDescribe();
        jvm_env->ExceptionClear();
    }

    return condition;
}

class MyApplication : public Application
{
public:
    SSDB *data_db;
    SSDB *meta_db;
    NetworkServer *net;
    SSDBServer *server;
    int writers;
    int readers;
    int bgworkers;
    int byte_array_offset;
    int main_class_offset;
    JNIEnv *jvm_env;
    JavaVM *jvm;
    jclass class_;
    jmethodID createThread_;
    
    // jvm args offset
    int other_args_start;
    
    // encryption
    const char* encrypted_session_key = nullptr;
    int encrypted_session_key_len = 0;

    const char* encrypted_session_iv = nullptr;
    int encrypted_session_iv_len = 0;

    int64_t encrypted_session_expires = 0;
    
    bool serve();
    virtual void usage(int argc, char **argv);
    virtual void welcome();
    virtual void run();
private:
    void cleanup(bool success);
    void destroy_jvm();
    bool init_jvm();
    bool init_jni(const char* lookupClass);
};

void MyApplication::welcome(){}

void MyApplication::usage(int argc, char **argv){
	printf("Usage:\n");
	printf("    %s [-d] /path/to/ssdb.conf [-s start|stop|restart] -Djava.class.path=/path/to/app.jar com.example.Main\n", argv[0]);
	printf("Options:\n");
	printf("    -d    run as daemon\n");
	printf("    -s    option to start|stop|restart the server\n");
	printf("    -h    show this message\n");
}

void MyApplication::run(){
	Options option;
	option.load(*conf);

	std::string data_db_dir = app_args.work_dir + "/data";
	std::string meta_db_dir = app_args.work_dir + "/meta";

	log_info("ssdb-server %s", SSDB_VERSION);
	log_info("conf_file        : %s", app_args.conf_file.c_str());
	log_info("log_level        : %s", Logger::shared()->level_name().c_str());
	log_info("log_output       : %s", Logger::shared()->output_name().c_str());
	log_info("log_rotate_size  : %" PRId64, Logger::shared()->rotate_size());

	log_info("main_db          : %s", data_db_dir.c_str());
	log_info("meta_db          : %s", meta_db_dir.c_str());
	log_info("cache_size       : %d MB", option.cache_size);
	log_info("block_size       : %d KB", option.block_size);
	log_info("write_buffer     : %d MB", option.write_buffer_size);
	log_info("max_open_files   : %d", option.max_open_files);
	log_info("compaction_speed : %d MB/s", option.compaction_speed);
	log_info("compression      : %s", option.compression.c_str());
	log_info("binlog           : %s", option.binlog? "yes" : "no");
	log_info("binlog_capacity  : %d", option.binlog_capacity);
	log_info("sync_speed       : %d MB/s", conf->get_num("replication.sync_speed"));

    data_db = NULL;
	data_db = SSDB::open(option, data_db_dir);
	if(!data_db){
		log_fatal("could not open data db: %s", data_db_dir.c_str());
		fprintf(stderr, "could not open data db: %s\n", data_db_dir.c_str());
		exit(1);
	}

    meta_db = NULL;
	meta_db = SSDB::open(Options(), meta_db_dir);
	if(!meta_db){
		log_fatal("could not open meta db: %s", meta_db_dir.c_str());
		fprintf(stderr, "could not open meta db: %s\n", meta_db_dir.c_str());
		exit(1);
	}

    cleanup(init_jvm());
}

bool MyApplication::serve(){
    net = NULL;
	net = NetworkServer::init(*conf, readers, writers);

	server = new SSDBServer(data_db, meta_db, *conf, net);
	
	log_info("pidfile: %s, pid: %d", app_args.pidfile.c_str(), (int)getpid());
	log_info("ssdb server started.");
	net->serve();

	delete net;
	delete server;

    return true;
}

void MyApplication::cleanup(bool success){
    delete meta_db;
    delete data_db;

    log_info("ssdb-server exit.");

    if (!success)
        exit(1);
}

void MyApplication::destroy_jvm() {
    jclass systemClass = jvm_env->FindClass("java/lang/System");
    jmethodID exitMethod = jvm_env->GetStaticMethodID(systemClass, "exit", "(I)V");
    jvm_env->CallStaticVoidMethod(systemClass, exitMethod, 0);
    jvm->DestroyJavaVM();
}

static bool check_jvm_args(MyApplication& app, std::string& arg)
{
    int pfx_len;
    int len = arg.size();
    
    if (starts_with(arg, pfx_es))
    {
        pfx_len = pfx_es.size();
        if (0 == arg.compare(pfx_len, 4, "key="))
        {
            pfx_len += 4;
            len = pfx_len == len ? 0 : len - pfx_len;
            if (len != 32)
            {
                fprintf(stderr, "crypt.key must be exactly 32 bytes long.\n");
                return false;
            }
            app.encrypted_session_key = arg.data() + pfx_len;
            app.encrypted_session_key_len = len;
        }
        else if (0 == arg.compare(pfx_len, 3, "iv="))
        {
            pfx_len += 3;
            len = pfx_len == len ? 0 : len - pfx_len;
            if (len != EVP_MAX_IV_LENGTH)
            {
                fprintf(stderr, "crypt.iv must be exactly %d bytes long.\n", EVP_MAX_IV_LENGTH);
                return false;
            }
            app.encrypted_session_iv = arg.data() + pfx_len;
            app.encrypted_session_iv_len = len;
        }
        else if (0 == arg.compare(pfx_len, 8, "expires="))
        {
            pfx_len += 8;
            app.encrypted_session_expires = pfx_len == len ? 0 : static_cast<int64_t>(std::stoll(arg.c_str() + pfx_len));
        }
    }
    else if (arg.length() == 28 && 0 == arg.compare("-Dprotostuffdb.checksum=true"))
    {
        protostuff::flags |= protostuff::FGLOBAL_CHECKSUM;
    }
    
    return true;
}

bool MyApplication::init_jvm() {
    const size_t size = other_args.size();
    size_t len = size, offset = size, start = 0;
    JavaVMInitArgs vm_args;
    JavaVMOption *options;
    JNIEnv *env;
    
    while (offset-- > 0 && !ends_with(other_args[offset], dot_jar))
        len--;

    if (len == size || len == 0)
    {
        fprintf(stderr, "Required jvm options: -Djava.class.path=/path/to/app.jar com.example.Main\n");
        return false;
    }
    
    for (int j = other_args_start; j < offset; j++)
    {
        if (!check_jvm_args(*this, other_args[j]))
            return false;
    }

    main_class_offset = ++offset;
    
    /*
    // from https://github.com/nginx-clojure/nginx-clojure
    void *env;
    void *libVM;
    jni_createvm_pt jvm_creator;

    if (jvm != NULL && jvm_env != NULL) {
        return 0;
    }

    // append RTLD_GLOBAL flag for Alpine Linux on which OpenJDK 7 libjvm.so 
    // can not correctly handle cross symbol links from libjava.so, libverify.so
    if (NULL == (libVM = dlopen(jvm_path, RTLD_LAZY | RTLD_GLOBAL))) {
        fprintf(stderr, "Could not open shared lib :%s,\n %s\n", jvm_path, dlerror());
        return false;
    }

    if (NULL == (jvm_creator = reinterpret_cast<jni_createvm_pt>(dlsym(libVM, "JNI_CreateJavaVM"))) &&
        // for macosx default jvm
        NULL == (jvm_creator = reinterpret_cast<jni_createvm_pt>(dlsym(libVM, "JNI_CreateJavaVM_Impl")))) {
        return false;
    }*/
    
    std::string jniClass;
    if (other_args[start] != "-jni" || other_args[++start][0] == '-') {
        // default class to lookup
        jniClass += "protostuffdb/Jni";
    } else {
        jniClass += other_args[start++];
        std::replace(jniClass.begin(), jniClass.end(), '.', '/');
    }

    len = offset - start;
    if (NULL == (options = static_cast<JavaVMOption*>(malloc(len * sizeof(JavaVMOption))))) {
        return false;
    }

    for (size_t i = 0; i < len; i++) {
        options[i].extraInfo = NULL;
        options[i].optionString = const_cast<char*>(other_args[start++].c_str());
    }

    vm_args.version = JNI_VERSION_1_6;
    vm_args.ignoreUnrecognized = JNI_TRUE;
    vm_args.options = options;
    vm_args.nOptions = len;
    vm_args.ignoreUnrecognized = false;

    /*if ((*jvm_creator)(&jvm, (void **)&env, (void *)&vm_args) < 0){
        free(options);
        fprintf(stderr, "Could not create java vm.\n");
        return 1;
    }*/

    if (JNI_CreateJavaVM(&jvm, (void **)&env, &vm_args) < 0) {
        free(options);
        fprintf(stderr, "Could not create java vm.\n");
        return false;
    }

    free(options);
    jvm_env = static_cast<JNIEnv*>(env);

    if (!init_jni(jniClass.c_str())) {
        fprintf(stderr, "Could not initialize jni env for %s\n", jniClass.c_str());
        destroy_jvm();
        return false;
    }
    
    auto mcString = other_args[offset++];
    std::replace(mcString.begin(), mcString.end(), '.', '/');
    auto mc = mcString.c_str();

    jclass mainClass = jvm_env->FindClass(mc);
    if (check(mainClass == NULL, jvm_env)) {
        fprintf(stderr, "Could not find main class: %s\n", mc);
        destroy_jvm();
        return false;
    }

    jmethodID mainMethod = jvm_env->GetStaticMethodID(mainClass, "main", "([Ljava/lang/String;)V");
    if (check(mainMethod == NULL, jvm_env)) {
        fprintf(stderr, "%s does not have the method: public static void main(String[] args)\n", mc);
        destroy_jvm();
        return false;
    }

    len = size - offset;
    jobjectArray arr = jvm_env->NewObjectArray(len, jvm_env->FindClass("java/lang/String"), NULL);
    if (check(arr == NULL, jvm_env)) {
        fprintf(stderr, "Could not create args for %s.main\n", mc);
        destroy_jvm();
        return false;
    }
    
    for (size_t i = 0; i < len; i++)
        jvm_env->SetObjectArrayElement(arr, i, jvm_env->NewStringUTF(other_args[offset++].c_str()));
    
    jvm_env->CallStaticVoidMethod(mainClass, mainMethod, arr);
    bool ok = !ex(jvm_env);

    jvm_env->DeleteLocalRef(arr);
    destroy_jvm();
    return ok;
}

static MyApplication* instance;

static void threadCreated(void* ptr, JniContext* jni)
{
    setupThread(jni, 1, (int)(intptr_t)ptr);
}

//extern "C" {

// public static native boolean init(int writers, int readers);
/*
 * Class:     protostuffdb_Jni
 * Method:    init
 * Signature: (IIII)Z
 */
//JNIEXPORT
jboolean JNICALL Java_protostuffdb_Jni_init(JNIEnv * env, jclass clazz,
        jint writers, jint readers, jint bgworkers, jint byte_array_offset) {
    instance->writers = writers;
    instance->readers = readers;
    instance->bgworkers = bgworkers;
    instance->byte_array_offset = byte_array_offset;
    instance->class_ = clazz;
    instance->createThread_ = env->GetStaticMethodID(clazz, "createThread", "(JJII)V");
    
    intptr_t pFn = (intptr_t)&threadCreated;
    
    for (int i = 0, len = bgworkers, bgstart = writers + readers; i < len; i++, bgstart++)
    {
        // create bgworkers
        instance->jvm_env->CallStaticVoidMethod(clazz, instance->createThread_, 
                (jlong)pFn, (jlong)bgstart, 1, bgstart);
    }
    
    return instance->serve();
}

// public static native boolean initThread(long ptrFn, long ptrArg, byte[] data);
/*
 * Class:     protostuffdb_Jni
 * Method:    initThread
 * Signature: (JJ[B[B[B)Z
 */
//JNIEXPORT
jboolean JNICALL Java_protostuffdb_Jni_initThread(JNIEnv * env, jclass clazz, jlong ptrFn, jlong ptrArg, 
        jbyteArray jbufRpc, jbyteArray jbufDb, jbyteArray jbufTmp)
{
    JniContext jni;
    intptr_t pFn = ptrFn,
            pArg = ptrArg;
    
    char* bufRpc = ((char*)(*(intptr_t*)jbufRpc) + instance->byte_array_offset);
    char* bufDb = ((char*)(*(intptr_t*)jbufDb) + instance->byte_array_offset);
    char* bufTmp = ((char*)(*(intptr_t*)jbufTmp) + instance->byte_array_offset);

    jni.env = env;
    jni.class_ = clazz;
    jni.handle_ = env->GetStaticMethodID(clazz, "handle", "(II)V");
    jni.buf = bufRpc;
    jni.bufDb = bufDb;
    jni.bufTmp = bufTmp;
    
    ThreadFn fn = (ThreadFn)pFn;
    (*fn)(reinterpret_cast<void*>(pArg), &jni);
    
    return true;
}

static uint64_t
convert_ntohll(uint64_t n)
{
#ifdef ntohll
    return ntohll(n);
#else
    return ((uint64_t) ntohl((unsigned long) n) << 32)
           + ntohl((unsigned long) (n >> 32));
#endif
}

static uint64_t
convert_htonll(uint64_t n)
{
#ifdef htonll
    return htonll(n);
#else
    return ((uint64_t) htonl((unsigned long) n) << 32)
           + htonl((unsigned long) (n >> 32));
#endif
}

void JNICALL Java_protostuffdb_Jni_encrypt(JNIEnv * env, jclass clazz, jlong contextPtr) {
    EVP_CIPHER_CTX           ctx;
    const EVP_CIPHER        *cipher;
    u_char                  *p, *data;
    const u_char            *key, *iv;
    int                      ret, key_len;//, iv_len;
    size_t                   block_size, buf_size, data_size;
    int                      len;
    uint64_t                 expires_time;
    time_t                   now;

    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* buf = context->bufTmp;
    size_t dst_len;
    const u_char *dst;
    const u_char *in = (const u_char*)(buf + 2);
    auto in_len = read16(buf);
    size_t totalSize;
    int64_t expires;

    key_len = instance->encrypted_session_key_len;
    if (key_len == 0) {
        write16(buf, 0);
        return;
    }
    key = (const u_char*)instance->encrypted_session_key;

    //iv_len = instance->encrypted_session_iv_len;
    iv = (const u_char*)instance->encrypted_session_iv;

    expires = instance->encrypted_session_expires;

    EVP_CIPHER_CTX_init(&ctx);

    cipher = EVP_aes_256_cbc();

    block_size = EVP_CIPHER_block_size(cipher);

    data_size = in_len + sizeof(expires_time);

    buf_size = MD5_DIGEST_LENGTH /* for the digest */
               + (data_size + block_size - 1) /* for EVP_EncryptUpdate */
               + block_size; /* for EVP_EncryptFinal */
    
    // no need to allocate a tmp buf as the buf is 0xFFFF bytes long
    totalSize = in_len + buf_size + data_size;
    p = (u_char*)(in + totalSize);

    dst = p;

    data = p + buf_size;

    memcpy(data, in, in_len);

    if (expires == 0) {
        expires_time = 0;
    } else if (-1 != (now = time(NULL))) {
        expires_time = (uint64_t) now + (uint64_t) expires;
    } else {
        EVP_CIPHER_CTX_cleanup(&ctx);
        write16(buf, 0);
        return;
    }

    //dd("expires before encryption: %lld", (long long) expires_time);

    expires_time = convert_htonll(expires_time);

    memcpy(data + in_len, (u_char *) &expires_time, sizeof(expires_time));

    MD5(data, data_size, p);

    p += MD5_DIGEST_LENGTH;

    ret = EVP_EncryptInit(&ctx, cipher, key, iv);
    if (!ret) {
        EVP_CIPHER_CTX_cleanup(&ctx);
        write16(buf, 0);
        return;
    }

    /* encrypt the raw input data */

    ret = EVP_EncryptUpdate(&ctx, p, &len, data, data_size);
    if (!ret) {
        EVP_CIPHER_CTX_cleanup(&ctx);
        write16(buf, 0);
        return;
    }

    p += len;

    ret = EVP_EncryptFinal(&ctx, p, &len);

    /* XXX we should still explicitly release the ctx
     * or we'll leak memory here */
    EVP_CIPHER_CTX_cleanup(&ctx);

    if (!ret) {
        write16(buf, 0);
        return;
    }

    p += len;

    dst_len = p - dst;

    if (dst_len > buf_size) {
        //ngx_log_error(NGX_LOG_ERR, log, 0,
        //              "encrypted_session: aes_mac_encrypt: buffer error");

        write16(buf, 0);
    } else {
        write16(buf, dst_len);
        write16(buf + 2, 2 + totalSize);
    }
}

void JNICALL Java_protostuffdb_Jni_decrypt(JNIEnv * env, jclass clazz, jlong contextPtr) {
    EVP_CIPHER_CTX           ctx;
    const EVP_CIPHER        *cipher;
    int                      ret;
    size_t                   block_size, buf_size, key_len;//, iv_len;
    int                      len;
    u_char                  *p;
    const u_char            *digest, *key, *iv;
    uint64_t                 expires_time;
    time_t                   now;

    u_char new_digest[MD5_DIGEST_LENGTH];

    JniContext* context = (JniContext*)(intptr_t)contextPtr;
    char* buf = context->bufTmp;
    size_t dst_len;
    const u_char *dst;
    const u_char *in = (const u_char*)(buf + 2);
    auto in_len = read16(buf);
    size_t totalSize;

    key_len = instance->encrypted_session_key_len;
    if (key_len == 0 || in_len < MD5_DIGEST_LENGTH) {
        write16(buf, 0);
        return;
    }
    key = (const u_char*)instance->encrypted_session_key;

    //iv_len = instance->encrypted_session_iv_len;
    iv = (const u_char*)instance->encrypted_session_iv;

    //expires = instance->encrypted_session_expires;

    digest = in;

    EVP_CIPHER_CTX_init(&ctx);

    cipher = EVP_aes_256_cbc();

    ret = EVP_DecryptInit(&ctx, cipher, key, iv);
    if (!ret) {
        EVP_CIPHER_CTX_cleanup(&ctx);
        write16(buf, 0);
        return;
    }

    block_size = EVP_CIPHER_block_size(cipher);

    buf_size = in_len + block_size /* for EVP_DecryptUpdate */
               + block_size; /* for EVP_DecryptFinal */

    totalSize = in_len + buf_size;
    p = (u_char*)(in + totalSize);

    dst = p;

    ret = EVP_DecryptUpdate(&ctx, p, &len, in + MD5_DIGEST_LENGTH,
                            in_len - MD5_DIGEST_LENGTH);

    if (!ret) {
        //dd("decrypt update failed");
        EVP_CIPHER_CTX_cleanup(&ctx);
        write16(buf, 0);
        return;
    }

    p += len;

    ret = EVP_DecryptFinal(&ctx, p, &len);

    /* XXX we should still explicitly release the ctx
     * or we'll leak memory here */
    EVP_CIPHER_CTX_cleanup(&ctx);

    if (!ret) {
        //ngx_log_debug0(NGX_LOG_DEBUG_HTTP, log, 0,
        //               "failed to decrypt session: bad AES-256 digest");
        write16(buf, 0);
        return;
    }

    p += len;

    dst_len = p - dst;

    if (dst_len > buf_size) {
        //ngx_log_error(NGX_LOG_ERR, log, 0,
        //              "encrypted_session: aes_mac_decrypt: buffer error");
        write16(buf, 0);
        return;
    }

    if (dst_len < sizeof(expires_time)) {
        write16(buf, 0);
        return;
    }

    MD5(dst, dst_len, new_digest);

    if (strncmp((const char*)digest, (const char*)new_digest, MD5_DIGEST_LENGTH) != 0) {
        //ngx_log_debug0(NGX_LOG_DEBUG_HTTP, log, 0,
        //               "failed to decrypt session: MD5 checksum mismatch");
        write16(buf, 0);
        return;
    }

    dst_len -= sizeof(expires_time);

    //dd("dst len: %d", (int) *dst_len);
    //dd("dst: %.*s", (int) *dst_len, *dst);

    p -= sizeof(expires_time);

    expires_time = convert_ntohll(*((uint64_t *) p));

    now = time(NULL);
    if (now == -1) {
        write16(buf, 0);
        return;
    }

    //dd("expires after decryption: %lld", (long long) expires_time);

    if (expires_time && expires_time <= (uint64_t) now) {
        //ngx_log_debug2(NGX_LOG_DEBUG_HTTP, log, 0,
        //               "encrypted_session: session expired: %uL <= %T",
        //               expires_time, now);
        write16(buf, 0);
    } else {
        write16(buf, dst_len);
        write16(buf + 2, 2 + totalSize);
    }
    
    //dd("decrypted successfully");
}

//} // extern C

void createThread(ThreadFn ptrFn, void* ptrArg, int type, int id) {
    intptr_t pFn = (intptr_t)ptrFn,
        pArg = (intptr_t)ptrArg;
    instance->jvm_env->CallStaticVoidMethod(instance->class_, instance->createThread_, 
            (jlong)pFn, (jlong)pArg, (jint)type, (jint)id);
}

void setupThread(JniContext* jni, int type, int id)
{
    jni->type = type;
    jni->id = id;
    jmethodID m = jni->env->GetStaticMethodID(jni->class_, "setupThread", "(IIJ)V");
    jni->env->CallStaticVoidMethod(jni->class_, m, (jint)type, (jint)id, (jlong)(intptr_t)jni);
}

bool MyApplication::init_jni(const char* lookupClass) {
    jclass jniClass = jvm_env->FindClass(lookupClass);
    if (check(jniClass == NULL, jvm_env))
        return false;
    
    JNINativeMethod methods[] = {
        { const_cast<char*>("init"), const_cast<char*>("(IIII)Z"), reinterpret_cast<void*>(Java_protostuffdb_Jni_init) },
        { const_cast<char*>("initThread"), const_cast<char*>("(JJ[B[B[B)Z"), reinterpret_cast<void*>(Java_protostuffdb_Jni_initThread) },
        { const_cast<char*>("encrypt"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_encrypt) },
        { const_cast<char*>("decrypt"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_decrypt) },
        { const_cast<char*>("newDb"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_newDb) },
        { const_cast<char*>("deleteDb"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_deleteDb) },
        { const_cast<char*>("backupDb"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_backupDb) },
        { const_cast<char*>("close"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_close) },
        { const_cast<char*>("sput"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_sput) },
        { const_cast<char*>("put"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_put) },
        { const_cast<char*>("sget"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_sget) },
        { const_cast<char*>("iget"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_iget) },
        { const_cast<char*>("get"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_get) },
        { const_cast<char*>("scan"), const_cast<char*>("(J)V"), reinterpret_cast<void*>(Java_protostuffdb_Jni_scan) }
    };

    jvm_env->RegisterNatives(jniClass, methods, sizeof(methods) / sizeof(JNINativeMethod));
    if (ex(jvm_env))
        return false;

    instance = this;
    return true;
}

int main(int argc, char **argv)
{
    if (argc == 2 && 0 == strcmp(argv[1], "--version"))
    {
        printf("%s\n", SSDB_VERSION);
        return 0;
    }

    MyApplication app;
    
    app.other_args.push_back("-XX:PretenureSizeThreshold=65534");
    app.other_args.push_back("-XX:MinTLABSize=65534");
    app.other_args.push_back("-XX:TLABSize=65534");
    app.other_args.push_back("-XX:+UseConcMarkSweepGC");
    
    app.other_args_start = app.other_args.size();
    
    return app.main(argc, argv);
}
